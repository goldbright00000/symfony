#!/bin/bash

rm -fr /var/www/html/app/cache/
mkdir /var/www/html/app/cache/dev/jms_serializer
chmod -R 777 /var/www/html/app/cache/
rm /var/lib/mongodb/mongod.lock && service mongodb restart