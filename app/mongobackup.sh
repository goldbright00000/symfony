#!/bin/bash

# Force file syncronization and lock writes
echo "Lock writes..."
mongo admin --eval "printjson(db.fsyncLock())"

MONGODUMP_PATH="/usr/bin/mongodump"
MONGO_HOST="127.0.0.1" #replace with your server ip
MONGO_PORT="27017"
MONGO_DATABASE="cab" #replace with your database name
mongo cab  --eval "printjson(db.getCollectionNames())"
mongo cab  --eval "printjson(db.Geolocation.count())"


mongo cab  --eval "printjson(db.Geolocation.find().forEach( function(x){db.GeolocationDriverbk.insert(x)}))";
