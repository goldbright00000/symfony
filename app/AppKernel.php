<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class AppKernel extends Kernel implements CompilerPassInterface
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
           // new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new CAB\MainBundle\CABMainBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\MediaBundle\SonataMediaBundle(),
            //new Sonata\ClassificationBundle\SonataClassificationBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new CAB\UserBundle\CABUserBundle(),
            new CAB\CourseBundle\CABCourseBundle(),
            new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
            new Sonata\IntlBundle\SonataIntlBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Sonata\FormatterBundle\SonataFormatterBundle(),
            new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new CAB\AdminBundle\CABAdminBundle(),
            new Vich\UploaderBundle\VichUploaderBundle(),
            new Sonata\TranslationBundle\SonataTranslationBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new CAB\ApiBundle\CABApiBundle(),
            new DotSmart\SmsBundle\DotSmartSmsBundle(),
            new CAB\ArticleBundle\CABArticleBundle(),
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new FOS\MessageBundle\FOSMessageBundle(),
            new CAB\MessageBundle\CABMessageBundle(),
            new RMS\PushNotificationsBundle\RMSPushNotificationsBundle(),
            new CAB\IosPushBundle\CABIosPushBundle(),
            new Hackzilla\Bundle\TicketBundle\HackzillaTicketBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new CAB\TicketBundle\CABTicketBundle(),
            new TFox\MpdfPortBundle\TFoxMpdfPortBundle(),
            new Presta\ImageBundle\PrestaImageBundle(),
            new CAB\CallcBundle\CABCallcBundle(),
            new Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle(),

         //   new Snc\RedisBundle\SncRedisBundle(),
            //new Sonata\DoctrSonataDoctrineORMAdminBundleine\Bridge\Symfony\Bundle\SonataDoctrineBundle()
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }
    public function __construct($environment, $debug)
    {
        date_default_timezone_set("Europe/Berlin");
        parent::__construct($environment, $debug);
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
	public function process(ContainerBuilder $container)
	{

		$container->getDefinition('nelmio_api_doc.form.extension.description_form_type_extension')->setPublic(true);
		$container->getDefinition('form.type_guesser.doctrine.mongodb')->setPublic(true);
		$container->getDefinition('sonata.core.form.type.boolean')->setPublic(true);
		$container->getDefinition('presta_image.form.type.image')->setPublic(true);
		$container->getDefinition('sonata.core.form.type.date_picker')->setPublic(true);
		$container->getDefinition('form.type.entity')->setPublic(true);
		$container->getDefinition('sonata.core.form.type.equal')->setPublic(true);
		$container->getDefinition('ivory_ck_editor.form.type')->setPublic(true);
		$container->getDefinition('assets.packages')->setPublic(true);
		$container->getDefinition('cab.planing.manager')->setPublic(true);

		//$container->get('fos_user.user_manager')->setPublic(true);
	}
}
/*
 * Try to set it in apache's php.ini. launch :

php -i |grep php.ini

to see where is your actual cli's php.ini
then set the time zone:
date.timezone = "Europe/Berlin"
 */
