<?php

namespace CAB\MainBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cab_main');

	    $rootNode
		    ->children()
		    ->arrayNode('imap')
		    ->children()
		    ->scalarNode('server')->end()
		    ->scalarNode('port')->defaultValue(143)->end()
		    ->scalarNode('box')->defaultValue('INBOX')->end()
		    ->end()
		    ->end()
		    ->end()
		    ->children()
		    ->arrayNode('access')
		    ->children()
		    ->scalarNode('email')->end()
		    ->scalarNode('password')->end()
		    ->end()
		    ->end()
		    ->end();

	    return $treeBuilder;
    }
}
