<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 29/08/2015
 * Time: 01:22
 */

namespace CAB\MainBundle\Entity;


interface CabEntityInterface {

    public function getType();
}