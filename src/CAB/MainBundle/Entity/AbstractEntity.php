<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 29/08/2015
 * Time: 01:31
 */

namespace CAB\MainBundle\Entity;

use CAB\MainBundle\Entity\CabEntityInterface;

class AbstractEntity implements CabEntityInterface {

    private $email;
    private $subject;
    private $bodyMessage;
    private $type;

    public function getType(){
        return get_class($this);
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Get bodyMessage
     *
     * @return string
     */
    public function getBodyMessage()
    {
        return $this->bodyMessage;
    }
}