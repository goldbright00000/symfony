<?php

// src/CAB/MainBundle/Entity/partner.php

namespace CAB\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\MainBundle\Entity\Partner
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_partner")
 * @ORM\Entity(repositoryClass="CAB\MainBundle\Entity\PartnerRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */

class Partner extends AbstractEntity
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $partnerName
     *
     * @ORM\Column(name="partner_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.partnerName")
     */
    protected $partnerName;


    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=64, nullable=false)
     * @Assert\Email(
     * message = "'{{ value }}' does not a valide email.",
     * checkMX = true
     * )
     */
    protected $email;

    /**
     * @var string $company
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.company")
     */
    protected $company;

    /**
     * @var string $phone
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.phone")
     */
    protected $phone;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.address")
     */
    protected $address;

    /**
     * @var string $subject
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "not.blank.subject")
     */
    protected $subject;

    /**
     * @var string $bodyMessage
     *
     * @ORM\Column(name="body_message", type="string", length=255, nullable=true)
     */
    protected $bodyMessage;

    /**
     * @var string $statusPartner
     *
     * @ORM\Column(name="status_partner", type="string", length=50, nullable=false)
     */
    protected $statusPartner;

    /**
     * @ORM\ManyToMany(targetEntity="CAB\CourseBundle\Entity\TypeVehicule", inversedBy="partners")
     * @ORM\JoinTable(name="partners_type_vehicules")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $vehiculesType;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at")
     */
    private $updatedAt;

    public function __construct() {
        $this->vehiculesType = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->setStatusPartner('New');
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    public function getType()
    {
        return "partner";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set partnerName
     *
     * @param string $partnerName
     * @return Partner
     */
    public function setPartnerName($partnerName)
    {
        $this->partnerName = $partnerName;

        return $this;
    }

    /**
     * Get partnerName
     *
     * @return string
     */
    public function getPartnerName()
    {
        return $this->partnerName;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Partner
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Partner
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Partner
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Partner
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Partner
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set statusPartner
     *
     * @param string $statusPartner
     * @return Partner
     */
    public function setStatusPartner($statusPartner)
    {
        $this->statusPartner = $statusPartner;

        return $this;
    }

    /**
     * Get statusPartner
     *
     * @return string
     */
    public function getStatusPartner()
    {
        return $this->statusPartner;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Partner
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Partner
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set bodyMessage
     *
     * @param string $bodyMessage
     * @return Partner
     */
    public function setBodyMessage($bodyMessage)
    {
        $this->bodyMessage = $bodyMessage;

        return $this;
    }

    /**
     * Get bodyMessage
     *
     * @return string
     */
    public function getBodyMessage()
    {
        return $this->bodyMessage;
    }

    /**
     * Add vehiculesType
     *
     * @param \CAB\CourseBundle\Entity\TypeVehicule $vehiculesType
     * @return Partner
     */
    public function addVehiculesType(\CAB\CourseBundle\Entity\TypeVehicule $vehiculesType)
    {
        $this->vehiculesType[] = $vehiculesType;

        return $this;
    }

    /**
     * Remove vehiculesType
     *
     * @param \CAB\CourseBundle\Entity\TypeVehicule $vehiculesType
     */
    public function removeVehiculesType(\CAB\CourseBundle\Entity\TypeVehicule $vehiculesType)
    {
        $this->vehiculesType->removeElement($vehiculesType);
    }

    /**
     * Get vehiculesType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehiculesType()
    {
        return $this->vehiculesType;
    }
}
