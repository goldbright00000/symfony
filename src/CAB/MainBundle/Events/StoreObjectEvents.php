<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:09
 */

namespace CAB\MainBundle\Events;


final class StoreObjectEvents {
    /**
     * The store.partner event is thrown each time an partner is created
     * in the system.
     *
     * The event listener receives an
     *  CAB\MainBundle\Event\StoreObjectEvent instance.
     *
     * @var string
     */
    const STORE_OBJECT = 'cab.main.event.store_object';
}