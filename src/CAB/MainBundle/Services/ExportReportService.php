<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 6/27/2016
 * Time: 7:42 PM
 */

namespace CAB\MainBundle\Services;

use CAB\CourseBundle\Entity\Course;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ExportReportService
{
    /** @var  EntityManager */
    protected $em;

    protected $mongoEm;

    protected $formFactory;

    public function __construct($em, $mongoEm, $formFactory)
    {
        $this->em = $em;
        $this->mongoEm = $mongoEm;
        $this->formFactory = $formFactory;
    }

    public function getCourses($data)
    {
        $driverId = empty($data['driverId']) ? null : (int)$data['driverId'];
        $dateInStr = $data['dateIn'];
        $dateOutStr = $data['dateOut'];

        if (strlen($dateInStr) > 0) {
            $dateIn = \DateTime::createFromFormat('m/d/Y', $dateInStr);
            if (false === $dateIn) {
                $dateIn = null;
            }
        } else {
            $dateIn = null;
        }

        if (strlen($dateOutStr) > 0) {
            $dateOut = \DateTime::createFromFormat('m/d/Y', $dateOutStr);
            if (false === $dateOut) {
                $dateOut = null;
            }
        } else {
            $dateOut = null;
        }

        if (null !== $dateIn && null !== $dateOut && $dateOut < $dateIn) {
            throw new BadRequestHttpException('error_about_date_start_more_than_date_end_label');
        }

        $races = $this->em->getRepository('CABCourseBundle:Course')->getCourcesByDriverAndDates(
            $driverId, $dateIn, $dateOut, $isPartial = true)->getQuery()->getArrayResult();

        $raceChoice = array();
        $raceChoice[] = array('id' => '', 'text' => '');
        foreach ($races as $race) {
            $text = Course::toString($race['id']) . ' ' . /* '#' . $race['client']['id'] . ' ' .*/
                $race['client']['username'] . ' ' . $race['client']['firstName'] . ' ' . $race['client']['lastName'];

            $temp = array('id' => $race['id'], 'text' => $text);
            $raceChoice[] = $temp;
        }

        return $raceChoice;
    }

    public function createPdfService($mpdfService, $html, $css)
    {
        /** @var $mpdfService \TFox\MpdfPortBundle\Service\MpdfService */
        $mpdfService->setAddDefaultConstructorArgs(false);

        $mpdf = $mpdfService->getMpdf(array('', 'A4', '', 7, 7, 7, 7));

        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($html, 2);

        return $mpdf;
    }
}