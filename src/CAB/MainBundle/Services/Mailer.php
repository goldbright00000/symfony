<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CAB\MainBundle\Services;


use Symfony\Component\Templating\EngineInterface;

class Mailer
{
protected $mailer;
protected $templating;
private $from = "no-reply@up.taxi";
private $reply = "contact@up.taxi";
private $name = "UP TAXI";

public function __construct($mailer, EngineInterface $templating)
{
$this->mailer = $mailer;
$this->templating = $templating;
}

protected function sendMessage($to, $subject, $body)
{
$mail = new \Swift_Message($subject);

$mail
->setFrom($this->from)
->setTo($to)
//->setSubject($subject)
->setBody($body)
->setReplyTo($this->reply)
->setContentType("text/html");

$this->mailer->send($mail);
}



}