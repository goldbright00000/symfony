<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 05/03/2016
 * Time: 01:59
 */

namespace CAB\MainBundle\Command\Generator;

use Symfony\Component\DomCrawler\Crawler;

class EntityManagerGenerator {
    protected static $_template =
        '<?php

<namespace>

use Doctrine\ORM\EntityManager;

/**
 * <className>
 *
 * Add your own custom manager methods below.
 */
class <className>
{
    protected $entityClass;

    /**
     * @param EntityManager $em
     * @param string        $entityClass
     */
    public function __construct(EntityManager $em, $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
    }

    /**
     * @param int $<entityName>Id
     *
     * @return <className>
     */
    public function load<className>($<entityName>Id)
    {
        return $this->getRepository()->find($<entityName>Id);
    }

    /**
     * @param <className> $<entityName>
     * @param string     $attr
     * @param int|string $value
     */
    public function setAttr(<className> $<entityName>, $attr, $value)
    {
        $attributeSetter = \'set\'.ucfirst($attr);
        $<entityName>->$attributeSetter($value);
        $this->save<className>($<entityName>);
    }

    /**
     * Save <className> entity
     *
     * @param <className> $<entityName>
     */
    public function save<className>(<className> $<entityName>)
    {
        $this->persistAndFlush($<entityName>);
    }

    /**
     * @return <repository>
     */
    public function getRepository()
    {
        return $this->em->getRepository($this->entityClass);
    }

    /**
     * Persist and flush entity
     *
     * @param $entity
     */
    protected function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

}
';

    /**
     * @param string $fullClassName
     *
     * @return string
     */
    public function generateEntityManagerClass($fullClassName, $customRepositoryClassName, $entityName)
    {
        $className = substr($fullClassName, strrpos($fullClassName, '\\') + 1, strlen($fullClassName));

        $variables = array(
            '<namespace>' => $this->generateEntityManagerNamespace($fullClassName),
            '<className>' => $className,
            '<entityName>' => $entityName,
            '<repository>' => $customRepositoryClassName
        );

        return str_replace(array_keys($variables), array_values($variables), self::$_template);
    }

    /**
     * Generates the namespace statement, if class do not have namespace, return empty string instead.
     *
     * @param string $fullClassName The full repository class name.
     *
     * @return string $namespace
     */
    private function generateEntityManagerNamespace($fullClassName)
    {
        $namespace = substr($fullClassName, 0, strrpos($fullClassName, '\\'));

        return $namespace ? 'namespace ' . $namespace . ';' : '';
    }

    /**
     * @param string $fullClassName
     * @param string $outputDirectory
     *
     * @return void
     */
    public function writeEntityManagerClass($fullClassName, $outputDirectory, $customRepositoryClassName, $entityName, $serviceConfigFile, $rootEntityName)
    {
        $code = $this->generateEntityManagerClass($fullClassName, $customRepositoryClassName, $entityName);

        $path = $outputDirectory . DIRECTORY_SEPARATOR
            . str_replace('\\', \DIRECTORY_SEPARATOR, $fullClassName) . '.php';
        $dir = dirname($path);


        if (! is_dir($dir)) {
            mkdir($dir, 0775, true);
        }

        if (! file_exists($path)) {
            file_put_contents($path, $code);
            chmod($path, 0664);
        }
        $aNameSpaces = explode('\\', $fullClassName);
        $idService = strtolower($aNameSpaces[0]).'_'.substr(strtolower($aNameSpaces[1]), 0, 6).'.manager.'.$entityName.'_manager';
        clearstatcache();
        if (file_exists($serviceConfigFile)) {
            $xml = simplexml_load_file($serviceConfigFile);
            $services = $xml->services;
            $existService = false;
            foreach ($services->children() as $child) {
                foreach ($child->attributes() as $key => $value) {
                    if ($value == $idService) {
                        $existService = true;

                    }
                }
            }

            if (!$existService) {
                $srv = $services->addChild('service');
                $srv->addAttribute('id', $idService);
                $srv->addAttribute('class', $fullClassName);
                $argService = $srv->addChild('argument');
                $argService->addAttribute('type', 'service');
                $argService->addAttribute('id', 'doctrine.orm.entity_manager');
                $argServiceEntity = $srv->addChild('argument', $rootEntityName);

                $xml->saveXML($serviceConfigFile);
            }
        } else {
            $errorMessage = 'Error opening file XML : '.$serviceConfigFile;
            exit($errorMessage);
        }
    }
}