<?php
/**
 * Created by PhpStorm.
 * Project: cab2018
 * User: m.benhenda
 * Date: 09/01/2018
 * Time: 16:56
 */

namespace CAB\MainBundle\Command;


use CAB\MainBundle\Imap\ImapParser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseMailboxCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('parse:mailbox')
			->setDescription('Parse the mail box')
			->addArgument(
				'folder',
				InputArgument::OPTIONAL,
				'The folder in mailBox e.g INBOX',
				'INBOX'
			)
			->addArgument(
				'since',
				InputArgument::OPTIONAL,
				'Scan mails from the date given e.g 09 Jan 2018',
				date ( 'd M Y' )
			)
			->addArgument(
				'host_sender',
				InputArgument::OPTIONAL,
				'string matches the mail host of the sender e.g tasm.fr',
				'gmail.com'
			)
			->setHelp(<<<EOT
The <info>%command.name%</info> command parse the mailbox address

* Parse the mailbox and give the mails sent by "host_sender" argument in "folder" argument from the "since" argument 

  <info>php %command.full_name% "INBOX" "09 Jan 2018" "gmail.com"</info>

EOT
	);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$folder = $input->getArgument('folder');
		$since = $input->getArgument('since');
		$hostSender = $input->getArgument('host_sender');

		$imap = $this->getContainer()->getParameter('imap');
		$access = $this->getContainer()->getParameter('access');
		$webDir = $this->getContainer()->get('kernel')->getRootDir() . '/../web';
		$courseManager = $this->getContainer()->get('cab.course_manager');
		$commandSncfManager = $this->getContainer()->get('cab_course.manager.commandSncf_manager');
		$parseImap = new ImapParser($courseManager, $commandSncfManager, $imap, $access, $since, $webDir);
		$result = $parseImap->handle();
		if (count($result)) {
			foreach ($result as $item) {
				$output->writeln('courseID : '.$item[0]);
				$output->writeln('Command SNCF ID : '.$item[1]);
			}
		}


	}
}