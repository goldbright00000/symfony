<?php
/**
 * Created by PhpStorm.
 * Project: cab2018
 * User: m.benhenda
 * Date: 09/01/2018
 * Time: 18:49
 */

namespace CAB\MainBundle\Imap;

use CAB\CourseBundle\Manager\CommandSncfManager;
use CAB\CourseBundle\Manager\CourseManager;

class ImapParser
{
	private $imap;

	private $access;

	private $since;

	private $webDir;

	/**
	 * @var \CAB\CourseBundle\Manager\CourseManager
	 */
	private $courseManager;

	/**
	 * @var CommandSncfManager $commandSncfManager
	 */
	private $commandSncfManager;

	public function __construct(CourseManager $courseManager, CommandSncfManager $commandSncfManager, array $imap, array $access, $since, $webDir)
	{
		$this->imap = $imap;
		$this->access = $access;
		$this->since = $since;
		$this->webDir = $webDir;
		$this->courseManager = $courseManager;
		$this->commandSncfManager = $commandSncfManager;
	}

	public function handle()
	{
		$imapMailBox = new IMAPMailbox($this->imap['server'], $this->imap['port'], $this->imap['box'], $this->access['email'], $this->access['password']);
		$inbox = $imapMailBox->getStream();

		$imapMessages = $imapMailBox->search("SINCE \"$this->since\"");
		$result = [];

		if (count($imapMessages)) {
			foreach ($imapMessages as $Imessage) {
				$uid = $Imessage->getNumber();

				$sendMail = false;
				$imapMessage = $imapMailBox->getMessageByNumber($uid);
				$headerinfo = $imapMessage->getHeaderInfo(2);
				$from = $headerinfo->from[0];
				$sender = $from->mailbox.'@'.$from->host;

				if (($from->mailbox === "benhenda.med" || $headerinfo->subject === 'Command SNCF')) {
					$imapAttachements = $imapMessage->getAttachments();
					/** @var \CAB\MainBundle\Imap\IMAPAttachment $imapAttachement */
					if (count($imapAttachements)) {
						foreach ($imapAttachements as $imapAttachement) {
							if ($imapAttachement->getExtension() === 'csv' && $imapAttachement->getAttachement()->isAttachment) {
								$savedir =  $this->webDir. '/sncf-mails/';
								$savepath = $savedir . $uid.'_'.$imapAttachement->getFilename();
								file_put_contents($savepath, utf8_encode($imapAttachement));
							}
						}
					}

					$imapStructures = $imapMessage->fetchStructure();
					$countParts = count($imapStructures->parts);

					$attachments = array();
					if(isset($imapStructures->parts) && count($imapStructures->parts)) {
						for($i = 0; $i < $countParts; $i++) {
							$attachments[$i] = array(
								'is_attachment' => false,
								'filename' => '',
								'name' => '',
								'attachment' => '');

							if($imapStructures->parts[$i]->ifdparameters) {
								if (count($imapStructures)) {
									foreach($imapStructures->parts[$i]->dparameters as $object) {
										if(strtolower($object->attribute) == 'filename') {
											$attachments[$i]['is_attachment'] = true;
											$attachments[$i]['filename'] = $object->value;
										}
									}
								}
							}

							if($imapStructures->parts[$i]->ifparameters) {
								if (count($imapStructures)) {
									foreach($imapStructures->parts[$i]->parameters as $object) {
										if(strtolower($object->attribute) == 'name') {
											$attachments[$i]['is_attachment'] = true;
											$attachments[$i]['name'] = $object->value;
										}
									}
								}
							}

							if($attachments[$i]['is_attachment']) {
								$attachments[$i]['attachment'] = imap_fetchbody($inbox, $uid, $i+1);
								if($imapStructures->parts[$i]->encoding == 3) { // 3 = BASE64
									$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
								}
								elseif($imapStructures->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
									$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
								}
							}
						} // for($i = 0; $i < count($structure->parts); $i++)
					} // if(isset($structure->parts) && count($structure->parts))

					if(count($attachments)!=0){
						foreach($attachments as $attachment)
						{
							if($attachment['is_attachment'] == 1)
							{
								$contentCSV = utf8_encode($attachment['attachment']);
								$Data = str_getcsv($contentCSV, "\n"); //parse the rows
								$csvRow = 0;
								foreach($Data as &$rowd) {
									$row = str_getcsv($rowd, ";");
									if ($csvRow === 1 && count($row) >= 34) {
										$commandSncf = $row[0];
										$commndType = $row[1];
										$codeJS = $row[5];
										$codeRLT = $row[6];
										$agentTransported = $row[10] . ' ' . $row[11];
										$agentTel = $row[12];
										$courseDate = $row[13];
										$courseTime = $row[14];
										$courseDepartureAddress = $row[18] . ' ' . $row[19] . ' ' . $row[20];
										$courseArrivalAddress = $row[28] . ' ' . $row[29] . ' ' . $row[30];
										$codeBupo = $row[34];

										if (strtoupper($commndType) === 'CREATION') {
											$paramsCommandSncf = [
												'codeBupo' => $codeBupo,
												'rlt' => $codeRLT,
												'js' => $codeJS,
												'commandeCourse' => $commandSncf,
												'tarifrefSncf' => null,
											];
											$commandSncfResult = $this->commandSncfManager->createCommandSncf($paramsCommandSncf);
											$params = [
												'departAddress' => $courseDepartureAddress,
												'arrivalAddress' => $courseArrivalAddress,
												'depDate' => $courseDate,
												'depTime' => $courseTime,
												'targetType' => 0,
												'withBack' => 0,
												'estimatedTime' => '',
												'estimatedDistance' => '',
												'personNumber' => 1,
												'depPrice' => 0,
												'totalPriceTTC' => 0,
												'coordinate' => [
													'lngDepart' => null,
													'latDepart' => null,
													'latArrival' => null,
													'lngArrival' => null
												],
												'company' => null,
											];
											$result[] = $this->courseManager->createCourseForSNCF($params, $commandSncfResult);
										} else {

										}
									}
									$csvRow ++;
								}
								$sendMail = true;
							}
						}
					}
				}
				if ($sendMail) {
					$courseId = '';
					$CommandSncfId = '';
					if (count($result)) {
						$courseId = $result[0][0];
						$CommandSncfId = $result[0][1];
					}
					$message = "The command was received and the course was created. Below the id of the course and the id of the Command : 
					\r\n- Course ID :$courseId\r\n- Command SNCF ID : $CommandSncfId\r\n Best regards,\r\n UpTaxi";
					$message = wordwrap($message, 150, "\r\n");
					$headers = 'From: no-reply@tasm.fr' . "\r\n" .
						"Reply-To: $sender" . "\r\n" .
						'X-Mailer: PHP/' . PHP_VERSION;
					mail($sender, 'TASM Confirm Reception of Command SNCF', $message, $headers);
					imap_mail_move($inbox, $uid,'SNCF_MOVED_BY_UPTAXI');
					imap_expunge($inbox);

					return $result;
				}
			}
		}
	}
}