(function($){
    "use strict";

  $(document).ready(function() {
    var map;
    var marker;
    var locations = [];
    var clearInt;
    var polyline;
    var speed = 1000;

    if ($.fn.datepicker) {
      $('.trip_date').datepicker({
          onSelect: function(date) {
              var company_id = $("#company").val();

              if (company_id == "" || company_id == "undefined") {
                alert('Veuillez selectionner une company!');
                return false;
              }

              $.get(Routing.generate('company_date_geo_tracking'), {"check_date": date}, function(response) {
                
                if (response.status) {
                    $.post(Routing.generate('company_drivers', {'company_id': company_id}), {'sel_date': date}, function(response) {
                        $("#drivers").html("");
                        
                        if (response.status) {
                          $("#drivers").append($("<option>", {value: "", text: "Select your driver"}));
                          $.each(response.drivers, function(index, val) {
                            $("#drivers").append($("<option>", {value: val.id, text: val.name}));
                          });
                        }

                      }, "json");
                }
              }, "json");

          },
          dateFormat: "dd/mm/yy"
      });
      
      $('.datepicker').datepicker();

    }

    $("#company").on('change', function() {
        var getId = $(this).val(),sel_date = $('.trip_date').val();

        if (getId == "" || sel_date == "") {
          $('#drivers').html('');
          return false;
        }

        $.post(Routing.generate('company_drivers', {'company_id': getId}), {'sel_date': sel_date}, function(response) {
          $("#drivers").html("");
          
          if (response.status) {
            $("#drivers").html($("<option>", {value: '', text: "Selectionner un chauffeur"}));
            $.each(response.drivers, function(index, val) {
              $("#drivers").append($("<option>", {value: val.id, text: val.name}));
            });
          }

        }, "json");
    });

    $("#track_btn_map").on('click', function() {
      var getDriverId = $("#drivers").val(),
        getOptionId = $("#option").val() /* 1 = driver, 2 = vehicule */,
        tripDate = $('.trip_date').val();

      if (getDriverId == "" || typeof getDriverId == "undefined" || tripDate == "") {
        console.log("driver id not defined");
        return false;
      }

      $.get(Routing.generate('company_driver_geo_tracking', {'driver_id': getDriverId, 'option': getOptionId}), {"trip_date": tripDate}, function(response) {
      
        if (response.status) {
          locations = response.geos;

          if (locations.length > 0){   
            $("#mapsdiv").show();
            $("#sliderdiv").fadeIn("slow");
          }else{
            new PNotify({
                title: 'Erreur',
                text: 'Aucun chauffeur trouvé',
                type: 'error',
            });
            $("#mapsdiv").hide();
            $("#sliderdiv").hide();
            return false;
          }

          // set hours
          $("#course_hour_start").fadeIn('slow', function() {
              $(this).find('span').text(response.first_hour);
          });
          
          $("#course_hour_end").fadeIn('slow', function() {
              $(this).find('span').text(response.last_hour);
          });

          var myOptions = {
              zoom: 11,
              center: new google.maps.LatLng(locations[0].lat, locations[0].lng),
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          // Set map
          map = new google.maps.Map(document.getElementById("tracker_area"), myOptions);

          polyline = new google.maps.Polyline({
            path: locations,
            strokeColor: '#FF0000',
            strokeWeight: 3
          }); 

          polyline.setMap(map);
          marker = new google.maps.Marker({
              map: map,
              position: new google.maps.LatLng(locations[0].lat, locations[0].lng),
              title: "UP TAXI"
          });

          // manipulate slider
          $( "#slider" ).slider({
              value: 1,
              min: 1,
              max: locations.length,
          });

          var pointer = 0;

          // Start interval
          clearInt = setInterval(function() {
            if (locations[pointer].lat == "undefined") {
              clearInterval(clearInt);
              return false;
            }

            // Set generic marker on map
            marker.setPosition(new google.maps.LatLng(locations[pointer].lat, locations[pointer].lng));
            map.setCenter(new google.maps.LatLng(locations[pointer].lat, locations[pointer].lng));

            // Update position
            pointer++;

            if (pointer == locations.length) {
              var lastLocation = locations[pointer-1];
              var infowindow = createWindowInfo(lastLocation);                

              var eventHandler = marker.addListener('click', function() {
                    infowindow.open(map, marker);
                    google.maps.event.removeListener(eventHandler);
              });

              clearInterval(clearInt);
            }
          }, speed);          
        }
      }, "json");

    });

    $( "#slider" ).slider({
        min: 1,
        value: 1,
        slide: function( event, ui ) {
          var setPoint = (ui.value-1);
          marker.setPosition(new google.maps.LatLng(locations[setPoint].lat, locations[setPoint].lng));
          map.setCenter(new google.maps.LatLng(locations[setPoint].lat, locations[setPoint].lng));

          var winInfo = createWindowInfo(locations[setPoint]);                
          var winInfoHandler = marker.addListener('click', function() {
                winInfo.open(map, marker);
                google.maps.event.removeListener(winInfoHandler);
          });
        }
    });

    $(window).load(function() {
      initializeTracking();
    });

  });
})(jQuery);

function createWindowInfo(lastLocation) {
    var contentString = "";

    contentString = '<div id="content">'+
      '<p><b>Chauffeur</b>: '+ lastLocation.driver +'</p>' +
      '<p><b>Vehicule</b>: '+ lastLocation.vehicle +'</p>' +
      '<p><b>Date/Heure</b>: '+ lastLocation.dateGeo +'</p>' +
      '<p><b>Vitesse</b>: '+ lastLocation.speed +'</p>' +
      '</div>';

    return new google.maps.InfoWindow({
        content: contentString
    });
}

function initializeTracking() {
    // set options
    var myOptions = {
        zoom: 13,   
        center: new google.maps.LatLng(30.351593, -9.554270),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    // Set map
    map = new google.maps.Map(document.getElementById("tracker_area"), myOptions);
}

// move point
function setpoint() {
    // Set new position
    marker.setPosition(new google.maps.LatLng(locations[pointer].lat, locations[pointer].lng));

    // Update position
    pointer++;

    if (pointer == locations.length)
      clearInterval(clearInt);
}
