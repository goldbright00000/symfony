/* global global */

acme = function(){
var locale = "";
return{
initLocale : function(){
     locale = "fr_FR";
},
getLocale : function(length){
      if(length === 2){
          return locale.split('_')[0];
      }
      return locale;
},
initDatePicker : function(){

       
        $('#cab_coursebundle_course_departureDate').datepicker({
            'yearRange':$(this).data('yearrange'),
            'changeMonth':$(this).data('changemonth'),
            'changeYear':$(this).data('changeyear'),
            'altFormat' : 'yy-mm-dd',
            'minDate' : null,
            'maxDate': null
        });

         $('#cab_coursebundle_course_departureDate').keyup(function(e) {
            if(e.keyCode === 8 || e.keyCode === 46) {
                $.datepicker._clearDate(this);
                $('#'+id_input)[0].value = '';
            }
        });
        var dateSf = $.datepicker.parseDate('yy-mm-dd',sfInput.value);

         $('#cab_coursebundle_course_departureDate').datepicker('setDate',dateSf);
         $('#cab_coursebundle_course_departureDate').show();
        $(sfInput).hide();
    
}
}}();