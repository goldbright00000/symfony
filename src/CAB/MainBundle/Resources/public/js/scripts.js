(function($){

	"use strict";
	$(document).ready(function () {
		transfers.init();
	});

	$(window).load(function () {
		transfers.load();
	});
	
	// ANIMATIONS
	new WOW().init();

	var transfers = {
	
		init: function () {

            //bouton immediat
            $('#basic-addon3').click(function(){
               var now = new Date();
                var dayNow = now.getDate();
                var monthNow = now.getMonth() + 1;
                var yearNow = now.getFullYear();
                var hourNow = now.getHours();
                var minuteNow = now.getMinutes();
                var dateToShow =  yearNow + '-' + monthNow + '-' + dayNow + ' ' + hourNow + ':' + minuteNow;
                $('#dep-date').val(dateToShow);

            });

            // bind form using 'ajaxForm'
            $('#booking-form').submit(function() {

                var formData = this;
                for (var i=0; i < formData.length; i++) {
                    if (formData[i].name == "dep_date" || formData[i].name == "depart_address" ||
                        formData[i].name == "arrival_address" ) {
                        if(!formData[i].value)
                        {

                            //console.log(formData[i]);
                            var nameField = formData[i].name;
                            var labelField = "";
                            if(nameField == "dep_date"){
                                labelField = "Date and time of departure";
                            }
                            if(nameField == "depart_address"){
                                labelField = "Departure address";
                            }
                            if(nameField == "arrival_address"){
                                labelField = "Arrival address";
                            }
                            $('#errorModal').modal();
                            $('#errorModal .modal-body .content-error').html(
                                labelField + " is required"
                            )


                            return false;
                        }
                    }else if(formData[i].name == "numberPerson"){
                        if(!formData[i].value){
                            $('#errorModal').modal();
                            $('#errorModal .modal-body .content-error').html(
                                "Person number is required"
                            )

                            return false;
                        }else if(formData[i].value == 0 || !Number(formData[i].value))
                        {
                            $('#errorModal .modal-body .content-error').html(
                                "Person number must contain a number greater than 0"
                            )
                            return false;
                        }
                    }
                }
                if($('input[name="with_return"]:checked').val() == 1 ){

                    if(!$('#input-pickup-ret').val()) {

                        if(!formData[i].value)
                        {
                            $('#errorModal').modal();
                            $('#errorModal .modal-body .content-error').html(
                                "Departure address of return is required"
                            )
                            return false;
                        }
                    }else if( !$('#input-dropoff-ret').val()){

                        $('#errorModal').modal();
                        $('#errorModal .modal-body .content-error').html(
                            "Arrival address of return is required"
                        )
                        return false;
                    }else if( !$('#ret-date').val()){

                        $('#errorModal').modal();
                        $('#errorModal .modal-body .content-error').html(
                            "Departure date of return is required"
                        )
                        return false;
                    }
                }
            });
            $("#cab_coursebundle_course_typeVehicleCourse").on('change', function() {
                var valueSelectedOption = $("#cab_coursebundle_course_typeVehicleCourse option:selected").val();
                var selector = "#cab_coursebundle_course_vehicle";
                $.ajax({
                    url: Routing.generate('get_vehicle_by_type', { id: valueSelectedOption }),
                    data: {
                        id: valueSelectedOption
                    },
                    error: function() {
                        $('#vehicle-alert').show('slow');
                        $('#vehicle-alert-body').html('An error has occurred');
                    },
                    //dataType: 'jsonp',
                    success: function(data) {
                        $(selector).empty();
                        console.log(data);
                        if(data.result != null) {
                            data.result.forEach(function (item) {
                                console.log(item.val + '---' + item.text);
                                $('#vehicle-alert').hide();
                                $(selector).append('<option selected="" value="' + item.val + '">' + item.text + '</option>');
                            });
                        }else if(data.code_error != 0){
                            $('#vehicle-alert').show('slow');
                            $('#vehicle-alert-body').html('Error vehiclesAjax !');
                        }
                    },
                    type: 'GET'
                });
            });


            function constructDate(dateString){
                var aDate = dateString.split(".");
                var monthDate = aDate[1];
                var dayDate = aDate[0];
                var aLastItemDate = aDate[2].split(" ");
                var yearDate = aLastItemDate[0];
                var aTimeDate = aLastItemDate[1].split(":");
                var hourDate = aTimeDate[0];
                var minDate = aTimeDate[1];
                var objDate = new Date(yearDate, monthDate, dayDate, hourDate, minDate, 0, 0);
                return objDate;
                //new Date(year, month, day, hours, minutes, seconds, milliseconds)
            }



            var locale = $('#locale-input').val();
            $('#dep-date').datetimepicker({
                format: 'Y-m-d H:i',
                lang: locale,
                step: 30,
                startDate: new Date()
            });
            $('#cab_coursebundle_course_departureDate').datetimepicker({
                format: 'Y-m-d',
                lang: locale,
                startDate: new Date()
            });

            $('#ret-date').datetimepicker({
                format: 'Y-m-d H:i',
                lang: locale,
                step: 30,
                startDate: new Date()
            });

			// MOBILE MENU
			$('.main-nav').slicknav({
				prependTo:'.header .wrap',
				label:''
			});
			
			// CUSTOM FORM ELEMENTS
			$('input[type=radio], input[type=checkbox],input[type=number], select').uniform();
			
			// SEARCH RESULTS 
			$('.information').hide();
			$('.trigger').click(function () {
				$(this).parent().parent().nextAll('.information').slideToggle(500);
			});
			$('.close').click(function () {
			   $('.information').hide(500);
			});	
			
			// FAQS
			$('.faqs dd').hide();
			$('.faqs dt').click(function () {
				$(this).next('.faqs dd').slideToggle(500);
				$(this).toggleClass('expanded');
			});
			
			// CONTACT FORM
			$('#contactform').submit(function(){
				var action = $(this).attr('action');
				$("#message").show(500,function() {
				$('#message').hide();
				$('#submit')
					.after('<img src="images/contact-ajax-loader.gif" class="loader" />')
					.attr('disabled','disabled');
				
				$.post(action, { 
					name: $('#name').val(),
					email: $('#email').val(),
					comments: $('#comments').val()
				},
				function(data){
					document.getElementById('message').innerHTML = data;
					$('#message').slideDown('slow');
					$('#contactform img.loader').fadeOut('slow',function(){$(this).remove()});
					$('#submit').removeAttr('disabled'); 
				});
				
				});
				return false; 
			});
			
			// TABS
			$('.tab-content').hide().first().show();
			$('.tabs li:first').addClass('active');

			$('.tabs a').on('click', function (e) {
				e.preventDefault();
				$(this).closest('li').addClass('active').siblings().removeClass('active');
				$($(this).attr('href')).show().siblings('.tab-content').hide();
			});

			var hash = $.trim( window.location.hash );
			if (hash) $('.tabs a[href$="'+hash+'"]').trigger('click');
			
			// SMOOTH ANCHOR SCROLLING
			var $root = $('html, body');
			$('a.anchor').click(function(e) {
				var href = $.attr(this, 'href');
				if (typeof ($(href)) != 'undefined' && $(href).length > 0) {
					var anchor = '';
					
					if(href.indexOf("#") != -1) {
						anchor = href.substring(href.lastIndexOf("#"));
					}
						
					if (anchor.length > 0) {
						console.log($(anchor).offset().top);
						console.log(anchor);
						$root.animate({
							scrollTop: $(anchor).offset().top
						}, 500, function () {
							window.location.hash = anchor;
						});
						e.preventDefault();
					}
				}
			});
		
		},
		load: function () {
			// UNIFY HEIGHT
			var maxHeight = 0;
				
			$('.heightfix').each(function(){
				if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
			});
			$('.heightfix').height(maxHeight);	

			// PRELOADER
			$('.preloader').fadeOut();
		}
	}

})(jQuery);