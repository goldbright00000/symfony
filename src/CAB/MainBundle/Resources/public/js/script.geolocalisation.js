/**
 * Created by Administrateur on 04/10/2015.
 */
(function($){

    "use strict";
    $(document).ready(function () {
        allgeolocalisation.init();

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(cabgeolocalisation.successFunction);
        }
        $('#booking-form').submit(function(){


            var lat_depart = $('#lat-input').val();
            var lng_depart = $('#lng-input').val();
            var origin = new google.maps.LatLng(lat_depart, lng_depart);

            var lat_arrival = $('#lat-arrival-input').val();
            var lng_arrival = $('#lng-arrival-input').val();
            var destination = new google.maps.LatLng(lat_arrival, lng_arrival);

            var service = new google.maps.DistanceMatrixService();

            service.getDistanceMatrix(
                {
                    origins: [origin],
                    destinations: [destination],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    durationInTraffic: true,
                    avoidHighways: true,
                    avoidTolls: true

                }, callback);

            function callback(response, status) {

                if (status == google.maps.DistanceMatrixStatus.OK) {
                    var origins = response.originAddresses;
                    var destinations = response.destinationAddresses;

                    for (var i = 0; i < origins.length; i++) {
                        var results = response.rows[i].elements;
                        console.log(results);
                        for (var j = 0; j < results.length; j++) {
                            var element = results[j];
                            var distance = element.distance.text;
                            var duration = element.duration.value;
                            var duration_text = element.duration.text;

                            $('.duration-geaocode').html(duration);
                            $('.distance-geaocode').html(distance);

                            $('#input-duration').val(duration);
                            $('#input-duration-text').val(duration_text);
                            $('#input-distance').val(distance);
                        }
                    }
                }
            }
        })
        return false;
    });

    //Get the latitude and the longitude for auto departure location;
    var cabgeolocalisation = {
        successFunction: function (position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            cabgeolocalisation.codeLatLng(lat, lng)
        },
        errorFunction: function () {
            alert("Geocoder failed");
        },
        codeLatLng: function (lat, lng) {
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //get the lat and the lng
                    var latAuto = results[0].geometry.location.lat();
                    var lngAuto = results[0].geometry.location.lng();

                    $('#lat-input-auto').val(latAuto);
                    $('#lng-input-auto').val(lngAuto);
                    if (results[1]) {
                        //formatted address
                        $('#current-localisation-input').val(results[0].formatted_address);
                        //find country name
                        for (var i=0; i<results[0].address_components.length; i++) {
                            for (var b=0;b<results[0].address_components[i].types.length;b++) {
                                //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                    //this is the object you are looking for
                                    var city= results[0].address_components[i];
                                    break;
                                }
                            }
                        }

                    } else {
                        alert("No results found");
                    }
                } else {
                    alert("Geocoder failed due to: " + status);
                }
            });
        }
    }

    var allgeolocalisation = {
        init: function () {
            var locale = $('#locale-input').val();
            //autogeolocalisation
            $('.departure-icon').click(function(){
                var current_geolocation = $('#current-localisation-input').val();

                if (current_geolocation != ''){
                    $('.departure-input-group #input-pickup').val(current_geolocation);
                    var autolat = $('#lat-input-auto').val();
                    var autolng = $('#lng-input-auto').val();
                    $('#lat-input').val(autolat);
                    $('#lng-input').val(autolng);
                }else{
                    alert("Geocoder failed");
                }
            });

            //departure autocomplete maps
            $("input#input-pickup").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat = result.geometry.location.lat();
                    var long = result.geometry.location.lng();
                    $('#lat-input').val(lat);
                    $('#lng-input').val(long);

                    // fill fields of back
                    var address_go = $("input#input-pickup").val();
                    $('#input-dropoff-ret').val(address_go);
                    $('#lat-back-arrival-input').val(lat);
                    $('#lng-back-arrival-input').val(long);

                    var origin = new google.maps.LatLng(lat, long);
                    if($('#lat-arrival-input').val() && $('#lng-arrival-input').val()){
                        var lat_arrival = $('#lat-arrival-input').val();
                        var lng_arrival = $('#lng-arrival-input').val();
                        var destination = new google.maps.LatLng(lat_arrival, lng_arrival);

                        var service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix(
                        {
                            origins: [origin],
                            destinations: [destination],
                            travelMode: google.maps.TravelMode.DRIVING,
                            unitSystem: google.maps.UnitSystem.METRIC,
                            durationInTraffic: true,
                            avoidHighways: true,
                            avoidTolls: true

                        }, callback);

                    }
            });

            // Arrival autocomplete maps
            $("input#input-dropoff").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrival = result.geometry.location.lat();
                    var long_arrival = result.geometry.location.lng();
                    $('#lat-arrival-input').val(lat_arrival);
                    $('#lng-arrival-input').val(long_arrival);

                    // fill all fields for back
                    var address_back = $("#input-dropoff").val();
                    $('#input-pickup-ret').val(address_back);
                    $('#lat-back-dep-input').val(lat_arrival);
                    $('#lng-back-dep-input').val(long_arrival);

                    var destination = new google.maps.LatLng(lat_arrival, long_arrival);
                    if($('#lat-input').val() && $('#lng-input').val()){
                        var lat_dep = $('#lat-input').val();
                        var lng_dep = $('#lng-input').val();
                        var origin = new google.maps.LatLng(lat_dep, lng_dep);

                        var service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix(
                            {
                                origins: [origin],
                                destinations: [destination],
                                travelMode: google.maps.TravelMode.DRIVING,
                                unitSystem: google.maps.UnitSystem.METRIC,
                                durationInTraffic: true,
                                avoidHighways: true,
                                avoidTolls: true

                            }, callback);

                    }
            });

            // back departure autocomplete maps
            $("input#input-pickup-ret").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrival = result.geometry.location.lat();
                    var long_arrival = result.geometry.location.lng();
                    $('#lat-back-dep-input').val(lat_arrival);
                    $('#lng-back-dep-input').val(long_arrival);

                    var destination = new google.maps.LatLng(lat_arrival, long_arrival);
                    if($('#lat-back-arrival-input').val() && $('#lng-back-arrival-input').val()){
                        var lat_dep = $('#lat-back-arrival-input').val();
                        var lng_dep = $('#lng-back-arrival-input').val();
                        var origin = new google.maps.LatLng(lat_dep, lng_dep);

                        var service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix({
                                origins: [origin],
                                destinations: [destination],
                                travelMode: google.maps.TravelMode.DRIVING,
                                unitSystem: google.maps.UnitSystem.METRIC,
                                durationInTraffic: true,
                                avoidHighways: true,
                                avoidTolls: true

                            }, callbackBack);

                    }
            });
            
            
            $("input#input-pickup-ret").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrival = result.geometry.location.lat();
                    var long_arrival = result.geometry.location.lng();
                    $('#lat-back-dep-input').val(lat_arrival);
                    $('#lng-back-dep-input').val(long_arrival);

                    var destination = new google.maps.LatLng(lat_arrival, long_arrival);
                    if($('#lat-back-arrival-input').val() && $('#lng-back-arrival-input').val()){
                        var lat_dep = $('#lat-back-arrival-input').val();
                        var lng_dep = $('#lng-back-arrival-input').val();
                        var origin = new google.maps.LatLng(lat_dep, lng_dep);

                        var service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix({
                                origins: [origin],
                                destinations: [destination],
                                travelMode: google.maps.TravelMode.DRIVING,
                                unitSystem: google.maps.UnitSystem.METRIC,
                                durationInTraffic: true,
                                avoidHighways: true,
                                avoidTolls: true

                            }, callbackBack);

                    }
            });

            $("#cab_coursebundle_course_startedAddress").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrival = result.geometry.location.lat();
                    var long_arrival = result.geometry.location.lng();
                    $('#cab_coursebundle_course_latDep').val(lat_arrival);
                    $('#cab_coursebundle_course_longDep').val(long_arrival);

                    var destination = new google.maps.LatLng(lat_arrival, long_arrival);
                    if($('#cab_coursebundle_course_latDepBack').val() && $('#cab_coursebundle_course_longDepBack').val()){
                        var lat_dep = $('#cab_coursebundle_course_latDepBack').val();
                        var lng_dep = $('#cab_coursebundle_course_longDepBack').val();
                        var origin = new google.maps.LatLng(lat_dep, lng_dep);

                        var service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix({
                                origins: [origin],
                                destinations: [destination],
                                travelMode: google.maps.TravelMode.DRIVING,
                                unitSystem: google.maps.UnitSystem.METRIC,
                                durationInTraffic: true,
                                avoidHighways: true,
                                avoidTolls: true

                            }, callbackBack);

                    }
            });
            
            $("#cab_coursebundle_course_arrivalAddress").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrival = result.geometry.location.lat();
                    var long_arrival = result.geometry.location.lng();
                    $('#cab_coursebundle_course_latArr').val(lat_arrival);
                    $('#cab_coursebundle_course_longArr').val(long_arrival);

                    var destination = new google.maps.LatLng(lat_arrival, long_arrival);
                    if($('#cab_coursebundle_course_latDepBack').val() && $('#cab_coursebundle_course_longDepBack').val()){
                        var lat_dep = $('#cab_coursebundle_course_latDepBack').val();
                        var lng_dep = $('#cab_coursebundle_course_longDepBack').val();
                        var origin = new google.maps.LatLng(lat_dep, lng_dep);

                        var service = new google.maps.DistanceMatrixService();

                        service.getDistanceMatrix({
                                origins: [origin],
                                destinations: [destination],
                                travelMode: google.maps.TravelMode.DRIVING,
                                unitSystem: google.maps.UnitSystem.METRIC,
                                durationInTraffic: true,
                                avoidHighways: true,
                                avoidTolls: true

                            }, callbackBack);

                    }
            });
            
            
            $("#cab_coursebundle_course_backAddressDeparture").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_departback = result.geometry.location.lat();
                    var long_departback = result.geometry.location.lng();
                    $('#cab_coursebundle_course_latDepBack').val(lat_departback);
                    $('#cab_coursebundle_course_longDepBack').val(long_departback);

                    var destination = new google.maps.LatLng(lat_departback, long_departback);
            });
            
            $("#cab_coursebundle_course_backAddressArrival").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrivalback = result.geometry.location.lat();
                    var long_arrivalback = result.geometry.location.lng();
                    $('#cab_coursebundle_course_latArrBack').val(lat_arrivalback);
                    $('#cab_coursebundle_course_longArrBack').val(long_arrivalback);

                    var destination = new google.maps.LatLng(lat_arrivalback, long_arrivalback);
            });
            
             $("#cab_main_partner_address").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrivalback = result.geometry.location.lat();
                    var long_arrivalback = result.geometry.location.lng();
                    var destination = new google.maps.LatLng(lat_arrivalback, long_arrivalback);
            });
            
            $("#cab_user_registration_address").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var lat_arrivalback = result.geometry.location.lat();
                    var long_arrivalback = result.geometry.location.lng();
                    $('#cab_coursebundle_course_latArrBack').val(lat_arrivalback);
                    $('#cab_coursebundle_course_longArrBack').val(long_arrivalback);

                    var destination = new google.maps.LatLng(lat_arrivalback, long_arrivalback);
            });
            
            function callback(response, status) {

                if (status == google.maps.DistanceMatrixStatus.OK) {
                    var origins = response.originAddresses;
                    var destinations = response.destinationAddresses;

                    for (var i = 0; i < origins.length; i++) {
                        var results = response.rows[i].elements;
                        for (var j = 0; j < results.length; j++) {
                            var element = results[j];

                            var distance = element.distance.text;
                            var distanceValue = Math.floor(element.distance.value / 1000);
                            var duration = element.duration.value;
                            var duration_text = element.duration.text;

                            $('.duration-geaocode').html(duration);
                            $('.distance-geaocode').html(distance);

                            $('#input-duration').val(duration);
                            $('#input-duration-text').val(duration_text);
                            $('#input-distance').val(distanceValue);

                            //fill fields of back
                            $('#input-duration-back').val(duration);
                            $('#input-duration-back-text').val(duration_text);
                            $('#input-distance-back').val(distanceValue);

                        }
                    }
                }
            }

            function callbackBack(response, status) {

                if (status == google.maps.DistanceMatrixStatus.OK) {
                    var origins = response.originAddresses;
                    var destinations = response.destinationAddresses;

                    for (var i = 0; i < origins.length; i++) {
                        var results = response.rows[i].elements;
                        for (var j = 0; j < results.length; j++) {
                            var element = results[j];

                            var distance = element.distance.text;
                            var distanceValue = Math.floor(element.distance.value / 1000);
                            var duration = element.duration.value;
                            var duration_text = element.duration.text;

                            $('#input-duration-back').val(duration);
                            $('#input-duration-back-text').val(duration_text);
                            $('#input-distance-back').val(distanceValue);
                        }
                    }
                }
            }
        }

    }

})(jQuery);
