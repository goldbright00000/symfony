<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 06/07/2015
 * Time: 01:01
 */

namespace CAB\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HomeController extends Controller
{

    /*
     * show the home page
     */
    public function indexAction(Request $request)
    {

	    $repository = $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media');

        $media = $repository->findBy(array('context' => 'slider'));

        if (!$widget = $this->get('cab.main.widget_manager')->getLastWidgetByRegion("bottom_content")) {
            throw new NotFoundHttpException($this->get('translator')->trans('No widget available.'));
        }

        $response = new Response();
        if (count($media)) {
            $response->setEtag(md5($media[0]->getUpdatedAt()->format('Y-m-d H:i:s')));
            $response->setLastModified($media[0]->getUpdatedAt());
        }

        $response->setCache(['max_age' => 86400]);
        $response->setPublic(); // make sure the response is public/cacheable

        // Check that the Response is not modified for the given Request
        if ($response->isNotModified($request)) {
            // return the 304 Response immediately
            return $response;
        }

        $slider2 = $repository->findBy(array('context' => 'slider2'));
        $slider3 = $repository->findBy(array('context' => 'slider3'));
        $icons = $repository->findBy(array('context' => 'icons'));

        return $this->render('CABMainBundle:Home:index.html.twig', array(
            'widget_bottom_content' => $widget,
            'media' => $media,
            'slider2' => $slider2,
            'slider3' => $slider3,
            'icons' => $icons
            ), $response
        );
    }
}