<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 06/07/2015
 * Time: 01:01
 */

namespace CAB\MainBundle\Controller;

use CAB\MainBundle\Event\StoreObjectEvent;
use CAB\MainBundle\Events\StoreObjectEvents;
use CAB\MainBundle\Imap\ImapParser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CAB\MainBundle\Entity\Partner;
use CAB\MainBundle\Entity\Compagny;
use CAB\MainBundle\Form\PartnerType;
use CAB\MainBundle\Entity\Contact;
use CAB\MainBundle\Form\ContactType;
use CAB\UserBundle\Form\Type\ProfileFormType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class CommonController extends Controller {


	public function TestParseAction()
	{
		$imap = $this->getParameter('imap');
		$access = $this->getParameter('access');
		$webDir = $this->get('kernel')->getRootDir() . '/../web';
		$courseManager = $this->get('cab.course_manager');
		$commandSncfManager = $this->get('cab_course.manager.commandSncf_manager');
		$parseImap = new ImapParser($courseManager, $commandSncfManager, $imap, $access, '10 Jan 2018', $webDir);
		$result = $parseImap->handle();
		dump($result); exit;
	}
    public function SliderAction() {
        $repository = $this->getDoctrine()
                ->getRepository('ApplicationSonataMediaBundle:Media');
        $media = $repository->findBy(array('context' => 'slider'));
        return $this->render('CABMainBundle:Common:slider.html.twig', array('media' => $media));
    }

    public function PartnerAction(Request $request) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $title = "Welcome partner";
        $oPartner = new Partner();
        $partnerFormType = new PartnerType();
        $form = $this->createForm(PartnerType::class, $oPartner);
        $form->handleRequest($request);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $request->getSession()->getFlashBag()->add('success', 'Thank you, Your request is sended!');
            //save the partner object
            $partnerManager = $this->get('cab.main.partner_manager');
            $partnerManager->savePartner($oPartner);

            $mailer = $this->container->get('mailer');
            $message = (new \Swift_Message('demande site partner'))

                    ->setFrom('no-reply@up.taxi')
                    ->setTo('support@up.taxi')
                    ->setBody(
                    $this->renderView('CABMainBundle:Mail:partnerEmail.html.twig', array('object' => $data, 'user'=>$user)), 'text/html'
            );
            $ret = $mailer->send($message);
            return $this->redirect($this->generateUrl('partner'));
        }

        return $this->render('CABMainBundle:Common:partner.html.twig', array('title' => $title,
                    "form" => $form->createView()
        ));
    }

    public function WhatAction() {
        $title = "How it works ?";
        return $this->render('CABMainBundle:Common:what.html.twig', array('title' => $title));
    }

    public function FaqAction(Request $request) {
        $title = "The faq";
        $language = $request->getLocale();
        $em = $this->getDoctrine();
        $faq = $em->getRepository('CABArticleBundle:Faq')->findBy(array('language' => $language,'cible'=>'1')); #cible 1 = customer
        return $this->render('CABMainBundle:Common:support.html.twig', array('title' => $title, 'faq' => $faq));
    }

    public function FaqdriverAction(Request $request) {
        $title = "The faq driver";
        $language = $request->getLocale();
        $em = $this->getDoctrine();
        $faq = $em->getRepository('CABArticleBundle:Faq')->findBy(array('language' => $language,'cible'=>'2')); #cible 2 = driver
        return $this->render('CABMainBundle:Common:supportdriver.html.twig', array('title' => $title, 'faq' => $faq));
    }

    public function RgpdAction(Request $request) {
        $title = "Vos données personnelles";
        $language = $request->getLocale();
        $em = $this->getDoctrine();
        $article = $em->getRepository('CABArticleBundle:Article')->findOneById('9');
        return $this->render('CABMainBundle:Common:rgpd.html.twig', array('title' => $title, 'article'=>$article));
    }



    public function ClickandupAction(Request $request) {
        $title = "ClickAndUp";
        $language = $request->getLocale();
        $em = $this->getDoctrine();
        return $this->render('CABMainBundle:Common:clickandup.html.twig', array('title' => $title));
    }

    public function TaxiaumarocAction(Request $request) {
        $title = "Taxi au Maroc";
        $language = $request->getLocale();
        $em = $this->getDoctrine();
        $repository = $this->getDoctrine()->getRepository('ApplicationSonataMediaBundle:Media');
        $media = $repository->findBy(array('context' => 'slider'));

        $price1 = $em->getRepository('CABArticleBundle:Price')->findBy(array('language' => $language,'cible'=>'2','position'=>'1'), array('departure' => 'ASC')); #cible 1 = maroc
        $price2 = $em->getRepository('CABArticleBundle:Price')->findBy(array('language' => $language,'cible'=>'2','position'=>'2'), array('departure' => 'ASC')); #cible 1 = maroc
        $price3 = $em->getRepository('CABArticleBundle:Price')->findBy(array('language' => $language,'cible'=>'2','position'=>'3'), array('departure' => 'ASC')); #cible 1 = maroc

        return $this->render('CABMainBundle:Common:taxiaumaroc.html.twig', array('title' => $title,'price1'=>$price1,'price2'=>$price2,'price3'=>$price3,'media' => $media));
    }

    public function TaxienfranceAction(Request $request) {
        $title = "Taxi en France";
        $language = $request->getLocale();

        return $this->render('CABMainBundle:Common:taxienfrance.html.twig', array('title' => $title));
    }

    public function TermsAction() {
        $em = $this->getDoctrine();

        $article = $em->getRepository('CABArticleBundle:Article')->findOneById('1');
        return $this->render('CABMainBundle:Common:terms.html.twig', array('article' => $article));
    }

    public function MentionAction() {
        $em = $this->getDoctrine();
        $article = $em->getRepository('CABArticleBundle:Article')->findOneById('2');
        return $this->render('CABMainBundle:Common:mention.html.twig', array('article' => $article));
    }

    public function ForpartnersAction(Request $request) {
        $em = $this->getDoctrine();
        $title = "Welcome partner";
        $oPartner = new Partner();
        $partnerFormType = new PartnerType();
        $form = $this->createForm($partnerFormType, $oPartner);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $request->getSession()->getFlashBag()->add('success', 'Thank you, Your request is sended!');
            //save the partner object
            $partnerManager = $this->get('cab.main.partner_manager');
            $partnerManager->savePartner($oPartner);

            $message = new \Swift_Message($data->getSubject());
	        $message->setContentType("text/html")
                    //->setSubject($data->getSubject())
                    ->addReplyTo($data->getEmail())
                    ->setFrom($data->getEmail())
                    ->setTo('contact@up.taxi')
                    ->setBody($this->renderView('CABMainBundle:Common:partnerEmail.html.twig', array('object' => $data)));

            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('partner'));
        }

        return $this->render('CABMainBundle:Common:forpartners.html.twig', array('title' => $title,
                    "form" => $form->createView()
        ));
    }

    public function ProfileuserAction(Request $request) {

        $id = $this->getUser()->getId();

        $em = $this->getDoctrine()->getManager();


        $listcourse = $em->getRepository('CABCourseBundle:Course')->findBy(array('client' => $id), array('departureTime' => 'asc'));

        $cost = $em->getRepository('CABCourseBundle:Course')->findAllOrderedByName();


        $user = $this->getUser();

        $formFactory = $this->get('fos_user.profile.form.factory');

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        return $this->render('CABMainBundle:Common:profileuser.html.twig', array('form' => $form->createView(), 'user' => $user, 'course' => $listcourse, 'client' => $cost));
    }

    public function ContactAction(Request $request) {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $request->getSession()->getFlashBag()->add('success', 'Thank you, Your request is sended!');
            //save the contact object
            $contactManager = $this->get('cab.main.contact_manager');
            $contactManager->saveContact($contact);
            $dispatcher = $this->get('event_dispatcher');
            $event = new StoreObjectEvent($contact, $contactManager);
            $dispatcher->dispatch(StoreObjectEvents::STORE_OBJECT, $event);

            return $this->redirect($this->generateUrl('contact'));
        }

        return $this->render('CABMainBundle:Common:contact.html.twig', array(
                    'title' => 'Contact',
                    "form" => $form->createView())
        );
    }

    public function ToulouseAndorreAction() {
        $language = $this->get('request')->getLocale();
        $ref = 'AND';
        $em = $this->getDoctrine();
        $result = $em->getRepository('CABArticleBundle:Price')->findBy(array('language' => $language,'ref'=>$ref)); #cible 4
        return $this->render('CABMainBundle:Price:toulouse.html.twig', array('result'=>$result));
    }

    public function ToulouseNarbonneAction() {
        $language = $this->get('request')->getLocale();
        $ref = '11';
        $em = $this->getDoctrine();
        $result = $em->getRepository('CABArticleBundle:Price')->findBy(array('language' => $language,'ref'=>$ref)); #cible 4
        return $this->render('CABMainBundle:Price:toulouse.html.twig', array('result'=>$result));
    }

}
