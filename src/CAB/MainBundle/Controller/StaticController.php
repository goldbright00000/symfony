<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 06/07/2015
 * Time: 01:01
 */

namespace CAB\MainBundle\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class staticController extends Controller
{
    public function privacyAction()
    {
        return $this->render('CABMainBundle:Static:privacy.html.twig');
    }
}