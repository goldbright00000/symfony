<?php

/**
 * Created by PhpStorm.
 * Project: cab2018
 * User: m.benhenda
 * Date: 09/01/2018
 * Time: 17:25
 */

namespace CAB\MainBundle\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use CAB\MainBundle\Command\ParseMailboxCommand;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Bundle\FrameworkBundle\Client;

abstract class UpCommandTestCase extends WebTestCase
{
	/**
	 * Runs a command and returns it output
	 */
	public function runCommand(Client $client, $command)
	{
		$application = new Application($client->getKernel());
		$application->setAutoExit(false);

		$fp = tmpfile();
		$input = new StringInput($command);
		$output = new StreamOutput($fp);

		$application->run($input, $output);

		fseek($fp, 0);
		$output = '';
		while (!feof($fp)) {
			$output = fread($fp, 4096);
		}
		fclose($fp);

		return $output;
	}
}