<?php

namespace CAB\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use CAB\CourseBundle\Entity\TypeVehicule;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use CAB\UserBundle\Form\TelType;

class PartnerType extends AbstractType {

   public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('partnerName', TextType::class, array(
          'attr' => array(
             'placeholder' => 'Firstname and Lastname',
              'translation_domain' => 'MainBundle',
              'maxlength' => 90,
        ),
          'label' => 'Firsname and Lastname',
          'trim' => true
        ))
        ->add('email', EmailType::class, array(
        	'attr' => array(
	            'maxlength' => 64,
        ),
           'label' => 'Email',
           'required' => true,
           'trim' => true
        ))
        ->add('company', TextType::class, array(
            'attr' => array(
                'placeholder' => 'Compagny name',
            ),
            'label' => 'Company',
            'required' => true,
            'trim' => true
        ))
        ->add('phone', TelType::class)
        ->add('address', TextType::class, array('required' => true
        ))

        ->add('subject', ChoiceType::class, array(
            'choices'   => array(
            'New Partner' => 'New Partner',
            'Partner Actived' => 'Partner Actived',
            'Partner Blocked'=>'Partner Blocked',
            'Contact' => 'Contact'),
            'placeholder' => 'Choose the subject',
            'required' => true,
            'translation_domain' => 'frontend'
        ))
        ->add('bodyMessage', TextareaType::class, array(
            'required' => true
        ))
        ->add('submit', SubmitType::class, array(
            'attr' => array('class' => 'btn medium green right' ))
        );

   }

   public function getName() {
        return "cab_main_partner";
   }
	public function getBlockPrefix() {

		return "cab_main_partner";
	}
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CAB\MainBundle\Entity\Partner'
        ));
    }
}