<?php
namespace CAB\MainBundle\Form;


use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

class EnquiryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('email', 'email');
        $builder->add('subject');
        $builder->add('body', 'textarea');
    }
public function getName()
    {
        return 'contact';
    }

	public function getBlockPrefix() {

		return "contact";
	}
}
