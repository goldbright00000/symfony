<?php

namespace CAB\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $driversChoice = $options['data']['driver_choice'];
        $raceChoice = $options['data']['course_choice'];

        $builder
            ->add('driver','choice', array('choices' => $driversChoice,  'empty_data'  => null,
                'required' => false, 'attr' => array('class' => 'export-select')))
            ->add('dateIn', 'date', array(
                'widget'=> 'single_text',
                'format'=>'M/d/y',

                'required' => false,
//                'attr' => array(
//                    'class' => 'f$options['course_path']orm-control input-inline export-select',
//                    'data-provide' => 'datepicker',
//                    'data-date-format' => 'm/d/yyyy'
//                ),
            ))
            ->add('dateOut', 'date', array(
                'widget'=> 'single_text',
                'format'=>'M/d/y',

                'required' => false,
//                'attr' => array(
//                    'class' => 'form-control input-inline export-select',
//                    'data-provide' => 'datepicker',
//                    'data-date-format' => 'm/d/yyyy'
//                ),
            ))
            ->add('race','choice', array('choices' => $raceChoice, 'empty_data'  => null,
                'required' => false, 'attr' => array('class' => 'export-select')))
            ->add('submit','submit')
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $view->vars = array_merge($view->vars, array(
            'course_path' => $options['data']['course_path']
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'driver_choice' => array(),
            'course_choice' => array(),
            'course_path' => null,
        ));
    }

    public function getName()
    {
        return 'export_report';
    }
	public function getBlockPrefix() {

		return "export_report";
	}
}