<?php
// CAB/MainBundle/Form/ContactType.php
namespace CAB\MainBundle\Form;

use CAB\UserBundle\Form\TelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Validator\Constraints\MaxLength;



class ContactType extends AbstractType {

   public function buildForm(FormBuilderInterface $builder, array $options) {
       $builder->add('contactName', TextType::class, array(
          'attr' => array(
              'maxlength' => 100,
          ),
           'required' => true,
           'trim' => true,
           'error_bubbling' => false)
       )
       ->add('email', EmailType::class, array(
           'required' => true,
           'label' => 'Email',
           'trim' => true )
       )
       ->add('address', TextType::class, array(
           'attr' => array(
           'class'=>'maps_input'),
           'required' => true,
           'label' => 'Zip code and City',
           'trim' => true)
       )
       ->add('phone', TelType::class, array(
           'required' => true,
           'label' => 'Phone',
           'trim' => true)
       )
       ->add('company', TextType::class, array(
           'required' => true)
       )
       ->add('subject', ChoiceType::class, array(
           'choices'   => array('Partner' => 'Partner',
               'Communication' => 'Communication',
               'Contact' => 'Contact',
               'Appointement' => 'Appointement',
               'Other' => 'Other'),
	           'preferred_choices' => array('Contact'),

           'placeholder' => 'Choose the subject',
           'translation_domain' => 'frontend',
           'required' => true
       ))
       ->add('bodyMessage', TextareaType::class, array(
           'required' => true)
       )
        ->add('submit', SubmitType::class, array(
            'attr' => array('class' => 'btn medium green right' ))
        );
   }

    public function getBlockPrefix() {

      return "cab_main_contact";
    }

	public function getName() {

		return "cab_main_contact";
	}

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new NotBlank());
        $metadata->addPropertyConstraint('email', new Email());
        $metadata->addPropertyConstraint('subject', new NotBlank());
        $metadata->addPropertyConstraint('subject', new MaxLength(50));
        $metadata->addPropertyConstraint('message', new MinLength(50));
    }
}


