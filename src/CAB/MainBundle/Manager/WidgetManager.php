<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 14/07/2015
 * Time: 01:35
 */

namespace CAB\MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use  CAB\MainBundle\Manager\BaseManager;
use CAB\MainBundle\Entity\Widget;

class WidgetManager extends BaseManager {

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function loadWidget($widgetID) {
        return $this->getRepository()
            ->findOneBy(array('id' => $widgetID));
    }

    public function getLastWidgetByRegion($region) {
        return $this->getRepository()
            ->findOneBy(array('region' => $region));
    }

    public function isEnabled(Widget $widget)
    {
        return $widget->getEnabled() ?
            true:
            false;
    }

    public function getRepository()
    {
        return $this->em->getRepository('CABMainBundle:Widget');
    }
}