<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 14/07/2015
 * Time: 01:35
 */

namespace CAB\MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\MainBundle\Manager\BaseManager;
use CAB\MainBundle\Entity\Contact;

class ContactManager extends BaseManager {

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function loadContact($contactID) {
        return $this->getRepository()
            ->findOneBy(array('id' => $contactID));
    }

    /**
     * Save Contact entity
     *
     * @param Contact $contact
     */
    public function saveContact(Contact $contact)
    {
        $this->persistAndFlush($contact);
    }

    public function getRepository()
    {
        return $this->em->getRepository('CABMainBundle:Contact');
    }


}