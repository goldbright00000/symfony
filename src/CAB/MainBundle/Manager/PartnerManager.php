<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 14/07/2015
 * Time: 01:35
 */

namespace CAB\MainBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\MainBundle\Manager\BaseManager;
use CAB\MainBundle\Entity\Partner;

class PartnerManager extends BaseManager {

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function loadPartner($partnerID) {
        return $this->getRepository()
            ->findOneBy(array('id' => $partnerID));
    }

    public function getStatus(Partner $partner)
    {
        return $partner->getStatusPartner();
    }

    /**
     * Save Partner entity
     *
     * @param Partner $partner
     */
    public function savePartner(Partner $partner)
    {
        $this->persistAndFlush($partner);
    }

    public function getRepository()
    {
        return $this->em->getRepository('CABMainBundle:Partner');
    }


}