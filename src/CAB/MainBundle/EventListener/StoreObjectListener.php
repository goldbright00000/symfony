<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:24
 */

namespace CAB\MainBundle\EventListener;

use CAB\MainBundle\Event\StoreObjectEvent;
use Symfony\Component\Templating\EngineInterface;

class StoreObjectListener {

    private $template;
    private $mailer;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating){
        $this->template = $templating;
        $this->mailer = $mailer;
    }
    public function onStoreObject(StoreObjectEvent $event)
    {
        $object = $event->getObject();


        $emailPartner = $object->getEmail();
        $message = (new \Swift_Message($object->getSubject()))
            ->setContentType("text/html")

            ->addReplyTo($object->getEmail())
            ->setFrom($object->getEmail())
            ->setTo('contact@up.fr')
            ->setBody($this->template->render('CABMainBundle:Common:view_email.html.twig',
                array('object' => $object))   );

        $this->mailer->send($message);
    }
}