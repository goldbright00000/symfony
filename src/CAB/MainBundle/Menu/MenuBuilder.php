<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 06/07/2015
 * Time: 02:28eeze
 */

namespace CAB\MainBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;



/**
 * Class MenuBuilder
 *
 * @package CAB\MainBundle\Menu
 */
class MenuBuilder {

    private $factory;
    private $securityContext;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory) {
        $this->factory = $factory;

    }

    /**
     * @param RequestStack    $requestStack
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $authorizationCheckerSecurity
     * @return \Knp\Menu\ItemInterface
     */
    public function createMainMenu(RequestStack $requestStack,TokenStorageInterface $tokenStorage,AuthorizationCheckerInterface $authorizationCheckerSecurity ) {

        $menu = $this->factory->createItem(
                'root', array(
            'childrenAttributes' => array(
                'class' => 'nav navbar-nav navbar-right',
            ),
                )
        );

        $user = $tokenStorage->getToken()->getUser();
        //echo "<pre>";print_r($tokenStorage);die;
        $loggedIn = $authorizationCheckerSecurity->isGranted('IS_AUTHENTICATED_FULLY');

        $menu->addChild('Become partner', array('route' => 'partner'))->setExtra('translation_domain', 'frontend');
        $menu->addChild('How it works ?', array('route' => 'what'))->setExtra('translation_domain', 'frontend');
        $menu->addChild('Entreprise', array('route' => 'clickandup'))->setExtra('translation_domain', 'frontend');
        if ($loggedIn) {
            $menu->addChild('user', array('route' => 'homepage', 'label' => $user))->setExtra('translation_domain', 'frontend')->setAttribute('icon', 'fa fa-list');
            $menu['user']->addChild('My profile', array('route' => 'cab_user_profile_show'))->setExtra('translation_domain', 'frontend');
            $menu['user']->addChild('Log out', array('route' => 'fos_user_security_logout'))->setExtra('translation_domain', 'frontend');
        } else {
            $menu->addChild('Login', array('route' => 'login'))->setExtra('translation_domain', 'frontend');
        }
        return $menu;
    }

    /**
     * @param RequestStack    $requestStack
     * @param SecurityContext $securityContext
     *
     * @return \Knp\Menu\ItemInterface
     */
    public function createFooterMenu(RequestStack $requestStack, TokenStorageInterface $tokenStorage,AuthorizationCheckerInterface $authorizationCheckerSecurity) {
        $menu = $this->factory->createItem(
                'root', array(
            'childrenAttributes' => array(
                'class' => 'nav navbar-nav',
            ),
                )
        );
        $menu->addChild('Contact us', array('route' => 'contact'))->setExtra('translation_domain', 'frontend');
        $menu->addChild('Who are we ?', array('route' => 'mention'))->setExtra('translation_domain', 'frontend');
        $menu->addChild('C.G.V', array('route' => 'terms'))->setExtra('translation_domain', 'frontend');
        $menu->addChild('Support and FAQ', array('route' => 'faq'))->setExtra('translation_domain', 'frontend');
        $menu->addChild('RGPD', array('route' => 'rgpd'))->setExtra('translation_domain', 'frontend');

        // $menu->addChild('Partenaire', array('route' => 'partner'));
        return $menu;
    }

}
