<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:11
 */

namespace CAB\MainBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use CAB\MainBundle\Entity\CabEntityInterface;

class StoreObjectEvent extends Event
{
    protected $object;
    protected $rm;

    public function __construct(CabEntityInterface $object, $repositoryManager)
    {
        $this->object = $object;
        $this->rm = $repositoryManager;
    }

    public function getObject()
    {
        return $this->object;
    }
}