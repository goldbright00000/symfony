<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\MainBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class WidgetAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    /**
     * @param string                            $code
     * @param string                            $class
     * @param string                            $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage Widgets. perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia
        alia esse nobis vacua ab omni periculo atque etiam suspicione hoc tempore in provinciis periculo atque eti';
    }
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text', array('label' => 'Title'))
            ->add('region', 'text', array('label' => 'Region'))
            ->add('hasSlider', 'checkbox', array('label' => 'Has slider', 'required' => false))
            ->add('enabled', 'checkbox', array('label' => 'Enabled', 'required' => false))
            ->add('body', 'ckeditor');
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('region')
            ->add('hasSlider')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('region')
            ->add('body')
            ->add('hasSlider')
            ->add('createdAt')
            ->add('updatedAt');
        ;
    }
}