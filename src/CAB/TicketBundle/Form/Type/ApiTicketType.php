<?php

namespace CAB\TicketBundle\Form\Type;

use CAB\CourseBundle\Form\DataTransformer\CourseToIntTransformer;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\CourseRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Hackzilla\Bundle\TicketBundle\Manager\UserManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Hackzilla\Bundle\TicketBundle\Form\Type\TicketMessageType;
use CAB\TicketBundle\Entity\Ticket;

class ApiTicketType extends AbstractType
{
    private $userManager;
    private $idUser;
    private $em;
    private $course;

    public function __construct(UserManagerInterface $userManager, $idUser, ObjectManager $em, Course $course)
    {
        $this->userManager = $userManager;
        $this->idUser = $idUser;
        $this->em = $em;
        $this->course = $course;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var UserManagerInterface $userManager */
        $userManager = $this->userManager;
        $idUser = $this->idUser;
        $builder->add('course', 'hidden');
        $builder->get('course')->addModelTransformer(new CourseToIntTransformer($this->em, $this->course));
        $builder->add(
                'subject',
                method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'Symfony\Component\Form\Extension\Core\Type\ChoiceType' : 'choice',
                array(
                    'choices' => Ticket::getValueSubject(),
                    'choice_translation_domain' => true,
                    'required' => true,
                    'label' => 'LABEL_SUBJECT',
                )
            )->add(
                'user',
                'hidden',
                array(
                    'required' => true,
                    'mapped' => false,
                )
            )
            ->add(
                'messages',
                method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'Symfony\Component\Form\Extension\Core\Type\CollectionType' : 'collection',
                array(
                    method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'entry_type' : 'type' => method_exists(
                        'Symfony\Component\Form\AbstractType',
                        'getBlockPrefix'
                    ) ? 'Hackzilla\Bundle\TicketBundle\Form\Type\TicketMessageType' : new TicketMessageType($this->userManager),
                    method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'entry_options' : 'options' => array(
                        'new_ticket' => true,
                    ),
                    'label' => false,
                    'allow_add' => true,
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\TicketBundle\Entity\Ticket',
                'userManager' => null,
                'idUser' => null,
            )
        );
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'ticket';
    }
}
