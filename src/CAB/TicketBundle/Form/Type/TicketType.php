<?php

namespace CAB\TicketBundle\Form\Type;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\CourseRepository;
use Hackzilla\Bundle\TicketBundle\Manager\UserManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Hackzilla\Bundle\TicketBundle\Form\Type\TicketMessageType;
use CAB\TicketBundle\Entity\Ticket;

class TicketType extends AbstractType
{
    private $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var UserManagerInterface $currentUser */
        $userManager = $this->userManager;

        if (!$builder->getData()->getCourse() instanceof Course) {
            $builder
                ->add(
                    'course',
                    'entity',
                    array(
                        'class' => 'CAB\CourseBundle\Entity\Course',
                        'query_builder' => function (CourseRepository $cr) use ($userManager) {
                            return $cr->getCourseNullTicket($userManager);
                        },
                    )
                );
        }
        $builder->add(
                'subject',
                method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'Symfony\Component\Form\Extension\Core\Type\ChoiceType' : 'choice',
                array(
                    'choices' => array_flip(Ticket::getValueSubjectCustomer()),
                    'choice_translation_domain' => true,
                    'required' => true,
                    'label' => 'LABEL_SUBJECT',
                )
            )
            ->add('ContactName')
            ->add('EmailContactName')
            ->add(
                'messages',
                method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'Symfony\Component\Form\Extension\Core\Type\CollectionType' : 'collection',
                array(
                    method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'entry_type' : 'type' => method_exists(
                        'Symfony\Component\Form\AbstractType',
                        'getBlockPrefix'
                    ) ? 'Hackzilla\Bundle\TicketBundle\Form\Type\TicketMessageType' : new TicketMessageType($this->userManager),
                    method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix') ? 'entry_options' : 'options' => array(
                        'new_ticket' => true,
                    ),
                    'label' => false,
                    'allow_add' => true,
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\TicketBundle\Entity\Ticket',
                'userManager' => null,
            )
        );
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'ticket';
    }
}
