<?php

namespace CAB\TicketBundle\Controller;

use CAB\CourseBundle\Entity\Course;
use CAB\TicketBundle\Entity\Ticket;
use Hackzilla\Bundle\TicketBundle\Entity\TicketMessage;
use Hackzilla\Bundle\TicketBundle\Event\TicketEvent;
use Hackzilla\Bundle\TicketBundle\Form\Type\TicketMessageType;
use CAB\TicketBundle\Form\Type\TicketType;
use Hackzilla\Bundle\TicketBundle\TicketEvents;
use Hackzilla\Bundle\TicketBundle\TicketRole;
use Symfony\Component\HttpFoundation\Request;
use Hackzilla\Bundle\TicketBundle\Controller\TicketController as HackzillaTicketController;
use Hackzilla\Bundle\TicketBundle\Model\TicketInterface;
use Hackzilla\Bundle\TicketBundle\Model\TicketMessageInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Ticket controller.
 */
class TicketController extends HackzillaTicketController
{
    /**
     * Lists all Ticket entities.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $existTicket = false;
        $userManager = $this->get('hackzilla_ticket.user_manager');
        $translator = $this->get('translator');

        $ticketState = $request->get('state', $translator->trans('STATUS_OPEN'));
        $ticketPriority = $request->get('priority', null);

        $repositoryTicket = $this->getDoctrine()->getRepository('CABTicketBundle:Ticket');

        $repositoryTicketMessage = $this->getDoctrine()->getRepository('CABTicketBundle:TicketMessage');

        $query = $repositoryTicket->getTicketList(
            $userManager,
            $repositoryTicketMessage->getTicketStatus($translator, $ticketState),
            $repositoryTicketMessage->getTicketPriority($translator, $ticketPriority)
        );

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query->getQuery(),
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:index.html.twig',
                [
                    'pagination'     => $pagination,
                    'ticketState'    => $ticketState,
                    'ticketPriority' => $ticketPriority,
                    'exist_ticket' => $existTicket,
                ]
            );
        }

        return $this->render(
            'CABTicketBundle:Ticket:index_admin.html.twig',
            [
                'pagination'     => $pagination,
                'ticketState'    => $ticketState,
                'ticketPriority' => $ticketPriority,
                'exist_ticket' => $existTicket,
            ]
        );
    }

    /**
     * Lists Ticket entities by course.
     *
     * @param Request $request
     *
     * @param Course $courseId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listByCourseAction(Request $request, Course $courseId)
    {
        $existTicket = false;
        if ($courseId->getTicket()) {
            $existTicket = true;
        }
        $userManager = $this->get('hackzilla_ticket.user_manager');
        $translator = $this->get('translator');

        $ticketState = $request->get('state', $translator->trans('STATUS_OPEN'));
        $ticketPriority = $request->get('priority', null);

        $repositoryTicket = $this->getDoctrine()->getRepository('CABTicketBundle:Ticket');

        $repositoryTicketMessage = $this->getDoctrine()->getRepository('CABTicketBundle:TicketMessage');

        $query = $repositoryTicket->getTicketsByCourse($courseId->getId());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query->getQuery(),
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:index.html.twig',
                [
                    'pagination'     => $pagination,
                    'ticketState'    => $ticketState,
                    'ticketPriority' => $ticketPriority,
                    'exist_ticket' => $existTicket,
                ]
            );
        }

        return $this->render(
            'CABTicketBundle:Ticket:index_admin.html.twig',
            [
                'pagination'     => $pagination,
                'ticketState'    => $ticketState,
                'ticketPriority' => $ticketPriority,
                'exist_ticket' => $existTicket,
            ]
        );
    }

    /**
     * Creates a new Ticket entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $userManager = $this->get('hackzilla_ticket.user_manager');
        $ticketManager = $this->get('hackzilla_ticket.ticket_manager');

        $ticket = $ticketManager->createTicket();
        $form = $this->createForm(TicketType::class, $ticket, ['userManager' => $userManager]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = $ticket->getMessages()->current();
            $message->setStatus(TicketMessage::STATUS_OPEN)
                ->setUser($userManager->getCurrentUser())
                ->setTicket($ticket);

            $ticketManager->updateTicket($ticket, $message);

            $event = new TicketEvent($ticket);
            $this->get('event_dispatcher')->dispatch(TicketEvents::TICKET_CREATE, $event);

            return $this->redirect($this->generateUrl('hackzilla_ticket_show', ['ticketId' => $ticket->getId()]));
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:new.html.twig',
                [
                    'entity' => $ticket,
                    'form'   => $form->createView(),
                ]
            );
        } else {
            return $this->render(
                'CABTicketBundle:Ticket:new_admin.html.twig',
                [
                    'entity' => $ticket,
                    'form'   => $form->createView(),
                ]
            );
        }
    }


    /**
     * Creates a new Ticket entity.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createForSpecificCourseAction(Request $request, Course $course)
    {
        $userManager = $this->get('hackzilla_ticket.user_manager');
        $ticketManager = $this->get('hackzilla_ticket.ticket_manager');


        $ticket = $ticketManager->createTicket();
        $ticket->setCourse($course);
        $form = $this->createForm(TicketType::class, $ticket, ['userManager' => $userManager]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = $ticket->getMessages()->current();
            $message->setStatus(TicketMessage::STATUS_OPEN)
                ->setUser($userManager->getCurrentUser())
                ->setTicket($ticket);

            $ticketManager->updateTicket($ticket, $message);

            $event = new TicketEvent($ticket);
            $this->get('event_dispatcher')->dispatch(TicketEvents::TICKET_CREATE, $event);

            $ticketId = $event->getTicket()->getId();

            $courseDate = $ticket->getCourse()->getDepartureDate();
            $courseTime = $ticket->getCourse()->getDepartureTime();

            if(is_null($ticket->getemailContactName())){
                $ticketemailbcc = 'aol@aol.com';
            }else{
                $ticketemailbcc = $ticket->getemailContactName(); #contact Mail
            }


            $ticketemailTo = $ticket->getCourse()->getdriver()->getemail();

            $sendemail = (new \Swift_Message('Création ticket '. $ticketId))
                ->setFrom('helpdesk@up.taxi')
                ->setTo('helpdesk@up.taxi') #driver
                //->setBcc($ticketemailbcc) # cobntact sncf
                ->setBcc([$ticketemailbcc,$ticketemailTo])
                ->setBody(
                    $this->renderView('CABTicketBundle:Mail:status_create_ticket.html.twig',array('ticket' => $ticket, 'message'=>$message,'courseDate'=>$courseDate,'courseTime'=>$courseTime)),
                    'text/html'
                );
            $this->get('mailer')->send($sendemail);

            return $this->redirect($this->generateUrl('hackzilla_ticket_show', ['ticketId' => $ticket->getId()]));
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:new.html.twig',
                [
                    'entity' => $ticket,
                    'form'   => $form->createView(),
                ]
            );
        } else {
            return $this->render(
                'CABTicketBundle:Ticket:new_admin.html.twig',
                [
                    'entity' => $ticket,
                    'form'   => $form->createView(),
                ]
            );
        }


    }

    /**
     * Displays a form to create a new Ticket entity.
     */
    public function newAction()
    {
        $entity = new Ticket();
        $userManager = $this->get('hackzilla_ticket.user_manager');
        //$ticketType = new TicketType($userManager);

        $form = $this->createForm(TicketType::class, $entity, ['userManager' => $userManager]);

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:new.html.twig',
                [
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ]
            );
        } else {
            return $this->render(
                'CABTicketBundle:Ticket:new_admin.html.twig',
                [
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ]
            );
        }
    }

    /**
     * Displays a form to create a new Ticket entity.
     */
    public function newForSpecificCourseAction(Course $course)
    {
        $entity = new Ticket();
        $entity->setCourse($course);
        $em = $this->getDoctrine()->getManager();
        $userManager = $this->get('hackzilla_ticket.user_manager');
        //$ticketType = new TicketType($userManager);

        $form = $this->createForm(TicketType::class, $entity, ['userManager' => $userManager]);

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:new.html.twig',
                [
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ]
            );
        } else {
            return $this->render(
                'CABTicketBundle:Ticket:new_admin_specific_course.html.twig',
                [
                    'entity' => $entity,
                    'course' => $course->getId(),
                    'form'   => $form->createView(),
                ]
            );
        }
    }

    /**
     * Finds and displays a TicketInterface entity.
     *
     * @param int $ticketId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($ticketId)
    {
        $ticketManager = $this->get('hackzilla_ticket.ticket_manager');
        $ticket = $ticketManager->getTicketById($ticketId);

        if (!$ticket) {
            return $this->redirect($this->generateUrl('hackzilla_ticket'));
        }

        $currentUser = $this->getUserManager()->getCurrentUser();
        $this->getUserManager()->hasPermission($currentUser, $ticket);

        $data = ['ticket' => $ticket];

        $message = $ticketManager->createMessage($ticket);

        if (TicketMessageInterface::STATUS_CLOSED != $ticket->getStatus()) {
            $data['form'] = $this->createMessageForm($message)->createView();
        }

        if ($currentUser && $this->getUserManager()->hasRole($currentUser, TicketRole::ADMIN)) {
            $data['delete_form'] = $this->createDeleteForm($ticket->getId())->createView();
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:show.html.twig',
                [
                    'ticket' => $data['ticket'],
                ]
            );
        } else {
            return $this->render(
                'CABTicketBundle:Ticket:show_admin.html.twig',
                [
                    'ticket' => $data['ticket'],
                ]
            );
        }

        return $this->render($this->container->getParameter('hackzilla_ticket.templates')['show'], $data);
    }

    /**
     * @param \Hackzilla\Bundle\TicketBundle\Model\UserInterface|string $user
     * @param Ticket                                                    $ticket
     */
    private function checkUserPermission($user, Ticket $ticket)
    {
        if (!\is_object($user) || (!$this->get('hackzilla_ticket.user_manager')->hasRole(
                    $user,
                    TicketRole::ADMIN
                ) && $ticket->getUserCreated() != $user->getId())
        ) {
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(403);
        }
    }

    /**
     * Finds and displays a TicketInterface entity.
     *
     * @param Request $request
     * @param int $ticketId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function replyAction(Request $request, $ticketId)
    {

        $ticketManager = $this->get('hackzilla_ticket.ticket_manager');
        $ticket = $ticketManager->getTicketById($ticketId);

        if(is_null($ticket->getemailContactName())){
            $ticketemailbcc = 'aol@aol.com';
        }else{
            $ticketemailbcc = $ticket->getemailContactName(); #contact Mail
        }

        $ticketemailTo = $ticket->getCourse()->getdriver()->getemail(); #driver

        if (!$ticket) {
            throw $this->createNotFoundException($this->get('translator')->trans('ERROR_FIND_TICKET_ENTITY'));
        }

        $user = $this->getUserManager()->getCurrentUser();
        $this->getUserManager()->hasPermission($user, $ticket);

        $message = $ticketManager->createMessage($ticket);

        if(is_null($ticket->getCourse()->getCommandSncf())){
            $coursebc = null;
        }else{
            $coursebc = $ticket->getCourse()->getCommandSncf()->getCommandeCourse();
        }

        $courseDate = $ticket->getCourse()->getDepartureDate();
        $courseTime = $ticket->getCourse()->getDepartureTime();

        $form = $this->createMessageForm($message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setUser($user);
            $ticketManager->updateTicket($ticket, $message);
            $this->dispatchTicketEvent(TicketEvents::TICKET_UPDATE, $ticket);

            $sendemail = (new \Swift_Message('Suivi ticket'. $ticketId))
                ->setFrom('helpdesk@up.taxi')
                ->setTo('helpdesk@up.taxi')
                ->setBcc([$ticketemailbcc,$ticketemailTo])
                ->setBody(
                    $this->renderView('CABTicketBundle:Mail:status_reply_ticket.html.twig',array('ticket' => $ticket, 'message'=>$message, 'user'=>$user,'coursebc'=>$coursebc,'courseDate'=>$courseDate,'courseTime'=>$courseTime)),
                    'text/html'
                );
            $this->get('mailer')->send($sendemail);

            return $this->redirect($this->generateUrl('hackzilla_ticket_show', ['ticketId' => $ticket->getId()]));
        }

        $data = ['ticket' => $ticket, 'form' => $form->createView()];

        if ($user && $this->get('hackzilla_ticket.user_manager')->hasRole($user, TicketRole::ADMIN)) {
            $data['delete_form'] = $this->createDeleteForm($ticket->getId())->createView();
        }

        if ($user->hasRole('ROLE_CUSTOMER')) {
            return $this->render(
                'CABTicketBundle:Ticket:show.html.twig',
                [
                    'ticket' => $data['ticket'],
                    'form' => $data['form']
                ]
            );
        } else {
            return $this->render(
                'CABTicketBundle:Ticket:show_admin.html.twig',
                [
                    'ticket' => $data['ticket'],
                    'form' => $data['form'],
                    'delete_form' => $data['delete_form'],
                ]
            );
        }
        //return $this->render($this->container->getParameter('hackzilla_ticket.templates')['show'], $data);
    }

    /**
     * Deletes a Ticket entity.
     *
     * @param Request $request
     * @param int $ticketId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $ticketId)
    {
        $userManager = $this->getUserManager();
        $user = $userManager->getCurrentUser();

        if (!\is_object($user) || !$userManager->hasRole($user, TicketRole::ADMIN)) {
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(403);
        }

        $form = $this->createDeleteForm($ticketId);

        if ($request->isMethod('DELETE')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                $ticketManager = $this->get('hackzilla_ticket.ticket_manager');
                $ticket = $ticketManager->getTicketById($ticketId);

                if (!$ticket) {
                    throw $this->createNotFoundException($this->get('translator')->trans('ERROR_FIND_TICKET_ENTITY'));
                }

                $ticketManager->deleteTicket($ticket);
                $this->dispatchTicketEvent(TicketEvents::TICKET_DELETE, $ticket);
            }
        }

        return $this->redirect($this->generateUrl('hackzilla_ticket'));
    }

    private function dispatchTicketEvent($ticketEvent, TicketInterface $ticket)
    {
        $event = new TicketEvent($ticket);
        $this->get('event_dispatcher')->dispatch($ticketEvent, $event);
    }

    /**
     * Creates a form to delete a Ticket entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', HiddenType::class)
            ->getForm();
    }

    /**
     * @param TicketMessageInterface $message
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createMessageForm(TicketMessageInterface $message)
    {
        $form = $this->createForm(
            TicketMessageType::class,
            $message,
            ['new_ticket' => false]
        );

        return $form;
    }

    /**
     * @return \Hackzilla\Bundle\TicketBundle\Manager\UserManagerInterface
     */
    private function getUserManager()
    {

        $userManager = $this->get('hackzilla_ticket.user_manager');

        return $userManager;
    }
}
