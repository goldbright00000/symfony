<?php

/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 04/03/2017
 * Time: 11:20
 */

namespace CAB\TicketBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use Hackzilla\Bundle\TicketBundle\Model\TicketInterface;
use Hackzilla\Bundle\TicketBundle\Model\TicketMessageInterface;

/**
 * Ticket.
 *
 * @ORM\Table(name="cab_ticket")
 * @ORM\Entity(repositoryClass="CAB\TicketBundle\Entity\TicketRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Ticket implements TicketInterface
{
    const OBJECT_MISSED_OBJECT = 'OBJECT_MISSED_OBJECT';
    const OBJECT_BEHAVIOUR_DRIVER = 'OBJECT_BEHAVIOUR_DRIVER';
    const OBJECT_DELAY_COURSE = 'OBJECT_DELAY_COURSE';
    const OBJET_PARCOURS_RACE = 'OBJET_PARCOURS_RACE';
    const OBJET_DANGEREUSE_RACE = 'OBJET_DANGEREUSE_RACE';
    const OBJET_ACCIDENT = 'OBJET_ACCIDENT';
    const OBJECT_BEHAVIOUR_CUSTOMER = 'OBJECT_BEHAVIOUR_CUSTOMER';
    const OBJECT_CAR_PANNE = 'OBJECT_CAR_PANNE';
    const OBJECT_OTHER = 'OBJECT_OTHER';
    const OBJECT_PLANIFICATION_SNCF = 'OBJECT_PLANIFICATION_SNCF';
    const OBJECT_PLANIFICATION_TAXI = 'OBJECT_PLANIFICATION_TAXI';
    const OBJECT_RACE_NOT_EXECUTE = 'OBJECT_RACE_NOT_EXECUTE';


    public $valueSubjectcustomer = array(
        0 => self::OBJECT_MISSED_OBJECT,
        1 => self::OBJECT_MISSED_OBJECT,
        2 => self::OBJECT_BEHAVIOUR_DRIVER,
        3 => self::OBJECT_DELAY_COURSE,
        4 => self::OBJET_PARCOURS_RACE,
        5 => self::OBJET_DANGEREUSE_RACE,
        6 => self::OBJET_ACCIDENT,
        7 => self::OBJECT_BEHAVIOUR_CUSTOMER,
        8 => self::OBJECT_CAR_PANNE,
        9 => self::OBJECT_OTHER,
    );

    public $valueSubjectdriver = array(
        0 => self::OBJECT_MISSED_OBJECT,
        2 => self::OBJECT_BEHAVIOUR_DRIVER,
        3 => self::OBJECT_DELAY_COURSE,
        4 => self::OBJET_PARCOURS_RACE,
        5 => self::OBJET_DANGEREUSE_RACE,
        6 => self::OBJET_ACCIDENT,
        7 => self::OBJECT_BEHAVIOUR_CUSTOMER,
        8 => self::OBJECT_CAR_PANNE,
        9 => self::OBJECT_OTHER,
    );

    public $valueSubject = array(
        0 => self::OBJECT_MISSED_OBJECT,
        1 => self::OBJECT_MISSED_OBJECT,
        2 => self::OBJECT_BEHAVIOUR_DRIVER,
        3 => self::OBJECT_DELAY_COURSE,
        4 => self::OBJET_PARCOURS_RACE,
        5 => self::OBJET_DANGEREUSE_RACE,
        6 => self::OBJET_ACCIDENT,
        7 => self::OBJECT_BEHAVIOUR_CUSTOMER,
        8 => self::OBJECT_CAR_PANNE,
        9 => self::OBJECT_PLANIFICATION_SNCF,
        10 => self::OBJECT_PLANIFICATION_TAXI,
        11 => self::OBJECT_RACE_NOT_EXECUTE,
        12 => self::OBJECT_OTHER,
    );

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_created_id", type="integer")
     */
    protected $userCreated;
    protected $userCreatedObject;

    /**
     * @var int
     *
     * @ORM\Column(name="last_user_id", type="integer")
     */
    protected $lastUser;
    protected $lastUserObject;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_message", type="datetime")
     */
    protected $lastMessage;

    /**
     * @ORM\Column(name="subject", type="string", length=255)
     * @Assert\NotBlank()
     */
    protected $subject;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    protected $status;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="smallint")
     */
    protected $priority;

    /**
     * @ORM\OneToMany(targetEntity="TicketMessage",  mappedBy="ticket")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $messages;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Course", inversedBy="ticket", cascade={"remove", "persist"})
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public", "Course"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $course;


    /**
     * @var string $contactName
     * @ORM\Column(type="string", name="contact_name", nullable=true, unique=false)
     * @Groups({"Public"})
     * @Expose
     *
     */
    protected $contactName;

    /**
     * @var string $emailContactName
     * @ORM\Column(type="string", name="email_contact_name", nullable=true, unique=false)
     * @Groups({"Public"})
     * @Expose
     */
    protected $emailContactName;


    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->messages = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status.
     *
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Set status string.
     *
     * @param string $status
     */
    public function setStatusString($status)
    {
        $status = \array_search(\strtolower($status), TicketMessage::$statuses);

        if ($status > 0) {
            $this->setStatus($status);
        }
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status string.
     *
     * @return string
     */
    public function getStatusString()
    {
        if (isset(TicketMessage::$statuses[$this->status])) {
            return TicketMessage::$statuses[$this->status];
        }

        return TicketMessage::$statuses[0];
    }

    /**
     * Set priority.
     *
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * Set priority string.
     *
     * @param string $priority
     */
    public function setPriorityString($priority)
    {
        $priority = \array_search(\strtolower($priority), TicketMessage::$priorities);

        if ($priority > 0) {
            $this->setPriority($priority);
        }
    }

    /**
     * Get priority.
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Get priority string.
     *
     * @return string
     */
    public function getPriorityString()
    {
        if (isset(TicketMessage::$priorities[$this->priority])) {
            return TicketMessage::$priorities[$this->priority];
        }

        return TicketMessage::$priorities[0];
    }

    /**
     * Set userCreated.
     *
     * @param int|object $userCreated
     *
     * @return Ticket
     */
    public function setUserCreated($userCreated)
    {
        if (\is_object($userCreated)) {
            $this->userCreatedObject = $userCreated;
            $this->userCreated = $userCreated->getId();
        } else {
            $this->userCreatedObject = null;
            $this->userCreated = $userCreated;
        }

        return $this;
    }

    /**
     * Get userCreated.
     *
     * @return int
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Get userCreated object.
     *
     * @return object
     */
    public function getUserCreatedObject()
    {
        return $this->userCreatedObject;
    }

    /**
     * Set lastUser.
     *
     * @param int|object $lastUser
     *
     * @return Ticket
     */
    public function setLastUser($lastUser)
    {
        if (\is_object($lastUser)) {
            $this->lastUserObject = $lastUser;
            $this->lastUser = $lastUser->getId();
        } else {
            $this->lastUserObject = null;
            $this->lastUser = $lastUser;
        }

        return $this;
    }

    /**
     * Get lastUser.
     *
     * @return int
     */
    public function getLastUser()
    {
        return $this->lastUser;
    }

    /**
     * Get lastUser object.
     *
     * @return object
     */
    public function getLastUserObject()
    {
        return $this->lastUserObject;
    }

    /**
     * Set lastMessage.
     *
     * @param \DateTime $lastMessage
     *
     * @return Ticket
     */
    public function setLastMessage($lastMessage)
    {
        $this->lastMessage = $lastMessage;

        return $this;
    }

    /**
     * Get lastMessage.
     *
     * @return \DateTime
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Ticket
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set subject.
     *
     * @param string $subject
     *
     * @return Ticket
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Add messages.
     *
     * @param TicketMessageInterface $messages
     *
     * @return Ticket
     */
    public function addMessage(TicketMessageInterface $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages.
     *
     * @param TicketMessageInterface $messages
     */
    public function removeMessage(TicketMessageInterface $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return Ticket
     */
    public function setCourse(\CAB\CourseBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    public function getValueSubject()
    {
        return array(
            1 => self::OBJECT_MISSED_OBJECT,
            2 => self::OBJECT_BEHAVIOUR_DRIVER,
            3 => self::OBJECT_DELAY_COURSE,
            4 => self::OBJET_PARCOURS_RACE,
            5 => self::OBJET_DANGEREUSE_RACE,
            6 => self::OBJET_ACCIDENT,
            7 => self::OBJECT_BEHAVIOUR_CUSTOMER,
            8 => self::OBJECT_CAR_PANNE,
            9 => self::OBJECT_PLANIFICATION_SNCF,
            10 => self::OBJECT_PLANIFICATION_TASM,
            11 => self::OBJECT_RACE_NOT_EXECUTE,
            12 => self::OBJECT_OTHER,
        );

    }

    public function getValueSubjectCustomer()
    {
        return array(
            1 => self::OBJECT_MISSED_OBJECT,
            2 => self::OBJECT_BEHAVIOUR_DRIVER,
            3 => self::OBJECT_DELAY_COURSE,
            4 => self::OBJET_PARCOURS_RACE,
            5 => self::OBJET_DANGEREUSE_RACE,
            6 => self::OBJET_ACCIDENT,
            7 => self::OBJECT_BEHAVIOUR_CUSTOMER,
            8 => self::OBJECT_CAR_PANNE,
            9 => self::OBJECT_PLANIFICATION_SNCF,
            10 => self::OBJECT_PLANIFICATION_TAXI,
            11 => self::OBJECT_RACE_NOT_EXECUTE,
            12 => self::OBJECT_OTHER,
        );

    }

    public function getSubjectString()
    {
        $subjectStr = str_replace('_', ' ', str_replace('OBJECT_', '', $this->valueSubject[$this->subject]));
        return $subjectStr;
    }

    /**
     * Set contactName
     *
     * @param string $contactName
     * @return Ticket
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * Get contactName
     *
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Set emailContactName
     *
     * @param string $emailContactName
     * @return Ticket
     */
    public function setEmailContactName($emailContactName)
    {
        $this->emailContactName = $emailContactName;

        return $this;
    }

    /**
     * Get emailContactName
     *
     * @return string
     */
    public function getEmailContactName()
    {
        return $this->emailContactName;
    }
}
