<?php

namespace CAB\TicketBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
class CABTicketBundle extends Bundle
{
    /**
     * Use Bundle Inheritance to Override Parts of a HackzillaTicketBundle
     *
     * @return string
     */
	public function build(ContainerBuilder $container)
	{
		parent::build($container);
	}
    /*public function getParent()
    {
        return 'HackzillaTicketBundle';
    }*/
}
