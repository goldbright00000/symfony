<?php

namespace CAB\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;

Class RatingHandler extends BaseHandler implements RatingHandlerInterface
{
    protected $om;
    private $repository;

    public function __construct(ObjectManager $om, $entityClass)
    {
        $this->om               = $om;
        $this->entityClass      = $entityClass;
        $this->ratingRepository = $this->om->getRepository($this->entityClass);
        $this->reopenEntityManager();
    }

    /**
     * Persist a new rating.
     *
     * @param array $params  variables to be persisted by handler
     *
     * @return boolean
     */
    public function postDriver(array $params = array())
    {
        if (null === $params || !isset($params['driver_id']) || !isset($params['client_id']) || !isset($params['course_id']))
            return false;

        $driver_id = (int) trim($params['driver_id']);
        $client_id = (int) trim($params['client_id']);
        $course_id = (int) trim($params['course_id']);
    
        $getDriver = $this->getSpecificRepo('CABUserBundle:User')->find($driver_id);
        $getClient = $this->getSpecificRepo('CABUserBundle:User')->find($client_id);
        $getCourse = $this->getSpecificRepo('CABCourseBundle:Course')->find($course_id);

        if (!$getDriver || !$getClient || !$getCourse)
            return false;

        $params['driver_id'] = $getDriver;
        $params['client_id'] = $getClient;
        $params['course_id'] = $getCourse;
        $this->persistNewRating($params);

        return true;        
    }

    /**
     * Persist a new rating.
     *
     * @param array $params  variables to be persisted by handler
     *
     * @return boolean
     */
    public function postClient(array $params = array())
    {
        if (null === $params || !isset($params['driver_id']) || !isset($params['client_id']) || !isset($params['course_id']))
            return false;

        $driver_id = (int) trim($params['driver_id']);
        $client_id = (int) trim($params['client_id']);
        $course_id = (int) trim($params['course_id']);
    
        $getDriver = $this->getSpecificRepo('CABUserBundle:User')->find($driver_id);
        $getClient = $this->getSpecificRepo('CABUserBundle:User')->find($client_id);
        $getCourse = $this->getSpecificRepo('CABCourseBundle:Course')->find($course_id);

        if (!$getDriver || !$getClient || !$getCourse)
            return false;

        $params['driver_id'] = $getDriver;
        $params['client_id'] = $getClient;
        $params['course_id'] = $getCourse;
        $this->persistNewRating($params, 2);

        return true;        
    }

    public function persistNewRating(array $data = array(), $voter = 1)
    {
        $rating = $this->getRatingEntity();

        if (isset($data['rate_ponctualite'])) {
            $ratePonctualite = (int) trim($data['rate_ponctualite']);
            $rating->setRatePonctualite($ratePonctualite);
        }

        if (isset($data['rate_confort'])) {
            $rateConfort = (int) trim($data['rate_confort']);
            $rating->setRateConfort($rateConfort);
        }

        if (isset($data['rate_conduite'])) {
            $rateConduite = (int) trim($data['rate_conduite']);
            $rating->setRateConduite($rateConduite);
        }

        if (isset($data['rate_courtoisie'])) {
            $rateCourtoisie = (int) trim($data['rate_courtoisie']);
            $rating->setRateCourtoisie($rateCourtoisie);
        }
        
        if (isset($data['rate_general'])) {
            $rateGeneral = (int) trim($data['rate_general']);
            $rating->setRateGeneral($rateGeneral);
        }

        if (isset($data['driver_id'])) {
            $rating->setDriver($data['driver_id']);
        }

        if (isset($data['client_id'])) {
            $rating->setClient($data['client_id']);
        }

        if (isset($data['course_id'])) {
            $rating->setCourse($data['course_id']);
        }

        if (isset($data['comment'])) {
            $rating->setComment(trim($data['comment']));
        }

        $rating->setVoter($voter);
        
        $this->om->persist($rating);
        $this->om->flush();
    }

    public function getSpecificRepo($repoName)
    {
        return $this->om->getRepository($repoName);
    }

    public function getRatingEntity()
    {
        return new $this->entityClass;
    }

    public function getDriver($driver_id) {}
    public function putDriver(array $params = array()) {}

    /**
     * Get average ratings of the given driver in whole time
     */
    public function getAverageDriverRatingsInAllPeriods($driver_id)
    {
        if (!$driver_id)
            return false;

        $getRepo = $this->ratingRepository->getAverageDriverRatingsInAllPeriods($driver_id);
        $getAverageRating = $this->calculateRatingAverage($getRepo);

        if (false === $getAverageRating || !count($getAverageRating))
            $getAverageRating = 5;

        return array('average_rating' => $getAverageRating);
    }

    /**
     * Get average ratings of the given driver in the current week
     */
    public function getAverageDriverRatingsInWorkingWeek($driver_id, $returnType = 'array')
    {
        if (!$driver_id)
            return false;

        $getRepo = $this->ratingRepository->getAverageDriverRatingsInWorkingWeek($driver_id);
        $getAverageRating = $this->calculateRatingAverage($getRepo);

        if (false === $getAverageRating || !count($getAverageRating))
            $getAverageRating = 5;

        if ($returnType == 'array') {
            return array('average_rating' => $getAverageRating);
        }

        return $getAverageRating;
    }

    /**
     * Get average ratings of the given customer in whole time
     */
    public function getAverageCustomerRatingsInAllPeriods($customer_id)
    {
        if (!$customer_id)
            return false;

        $getRepo = $this->ratingRepository->getAverageCustomerRatingsInAllPeriods($customer_id);
        $getAverageRating = $this->calculateRatingAverage($getRepo);

        if (false === $getAverageRating || !count($getAverageRating))
            $getAverageRating = 5;

        return array('average_rating' => $getAverageRating);
    }

    /**
     * Get average ratings of the given customer in the current week
     */
    public function getAverageCustomerRatingsInWorkingWeek($customer_id)
    {
        if (!$customer_id)
            return false;

        $getRepo = $this->ratingRepository->getAverageCustomerRatingsInWorkingWeek($customer_id);
        $getAverageRating = $this->calculateRatingAverage($getRepo);

        if (false === $getAverageRating || !count($getAverageRating))
            $getAverageRating = 5;

        return array('average_rating' => $getAverageRating);
    }

    public function calculateRatingAverage(array $params = array())
    {
        if (count($params)) {
            $setSingleAverageRating = array();

            foreach ($params as $key => $param) {
                $setSingleAverageRating[] = $this->getRatingTotal(array(
                    $param->getRatePonctualite(),
                    $param->getRateGeneral(),
                    $param->getRateCourtoisie(),
                    $param->getRateConduite(),
                    $param->getRateConfort()
                ), true);
            }

            if (count($setSingleAverageRating)) {
                return $this->getRatingTotal($setSingleAverageRating, true);
            }
        }

        return false;
    }

    public function getRatingTotal(array $rates = array(), $convertRates = false)
    {
        if (count($rates)) {
            $getTotalRates  = 0;
            $counter        = 0;

            foreach ($rates as $key => $rate) {
                $getTotalRates += $rate;
                $counter++;
            }

            if (false !== $convertRates && $counter > 0) {
                return round($getTotalRates/$counter, 2);
            }

            return round($getTotalRates, 2);
        }

        return false;
    }
}