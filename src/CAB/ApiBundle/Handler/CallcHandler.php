<?php

namespace CAB\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use CAB\CallcBundle\Entity\Callc;

Class CallcHandler extends BaseHandler implements CallcHandlerInterface 
{

    protected $om;
    private $entityClass;
    private $repository;
    

    public function __construct(ObjectManager $om, $entityClass)
    {
        $this->om               = $om;
        $this->entityClass      = $entityClass;
        $this->callcRepository = $this->om->getRepository($this->entityClass);
        $this->reopenEntityManager();
    }

    /**
     * Persist a new callc
     *
     * @param array $params  variables to be persisted by handler
     *
     * @return boolean
     */
    public function postCallc(array $params = array())
    {

        $course_id = $params['course_id'];
        $agentId = $params['agentId'];
        $callDuration =  $params['callDuration'];
        $datePickUp = $params['datePickUp'];
        $callSubject = $params['callSubject'];
        $cidname = $params['cidname'];
        $ciddnid = $params['ciddnid'];
        $callevent_time = $params['callevent_time'];
        $infocallentreprise = $params['infocallentreprise'];
        $infocallnom = $params['infocallnom'];
        $infocallprenom = $params['infocallprenom'];
        $infocalltelephone = $params['infocalltelephone'];
        $infocallcomment = $params['infocallcomment'];

        $getagentId = $this->getSpecificRepo('CABUserBundle:User')->find($agentId);

        if (isset($course_id)){
            $getCourse = $this->getSpecificRepo('CABCourseBundle:Course')->find($course_id);
            $params['course_id'] = $getCourse;
        }else{
            $params['course_id'] = null;
        }


        $params['agentId'] = $getagentId;


        $this->persistNewCallc($params);

        return true;        
    }


    public function persistNewCallc(array $data = array())
    {
        $createdAt = new \DateTime('now');
        $updatedAt = new \DateTime('now');
        $callevent_time = new \DateTime('now');


        $callc = $this->getCallcEntity();
        if (isset($data['course_id'])) {
            $course_id = $data['course_id'];
            $callc->setCourse($course_id);
            $datePickUp = $course_id->getDepartureDate();
            $callc->setdatePickUp($datePickUp);
        }else{
            $course_id = $data['course_id'];
            $callc->setCourse($course_id);
            $datePickUp = new \DateTime('now');
            $callc->setdatePickUp($datePickUp);
        }
        if (isset($data['agentId'])) {
            $agentId = $data['agentId'];
            $callc->setagentId($agentId);
        }
        if (isset($data['callDuration'])) {
            $callDuration = $data['callDuration'];
            $callc->setcallDuration($callDuration);
        }
        if (isset($data['callSubject'])) {
            $callSubject = $data['callSubject'];
            $callc->setcallSubject($callSubject);
        }
        if (isset($data['cidname'])) {
            $cidname = $data['cidname'];
            $callc->setcidname($cidname);
        }
        if (isset($data['ciddnid'])) {
            $ciddnid = $data['ciddnid'];
            $callc->setciddnid($ciddnid);
        }
        if (isset($data['infocallentreprise'])) {
            $infocallentreprise = $data['infocallentreprise'];
            $callc->setinfocallentreprise($infocallentreprise);
        }
        if (isset($data['infocallnom'])) {
            $infocallnom = $data['infocallnom'];
            $callc->setinfocallnom($infocallnom);
        }
        if (isset($data['infocallprenom'])) {
            $infocallprenom = $data['infocallprenom'];
            $callc->setinfocallprenom($infocallprenom);
        }
        if (isset($data['infocalltelephone'])) {
            $infocalltelephone = $data['infocalltelephone'];
            $callc->setinfocalltelephone($infocalltelephone);
        }
        if (isset($data['infocallcomment'])) {
            $infocallcomment = $data['infocallcomment'];
            $callc->setinfocallcomment($infocallcomment);
        }

       // if (isset($data['callevent_time'])) {
       //     $callevent_time = $data['callevent_time'];
        //    $callc->setcalleventtime($callevent_time);
        //}

        $callc->setcalleventtime($callevent_time);
        $callc->setCreatedAt($createdAt);
        $callc->setUpdatedAt($updatedAt);
        
        $this->om->persist($callc);
        $this->om->flush();
    }

    public function getSpecificRepo($repoName)
    {
        return $this->om->getRepository($repoName);
    }

    public function getCallcEntity()
    {
        return new $this->entityClass;
    }

}