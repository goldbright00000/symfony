<?php

namespace CAB\ApiBundle\Handler;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

interface UserHandlerInterface
{
    /**
     * Post User, creates a new User.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return UserInterface
     */
    public function post(array $parameters);

    /**
     * Edit a user.
     *
     * @api
     *
     * @param UserInterface   $user
     * @param array           $parameters
     *
     * @return UserInterface
     */
    public function put(array $parameters);
}