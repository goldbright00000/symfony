<?php

namespace CAB\ApiBundle\Handler;

use CAB\ApiBundle\Exception\InvalidFormException;
use CAB\CourseBundle\Entity\CourseRepository;
use CAB\TicketBundle\Entity\Ticket;
use CAB\TicketBundle\Entity\TicketMessage;
use CAB\TicketBundle\Form\Type\ApiTicketType;
use CAB\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Hackzilla\Bundle\TicketBundle\Manager\UserManagerInterface;
use Hackzilla\Bundle\TicketBundle\Model\TicketMessageInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Class TicketHandler
 *
 * @package CAB\ApiBundle\Handler
 */
Class TicketHandler extends BaseHandler
{
    /**
     * @var ObjectManager
     */
    protected $om;
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @param ObjectManager $om
     * @param UserManagerInterface $userManager
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(
        ObjectManager $om,
        UserManagerInterface $userManager,
        FormFactoryInterface $formFactory
    )
    {
        $this->om = $om;
        $this->repository = $this->om->getRepository('CABTicketBundle:Ticket');
        $this->formFactory = $formFactory;
        $this->userManager = $userManager;
    }

    /**
     * Create a new ticket.
     *
     * @param array $parameters
     *
     * @return Ticket
     */
    public function post(array $parameters)
    {
        try {
            $ticket = new Ticket();
            $messageTicket = new TicketMessage();
            $idUser = $parameters['user'];
            $courseId = $parameters['course'];
            $subject = $parameters['subject'];
            $message = $parameters['message'];
            $course = $this->om->getRepository('CABCourseBundle:Course')->find($courseId);
            $ticket->setUserCreated($idUser);
            $ticket->setLastUser($idUser);
            $ticket->setCourse($course);
            $ticket->setLastMessage(new \DateTime('now'));
            $ticket->setStatus(TicketMessage::STATUS_OPEN);
            $ticket->setPriority(TicketMessage::PRIORITY_LOW);
            $ticket->setSubject($subject);


            $messageTicket->setUser($idUser);
            $messageTicket->setMessage($message);
            $messageTicket->setStatus(TicketMessage::STATUS_OPEN);
            $messageTicket->setPriority(TicketMessage::PRIORITY_LOW);
            $this->om->persist($messageTicket);

            $ticket->addMessage($messageTicket);
            $this->om->persist($ticket);
            $messageTicket->setTicket($ticket);
            $this->om->flush();

            return array(
                'status' => 200,
                'message' => 'Success create Ticket. Ticket ID : ' . $ticket->getId()
            );
        } catch (\Exception $e) {
            return array(
                'status' => 400,
                'message' => $e->getMessage(),
            );
        }
    }

    /**
     * reply to a ticket.
     *
     * @param array $parameters
     *
     * @return Ticket
     */
    public function reply(array $parameters)
    {
        try {
            $ticketId = $parameters['ticket'];
            $idUser = $parameters['user'];

            
            $message = $parameters['message'];
            /** @var Ticket $ticket */
            $ticket = $this->om->getRepository('CABTicketBundle:Ticket')->find($ticketId);
            if (!$ticket) {
                return array(
                    'status_http' => 403,
                    'data' => 'Error ticket ID: The ID ticket is not found.',
                );
            }

            $messageTicket = new TicketMessage();

            $messageTicket->setUser($idUser);
            $messageTicket->setMessage($message);
            $messageTicket->setStatus(TicketMessage::STATUS_IN_PROGRESS);
            $messageTicket->setPriority(TicketMessage::PRIORITY_LOW);
            $ticket->addMessage($messageTicket);
            $messageTicket->setTicket($ticket);

            $this->om->persist($messageTicket);
            $this->om->flush();

            return array(
                'status' => 200,
                'message' => 'Success reply Ticket. Ticket ID : ' . $ticket->getId()
            );
        } catch (\Exception $e) {
            return array(
                'status' => 400,
                'message' => $e->getMessage(),
            );
        }
    }

    /**
     * @param $ticketId
     * @return array
     */
    public function getDetailsTicket ($ticketId) {
        /** @var Ticket $ticket */
        $ticket = $this->om->getRepository('CABTicketBundle:Ticket')->find($ticketId);
        if (!$ticket) {
            return array(
                'status_http' => 403,
                'data' => 'Error ticket ID: The ID ticket is not found.',
            );
        }
        //get the user of the ticket
        $userTicketId = $ticket->getUserCreated();


        /** @var User $oUserTicket */
        $oUserTicket = $this->userManager->getUserById($userTicketId);
        $companyTicket = $this->getCompanyUser($oUserTicket);
        //check if the user id has access to the ticket
        /** @var User $oUser */

       /* $oUser = $this->userManager->getUserById($userId);
        if (!$oUser instanceof User) {
            return array(
                'status_http' => 404,
                'data' => 'Error user ID: The ID user not found.',
            );
        }
        if ($oUser->hasRole('ROLE_DRIVER') || $oUser->hasRole('ROLE_CUSTOMER')) {
            if ($userTicketId != $userId) {
                return array(
                    'status_http' => 403,
                    'data' => 'Access denied',
                );
            }
        } elseif ($oUser->hasRole('ROLE_ADMINCOMPANY') ||
            $oUser->hasRole('ROLE_COMPANYBUSINESS') ||
            $oUser->hasRole('ROLE_AGENT')
        ) {
            $companyCurrentUser = $this->getCompanyUser($oUser);
            if ($companyTicket !== null && $companyCurrentUser !== null ) {
                if ($companyTicket->getId() !== $companyCurrentUser->getId()) {
                    return array(
                        'status_http' => 403,
                        'data' => 'Access denied',
                    );
                }
            } else {
                return array(
                    'status_http' => 403,
                    'data' => 'Access denied',
                );
            }
        }
        */
        $messages = array();
        /** @var TicketMessage $message */
        foreach ($ticket->getMessages() as $message) {
            $messages[] = array(
                'body' => $message->getMessage(),
                'created' => $message->getCreatedAt()->format('Y-m-d H:i:s'),
                'author' => $message->getUserObject()->getUsedName(),
                'author_id' => $message->getUserObject()->getId(),
            );
        }
        $result = array(
            'status_http' => 200,
            'data' => array(
                'ticket_id' => $ticket->getId(),
                'subject' => $ticket->getSubjectString(),
                'status' => $ticket->getStatusString(),
                'priority' => $ticket->getPriorityString(),
                'message' => $messages,
            ),
        );

        return $result;
    }

    /**
     * @param User $user
     * @return \CAB\CourseBundle\Entity\Company|mixed|null
     */
    private function getCompanyUser(User $user) {
        $company = null;
        if ($user->hasRole('ROLE_ADMINCOMPANY')) {
            $company = $user->getContacts()->first();
        } elseif ($user->hasRole('ROLE_DRIVER')) {
            $company = $user->getDriverCompany();
        } elseif ($user->hasRole('ROLE_CUSTOMER')) {
            $company = $user->getCustomerCompany();
        } elseif ($user->hasRole('ROLE_AGENT')) {
            $company = $user->getAgentCompany();
        } elseif ($user->hasRole('ROLE_COMPANYBUSINESS')) {
            $company = $user->getBusinessCompany();
        }

        return $company;
    }
}