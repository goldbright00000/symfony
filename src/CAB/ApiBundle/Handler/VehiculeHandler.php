<?php

namespace CAB\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use CAB\CourseBundle\Entity\Vehicule;
use CAB\CourseBundle\Entity\VehiculeAffect;
use CAB\CourseBundle\Entity\VehiculeFuel;
use CAB\CourseBundle\Entity\VehiculeClean;
use CAB\CourseBundle\Entity\VehiculeMaintenance;
use Presta\ImageBundle\Form\DataTransformer\Base64ToImageTransformer;

Class VehiculeHandler extends BaseHandler {

    protected $om;
    private $entityClass;
    private $repository;
    private $manager;

    public function __construct(ObjectManager $om, $manager, $entityClass) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->manager = $manager;
        $this->reopenEntityManager();
    }



    /**
     * Create a new Vehicule affect.
     *
     * @param array $parameters
     *
     * @return VehiculeAffect
     */
    public function postVehicule(array $parameters) {
        //A FAIRE

        $vehicule = $this->createVehicule();
        $vehicule_id = (int) trim($parameters['vehicule_id']);
        $driver_id = (int) trim($parameters['driver_id']);
        $km = null;
        $ecart = null;
        if (isset($parameters['km']))
            $km = trim($parameters['km']);

        if (!$vehicule_id || !$driver_id)
            return false;
        $getDriver = $this->getDriverObject($driver_id);
        $getVehicule = $this->getVehiculeObject($vehicule_id);

        if (!$getDriver || !$getVehicule)
            return false;





        $this->repository->find($vehicule_id);

        $vehicule->setDriver($getDriver);




        $this->om->persist($vehicule);
        $this->om->flush($vehicule);

        return 'toto';
    }

    /**
     * Create a new Vehicule affect.
     *
     * @param array $parameters
     *
     * @return VehiculeAffect
     */
    public function post(array $parameters) {
        $vehicule = $this->createVehicule();
        $vehicule_id = (int) trim($parameters['vehicule_id']);
        $driver_id = (int) trim($parameters['driver_id']);
        $km = null;
        $ecart = null;
        $debutservice = $parameters['debut_service'];
        $finservice = $parameters['fin_service'];
        $latitude = $parameters['latitude'];
        $longitude = $parameters['longitude'];
        $numberlic = $parameters['number_lic'];
        $numberlicpager = $parameters['number_lic_pager'];
        $numberlicpagerfile = $parameters['number_lic_pager_file'];
        if (isset($parameters['km']))
            $km = trim($parameters['km']);
        if (!$vehicule_id || !$driver_id)
            return false;
        $getDriver = $this->getDriverObject($driver_id);
        $getVehicule = $this->getVehiculeObject($vehicule_id);
        if (!$getDriver || !$getVehicule)
            return false;
        $getecart = $this->vehiculeAffectRepo()->getEcartByVehicule($vehicule_id);
        $ecart = $km - $getecart;
        $vehicule->setDriver($getDriver);
        $vehicule->setVehicule($getVehicule);
        $vehicule->setKm($km);
        $vehicule->setEcart($ecart);
        $vehicule->setDebutservice($debutservice);
        $vehicule->setFinservice($finservice);
        $vehicule->setLongitude($longitude);
        $vehicule->setLatitude($latitude);
        $vehicule->setNumberlic($numberlic);
        $vehicule->setNumberlicpager($numberlicpager);
        $vehicule->setNumberlicpagerfile($numberlicpagerfile);
        $this->om->persist($vehicule);
        $this->om->flush($vehicule);

        return $vehicule;
    }

    /**
     * Create a fuel by vehicule.
     *
     * @param array $parameters
     *
     * @return VehiculeFuel
     */
    public function postFuel(array $parameters) {
        $vehicule_id = (int) trim($parameters['vehicule_id']);
        $driver_id = (int) trim($parameters['driver_id']);
        $km = (int) trim($parameters['km']);
        $quantity = (int) trim($parameters['quantity']);
        $amount = (int) trim($parameters['amount']);
        $getDriver = $this->getDriverObject($driver_id);
        $getVehicule = $this->getVehiculeObject($vehicule_id);

        $vehiculefuel = new VehiculeFuel();
        $vehiculefuel->setDriver($getDriver);
        $vehiculefuel->setVehicule($getVehicule);
        $vehiculefuel->setKm($km);
        $vehiculefuel->setQuantity($quantity);
        $vehiculefuel->setAmount($amount);

        if (isset($parameters['file']) && trim($parameters['file'])) {
            $b64 = new Base64ToImageTransformer();
            $file = $b64->reverseTransform(['base64' => trim($parameters['file'])]);
            $vehiculefuel->setFile($file);
        }

        $this->manager->saveEntity($vehiculefuel);

        return $vehiculefuel;
    }

    /**
     * Create a clean by vehicule.
     *
     * @param array $parameters
     *
     * @return VehiculeClean
     */
    public function postClean(array $parameters) {
        $vehicule_id = (int) trim($parameters['vehicule_id']);
        $driver_id = (int) trim($parameters['driver_id']);
        $interior = (int) trim($parameters['interior']);
        $exterior = (int) trim($parameters['exterior']);
        $km = (int) trim($parameters['km']);
        $amount = (int) trim($parameters['amount']);
        $getDriver = $this->getDriverObject($driver_id);
        $getVehicule = $this->getVehiculeObject($vehicule_id);

        $vehiculeclean = new VehiculeClean();
        $vehiculeclean->setDriver($getDriver);
        $vehiculeclean->setVehicule($getVehicule);
        $vehiculeclean->setInterior($interior);
        $vehiculeclean->setExterior($exterior);
        $vehiculeclean->setKm($km);
        $vehiculeclean->setAmount($amount);

        if (isset($parameters['file']) && trim($parameters['file'])) {
            $b64 = new Base64ToImageTransformer();
            $file = $b64->reverseTransform(['base64' => trim($parameters['file'])]);
            $vehiculeclean->setFile($file);
        }

        $this->manager->saveEntity($vehiculeclean);

        return $vehiculeclean;
    }

    /**
     * Create a maintenance by vehicule.
     *
     * @param array $parameters
     *
     * @return VehiculeMaintenance
     */
    public function postmaintenance($parameters) {
        $vehicule_id = (int) trim($parameters['vehicule_id']);
        $driver_id = (int) trim($parameters['driver_id']);
        $amount = (int) trim($parameters['amount']);
        $km = (int) trim($parameters['km']);
        $oilchange = (int) trim($parameters['oilchange']);
        $oilfilter = (int) trim($parameters['oilfilter']);
        $fronttyres = (int) trim($parameters['fronttyres']);
        $reartyres = (int) trim($parameters['reartyres']);
        $battery = (int) trim($parameters['battery']);
        $chassis = (int) trim($parameters['chassis']);
        $brokenwindows = (int) trim($parameters['brokenwindows']);
        $bulbs = (int) trim($parameters['bulbs']);
        $frontbrakepads = (int) trim($parameters['frontbrakepads']);
        $rearbrakepads = (int) trim($parameters['rearbrakepads']);
        $frontbrakediscs = (int) trim($parameters['frontbrakediscs']);
        $rearbrakediscs = (int) trim($parameters['rearbrakediscs']);
        $tow = (int) trim($parameters['tow']);
        $detail = $parameters['detail'];

        $getDriver = $this->getDriverObject($driver_id);
        $getVehicule = $this->getVehiculeObject($vehicule_id);

        $vehiculemaintenance = new VehiculeMaintenance();
        $vehiculemaintenance->setDriver($getDriver);
        $vehiculemaintenance->setVehicule($getVehicule);
        $vehiculemaintenance->setAmount($amount);
        $vehiculemaintenance->setKm($km);
        $vehiculemaintenance->setOilchange($oilchange);
        $vehiculemaintenance->setOilfilter($oilfilter);
        $vehiculemaintenance->setFronttyres($fronttyres);
        $vehiculemaintenance->setReartyres($reartyres);
        $vehiculemaintenance->setBattery($battery);
        $vehiculemaintenance->setChassis($chassis);
        $vehiculemaintenance->setBrokenwindows($brokenwindows);
        $vehiculemaintenance->setBulbs($bulbs);
        $vehiculemaintenance->setFrontbrakepads($frontbrakepads);
        $vehiculemaintenance->setRearbrakepads($rearbrakepads);
        $vehiculemaintenance->setFrontbrakediscs($frontbrakediscs);
        $vehiculemaintenance->setRearbrakediscs($rearbrakediscs);
        $vehiculemaintenance->setTow($tow);
        $vehiculemaintenance->setDetail($detail);

        if (isset($parameters['file']) && trim($parameters['file'])) {
            $b64 = new Base64ToImageTransformer();
            $file = $b64->reverseTransform(['base64' => trim($parameters['file'])]);
            $vehiculemaintenance->setFile($file);
        }

        $this->manager->saveEntity($vehiculemaintenance);

        return $vehiculemaintenance;
    }

    /**
     * Get a list of Vehicules by driver id.
     *
     * @param int $driver_id
     *
     * @return VehiculeAffect
     */
    public function get($driver_id) {
        if (!$driver_id)
            return false;

        $getDriver = $this->getDriverObject($driver_id);
        $driver = $getDriver->getId();



        if (!$getDriver)
            return false;

        $company = $getDriver->getDriverCompany();


        $qb = $this->om->createQueryBuilder();
        $qb->select('f')
            ->from('CABCourseBundle:Vehicule', 'f')//replace friends with the correct entity name
            ->where('f.company= :fcompany AND (f.driver = :fdriver OR f.partageCompany = :fpartage)')
            ->setParameter('fcompany',$company)
            ->setParameter('fdriver', $driver_id)
            ->setParameter('fpartage', true)
	        ->setCacheable(true)
	        ->setCacheRegion('cache_long_time')
            ;

        $users = $qb->getQuery()->getResult();//or more



        return $users;
    }

    /**
     * Get a vehicule by id.
     *
     * @param int $driver_id  Vehicule id
     *
     * @return Vehicule
     */
    public function getVehicule($driver_id) {
        if (!$driver_id)
            return false;

        $getVehiculeAffect = $this->vehiculeRepo()->getVehicleAffectByDriver($driver_id);

        if (!$getVehiculeAffect)
            return false;

        return $getVehiculeAffect;
    }

    /**
     * Get a vehicule by id.
     *
     * @param int $vehicule_id
     *
     * @return Vehicule
     */
    public function getVehiculeById($vehicule_id) {

        $getVehiculeId = $this->vehiculeRepo()->getVehicleById($vehicule_id);

        return $getVehiculeId;
    }

    private function createVehicule() {
        return new $this->entityClass();
    }

    public function getDriverObject($driver_id) {
        $driver = $this->om->getRepository('CABUserBundle:User')->find($driver_id);

        return $driver;
    }

    public function getVehiculeObject($vehicule_id) {
        $vehicule = $this->om->getRepository('CABCourseBundle:Vehicule')->find($vehicule_id);

        return $vehicule;
    }

    /**
     * Get last vehicule affected to driver.
     *
     * @param int $driver_id  Driver id
     *
     * @return Vehicule
     */
    public function getLastAffectVehiculeByDriverId($driver_id) {
        $getVehiculeId = $this->vehiculeAffectRepo()->getLastAffectVehiculeByDriverId($driver_id);

        return $this->vehiculeRepo()->find($getVehiculeId);
    }

    public function vehiculeRepo() {
        return $this->om->getRepository('CABCourseBundle:Vehicule');
    }

    public function vehiculeAffectRepo() {
        return $this->om->getRepository('CABCourseBundle:VehiculeAffect');
    }

}
