<?php

namespace CAB\ApiBundle\Handler;

interface ArticleHandlerInterface
{
    /**
     * Post User, creates a new User.
     *
     * @api
     *
     * @param inetegr $id artocle id
     *
     * @return Article Entity
     */
    public function get($id);
}