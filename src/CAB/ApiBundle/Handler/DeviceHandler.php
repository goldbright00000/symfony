<?php

namespace CAB\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use CAB\CourseBundle\Entity\Device;

Class DeviceHandler extends BaseHandler
{
    protected $om;
    private $entityClass;
    private $repository;

    public function __construct(ObjectManager $om, $entityClass)
    {
        $this->om           = $om;
        $this->reopenEntityManager();
        $this->entityClass  = $entityClass;
        $this->repository   = $this->om->getRepository($this->entityClass);
    }

    /**
     * Create a new push notification.
     *
     * @param array $parameters
     *
     * @return Device
     */
    public function post(array $parameters)
    {
        if (!isset($parameters['user_id']) || empty($parameters['user_id']) || !isset($parameters['registerId'])
         || !isset($parameters['brand']) || !isset($parameters['model']) || !isset($parameters['os']) 
         || !isset($parameters['imeiDevice']) || !isset($parameters['imsiDevice']))
            return false;
        
        $device     = $this->createDevice();
        $user_id    = (int) trim($parameters['user_id']);
        $registerId = trim($parameters['registerId']);
        $brand      = trim($parameters['brand']);
        $model      = trim($parameters['model']);
        $os         = trim($parameters['os']);
        $imeiDevice = trim($parameters['imeiDevice']);
        $imsiDevice = trim($parameters['imsiDevice']);

        $appleImei  = (isset($parameters['appleImei']) && !empty($parameters['appleImei']) ? trim($parameters['appleImei']) : null);
        $token      = (isset($parameters['device_token']) && !empty($parameters['device_token']) ? trim($parameters['device_token']) : null);

        $checkDeviceByUser = $this->repository->findOneBy(array('user' => $user_id));

        if ($checkDeviceByUser)
            return false;

        $user = $this->getUserObject($user_id);

        $device->setUser($user);
        $device->setBrand($brand);
        $device->setModel($model);
        $device->setOs($os);
        $device->setImeiDevice($imeiDevice);
        $device->setAppleImei($appleImei);
        $device->setImsiDevice($imsiDevice);
        $device->setRegisterId($registerId);
        $device->setDeviceToken($token);

        $this->om->persist($device);
        $this->om->flush($device);

        return $device;
    }

    /**
     * Get a device by its id.
     *
     * @param array $parameters
     *
     * @return Device
     */
    public function get($user_id)
    {
        if (!$user_id)
            return false;

        $getDeviceInfo = $this->repository->findOneBy(array('user' => $user_id));
    
        if (!$getDeviceInfo)
            return false;

        return $getDeviceInfo;
    }

    /**
     * Update device push notification.
     *
     * @param array $parameters  Array of parameters to modify
     *
     * @return Device
     */
    public function put(array $parameters = array())
    {        
        if (!$parameters['user_id'])
            return false;

        $user  = trim($parameters['user_id']);
        $device = $this->repository->findOneBy(array('user' => $user));

        if (!$device)
            return false;

        $canUpdate  = 0;

        if (isset($parameters['user_id']) && !empty($parameters['user_id'])) {
            $user_id = (int) trim($parameters['user_id']);
            $user    = $this->getUserObject($user_id);
            $device->setUser($user);
            $canUpdate++;
        }

        if (isset($parameters['registerId']) && !empty($parameters['registerId'])) {
            $registerId = trim($parameters['registerId']);
            $device->setRegisterId($registerId);
            $canUpdate++;
        }

        if (isset($parameters['brand']) && !empty($parameters['brand'])) {
            $brand = trim($parameters['brand']);
            $device->setBrand($brand);
            $canUpdate++;
        }

        if (isset($parameters['model']) && !empty($parameters['model'])) {
            $model = trim($parameters['model']);
            $device->setModel($model);
            $canUpdate++;
        }

        if (isset($parameters['os']) && !empty($parameters['os'])) {
            $os = trim($parameters['os']);
            $device->setOs($os);
            $canUpdate++;
        }

        if (isset($parameters['imeiDevice']) && !empty($parameters['imeiDevice'])) {
            $imeiDevice = trim($parameters['imeiDevice']);
            $device->setImeiDevice($imeiDevice);
            $canUpdate++;
        }

        if (isset($parameters['imsiDevice']) && !empty($parameters['imsiDevice'])) {
            $imsiDevice = trim($parameters['imsiDevice']);
            $device->setImsiDevice($imsiDevice);
            $canUpdate++;
        }

        if (isset($parameters['appleImei']) && !empty($parameters['appleImei'])) {
            $appleImei = trim($parameters['appleImei']);
            $device->setAppleImei($appleImei);
            $canUpdate++;
        }
        
        if (isset($parameters['device_token']) && !empty($parameters['device_token'])) {
            $token = trim($parameters['device_token']);
            $device->setDeviceToken($token);
            $canUpdate++;
        }
        

        if (0 == $canUpdate)
            return false;
        
        $device->setUpdateAt(new \DateTime());
        
        $this->om->flush();

        return true;
    }

    private function createDevice()
    {
        return new $this->entityClass();
    }

    public function getUserObject($user_id)
    {
        $user = $this->om->getRepository('CABUserBundle:User')->find($user_id);
    
        return $user;
    }
}