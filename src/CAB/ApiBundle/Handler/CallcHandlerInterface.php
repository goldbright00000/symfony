<?php

namespace CAB\ApiBundle\Handler;

use CAB\CallcBundle\Entity\Callc;

interface CallcHandlerInterface
{
    /**
     * Persist a Callc.
     *
     * @api
     *
     * @param array $params contains variables to be persisted
     *
     * @return Callc Entity
     */
    public function postCallc(array $params = array());


}