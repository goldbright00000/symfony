<?php
/**
 * Created by PhpStorm.
 * User: viasyst
 * Date: 29/10/2016
 * Time: 01:51
 */

namespace CAB\ApiBundle\Handler;


class BaseHandler
{
    protected function reopenEntityManager()
    {
        if (!$this->om->isOpen()) {
            $this->om = $this->om->create(
                $this->om->getConnection(),
                $this->om->getConfiguration()
            );
        }
    }
}