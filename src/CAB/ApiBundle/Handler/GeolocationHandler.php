<?php
/**
 * Created by PhpStorm.
 * User: m.benhedna
 * Date: 13/10/2015
 * Time: 16:28
 */

namespace CAB\ApiBundle\Handler;

// use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use CAB\CourseBundle\Document\Geolocation;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GeolocationHandler
{
    private $repository;
    protected $odm;
    private $document;
    private $container;
    private $availableDrivers = array();

    public function __construct(DocumentManager $odm, $document, ContainerInterface $container)
    {
        $this->container = $container;
        $this->odm = $odm;
        $this->document = $document;
        $this->repository = $this->odm->getRepository('CABCourseBundle:Geolocation');
    }

    /**
     * List all available drivers from given coords
     *
     * @param int|null $courseID
     * @param int $vehicleTypeID
     * @return Geolocation
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getAvailableDrivers($courseID = null, $vehicleTypeID = 0)
    {
        $from = new \DateTime('now');
        $to = new \DateTime('now');
        $timezone = new \DateTimeZone('GMT');
        $from->setTimezone($timezone);
        $to->setTimezone($timezone);
        $from->modify("-10 seconde");
        $now  = $from->format('Y-m-d H:i:s');
        $to = $to->format('Y-m-d H:i:s');
        $result = array();
        $driverNotAvailable = array();

        $q = $this->odm->createQueryBuilder('CAB\CourseBundle\Document\Geolocation')
            ->distinct('driver')
            ->field('availibility')->equals(true)
            ->field('availibility')->notEqual(0)
            ->field('driver')->notEqual(0)
            ->field('dateGeolocate')->gte($now)
            ->field('dateGeolocate')->lte($to)
            ->sort('dateGeolocate', 'desc')
            ->eagerCursor(true)
            ->limit(5)
            ->getQuery()
            ->execute()
        ;

        foreach ($q as $geolocationDriver) {
            //check if the last course of the driver is completed
            $driverNotAvailable = array();
            $courseManager = $this->container->get('cab.course_manager');
            $courseNotCompleted = $courseManager->getCourseNotCompletedByDriver($geolocationDriver);

            if (count($courseNotCompleted) > 0) {
                foreach ($courseNotCompleted as $course) {
                    $driverNotAvailable[$course->getDriver()->getId()] = $course->getDriver()->getId();
                }
            }

            $listCancelledByDriver = array();
            $oUser = $this->container->get('cab.cab_user.manager')->loadUser($geolocationDriver);
            //Check if the driver exist in history of this course
            if ($courseID) {
                $courseHistoryManager = $this->container->get('cab_course.manager.courseHistory_manager');
                $oCourse = $courseManager->loadCourse($courseID);
                $listCancelledByDriver = $courseHistoryManager->getHitoryByCourseAndDriver($oCourse, $oUser);
            } else {
                $tz = new \DateTimeZone('UTC');
                $valueFrom = new \DateTime('now', $tz);
                $listCancelledByDriver = $courseManager->getRepository()->findCourseByDatesDepArrival($oUser, $valueFrom);
            }
            if (!array_key_exists($geolocationDriver, $result) &&
                !array_key_exists($geolocationDriver, $driverNotAvailable) && count($listCancelledByDriver) === 0) {
                $result[$geolocationDriver] = $this->repository->findBy(
                    array(
                        'driver' => $geolocationDriver,
                        'availibility' => true,
                    ),
                    array(

                        'dateGeolocate' => 'DESC',
                    ),
                    1
                );
            }
        }

        return $result;
    }

    /**
     * List geos with date
     *
     * @return Geolocation
     */
    public function getGeosByDate($date)
    {
        return $this->repository->checkAvailableDriversByDate($date);
    }

    /**
     * Get closest driver to customer
     *
     * @param  array $params Customer's latitude and longitude
     *
     * @param bool $futureCourse
     * @return array
     */
    public function getCloseDriver(array $params = array(), $futureCourse = false)
    {
        $courseID = null;
        if (array_key_exists('course_id', $params)) {
            $courseID = $params['course_id'];
        }
        $vehicleTypeID = 0;
        if (array_key_exists('type_vehicle_id', $params)) {
            $vehicleTypeID = $params['type_vehicle_id'];
        }
        $customerCoords = $this->customerCoords($params);

        if (!$customerCoords || !$params) {
            return array();
        }

        /*
         * List of driver availables
         */
        $driversCollection = $this->getAvailableDrivers($courseID);

        if (!$driversCollection) {
            return array();
        }

        if ($vehicleTypeID !== 0) {
            foreach ($driversCollection as $idDriver => $driver) {
                $oDriver = $this->container->get('cab.cab_user.manager')->loadUser($idDriver);
                $idTypeVehicle = $oDriver->getVehicle()->getTypeVehicule()->getId();

                if ($idTypeVehicle != $vehicleTypeID) {
                    unset($driversCollection[$idDriver]);
                }
            }
        }

        $getDriversCoords = $this->getDestinationsAsString($driversCollection, $futureCourse);

        if (!$getDriversCoords) {
            return array();
        }

        if ($futureCourse) {
            return $this->availableDrivers;
        }

        return $this->getNearestDriver(
            array(
                'origins' => $customerCoords,
                'destinations' => $getDriversCoords
            )
        );
    }


    /**
     * Get customer's lat and long to be compatible with url
     */
    public function customerCoords(array $params = array())
    {
        if (!isset($params['latitude']) || !isset($params['longitude'])) {
            return false;
        }

        return $params['latitude'].','.$params['longitude'];
        //return urlencode($params['latitude'].','.$params['longitude']);
    }

    /**
     * Grab drivers' positions to search for
     * @param array $positions A list of object(CAB\CourseBundle\Document\Geolocation)
     * @param $futureCourse
     * @return bool|string
     */
    public function getDestinationsAsString(array $positions = array(), $futureCourse)
    {
        $coords = false;

        if (is_array($positions) && count($positions)) {
            $p = 0;
            $from = new \DateTime("now");
            $timezone = new \DateTimeZone("GMT");
            $from->setTimezone($timezone);
            $from->modify("-15 seconde");

            foreach ($positions as $key => $position) {
                $pos = $position[0];
                $dateGeolocate = $pos->getDateGeolocate() instanceof \MongoDate ? date(
                    'Y-m-d H:i:s',
                    $pos->getDateGeolocate()->sec
                ) : $pos->getDateGeolocate();

                $dateGeo = new \DateTime($dateGeolocate, new \DateTimeZone('GMT'));

                $interval = date_diff($from, $dateGeo);

                $seconds = $interval->days*86400 + $interval->h*3600
                    + $interval->i*60 + $interval->s;

                /*
                 * Compare $dateGeo with actual date, if the difference is less than 10 secondes, we return the record, else remove the record from returned list.
                 * Have to 5 result max
                 */
                if ($p >= 20) {
                    break;
                }
                if ($futureCourse) {
                    if (($pos->getLatitude() && $pos->getLongitude()) && !in_array(
                            $pos->getDriver(),
                            $this->availableDrivers
                        )
                    ) {
                        $coords .= ($p > 0 ? '|' : '');
                        $coords .= $pos->getLatitude() . ',' . $pos->getLongitude();
                        $this->availableDrivers[] = $pos->getDriver();
                        $p++;
                    }
                } else {
                    if ($seconds < 20 && ($pos->getLatitude() && $pos->getLongitude()) && !in_array(
                            $pos->getDriver(),
                            $this->availableDrivers
                        )
                    ) {
                        $coords .= ($p > 0 ? '|' : '');
                        $coords .= $pos->getLatitude() . ',' . $pos->getLongitude();
                        $this->availableDrivers[] = $pos->getDriver();
                        $p++;
                    }
                }
            }
        }

        return $coords;
    }

    /**
     * Get endpoint API
     */
    public function getEndPoint(array $coords = array())
    {
        if (!$this->container->hasParameter('server_key') || !$this->container->getParameter('server_key')
            || !$coords['origins'] || !$coords['destinations']
        ) {
            return false;
        }
        $origins = $coords['origins'];
        $destinations = $coords['destinations'];

        //return "http://maps.googleapis.com/maps/api/distancematrix/json?origins=".$origins."&destinations=".$destinations;

        return "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$origins."&destinations=".$destinations."&key=".$this->container->getParameter(
            'server_key'
        );
    }

    /**
     * Get list of distination with km and mins and locations names
     */
    public function getMatrixInfo(array $coords = array())
    {
        if (!count($coords['origins']) || !count($coords['destinations'])) {
            return false;
        }

        return json_decode(file_get_contents($this->getEndPoint($coords)));
    }

    public function getListOfDestinations(array $coords = array())
    {
        if (!count($coords)) {
            return false;
        }

        $getFoundDrivers = array();
        $result = $this->getMatrixInfo($coords);

        if ($result->status == "OK") {
            $points = $result->rows[0]->elements;

            if (count($points)) {
                foreach ($points as $key => $point) {
                    if ($point->status == "OK") {
                        if ($point->duration->value < 1800) {
                            $getFoundDrivers[$key] = array(
                                'location_street' => (isset($result->destination_addresses[$key]) ? $result->destination_addresses[$key] : ''),
                                'driver_id' => (isset($this->availableDrivers[$key]) ? $this->availableDrivers[$key] : ''),
                                'distance_text' => $point->distance->text,
                                'distance_value' => $point->distance->value,
                                'duration_text' => $point->duration->text,
                                'duration_value' => $point->duration->value,
                            );
                        }
                    }
                }
            }
        } else {
            $getFoundDrivers = array(
                'status' => $result->status
            );
        }

        return $getFoundDrivers;
    }

    /**
     * Get closest drivers to current client
     * @return array
     *
     */
    public function getNearestDriver(array $coords = array())
    {
        if (!count($coords)) {
            return false;
        }

        // $min = 0;
        $close_drivers = [];

        // get a list of drivers with their distances from client
        $result = $this->getListOfDestinations($coords);
        // ump($result); exit;

        if (count($result)) {
            // if status is set that means there is something wrong with google API
            if (isset($result['status']) && !empty($result['status'])) {
                $close_drivers['status'] = $result['status'];
            } else {
                $check_existing_keys = [];

                foreach ($result as $key => $res) {
                    $duration = $res['duration_value'];
                    // we may have drivers with same distance durations, so fix it
                    if (count($check_existing_keys) && in_array($duration, $check_existing_keys)) {
                        foreach ($check_existing_keys as $key2 => $duration_key) {
                            if ($duration_key == $duration)
                                $duration += 1;
                        }
                    }

                    $close_drivers[$duration] = $res;
                    $check_existing_keys[]    = $duration;
                }
                unset($check_existing_keys);
            }
        }

        $get_five_drivers = [];
        if (count($close_drivers)) {
            // sort drivers, so that we get the closest drivers first and so on
            ksort($close_drivers);

            // get rid of keys
            foreach ($close_drivers as $key => $closestDriver) {
                $get_five_drivers[] = $closestDriver;
            }
        }
        // clean up memory
        unset($close_drivers, $result);

        // just return the five closest drivers
        return array_slice($get_five_drivers, 0, 5);
    }

    public function getDriverById($driver_id, $order = "DESC", $limit = 1,$return = false)
    {
        $checkLimit = false;

        if (false !== $return || $limit > 1)
            $checkLimit = true;
/*        $limit = (is_int($limit) ? (int)$limit : '');
        $driverGeo = $this->repository->findBy(array('driver' => (int)$driver_id), array('dateGeolocate' => $order), $limit);*/
        $driverGeo = $this->repository->getDriverById($driver_id, $checkLimit);

        return $driverGeo;
    }

    /**
     * Create a new Geolocation.
     *
     * @param array $parameters
     *
     * @return Geolocation
     */
    public function post(array $parameters)
    {
        $course = new Geolocation();

        return $this->processForm($course, $parameters, 'POST');
    }

    /**
     * Processes the form.
     *
     * @param Geolocation $geolocation
     * @param array       $parameters
     * @param String      $method
     *
     * @return Geolocation
     * @throws InvalidFormException
     */
    private function processForm(Geolocation $geolocation, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new ApiCourseType(), $course, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->instanceCourse($course);

            if ($result['status'] === 1) {
                $oCourse = $result['response'];
                $oCourse->setDriver(null);
                $this->om->persist($oCourse);
                $this->om->flush($oCourse);

                return $oCourse;
            } else {
                return $result;
            }
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    public function getGeoRepo()
    {
        return $this->repository;
    }

    public function getDriverGeos($driverGeos, $getDate = false)
    {
        $getDriverGeos = array();

        if ($driverGeos)
        foreach ($driverGeos as $key => $geo) {
            $dateGeolocate = $geo->getDateGeolocate() instanceof \MongoDate ? date(
                'Y-m-d H:i:s',
                $geo->getDateGeolocate()->sec
            ) : $geo->getDateGeolocate();
            $dateGeo = new \DateTime($dateGeolocate);

            if (false === $getDate) {
                $timeZone       = new \DateTimeZone('GMT');
                $requestedDate  = new \DateTime('now');
                $requestedDate->setTimezone($timeZone);
            } else {
                $requestedDate  = new \DateTime($this->convertDateTo($getDate));
            }

            $today = $requestedDate->format('Y-m-d 00:00:05');

            if (strtotime($dateGeo->format('Y-m-d H:i:s')) >= strtotime($today)
                && ($geo->getLatitude() && $geo->getLongitude())) {
                $getDriverGeos[] = array(
                    'driver' => $geo->getDriver(),
                    'lat' => $geo->getLatitude(),
                    'lng' => $geo->getLongitude(),
                    'geo_date' => $dateGeo->format('Y-m-d H:i:s')
                );
            }
        }

        return $getDriverGeos;
    }

    public function convertDateTo($date, $format = 'sql')
    {
        if ($format == 'sql') {
            list($day, $month, $year) = explode("/", $date);
            return $year .'-'. $month .'-'. $day;
        }
    }

    public function getAvailableDriversByDate(array $drivers = array(), $date)
    {
        return $this->repository->getAvailableDriversByDate($drivers, $date);
    }
}
