<?php

namespace CAB\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;

Class AppHandler extends BaseHandler implements ArticleHandlerInterface
{
    protected $om;
    private $entityClass;
    private $repository;

    public function __construct(ObjectManager $om, $entityClass)
    {
        $this->om           = $om;
        $this->entityClass  = $entityClass;
        $this->repository   = $this->om->getRepository($this->entityClass);
        $this->reopenEntityManager();
    }

    /**
     * Get an article.
     *
     * @param integer|string $mixed id/slug of article
     *
     * @return Article
     */
    public function get($mixed)
    {
        if (is_numeric($mixed))
            return $this->repository->find($mixed);
        else 
            return $this->repository->findOneBy(array('slug' => $mixed));
    }
}