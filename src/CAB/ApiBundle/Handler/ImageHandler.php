<?php

namespace CAB\ApiBundle\Handler;

Class ImageHandler
{
    /**
     * Decode image based on base64.
     *
     * @return string
     */
    public function decodeImage($encoded_image, $path)
    {
        if (empty($encoded_image) || empty($path))
            return false;

        $data  = base64_decode($encoded_image);
        $image = imagecreatefromstring($data);

        if (false !== $image) {
            $createImage = imagepng($image, $path);
            imagedestroy($image);
            
            if (false !== $createImage)
                return basename($path);
        }

        return false;
    }

    /**
     * Get image and encode it
     *
     * @param string $image  image to be encoded
     *
     * @return string
     */
    public function getContentAndEncode($image)
    {
        $image_data = @file_get_contents($image);
        
        return base64_encode($image_data);
    }

    public function getImageMime($image)
    {
        return getimagesize($image)['mime'];
    }
}