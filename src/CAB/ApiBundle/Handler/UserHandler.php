<?php

namespace CAB\ApiBundle\Handler;

use CAB\CourseBundle\Form\FormErrors;
use CAB\CourseBundle\Locale\CountryAliases;
use CAB\UserBundle\Form\Type\ApiRegistrationFormType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Intl\Locale;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use CAB\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

Class UserHandler extends BaseHandler implements UserHandlerInterface {

    protected $om;
    private $entityClass;
    private $repository;
    private $formFactory;
    private $token;
    private $role = null;
    private $formErrors;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory, TokenStorageInterface $token, FormErrors $formErrors) {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
        $this->token = $token;
        $this->formErrors = $formErrors;
        $this->reopenEntityManager();
    }

    /**
     * Create a new Page.
     *
     * @param array $parameters
     *
     * @return AdvancedUserInterface
     */
    public function post(array $parameters) {
        $user = $this->createUser();

        if (isset($parameters['roles'])) {
            $this->role = $parameters['roles'];
            unset($parameters['roles']);
        }

        if (isset($parameters['country']) && strlen($parameters['country']) > 2) {
            $parameters['country'] = CountryAliases::getCodeCountryByFullName($parameters['country']);
        }

        return $this->processForm($user, $parameters, 'POST');
    }

    /**
     * Edit a Page.
     *
     * @param array $parameters
     *
     * @return AdvancedUserInterface
     */
    public function put(array $parameters = array()) {
        $user_id = (int) $parameters['user_id'];

        if (!isset($user_id) || !count($parameters))
            return false;

        $user = $this->repository->find($user_id);

        if (!$user)
            return false;

        $address = $parameters['address'] ? trim($parameters['address']) : '';
        $email = $parameters['email'] ? trim($parameters['email']) : '';
        $phoneMobile = $parameters['phoneMobile'] ? trim($parameters['phoneMobile']) : '';
        $postCode = $parameters['postCode'] ? trim($parameters['postCode']) : '';
        $country = $parameters['country'] ? trim($parameters['country']) : '';
        $latuser = $parameters['latuser'] ? trim($parameters['latuser']) : '';
        $lnguser = $parameters['lnguser'] ? trim($parameters['lnguser']) : '';
        unset($parameters);

        if (!$address || !$email || !$phoneMobile || !$postCode || !$country || !$latuser || !$lnguser) {
            return false;
        }

        $user->setAddress($address);
        $user->setEmail($email);
        $user->setPhoneMobile($phoneMobile);
        $user->setPostCode($postCode);
        $user->setCountry($country);
        $user->setLatUser($latuser);
        $user->setLngUser($lnguser);
        $user->setisCredit('TOTO');
        $user->setUpdatedAt(new \DateTime());

        $this->om->flush();

        /* if ($user) {
          // handle Avatar
          if (isset($parameters['avatar']) && !empty($parameters['avatar'])) {
          $avatar = $parameters['avatar'];

          }
          } */

        return $user;
    }

    /**
     * Processes the form.
     *
     * @param AdvancedUserInterface $user
     * @param array                 $parameters
     * @param String                $method
     *
     * @return AdvancedUserInterface
     *
     * @throws \CAB\ApiBundle\Exception\InvalidFormException
     */
    private function processForm(AdvancedUserInterface $user, array $parameters, $method = "PUT") {
        $form = $this->formFactory->create(new ApiRegistrationFormType($this->token, null, true), $user, array('method' => $method));

        $form->submit($parameters, 'PATCH' !== $method);

        if ($form->isSubmitted() && $form->isValid()) {
            if (null !== $this->role) {
                $user->setRoles(array($this->role));
            }
            $role = $form->getData()->getRoles();
            if (in_array("ROLE_CUSTOMER", $role)) {
                $user->setEnabled(true);
            } else {
                $user->setEnabled(false);
            }
            $user = $form->getData();
            $this->om->persist($user);
            $this->om->flush($user);

            return $user;
        }
        $fErrors = $this->formErrors->getFormErrors($form);

        return $fErrors;
    }

    private function createUser() {
        return new $this->entityClass();
    }

    private function allowedUserFields() {
        return array(
            'address', 'phonemobile', 'email',
            'postCode', 'country', 'latuser', 'lnguser'
        );
    }

}
