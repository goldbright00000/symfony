<?php

namespace CAB\ApiBundle\Handler;

interface RatingHandlerInterface
{
    /**
     * Get Rating.
     *
     * @api
     *
     * @param inetegr $id rating id
     *
     * @return Rating Entity
     */
    public function getDriver($id);

    /**
     * Persist a Rating.
     *
     * @api
     *
     * @param array $params contains variables to be persisted
     *
     * @return Rating Entity
     */
    public function postDriver(array $params = array());

    /**
     * Update a Rating.
     *
     * @api
     *
     * @param array $params  contains variables to be updated
     *
     * @return Rating Entity
     */
    public function putDriver(array $params = array());

    /**
     * Persist a Rating.
     *
     * @api
     *
     * @param array $params contains variables to be persisted
     *
     * @return Rating Entity
     */
    public function postClient(array $params = array());
}