<?php

namespace CAB\ApiBundle\Handler;

use Application\Sonata\UserBundle\Entity\User;
use CAB\ApiBundle\Exception\InvalidFormException;
use CAB\ApiBundle\Manager\UserManager;
use CAB\CourseBundle\Entity\DetailsTarif;
use CAB\CourseBundle\Entity\Tarif;
use CAB\CourseBundle\Entity\TypeVehicule;
use CAB\CourseBundle\Events\HistoryCourseEvents;
use CAB\CourseBundle\Events\UpdateStatusCourseEvents;
use CAB\CourseBundle\Form\ApiCourseType;
use CAB\CourseBundle\Form\ApiCommandCourseType;
use CAB\CourseBundle\Handler\PlaningHandler;
use CAB\CourseBundle\Manager\CompanyManager;
use CAB\CourseBundle\Manager\TarifManager;
use CAB\CourseBundle\Manager\CourseManager;
use CAB\CourseBundle\Manager\VehicleTypeManager;
use CAB\CourseBundle\PushNotification\Push;
use Doctrine\Common\Persistence\ObjectManager;
use CAB\CourseBundle\Entity\Course;
use Doctrine\ORM\PersistentCollection;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CourseHandler
 *
 * @package CAB\ApiBundle\Handler
 */
Class CourseHandler extends BaseHandler
{
    /**
     * @var ObjectManager
     */
    protected $om;
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;
    /**
     * @var UserManager
     */
    protected $userManager;
    /**
     * @var CourseManager
     */
    protected $courseManager;
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;
    /**
     * @var TarifManager
     */
    protected $tarifManager;

    /**
     * @var CompanyManager
     */
    protected $companyManager;

    /**
     * @var PlaningHandler
     */
    protected $planingHandler;

    /**
     * @var $vehicleTypeManager
     */
    protected $vehicleTypeManager;

    /** @var Push $push */
    protected $push;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param ObjectManager $om
     * @param UserManager $userManager
     * @param CourseManager $courseManager
     * @param FormFactoryInterface $formFactory
     * @param TarifManager $tarifManager
     * @param CompanyManager $companyManager
     * @param PlaningHandler $planingHandler
     * @param VehicleTypeManager $vehicleTypeManager
     * @param Push $push
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct (
        ObjectManager $om,
        UserManager $userManager,
        CourseManager $courseManager,
        FormFactoryInterface $formFactory,
        TarifManager $tarifManager,
        CompanyManager $companyManager,
        PlaningHandler $planingHandler,
        VehicleTypeManager $vehicleTypeManager,
        Push $push,
        EventDispatcherInterface $dispatcher
    )
    {
        $this->om = $om;
        $this->repository = $this->om->getRepository('CABCourseBundle:Course');
        $this->userManager = $userManager;
        $this->courseManager = $courseManager;
        $this->formFactory = $formFactory;
        $this->tarifManager = $tarifManager;
        $this->companyManager = $companyManager;
        $this->planingHandler = $planingHandler;
        $this->vehicleTypeManager = $vehicleTypeManager;
        $this->push = $push;
        $this->dispatcher = $dispatcher;
        $this->reopenEntityManager();
    }

    /**
     * Create a new course.
     *
     * @param array $parameters
     *
     * @return Course
     * @throws \CAB\ApiBundle\Exception\InvalidFormException
     */
    public function post (array $parameters)
    {

        $course = new Course();
        $typeVehicle = 0;
        $callc = false;
        if (array_key_exists('vehicleType', $parameters)) {
            $typeVehicle = $parameters['vehicleType'];
            unset($parameters['vehicleType']);
        }
        if (array_key_exists('callc', $parameters)) {
            $callc = $parameters['callc'];
            unset($parameters['callc']);
        }

        return $this->processForm($course, $parameters, 'POST', $typeVehicle, $callc);
    }

    /**
     * patch a course.
     *
     * @param array $parameters
     *
     * @return array
     */
    public function patch (array $parameters)
    {
        if (!$parameters['course_id']) {
            return false;
        }
        if (!$parameters['driver_id']) {
            $parameters['driver_id'] = null;
        }

        $courseID = trim($parameters['course_id']);
        $oCourse = $this->repository->find($courseID);
        echo "<pre>";print_r($oCourse->getDriver());die;
        //check if the current driver is the affected driver for this course
        if ($oCourse->getDriver()) {
            if ($oCourse->getDriver()->getId() != $parameters['current_driver_id']) {
                return array(
                    'status' => false,
                );
            }
            // if the driver id is defined

            if ($parameters['driver_id'] != null) {
                $oNewAffectedDriver = $this->userManager->findUserBy(array('id' => $parameters['driver_id']));
                if ($oNewAffectedDriver instanceof \CAB\UserBundle\Entity\User && $oNewAffectedDriver->hasRole('ROLE_DRIVER')) {
                    $this->courseManager->setAttr($oCourse, 'driver', $oNewAffectedDriver);
                    $this->courseManager->save($oCourse);

                    $event = new GenericEvent(
                        $oCourse,
                        array(
                            'status_course' => Course::STATUS_AFFECTED,
                        )
                    );

                    $eventHistory = new GenericEvent(
                        $oCourse,
                        array(
                            'course_history' => Course::STATUS_UPDATED_BY_DRIVER,
                        )
                    );

                    $this->dispatcher->dispatch(UpdateStatusCourseEvents::UPDATE_STATUS_COURSE, $event);
                    $this->dispatcher->dispatch(HistoryCourseEvents::HISTORY_COURSE, $eventHistory);

                    return array(
                        'status' => true,
                        'course' => $oCourse,
                        'new_affected_driver' => $oCourse->getDriver()->getId(),
                    );
                } else {
                    return array(
                        'status' => false,
                    );
                }
            } else {
                $this->courseManager->setAttr($oCourse, 'driver', null);
                $this->courseManager->setAttr($oCourse, 'courseStatus', Course::STATUS_UPDATED_BY_DRIVER);
                $this->courseManager->save($oCourse);

                $repository = $this->userManager->getRepository();
                $aDriversEnabled = $repository->getDiverAvailableListByCompany($parameters['current_driver_id']);


                return array(
                    'status' => true,
                    'course' => $oCourse,
                    'list_drivers' => $aDriversEnabled,
                );

            }
        }

    }

	/**
	 * called by patchCommentCourseAction
	 *
	 * @param array $parameters
	 *
	 * @return array
	 */
	public function patchComment( array $parameters)
	{
		if (!$parameters['course']) {
			return [
				'status' => false,
			];
		}

		$courseID = trim($parameters['course']);
		/** @var Course $oCourse */
		$oCourse = $this->repository->find($courseID);
		$oDriver = $oCourse->getDriver();
		//check if the current driver is the affected driver for this course
		if (!$oDriver) {
			return [
				'status' => false,
			];
		}

		$comment = htmlspecialchars($parameters['comment'], ENT_QUOTES, 'UTF-8');
		$oCourse->setDescriptionChildren($comment);
		$this->courseManager->save($oCourse, false);

		return array(
			'status' => true,
		);




    }

    public function putCallc(array $parameters) {
        try {
            $resolver = new OptionsResolver();
            $resolver->setDefaults(array(
                'initial_course_time' => null,
                'departureDate' => '',
                'departureTime' => '',
                'startedAddress' => '',
                'indicatorStartedAddress' => '',
                'arrivalAddress' => '',
                'indicatorArrivalAddress' => '',
                'course_notes_particulieres' => '',
            ));
            $resolver->setRequired(array('client_id', 'initial_course_date'));
            $options = $resolver->resolve($parameters);
            /** @var \CAB\UserBundle\Entity\User $oClient */
            $oClient = $this->userManager->getRepository()->find($options['client_id']);
            $valueDeparture = \DateTime::createFromFormat('d-m-Y', $options['initial_course_date']);
            if ($options['initial_course_time']) {
                $timeDeparture = explode(':', $options['initial_course_time']);
                $valueDeparture->setTime($timeDeparture[0], $timeDeparture[1], $timeDeparture[2]);
                $result = $this->repository->findCourseByDatesDepArrival($oClient, $valueDeparture, 'client', false);
            } else {
                $valueFrom = \DateTime::createFromFormat('d-m-Y', $options['initial_course_date']);
                $valueTo = \DateTime::createFromFormat('d-m-Y', $options['initial_course_date']);
                $valueFrom->setTime(0, 0, 0);
                $valueTo->setTime(23, 59, 59);
                $result = $this->repository->findCourseByDates($oClient, 'client', 'departureDate', $valueFrom, $valueTo, true);
            }

            if (count($result)) {
                /** @var Course $course */
                $course = $result[0];
                foreach ($options as $key => $value) {
                    if ($key === 'departureDate' && $value !== '') {
                        $valueDate = \DateTime::createFromFormat('d-m-Y', $value);
                        $this->courseManager->setAttr($course, $key, $valueDate, false);
                    } elseif ($key === 'departureTime' && $value !== '') {
                        $valueTime = \DateTime::createFromFormat('H:i:s', $value);
                        $this->courseManager->setAttr($course, $key, $valueTime, false);
                    } elseif (strlen($value)) {
                        $this->courseManager->setAttr($course, $key, $value, false);
                    }
                }
                //$this->courseManager->setAttr($course, 'course_status', Course::STATUS_UPDATED_BY_CALLC, false);
                $this->courseManager->save($course);
                return array(
                    'status' => Response::HTTP_OK,
                    'response' => 'Course updated',
                    'course' => $course,
                    'client' => $oClient,
                );
            } else {
                $messageDepDate = $options['initial_course_date'];
                if ($options['initial_course_time']) {
                    $messageDepDate = $options['initial_course_date'] . ' ' . $options['initial_course_time'];
                }
                return array(
                    'status' => Response::HTTP_NOT_FOUND,
                    'response' => 'No race found for The client ' . $oClient->getUsedName() . ' in this date : ' .  $messageDepDate,
                );
            }

        }  catch (\Exception $e) {
            return array(
                'status' => Response::HTTP_BAD_REQUEST,
                'response' => $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine(),
            );
        }
    }
    /**
     * Update course
     *
     * @param array $parameters Array of parameters to modify
     *
     * @return Course
     */
    public function put (array $parameters = array())
    {
        if (!$parameters['course_id']) {
            return false;
        }

        $courseID = trim($parameters['course_id']);
        $oCourse = $this->repository->find($courseID);

        $oDriver = $this->userManager->findUserBy(array('id' => $parameters['driver_id']));

        if (!$oCourse || !$oDriver) {
            return array(
                'status' => 0,
                'response' => 'Error driver or course',
            );
        }


        $statusCourse = $parameters['status'];
        if ($statusCourse === Course::STATUS_AFFECTED) {
            $messageReturned = 'Course successfully affected.';
            $status = 1;
            $courseID = trim($parameters['course_id']);
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
        } elseif ($statusCourse === Course::STATUS_REJECTED) {
            $status = 2;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'Course rejected by driver';
            $courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_NO_REPONSE) {
            $status = 14;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'Pas de reponse du chauffeur';
            $courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_ARRIVED_DRIVER) {
            $status = 13;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'driver arrival';
            $courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_ON_RACE_DRIVER) {
            $status = 9;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'driver arrival';
            $courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_DONE) {
            $status = 6;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'race finish';
            $courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_CANCELLED_BY_DRIVER) {
            $status = 15;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'race cancel by the driver';
            //$courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_CANCELLED_BY_CLIENT) {
            $status = 3;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'race cancel by the driver';
            //$courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_CREATED) {
            $status = 0;
            $this->courseManager->setAttr($oCourse, 'driver', null);
            $messageReturned = 'new race is available';
            $courseID = trim($parameters['course_id']);
        } elseif ($statusCourse === Course::STATUS_DRIVER_GO_TO_CLIENT) {
            $status = 18;
            $this->courseManager->setAttr($oCourse, 'driver', $oDriver);
            $messageReturned = 'driver go to customer';
            $courseID = trim($parameters['course_id']);
        }

        $this->courseManager->setAttr($oCourse, 'courseStatus', $statusCourse);
        $this->courseManager->saveCourse($oCourse);

        $this->om->flush();

        return array(
            'status' => $status,
            'response' => $messageReturned,
            'course_id' => $courseID,
            'course_date' => $oCourse->getDepartureDate()->format('Y-m-d'),
            'course_time' => $oCourse->getDepartureTime()->format('H:i:s'),
            'course_departure' => $oCourse->getStartedAddress(),
            'course_arrival' => $oCourse->getArrivalAddress(),
            'long_departure' => $oCourse->getLongDep(),
            'lat_departure' => $oCourse->getLatDep(),
            'long_arrival' => $oCourse->getLongArr(),
            'lat_arrival' => $oCourse->getLatArr(),
        );
    }

    /**
     * Processes the form.
     *
     * @param Course $course
     * @param array $parameters
     * @param String $method
     * @param int $typeVehicle
     *
     * @return Course
     * @throws \Exception
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     */
    private function processForm (Course $course, array $parameters, $method = 'PUT', $typeVehicle = 0, $callc = false)
    {
        $depDate = null;

        if (array_key_exists('departureDate', $parameters)) {
            $depDate = new \DateTime($parameters['departureDate']);
            $parameters['departureDate'] = $depDate;

            $form = $this->formFactory->create(ApiCommandCourseType::class, $course, array('method' => $method));
        } else {
            $form = $this->formFactory->create(ApiCourseType::class, $course, array('method' => $method))->add('departureDate');
        }
        try {
            $form->submit($parameters, 'PATCH' !== $method);
        } catch (\Exception $e) {
            throw new Exception($e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine());
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->instanceCourse($course, $depDate, $typeVehicle, $callc);
            if ($result['status'] === 1) {
                if ($result['courseId'] != 0) {
                    /** @var Course $oCourse */
                    $oCourse = $result['response'];
                } else {
                    $oCourse = $result['course'];
                }

                $oCourse->setDriver(null);
                $this->om->persist($oCourse);
                //$this->om->persist($oCourse->getCourseDetailsTarif());
                $this->om->flush();

                //dispatch an event course saved if the entity stored is a course
                if ($oCourse instanceof Course) {
                    $event = new GenericEvent($oCourse, array('course_history' => 1));
                    $this->dispatcher->dispatch(HistoryCourseEvents::HISTORY_COURSE, $event);
                }

                // check if is a command
                if ($result['courseId'] == 0) {

                  $result['response']['commandCourseId'] = $oCourse->getId();


                   return $result['response'];
                }

                return $oCourse;
            } else {

                return $result;
            }
        } else {
            $errors = array();
            foreach ($form as $fieldName => $formField) {
                foreach ($formField->getErrors(true) as $key => $error) {
                    $errors[$fieldName] = $error->getMessage();
                    $errors['#'][$key] = $error->getMessage() . ' ' . $error->getCause();
                }
            }

            return $errors;
        }
        /*
                $errors = array();
                foreach ($form as $fieldName => $formField) {
                    foreach ($formField->getErrors(true) as $key => $error) {

                        $errors[$fieldName] = $error->getMessage();
                        $errors['#'][$key] = $error->getMessage().' '.$error->getCause();

                    }
                }

                foreach ($form->all() as $child) {
                    if (!$child->isValid()) {
                        $errors = array("not_valid" => 1);
                        foreach ($child->getErrors() as $key => $error) {
                            if ($child->isRoot()) {
                                $errors['#'][$key] = $error->getMessage().' '.$error->getCause();
                            } else {
                                $errors[$key] = $error->getMessage().' '.$error->getCause();
                            }
                        }
                    }
                }
        */

        throw new InvalidFormException($form->getErrorsAsString(), $form);
    }

    /**
     * Instance course
     *
     * @param Course $course
     *
     * @return array
     */
    private function instanceCourse (Course $course, $depDate = null, $idTypeVehicle = null, $callc = false)
    {
        try {
            //@TODO check the format of the date
            if (!$depDate) {
                $departureDate = new \DateTime('now');
                $this->courseManager->setAttr($course, 'departureDate', $departureDate);
                $this->courseManager->setAttr($course, 'departureTime', $departureDate);
            } else {
                $this->courseManager->setAttr($course, 'departureTime', $depDate);
            }

            if ($course->getPersonNumber() === null) {
                $this->courseManager->setAttr($course, 'personNumber', 1);
            }

            $company = null;
            $longDep = $course->getLongDep();
            $latDep = $course->getLatDep();
            $longArr = $course->getLongArr();
            $latArr = $course->getLatArr();
            $driver = $course->getDriver();
            $client = $course->getClient();
            if ($callc && $client !== null) {
                // API callc/course/new
                $company = $client->getCustomerCompany();
                /** @var PersistentCollection $tarifCollection*/
                $tarifCollection = $company->getCompanyTarif();

                $tarif = $tarifCollection->first();
                $idTypeVehicle = $tarif->getTypeVehiculeTarif()->getId();
                $this->courseManager->setAttr($course, 'company', $company);
                $course->setCourseStatus(Course::STATUS_CREATED_BY_CALLC);
            } elseif ($driver !== null) {
                // API course/new
                $company = $driver->getDriverCompany();
                $this->courseManager->setAttr($course, 'company', $company);
                $idTypeVehicle = $driver->getVehicle()->getTypeVehicule()->getId();
            } elseif (!$depDate) {
                return array(
                    'status' => 0,
                    'courseId' => 0,
                    'response' => 'Driver or departure date are missing',
                );
            }
            else {
                // API course/command
                $aDetailsTarifCompany = array();
                $course->setApplicableTarif(null);
                $course->setCourseStatus(Course::STATUS_COMMAND);
                // Get the closed Company.
                $companyRepository = $this->companyManager->getRepository();

                $companyCustomerId = null;
                if ($course->getClient() !== null) {
                    if ($course->getClient()->getCustomerCompany() != null) {
                        $companyCustomer = $course->getClient()->getCustomerCompany();
                        $companyCustomerId = $course->getClient()->getCustomerCompany()->getId();
                    }
                }
                // Check tarif forfait
                $resultCompanyForfait = $this->checkForfait($longDep, $latDep, $longArr, $latArr, $companyCustomerId,
                    $idTypeVehicle);
                if (count($resultCompanyForfait)) {
                    $aDetailsTarif = $this->getDetailsTarif($course, $resultCompanyForfait, true);
                    $company_id = $aDetailsTarif['company'];
                    $company = $this->companyManager->getRepository()->findById($company_id);
                    $currency = $company[0]->getFormat()->getcurrencyCode() ;

                    $aDetailsTarifCompany[] = array(
                        'forfait' => 1,
                        'companyId' => $aDetailsTarif['company'],
                        'tarifId' => 0,
                        'tarifHT' => $aDetailsTarif['tarifTotalHT'],
                        'tarifTTC' => $aDetailsTarif['tarifTotalTTC'],
                        'tarifApproachTime' => $aDetailsTarif['tarifApproachTime'],
                        'estimatedTarif' => $aDetailsTarif['estimatedTarif'],
                        'tarifDetailId' => $aDetailsTarif['id_details'],
                        'command' => 'command',
                        'currency' => $currency,
                        'isForfait' => 1,

                    );
                }

                $params = $this->tarifManager->getDetailsCourse($course, true);

                if ($companyCustomerId && $params !== false) {
                    $aDetailsTarifCompany = $this->commandCalculDetailsTarifPerCompany(
                        $companyCustomer,
                        $params,
                        $aDetailsTarifCompany,
                        $idTypeVehicle
                    );

                    return array(
                        'status' => 1,
                        'courseId' => 0,
                        'course' => $course,
                        'response' => $aDetailsTarifCompany,

                    );
                } else {
                    // No customer id => no vehicle type found
                    $resultCompany = $companyRepository->findByLatLng($course->getLatDep(), $course->getLongDep());
                    if (count($resultCompany)) {
                        /** @var array $params */
                        if ($params !== false) {
                            foreach ($resultCompany as $itemCompany) {
                                $aDetailsTarifCompany = $this->commandCalculDetailsTarifPerCompany(
                                    $itemCompany['id'],
                                    $params,
                                    $aDetailsTarifCompany
                                );
                            }
                        }

                        return array(
                            'status' => 1,
                            'courseId' => 0,
                            'course' => $course,
                            'response' => $aDetailsTarifCompany,

                        );
                    } else {
                        return array(
                            'status' => 0,
                            'courseId' => 0,
                            'response' => 'Company not defined by the customer',
                        );
                    }
                }
            }

            $resultCompanyForfait = $this->checkForfait($longDep, $latDep, $longArr, $latArr, $company->getId(), $idTypeVehicle);
            if (count($resultCompanyForfait)) {
                $aDetailsTarif = $this->getDetailsTarif($course, $resultCompanyForfait);

                if ($aDetailsTarif['status']) {
                    $course->setCourseDetailsTarif($aDetailsTarif['response']);
                } else {
                    return array(
                        'status' => 0,
                        'courseId' => 0,
                        'response' => 'Probleme Tarif details.',
                    );
                }

                $tarifCourse = $course;

            } else {

                if ($idTypeVehicle) {
                    /** @var TypeVehicule $tarifCourseByTypeVehicle */
                    $tarifCourseByTypeVehicle = $this->vehicleTypeManager->loadObject($idTypeVehicle);
                    if ($tarifCourseByTypeVehicle->getTarifTypeVehicle() instanceof Tarif) {
                        $idTarif = $tarifCourseByTypeVehicle->getTarifTypeVehicle()->getId();
                        $tarifCourse = $this->tarifManager->getTarifCourse(array(), $course, $idTarif);
                    } else {
                        $tarifCourse = $this->tarifManager->getTarifCourse(array(), $course);
                    }

                } else {

                    $tarifCourse = $this->tarifManager->getTarifCourse(array(), $course);
                }
            }

            return array(
                'status' => 1,
                'courseId' => 1,
                'response' => $tarifCourse,
            );
        } catch (\Exception $e) {
            return array(
                'status' => 0,
                'courseId' => 0,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * Get total courses by driver
     */
    public function getCoursesByDriverId ($driver_id)
    {
        if (!$driver_id)
            return false;

        $courses = $this->repository->findCoursesByDriverId($driver_id);

        return count($courses);
    }

    /**
     * Get total courses CA by driver
     */

    public function getCoursesCaByDriverId ($driver_id)
    {
        if (!$driver_id)
            return false;
        $courses = $this->repository->findCoursesCaByDriverId($driver_id);
        //$coursesCAbidon = $courses / 1.20  ;
        $coursesCAbidon = 0 ;
        return round($coursesCAbidon,0);
    }

    /**
     * Get total courses by customer
     */
    public function getCoursesByCustomerId ($customer_id)
    {
        if (!$customer_id)
            return false;

        $courses = $this->repository->findCoursesByCustomerId($customer_id);

        return count($courses);
    }


    /**
     * Get total courses by customer
     */
    public function getStatusByRace ($courseId)
    {
        if (!$courseId)
            return false;

        $courses = $this->repository->findStatusByCourse($courseId);

        return $courses;
    }


    /**
     * Get total courses by customer
     *
     * @param int $customerId
     *
     * @return bool|int
     */
    public function getCoursesCaByCustomerId ($customerId)
    {
        if (!$customerId) {
            return false;
        }

        $courses = $this->repository->findCoursesCaByCustomerId($customerId);

        return count($courses);
    }

    /**
     * updateCourse
     *
     * @param array $aData
     *
     * @return Course
     */
    public function updateCourse (array $aData)
    {
        $course = $this->courseManager->loadCourse($aData['course_id']);
        // get the company of the driver and assign it to the course
        $driver = $this->userManager->findUserBy(array('id' => $aData['driver_id']));
        $course->setCompany($driver->getDriverCompany());
        $course->setDriver($driver);
        $this->courseManager->save($course);

        return $course;
    }

    public function getCourseByIdByDepartureDateTime ($course_id, $driver_id)
    {
        $getCourse = $this->repository->getCourseByIdByDepartureDateTime($course_id, $driver_id);

        if ($getCourse) {

            return array(
                'time' => sprintf("%02d", $getCourse->h) . ':' . sprintf("%02d", $getCourse->i) . ':' . sprintf("%02d", $getCourse->s),
            );
        }

        return false;
    }

    /**
     * @param float $longDep
     * @param float $latDep
     * @param float $longArr
     * @param float $latArr
     * @param int $company
     * @param int $typeVehicle
     * @return array
     */
    private function checkForfait ($longDep, $latDep, $longArr, $latArr, $company, $typeVehicle = null)
    {
        $resultCompanyForfait = $this->om->getRepository('CABCourseBundle:Forfait')->getForfaitByLongAndLat(
            $longDep,
            $latDep,
            $longArr,
            $latArr,
            $company,
            $typeVehicle
        );
        return $resultCompanyForfait;
    }

    /**
     * @param Course $course
     * @param $resultCompanyForfait
     * @return array
     */
    private function getDetailsTarif (Course $course, $resultCompanyForfait, $command = false)
    {
        $forfait = $this->om->getRepository('CABCourseBundle:Forfait')->find($resultCompanyForfait[0]['id']);
        if (!$command) {
            $company = $forfait->getCompany();
            $course->setCompany($company);
            // for create course from app phone
            $course->setApplicableTarif(null);
            $course->setPriceHT(number_format($forfait->getTarifHT(), 0, '.', ''));
            $course->setPriceTTC(number_format($forfait->getTarifTTC(), 0, '.', ''));
        }

        // Details tarif
        $detailsTarif = array(
            'tarifTotalHT' => number_format($forfait->getTarifHT(), 0, '.', ''),
            'tarifTotalTTC' => number_format($forfait->getTarifTTC(), 0, '.', ''),
            'tarifHT' => number_format($forfait->getTarifHT(), 0, '.', ''),
            'tarifApproachTime' => number_format(0, 0, '.', ''),
            'estimatedTarif' => number_format(0, 0, '.', ''),
            'isForfait' => 1,
        );

        $aDetailsTarif = $this->tarifManager->saveDetailsTarif($detailsTarif);

        if ($command) {
            /** @var DetailsTarif $oDetailsTarif */
            $oDetailsTarif = $aDetailsTarif['response'];
            $detailsTarif['id_details'] = $oDetailsTarif->getId();
            $detailsTarif['company'] = $forfait->getCompany()->getId();

            return $detailsTarif;
        } else {
            return $aDetailsTarif;
        }
    }

    private function getDetailsTarifNotforfait (array $detailsTarif)
    {
        // Details tarif
        $detailsTarif = array(
            'tarifTotalHT' => number_format($detailsTarif['price_total'], 0, '.', ''),
            'tarifTotalTTC' => number_format($detailsTarif['price_total_ttc'], 0, '.', ''),
            'tarifHT' => number_format($detailsTarif['price_total'], 0, '.', ''),
            'tarifApproachTime' => number_format($detailsTarif['details']['departure']['priceFromCompanyToDepAddr'], 0, '.', ''),
            'estimatedTarif' => number_format($detailsTarif['estimated_tarif'], 0, '.', ''),
        );

        $aDetailsTarif = $this->tarifManager->saveDetailsTarif($detailsTarif);

        /** @var DetailsTarif $oDetailsTarif */
        $oDetailsTarif = $aDetailsTarif['response'];
        $detailsTarif['id_details'] = $oDetailsTarif->getId();

        return $detailsTarif;
    }

    /**
     * @param int $companyId
     * @param array $params
     * @param array $aDetailsTarifCompany
     * @param null $idTypeVehicle
     *
     * @return array
     */
    private function commandCalculDetailsTarifPerCompany ($companyId, array $params, array $aDetailsTarifCompany,
                                                          $idTypeVehicle = null
    ){
        $params['companyId'] = $companyId;
        //get the id tarif from type vehicle
        if ($idTypeVehicle) {
            /** @var TypeVehicule $tarifCourseByTypeVehicle */
            $tarifCourseByTypeVehicle = $this->vehicleTypeManager->loadObject($idTypeVehicle);
            if ($tarifCourseByTypeVehicle->getTarifTypeVehicle() instanceof Tarif) {
                $idTarif = $tarifCourseByTypeVehicle->getTarifTypeVehicle()->getId();
                $tarifCompany = $this->tarifManager->getTarifCourse($params, null, $idTarif);
            } else {
                $tarifCompany = $this->tarifManager->getTarifCourse($params);
            }

        } else {
            $tarifCompany = $this->tarifManager->getTarifCourse($params);
        }

        $detailsTarif = $this->getDetailsTarifNotforfait($tarifCompany);

        // changement du à un probleme Json APi course/command
        $idcompany = $companyId->getId();

        $currency = $companyId->getFormat()->getcurrencyCode() ;

        //save the details tarifs for each company
        $aDetailsTarifCompany[] = array(
            'forfait' => 0,
            'companyId' => $idcompany,
            'tarifId' => $tarifCompany['tarif_id'],
            'tarifHT' => $detailsTarif['tarifTotalHT'],
            'tarifTTC' => $detailsTarif['tarifTotalTTC'],
            'tarifApproachTime' => $detailsTarif['tarifApproachTime'],
            'estimatedTarif' => $detailsTarif['estimatedTarif'],
            'tarifDetailId' => $detailsTarif['id_details'],
            'command' => 'command',
            'currency' => $currency,
            'isForfait' => 0,
        );
        return $aDetailsTarifCompany;
    }
}