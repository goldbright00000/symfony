<?php

namespace CAB\ApiBundle;

use CAB\ApiBundle\DependencyInjection\Security\Factory\WsseFactory;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class CABApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');

        // Tell the security context about our factory
        $extension->addSecurityListenerFactory(new WsseFactory());
    }
}
