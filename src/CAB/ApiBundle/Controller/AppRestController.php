<?php

namespace CAB\ApiBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Response;
/**
 * APP Rest API
 */
class AppRestController extends FOSRestController {

    /**
     * Get a app timeaccept.
     *
     * @ApiDoc(
     *   section="App",
     *   resource = true,
     *   description = "Get configarution time reponse driver maximium",
     *   output = "CAB\CourseBundle\TimeAcceptDriver",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     * @Annotations\Get("/timeacceptdriver/{id}", name="get_app_time", options={ "method_prefix" = false })
     * @Annotations\View()
     * @param $id
     * @return mixed
     *
     * @throws NotFoundHttpException when id not exist/found
     */
    public function getAppAction($id) {

        $typeRepository = $this->getDoctrine()->getManager()->getRepository('CABCourseBundle:TimeAcceptDriver')->find($id);

        if (!$typeRepository)
            return array('message' => 'Id est vide');

        return $typeRepository;
    }

    /**
     * Get datetime on the server fot created WSSE.
     *
     * @ApiDoc(
     *   section="App",
     *   resource = true,
     *   description = "dateTimeGMT",
     *
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     * @Annotations\Get("/datetime/{id}", name="get_app_datetime", options={ "method_prefix" = false })
     * @Annotations\View()
     * @param $id
     * @return mixed
     *
     * @throws NotFoundHttpException when id not exist/found
     */
    public function getDatetimeAction($id) {
        date_default_timezone_set("GMT");
        $result = new \DateTime();

        if (false === $result) {
            $view = $this->view(
                    array(
                'status' => Response::HTTP_BAD_REQUEST,
                    ), Response::HTTP_BAD_REQUEST
            );
        } else {
            $result= array(
            'status' => Response::HTTP_OK,
            'datetime' => $result);
        return $this->view($result, Response::HTTP_OK);
        }
    }

}
