<?php

namespace CAB\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use CAB\CourseBundle\Entity\Aricle;
use CAB\CourseBundle\Entity\Faq;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;

/**
 * Aricle Rest API
 */
class ArticleRestController extends FOSRestController {

    /**
     * Get an article,
     *
     * @ApiDoc(
     *   section="Article",
     *   resource = true,
     *   description = "Gets an article for a given id, if a string is given it will search for matched slug",
     *   output = "CAB\ArticleBundle\Entity\Article",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\Get("/article/{mixed}", name="get_article", options={ "method_prefix" = false })
     * @Annotations\View(templateVar="article")
     *
     * @param int|string   $mixed      the article id or slug
     *
     * @return array
     *
     * @throws NotFoundHttpException when article not exist/found
     */
    public function getArticleAction($mixed) {
        return $this->getArticleOr404($mixed);
    }

    /**
     * Fetch the Article or throw a 404 exception.
     *
     * @param mixed $mixed
     *
     * @return Article
     *
     * @throws NotFoundHttpException
     */
    protected function getArticleOr404($mixed) {
        if (!($article = $this->container->get('cab_api.article.handler')->get($mixed))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $mixed));
        }

        return $article;
    }
    
    
    

}
