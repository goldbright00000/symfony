<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/10/2015
 * Time: 17:41
 */

namespace CAB\ApiBundle\Controller;

use CAB\ApiBundle\Exception\InvalidFormException;
use CAB\CallcBundle\Entity\Callc;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\VehiculeAffect;
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class CallcRestController extends FOSRestController
{

    /**
     * Post callc new
     *
     * example:
     * id_course:6499
     * @Post("/callc/new", name="callc_new", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Callc",
     *  resource=false,
     *  authentication=false,
     *  description="Post callc new",
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=false, "description"="Id of course."},
     *      {"name"="agent_id", "dataType"="integer", "required"=true, "description"="Id of Agent to call."},
     *      {"name"="CallDuration", "dataType"="float", "required"=true, "description"="Duration for the call"},
     *      {"name"="DatePickUp", "dataType"="float", "required"=false, "description"="DatePickUp."},
     *      {"name"="CallSubject", "dataType"="text", "required"=true, "description"="Subjet for the call"},
     *      {"name"="CreatedAt", "dataType"="datetime", "required"=true, "description"="CreatedAt"},
     *  },
     *
     * statusCodes={
     *         200="Returned when successful",
     *         404={
     *           "Returned when the course is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function postCallcNewAction(Request $request)
    {
        $callcHandler 	= $this->container->get('cab_api.callc_handler');
        $data 			= json_decode($request->getContent(), true);
        $getResult 		= $callcHandler->postCallc($data);

        if (false === $getResult) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
            ), Response::HTTP_BAD_REQUEST);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_CREATED
            ), Response::HTTP_CREATED);
        }

        return $this->handleView($view);
    }



    /**
     * Get Course details for callc
     *
     * example:
     * id_course:6499
     * @Get("/callc/details/{id_course}", name="callc_get_details_course", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Callc",
     *  resource=false,
     *  authentication=false,
     *  description="Get Course details for callc",
     *  parameters={
     *      {"name"="id_course", "dataType"="integer", "required"=false, "description"="Id of course for Callc"}
     *  },
     *
     * statusCodes={
     *         200="Returned when successful",
     *         404={
     *           "Returned when the course is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCourseForCallcAction($id_course)
    {
        $view = $this->view(array(
            'status' => Response::HTTP_BAD_REQUEST
        ), Response::HTTP_BAD_REQUEST);
        $CourseManager = $this->get('cab.course_manager');
        $course = $CourseManager->loadCourse($id_course);

        if (!is_object($course)) {
            throw $this->createNotFoundException();
        }
        /*
        if($course->getCourseStatus() != 18) {

            $view = $this->view(array('Error' => 'Course don\'t have a validated status. Tha validated status is 18 but the status of this course is : ' . $course->getCourseStatus()),
                Codes::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }
        */
        $driverSearched = null;
        $vehicleCourse = [];
        $resultGeo = [
            'time' => '',
            'speed' => '',
            'latitude' => '',
            'longitude' => '',
        ];
        $forfait = false;
        $course_details_tarif = null;
        $course_estimated = null;
        $indicated_depart = '';
        $indicated_arrival = '';
        $indicated_message = '';
        $currency = 'EUR';
        $ticket = array();
        $priceview = false;
        $comp = null;

        if ($course->getClient() !== null) {
            $client['id'] = $course->getClient()->getid();
            $client['first_name'] = $course->getClient()->getfirstName();
            $client['last_name'] = $course->getClient()->getlastName();
            $client['phone_mobile'] = $course->getClient()->getphoneMobile();
            $client['phone_home'] = $course->getClient()->getphoneHome();
            $client['mail'] = $course->getClient()->getEmail();
        }

        if ($course->getCompany() !== null) {
            $comp = $course->getCompany()->getId();
            if ($course->getCompany()->getFormat() !== null) {
                $currency = $course->getCompany()->getFormat()->getcurrencyCode();
            }
        }

        if ($course->getDriver() !== null) {

            $driverCourse = $course->getDriver();
            $driverSearched['first_name'] = $course->getDriver()->getfirstName();
            $driverSearched['last_name'] = $course->getDriver()->getLastName();
            $driverSearched['phone_mobile'] = $course->getDriver()->getPhoneMobile();
            $driverSearched['id'] = $course->getDriver()->getId();
            $vehicleCollections = $driverCourse->getUserAffect();
            /** @var VehiculeAffect $ladtAffetation */
            $ladtAffetation = $vehicleCollections->last();

            $vehicleCourse = [
                'id' => $ladtAffetation->getVehicule()->getId(),
                'name'=> $ladtAffetation->getVehicule()->getVehiculeName(),
                'immatriculation' => $ladtAffetation->getVehicule()->getLicensePlateNumber(),
            ];

            $driverGeo = $this->get('cab_api.gelocalisation.handler')->getDriverById($driverCourse->getId());

            $error  = false;
            if (!$course || !$driverCourse || !$course->getLatDep() || !$course->getLongDep()) {
                $error = true;
            }

            if (!$error) {
                if (count($driverGeo)) {
                    // call google api to grab the time
                    $driverCoords = urlencode($driverGeo[0]->getLatitude() . ',' . $driverGeo[0]->getLongitude());
                    $courseCoords = urlencode($course->getLatArr() . ',' . $course->getLongArr());
                    $getCoords = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $driverCoords . "&destinations=" . $courseCoords . "&key=" . $this->container->getParameter('server_key');
                    $getCoords = json_decode(file_get_contents($getCoords));

                    $resultGeo['time'] = $getCoords->status == 'OK' ? $getCoords->rows[0]->elements[0]->duration->text : 0;
                    $resultGeo['speed'] = $driverGeo[0]->getSpeed();
                    $resultGeo['latitude'] = $driverGeo[0]->getLatitude();
                    $resultGeo['longitude'] = $driverGeo[0]->getLongitude();
                }

            }

        }
        if ($course->getCourseDetailsTarif() !== null) {
            $course_details_tarif = $course->getCourseDetailsTarif()->getId();
            $course_estimated = $course->getCourseDetailsTarif()->getEstimatedTarif();

            $course_estimated_TTC = $course->getCourseDetailsTarif()->getTarifTotalTTC();
            $forfait = $course->getCourseDetailsTarif()->getIsForfait();
        } else {
            $course_details_tarif = " ";
            $course_estimated = " ";

        }
        if ($course->getIndicatorStartedAddress() !== null) {
            $indicated_depart = $course->getIndicatorStartedAddress();
        }

        if ($course->getindicatorArrivalAddress() !== null) {
            $indicated_arrival = $course->getindicatorArrivalAddress();
        }
        if ($course->getCourseDetailsTarif() !== null) {
            $course_estimated_HT = $course->getCourseDetailsTarif()->getTarifHT();
        } else {
            $course_estimated_HT = $course->getPriceHT();
            $course_estimated_TTC = $course->getPriceTTC();
        }
        if ($course->getpriceHt() !== null) {
            $priceHT = $course->getpriceHt();
        } else {
            $priceHT = "";
        }
        if ($course->getpriceTTC() !== null) {
            $priceTTC = $course->getpriceTTC();
        } else {
            $priceTTC = "";
        }
        if ($course->getTicket() !== null && $course->getTicket()->first() != false) {
	        if($course->getTicket()->first() != false)
	        {
		        $ticket['id'] = $course->getTicket()->first()->getId();
		        $ticket['user'] = $course->getTicket()->first()->getlastUser();
	        }
	        else{
		        $ticket['id'] = $course->getTicket()->getId();
		        $ticket['user'] = $course->getTicket()->getlastUser();
	        }


        } else {
            $ticket['id'] = '';
            $ticket['user'] = '';
        }

        if ($course->getCourseTime() !== null) {
            $course_time = $course->getCourseTime();
        } else {
            $course_time = '';
        }

        if ($course->getserviceTransportCourses() !== null) {

            foreach ( $course->getserviceTransportCourses() as $v ) {
                $priceview = $v->getPriceView();

            }
        }


        $result = array(
            'id' => $course->getId(),
            'client' => $client,
            'company' => array('id' => $comp),
            'driver' => $driverSearched,
            'course_details_tarif' => array('id' => $course_details_tarif, "estimated_tarif" => $course_estimated),
            'started_address' => $course->getStartedAddress(),
            'indicator_start_adresse' => $indicated_depart,
            'arrival_address' => $course->getArrivalAddress(),
            'indicator_arrival_adresse' => $indicated_arrival,
            'message' => $indicated_message,
            'departure_date' => $course->getDepartureDate()->format('d/m/Y'),
            'departure_time' => $course->getDepartureTime()->format('H:i'),
            'person_number' => $course->getpersonNumber(),
            'priceHT' => $course_estimated_HT,
            'priceTTC' => $course_estimated_TTC,
            'course_status' => $course->getcourseStatus(),
            'currency' => $currency,
            'forfait' => $forfait,
            'ticket' => $ticket,
            'course_time' => $course_time,
            'long_dep' => $course->getlongDep(),
            'lat_dep' => $course->getlatDep(),
            'long_arr' => $course->getlongArr(),
            'lat_arr' => $course->getlatArr(),
            'PriceView' => $priceview,
            'vehicle' => $vehicleCourse,
            'remaining_temp' => $resultGeo['time'] ,
            'speed' => $resultGeo['speed'] ,
            'actual_latitude' => $resultGeo['latitude'],
            'actual_longitude' => $resultGeo['longitude'],
            'currency' => $course->getCompany()->getFormat()->getCurrencyCode(),
        );



        $view = $this->view($result, Response::HTTP_OK);
        return $this->handleView($view);
        /*return array(
            'status' => Codes::HTTP_OK,
            'result' => $result,
        );*/
    }


    /**
     * Creates a new course from Callc.
     *
     *example:
     *
     * {
     *
     * "client" : 31,
     *
     * "startedAddress" : "Aéroport Toulouse Blagnac, Blagnac, France",
     *
     * "longDep" :"1.367682",
     *
     * "latDep" :"43.629386",
     *
     * "arrivalAddress" : "Balma-Gramont, Balma, France","longArr" :"1.482260",
     *
     * "longArr" :"1.48170800",
     *
     * "latArr" : "43.62856900",
     *
     * "departureDate" : "2017-11-28 10:00:00",
     *
     * "personNumber" : 1
     *
     * }
     *
     * @ApiDoc(
     *   section="Callc",
     *   resource = true,
     *   description = "Creates a new course from Callc.",
     *   parameters={
     *  {"name"="client", "dataType"="integer", "required"=true, "description"="client Id"},
     *  {"name"="startedAddress", "dataType"="string", "required"=true, "description"="Course departure address"},
     *  {"name"="longDep", "dataType"="float", "required"=true, "description"="Departure address longitude"},
     *  {"name"="latDep", "dataType"="float", "required"=true, "description"="Departure address latitude"},
     *  {"name"="arrivalAddress", "dataType"="string", "required"=true, "description"="Course arrival address"},
     *  {"name"="longArr", "dataType"="float", "required"=true, "description"="Arrival address longitude"},
     *  {"name"="latArr", "dataType"="float", "required"=true, "description"="Arrival address latitude"},
     *  {"name"="departureDate", "dataType"="string", "required"=true, "description"="departure date time Y-m-d H:i:s"},
     *  {"name"="personNumber", "dataType"="integer", "required"=true, "description"="Person number for the course"}
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Post("/callc/course/new", name="callc_new_course", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Details", "Public"}
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface |View
     */
    public function postCourseForCallcAction(Request $request)
    {
        try {
            $jsonData = json_decode($request->getContent(), true); // "true" to get an associative array
            $jsonData['callc'] = 1;
            /** @var array $result */
            $result = $this->get('cab_api.course.handler')->post($jsonData);
            $headers = array(
                'status' => Response::HTTP_CREATED,
                'Content-Type' => 'application/json',
            );
            $view = $this->view(
                array(
                    'status' => Response::HTTP_CREATED,
                    'result' => $result,
                    ),
                Response::HTTP_CREATED,
                $headers
            );

            return $this->handleView($view);
        } catch (InvalidFormException $exception) {
            $headers = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'Content-Type' => 'application/json',
            );
            $view = $this->view(
                array(
                    'status' => 0,
                    'response' => $exception->getMessage(),
                ),
                Response::HTTP_BAD_REQUEST,
                $headers
            );
            return $this->handleView($view);
        }
    }

    /**
     * Field when register
     *
     * @View(serializerGroups={"Default","Details", "Public"})
     *
     *
     * @Get("/callc/field", name="callc_get_field", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *   section="Callc",
     *   resource = true,
     *   description = "field on the callc add race.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   },
     *   parameters={
     *     {"name"="name", "dataType"="string", "required"=false, "description"="user's username"},
     *     {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     *     {"name"="phone", "dataType"="string", "required"=false, "description"="phone"},
     *     {"name"="iduser", "dataType"="string", "required"=false, "description"="iduser"},
     *
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function getCallcFielAction(Request $request) {

        $name = $request->query->get('name');
        $email = $request->query->get('email');
        $phone = $request->query->get('phone');
        $iduser = $request->query->get('iduser');

        $checkname = $this->get('fos_user.user_manager')->findUserBy(array('lastName' => $name));
        $checkemail = $this->get('fos_user.user_manager')->findUserByEmail($email);
        $checkphone = $this->get('fos_user.user_manager')->findUserBy(array('phoneMobile' => $phone));
        $checkiduser = $this->get('fos_user.user_manager')->findUserBy(array('id' => $iduser));

        if (!empty($name)) {
            if (null === $checkname) {
                $reponse = '0';
            } else {
                $reponse = array(
                    "id_user" => $checkname->getid(),
                    "firstname" => $checkname->getfirstName(),
                    "lastname" => $checkname->getlastName(),
                    "email" => $checkname->getemail(),
                    "address" => $checkname->getaddress(),
                    "latadress" => $checkname->getlatUser(),
                    "lngadress" => $checkname->getlngUser(),
                    "adressComplement" => $checkname->getaddressComplement(),
                    "phoneMobile" => $checkname->getphoneMobile()
                );
            }
        }
        if (!empty($email)) {
            if (null === $checkemail) {
                $reponse = '0';
            } else {
                $reponse = array(
                    "id_user" => $checkemail->getid(),
                    "firstname" => $checkemail->getfirstName(),
                    "lastname" => $checkemail->getlastName(),
                    "email" => $checkemail->getemail(),
                    "address" => $checkemail->getaddress(),
                    "latadress" => $checkemail->getlatUser(),
                    "lngadress" => $checkemail->getlngUser(),
                    "adressComplement" => $checkemail->getaddressComplement(),
                    "phoneMobile" => $checkemail->getphoneMobile()
                );
            }
        }
        if (!empty($phone)) {
            if (null === $checkphone) {
                $reponse = '0';
            } else {
                $reponse = array(
                    "id_user" => $checkphone->getid(),
                    "firstname" => $checkphone->getfirstName(),
                    "lastname" => $checkphone->getlastName(),
                    "email" => $checkphone->getemail(),
                    "address" => $checkphone->getaddress(),
                    "latadress" => $checkphone->getlatUser(),
                    "lngadress" => $checkphone->getlngUser(),
                    "adressComplement" => $checkphone->getaddressComplement(),
                    "phoneMobile" => $checkphone->getphoneMobile()
                );
            }
        }
        if (!empty($iduser)) {
            if (null === $checkiduser) {
                $reponse = '0';
            } else {
                $reponse = array(
                    "id_user" => $checkiduser->getid(),
                    "firstname" => $checkiduser->getfirstName(),
                    "lastname" => $checkiduser->getlastName(),
                    "email" => $checkiduser->getemail(),
                    "address" => $checkiduser->getaddress(),
                    "latadress" => $checkiduser->getlatUser(),
                    "lngadress" => $checkiduser->getlngUser(),
                    "adressComplement" => $checkiduser->getaddressComplement(),
                    "phoneMobile" => $checkiduser->getphoneMobile()
                );
            }
        }
        $view = $this->view(array(
            'reponse' => $reponse,
        ), Response::HTTP_OK);
        return $this->handleView($view);
    }

    /**
     * Cancel course by callc
     * example:
     *
     * {"course_id": 1, "agent_id": 22}
     * @ApiDoc(
     *   section="Callc",
     *   resource = true,
     *   description = "Cancel course by callc",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="agent_id", "dataType"="integer", "required"=true, "description"="Id of Agent who cancelled the course."},
     *  },
     * @Annotations\Put("/callc/course/cancel", name="callc_cancel_booking", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function callcCancelCourseAction(Request $request)
    {
        $aData = json_decode($request->getContent(), true);

        $course = $this->get('cab.course_manager')->loadCourse($aData['course_id']);
        $oAgent = $this->get('fos_user.user_manager')->findUserBy(array('id' => $aData['agent_id']));
        $result = $this->get('cab.course_manager')->cancelCourseByAgent($course, $oAgent);

        if ($course->getDriver()) {
            $oDriver = $course->getDriver();
            // send push notification to driver
            $messagePush = 'THIS RACE IS CANCELED';
            $data = array(
                'type_notification' => 'driver_notification_course',
                'subject_notification' => 'A course is canceled by a Callc agent',
                'type' => '0',
                'alert' => $messagePush,
                'id_course' => $aData['course_id'],
                //'sound' => 'ringingbell.mp3',
            );
            $this->get('cab_course.push_notification.push')->getServiceByOs(
                $oDriver->getDevices()->toArray(),
                $messagePush,
                $data
            );
        }

        if ($result['status'] == 1) {
            $status = Response::HTTP_OK;
            $response = $result['response'];
        } else {
            $status = Response::HTTP_BAD_REQUEST;
            $response = $result['response'];
        }
        $view = $this->view(
            array(
                'status' => $status,
                'response' => $response,
                'course' => $course,
            ),
            $status
        );

        return $this->handleView($view);
    }

    /**
     * Edit course by Callc
     * example:
     *
     * {
     * "client_id": 1429,
     * "initial_course_date":  "11-01-2018",
     * "initial_course_time": "10:00:00",
     * "departureDate": "28-11-2017",
     * "departureTime": "10:00:00",
     * "startedAddress": "Aéroport Toulouse Blagnac, Blagnac, France",
     * "indicatorStartedAddress": "Terminal B",
     * "arrivalAddress": "Balma-Gramont, Balma, France",
     * "indicatorArrivalAddress": "",
     * "course_notes_particulieres": ""
     * }
     *
     *
     * @ApiDoc(
     *   section="Callc",
     *   resource = true,
     *   description = "Edit course by Callc",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   },
     *
     *  parameters={
     *      {"name"="client_id", "dataType"="integer", "required"=true, "description"="Id of client."},
     *      {"name"="initial_course_date", "dataType"="string", "required"=true, "description"="The initial date of the course. Format : d-m-Y"},
     *      {"name"="initial_course_time", "dataType"="string", "required"=true, "description"="The initial time of the course. Format : H:i:s"},
     *      {"name"="departureDate", "dataType"="string", "required"=false, "description"="Date of departure date"},
     *      {"name"="departureTime", "dataType"="string", "required"=false, "description"="Time of departure date"},
     *      {"name"="startedAddress", "dataType"="string", "required"=false, "description"="Departure address"},
     *      {"name"="indicatorStartedAddress", "dataType"="string", "required"=false, "description"="Departure address info"},
     *      {"name"="arrivalAddress", "dataType"="string", "required"=false, "description"="Arrival address"},
     *      {"name"="indicatorArrivalAddress", "dataType"="string", "required"=false, "description"="Arrival address info"},
     *      {"name"="course_notes_particulieres", "dataType"="string", "required"=false, "description"="Note from the client/agent"},
     *  },
     *     )
     * @Annotations\Put("/callc/course/update", name="callc_update_course", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function callcUpdateCourseAction(Request $request)
    {
        $getJsonData = json_decode($request->getContent(), true);
var_dump($getJsonData);
        exit();

        $courseResult = $this->get('cab_api.course.handler')->putCallc($getJsonData);
        $view = $this->view(
            $courseResult,
            $courseResult['status']
        );

        return $this->handleView($view);
    }

    /**
     * Get Course details by numéro command sncf for callc
     *
     * example:
     * command_sncf:2017-12-15-00058-963
     * @Get("/callc/course-sncf/{command_sncf}", name="callc_get_details_course_sncf", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Callc",
     *  resource=false,
     *  authentication=false,
     *  description="Get Course details for callc",
     *  parameters={
     *      {"name"="command_sncf", "dataType"="integer", "required"=false, "description"="N° command SNCF"}
     *  },
     *
     * statusCodes={
     *         200="Returned when successful",
     *         404={
     *           "Returned when the course is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCourseByCommandSncfAction($command_sncf)
    {
        $view = $this->view(array(
            'status' => Response::HTTP_BAD_REQUEST
        ), Response::HTTP_BAD_REQUEST);
        $CourseManager = $this->get('cab.course_manager');

        /** @var Course $course */
        $course = $CourseManager->findByCommandSncf($command_sncf);

        if (!is_object($course)) {
            $view = $this->view(array('Error' => 'Any course exists for the command SNCF : ' . $command_sncf),
                                Response::HTTP_BAD_REQUEST);
            return $this->handleView($view);
        }

        $driverSearched = null;
        $vehicleCourse = [];
        $resultGeo = [
            'time' => '',
            'speed' => '',
            'latitude' => '',
            'longitude' => '',
        ];
        $forfait = false;
        $course_details_tarif = null;
        $course_estimated = null;
        $indicated_depart = '';
        $indicated_arrival = '';
        $indicated_message = '';
        $currency = 'EUR';
        $ticket = array();
        $priceview = false;
        $comp = null;

        if ($course->getClient() !== null) {
            $client['id'] = $course->getClient()->getid();
            $client['first_name'] = $course->getClient()->getfirstName();
            $client['last_name'] = $course->getClient()->getlastName();
            $client['phone_mobile'] = $course->getClient()->getphoneMobile();
            $client['phone_home'] = $course->getClient()->getphoneHome();
            $client['mail'] = $course->getClient()->getEmail();
        }

        if ($course->getCompany() !== null) {
            $comp = $course->getCompany()->getId();
            if ($course->getCompany()->getFormat() !== null) {
                $currency = $course->getCompany()->getFormat()->getcurrencyCode();
            }
        }

        if ($course->getDriver() !== null) {

            $driverCourse = $course->getDriver();
            $driverSearched['first_name'] = $course->getDriver()->getfirstName();
            $driverSearched['last_name'] = $course->getDriver()->getLastName();
            $driverSearched['phone_mobile'] = $course->getDriver()->getPhoneMobile();
            $driverSearched['id'] = $course->getDriver()->getId();
            $vehicleCollections = $driverCourse->getUserAffect();
            /** @var VehiculeAffect $ladtAffetation */
            $ladtAffetation = $vehicleCollections->last();

            $vehicleCourse = [
                'id' => $ladtAffetation->getVehicule()->getId(),
                'immatriculation' => $ladtAffetation->getVehicule()->getLicensePlateNumber(),
            ];

            $driverGeo = $this->get('cab_api.gelocalisation.handler')->getDriverById($driverCourse->getId());

            $error  = false;
            if (!$course || !$driverCourse || !$course->getLatDep() || !$course->getLongDep()) {
                $error = true;
            }

            if (!$error) {
                if (count($driverGeo)) {
                    // call google api to grab the time
                    $driverCoords = urlencode($driverGeo[0]->getLatitude() . ',' . $driverGeo[0]->getLongitude());
                    $courseCoords = urlencode($course->getLatArr() . ',' . $course->getLongArr());
                    $getCoords = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $driverCoords . "&destinations=" . $courseCoords . "&key=" . $this->container->getParameter('server_key');
                    $getCoords = json_decode(file_get_contents($getCoords));

                    $resultGeo['time'] = $getCoords->status == 'OK' ? $getCoords->rows[0]->elements[0]->duration->text : 0;
                    $resultGeo['speed'] = $driverGeo[0]->getSpeed();
                    $resultGeo['latitude'] = $driverGeo[0]->getLatitude();
                    $resultGeo['longitude'] = $driverGeo[0]->getLongitude();
                }

            }

        }
        if ($course->getCourseDetailsTarif() !== null) {
            $course_details_tarif = $course->getCourseDetailsTarif()->getId();
            $course_estimated = $course->getCourseDetailsTarif()->getEstimatedTarif();

            $course_estimated_TTC = $course->getCourseDetailsTarif()->getTarifTotalTTC();
            $forfait = $course->getCourseDetailsTarif()->getIsForfait();
        } else {
            $course_details_tarif = " ";
            $course_estimated = " ";

        }
        if ($course->getIndicatorStartedAddress() !== null) {
            $indicated_depart = $course->getIndicatorStartedAddress();
        }

        if ($course->getindicatorArrivalAddress() !== null) {
            $indicated_arrival = $course->getindicatorArrivalAddress();
        }
        if ($course->getCourseDetailsTarif() !== null) {
            $course_estimated_HT = $course->getCourseDetailsTarif()->getTarifHT();
        } else {
            $course_estimated_HT = $course->getPriceHT();
            $course_estimated_TTC = $course->getPriceTTC();
        }
        if ($course->getpriceHt() !== null) {
            $priceHT = $course->getpriceHt();
        } else {
            $priceHT = "";
        }
        if ($course->getpriceTTC() !== null) {
            $priceTTC = $course->getpriceTTC();
        } else {
            $priceTTC = "";
        }
	    if ($course->getTicket() !== null && $course->getTicket()->first() != false) {
		    if($course->getTicket()->first() != false)
		    {
			    $ticket['id'] = $course->getTicket()->first()->getId();
			    $ticket['user'] = $course->getTicket()->first()->getlastUser();
		    }
		    else{
			    $ticket['id'] = $course->getTicket()->getId();
			    $ticket['user'] = $course->getTicket()->getlastUser();
		    }


	    } else {
            $ticket['id'] = '';
            $ticket['user'] = '';
        }

        if ($course->getCourseTime() !== null) {
            $course_time = $course->getCourseTime();
        } else {
            $course_time = '';
        }

        if ($course->getserviceTransportCourses() !== null) {

            foreach ( $course->getserviceTransportCourses() as $v ) {
                $priceview = $v->getPriceView();

            }
        }


        $result = array(
            'id' => $course->getId(),
            'client' => $client,
            'company' => array('id' => $comp),
            'driver' => $driverSearched,
            'course_details_tarif' => array('id' => $course_details_tarif, "estimated_tarif" => $course_estimated),
            'started_address' => $course->getStartedAddress(),
            'indicator_start_adresse' => $indicated_depart,
            'arrival_address' => $course->getArrivalAddress(),
            'indicator_arrival_adresse' => $indicated_arrival,
            'message' => $indicated_message,
            'departure_date' => $course->getDepartureDate()->format('d/m/Y'),
            'departure_time' => $course->getDepartureTime()->format('H:i'),
            'person_number' => $course->getpersonNumber(),
            'priceHT' => $course_estimated_HT,
            'priceTTC' => $course_estimated_TTC,
            'course_status' => $course->getcourseStatus(),
            'currency' => $currency,
            'forfait' => $forfait,
            'ticket' => $ticket,
            'course_time' => $course_time,
            'long_dep' => $course->getlongDep(),
            'lat_dep' => $course->getlatDep(),
            'long_arr' => $course->getlongArr(),
            'lat_arr' => $course->getlatArr(),
            'PriceView' => $priceview,
            'vehicle' => $vehicleCourse,
            'remaining_temp' => $resultGeo['time'] ,
            'speed' => $resultGeo['speed'] ,
            'actual_latitude' => $resultGeo['latitude'],
            'actual_longitude' => $resultGeo['longitude'],
        );

        if ($course->getCommandSncf()) {
            $result ['id_command_sncf_course'] = $course->getCommandSncf()->getId();
            $result ['codeBupo'] = $course->getCommandSncf()->getCodeBupo();
            $result ['js'] = $course->getCommandSncf()->getJs();
            $result ['rlt'] = $course->getCommandSncf()->getRlt();
            $result ['commande_sncf'] = $course->getCommandSncf()->getCommandeCourse();
        }


        $view = $this->view($result, Response::HTTP_OK);

        return $this->handleView($view);
    }

}
