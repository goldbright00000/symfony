<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 04/03/2016
 * Time: 23:39
 */

namespace CAB\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\UserBundle\Model\UserInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class PlaningRestController extends FOSRestController {

    /**
     * @View(serializerGroups={"Public"})
     * Get planing details
     * @Get("/planing/{planingId}", name="get_planing", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Planing",
     *  documentation="All planing details",
     *  resource=true,
     *  authentication=true,
     *  description="Get planing details",
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     */
    public function getPlaningAction($planingId)
    {
        //$this->forwardIfNotAuthenticated();
        $oPlaning =  $this->get('cab.planing.manager')->loadObject($planingId);
        
        $view = array(
            'planing' => $oPlaning,
        );

        return $view;
        
        
        //return $oPlaning;
    }

    /**
     * @View(serializerGroups={"Public"})
     * Get Planing list by criteria .
     * @Get("/planings/list/{criteria}/{value}/{optionCriteria}/{valueOptionCriteria}", name="get_list_planing_by_criteria", options={ "method_prefix" = false })
     * @QueryParam(
     *   name="optionCriteria",
     *   requirements="false",
     *   default="null",
     *   description="The second criteria to find the planing",
     * )
     * * @QueryParam(
     *   name="valueOptionCriteria",
     *   requirements="false",
     *   default="null",
     *   description="The value of the second criteria",
     * )
     * @ApiDoc(
     *  section="Planing",
     *  documentation="Get planing list by criteria.",
     *  resource=true,
     *  authentication=true,
     *  description="Get planing list by criteria",
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     * @return array
     */
    public function getPlaningsByCriteriaAction($criteria, $value, $optionCriteria = null, $valueOptionCriteria = null)
    {
        //$this->forwardIfNotAuthenticated();
        if (in_array($criteria, array('start', 'end', 'createdAt', 'updatedAt'))) {
            $aPlaning = array();
            $aDate = explode('-', $value);
            if (count($aDate) < 3) {
                return array(
                    'status' => 0,
                    'response' => 'You should give a date with this format: d-m-Y',
                );
            }
            $checkDate = checkdate($aDate[1], $aDate[0], $aDate[2]);
            if ($checkDate) {
                $aPlaning = $this->get('cab.planing.manager')->getPlaningByDate($criteria, $value, $optionCriteria, $valueOptionCriteria);
            }
        } else {
            $aPlaning = $this->get('cab.planing.manager')->getPlaningsBy($criteria, $value);
        }
        
        $view = array(
            'planing' => $aPlaning,
        );

        return $view;

        //return $aPlaning;
    }
}