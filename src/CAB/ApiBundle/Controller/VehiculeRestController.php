<?php

namespace CAB\ApiBundle\Controller;

use CAB\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Response;

/**
 * Vehicule Rest API
 */
class VehiculeRestController extends FOSRestController {

    /**
     * Get a list of available vehicules.
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "Get a list of available vehicules by user id",
     *   output = "CAB\VehiculeBundle\Entity\Vehicule",
     *   requirements={
     *      {
     *          "name"="driver_id", "dataType"="integer", "requirement"="\d+", "description"="driver id to look for"
     *      }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\Get("/vehicule/user/{driver_id}", name="get_driver_vehicules", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @param int   $driver_id    the driver id
     *
     * @return view
     */
    public function getVehiculesByUserAction($driver_id) {
        $vehiculeHandler = $this->container->get('cab_api.vehicule_handler');
        $getAvailableVehicules = $vehiculeHandler->get($driver_id);

        if (false === $getAvailableVehicules) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
                    ), Response::HTTP_BAD_REQUEST
            );
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_OK,
                'vehicules' => $getAvailableVehicules
                    ), Response::HTTP_OK
            );
        }

        return $this->handleView($view);
    }

    /**
     * Add new vehicule attached to driver. It takes the driver id and vehicule id and optionally km as a json data
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "Add new vehicule attached to driver. It takes the driver id and vehicule id and optionally km as a json data",
     *   output = "CAB\VehiculeBundle\Entity\Vehicule",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *
     * @Annotations\Post("/vehicule/assignment", name="get_vehicule_assignment", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param int   $driver_id    the driver id
     *
     * @return view
     */
    public function postVehiculeAffectAction(Request $request) {
        $vehiculeHandler = $this->container->get('cab_api.vehicule_handler');
        $getJsonData = json_decode($request->getContent(), true); // "true" to get an associative array
        if (isset($getJsonData['number_lic_pager_file']) && !empty($getJsonData['number_lic_pager_file'])):
            $vehicule_img = rand() . '.jpeg';
            $path = __DIR__ . '/../../../../web/uploads/vehicule/affect/' . $vehicule_img;
            if (isset($getJsonData['number_lic_pager_file']) && !empty($getJsonData['number_lic_pager_file'])):
                $img_string = str_replace(' ', '+', $getJsonData['number_lic_pager_file']);
                $image_base64 = base64_decode($img_string);
                file_put_contents($path, $image_base64);
                $getJsonData['number_lic_pager_file'] = $vehicule_img;
            endif;
        endif;

        $vehiculeAffect = $vehiculeHandler->post($getJsonData);
        // AFAIRE
        if (false === $vehiculeAffect) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
                    ), Response::HTTP_BAD_REQUEST
            );
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_CREATED
                    ), Response::HTTP_CREATED
            );
        }

        return $this->handleView($view);
    }

    /**
     * Get a vehicule assignment by user id.
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "Get a vehicule assignment by user id",
     *   output = "CAB\VehiculeBundle\Entity\Vehicule",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\Get("/vehicule/{driver_id}", name="get_vehicule", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @return view
     */
    public function getVehiculesAction($driver_id) {
        $vehiculeHandler = $this->container->get('cab_api.vehicule_handler');
        /** @var array $getVehicule */
        $getVehicule = $vehiculeHandler->getVehicule($driver_id);

        if (false === $getVehicule) {
            $view = $this->view(array(
                'status' => Response::HTTP_NOT_FOUND
                    ), Response::HTTP_NOT_FOUND
            );
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_OK,
                'name' => $getVehicule['name'],
                'km' => $getVehicule['vkm'],
                'license_plate_number' => $getVehicule['licensePlateNumber'],
                'vehicule_id' => $getVehicule['id'],
                    ), Response::HTTP_OK
            );
        }

        return $this->handleView($view);
    }

    /**
     * Get a vehicule id.
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "Get a vehicule id",
     *   output = "CAB\VehiculeBundle\Entity\Vehicule",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\Get("/vehiculelastkm/{vehicule_id}", name="get_vehicule_id", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @return view
     */
    public function getVehiculesIdAction($vehicule_id) {
        $vehiculeHandler = $this->container->get('cab_api.vehicule_handler');
        /** @var array $getVehicule */
        $getVehicule = $vehiculeHandler->getVehiculeById($vehicule_id);
        if (false === $getVehicule) {
            $view = $this->view(array(
                'status' => Response::HTTP_NOT_FOUND
                    ), Response::HTTP_NOT_FOUND
            );
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_OK,
                'vehicule_id' => $getVehicule['id'],
                'km' => $getVehicule['vkm'],
                    ), Response::HTTP_OK);
        }
        return $this->handleView($view);
    }

    /**
     * Post fuel assignment by user id and vehicule
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "POST a vehicule fuel assignment by user id",
     *     parameters={
     *      {"name"="vehicule_id", "dataType"="integer", "required"=true, "description"="vehicule Id"},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="driver Id"},
     *      {"name"="quantity", "dataType"="string", "required"=true, "description"="quantity"},
     *      {"name"="km", "dataType"="string", "required"=true, "description"="km"},
     *      {"name"="amount", "dataType"="string", "required"=true, "description"="amount"},
     *      {"name"="file", "dataType"="string", "required"=true, "description"="Course arrival address"},
     *  },
     *   output = "CAB\VehiculeBundle\Entity\Vehicule",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\Post("/vehicule/fuel/{driver_id}", name="post_vehicule_fuel", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @return view
     */
    public function postVehiculeFuelAction(Request $request) {
        $vehiculeHandler = $this->container->get('cab_api.vehicule_handler');
        $getJsonData = json_decode($request->getContent(), true); // "true" to get an associative array

        $vehiculeAffect = $vehiculeHandler->postFuel($getJsonData);

        if (false === $vehiculeAffect) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
                    ), Response::HTTP_BAD_REQUEST
            );
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_CREATED
                    ), Response::HTTP_CREATED
            );
        }

        return $this->handleView($view);
    }

    /**
     * Post Clean assignment by user id and vehicule
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "POST a vehicule clean assignment by user id",
     *   parameters={
     *      {"name"="vehicule_id", "dataType"="integer", "required"=true, "description"="vehicule Id"},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="driver Id"},
     *      {"name"="interior", "dataType"="string", "required"=true, "description"="interior"},
     *      {"name"="exterior", "dataType"="float", "required"=true, "description"="exterior"},
     *      {"name"="km", "dataType"="string", "required"=true, "description"="km"},
     *      {"name"="amount", "dataType"="string", "required"=true, "description"="amount"},
     *      {"name"="file", "dataType"="string", "required"=true, "description"="Course arrival address"},
     *  },
     *   output = "CAB\VehiculeBundle\Entity\Vehicule",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\Post("/vehicule/clean/{driver_id}", name="get_vehicule_clean", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @return view
     */
    public function postVehiculeCleanAction(Request $request) {
        $vehiculeHandler = $this->container->get('cab_api.vehicule_handler');
        $getJsonData = json_decode($request->getContent(), true); // "true" to get an associative array

        $vehiculeAffect = $vehiculeHandler->postClean($getJsonData);

        if (false === $vehiculeAffect) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
                    ), Response::HTTP_BAD_REQUEST
            );
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_CREATED
                    ), Response::HTTP_CREATED
            );
        }

        return $this->handleView($view);
    }

    /**
     * Post Maintenance assignment by user id and vehicule
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "POST a vehicule maintenance assignment by driver",
     *      parameters={
     *      {"name"="vehicule_id", "dataType"="integer", "required"=true, "description"="vehicule Id"},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="driver Id"},
     *      {"name"="oilchange", "dataType"="string", "required"=true, "description"="oilchange"},
     *      {"name"="oilfilter", "dataType"="float", "required"=true, "description"="oilfilter"},
     *      {"name"="fronttyres", "dataType"="float", "required"=true, "description"="fronttyres"},
     *      {"name"="reartyres", "dataType"="float", "required"=true, "description"="reartyres"},
     *      {"name"="battery", "dataType"="float", "required"=true, "description"="battery"},
     *      {"name"="chassis", "dataType"="float", "required"=true, "description"="chassis"},
     *      {"name"="brokenwindows", "dataType"="float", "required"=true, "description"="brokenwindows"},
     *      {"name"="bulbs", "dataType"="float", "required"=true, "description"="bulbs"},
     *      {"name"="frontbrakepads", "dataType"="float", "required"=true, "description"="frontbrakepads"},
     *      {"name"="rearbrakepads", "dataType"="float", "required"=true, "description"="rearbrakepads"},
     *      {"name"="frontbrakediscs", "dataType"="float", "required"=true, "description"="frontbrakediscs"},
     *      {"name"="rearbrakediscs", "dataType"="float", "required"=true, "description"="rearbrakediscs"},
     *      {"name"="tow", "dataType"="float", "required"=true, "description"="tow"},
     *      {"name"="detail", "dataType"="float", "required"=true, "description"="detail"},
     *      {"name"="km", "dataType"="string", "required"=true, "description"="km"},
     *      {"name"="amount", "dataType"="string", "required"=true, "description"="amount"},
     *      {"name"="file", "dataType"="string", "required"=true, "description"="Course arrival address"},
     *  },
     *   output = "CAB\VehiculeBundle\Entity\Vehicule",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\Post("/vehicule/maintenance/{driver_id}", name="get_vehicule_maintenance", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @return view
     */
    public function postVehiculeMaintnanceAction(Request $request) {
        $vehiculeHandler = $this->container->get('cab_api.vehicule_handler');
        $getJsonData = json_decode($request->getContent(), true); // "true" to get an associative array

        $vehiculeAffect = $vehiculeHandler->postmaintenance($getJsonData);

        if (false === $vehiculeAffect) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
                    ), Response::HTTP_BAD_REQUEST
            );
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_CREATED
                    ), Response::HTTP_CREATED
            );
        }

        return $this->handleView($view);
    }

    /**
     * Get the vehicule type.
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "Get vehicles type",
     *   output = "CAB\VehiculeBundle\Entity\TypeVehicule",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   },
     *   parameters={
     *      {"name"="latitude", "dataType"="float", "required"=true, "description"="Customer latitude"},
     *      {"name"="longitude", "dataType"="float", "required"=true, "description"="Customer longitude"}
     *  }
     * )
     *
     * @Annotations\Post("/vehicule-type", name="get_vehicule_type", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @return view
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Exception
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getVehicleTypeAction(Request $request) {
        $userManager = $this->container->get('cab.cab_user.manager');
        $tarifManager = $this->container->get('cab.tarif.manager');
        $vahicleTypeManager = $this->container->get('cab.vehicle_type.manager');
        $data = json_decode($request->getContent(), true);
        $geoHandler = $this->get('cab_api.gelocalisation.handler');
        $drivers = $geoHandler->getCloseDriver($data);
        $vehicleType = array();

        //get company for each driver
        if (count($drivers)) {
            foreach ($drivers as $driver) {

                /** @var User $oDriver */
                $oDriver = $userManager->loadUser($driver['driver_id']);
                $company = $oDriver->getDriverCompany();
                try {
                    /** @var array $aTarif */
                    $aTarif = $tarifManager->getTarifBy('company', $company->getId());
                    if (count($aTarif)) {
                        foreach ($aTarif as $tarif) {
                            $idTarif = $tarif->getTypeVehiculeTarif();
                            $oVehicletype = $vahicleTypeManager->loadObject($idTarif);
                            $idTypeVehicle = $oVehicletype->getId();
                            $nameTypeVehicle = $oVehicletype->getNameType();
                            $vehicleType[] = array(
                                'id_type_vehicle' => $idTypeVehicle,
                                'type_vehicle' => $nameTypeVehicle,
                            );
                        }
                    }
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
        }
        $view = $this->view(array(
            'status' => Response::HTTP_OK,
            $vehicleType
                ), Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Get the vehicule type by company.
     *
     * @ApiDoc(
     *   section="Vehicule",
     *   resource = true,
     *   description = "Get vehicles type by company",
     *   output = "CAB\VehiculeBundle\Entity\TypeVehicule",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   },
     *   parameters={
     *      {"name"="customer", "dataType"="int", "required"=true, "description"="customer ID"},
     *  }
     * )
     *
     * @Annotations\Get("/vehicule-type/customer/{customer}", name="get_vehicule_type_by_company", options={ "method_prefix" = false })
     * @Annotations\View(
     *  serializerGroups={"Details"}
     * )
     *
     * @return view
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Exception
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getVehicleTypeBycompanyAction($customer) {
        $userManager = $this->container->get('cab.cab_user.manager');
        $tarifManager = $this->container->get('cab.tarif.manager');
        $vahicleTypeManager = $this->container->get('cab.vehicle_type.manager');
        $vehicleType = array();

        /** @var User $oCustomer */
        $oCustomer = $userManager->loadUser($customer);
        $company = $oCustomer->getCustomerCompany();
        /** @var array $aTarif */
        $aTarif = $tarifManager->getTarifBy('company', $company->getId());
        try {
            if (count($aTarif)) {
                foreach ($aTarif as $tarif) {
                    $idTarif = $tarif->getTypeVehiculeTarif();
                    $oVehicletype = $vahicleTypeManager->loadObject($idTarif);
                    $idTypeVehicle = $oVehicletype->getId();
                    $nameTypeVehicle = $oVehicletype->getNameType();
                    $vehicleType[] = array(
                        'id_type_vehicle' => $idTypeVehicle,
                        'type_vehicle' => $nameTypeVehicle,
                    );
                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        $view = $this->view(array(
            'status' => Response::HTTP_OK,
            $vehicleType
                ), Response::HTTP_OK);

        return $this->handleView($view);
    }

}
