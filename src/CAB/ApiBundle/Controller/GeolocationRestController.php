<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/10/2015
 * Time: 17:41
 */

namespace CAB\ApiBundle\Controller;

use CAB\ApiBundle\Exception\InvalidFormException;
use CAB\CourseBundle\Form\GeolocationCustomerType;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use CAB\CourseBundle\Entity\Geolocation;
use CAB\CourseBundle\Document\Geolocation as DocumentGeolocation;
use CAB\CourseBundle\Document\GeolocationCustomer as DocumentGeolocationCustomer;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\UserBundle\Model\UserInterface;
use CAB\CourseBundle\Form\GeolocationType;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;

class GeolocationRestController extends FOSRestController
{

    /**
     * Create a new resource
     * @return View view instance
     * @throws \InvalidArgumentException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * Create a geolocation from the submitted data.
     * @Post("/geolocation", name="create_geolocation", options={ "method_prefix" = false }),
     *
     * @ApiDoc(
     *   resource = true,
     *   authentication=true,
     *   route="create_geolocation",
     *   section="Geolocation",
     *   description = "Creates a new geolocation from the submitted data.",
     *   input = "CAB\CourseBundle\Form\GeolocationType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @View(serializerGroups={"Public"})
     *
     * @param Request $request the request object
     */
    public function postGeolocationAction(Request $request)
    {
        $logger = $this->get('monolog.logger.api');
        //$this->forwardIfNotAuthenticated();

        try {
            $dGeolocation = new DocumentGeolocation();
            $form = $this->createForm(GeolocationType::class, $dGeolocation);
            $form->handleRequest($request);
            $logger->err($request);
            $form->getErrors();
            if ($form->isSubmitted() && $form->isValid()) {
	            $driverID = $dGeolocation->getDriver();

                if(!$driverID){

                }else{
                    $dm = $this->get('doctrine_mongodb')->getManager();

                    $dm->persist($dGeolocation);
                    $dm->flush();
                    return $dGeolocation;
                }
            }
            return array('form' => $form);

        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * @View(serializerGroups={"Default","Details"})
     * Get geolocation details
     * @Get("/geolocation/{driverId}", name="get_geolocation", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Geolocation",
     *  documentation="All Geolocation features",
     *  resource=true,
     *  authentication=true,
     *  description="Get geolocation details",
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return Geolocation object
     */
    public function getGeolocationAction($driverId)
    {
        //$this->forwardIfNotAuthenticated();
        $geolocationManager = $this->get('cab.geolocation.manager');
        $geolocation = $geolocationManager->getGeolocationsBy('driver', (int) $driverId);
        if (!is_object($geolocation)) {
            throw new \LogicException('Unable to guess how to get a Doctrine instance from the request information.');
        }

        return $geolocation;
    }

    /**
     * Create a new resource
     * @return View view instance
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \InvalidArgumentException
     * Create a customer geolocation from the submitted data.
     * @Post("/geolocation-customer", name="create_geolocation_customer", options={ "method_prefix" = false }),
     *
     * @ApiDoc(
     *   resource = true,
     *   authentication=true,
     *   route="create_geolocation_customer",
     *   section="Geolocation",
     *   description = "Creates a new geolocation for customer from the submitted data.",
     *   input = "CAB\CourseBundle\Form\GeolocationCustomerType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @View(serializerGroups={"Public","Details"})
     *
     * @param Request $request the request object
     */
    public function postGeolocationCustomerAction(Request $request)
    {
        //$this->forwardIfNotAuthenticated();
        try {
            $dGeolocation = new DocumentGeolocationCustomer();
            $form = $this->createForm(GeolocationCustomerType::class, $dGeolocation);
            $form->handleRequest($request);

            $form->getErrors();
            if ($form->isSubmitted() && $form->isValid()) {
	            $driverID = $dGeolocation->getCustomer();
	            if (is_numeric($driverID)) {
		            /** @var \CAB\UserBundle\Entity\User $oUser */
		            $oUser = $this->get('cab.cab_user.manager')->loadUser($driverID);
		            if (!$oUser) {
			            $view = $this->view(
				            [
					            'status'   => Response::HTTP_BAD_REQUEST,
					            'response' => 'Customer ID Error : The user id does not exist',
				            ],
				            Response::HTTP_BAD_REQUEST
			            );

			            return $this->handleView($view);
		            }
		            elseif (!in_array('ROLE_CUSTOMER', $oUser->getRoles())) {
			            $view = $this->view(
				            [
					            'status'   => Response::HTTP_BAD_REQUEST,
					            'response' => 'Customer ID Error : The customer ID specified do not match to an customer role',
				            ],
				            Response::HTTP_BAD_REQUEST
			            );

			            return $this->handleView($view);
		            }
	            } else {
		            $view = $this->view(
			            [
				            'status'   => Response::HTTP_BAD_REQUEST,
				            'response' => 'Customer ID Error : The customer id is not numeric data',
			            ],
			            Response::HTTP_BAD_REQUEST
		            );

		            return $this->handleView($view);
	            }
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($dGeolocation);
                $dm->flush();

                return $dGeolocation;
            }

            return array(
                'form' => $form,
            );

        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * @View(serializerGroups={"Default","Details"})
     * Get geolocation details
     * @Get("/geolocation-customer/{customerId}", name="get_geolocation_customer", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Geolocation",
     *  documentation="All Geolocation features",
     *  resource=true,
     *  authentication=true,
     *  description="Get customer geolocation details",
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return Geolocation object
     * @throws \LogicException
     */
    public function getGeolocationCustomerAction($customerId)
    {
        //$this->forwardIfNotAuthenticated();
        $geolocationManager = $this->get('cab.geolocation.manager');
        $geolocation = $geolocationManager->getGeolocationsBy('customer', (int) $customerId, true);
        if (!is_object($geolocation)) {
            throw new \LogicException('Unable to guess how to get a Doctrine instance from the request information.');
        }

        return $geolocation;
    }

    /**
     * @View()
     * Get nearest drivers to customer
     * @Post("/geolocation/drivers", name="get_geolocation_nearest_drivers", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Geolocation",
     *  documentation="Get nearest drivers to customer",
     *  resource=true,
     *  description="Get five nearest drivers to customer",
     *  parameters={
     *      {"name"="latitude", "dataType"="float", "required"=true, "description"="Address longitude"},
     *      {"name"="longitude", "dataType"="float", "required"=true, "description"="Address latitude"},
     *      {"name"="type_vehicle_id", "dataType"="int", "required"=false, "description"="ID of vehicle type which
 * is getted from get vehicle type. If doesn't exists, you should put 0"},
     *  },
     * statusCodes={
     *         200="Returned when successful",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getGeolocationDriversAction(Request $request)
    {
        $data       = json_decode($request->getContent(), true);
        $geoHandler = $this->get('cab_api.gelocalisation.handler');

        $drivers = $geoHandler->getCloseDriver($data);

        if (count($drivers) > 5) {
            $listAvailableDrivers = array_slice($drivers, 0, 5, true);
        } else {
            $listAvailableDrivers = $drivers;
        }

        if (false === $drivers || !count($drivers)) {
            $view = $this->view(array(
                'status' => Response::HTTP_OK,
                'driver' => array(),
            ), Response::HTTP_OK);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_OK,
                'driver' => $listAvailableDrivers
            ), Response::HTTP_OK);

        }
        $result = $this->handleView($view);

        return $result;
    }

    /**
     * Get driver geolocation on a specific course
     *
     * @ApiDoc(
     *   section="Geolocation",
     *   resource = true,
     *   description = "Get driver geolocation on a specific course",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="Id of driver."}
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *  },
     * @Get("/geolocation/driver/{driver_id}/{course_id}", name="geolocation_driver_course", options={ "method_prefix" = false })
     * @View()
     *
     * @param integer $driver_id ID of driver
     * @param integer $course_id ID of course
     *
     * @return view
     * @throws \LogicException
     */
    public function getCourseByIdByDepartureDateTimeAction($driver_id, $course_id)
    {
        $view = $this->view(array(
            'status' => Response::HTTP_BAD_REQUEST
        ), Response::HTTP_BAD_REQUEST);

        if ($course_id && $driver_id) {
            /** @var AbstractManagerRegistry $em */
            $em = $this->getDoctrine()->getManager();
            $getCourse = $em->getRepository('CABCourseBundle:Course')->find($course_id);
            $driverGeo = $this->get('cab_api.gelocalisation.handler')->getDriverById($driver_id);
            $result = array();
            $error  = false;

            if (!$getCourse || !$driverGeo || !$getCourse->getLatDep() || !$getCourse->getLongDep()) {
                $error = true;
            }

            if (!$error) {
                // call google api to grab the time
                $driverCoords = urlencode($driverGeo[0]->getLatitude().','.$driverGeo[0]->getLongitude());
                $courseCoords = urlencode($getCourse->getLatDep().','.$getCourse->getLongDep());
                $getCoords = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$driverCoords."&destinations=".$courseCoords."&key=".$this->container->getParameter('server_key');
                $getCoords = json_decode(file_get_contents($getCoords));

                $result['time'] = $getCoords->status == 'OK' ? $getCoords->rows[0]->elements[0]->duration->text : 0;
                $result['geolocation'] = array(
                    'latitude' => $driverGeo[0]->getLatitude(),
                    'longitude' => $driverGeo[0]->getLongitude(),
                );

                $view = $this->view($result, Response::HTTP_OK);
            }
        }

        return $this->handleView($view);
    }

    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws \LogicException
     */
    protected function forwardIfNotAuthenticated($message = 'warn.user.notAuthenticated'){
        //@TODO return a json response in failed case: http://symfony.com/doc/current/bundles/FOSRestBundle/4-exception-controller-support.html
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }

}