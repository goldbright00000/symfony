<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/10/2015
 * Time: 17:41
 */

namespace CAB\ApiBundle\Controller;

use CAB\ApiBundle\Exception\InvalidFormException;
use CAB\TicketBundle\Entity\Ticket;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class TicketRestController extends FOSRestController
{
    /**
     * Create a ticket from the given json data
     *
      Subject:
      - 0 = OBJECT_MISSED_OBJECT
      - 1 = OBJECT_BEHAVIOUR_DRIVER
      - 2 = OBJECT_DELAY_COURSE
     *
      example:
      {
      "course":2267, "user":3, "subject":1, "message": "test api ticket"
      }
     *
     * @ApiDoc(
     *   section="Ticket",
     *   resource = true,
     *   description = "Creates a new ticket.",
     *   parameters={
     *      {"name"="course", "dataType"="integer", "required"=true, "description"="course Id"},
     *      {"name"="user", "dataType"="integer", "required"=true, "description"="user Id"},
     *      {"name"="subject", "dataType"="integer", "required"=true, "description"="subject"},
     *      {"name"="message", "dataType"="string", "required"=true, "description"="message"},
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Post("/ticket/new", name="new_ticket", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Details", "Public"}
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postTicketAction(Request $request)
    {
        try {
            $jsonData = json_decode($request->getContent(), true); // "true" to get an associative array

            /** @var \Doctrine\Common\Persistence\ObjectManager $em */
            $em = $this->getDoctrine()->getManager();
            /** @var array $result */
            $result = $this->get('cab_api.ticket.handler')->post($jsonData);

            if ($result['status'] == 200) {

                $headers = array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'Content-Type' => 'application/json',
                );
                //$view = $this->view($result['message'], Codes::HTTP_CREATED, $headers);
                $view = $this->view(array(
                    'status' => Response::HTTP_CREATED,
                    'message'=>$result['message'],
                ), Response::HTTP_CREATED);
            } else {
                $headers = array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'Content-Type' => 'application/json',
                );
               // $view = $this->view($result['message'], Codes::HTTP_BAD_REQUEST, $headers);
                $view = $this->view(array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message'=>$result['message'],
                ), Response::HTTP_BAD_REQUEST);
            }

            return $this->handleView($view);
        } catch (InvalidFormException $exception) {
            $headers = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'Content-Type' => 'application/json',
            );
           // $view = $this->view($result, Codes::HTTP_BAD_REQUEST, $headers);
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message'=>$result['message'],
            ), Response::HTTP_BAD_REQUEST);

            return $this->handleView($view);
        }
    }


    /**
     * Reply to a ticket.

      example:
      {
      "ticket":20, "user":3, "priority":21, "message": "test api ticket"
      }
     *
     * @ApiDoc(
     *   section="Ticket",
     *   resource = true,
     *   description = "reply to a ticket.",
     *   parameters={
     *      {"name"="ticket", "dataType"="integer", "required"=true, "description"="course Id"},
     *      {"name"="user", "dataType"="integer", "required"=true, "description"="user Id"},
     *      {"name"="status", "dataType"="integer", "required"=true, "description"="status"},
     *      {"name"="priority", "dataType"="integer", "required"=false, "description"="priority "},
     *      {"name"="message", "dataType"="string", "required"=true, "description"="message"},
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Post("/ticket/reply", name="reply_ticket", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Details", "Public"}
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function replyTicketAction(Request $request)
    {
        try {
            $jsonData = json_decode($request->getContent(), true); // "true" to get an associative array
            /** @var array $result */
            $result = $this->get('cab_api.ticket.handler')->reply($jsonData);

            if ($result['status'] == 200) {

                $headers = array(
                    'status' => Response::HTTP_CREATED,
                    'Content-Type' => 'application/json',
                );
               // $view = $this->view($result['message'], Codes::HTTP_CREATED, $headers);
                $view = $this->view(array(
                    'status' => Response::HTTP_CREATED,
                    'message'=>$result['message'],
                ), Response::HTTP_CREATED);
            } else {
                $headers = array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'Content-Type' => 'application/json',
                );
                //$view = $this->view($result['message'], Codes::HTTP_BAD_REQUEST, $headers);
                $view = $this->view(array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message'=>$result['message'],
                ),Response::HTTP_BAD_REQUEST);
            }
        } catch (InvalidFormException $exception) {
            $headers = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'Content-Type' => 'application/json',
            );
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST,

            ),Response::HTTP_BAD_REQUEST);

        }
        return $this->handleView($view);
    }

    /**
     * Get Ticket details
     *
      example:
        ticketId:20
        userId:2
     * @Get("/ticket/{ticketId}/{userId}", name="get_ticket", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Ticket",
     *  resource=false,
     *  authentication=false,
     *  description="Get ticket details",
     *  parameters={
     *      {"name"="ticketId", "dataType"="integer", "required"=false, "description"="Id of ticket."},
     *      {"name"="userId", "dataType"="integer", "required"=false, "description"="Id of user."}
     *  },
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not allowed to view the ticket",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getTicketAction($ticketId, $userId)
    {
        $result = $this->get('cab_api.ticket.handler')->getDetailsTicket($ticketId, $userId);

        $headers = array(
            'status' => $result['status_http'],
            'Content-Type' => 'application/json',
        );
        //$view = $this->view($result['data'], $result['status_http'], $headers);
        $view = $this->view(array(
            'status' => Response::HTTP_CREATED,
            'message'=>$result['data'],
            'status'=>$result['status_http'],
        ), Response::HTTP_CREATED);


        return $this->handleView($view);
    }

    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     *
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message = 'warn.user.notAuthenticated')
    {
        //@TODO return a json response in failed case: http://symfony.com/doc/current/bundles/FOSRestBundle/4-exception-controller-support.html
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }

}
