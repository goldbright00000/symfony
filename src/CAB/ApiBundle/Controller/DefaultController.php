<?php

namespace CAB\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CABApiBundle:Default:index.html.twig', array('name' => $name));
    }
    
    public function getArticleAction($id)
    {
        return array('hello' => 'world');
    }
}
