<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/10/2015
 * Time: 17:41
 */

namespace CAB\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use CAB\UserBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use CAB\UserBundle\Form\Type\ApiRegistrationFormType;

class UserRestController extends FOSRestController {

    /**
     *
     * @View(serializerGroups={"Default","Login","Details", "Public"})
     *
     * @Get("/user/login", name="get_login", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="User",
     *  resource=true,
     * route="login",
     *  description="Client side needs to know user salt in order to keep in his database an encrypted password :
      the salt has to be a public member on getUser call in our API. (see Get user details)",
     *  authentication=true,
     *  parameters={
     *      {"name"="CustomHeader", "dataType"="string", "required"=true, "description"="WSSE Header generation"},
     *      {"name"="username", "dataType"="string", "required"=true, "description"="username unique Id of user"},
     *      {"name"="PasswordDigest", "dataType"="string", "required"=true, "description"="the crypted password :
      salt+password = password as in database"},
     *      {"name"="Nonce", "dataType"="string", "required"=true, "description"="is a cryptographically random string.
      The Nonce header value is unique within five minutes"},
     *      {"name"="createdAt", "dataType"="datetime", "required"=true, "description"="the date of request : now"}
     *  }
     * )
     */
    public function getLoginAction()
    {

    	$this->forwardIfNotAuthenticated();

        /*
         * Add track login
         */
        $trackLoginManager = $this->get('cab_api.track_login_manager');
        $trackLoginParams = array(
            'user' => $this->getUser(),
            'status' => 1
        );
        $trackLoginManager->createTrackLogin($trackLoginParams, true);

        // return user
        return $this->getUser();
    }

    /**
     * @View(serializerGroups={"Default","Details", "Public", "Avatar", "Rating"})
     * Get user details
     * @Get("/user/{username}", name="get_user", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="User",
     *
     *  resource=true,
     *  description="Get user details",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="username unique Id of user"}
     *  },
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     */
    public function getUserAction($username) {
        $user = $this->getDoctrine()->getRepository('CABUserBundle:User')->findOneByUsername($username);


        if (!is_object($user)) {
            throw $this->createNotFoundException();
        }

        if (null !== $user->getAvatar()) {
            $avatar = $this->getParameter('cab_base_url') . $user->getUploadDir('avatar') . '/' . $user->getAvatar();
            $user->setAvatar($avatar);
        }

        if ($user) {
            $ratingHandler = $this->container->get('cab_api.rating_handler');
            $rating = $ratingHandler->getAverageDriverRatingsInWorkingWeek($user->getId(), 'integer');
            $user->setAverageDriverRating($rating);
        }

        return $user;
    }

    /**
     *
     * @View()
     * @Get("/user/logout/{username}", name="get_logout", options={ "method_prefix" = false })
     * @ApiDoc(
     *  resource=true,
     *  section="User",
     *  description="Client logout: just to add track",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="username which logout"},
     *  }
     * )
     */
    public function getLogoutAction($username) {

        $user = $this->get('fos_user.user_manager')->findUserByUsername($username);
        if (!is_object($user)) {
            $view = $this->view(array('status' => 0), 404);
        } else {
            $trackLoginManager = $this->get('cab_api.track_login_manager');
            $trackLoginParams = array(
                'user' => $user,
                'status' => 0
            );
            $trackLoginManager->createTrackLogin($trackLoginParams, true);

            $view = $this->view(array('status' => "logout"), 200);
        }
        return $this->handleView($view);
    }

    /**
     * Create a User from the given json data.
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "Creates a new user from the submitted data.",
     *   parameters={
     *      {"name"="email", "dataType"="string", "required"=true, "description"="user's email"},
     *      {"name"="username", "dataType"="string", "required"=true, "description"="username unique Id of user"},
     *      {"name"="plainPassword", "dataType"="string", "required"=true, "description"="user's password"},
     *      {"name"="firstName", "dataType"="string", "required"=true, "description"="user's firstname"},
     *      {"name"="lastName", "dataType"="string", "required"=true, "description"="user's lastname"},
     *      {"name"="address", "dataType"="string", "required"=true, "description"="user's complete adress"},
     *      {"name"="phoneMobile", "dataType"="string", "required"=true, "description"="user's mobile phone number"},
     *      {"name"="postCode", "dataType"="string", "required"=false, "description"="user's post code"},
     *      {"name"="city", "dataType"="string", "required"=false, "description"="user's city"},
     *      {"name"="country", "dataType"="string", "required"=false, "description"="user's country"},
     *      {"name"="latuser", "dataType"="string", "required"=false, "description"="user's latitude"},
     *      {"name"="lnguser", "dataType"="string", "required"=false, "description"="user's longitude"},
     *      {"name"="roles", "dataType"="string", "required"=false, "description"="user's role, either ROLE_CUSTOMER or ROLE_DRIVER"}
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Post("/user/users", name="create_user", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Default","Details", "Public"}
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postUserAction(Request $request) {
        try {
           // $jsonData = json_decode($request->getContent(), true); // "true" to get an associative array

            /*$valuearray = array("email" => $jsonData['email'],
                "username" => $jsonData['username'],
                "plainPassword" => $jsonData['plainPassword'],
                "firstName" => $jsonData['firstName'],
                "lastName" => $jsonData['lastName'],
                "address" => $jsonData['address'],
                "phoneMobile" => $jsonData['phoneMobile'],
                "postCode" => $jsonData['postCode'],
                "city" => $jsonData['city'],
                "country" => $jsonData['country'],
                "latuser" => $jsonData['latuser'],
                "lnguser" => $jsonData['lnguser'],
                "roles" => $jsonData['roles']);*/
            $aData = $request->request->all();



            //$form = new ApiRegistrationFormType($this->get('security.token_storage'), null, true);
            $user = $this->container->get('cab_api.user.handler')->post(
                $aData
            );

            $role = $aData['roles'];


            if ($user instanceof User) {
                $mailer = $this->container->get('mailer');
                $message = new \Swift_Message($user->getUsername().' account has create');
                        //->setSubject($user->getUsername().' account has create')
	            $message->setFrom('no-reply@up.taxi')
                        ->setTo($user->getEmail())
                        ->setBcc('info@up.taxi');

                if($role == 'ROLE_CUSTOMER') {
                    //$defaultcompany = $this->companyManager->loadDefaultCompany();
                    //$user->setCustomerCompany($defaultcompany);
                $message->setBody(
                        $this->renderView('CABUserBundle:Registration:email.html.twig', array('user' => $user)),
                        'text/html'
                    );

                } else {
                    $message->setBody(
                        $this->renderView('CABUserBundle:Registration:email.driver.html.twig', array('user' => $user)),
                        'text/html'
                    );
                }

                $ret = $mailer->send($message);
                // Send sms to new registered client
                $translator = $this->get('translator');
                $messagesms = $translator->trans(
                        'Welcome to UP, transfers easily and with professionals.', array(), 'frontend'
                );
                if ($user && $user->getPhoneMobile() !== null) {
                    $sms_sender = $this->container->get('dot_smart_sms.send_sms');
                    /*$result = $sms_sender->send(
                            array(
                                'user_id' => $user->getId(),
                                'designation' => 'Registrar',
                                'message' => $messagesms,
                                'numbers' => trim($user->getPhoneMobile())
                            )
                    );*/
                }

                $headers = array(
                    'Location' => $this->generateUrl(
                            'api_get_user', array(
                        'username' => $user->getUsername(),
                        '_format' => $request->get('_format')
                            )
                    )
                );
                $view = $this->view($user, Response::HTTP_CREATED, $headers);
            } else {

                $headers = array(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST,
                        '_format' => $request->get('_format'),
                    ),
                );
                $view = $this->view($user, Response::HTTP_BAD_REQUEST, $headers);
            }

            return $this->handleView($view);
        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * Reset a password.
     * @View(serializerGroups={"Default","Details", "Public"})
     *
     *
     * @Post("/user/reset", name="reset_user", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "Reset a password.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   },
     *   parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="user's username or email"}
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function resetPasswordAction(Request $request) {
        $username = strtolower($request->request->get('username'));

        if (!isset($username) || empty($username)) {
            $view = $this->view(array(
                'message' => 'The field is empty !'
                    ), Response::HTTP_OK);

            return $this->handleView($view);
        }

        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            $view = $this->view(array(
                'message' => 'No username or email found!'
                    ), Response::HTTP_OK);
            return $this->handleView($view);
        }

        // if user has already request a password
        if ($user->isPasswordRequestNonExpired($this->getParameter('fos_user.resetting.token_ttl'))) {
            $view = $this->view(array(
                'message' => 'Your password already resetted! Check in your boxmail'
                    ), Response::HTTP_OK);
            return $this->handleView($view);
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $user->setConfirmationToken($this->get('fos_user.util.token_generator')->generateToken());
        }

        $userEmail = $user->getEmail();

        if (empty($userEmail)) {
            $view = $this->view(array(
               'message' => 'Your email not found!'
                    ), Response::HTTP_OK);
            return $this->handleView($view);
        }

        $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);

        $view = $this->view(array(
           'message' => 'Check your email to complete resetting your password'
                ), Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Field when register
     *
     * @View(serializerGroups={"Default","Details", "Public"})
     *
     *
     * @Get("/user/field", name="register_field", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "field when register.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   },
     *   parameters={
     *      {"name"="username", "dataType"="string", "required"=false, "description"="user's username"},
     *     {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     *     {"name"="phone", "dataType"="string", "required"=false, "description"="phone"},
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function getFielAction(Request $request) {
        $username = $request->query->get('username');
        $email = $request->query->get('email');
        $phone = $request->query->get('phone');
        $checkusername = $this->get('fos_user.user_manager')->findUserByUsername($username);
        $checkemail = $this->get('fos_user.user_manager')->findUserByEmail($email);
        $checkphone = $this->get('fos_user.user_manager')->findUserBy(array('phoneMobile' => $phone));
        if (!empty($username)) {
            if (null === $checkusername) {
                $reponse = '0';
            } else {
                $reponse = '1';
            }
        }
        if (!empty($email)) {
            if (null === $checkemail) {
                $reponse = '0';
            } else {
                $reponse = '1';
            }
        }
        if (!empty($phonel)) {
            if (null === $checkphone) {
                $reponse = '0';
            } else {
                $reponse = '1';
            }
        }
        $view = $this->view(array(
            'reponse' => $reponse,
                ), Response::HTTP_OK);
        return $this->handleView($view);
    }

    private function errorArised($message) {
        $view = $this->view(array(
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => $message
                ), Response::HTTP_BAD_REQUEST);
        return $this->handleView($view);
    }

    /** Get User's avatar.
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "Get user's avatar.",
     *   parameters={
     *      {"name"="userId", "dataType"="string", "required"=true, "description"="user's ID"}
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Get("/user/{userId}/avatar", name="user_avatar", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Avatar", "Public"}
     * )
     *
     * @param integer $userId  The user's ID
     *
     * @return View
     */
    public function getUserAvatarAction($userId) {
        $user = $this->getDoctrine()->getRepository('CABUserBundle:User')->find((int) $userId);

        if ($user) {
            $reponse = array(
                'status' => Response::HTTP_OK,
                'avatar' => ''
            );

            if (null !== $user->getAvatar()) {
                $reponse['avatar'] = $this->getParameter('cab_base_url') . $user->getUploadDir('avatar') . '/' . $user->getAvatar();
            }

            $view = $this->view($reponse, Response::HTTP_OK);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'User not found!'
                    ), Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /** Get User's files.
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "Get user's files.",
     *   parameters={
     *      {"name"="userId", "dataType"="string", "required"=true, "description"="user's ID"}
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Get("/user/{userId}/files", name="user_files", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Files", "Public"}
     * )
     *
     * @param integer $userId  the user's ID
     *
     * @return View
     */
    public function getUserFilesAction($userId) {
        $user = $this->getDoctrine()->getRepository('CABUserBundle:User')->find((int) $userId);

        if ($user) {
            $reponse = array(
                'status' => Response::HTTP_OK,
                'avatar' => '',
                'driving_license' => '',
                'medical_examination' => '',
                'piece_identity' => '',
                'locker_judiciare' => ''
            );

            $base_url = $this->getParameter('cab_base_url');

            if (null !== $user->getAvatar()) {
                $reponse['avatar'] = $base_url . $user->getUploadDir('avatar') . '/' . $user->getAvatar();
            }

            if (null !== $user->getDrivingLicense()) {
                $reponse['driving_license'] = $base_url . $user->getUploadDir('drivingLicense') . '/' . $user->getDrivingLicense();
            }

            if (null !== $user->getMedicalExamination()) {
                $reponse['medical_examination'] = $base_url . $user->getUploadDir('medicalExamination') . '/' . $user->getMedicalExamination();
            }

            if (null !== $user->getPieceIdentity()) {
                $reponse['piece_identity'] = $base_url . $user->getUploadDir('pieceIdentity') . '/' . $user->getPieceIdentity();
            }

            if (null !== $user->getLockerJudiciare()) {
                $reponse['locker_judiciare'] = $base_url . $user->getUploadDir('lockerJudiciare') . '/' . $user->getLockerJudiciare();
            }

            $view = $this->view($reponse, Response::HTTP_OK);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'User not found!'
                    ), Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /** Get User's avatar as stream.
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "Get user's avatar as stream.",
     *   parameters={
     *      {"name"="userId", "dataType"="string", "required"=true, "description"="user's ID"}
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Get("/user/{userId}/avatar/stream", name="user_avatar_stream", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Avatar", "Public"}
     * )
     *
     * @param integer $userId  The user's ID
     *
     * @return View
     */
    public function getUserAvatarStreamAction($userId) {
        $user = $this->getDoctrine()->getRepository('CABUserBundle:User')->find((int) $userId);

        if ($user) {
            $imageHandler = $this->get('cab_api.image_handler');
            $reponse = array(
                'status' => Response::HTTP_OK,
                'avatar' => ''
            );

            if (null !== $user->getAvatar()) {
                $avatar = $this->getParameter('cab_base_url') . $user->getUploadDir('avatar') . '/' . $user->getAvatar();
                $reponse['avatar'] = $imageHandler->getContentAndEncode($avatar);
            }

            $view = $this->view($reponse, Response::HTTP_OK);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'User not found!'
                    ), Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /** Create User's avatar from stream.
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "Create user's avatar from stream.",
     *   parameters={
     *      {"name"="userId", "dataType"="string", "required"=true, "description"="user's ID"},
     *      {"name"="Image stream", "dataType"="string", "required"=true, "description"="image stream to be converted in image"}
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @POST("/user/avatar", name="user_avatar_create", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Avatar", "Public"}
     * )
     *
     * @param Request $request  Request Object
     *
     * @return View
     */
    public function createAvatarFromStringAction(Request $request) {
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('CABUserBundle:User')->find((int) $data['user_id']);

        if ($user) {
            $imageHandler = $this->get('cab_api.image_handler');
            $base_url = $this->getParameter('cab_base_url');
            $status_ok = Response::HTTP_OK;

            $reponse = array(
                'status' => $status_ok,
                'avatar' => ''
            );

            $userAvatr = sha1(uniqid(mt_rand(), true)) . '.png';
            $path = $user->getUploadRootDir('avatar') . '/' . $userAvatr;
            $createNewAvatar = $imageHandler->decodeImage($data['data'], $path);

            if (false !== $createNewAvatar) {
                if (null !== $user->getAvatar()) {
                    @unlink($user->getAbsolutePath('avatar'));
                }

                $base_url = rtrim($base_url, '/') . '/';
                $reponse['avatar'] = $base_url . $user->getUploadDir('avatar') . '/' . $userAvatr;
                $user->setAvatar($userAvatr);
                $em->flush();
            } else {
                $status_ok = Response::HTTP_BAD_REQUEST;
                $reponse['status'] = $status_ok;
                $reponse['avatar'] = 'Avatar could not be created';
            }

            $view = $this->view($reponse, $status_ok);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message' => 'User not found!'
                    ), Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Update user information from the given json data.
     *
     * @ApiDoc(
     *   section="User",
     *   resource = true,
     *   description = "Update user information from the given json data.",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Put("/user/update", name="update_user", options={ "method_prefix" = false })
     * @View()
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function updateUserAction(Request $request) {
        try {
            $jsonData = json_decode($request->getContent(), true);
            $user = $this->container->get('cab_api.user.handler')->put($jsonData);

            if (false !== $user) {

                if (isset($jsonData['avatar']) && !empty($jsonData['avatar'])) {
                    $em = $this->getDoctrine()->getManager();
                    $userAvatr = sha1(uniqid(mt_rand(), true)) . '.png';
                    $imageHandler = $this->get('cab_api.image_handler');

                    $path = $user->getUploadRootDir('avatar') . '/' . $userAvatr;
                    $createNewAvatar = $imageHandler->decodeImage($jsonData['avatar'], $path);

                    if (false !== $createNewAvatar) {
                        if (null !== $user->getAvatar()) {
                            @unlink($user->getAbsolutePath('avatar'));
                        }

                        $user->setAvatar($userAvatr);
                        $em->flush();
                    }
                }

                $view = $this->view(array(
                    'status' => Response::HTTP_OK,
                    'message' => 'user updated'
                        ), Response::HTTP_OK);
            } else {
                $view = $this->view(array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message' => 'Error occured'
                        ), Response::HTTP_BAD_REQUEST);
            }

            return $this->handleView($view);
        } catch (\Exception $ex) {
            die(sprintf("Error found wiht message %s", $ex->getMessage()));
        }
    }


     /**
     * @View(serializerGroups={"Default","Details"})
     * Get non affected course list .
     * @Get("/user/listdrivercompany/{driverID}", name="get_list_driver_company", options={ "method_prefix" =
     * false })
     * @ApiDoc(
     *  section="User",
     *  documentation="Get list driver the seem company.",
     *  resource=true,
     *  authentication=false,
     *  description="Get non affected course list by company.",
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getlistdrivercompanysAction($driverID)
    {
        //APi list drvier enable seem company
        $user = $this->getDoctrine()->getRepository('CABUserBundle:User')->getDiverAvailableListByCompany($driverID);
        $view = array(
            'driverlist' => $user,
        );

        return $view;
    }

    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message = 'warn.user.notAuthenticated') {

    	//@TODO return a json response in failed case: http://symfony.com/doc/current/bundles/FOSRestBundle/4-exception-controller-support.html
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }

}
