<?php
namespace CAB\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Response;

/**
* Device Rest API
*/
class DeviceRestController extends FOSRestController
{
    /**
     * Add new push notification
     *
     * @ApiDoc(
     *   section="Device",
     *   resource = true,
     *   description = "Add new push notification",
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *
     * @Annotations\Post("/device/push", name="post_device_push", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param integer  $user_id      User id
     * @param string   $registerId   RegisterId
     * @param string   $brand        Phone's brand
     * @param string   $model        Phone's model
     * @param string   $os           Phone's os
     * @param string   $imeiDevice   Phone's imeiDevice
     * @param string   $imsiDevice   Phone's imsiDevice
     * @param string   $appleImei    Phone's appleImei in case the phone os was Apple
     * @param string   $device_token    device token user
     * @param boolean   $gateway    device token user
     * @return int  status code
     */
    public function devicePushAction(Request $request)
    {
        $deviceHandler = $this->container->get('cab_api.device_handler');
        $getJsonData   = json_decode($request->getContent(), true); // "true" to get an associative array
        $getPush       = $deviceHandler->post(
            $getJsonData
        );

        if (false === $getPush) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
            ), Response::HTTP_BAD_REQUEST);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_CREATED
            ), Response::HTTP_CREATED);
        }

        return $this->handleView($view);
    }

    /**
     * Get a push notification by user_id
     *
     * @ApiDoc(
     *   section="Device",
     *   resource = true,
     *   description = "Get a push notification by token",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *
     * @Annotations\Get("/device/push/{user_id}", name="get_device_push", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @return int  status code
     */
    public function getDevicePushAction($user_id)
    {
        $deviceHandler = $this->container->get('cab_api.device_handler');
        $getDevice      = $deviceHandler->get($user_id);

        if (false === $getDevice) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
            ), Response::HTTP_BAD_REQUEST);
        } else {
            $view = $this->view($getDevice, Response::HTTP_OK);
        }

        return $this->handleView($view);
    }

    /**
     * Update a push notification by its token
     *
     * @ApiDoc(
     *   section="Device",
     *   resource = true,
     *   description = "Update a push notification by its token",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *
     * @Annotations\Put("/device/push/update", name="update_device_push", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param string  $device_token      A device token to update a push notification
     * @return int  status code
     */
    public function updateDevicePushAction(Request $request)
    {
        $deviceHandler  = $this->container->get('cab_api.device_handler');
        $getData        = json_decode($request->getContent(), true); // "true" to get an associative array
        $getPush        = $deviceHandler->put($getData);

        if (false === $getPush) {
            $view = $this->view(array(
                'status' => Response::HTTP_BAD_REQUEST
            ), Response::HTTP_BAD_REQUEST);
        } else {
            $view = $this->view(array(
                'status' => Response::HTTP_NO_CONTENT
            ), Response::HTTP_NO_CONTENT);
        }

        return $this->handleView($view);
    }
}