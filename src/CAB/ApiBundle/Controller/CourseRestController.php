<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/10/2015
 * Time: 17:41
 */

namespace CAB\ApiBundle\Controller;

use CAB\ApiBundle\Exception\InvalidFormException;
use CAB\CourseBundle\Entity\Country;
use CAB\CourseBundle\Entity\CourseHistory;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\DetailsTarif;
use CAB\UserBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class CourseRestController extends FOSRestController
{

    /**
     * @View(serializerGroups={"Public", "Details"})
     * Get course details
     * @Get("/course/{courseId}", name="get_course", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Course details",
     *  resource=false,
     *  authentication=false,
     *  description="Get course details",
     *  parameters={
     *      {"name"="courseId", "dataType"="integer", "required"=true, "description"="Id of course."}
     *  },
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCourseAction($courseId)
    {
        //$this->forwardIfNotAuthenticated();
        $CourseManager = $this->get('cab.course_manager');
        $course = $CourseManager->loadCourse($courseId);


        /** @var Course $item */

        if (!is_object($course)) {
            throw $this->createNotFoundException();
        }

        $driverSearched = null;
        $forfait = false;
        $course_details_tarif = null;
        $course_estimated = null;
        $indicated_depart = '';
        $indicated_arrival = '';
        $indicated_message = '';
        $currency = 'EUR';
        $ticket = array();
        $priceview = false;
        $comp = null;

        if ($course->getClient() !== null) {
            $client['id'] = $course->getClient()->getid();
            $client['first_name'] = $course->getClient()->getfirstName();
            $client['last_name'] = $course->getClient()->getlastName();
            $client['phone_mobile'] = $course->getClient()->getphoneMobile();
            $client['phone_home'] = $course->getClient()->getphoneHome();
        }

        if ($course->getClient() !== null) {
            $client['first_name'] = $course->getClient()->getfirstName();
            $client['last_name'] = $course->getClient()->getlastName();
            $client['phone_mobile'] = $course->getClient()->getphoneMobile();
            $client['phone_home'] = $course->getClient()->getphoneHome();
        }

        if ($course->getCompany() !== null) {
            $comp = $course->getCompany()->getId();
            if ($course->getCompany()->getFormat() !== null) {
                $currency = $course->getCompany()->getFormat()->getcurrencyCode();
            }
        }
        if ($course->getDriver() !== null) {
            //$driverSearched = $item->getDriver()->getId();
        $driverSearched['first_name'] = $course->getDriver()->getfirstName();
        $driverSearched['last_name'] = $course->getDriver()->getLastName();
        $driverSearched['phone_mobile'] = $course->getDriver()->getPhoneMobile();
        $driverSearched['id'] = $course->getDriver()->getId();

    }
        if ($course->getCourseDetailsTarif() !== null) {
            $course_details_tarif = $course->getCourseDetailsTarif()->getId();
            $course_estimated = $course->getCourseDetailsTarif()->getEstimatedTarif();

            $course_estimated_TTC = $course->getCourseDetailsTarif()->getTarifTotalTTC();
            $forfait = $course->getCourseDetailsTarif()->getIsForfait();
        }else{
            $course_details_tarif = " ";
            $course_estimated = " ";

        }
        if ($course->getIndicatorStartedAddress() !== null) {
            $indicated_depart = $course->getIndicatorStartedAddress();
        }

        if ($course->getindicatorArrivalAddress() !== null) {
            $indicated_arrival = $course->getindicatorArrivalAddress();
        }
        if ($course->getCourseDetailsTarif() !== null) {
            $course_estimated_HT = $course->getCourseDetailsTarif()->getTarifHT();
        }else{
            $course_estimated_HT = $course->getPriceHT();
            $course_estimated_TTC = $course->getPriceTTC();
        }
        if ($course->getpriceHt() !== null) {
            $priceHT = $course->getpriceHt();
        }else{
            $priceHT = "";
        }
        if ($course->getpriceTTC() !== null) {
            $priceTTC = $course->getpriceTTC();
        }else{
            $priceTTC = "";
        }

        if ($course->getTicket() != null && $course->getTicket()->first() != false) {

        	if($course->getTicket()->first() != false)
	        {
		        $ticket['id'] = $course->getTicket()->first()->getId();
		        $ticket['user'] = $course->getTicket()->first()->getlastUser();
	        }
	        else{
		        $ticket['id'] = $course->getTicket()->getId();
		        $ticket['user'] = $course->getTicket()->getlastUser();
	        }

        }else{
            $ticket['id'] = '';
            $ticket['user'] = '';
        }

        if ($course->getCourseTime() !== null){
            $course_time = $course->getCourseTime();
        }else{
            $course_time = '' ;
        }

        if ($course->getserviceTransportCourses() !== null)  {

            foreach ($course->getserviceTransportCourses() as $v) {
                $priceview = $v->getPriceView();

            } }

        if ($course->getpersonNumber() !== null){
            $NumberPersonn = $course->getpersonNumber();
        } else {
            $NumberPersonn = 0;
        }

        if ($course->getDescriptionChildren() !== null){
            $message = $course->getDescriptionChildren();
        } else {
            $message = '';
        }



        /** @var Course $course */

        $result= array(
            'id' => $course->getId(),
            'client' => $client,
            'company' => array('id' => $comp),
            'driver' => $driverSearched,
            'course_details_tarif' => array('id' => $course_details_tarif, "estimated_tarif" => $course_estimated ),
            'started_address' => $course->getStartedAddress(),
            'indicator_start_adresse' => $indicated_depart,
            'arrival_address' => $course->getArrivalAddress(),
            'indicator_arrival_adresse' => $indicated_arrival,
            'message' => $message,
            'isgroupage' => $course->getIsGroupage(),
            'departure_date' => $course->getDepartureDate()->format('d/m/Y'),
                'departure_time' => $course->getDepartureTime()->format('H:i'),
                'person_number' => $course->getpersonNumber(),
                'priceHT' => $course_estimated_HT,
                'priceTTC' => $course_estimated_TTC,
                'course_status' => $course->getcourseStatus(),
                'currency' => $currency,
                'forfait' => $forfait,
                'ticket' => $ticket,
                'course_time' => $course_time,
            'long_dep' => $course->getlongDep(),
            'lat_dep'=> $course->getlatDep(),
            'long_arr' => $course->getlongArr(),
            'lat_arr' => $course->getlatArr(),
            'PriceView' => $priceview,
            'NumberPersonn' => $NumberPersonn
        );

        return array(
            'status' => Response::HTTP_OK,
            'result' => $result,
            'currency' => $course->getCompany()->getFormat()->getCurrencyCode(),
        );
    }


    /**
     * Get Status by course
     * @View()
     * @Get("/coursestatus/{courseId}", name="get_courses_status", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get status by race",
     *  resource=false,
     *  authentication=false,
     *  description="Get status by race",
     *  parameters={
     *      {"name"="courseId", "dataType"="integer", "required"=true, "description"="Id of race."}
     *  },
     *
     * statusCodes={
     *         200 = "Returned when successful",
     *         403 = "Returned when the user is not authorized to access to resource",
     *         404 = {
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getStatusByRaceAction($courseId)
    {
        $result = $this->get('cab_api.course.handler')->getStatusByRace($courseId);
        if (false === $result) {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                ), Response::HTTP_BAD_REQUEST
            );
        } else {
            $result = array(
                'status' => Response::HTTP_OK,
                'statutrace' => $result);
            return $this->view($result, Response::HTTP_OK);
        }


    }

    /**
     * @View(serializerGroups={"Default","Details"})
     * Get non affected course list .
     * @Get("/course/list-not-affected/{driverID}", name="get_list_not_affected_course", options={ "method_prefix" =
     *                                                false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get non affected course list by company.",
     *  resource=true,
     *  authentication=false,
     *  description="Get non affected course list by company.",
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getNotAffectedCoursesAction($driverID)
    {
        //$this->forwardIfNotAuthenticated();
        $courseManager = $this->get('cab.course_manager');

        $user = $this->getDoctrine()->getRepository('CABUserBundle:User')->findOneById($driverID);
        $company = $user->getDriverCompany();

        $courses = $courseManager->getAllCourseNotAffected($company);

        $aResult = array();

        foreach ($courses as $item) {

            if ($item->getClient() != null) {

                if ($item->getCourseDetailsTarif() !== null) {
                    $course_details_tarif = $item->getCourseDetailsTarif()->getId();
                    $course_estimated = $item->getCourseDetailsTarif()->getEstimatedTarif();

                    $course_estimated_TTC = $item->getCourseDetailsTarif()->getTarifTotalTTC();
                    $forfait = $item->getCourseDetailsTarif()->getIsForfait();
                }else{
                    $course_details_tarif = " ";
                    $course_estimated = " ";
                    $forfait = false;

                }
                if ($item->getCourseDetailsTarif() !== null) {
                    $course_estimated_HT = $item->getCourseDetailsTarif()->getTarifHT();
                }else{
                    $course_estimated_HT = $item->getPriceHT();
                    $course_estimated_TTC = $item->getPriceHT();
                }

                /** @var Course $course */
                $aResult[] = array(
                    'id' => $item->getId(),
                    'id_customer' => $item->getClient()->getId(),
                    'name_customer' => $item->getClient()->getUsedName(),
                    'started_address' => $item->getStartedAddress(),
                    'arrival_address' => $item->getArrivalAddress(),
                    'departure_date' => $item->getDepartureDate()->format('d/m/Y') . ' ' . $item->getDepartureTime()
                            ->format('H:i:s'),
                    'distance' => $item->getDistance(),
                    'price_ttc' => $course_estimated_TTC,
                    'price_max' => $course_estimated,
                    'forfait'=>$forfait,



                );

            }
        }


        $view = array(
            'course' => $aResult,

        );

        return $view;
    }

    /**
     * @View(serializerGroups={"Default","Details"})
     * Get non affected course list by company.
     * @Get("/course/count/{driverID}", name="count", options={ "method_prefix" =
     *                                                false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="count non affected course list by company.",
     *  resource=true,
     *  authentication=false,
     *  description="Get non affected course list by company",
     *
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCountNotAffectedCoursesAction($driverID)
    {
        //$this->forwardIfNotAuthenticated();
        $courseManager = $this->get('cab.course_manager');

        $user = $this->getDoctrine()->getRepository('CABUserBundle:User')->findOneById($driverID);
        $user = $user->getDriverCompany();

        if($user != null)
        {
	        $courses = $courseManager->getAllCourseNotAffected($user);

	        $aResult = array();

	        foreach($courses as $item)
	        {
		        if($item->getClient() != null)
		        {
			        $aResult[] = array();
		        }
	        }
	        $view = array(
		        'count' => count($aResult),
	        );
        }
        else{

        	$view = array(
		        'message' => 'Not Found',

	        );

        }



        return $view;
    }

    //@TODO : check the permission of the user whether the user can access to course
    /**
     * @View(serializerGroups={"Public","Details"})
     * Get course list by criteria.
     * @Get("/course/list/{userID}/{role}/{criteria}/{value}", name="get_list_course_by_criteria", options={
     *                                                         "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get course list by criteria.",
     *  resource=true,
     *  authentication=false,
     *  description="Get course list by role",
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     * * @QueryParam(
     *   name="userID",
     *   requirements="true",
     *   default="",
     *   description="The user ID",
     * )
     * * @QueryParam(
     *   name="role",
     *   requirements="true",
     *   default="driver",
     *   description="The role of the user, can be 'driver' or 'client'",
     * )
     * * @QueryParam(
     *   name="criteria",
     *   requirements="true",
     *   default="null",
     *   description="The criteria",
     * )
     * * @QueryParam(
     *   name="value",
     *   requirements="true",
     *   default="driver",
     *   description="The criteria value",
     * )
     *
     */
    public function getCourseByCriteriaAction($userID, $role, $criteria, $value)
    {
        $oUser = $this->get('cab.cab_user.manager')->loadUser($userID);
        $result = $this->get('cab.course_manager')->getCourseBy($oUser, $role, $criteria, $value);

        $view = array(
            'course' => $result,
        );

        return $view;


        //return $result;
    }

    //@TODO : check the permission of the user whether the user can access to course
    /**
     * @View(serializerGroups={"Public","Details", "Course"})
     * Get course list by criteria.
     * @Get("/course/list-by-date-interval/{userID}/{role}/{valueDateFrom}/{valueDateTo}",
     *     name="get_list_course_by_date_interval",
     *                                                                                     options={ "method_prefix" =
     *                                                                                     false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get course list by date interval.",
     *  resource=true,
     *  authentication=false,
     *  description="Get course list by date interval",
     * statusCodes={
     *         200="Returned when successful",
     *         403="Returned when the user is not authorized to access to resource",
     *         404={
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     * * @QueryParam(
     *   name="userID",
     *   requirements="true",
     *   default="",
     *   description="The user ID",
     * )
     * * @QueryParam(
     *   name="role",
     *   requirements="true",
     *   default="driver",
     *   description="The role of the user, can be 'driver' or 'client'",
     * )
     * * @QueryParam(
     *   name="valueDateFrom",
     *   requirements="true",
     *   default="",
     *   description="The criteria value",
     * )
     *
     * * @QueryParam(
     *   name="valueDateTo",
     *   requirements="true",
     *   default="",
     *   description="The criteria value",
     * )
     *
     * @return View
     */
    public function getCourseByDateAction($userID, $role, $valueDateFrom, $valueDateTo)
    {
        /** @var User $oUser */
        $oUser = $this->get('cab.cab_user.manager')->loadUser($userID);
        $aResult = $this->get('cab.course_manager')->getCourseByDate($oUser, $role, $valueDateFrom, $valueDateTo);
        $result = array();
        /** @var Course $item */
        foreach ($aResult as $item) {
            $driverSearched = null;
            $forfait = false;
            $course_details_tarif = null;
            $course_estimated = null;
            $indicated_depart = '';
            $indicated_arrival = '';
            $indicated_message = '';
            $currency = 'EUR';
            $ticket = '';
            $comp = null;
            $client = array(
                'first_name' => '',
                'last_name' => '',
                'phone_mobile' => '',
                'phone_home' => '',
            );


            if ($item->getDriver() !== null) {
                //$driverSearched = $item->getDriver()->getId();
                $driverSearched['first_name'] = $item->getDriver()->getfirstName();
                $driverSearched['last_name'] = $item->getDriver()->getLastName();
                $driverSearched['phone_mobile'] = $item->getDriver()->getPhoneMobile();
                $driverSearched['id'] = $item->getDriver()->getId();
            }
            if ($item->getCourseDetailsTarif() !== null) {
                $course_details_tarif = $item->getCourseDetailsTarif()->getId();
                $course_estimated = $item->getCourseDetailsTarif()->getEstimatedTarif();

                $course_estimated_TTC = $item->getCourseDetailsTarif()->getTarifTotalTTC();
                $forfait = $item->getCourseDetailsTarif()->getIsForfait();
            }else{
                $course_details_tarif = " ";
                $course_estimated = " ";

            }
            if ($item->getCourseDetailsTarif() !== null) {
                $course_estimated_HT = $item->getCourseDetailsTarif()->getTarifHT();
            }else{
                $course_estimated_HT = $item->getPriceHT();
                $course_estimated_TTC = $item->getPriceHT();
            }



            if ($item->getCompany() !== null) {
                $comp = $item->getCompany()->getId();
                if ($item->getCompany()->getFormat() !== null) {
                    $currency = $item->getCompany()->getFormat()->getcurrencyCode();
                }
            }
            if ($item->getClient() !== null) {
                $client['first_name'] = $item->getClient()->getfirstName();
                $client['last_name'] = $item->getClient()->getlastName();
                $client['phone_mobile'] = $item->getClient()->getphoneMobile();
                $client['phone_home'] = $item->getClient()->getphoneHome();
            }

            if ($item->getIndicatorStartedAddress() !== null) {
                $indicated_depart = $item->getIndicatorStartedAddress();
            }

            if ($item->getindicatorArrivalAddress() !== null) {
                $indicated_arrival = $item->getindicatorArrivalAddress();
            }
            if ($item->getdescriptionChildren() !== null) {
                $indicated_message = $item->getdescriptionChildren();
            }
            if ($item->getpriceHt() !== null) {
                $priceHT = $item->getpriceHt();
            }else{
                $priceHT = "";
            }
            if ($item->getpriceTTC() !== null) {
                $priceTTC = $item->getpriceTTC();
            }else{
                $priceTTC = "";
            }
            if ($item->getTicket() !== null) {
                $ticket = $item->getTicket()->getId();
            }else{
                $ticket = "";
            }
            if ($item->getCourseTime() !== null){
                $course_time = $item->getCourseTime();
            }else{
                $course_time = 30 ;
            }


            $result[] = array(
                'id' => $item->getId(),
                'client' => $client,
                'company' => array('id' => $comp),
                'driver' => array($driverSearched ),
                'course_details_tarif' => array('id' => $course_details_tarif, "price_estimate" => $course_estimated ),
                'started_address' => $item->getStartedAddress(),
                'indicator_start_adresse' => $indicated_depart,
                'arrival_address' => $item->getArrivalAddress(),
                'indicator_arrival_adresse' => $indicated_arrival,
                'message' => $indicated_message,
                'departure_date' => $item->getDepartureDate()->format('d/m/Y'),
                'departure_time' => $item->getDepartureTime()->format('H:i'),
                'person_number' => $item->getpersonNumber(),
                'priceHT' => $course_estimated_HT,
                'priceTTC' => $priceTTC,
                'course_status' => $item->getcourseStatus(),
                'currency' => $currency,
                'forfait' => $forfait,
                'ticket' => $ticket,
                'course_time' => $course_time,
            );
        }

        $view = array(
            'course' => $result,

        );

        return $view;
    }

    /**
     * Accept/Refuse course by driver
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Accept course by driver",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="Id of driver."},
     *      {"name"="latitude", "dataType"="decimal", "required"=true, "description"="latitude"},
     *      {"name"="longitude", "dataType"="decimal", "required"=true, "description"="longitude"},
     *      {"name"="status", "dataType"="integer", "required"=true, "description"="accepted or refused course."}
     *  },
     * @Annotations\Put("/course/action-driver", name="driver_action_course", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function updateCourseAction(Request $request)
    {
        $getJsonData = json_decode($request->getContent(), true);

        //var_dump($getJsonData);
        //exit;

        $courseResult = $this->get('cab_api.course.handler')->put($getJsonData);
        /** @var User $oDriver */
        $oDriver = $this->get('fos_user.user_manager')->findUserBy(array('id' => $getJsonData['driver_id']));

        $oCourse = $this->get('cab.course_manager')->loadCourse($getJsonData['course_id']);

        $client =  $oCourse->getclient();
        $Emailclient = $oCourse->getclient()->getEmail();

        $Companyname = $oCourse->getcompany()->getcompanyName();
        $taxId = $oCourse->getCompany()->getId();
        $oTax = $this->get('cab_course.manager.tax_manager')->loadTax($taxId);

        if (is_array($courseResult)) {

            $historyParams = array(
                'course' => $oCourse,
                'status' =>$getJsonData['status'],
//                'status' => $courseResult['status'],
                'driver' => $oCourse->getDriver(),
                'departureAddress' => $oCourse->getStartedAddress(),
                'arrivalAddress' => $oCourse->getArrivalAddress(),
                'priceTTC' => $oCourse->getPriceTTC(),
                'departureDate' => $oCourse->getDepartureDate(),
                'departureTime' => $oCourse->getDepartureTime(),
                'targetType' => $oCourse->getTargetType(),
                'agent' => $oCourse->getcreatedBy(),
                'latitude' => $getJsonData['latitude'],
                'longitude' => $getJsonData['longitude']
            );
            $courseHistory = new CourseHistory();
            foreach ($historyParams as $attr => $value) {
                $attributeSetter = 'set' . ucfirst($attr);
                $courseHistory->$attributeSetter($value);
            }
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($courseHistory);
            $em->flush();

            if (0 === $courseResult['status']) {
                $view = $this->view(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                    ),
                    Response::HTTP_BAD_REQUEST
                );
            } elseif (1 === $courseResult['status']) {
                $messagePush = 'La course est affectée';
                $data = array(
                    'type_notification' => 'the_race_is_affected',
                    'subject_notification' => 'Votre commande est acceptée',
                    'id_driver' => $oDriver->getId(),
                    'type' => '0',
                    'alert' => $messagePush,
                    'id_course' => $oCourse->getId(),

                    //'sound' => 'ringingbell.mp3',
                );

                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );

                $mailer = $this->container->get('mailer');
                $message = new \Swift_Message('commande n° ' . $oCourse->getId().' est acceptée');
	            $message->setFrom('invoice@up.taxi')
                    ->setTo($Emailclient);
                $message->setBody(
                    $this->renderView('CABCourseBundle:Mail:updateraceaffect.html.twig', array('driver' => $oDriver, 'user'=> $client, 'course' => $oCourse, 'tax' => $oTax, 'companyname' => $Companyname)),
                    'text/html'
                );
                $ret = $mailer->send($message);

                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],

                    ),
                    Response::HTTP_OK
                );

            } elseif (2 === $courseResult['status']) {
                // Course rejected
                // send push notification to customer
                $messagePush = 'A course is rejected by driver';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'A course is rejected by driver',
                    'id_driver' => $oDriver->getId(),
                    'type' => '0',
                    'alert' => $messagePush,
                    'id_course' => $oCourse->getId(),
                    //'sound' => 'ringingbell.mp3',
                );

                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );

                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],
                    ),
                    Response::HTTP_OK
                );
            } elseif (14 === $courseResult['status']) {
                // Course rejected
                // send push notification to customer
                $messagePush = 'pas de reponse du chauffeur';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'Pas de reponse du chauffeur',
                    'id_driver' => $oDriver->getId(),
                    'type' => '0',
                    'alert' => $messagePush,
                    'id_course' => $oCourse->getId(),
                    //'sound' => 'ringingbell.mp3',
                );

                $this->get('cab_course.getCourseActionpush_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );

                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],
                    ),
                    Response::HTTP_OK
                );
            }elseif (13 === $courseResult['status']) {
                // Driver arrival
                // send push notification to customer
                $messagePush = 'Le chauffeur est arrivé';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => $this->get('translator')->trans(
                        'Le chaufeur %name% vous attend. Tel: %phone%  Voiture: %vehiclename% Immat : %immat%',
                        array(
                            '%name%' => $oDriver->getFirstName(),
                            '%phone%' => $oDriver->getPhoneMobile(),
                            '%immat%' => $oDriver->getVehicle()->getLicensePlateNumber(),
                            '%vehiclename%' => $oDriver->getVehicle()->getVehiculeName(),
                        ), 'frontend'
                    ),
                    'id_course' => $oCourse->getId(),
                    'id_driver' => $oDriver->getId(),
                    'type' => '0',
                    'sound' => 'ringingbell.mp3',
                    'alert' => $messagePush,

                );
                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );
                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],
                    ),
                    Response::HTTP_OK
                );

                //send SMS
                $smsNotification = $oCourse->getClient()->getNotificationSms();
                if ($oCourse->getClient()->getPhoneMobile() !== null && $smsNotification && $smsNotification->getStatusArrivedDriver()) {
                    $messagesms = $this->get('translator')->trans(
                        'Le chaufeur %name% vous attend. Tel: %phone%  Voiture: %vehiclename% Immat : %immat%',
                            array(
                                '%name%' => $oDriver->getFirstName(),
                                '%phone%' => $oDriver->getPhoneMobile(),
                                '%immat%' => $oDriver->getVehicle()->getLicensePlateNumber(),
                                '%vehiclename%' => $oDriver->getVehicle()->getVehiculeName(),
                            ), 'frontend'
                    );
                    $sendSmsService = $this->get('dot_smart_sms.send_sms');
                    $resultSMS = $sendSmsService->send(
                        array(
                            'user_id' => $oCourse->getClient()->getId(),
                            'designation' => 'Arrival Driver '.$oCourse->getId(),
                            'message' => $messagesms,
                            'numbers' => trim($oCourse->getClient()->getPhoneMobile())
                        )
                    );
                }
            } elseif (15 === $courseResult['status']) {
                // Driver arrival
                // send push notification to customer
                $messagePush = 'Le chauffeur a annulé la course';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'Le chauffeur a annulé la course',
                    'id_course' => $oCourse->getId(),
                    'id_driver' => $oDriver->getId(),
                    'type' => '0',
                    'sound' => 'ringingbell.mp3',
                    'alert' => $messagePush,

                );
                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );
                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],
                    ),
                    Response::HTTP_OK
                );
            } elseif (9 === $courseResult['status']) {
                // Course rejected
                // send push notification to customer
                $messagePush = 'La course commence';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'bonne route avec votre taxi UP',
                    'id_course' => $oCourse->getId(),
                    'type' => '0',
                    'sound' => 'ringingbell.mp3',
                    'alert' => $messagePush,
                );
                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );
                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],
                    ),
                    Response::HTTP_OK
                );
            } elseif (18 === $courseResult['status']) {
                // Course rejected
                // send push notification to customer
                $messagePush = 'Chauffeur en route ';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'Chauffeur en route',
                    'id_driver' => $oDriver->getId(),
                    'type' => '0',
                    'alert' => $messagePush,
                    'id_course' => $oCourse->getId(),
                    //'sound' => 'ringingbell.mp3',
                );

                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );

                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],
                    ),
                    Response::HTTP_OK
                );
            } elseif (6 === $courseResult['status']) {

                // send push notification to customer
                $messagePush = 'La course est terminée';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'merci de noter votre trajet',
                    'id_course' => $oCourse->getId(),
                    'type' => '0',
                    'alert' => $messagePush,
                );
                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );
                $mailer = $this->container->get('mailer');
                $message = new \Swift_Message('facture n° ' . $oCourse->getId());

                    $message->setFrom('invoice@up.taxi')
                    ->setTo($Emailclient);
                $message->setBody(
                    $this->renderView('CABMainBundle:Mail:invoice.html.twig', array('driver' => $oDriver, 'client'=> $client, 'race' => $oCourse, 'tax' => $oTax, 'companyname' => $Companyname)),
                    'text/html'
                );

               $ret = $mailer->send($message);


                $view = $this->view(
                    array(
                        'status' => Response::HTTP_OK,
                        'status_update' => $courseResult['status'],
                        'response' => $courseResult['response'],
                        'course_id' => $courseResult['course_id'],
                    ),
                    Response::HTTP_OK
                );
            } else {
                // send push notification to customer
                $messagePush = 'A course is accepted by ' . $oDriver->getUsedName() ;
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'A course is accepted by a driver',
                    'id_driver' => $oDriver->getId(),
                    'driver_name' => $oDriver->getUsedName(),
                    'driver_vehicle' => $oDriver->getVehicle(),
                    'duration_arrival_driver' => $oCourse->getApproachTime(),
                    'type' => '0',
                    'alert' => $messagePush,
                    'id_course' => $oCourse->getId(),
                    'sound' => 'ringingbell.mp3',
                    'alert' => $messagePush,
                );

                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $oCourse->getClient()->getDevices()->toArray(),
                    $messagePush,
                    $data,
                    'customer'
                );

            }


            // Planing: create planing for this course
            /* I comment theses line because it exists an event listner that make the planing when the course is
              affected to the driver
              CAB/CourseBundle/EventListener/UpdateStatusCourseListener.php
             */
            /* $this->get('cab_course.handler.planing_handler')->handlerEvent(
              $oDriver,
              $oCourse,
              'Accepted by driver via App mobile'
              ); */
            $view = $this->view(
                array(
                    'status' => Response::HTTP_CREATED,
                    'status_update' => $courseResult['status'],
                    'response' => $courseResult['response'],
                    'course_id' => $courseResult['course_id'],
                    'course_date' => $courseResult['course_date'],
                    'course_time' => $courseResult['course_time'],
                    'course_departure' => $courseResult['course_departure'],
                    'course_arrival' => $courseResult['course_arrival'],
                    'long_departure' => $courseResult['long_departure'],
                    'lat_departure' => $courseResult['lat_departure'],
                    'long_arrival' => $courseResult['long_arrival'],
                    'lat_arrival' => $courseResult['lat_arrival'],
                ),
                Response::HTTP_CREATED
            );
        } else {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'response' => 'Error course Id',
                ),
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->handleView($view);
    }

    /**
     * Change statut Race
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Transfert course by driver",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   },
     *      parameters={
     *     {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *     {"name"="driver_id", "dataType"="integer", "required"=false, "description"="Id of driver."},
     *     {"name"="current_driver_id", "dataType"="integer", "required"=true, "description"="Id of current driver."},
     *  }
     * )
     *
     * @Annotations\Patch("/course/transfert-driver", name="driver_transfert_course", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     * @throws \LogicException
     */
    public function updatetransfertCourseAction(Request $request)
    {
        $getJsonData = json_decode($request->getContent(), true);

        $response = $this->get('cab_api.course.handler')->patch($getJsonData);

        if ($response['status'] && $getJsonData['driver_id']) {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_OK,
                    'new_driver' => $getJsonData['driver_id'],
                    'status_course' => Course::STATUS_AFFECTED,
                    'response' => 'Transfert course success',
                ),
                Response::HTTP_OK
            );
        } elseif ($response['status'] && array_key_exists('list_drivers', $response)) {
            $aDriversEnabled = $response['list_drivers'];

            $oCourse = $response['course'];
             // send push notification to driver
                foreach ($aDriversEnabled as $driver) {
                    $oDriver = $this->getDoctrine()->getRepository('CABUserBundle:User')->find($driver);
                    //$oDriver = $this->userManager->findUserBy(array('id' => $driver['id']));
                    $messagePush = 'A transfert trip is updated by driver, can you accept ?';

                    $data = array(
                        'type_notification' => 'driver_notification_course',
                        'subject_notification' => 'Accept or refuse this course',
                        'alert' => 'Veuillez, Thanks',
                        'badge' => '1',
                        'sound' => 'ringingbell.mp3',
                        'type' => '1',
                    );


                    $this->get('cab_course.push_notification.push')->getServiceByOS(
                        $oDriver->getDevices()->toArray(),
                        $messagePush,
                        $data
                    );
                }

            $view = $this->view(
                array(
                    'status' => Response::HTTP_OK,
                    'new_driver' => null,
                    'status_course' => Course::STATUS_UPDATED_BY_DRIVER,
                    'response' => 'Transfert course success',
                ),
                Response::HTTP_OK
            );
        } else {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'response' => 'Transfert course error',
                ),
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->handleView($view);
    }


    /**
     * Create a course from the given json data.
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Creates a new course from the submitted data.",
     *   parameters={
     *      {"name"="client", "dataType"="integer", "required"=true, "description"="client Id"},
     *      {"name"="driver", "dataType"="integer", "required"=true, "description"="driver Id"},
     *      {"name"="startedAddress", "dataType"="string", "required"=true, "description"="Course departure address"},
     *      {"name"="longDep", "dataType"="float", "required"=true, "description"="Departure address longitude"},
     *      {"name"="latDep", "dataType"="string", "required"=true, "description"="Departure address latitude"},
     *      {"name"="arrivalAddress", "dataType"="string", "required"=true, "description"="Course arrival address"},
     *      {"name"="longArr", "dataType"="float", "required"=true, "description"="Arrival address longitude"},
     *      {"name"="latArr", "dataType"="string", "required"=true, "description"="Arrival address latitude"},
     *      {"name"="vehicle", "dataType"="string", "required"=false, "description"="vehicle for the driver"},
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Post("/course/new", name="new_course", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Details", "Public"}
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface |View
     */
    public function postCourseAction(Request $request)
    {
        try {
            $jsonData = json_decode($request->getContent(), true); // "true" to get an associative array
            $result = $this->get('cab_api.course.handler')->post($jsonData);
            if ($result instanceof Course) {
                $headers = array(
                    'Location' => $this->generateUrl(
                        'api_get_course',
                        array(
                            'courseId' => $result->getId(),
                            '_format' => $request->get('_format'),
                        )
                    ),
                );

                $resultJson = array(
                    'course' => $result,
                    'currency' => $result->getCompany()->getFormat()->getCurrencyCode(),
                );

                $view = $this->view($resultJson, Response::HTTP_CREATED, $headers);
            } else {
                $headers = array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'Content-Type' => 'application/json',
                );
                $view = $this->view($result, Response::HTTP_BAD_REQUEST, $headers);
            }

            return $this->handleView($view);
        } catch (InvalidFormException $exception) {
            $headers = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'Content-Type' => 'application/json',
            );
            $view = $this->view($result, Response::HTTP_BAD_REQUEST, $headers);

            return $this->handleView($view);
        }
    }

    /**
     * Create a command course from the given json data.
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Creates a new command course from the submitted data.",
     *   parameters={
     *  {"name"="client", "dataType"="integer", "required"=true, "description"="client Id"},
     *  {"name"="startedAddress", "dataType"="string", "required"=true, "description"="Course departure address"},
     *  {"name"="longDep", "dataType"="float", "required"=true, "description"="Departure address longitude"},
     *  {"name"="latDep", "dataType"="float", "required"=true, "description"="Departure address latitude"},
     *  {"name"="arrivalAddress", "dataType"="string", "required"=true, "description"="Course arrival address"},
     *  {"name"="longArr", "dataType"="float", "required"=true, "description"="Arrival address longitude"},
     *  {"name"="latArr", "dataType"="float", "required"=true, "description"="Arrival address latitude"},
     *  {"name"="departureDate", "dataType"="string", "required"=true, "description"="departure date time Y-m-d H:i:s"},
     *  {"name"="personNumber", "dataType"="integer", "required"=true, "description"="Person number for the course"}
     *  },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Post("/course/command", name="command_course", options={ "method_prefix" = false })
     * @View(
     *  serializerGroups={"Details", "Public"}
     * )
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postCommandCourseAction(Request $request)
    {
        try {
            $jsonData = json_decode($request->getContent(), true); // "true" to get an associative array

            /** @var array $result */
            $result = $this->get('cab_api.course.handler')->post($jsonData);
            if (array_key_exists('commandCourseId', $result)){
                $commandId = $result['commandCourseId'];
                unset($result['commandCourseId']);
                $view = $this->view(array(
                    'status' => Response::HTTP_CREATED,
                    'commandCourseId' => $commandId,
                    'result' => $result,

                ));
            } else {
                $headers = array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'Content-Type' => 'application/json',
                );
                $view = $this->view($result, Response::HTTP_BAD_REQUEST, $headers);
            }

            return $this->handleView($view);
        } catch (InvalidFormException $exception) {
            $headers = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'Content-Type' => 'application/json',
            );
            $view = $this->view(
                array(
                    'status' => 0,
                    'response' => $exception->getMessage(),
                ),
                Response::HTTP_BAD_REQUEST,
                $headers
            );
            return $this->handleView($view);
        }
    }

    /**
     * Confirm booking course by customer
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Confirm booking course by customer",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="Id of driver."}
     *  },
     * @Annotations\Put("/course/confirm-booking", name="driver_confirm_booking", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function confirmBookingAction(Request $request)
    {
        $aData = json_decode($request->getContent(), true);

        $course = $this->get('cab.course_manager')->loadCourse($aData['course_id']);
        $oDriver = $this->get('fos_user.user_manager')->findUserBy(array('id' => $aData['driver_id']));
        $oVehicle = $this->get('cab_api.vehicule_handler')->getLastAffectVehiculeByDriverId($aData['driver_id']);

        $this->get('cab.course_manager')->setAttr($course, 'driver', $oDriver);
        $this->get('cab.course_manager')->setAttr($course, 'vehicle', $oVehicle);
        $this->get('cab.course_manager')->saveCourse($course);
        // send push notification to driver
        $messagePush = 'A new trip is created by customer, can you accept ?';

        $data = array(
            'type_notification' => 'driver_notification_course',
            'subject_notification' => 'Acceptez ou refusez un course',
            'alert' => 'veuillez répondre, merci',
            'id_course' => $aData['course_id'],
            'badge' => '1',
            'sound' => 'ringingbell.mp3',
            'type' => '1',
        );


        $this->get('cab_course.push_notification.push')->getServiceByOS(
            $oDriver->getDevices()->toArray(),
            $messagePush,
            $data
        );

        $view = $this->view(
            array(
                'status' => Response::HTTP_OK,
                'response' => 'Booking is notified to driver',
            ),
            Response::HTTP_OK
        );

        return $this->handleView($view);
    }

    /**
     * Confirm command course by customer
     *
     * @View(serializerGroups={"Default","Details"})
     * @Annotations\Patch("/course/command/confirm", name="customer_command_confirm", options={ "method_prefix" = false })
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Confirm command course by customer",
     *   parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="company_id", "dataType"="integer", "required"=true, "description"="Id of company."},
     *      {"name"="details_tarif_id", "dataType"="integer", "required"=true, "description"="Id of details tarif."},
     *      {"name"="tarif_id", "dataType"="integer", "required"=true, "description"="Id of tarif applicable belong the company."},
     *      {"name"="tarif_ht", "dataType"="integer", "required"=true, "description"="Tarif without tax."},
     *      {"name"="tarif_ttc", "dataType"="integer", "required"=true, "description"="Tarif all taxes included."},
     *      {"name"="tarif_approach_time", "dataType"="integer", "required"=false, "Approach time tarif."},
     *      {"name"="estimated_tarif", "dataType"="integer", "required"=false, "Estimated tarif."}
     *  },
     *   statusCodes = {
     *     201 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *
     *
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function confirmCommandAction (Request $request)
    {
        try {
            $params = json_decode($request->getContent(), true);
            $em = $this->get('doctrine.orm.entity_manager');
            $course = $em->getRepository('CABCourseBundle:Course')->find($params['course_id']);

            if ($course->getCourseStatus() != Course::STATUS_COMMAND) {
                $view = $this->view(
                    array(
                        'status' => Response::HTTP_BAD_REQUEST,
                        'response' => 'The Command has been confirmed',
                    ),
                    Response::HTTP_BAD_REQUEST
                );

                return $this->handleView($view);
            }
            $tarifDetails = null;
            $tarif = null;
            if ($params['details_tarif_id'] != 0) {
                $tarifDetails = $em->getRepository('CABCourseBundle:DetailsTarif')->find($params['details_tarif_id']);
            }
            if ($params['tarif_id'] != 0) {
                $tarif = $em->getRepository('CABCourseBundle:Tarif')->find($params['tarif_id']);
            }


            $company = $em->getRepository('CABCourseBundle:Company')->find($params['company_id']);
            $course->setCompany($company);
            $course->setCourseDetailsTarif($tarifDetails);

            $course->setCourseStatus(Course::STATUS_CREATED);

            $em->persist($course);
            $em->flush();

            // command is confirmed
            $listeDrivers = $em->getRepository('CABUserBundle:User')->getDriverByCompanyAPI($company->getId());
            /** @var User $driver */
            foreach ($listeDrivers as $driver) {
                // send push notification to driver
                $messagePush = 'A new trip is created by customer, can you accept ?';




                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'Nouvelle course disponible',
                    'alert' => 'veuillez répondre, merci',
                    'badge' => '1',
                    'type' => '0',

                );


                $this->get('cab_course.push_notification.push')->getServiceByOS(
                    $driver->getDevices()->toArray(),
                    $messagePush,
                    $data
                );
            }

            $view = $this->view(
                array(
                    'status' => Response::HTTP_OK,
                    'response' => 'Booking is notified to driver',
                ),
                Response::HTTP_OK
            );
        } catch (HttpException $e) {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'response' => 'Command not accepted',
                ),
                Response::HTTP_BAD_REQUEST
            );
        }

        return $this->handleView($view);
    }

    /**
     * Cancel booking course by customer
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Cancel booking course by customer",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="customer_id", "dataType"="integer", "required"=true, "description"="Id of customer."}
     *  },
     * @Annotations\Put("/course/cancel-booking", name="customer_cancel_booking", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function cancelCourseAction(Request $request)
    {
        $aData = json_decode($request->getContent(), true);
        $course = $this->get('cab.course_manager')->loadCourse($aData['course_id']);
        $oCustomer = $this->get('fos_user.user_manager')->findUserBy(array('id' => $aData['customer_id']));
        $result = $this->get('cab.course_manager')->cancelCourse($course, $oCustomer);

        if ($course->getDriver()) {
            $oDriver = $course->getDriver();
            // send push notification to driver
            $messagePush = 'CETTE COURSE EST ANNULEE';
            //$data = array(
            //    'type_notification' => 'driver_notification_course',
            //    'subject_notification' => 'Course cancelled',
            //    'id_course' => $aData['course_id'],
            //);
            $data = array(
                'type_notification' => 'driver_notification_course',
                'subject_notification' => 'A course is canceled by the customer',
                'type' => '0',
                'alert' => $messagePush,
                'id_course' => $aData['course_id'],
                //'sound' => 'ringingbell.mp3',
            );
            $this->get('cab_course.push_notification.push')->getServiceByOS(
                $oDriver->getDevices()->toArray(),
                $messagePush,
                $data
            );
        }

        if ($result['status'] == 1) {
            $status = Response::HTTP_OK;
            $response = $result['response'];
        } else {
            $status = Response::HTTP_BAD_REQUEST;
            $response = $result['response'];
        }
        /** @var Course $course */
        $view = $this->view(
            array(
                'status' => $status,
                'response' => $response,
                'course' => array('course_date'=>date_format($course->getDepartureDate(),"d/m"),'course_time'=>date_format($course->getDepartureTime(),"H:i"))
            ),
            $status
        );

        return $this->handleView($view);
    }

    /**
     * Cancel booking course by driver
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Cancel booking course by customer",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="Id of customer."}
     *  },
     * @Annotations\Put("/course/driver-cancel-booking", name="driver_cancel_booking", options={ "method_prefix" =
     *                                                   false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function cancelDriverCourseAction(Request $request)
    {
        $aData = json_decode($request->getContent(), true);
        $course = $this->get('cab.course_manager')->loadCourse($aData['course_id']);
        $oDriver = $this->get('fos_user.user_manager')->findUserBy(array('id' => $aData['driver_id']));
        $result = $this->get('cab.course_manager')->cancelDriverCourse($course, $oDriver);

        if ($result['status'] == 1) {
            $oCustomer = $course->getClient();
            $messagePush = 'La course est annulée par le chauffeur';
            //$data = array(
            //    'type_notification' => 'driver_notification_course',
            //    'subject_notification' => 'la course est annulée',
            //    'id_course' => $oCustomer->getId(),
            //);
            $data = array(
                'type_notification' => 'driver_notification_course',
                'subject_notification' => 'The race is cancelled by driver',
                'type' => '0',
                'id_course' => $oCustomer->getId(),
                'alert' => $messagePush,
                'id_course' => $aData['course_id'],
                'sound' => 'ringingbell.mp3',
            );
            $this->get('cab_course.push_notification.push')->getServiceByOS(
                $oCustomer->getDevices()->toArray(),
                $messagePush,
                $data,
                'customer'
            );

            $status = Response::HTTP_OK;
            $response = $result['response'];
        } else {
            $status = Response::HTTP_BAD_REQUEST;
            $response = $result['response'];
        }
        $view = $this->view(
            array(
                'status' => $status,
                'response' => $response,
                'id_course' => $course->getId(),
                "status_update" => $course->getcourseStatus(),
            ),
            $status
        );

        return $this->handleView($view);
    }

    /**
     * Get courses by driver nombre
     * @View()
     * @Get("/courses/driver/{driverId}", name="get_courses_driver", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get courses by driver",
     *  resource=false,
     *  authentication=false,
     *  description="Get courses by driver",
     *  parameters={
     *      {"name"="driverId", "dataType"="integer", "required"=true, "description"="Id of driver."}
     *  },
     *
     * statusCodes={
     *         200 = "Returned when successful",
     *         403 = "Returned when the user is not authorized to access to resource",
     *         404 = {
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCoursesByDriverAction($driverId)
    {
        $result = $this->get('cab_api.course.handler')->getCoursesByDriverId($driverId);

        if (false === $result) {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                ),
                Response::HTTP_BAD_REQUEST
            );
        } else {
            $result = array(
                'number_of_courses' => $result,
                'status' => Response::HTTP_OK,
            );

            $view = $this->view($result, Response::HTTP_OK);
        }

        return $this->handleView($view);
    }

    /**
     * Get courses by driver CA
     * @View()
     * @Get("/coursesca/driver/{driver_id}", name="get_coursesca_driver", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get courses by driver",
     *  resource=false,
     *  authentication=false,
     *  description="Get courses by driver",
     *  parameters={
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="Id of driver."}
     *  },
     *
     * statusCodes={
     *         200 = "Returned when successful",
     *         403 = "Returned when the user is not authorized to access to resource",
     *         404 = {
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCoursesCaByDriverAction($driver_id)
    {
        $result = $this->get('cab_api.course.handler')->getCoursesCaByDriverId($driver_id);

        if (false === $result) {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                ),
                Response::HTTP_BAD_REQUEST
            );
        } else {
            $result = array(
                'CA_number' => $result,
                'status' => Response::HTTP_OK,
            );

            $view = $this->view($result, Response::HTTP_OK);
        }

        return $this->handleView($view);
    }

    /**
     * Get courses by customer CA
     * @View()
     * @Get("/coursesca/customer/{customerId}", name="get_coursesca_customer", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get courses CA by customer",
     *  resource=false,
     *  authentication=false,
     *  description="Get courses CA by customer",
     *  parameters={
     *      {"name"="customerId", "dataType"="integer", "required"=true, "description"="Id of customer."}
     *  },
     *
     * statusCodes={
     *         200 = "Returned when successful",
     *         403 = "Returned when the user is not authorized to access to resource",
     *         404 = {
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCoursesCaByCustomerAction($customerId)
    {
        $result = $this->get('cab_api.course.handler')->getCoursesCaByCustomerId($customerId);

        if (false === $result) {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                ),
                Response::HTTP_BAD_REQUEST
            );
        } else {
            $result = array(
                'CA_number' => $result,
                'status' => Response::HTTP_OK,
            );

            $view = $this->view($result, Response::HTTP_OK);
        }

        return $this->handleView($view);
    }

    /**
     * Get courses by customer
     * @View()
     * @Get("/courses/customer/{customerId}", name="get_courses_customer", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get courses by customer",
     *  resource=false,
     *  authentication=false,
     *  description="Get courses by customer",
     *  parameters={
     *      {"name"="customerId", "dataType"="integer", "required"=true, "description"="Id of customer."}
     *  },
     *
     * statusCodes={
     *         200 = "Returned when successful",
     *         403 = "Returned when the user is not authorized to access to resource",
     *         404 = {
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCoursesByCustomerAction($customerId)
    {
        $result = $this->get('cab_api.course.handler')->getCoursesByCustomerId($customerId);

        if (false === $result) {
            $view = $this->view(
                array(
                    'status' => Response::HTTP_BAD_REQUEST,
                ),
                Response::HTTP_BAD_REQUEST
            );
        } else {
            $result = array(
                'number_of_courses' => $result,
                'status' => Response::HTTP_OK,
            );

            $view = $this->view($result, Response::HTTP_OK);
        }

        return $this->handleView($view);
    }

    /**
     * Get courses by status
     * @View()
     * @Get("/course/status/{courseId}/{userId}/{roleUser}", name="get_course_status", options={ "method_prefix" = false })
     * @ApiDoc(
     *  section="Course",
     *  documentation="Get courses status",
     *  resource=false,
     *  authentication=false,
     *  description="Get courses status",
     *  parameters={
     *      {"name"="courseId", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="userId", "dataType"="integer", "required"=true, "description"="Id of driver or customer."},
     *      {"name"="roleUser", "dataType"="string", "required"=true, "description"="customer or driver"}
     *  },
     *
     * statusCodes={
     *         200 = "Returned when successful",
     *         403 = "Returned when the user is not authorized to access to resource",
     *         404 = {
     *           "Returned when the user is not found",
     *           "Returned when something else is not found"
     *         }
     *     }
     * )
     *
     * @return array
     */
    public function getCoursesStatusAction($courseId, $userId, $roleUser)
    {
        $course = $this->get('cab.course_manager')->loadCourse($courseId);

        if (($userId == $course->getDriver()->getId() && $roleUser === 'driver') ||
            ($userId == $course->getClient()->getId() && $roleUser === 'customer')
        ) {


            if (in_array($userId, array($course->getDriver()->getId(), $course->getClient()->getId()))) {
                $statusCourse = $course->getCourseStatus();
                switch ($statusCourse) {
                    case 0:
                        $status_course_content = 'Course created';
                        break;

                    case 1:
                        $status_course_content = 'Course Payed';
                        break;

                    case 2:
                        $status_course_content = 'Course rejected';
                        break;

                    case 3:
                        $status_course_content = 'Course cancelled by client';
                        break;

                    case 4:
                        $status_course_content = 'Course affected';
                        break;

                    case 5:
                        $status_course_content = 'Course updated by customer';
                        break;

                    case 6:
                        $status_course_content = 'Course done';
                        break;

                    case 7:
                        $status_course_content = 'Course payed and updated by customer';
                        break;

                    case 8:
                        $status_course_content = 'Course cancelled by admin';
                        break;

                    case 9:
                        $status_course_content = 'status on race driver';
                        break;

                    case 10:
                        $status_course_content = 'course payed cach';
                        break;

                    case 11:
                        $status_course_content = 'course waiting pay';
                        break;

                    case 12:
                        $status_course_content = 'course accepted by driver';
                        break;

                    case 13:
                        $status_course_content = 'arrived driver';
                        break;

                    case 14:
                        $status_course_content = 'no response';
                        break;

                    case 14:
                        $status_course_content = 'course cancelled by driver';
                        break;
                }

                $result = array(
                    'status' => Response::HTTP_ACCEPTED,
                    'status_course' => $statusCourse,
                    'status_course_msg' => $status_course_content,
                );
                $view = $this->view($result, Response::HTTP_OK);
            } else {
                $result = array(
                    'status' => Response::HTTP_BAD_REQUEST,
                    'message error' => 'check the user id',
                );
                $view = $this->view($result, Response::HTTP_BAD_REQUEST);
            }
        } else {
            $result = array(
                'status' => Response::HTTP_BAD_REQUEST,
                'message error' => 'check the user id and his role',
            );

            $view = $this->view($result, Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * Calcul course Price
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Calcul course Price",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="Id of driver."}
     *  },
     * @Annotations\Put("/course/calcul-new-price", name="calcul_new_price", options={ "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function calculNewPriceCourseAction(Request $request)
    {
        $aData = json_decode($request->getContent(), true);
        $course = $this->get('cab_api.course.handler')->updateCourse($aData);
        $oCourse = $this->get('cab.tarif.manager')->getTarifCourse(array(), $course);

        $result = array();

        if (!array_key_exists('status', $oCourse)) {


            /** @var Course $item */

            $driverSearched = null;

            $course_details_tarif = null;
            $course_estimated = null;
            $indicated_depart = '';
            $indicated_arrival = '';
            $indicated_message = '';
            $currency = 'EUR';
            $comp = null;


            if ($oCourse->getDriver() !== null) {
                //$driverSearched = $item->getDriver()->getId();
                $driverSearched['id'] = $oCourse->getDriver()->getId();
                $driverSearched['first_name'] = $oCourse->getDriver()->getfirstName();
                $driverSearched['last_name'] = $oCourse->getDriver()->getLastName();
                $driverSearched['phone_mobile'] = $oCourse->getDriver()->getPhoneMobile();
                $driverSearched['id'] = $oCourse->getDriver()->getId();

            }
            if ($oCourse->getCourseDetailsTarif() !== null) {
                $course_details_tarif = $oCourse->getCourseDetailsTarif()->getId();
                $course_estimated = $oCourse->getCourseDetailsTarif()->getEstimatedTarif();
                $forfait = $oCourse->getCourseDetailsTarif()->getIsForfait();
            } else {
                $course_details_tarif = " ";
                $course_estimated = " ";
            }


            $forfait = $oCourse->getCourseDetailsTarif()->getIsForfait();


            if ($oCourse->getpriceHt() !== null) {
                $priceHT = $oCourse->getpriceHt();
            } else {
                $priceHT = "";
            }
            if ($oCourse->getpriceTTC() !== null) {
                $priceTTC = $oCourse->getpriceTTC();
            } else {
                $priceTTC = "";
            }


            $result[] = array(
                'id' => $oCourse->getId(),
                'driver' => array($driverSearched),
                'course_details_tarif' => array('id' => $course_details_tarif, "price_estimate" => $course_estimated),
                'priceHT' => $priceHT,
                'priceTTC' => $priceTTC,
                'currency' => $oCourse->getCompany()->getFormat()->getcurrencyCode(),
                'forfait' => $oCourse->getCourseDetailsTarif()->getIsForfait(),
            );


            $view = array(
                'course' => $result,

            );

            return $view;
        }else{
            return $oCourse;
        }


    }

    /**
     * Set Price course after course is complete (action made by the driver)
     *
     * @ApiDoc(
     *   section="Course",
     *   resource = true,
     *   description = "Update the price and the payment mode of a course",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when error occurs"
     *   }
     * )
     *  parameters={
     *      {"name"="course_id", "dataType"="integer", "required"=true, "description"="Id of course."},
     *      {"name"="driver_id", "dataType"="integer", "required"=true, "description"="Id of driver."}
     *      {"name"="price", "dataType"="decimal", "required"=true, "description"="The real price."}
     *      {"name"="payment_mode", "dataType"="integer", "required"=false, "description"="The payment mode; cach or
     *      CB."}
     *      {"name"="course_status", "dataType"="integer", "required"=false, "description"="The course status must
     *      equal 6."}
     *  },
     * @Annotations\Put("/course/update-price-and-payment-mode", name="update_price_and_payment_mode", options={
     *                                                           "method_prefix" = false })
     * @Annotations\View()
     *
     * @param Request $request the request object
     *
     * @return int  status code
     */
    public function updatePriceAndPaymentModeAction(Request $request)
    {

    	$aData = json_decode($request->getContent(), true);

		if(is_array($aData))
		{
			$result = $this->get('cab.course_manager')->updatePriceAndPaymentModeApi($aData);


			if($result['status'] == 1)
			{
				$status   = Response::HTTP_OK;
				$response = $result['response'];
			}
			else
			{
				$status   = Response::HTTP_BAD_REQUEST;
				$response = $result['response'];
			}
		}
		else{
			$status   = Response::HTTP_BAD_REQUEST;
			$response = 'Not Found';
		}

        $view = $this->view(
            array(
                'status' => $status,
                'course' => $response,
            ),
            $status
        );

        return $this->handleView($view);
    }

	/**
	 * Create a command course from the given json data.
	 *
	 * @ApiDoc(
	 *   section="Course",
	 *   resource = true,
	 *   description = "Post a comment for the course.",
	 *   parameters={
	 *  {"name"="course", "dataType"="integer", "required"=true, "description"="Course ID"},
	 *  {"name"="comment", "dataType"="string", "required"=true, "description"="Comment"},
	 *  },
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     400 = "Returned when the form has errors"
	 *   }
	 * )
	 *
	 * @Annotations\Patch("/course/comment", name="comment_course", options={ "method_prefix" = false })
	 * @View(
	 *  serializerGroups={"Details", "Public"}
	 * )
	 * @param Request $request the request object
	 *
	 * @return FormTypeInterface|View
	 */
	public function patchCommentCourseAction(Request $request)
	{
		try {
			$jsonData = json_decode($request->getContent(), true); // "true" to get an associative array

			/** @var array $result */
			$result = $this->get('cab_api.course.handler')->patchComment($jsonData);
			if ($result['status']){
				$headers = array(
					'status' => Response::HTTP_OK,
					'Content-Type' => 'application/json',
				);
				$view = $this->view($result, Response::HTTP_OK, $headers);
			} else {
				$headers = array(
					'status' => Response::HTTP_BAD_REQUEST,
					'Content-Type' => 'application/json',
				);
				$view = $this->view($result, Response::HTTP_BAD_REQUEST, $headers);
			}

			return $this->handleView($view);
		} catch (InvalidFormException $exception) {
			$headers = array(
				'status' => Response::HTTP_BAD_REQUEST,
				'Content-Type' => 'application/json',
			);
			$view = $this->view(
				array(
					'status' => 0,
					'response' => $exception->getMessage(),
				),
				Response::HTTP_BAD_REQUEST,
				$headers
			);

			return $this->handleView($view);
		}
	}

    /**
     * Shortcut to throw a AccessDeniedException($message) if the user is not authenticated
     *
     * @param String $message The message to display (default:'warn.user.notAuthenticated')
     */
    protected function forwardIfNotAuthenticated($message = 'warn.user.notAuthenticated')
    {
        //@TODO return a json response in failed case: http://symfony.com/doc/current/bundles/FOSRestBundle/4-exception-controller-support.html
        if (!is_object($this->getUser())) {
            throw new AccessDeniedException($message);
        }
    }

}
