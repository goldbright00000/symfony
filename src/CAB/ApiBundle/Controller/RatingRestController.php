<?php
namespace CAB\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use CAB\CourseBundle\Entity\Rating;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Response;

/**
* Rating Rest API
*/
class RatingRestController extends FOSRestController
{
	const FIVE_RATING = 5;

	/**
	 * Persist new driver rating from the given json data
	 *
	 * @ApiDoc(
     *   section="Rating",
	 *   resource = true,
	 *   description = "Persist new driver rating from the given json data",
	 *   output = "CAB\CourseBundle\Entity\Rating",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when something is wrong"
	 *   }
	 * )
	 *
     * @Annotations\Post("/rating/driver", name="post_driver_rating", options={ "method_prefix" = false })
	 * @Annotations\View()
	 *
	 * @param int  $driver
	 * @return string
	 */
	public function postDriverRatingAction(Request $request)
	{
		$ratingHandler 	= $this->container->get('cab_api.rating_handler');
		$data 			= json_decode($request->getContent(), true);
		$getResult 		= $ratingHandler->postDriver($data);

	    if (false === $getResult) {
			$view = $this->view(array(
				'status' => Response::HTTP_BAD_REQUEST
			), Response::HTTP_BAD_REQUEST);
		} else {
			$view = $this->view(array(
				'status' => Response::HTTP_CREATED
			), Response::HTTP_CREATED);
		}

        return $this->handleView($view);
	}

	/**
	 * Persist new client rating from the given json data
	 *
	 * @ApiDoc(
     *   section="Rating",
	 *   resource = true,
	 *   description = "Persist new client rating from the given json data",
	 *   output = "CAB\CourseBundle\Entity\Rating",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when something is wrong"
	 *   }
	 * )
	 *
     * @Annotations\Post("/rating/client", name="post_client_rating", options={ "method_prefix" = false })
	 * @Annotations\View()
	 *
	 * @param int  $client
	 * @return string
	 */
	public function postClientRatingAction(Request $request)
	{
		$ratingHandler 	= $this->container->get('cab_api.rating_handler');
		$data 			= json_decode($request->getContent(), true);
		$getResult 		= $ratingHandler->postClient($data);

	    if (false === $getResult) {
			$view = $this->view(array(
				'status' => Response::HTTP_BAD_REQUEST
			), Response::HTTP_BAD_REQUEST);
		} else {
			$view = $this->view(array(
				'status' => Response::HTTP_CREATED
			), Response::HTTP_CREATED);
		}

        return $this->handleView($view);
	}

	/**
	 * Get average driver's ratings in all periodes
	 *
	 * @ApiDoc(
     *   section="Rating",
	 *   resource = true,
	 *   description = "Get average driver's ratings in all periodes",
	 *   output = "CAB\CourseBundle\Entity\Rating",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when something is wrong"
	 *   }
	 * )
	 *
     * @Annotations\Get("/rating/driver/allPeriodsRatings/{driver_id}", name="get_driver_rating_allperiods", options={ "method_prefix" = false })
	 * @Annotations\View()
	 *
	 * @param int       $driver_id    Id of driver
	 * @return string
	 */
	public function getAverageDriverRatingsInAllPeriodsAction($driver_id)
	{
		$ratingHandler 	= $this->container->get('cab_api.rating_handler');
		$getResult 		= $ratingHandler->getAverageDriverRatingsInAllPeriods($driver_id);

		$view = $this->view($getResult, Response::HTTP_OK);

        return $this->handleView($view);
	}

	/**
	 * Get average driver's ratings in current working weeek
	 *
	 * @ApiDoc(
     *   section="Rating",
	 *   resource = true,
	 *   description = "Get average driver's ratings in current working weeek",
	 *   output = "CAB\CourseBundle\Entity\Rating",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when something is wrong"
	 *   }
	 * )
	 *
     * @Annotations\Get("/rating/driver/currentWorkingWeek/{driver_id}", name="get_driver_rating_currentworkingweek", options={ "method_prefix" = false })
	 * @Annotations\View()
	 *
	 * @param int       $driver_id    Id of driver
	 * @return string
	 */
	public function getAverageDriverRatingsInWorkingWeekAction($driver_id)
	{
		$ratingHandler 	= $this->container->get('cab_api.rating_handler');
		$getResult 		= $ratingHandler->getAverageDriverRatingsInWorkingWeek($driver_id);

		$view = $this->view($getResult, Response::HTTP_OK);

        return $this->handleView($view);
	}

	/**
	 * Get average customer's ratings in all periodes
	 *
	 * @ApiDoc(
     *   section="Rating",
	 *   resource = true,
	 *   description = "Get average customer's ratings in all periodes",
	 *   output = "CAB\CourseBundle\Entity\Rating",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when something is wrong"
	 *   }
	 * )
	 *
     * @Annotations\Get("/rating/customer/allPeriodsRatings/{customer_id}", name="get_customer_rating_allperiods", options={ "method_prefix" = false })
	 * @Annotations\View()
	 *
	 * @param int       $customer_id    Id of customer
	 * @return string
	 */
	public function getAverageCustomerRatingsInAllPeriodsAction($customer_id)
	{
		$ratingHandler 	= $this->container->get('cab_api.rating_handler');
		$getResult 		= $ratingHandler->getAverageCustomerRatingsInAllPeriods($customer_id);

		$view = $this->view($getResult, Response::HTTP_OK);

        return $this->handleView($view);
	}

	/**
	 * Get average customer's ratings in current working weeek
	 *
	 * @ApiDoc(
     *   section="Rating",
	 *   resource = true,
	 *   description = "Get average customer's ratings in current working weeek",
	 *   output = "CAB\CourseBundle\Entity\Rating",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when something is wrong"
	 *   }
	 * )
	 *
     * @Annotations\Get("/rating/customer/currentWorkingWeek/{customer_id}", name="get_customer_rating_currentworkingweek", options={ "method_prefix" = false })
	 * @Annotations\View()
	 *
	 * @param int       $customer_id    Id of customer
	 * @return string
	 */
	public function getAverageCustomerRatingsInWorkingWeekAction($customer_id)
	{
		$ratingHandler 	= $this->container->get('cab_api.rating_handler');
		$getResult 		= $ratingHandler->getAverageCustomerRatingsInWorkingWeek($customer_id);

		$view = $this->view($getResult, Response::HTTP_OK);

        return $this->handleView($view);
	}
}