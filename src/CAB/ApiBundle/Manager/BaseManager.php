<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:44
 */
namespace CAB\ApiBundle\Manager;

abstract class BaseManager
{
    protected function persistAndFlush($entity, $andFlush = true)
    {
        if (!$this->em->isOpen()) {
            $this->entityManager = $this->em->create(
                $this->em->getConnection(),
                $this->em->getConfiguration()
            );
        }
        
        $this->em->persist($entity);
        if ($andFlush) {
            $this->em->flush();
        }
    }
}