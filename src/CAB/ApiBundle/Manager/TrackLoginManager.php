<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\ApiBundle\Manager;


use CAB\ApiBundle\Entity\TrackLogin;
use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Manager\BaseManager;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Doctrine\ORM\Mapping\Entity
 * @Doctrine\ORM\Mapping\Table(name="track_login_manager")
 */
class TrackLoginManager extends BaseManager
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $user
     * @return null|object
     */
    public function loadAllUserTrack($user) {
        return $this->getRepository()
            ->findOneBy(array('user' => $user));
    }


    /**
     * @param array $trackLoginParams
     * @param bool $andFlush
     */
    public function createTrackLogin(array $trackLoginParams, $andFlush = true)
    {
        $oTrackLogin = new TrackLogin();
        $oTrackLogin->setUser($trackLoginParams['user']);
        $oTrackLogin->setStatus($trackLoginParams['status']);
        $this->persistAndFlush($oTrackLogin, $andFlush);
    }

    /**
     * persist TrackLogin entity
     *
     * @param TrackLogin $oTrackLogin
     */
    public function persistTrackLogin(TrackLogin $oTrackLogin, $andFlush = true)
    {
        $this->persistAndFlush($oTrackLogin, $andFlush);
    }

    public function getRepository()
    {
        return $this->em->getRepository('CABApiBundle:TrackLogin');
    }

}