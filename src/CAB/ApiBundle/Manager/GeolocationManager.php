<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\ApiBundle\Manager;


use CAB\ApiBundle\Entity\Geolocation;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class GeolocationManager
 *
 * @package CAB\ApiBundle\Manager
 */
class GeolocationManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Load driver
     *
     * @param UserInterface $user
     *
     * @return null|object
     */
    public function loadDriverGeolocation(UserInterface $user)
    {
        return $this->getRepository()
            ->findOneBy(array('driver' => $user));
    }


    /**
     * Create Geolocation object
     *
     * @param array $geolocationParams
     * @param bool  $andFlush
     */
    public function createGeolocation(array $geolocationParams, $andFlush = true)
    {
        $oGeolocation = new Geolocation();
        $oGeolocation->setDriver($geolocationParams['user']);
        $oGeolocation->setStatus($geolocationParams['status']);
        $this->persistAndFlush($oGeolocation, $andFlush);
    }

    /**
     * Repository
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Geolocation');
    }

}