<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/10/2015
 * Time: 16:27
 */

namespace CAB\ApiBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use CAB\ApiBundle\Security\Authentication\Token\WsseUserToken;
use Psr\Log\LoggerInterface;

class WsseListener implements ListenerInterface
{
    protected $tokenStorage;
    protected $authenticationManager;
    protected $logger;

    public function __construct(TokenStorageInterface $tokenStorage,
                                AuthenticationManagerInterface $authenticationManager,
                                LoggerInterface $logger)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->logger = $logger;
    }

    /*
     * This listener checks the request for the expected X-WSSE header, matches the value returned for the expected
     * WSSE information, creates a token using that information, and passes the token on to the authentication manager.
     * If the proper information is not provided, or the authentication manager throws an AuthenticationException,
     * a 403 Response is returned.
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $wsseRegex = '/UsernameToken Username="([^"]+)", PasswordDigest="([^"]+)", Nonce="([^"]+)", Created="([^"]+)"/';
        if (!$request->headers->has('x-wsse') || 1 !== preg_match($wsseRegex, $request->headers->get('x-wsse'),
                $matches)
        ) {
            return;
        }

        $token = new WsseUserToken();
        $token->setUser($matches[1]);

        $token->digest = $matches[2];
        $token->nonce = $matches[3];
        $token->created = $matches[4];

        $date = date('Y-m-d H:i:s');

        $this->logger->info('##################################################################');
        $this->logger->info($date.' : Login from API');
        $this->logger->info($date.' : Username : '.$matches[1]);
        $this->logger->info($date.' : Nonce : '.$token->nonce);
        $this->logger->info($date.' : Token : '.$token->digest);
        $this->logger->info($date.' : Created : '.$token->created);


        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);
            $this->logger->info($date.' : Success authenticate for '.$matches[1]);
            return;
        } catch (AuthenticationException $failed) {
            $failedMessage = 'WSSE Login failed for '.$token->getUsername().'. Why ? '.$failed->getMessage();
            //$this->logger->warning($failedMessage);
            $this->logger->warning($date.' : Faild authenticate for '.$failedMessage);
            // Deny authentication with a '403 Forbidden' HTTP response
            $response = new Response();
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            $response->setContent($failedMessage);
            $event->setResponse($response);
            return;
        }
    }
}