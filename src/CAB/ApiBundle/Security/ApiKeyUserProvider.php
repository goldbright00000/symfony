<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 17/12/2017
 * Time: 22:58
 */

namespace CAB\ApiBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use CAB\ApiBundle\Manager\UserManager;

class ApiKeyUserProvider implements UserProviderInterface
{
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param string $apiKey
     * @return null|\CAB\UserBundle\Entity\User
     */
    public function getUserForApiKey($apiKey)
    {
        // @TODO Add custom finding for user tasm and superadmin
        return $this->userManager->findUserBy(['apiToken' => $apiKey]);
    }

    public function loadUserByUsername($username)
    {
        return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('ROLE_API')
        );
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // Throwing this exception is proper to make things stateless
        return $user;
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}