<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\ApiBundle\Entity\TrackLogin
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_track_login")
 * @ORM\Entity(repositoryClass="CAB\ApiBundle\Entity\TrackLoginRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class TrackLogin {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="trackUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $user;

    /**
     * @var date $trackingDate
     *
     * @ORM\Column(type="datetime", name="tracking_date", nullable=false)
     */
    private $trackingDate;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", name="status", nullable=true)
     */
    private $status = 1;

    public function __toString(){
        return (string)$this->getId();
    }
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setCreatedAtValue()
    {
        $this->trackingDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trackingDate
     *
     * @param \DateTime $trackingDate
     * @return TrackLogin
     */
    public function setTrackingDate($trackingDate)
    {
        $this->trackingDate = $trackingDate;

        return $this;
    }

    /**
     * Get trackingDate
     *
     * @return \DateTime
     */
    public function getTrackingDate()
    {
        return $this->trackingDate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return TrackLogin
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param \CAB\UserBundle\Entity\User $user
     * @return TrackLogin
     */
    public function setUser(\CAB\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
