<?php
/**
 * Created by PhpStorm.
 * User: m.benhedna
 * Date: 11/10/2015
 * Time: 17:00
 */

namespace CAB\ApiBundle\DependencyInjection\Security\Factory;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;

class WsseFactory implements SecurityFactoryInterface
{
    /*
     * Method adds the listener and authentication provider to the DI container for the appropriate security context.
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.wsse.'.$id;
        $container
            ->setDefinition($providerId, new DefinitionDecorator('wsse.security.authentication.provider'))
            ->replaceArgument(0, new Reference($userProvider))
        ;

        $listenerId = 'security.authentication.listener.wsse.'.$id;
        $listener = $container->setDefinition($listenerId,
            new DefinitionDecorator('wsse.security.authentication.listener'));

        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /*
     * Method which must be of type pre_auth, form, http, and remember_me and defines the position at which the
     * provider is called.
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /*
     * Method which defines the configuration key used to reference the provider in the firewall configuration.
     */
    public function getKey()
    {
        return 'wsse';
    }

    /*
     * Method which is used to define the configuration options underneath the configuration key in your security config
     */
    public function addConfiguration(NodeDefinition $node)
    {
    }
}