<?php

namespace CAB\CourseBundle\Controller;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Payment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class CourseController
 *
 * @package CAB\CourseBundle\Controller
 */
class CourseController extends Controller {

    /**
     * @Route("/create-booking", name="create_course")
     * @Method({"POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request) {
        $aParams = $this->getParamsForCreateBooking($request);

        try {
            $status = 0;
            $result = $this->get('cab_course.handler.calcul_price_handler')->createBooking($aParams);

        } catch (\Exception $e) {

            $status = 0;
            $result = $e->getMessage();
			echo '<pre>';print_r($result);die;
        }

        $session = $request->getSession();
        $session->remove('course');
        $session->clear();
        $session->set('course', $result);
        $session->getFlashBag()->add('course', $result);

        return $result;
    }

    /**
     * @Route("confirm-booking", name="confirm_booking", options={"expose"=true})
     *
     * @Method({"POST", "GET"})
     * @Template()
     *
     * @param Request $request
     *
     * @return Response $response
     *
     * @throws \Exception
     */
    public function confirmbookingAction(Request $request)
    {
        list($session, $paramsCourse, $user, $course) = $this->initConfirmBooking($request);
        if (!count($paramsCourse)) {
            return $this->redirectToHomePage('Error session');
        }

        $result = $this->get('cab_course.handler.course_handler')->confirmBooking($paramsCourse, $user, $course);

        if ($result['status'] === 1) {
            $session->set('obj_course', $result['course']);
            if (array_key_exists('value', $result)) {
                $url = $this->generateUrl($result['value'], array('courseID' => $course->getId()));
                $response = new RedirectResponse($url);

                return $response;
            } elseif (array_key_exists('company', $result)) {
                return array('company' => $result['company'], 'course' => $result['course']);
            }
        } else {
            $this->redirectToHomePage($result['value']);
        }

        return $this->redirectToHomePage('Error session');
    }

    /**
     * @Route("finalize-booking/{courseID}",name="finalize_booking", options={"expose"=true})
     * @Method({"GET"})
     * @Template()
     * @ParamConverter("course", class="CABCourseBundle:Course", options={"id" = "courseID"})
     *
     * @param Course $course
     *
     * @return Response $response;
     *
     * @throws \Exception
     */
    public function finalizebookingAction(Course $course) {
        if (!$this->isGranted('ROLE_USER')) {
            $response = $this->redirectToHomePage('You should be authenticate');

            return $response;
        }
        if ($course->getClient() === null) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $course->setClient($user);
            $this->get('cab.course_manager')->setAttr($course, 'client', $user);
            $this->get('cab.course_manager')->setAttr($course, 'createdBy', $user);
            $this->get('cab.course_manager')->saveCourse($course);
        }

        return array('course' => $course);
    }

    /**
     * @Route("finalize-booking-step-2/",name="finalize_booking_step_2", options={"expose"=true})
     * @Method({"POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return Response $response;
     *
     * @throws \Exception
     */
    public function finalizebookingstep2Action(Request $request) {
        if (!$this->isGranted('ROLE_USER')) {
            $response = $this->redirectToHomePage('You should be authenticate');

            return $response;
        }

        list($oCourse, $option) = $this->getParamsCourses($request);
        $price = $this->get('cab.tarif.manager')->calculTarifCourseFront($option, $oCourse);

        $this->get('cab.course_manager')->setAttr($oCourse, 'priceHT', $price['tarifTotalHT']);
        $this->get('cab.course_manager')->setAttr($oCourse, 'priceTTC', $price['tarifTotalTTC']);
        $this->get('cab.course_manager')->setAttr($oCourse, 'indicatorStartedAddress', $option['departure_info']);
        $this->get('cab.course_manager')->setAttr($oCourse, 'indicatorArrivalAddress', $option['arrival_info']);
        $this->get('cab.course_manager')->setAttr($oCourse, 'descriptionChildren', $option['comment_booking']);

        //save details price
        $oDetailsPrice = $oCourse->getCourseDetailsTarif();
        $responseSave = $this->get('cab.details_tarif.manager')->saveObject($price, $oDetailsPrice);

        if ($responseSave['status']) {
            $this->get('cab.course_manager')->setAttr($oCourse, 'courseDetailsTarif', $responseSave['response']);
        }
        $this->get('cab.course_manager')->saveCourse($oCourse);

        $session = $request->getSession();
        $session->getFlashBag()->add('course_payment', $oCourse);
        $session->remove('course_payment');
        $session->set('course_payment', $oCourse);
        $courseDuration = $oCourse->getCourseTime();

        $hours = floor($courseDuration / 3600);
        $minutes = floor(($courseDuration / 60) % 60);
        $seconds = $courseDuration % 60;

        $courseDuration = "$hours:$minutes:$seconds";

        return array(
            'course' => $oCourse,
            'tax' => $price['tax'],
            'taxname' => $price['taxname'],
            'courseDuration' => $courseDuration,
        );
    }

    /**
     * @Route("payment-after-update-course/{courseID}",name="payment_after_update_course", options={"expose"=true})
     * @Method({"GET"})
     * @Template()
     *
     * @param int $courseID
     *
     * @return Response $response;
     *
     * @throws \Exception
     */
    public function paymentafterupdatecourseAction($courseID, Request $request) {
        if (!$this->isGranted('ROLE_USER')) {
            $response = $this->redirectToHomePage('You should be authenticate');

            return $response;
        }
        //@TODO update old payment. We can make the update only with WS. So We should update the transaction manually
        // we need to make notification (mail + sms + internal message) to admin company

        $oCourse = $this->get('cab.course_manager')->loadCourse($courseID);

        if ($oCourse->getDriver() == null) {
            // Notify Super Admin for the update
            $admin = $this->get('cab.cab_user.manager')->loadUser(1);
            $receivers = array($admin->getEmail());
        } else {
            $receivers = array($oCourse->getDriver()->getDriverCompany()->getContact()->getEmail());
        }
        $resUpdate = $this->get('cab_course.handler.course_handler')->sendNotifications($receivers, $oCourse);
        $session = $request->getSession();
        $session->getFlashBag()->add('course_payment', $oCourse);
        $session->remove('course_payment');
        $session->set('course_payment', $oCourse);

        return array('course' => $oCourse);
    }

    /**
     * @Route("confirmation-booking/", name="select_payment_booking", options={"expose"=true})
     * @Method({"POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return Response $response;
     *
     * @throws \Exception
     */
    public function payementAction (Request $request)
    {
        if (!$this->isGranted('ROLE_USER')) {
            $response = $this->redirectToHomePage('You should be authenticate');

            return $response;
        }

        $paymentMode = $request->request->get('type_payment');

        //@todo Check if the current customer is the owner of the course
        $session = $request->getSession();
        $course = $session->get('course_payment');

        $idCourse = $course->getId();

        /** @var Course $course */
        $course = $this->get('cab.course_manager')->loadCourse($idCourse);

        if ($course->getClient() === null) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $course->setClient($user);
            $this->get('cab.course_manager')->setAttr($course, 'client', $user);
            $this->get('cab.course_manager')->setAttr($course, 'createdBy', $user);
        }

        $this->get('cab.course_manager')->setAttr($course, 'paymentMode', $paymentMode);
        $this->get('cab.course_manager')->saveCourse($course);

        if (empty($course)) {
            $response = $this->redirectToHomePage('Error session of course payment');

            return $response;
        }
        try {
            $oCourse = $this->get('cab.course_manager')->loadCourse($course->getId());
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $systempay = $this->container->getParameter('systempay');
            $response = $this->get('cab_course.handler.transaction_handler')->doPayement(
                $oCourse, $paymentMode, $systempay, $user, $session
            );

        } catch (\ErrorException $e) {
            $response = $this->redirectToHomePage('Error payment ' . $e->getMessage());

            return $response;
        }

        $mailer = $this->container->get('mailer');
        $message = new \Swift_Message('your booking ref '.$oCourse->getId());

                $message->setFrom('no-reply@up.taxi')
                ->setTo($user->getEmail())
                ->setBcc('info@up.taxi');
        $message->setBody(
            $this->renderView('CABCourseBundle:Mail:bookconfirm.html.twig', $response), 'text/html'
        );
        $mailer->send($message);
        $this->addFlash('info', 'The course was created successfully.');

        //return $this->redirectToRoute('success_booking', array('course' => $course->getId()));
        return $response;
    }

    /**
     * Last step.
     * @Route("success-booking/{course}", name="success_booking", options={"expose"=true})
     * @Method({"GET"})
     * @Template()
     *
     * @param Request $request
     *
     * @return Response $response;
     *
     * @throws \Exception
     */
    public function successBookingAction ($course, Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $oCourse = $em->getRepository('CABCourseBundle:Course')->find($course);

        return array('course' => $oCourse);
    }

    /**
     * @Route("cancel-course/{course}", name="cancel_course", options={"expose"=true})
     * @Method({"GET"})
     * @Template()
     * @ParamConverter("course", class="CABCourseBundle:Course", options={"id" = "course"})
     *
     * @param Course $course
     *
     * @return Response $response;
     *
     * @throws \Exception
     */
    public function cancelCourseAction($course) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $result = $this->get('cab.course_manager')->cancelCourse($course, $user);

        if ($result['status'] == 1) {
            if ($course->getDriver()) {
                $oDriver = $course->getDriver();
                // send push notification to driver
                $messagePush = 'A course is canceled by the customer';
                $data = array(
                    'type_notification' => 'driver_notification_course',
                    'subject_notification' => 'Course cancelled',
                    'id_course' => $course->getId(),
                );
                $this->get('cab_course.push_notification.push')->getServiceByOs(
                        $oDriver->getDevices()->toArray(), $messagePush, $data
                );
            }
            $url = $this->generateUrl('cab_user_profile_show');
            try {
                $response = new RedirectResponse($url);
            } catch (\Exception $e) {
                return array('status' => 0, 'content' => $e->getMessage());
            }

            return $response;
        } else {
            return array('result' => $result);
        }
    }

    /**
     * @param null $message
     * @param string $typeMessage
     *
     * @return RedirectResponse
     *
     * @throws \Exception
     */
    protected function redirectToHomePage($message = null, $typeMessage = 'warning') {
        $url = $this->generateUrl('homepage');
        $response = new RedirectResponse($url);
        if ($message) {
            $this->get('session')->getFlashBag()->add($typeMessage, $message);
        }

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getParamsCourses(Request $request) {
        $departureInfo = $request->request->get('departure_info');
        $arrivalInfo = $request->request->get('arrival_info');
        $commentBooking = $request->request->get('comment_booking');
        $courseId = $request->request->get('id_course');
        $repCourse = $this->get('cab.course_manager');
        $oCourse = $repCourse->loadCourse($courseId);
        $luggageDeparture = $request->request->get('departure_luggage');
        $equipmentDeparture = $request->request->get('departure_equipment');
        $babySeatDeparture = $request->request->get('departure_baby_seat');
        $handicapWheelchair = 0;
        $luggageBack = 0;
        $equipmentBack = 0;
        $babySeatBack = 0;
        $handicapWheelchairBack = 0;

        if ($oCourse->getIsHandicap()) {
            $handicapWheelchair = $request->request->get('departure_handicap_wheelchair');
        }
        if ($oCourse->getTargetType()) {
            $luggageBack = $request->request->get('departure_luggage');
            $equipmentBack = $request->request->get('back_equipment');
            $babySeatBack = $request->request->get('back_baby_seat');
            if ($oCourse->getIsHandicap()) {
                $handicapWheelchairBack = $request->request->get('back_handicap_wheelchair');
            }
        }

        $option = array(
            'departure_info' => $departureInfo,
            'arrival_info' => $arrivalInfo,
            'comment_booking' => $commentBooking,
            'luggage_dep' => $luggageDeparture,
            'equipment_dep' => $equipmentDeparture,
            'baby_seat_dep' => $babySeatDeparture,
            'handicap_wheelchair_dep' => $handicapWheelchair,
            'luggage_back' => $luggageBack,
            'equipment_back' => $equipmentBack,
            'baby_seat_back' => $babySeatBack,
            'handicap_wheelchair_back' => $handicapWheelchairBack,
        );

        return array($oCourse, $option);
    }

    /**
     * Method description
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getParamsForCreateBooking(Request $request) {
        $luggage = 0;
        /* Longitude and latitude for all address */
        $latitudeDepart = $request->request->get('lat');
        $longitudeDepart = $request->request->get('lng');
        $latitudeArrival = $request->request->get('latArrival');
        $longitudeArrival = $request->request->get('lngArrival');
        $estimatedTime = $request->request->get('estimated_time');
        $estimatedTimeText = $request->request->get('estimated_time_text');
        $estimatedDistance = $request->request->get('estimated_distance');
        //Back address
        $latitudeDepartBack = $request->request->get('latDepBack');
        $longitudeDepartBack = $request->request->get('lngDepBack');
        $latitudeArrivalBack = $request->request->get('latArrivalBack');
        $longitudeArrivalBack = $request->request->get('lngArrivalBack');
        $estimatedTimeBack = $request->request->get('estimated_time_back');
        $estimatedTimeBackText = $request->request->get('estimated_time_back_text');
        $estimatedDistanceBack = $request->request->get('estimated_distance_back');
        $withBack = $request->request->get('with_return');

        $nbPerson = $request->request->get('numberPerson');
        $arrivalAddress = $request->request->get('arrival_address');
        $departureAddress = $request->request->get('depart_address');
        $arrivalAddressRet = $request->request->get('arrival_address_ret');
        $departureAddressRet = $request->request->get('depart_address_ret');
        $depDate = $request->request->get('dep_date');
        $backDate = $request->request->get('back_date');
        if ($depDate) {
            list($dateDeparture, $timeDeparture) = explode(' ', $depDate);
        } else {
            $dateDeparture = null;
            $timeDeparture = null;
        }

        if ($backDate) {
            list($dateDepartureBack, $timeDepartureBack) = explode(' ', $backDate);
        } else {
            $estimatedDistanceBack = 0;
            $estimatedTimeBack = 0;
            $dateDepartureBack = null;
            $timeDepartureBack = null;
        }

        if (!$latitudeDepart) {
            $latitudeDepart = 0;
        }
        if (!$longitudeDepart) {
            $longitudeDepart = 0;
        }
		if(!$latitudeArrival) {
			$latitudeArrival = 0;
		}
		if(!$longitudeArrival) {
			$longitudeArrival = 0;
		}


		if(!$latitudeArrivalBack) {
			$latitudeArrivalBack = 0;
		}
		if(!$longitudeArrivalBack) {
			$longitudeArrivalBack = 0;
		}
		if (!$latitudeDepartBack) {
            $latitudeDepartBack = 0;
        }
        if (!$longitudeDepartBack) {
            $longitudeDepartBack = 0;
        }

        if (!$withBack) {
            $estimatedDistanceBack = 0;
            $dateDepartureBack = 0;
            $timeDepartureBack = 0;
            $nbPersonBack = 0;
            $luggageBack = 0;
            $totalDuration = $estimatedTime;
            $totalDistance = $estimatedDistance;
        } else {
            $totalDistance = $estimatedDistance + $estimatedDistanceBack;
            // @TODO calcul total estimated time (convert time to text)
            $totalDuration = $estimatedTime + $estimatedTimeBack;
            $nbPersonBack = $nbPerson;
            $luggageBack = $luggage;
        }
        $coordinateAddr = array(
            'latDepart' => $latitudeDepart,
            'lngDepart' => $longitudeDepart,
            'latArrival' => $latitudeArrival,
            'lngArrival' => $longitudeArrival,
        );

        $coordinateAddrBack = array(
            'latDepart' => $latitudeDepartBack,
            'lngDepart' => $longitudeDepartBack,
            'latArrival' => $latitudeArrivalBack,
            'lngArrival' => $longitudeArrivalBack,
        );

        return array(
            'dateDeparture' => $dateDeparture,
            'timeDeparture' => $timeDeparture,
            'dateDepartureBack' => $dateDepartureBack,
            'timeDepartureBack' => $timeDepartureBack,
            'estimatedTime' => $estimatedTime,
            'estimatedTimeText' => $estimatedTimeText,
            'estimatedDistance' => $estimatedDistance,
            'estimatedTimeBack' => $estimatedTimeBack,
            'estimatedDistanceBack' => $estimatedDistanceBack,
            'luggage' => $luggage,
            'withBack' => $withBack,
            'nbPerson' => $nbPerson,
            'arrivalAddress' => $arrivalAddress,
            'departureAddress' => $departureAddress,
            'arrivalAddressRet' => $arrivalAddressRet,
            'departureAddressRet' => $departureAddressRet,
            'depDate' => $depDate,
            'backDate' => $backDate,
            'coordinateAddr' => $coordinateAddr,
            'coordinateAddrBack' => $coordinateAddrBack,
            'totalDistance' => $totalDistance,
            'totalDuration' => $totalDuration,
            'nbPersonBack' => $nbPersonBack,
            'luggageBack' => $luggageBack,
        );
    }

    /**
     * Method description
     *
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function initConfirmBooking(Request $request) {
        $session = $request->getSession();

        $paramsCourse = $session->get('course');
        $user['authorizationChecker'] = $this->get('security.authorization_checker');
        $user['tokenStorage'] = $this->get('security.token_storage')->getToken()->getUser();
        $course = new Course();
        if ($session->has('obj_course')) {
            $course = $session->get('obj_course');
        }
        if ($request->request->has('company')) {
            $company = $this->get('cab.company.manager')->loadCompany($request->request->get('company'));
            //$course->setCompany($company);
            $this->get('cab.course_manager')->setAttr($course, 'company', $company);
        } else {
            $company = $course->getCompany();
        }
        $paramsCourse['totalPrice'] = $request->request->get('total_price');
        $paramsCourse['totalPriceTTC'] = $request->request->get('total_price_ttc');
        $paramsCourse['estimatedTarif'] = $request->request->get('estimated_tarif');
        $paramsCourse['depPrice'] = $request->request->get('dep_price');
        $paramsCourse['company'] = $company;
        $paramsCourse['isForfait'] = $request->request->get('forfait');

        return array($session, $paramsCourse, $user, $course);
    }

}
