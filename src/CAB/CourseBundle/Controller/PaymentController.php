<?php

namespace CAB\CourseBundle\Controller;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\TransactionResponse;
use CAB\CourseBundle\Payment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class PaymentController
 *
 * @package CAB\CourseBundle\Controller
 */
class PaymentController extends Controller
{
    /**
     * @Route("end-payment",name="end_payment", options={"expose"=false})
     * @Method({"POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     */
    public function endpaymentAction(Request $request)
    {
        if ($request->request->get('vads_page_action') == 'REGISTER') {
            $url = $this->generateUrl('cab_user_profile_show');
            $response = new RedirectResponse($url);

            return $response;
        } else {
            $course = $this->get('cab.course_manager')->loadCourse($request->request->get('vads_order_id'));
            $params = array(
                'vads_amount' => $request->request->get('vads_amount'),
                'vads_currency' => $request->request->get('vads_currency'),

            );

            return array(
                'params' => $params,
                'course' => $course,
            );
        }


    }

    /**
     * @Route("end-payment-auto",name="end_payment_auto", options={"expose"=false})
     * @Method({"POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     */
    public function endpaymentautoAction(Request $request)
    {
        $logger = $this->get('monolog.logger.cab');
        $logger->info('############################*****Response from systempay-----######################################');

        $params = $this->getParamsTransResponse($request);
        $serviceResponseTrans = $this->get('cab.transaction_response.manager');
        if ($request->request->get('vads_page_action') != 'REGISTER') {
            $transactionId = intval($params['vadsTransId']);
            $transactionService = $this->get('cab.transaction_payment.manager');
            $oTransaction = $transactionService->loadObject($transactionId);

            $params['reponseTransactionPayment'] = $oTransaction;
            $serviceCourse = $this->get('cab.course_manager');
            $oCourse = $serviceCourse->loadCourse($oTransaction->getTransactionPaymentCourse());
            $serviceCourse->setAttr($oCourse, 'courseStatus', Course::STATUS_PAYED);
            $serviceCourse->saveCourse($oCourse);
            try {
                $this->get('cab_course.handler.transaction_handler')->sendMessageAfterPayment($oCourse);
            } catch (\Exception $e) {
                $logger->error(
                    'CAB/CourseBundle/Controller/PaymentController.php  exception sending message:***** '.$e->getMessage(
                    )
                );
            }
        }
        $oTransResponse = $serviceResponseTrans->createTransaction($params);
        $vadsCustEmail = $request->request->has('vads_cust_email') ? $request->request->get('vads_cust_email') : null;
        if ($vadsCustEmail !== null
            && $oTransResponse instanceof TransactionResponse
            && in_array($params['vadsPageAction'], array('REGISTER', 'REGISTER_PAY'))
            && $params['vadsIdentifier'] !== null
        ) {
            $this->identifiantCardPayment(
                $vadsCustEmail,
                $params['vadsIdentifier'],
                $oTransResponse
            );
        }

        $logger->info('############################ END Response from systempay-#####################################');

        return array();
    }

    /**
     * Method description
     *
     * @param $vadsCustEmail
     * @param $params
     * @param $logger
     * @param $oTransResponse
     *
     * @return array
     */
    protected function identifiantCardPayment($vadsCustEmail, $vadsIdentifier, $oTransResponse)
    {
        $userCustomer = $this->get('cab_course.handler.transaction_handler')->updateIdentifiantUser($vadsCustEmail, $vadsIdentifier);

        $serviceCard = $this->get('cab_course.manager.payment_card_manager');

        $cardIsExist = $serviceCard->getCardBy('cardNumber', $oTransResponse->getVadsCardNumber());

        if (!$cardIsExist) {
            $cardIsExist = null;
        }
        $serviceCard->saveObject(
            array(
                'paymentId' => $oTransResponse->getVadsIdentifier(),
                'user' => $userCustomer,
                'transactionResponse' => $oTransResponse,
                'cardNumber' => $oTransResponse->getVadsCardNumber(),
                'monthExpiry' => $oTransResponse->getVadsExpiryMonth(),
                'yearExpiry' => $oTransResponse->getVadsExpiryYear(),
                'cardBrand' => $oTransResponse->getVadsCardBrand(),
            ),
            $cardIsExist
        );
    }

    /**
     * Method description
     *
     * @param Request $request
     *
     * @return array
     */
    protected function getParamsTransResponse(Request $request)
    {
        $params = array();
        $trans = new TransactionResponse();
        $reflect = new \ReflectionClass($trans);
        $props = $reflect->getProperties();
        foreach ($props as $prop) {
            if ($prop->getName() == 'vadsThreedsCavvAlgorithm') {
                $ret = 'vads_threeds_cavvAlgorithm';
            } else {
                preg_match_all(
                    '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!',
                    $prop->getName(),
                    $matches
                );
                $ret = $matches[0];
                foreach ($ret as &$match) {
                    $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
                }
                $ret = implode('_', $ret);
            }
            $nameProperty = $prop->getName();
            $aProp[$nameProperty] = $ret;
        }
        foreach ($aProp as $propertyCC => $property) {
            if ($request->request->has($property)) {
                $params[$propertyCC] = $request->request->get($property);
            }
        }
        return $params;
}
}
