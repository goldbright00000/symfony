<?php

namespace CAB\CourseBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class MapsController extends Controller
{
    /**
     * @Route("/geocoding")
     * @Method({"POST"})
     * @Template()
     */
    public function geocodingAction(Request $request)
    {
        $departAddress = $request->request->get('depart_address');
        $arrivalAddress = $request->request->get('arrival_address');

        echo $departAddress; exit();
        //return array();
    }

    /**
     * @Route("/get-address-list-by-lat-and-lng/{lat}/{lng}", name="address-list-lat-lng", options={"expose"=true})
     * @Method({"GET"})
     * @Template()
     */
    public function GetAddressByLatAndLngAction($lat, $lng)
    {
        $em = $this->getDoctrine()->getManager();
        $companyRepository = $em->getRepository('CABCourseBundle:Company');

        $listAddress = $companyRepository->findByLatLng($lat, $lng);

        $response = new JsonResponse();
        $response->setData(array(
            'data' => 123
        ));
    }
}
