<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 05/01/2016
 * Time: 17:34
 */

namespace CAB\CourseBundle\Controller;


use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Payment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class CustomerController
 *
 * @package CAB\CourseBundle\Controller
 */
class CustomerController extends Controller
{
    /**
     * Get parameters from request
     *
     * @param Request $request request object
     * @param Course  $oCourse course object
     *
     * @return array
     */
    protected function getParamsCourses(Request $request, Course $oCourse)
    {
        $departureInfo = array();
        $backInfo = array();
        $paramCourse = $request->request->get('cab_coursebundle_course');
        $departureInfo['startedAddress'] = $paramCourse['startedAddress'];
        $departureInfo['arrivalAddress'] = $paramCourse['arrivalAddress'];
        $departureInfo['departureDate'] = $paramCourse['departureDate'];
        $departureInfo['departureTime'] = $paramCourse['departureTime'];
        $departureInfo['lastDepartureDate'] = $request->request->get('last_dep_date');
        $departureInfo['lastDepartureTime'] = $request->request->get('last_dep_time');
        $departureInfo['longDep'] = $paramCourse['longDep'];
        $departureInfo['latDep'] = $paramCourse['latDep'];
        $departureInfo['longArr'] = $paramCourse['longArr'];
        $departureInfo['latArr'] = $paramCourse['latArr'];
        $departureInfo['luggageDeparture'] = $paramCourse['laguage'];
        $departureInfo['personNb'] = $paramCourse['personNumber'];
        $departureInfo['nbChildren'] = $paramCourse['nbChildren'];
        $departureInfo['handicapWheelchair'] = $paramCourse['nbWheelchair'];
        $departureInfo['typeVehicleCourse'] = $paramCourse['typeVehicleCourse'];
        $departureInfo['initialDistance'] = $request->request->get('initial_distance');
        $departureInfo['transaction'] = $request->request->get('transaction');
        $departureInfo['price'] = $request->request->get('price');
        if ($departureInfo['handicapWheelchair']) {
            $oCourse->setIsHandicap(1);
        }

        $courseLatLongDep = $oCourse->getLatDep() . ',' . $oCourse->getLongDep();
        $courseLatLongArr = $oCourse->getLatArr() . ',' . $oCourse->getLongArr();
        list($departureInfo['distanceCourse'], $departureInfo['durationCourse']) =
            $this->get('cab_course.handler.geolocation_handler')->getDistanceMatrix($paramCourse['startedAddress'],
                $paramCourse['arrivalAddress'], $courseLatLongDep, $courseLatLongArr);

        $backInfo['backAddressDeparture'] = '';
        $backInfo['backAddressArrival'] = '';
        $backInfo['departureDate'] = 0;
        $backInfo['departureTime'] = 0;
        $backInfo['lngDepBack'] = 0;
        $backInfo['latDepBack'] = 0;
        $backInfo['lngArrBack'] = 0;
        $backInfo['latArrBack'] = 0;
        $backInfo['luggageBack'] = 0;
        $backInfo['personNb'] = 0;
        $backInfo['nbChildren'] = 0;
        $backInfo['handicapWheelchairBack'] = 0;
        $backInfo['distanceCourse'] = 0;
        $backInfo['durationCourse'] = 0;

        if ($oCourse->getTargetType()) {
            $backInfo['backAddressDeparture'] = $paramCourse['backAddressDeparture'];
            $backInfo['backAddressArrival'] = $paramCourse['backAddressArrival'];
            $backInfo['departureDate'] = $paramCourse['backDate'];
            $backInfo['departureTime'] = $paramCourse['backTime'];
            $backInfo['lngDepBack'] = $paramCourse['longDepBack'];
            $backInfo['latDepBack'] = $paramCourse['latDepBack'];
            $backInfo['lngArrBack'] = $paramCourse['longArrBack'];
            $backInfo['latArrBack'] = $paramCourse['latArrBack'];
            $backInfo['luggageBack'] = $request->request->get('departure_luggage');
            $backInfo['personNb'] = $paramCourse['personNumberBack'];
            $backInfo['nbChildren'] = $paramCourse['nbChildrenBack'];
            $backInfo['handicapWheelchairBack'] = $request->request->get('nbWheelchairBack');
            if ($backInfo['handicapWheelchairBack']) {
                $oCourse->setIsHandicap(1);
            }
            $courseLatLongDepBack = $oCourse->getLatDepBack() . ',' . $oCourse->getLongDepBack();
            $courseLatLongArrBack = $oCourse->getLatArrBack() . ',' . $oCourse->getLongArrBack();

            list($backInfo['distanceCourse'], $backInfo['durationCourse']) =
                $this->get('cab_course.handler.geolocation_handler')->getDistanceMatrix(
                    $paramCourse['backAddressDeparture'],
                    $paramCourse['backAddressArrival'],
                    $courseLatLongDepBack,
                    $courseLatLongArrBack
                );
        }

        $option = array(
            'departure_info' => $departureInfo,
            'back_info' => $backInfo,
        );

        return $option;
    }

    /**
     * @param null $message
     * @param string $typeMessage
     *
     * @return RedirectResponse
     */
    private function redirectToHomePage($message = null, $typeMessage = 'warning')
    {
        $url = $this->generateUrl('homepage');
        $response = new RedirectResponse($url);
        if ($message) {
            $this->get('session')->getFlashBag()->add($typeMessage, $message);
        }

        return $response;
    }
}