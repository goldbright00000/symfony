<?php

namespace CAB\CourseBundle\Controller;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\TypeVehicule;
use CAB\CourseBundle\Entity\Zone;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;

class MainController extends Controller
{
    /**
     * @Route("get-service-by-zone-company/{zoneID}/{companyID}",name="get_service_by_zone_company", options={"expose"=true})
     * @Method({"GET"})
    * @Template()
     */
    public function getServicesByZoneAction($zoneID, $companyID)
    {
        //@Todo: add exception if $result is empty
        $objManager = $this->get('cab.serviceTransport.manager');
        $result = $objManager->getBy(array('zone' => $zoneID, 'company' => $companyID));
        $response = new JsonResponse();
        $response->setData(array(
            'status' => 1,
            'result' => $result
        ));

        return $response;
    }

    
    /**
     * @Route("get-vehicle-by-type/{id}",name="get_vehicle_by_type", options={"expose"=true})
     * @Method({"GET"})
     * @Template()
     * @ParamConverter("vehiculeType", class="CABCourseBundle:TypeVehicule")
     *
     * @param TypeVehicule $vehiculeType
     * @return array
     */
    public function getvehiclesbytypeAction(TypeVehicule $vehiculeType)
    {
        //@Todo: add exception if $result is empty
        $objManager = $this->get('cab.vehicule_manager');
        $result = $objManager->getVehiclesBy('typeVehicule', $vehiculeType->getId());
        $response = new JsonResponse();
        $response->setData(array(
            'status' => 1,
            'result' => $result,
        ));

        return $response;
    }


    /**
     * @Route("/driver-by-vehicle/{vehicleId}",name="driver_by_vehicle", options={"expose"=true})
     * @Method({"GET"})
     *
     * @throws \InvalidArgumentException
     */
    public function getDriverByVehicleAction($vehicleId)
    {
        $response = new JsonResponse();
        $em       = $this->getDoctrine();
        $data = array(
            'status' => false,
            'driver' => null,
        );

        if ($vehicleId) {
            $driver = $em->getRepository('CABUserBundle:User')->getDriverByVehicle($vehicleId, false);

            if (null !== $driver) {
                $data['status']  = true;
                $data['driver'] = array(
                    'id' => $driver->getId(),
                    'text' => $driver->getUsedName(),
                );
            } else {
                $data['status']  = true;
                $data['driver'] = array(
                    'id' => null,
                    'text' => null,
                );
            }
        }

        $response->setData($data);

        return $response;
    }

}