<?php

namespace CAB\CourseBundle\Controller;

use CAB\CourseBundle\Entity\Company;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Session\Session;

class CompanyController extends Controller
{

    /**
     * @Route("/company/tracking", name="company_tracking")
     * @Method("GET")
     */
    public function trackingAction()
    {
        $companies  = array();
        $drivers    = array();
        $em         = $this->getDoctrine();

        if ($this->isGranted('ROLE_SUPER_ADMIN') || $this->isGranted('ROLE_ADMIN')) {
            $companies = $em->getRepository('CABCourseBundle:Company')->findAll();
        } else if ($this->isGranted('ROLE_ADMINCOMPANY')) {
            $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser()]);
            $companyId = $company->getId();

            $companies = $em->getRepository('CABCourseBundle:Company')->find($companyId);
            // $drivers   = $em->getRepository('CABUserBundle:User')->getUsersByCompanyId($companyId, $this->getUser()->getId());
        }

        return $this->render('CABCourseBundle:Tracking:tracker.html.twig', array(
            'companies' => $companies,
            /*'drivers'   => $drivers*/
        ));
    }

    /**
     * @Route("get-object-by-company/{id}/{searched}",name="get_object_company", options={"expose"=true})
     * @Method({"GET"})
     * @Template()
     * @ParamConverter("company", class="CABCourseBundle:Company")
     */
    public function getObjectsBycompanyAction(Company $company, $searched)
    {
        //@Todo: add exception if $result is empty
        $serviceName = 'cab.'.$searched.'.manager';

        if ($searched == 'tarifFrenchTaxi') {
            $objManager = $this->get('cab.tarifFrenshTaxi.manager');
            //get location from company
            $postCode = $company->getPostCode();
            $departementCode = $result = substr($postCode, 0, 2);
            $result = $objManager->loadTarifsByDepartement($departementCode);
        } else {
            $objManager = $this->get($serviceName);
            $key = $searched == 'zone' ? 'company'.ucfirst($searched) : 'company';
            $result = $objManager->getBy(array($key => $company->getId()));
        }


        if ($result) {
            $status = true;
            $value = $result;
            $codeError = 0;
        } else {
            $value = null;
            $codeError = 1;
            $status = false;
        }

        $response = new JsonResponse();
        $response->setData(array(
            'status_response' => $status,
            'code_error' => $codeError,
            'response' => $value,
        ));

        return $response;
    }

    /**
     * @Route("/company/getDrivers/{company_id}",name="company_drivers", options={"expose"=true})
     * @Method({"POST"})
     */
    public function getDriversByCompanyAction($company_id, Request $request)
    {
        $response = new JsonResponse();
        $data     = array();
        $em       = $this->getDoctrine();
        $sel_date = $request->request->get('sel_date');

        // get drivers by company id
        /*if ($this->isGranted('ROLE_SUPER_ADMIN') || $this->isGranted('ROLE_ADMIN')) {
            $user_id = $company_id;
        } else if ($this->isGranted('ROLE_ADMINCOMPANY')) {
            $user_id = $this->getUser()->getId();
        }*/

        if (!$company_id || !$sel_date) {
            $data['status'] = false;
        } else {
            $aDrivers   = $em->getRepository('CABUserBundle:User')->getUsersByCompanyId($company_id);
            // $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $user_id]);
            
            if (null !== $aDrivers) {
                /*$aDrivers = $company->getUsers();
                $aDrivers = $aDrivers->toArray();*/
                $aDriversId = array();

                foreach ($aDrivers as $driver) {
                    $aDriversId[] = $driver->getId();
                }

                $drivers = $this->get('cab_api.gelocalisation.handler')->getAvailableDriversByDate($aDriversId, $sel_date);

                $data['status']  = true;
                $data['drivers'] = $this->getDriversValues($drivers);
            }
        }

        $response->setData($data);

        return $response;
    }

    /**
     * @Route("/company/getDriversGeolocation/{driver_id}/{option}",name="company_driver_geo_tracking", options={"expose"=true})
     * @Method({"GET"})
     */
    public function getDriverGeolocationAction($driver_id, $option, Request $request)
    {
        $response = new JsonResponse();
        $data     = array();
        $error    = 0;

        if (!$driver_id) {
            return false;
        }

        $geolocationHandler = $this->get('cab_api.gelocalisation.handler');
        $trip_date  = $request->query->get('trip_date');
        $getDate    = ($trip_date ? trim($trip_date) : false);
        $getDriverGeos = $geolocationHandler->getGeoRepo()->getDriverInfoByIdByDate($driver_id, $getDate);

        if (!$getDriverGeos) {
            $error++;
        }

        if (!$error) {
            $em               = $this->getDoctrine();
            $getDriverVehicle = $em->getRepository('CABCourseBundle:VehiculeAffect')->getVehicleAffectByDriverId((int)$driver_id, 'DESC', $getDate);

            if (null === $getDriverVehicle) {
                return false;
            }
            // get Vehicle name/Id
            $vehicle_id = $getDriverVehicle->getVehicule()->getId();
            $vehicleName= $getDriverVehicle->getVehicule()->getVehiculeName(); 

            if ($option && $option == 2) {
                $getDrivers = false;
                $getDrivers = $em->getRepository('CABCourseBundle:VehiculeAffect')->getDriversByVehiculeId($vehicle_id, 'DESC', $getDate);

                if ($getDrivers) {
                    $drivers = array();

                    foreach ($getDrivers as $key => $driver) {
                        $driverId = $driver->getDriver()->getId();

                        if ($driver_id != $driverId) {
                            $getDriverGeoArr = $geolocationHandler->getGeoRepo()->getDriverInfoByIdByDate($driverId, $getDate);

                            if (count($getDriverGeoArr)) {
                                $getDriverGeos = array_merge($getDriverGeos, $getDriverGeoArr);
                            }
                        }
                    }
                }
            }
            
            $updatedGeolocationsForDrivers = $this->updateGeolocationData($getDriverGeos, $vehicleName);
            $geolocationInfos['status'] = true;

            $geolocationInfos['geos']   = $updatedGeolocationsForDrivers['data'];
            $geolocationInfos['first_hour']   = $updatedGeolocationsForDrivers['hours']['first'];
            $geolocationInfos['last_hour']   = $updatedGeolocationsForDrivers['hours']['last'];

            $response->setData($geolocationInfos);
        } else {
            $response->setData(array('status' => false));
        }

        return $response;
    }

    /**
     * @Route("/company/checkGeoDate",name="company_date_geo_tracking", options={"expose"=true})
     * @Method({"GET"})
     */
    public function checkGeoDateAction(Request $request)
    {
        $geolocationHandler = $this->get('cab_api.gelocalisation.handler');
        $check_date  = $request->query->get('check_date');
        $isFound     = $geolocationHandler->getGeoRepo()->checkAvailableDriversByDate($check_date);

        $isExisting = false;
        if ($isFound) {
            $isExisting = true;
        }

        $response = new JsonResponse();
        $response->setData(array('status' => $isExisting));
        return $response;
    }

    private function getDriversValues($drivers)
    {
        $getDrivers = array();
        $trackID    = array();
        $em         = $this->getDoctrine();

        foreach ($drivers as $key => $driver) {
            $driver_id = $driver->getDriver();
            
            if (!in_array($driver_id, $trackID)) {   
                $trackID[] = $driver_id;
                $user = $em->getRepository('CABUserBundle:User')->find($driver_id);

                $getDrivers[] = array(
                    'id'   => $driver_id,
                    'name' => strtoupper($user->getLastName()) .' '. ucfirst($user->getFirstName())
                );
            }
        }

        $trackID = null;
        return $getDrivers;
    }

    private function updateGeolocationData($data, $vehicle_name)
    {
        $em   = $this->getDoctrine();

        $getFirstLastHours = array();

        foreach ($data as $key => $val) {
            // check first and last hours
            $getFirstLastHours[] = (string)explode(' ',$data[$key]['dateGeo'])[1];
            $getDriver  = $em->getRepository('CABUserBundle:User')->find((int)$data[$key]['driver_id']);

            $data[$key]['driver']  = strtoupper($getDriver->getLastName()) .' '. ucfirst($getDriver->getFirstName());
            $data[$key]['vehicle'] = ucfirst($vehicle_name);
            unset($data[$key]['driver_id']);
        }

        return array(
            'data' => $data,
            'hours' => array(
                'first' => min($getFirstLastHours),
                'last'  => max($getFirstLastHours),
            )
        );

    }
}
