<?php

namespace CAB\CourseBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * @MongoDB\Document 
 * @MongoDB\Document(repositoryClass="CAB\CourseBundle\Document\Repository\GeolocationRepository")
 * @ExclusionPolicy("all")
 */
class Geolocation {

    /**
     * @MongoDB\Id
     * @Expose
     * @Groups({"Public"})
     */
    protected $id;

    /**
     * @MongoDB\Field(name="driver", type="int")
     * @Expose
     * @Groups({"Public"})
     */
    private $driver;

    /**
     * @MongoDB\Field(name="course", type="int")
     * @Expose
     * @Groups({"Public"})
     */
    private $course;

    /**
     * @MongoDB\Field(name="accuracy", type="float")
     * @Expose
     * @Groups({"Public"})
     */
    private $accuracy;

    /**
     * @MongoDB\Field(name="altitude", type="float")
     * @Expose
     * @Groups({"Public"})
     */
    private $altitude;

    /**
     * @MongoDB\Field(name="latitude", type="float")
     * @Expose
     * @Groups({"Public"})
     */
    private $latitude;

    /**
     * @MongoDB\Field(name="longitude", type="float")
     * @Expose
     * @Groups({"Public"})
     */
    private $longitude;

    /**
     * @MongoDB\Field(name="heading", type="float")
     * @Expose
     * @Groups({"Public"})
     */
    private $heading;

    /**
     * @MongoDB\Field(name="speed", type="float")
     * @Expose
     * @Groups({"Public"})
     */
    private $speed;

    /**
     * @MongoDB\Field(name="dateGeolocate", type="string")
     * @Expose
     * @Groups({"Public"})
     */
    private $dateGeolocate;

    /**
     * @MongoDB\Field(name="availibility", type="boolean")
     * @Expose
     * @Groups({"Public"})
     */
    private $availibility;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set accuracy
     *
     * @param string $accuracy
     * @return self
     */
    public function setAccuracy($accuracy) {
        $this->accuracy = $accuracy;
        return $this;
    }

    /**
     * Get accuracy
     *
     * @return string $accuracy
     */
    public function getAccuracy() {
        return $this->accuracy;
    }

    /**
     * Set altitude
     *
     * @param float $altitude
     * @return self
     */
    public function setAltitude($altitude) {
        $this->altitude = $altitude;
        return $this;
    }

    /**
     * Get altitude
     *
     * @return float $altitude
     */
    public function getAltitude() {
        return $this->altitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return self
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float $latitude
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return self
     */
    public function setLongitude($longitude) {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float $longitude
     */
    public function getLongitude() {
        return $this->longitude;
    }

    /**
     * Set heading
     *
     * @param float $heading
     * @return self
     */
    public function setHeading($heading) {
        $this->heading = $heading;
        return $this;
    }

    /**
     * Get heading
     *
     * @return float $heading
     */
    public function getHeading() {
        return $this->heading;
    }

    /**
     * Set speed
     *
     * @param float $speed
     * @return self
     */
    public function setSpeed($speed) {
        $this->speed = $speed;
        return $this;
    }

    /**
     * Get speed
     *
     * @return float $speed
     */
    public function getSpeed() {
        return $this->speed;
    }

    /**
     * Set driver
     *
     * @param int $driver
     * @return self
     */
    public function setDriver($driver) {
        $this->driver = $driver;
        return $this;
    }

    /**
     * Get driver
     *
     * @return int $driver
     */
    public function getDriver() {
        return $this->driver;
    }

    /**
     * Set availibility
     *
     * @param boolean $availibility
     * @return self
     */
    public function setAvailibility($availibility) {
        $this->availibility = $availibility;

        return $this;
    }

    /**
     * Get availibility
     *
     * @return boolean $availibility
     */
    public function getAvailibility() {
        return $this->availibility;
    }

    /**
     * Set dateGeolocate
     *
     * @param string $dateGeolocate
     * @return self
     */
    public function setDateGeolocate($dateGeolocate) {
        date_default_timezone_set("GMT");
        $date = new \DateTime();
        $dateGMT = $date->format('Y-m-d H:i:s');
        $this->dateGeolocate = $dateGMT;

        return $this;
    }

    /**
     * Get dateGeolocate
     *
     * @return string $dateGeolocate
     */
    public function getDateGeolocate() {
        return $this->dateGeolocate;
    }

    /**
     * Set course
     *
     * @param int $course
     * @return self
     */
    public function setCourse($course) {
        $this->course = $course;
        return $this;
    }

    /**
     * Get course
     *
     * @return int $course
     */
    public function getCourse() {
        return $this->course;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateGeolocateAsDateTime() {
        if (FALSE === $date = date_create_from_format('Y-m-d H:i:s', $this->dateGeolocate)) {
            return null;
        }

        return $date;
    }

}
