<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 11/03/2016
 * Time: 01:57
 */

namespace CAB\CourseBundle\Handler;


use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

interface CabCourseHandlerInterface
{

    /**
     * Constructor
     *
     * @param EntityManager   $em     entity manager
     * @param LoggerInterface $logger logger
     */
    public function __construct(EntityManager $em, LoggerInterface $logger);

    /**
     * Return class properties
     *
     * @param string $class
     *
     * @return mixed
     */
    public function getClassProperties($class);
}