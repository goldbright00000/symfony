<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 22/01/2016
 * Time: 15:42
 */

namespace CAB\CourseBundle\Handler;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class BaseHandler
 *
 * @package CAB\CourseBundle\Handler
 */
class BaseHandler implements CabCourseHandlerInterface {

    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param EntityManager   $em     entity manager
     * @param LoggerInterface $logger logger
     */
    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * Method description
     *
     * @param string $class
     *
     * @return array
     */
    public function getClassProperties($class)
    {
        $oClass = new $class();
        $res = array();
        $reflect = new ReflectionClass($oClass);
        $props   = $reflect->getProperties();

        foreach ($props as $prop) {
            $res[] = $prop->getName();
        }

        return$res;
    }
}