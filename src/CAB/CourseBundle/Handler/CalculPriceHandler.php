<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 20/01/2016
 * Time: 22:20
 */

namespace CAB\CourseBundle\Handler;

use CAB\CourseBundle\Manager\CompanyManager;
use CAB\CourseBundle\Manager\TarifManager;
use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Manager\VehiculeManager;
use Psr\Log\LoggerInterface;

/**
 * Class CalculPriceHandler
 *
 * @package CAB\CourseBundle\Handler
 */
class CalculPriceHandler extends BaseHandler
{
    /**
     * @var VehiculeManager
     */
    protected $vehicleManager;

    /**
     * @var TarifManager
     */
    protected $tarifManager;

    /**
     * @var CompanyManager
     */
    protected $companyManager;

    /**
     * Method description
     *
     * @param VehiculeManager $vehicleManager
     */
    public function setVehicleManager(VehiculeManager $vehicleManager)
    {
        $this->vehicleManager = $vehicleManager;
    }

    /**
     * Method description
     *
     * @param TarifManager $tarifManager
     */
    public function setTarifManager(TarifManager $tarifManager)
    {
        $this->tarifManager = $tarifManager;
    }

    /**
     * Method description
     *
     * @param CompanyManager $companyManager
     */
    public function setCompanyManager(CompanyManager $companyManager)
    {
        $this->companyManager = $companyManager;
    }

    /**
     * Method description
     *
     * @param array $aParams
     *
     * @return array
     */
    public function createBooking(array $aParams)
    {
        $result = $this->companyManager->getPriceCourseByCompany(
            $aParams['coordinateAddr']['lngDepart'],
            $aParams['coordinateAddr']['latDepart']
        );
        if ($aParams['withBack'] == 1) {
            $resultRet = $this->companyManager->getPriceCourseByCompany(
                $aParams['coordinateAddrBack']['lngDepart'],
                $aParams['coordinateAddrBack']['latDepart']
            );
            $aParams['returnCompanyId'] = $resultRet[0]["companyId"];
        }

        $aCompany = array();
        $idTarif = 0;
        // 2- get only zones when her address is less than estimated distance
        foreach ($result as $key => $item) {
            if ($item['distance'] < $item['zoneRaduis']) {
                // 3- select vehicles types and vehicles for the company passed as argument
                try {
                    $aParams['companyId'] = $item["companyId"];
                    $aPrice = $this->tarifManager->getTarifCourse($aParams);
                    $resultPrice = array('price' => $aPrice);
                    $idTarif = $aPrice['tarif_id'];
                } catch (\Exception $e) {
                    $resultPrice = $e->getMessage();
                }

                $aCompany[$key][0] = $this->companyManager->loadCompany($item["companyId"]);
                $aCompany[$key][1] = $item;
                $aCompany[$key][2] = $resultPrice;
                $aCompany[$key]['price_ttc'] = $resultPrice['price']['price_total_ttc'];
                $aCompany[$key]['estimated_tarif'] = $resultPrice['price']['estimated_tarif'];
            }
        }
        usort(
            $aCompany,
            function ($a, $b) {
                return ceil($a['price_ttc'] - $b['price_ttc']);
            }
        );

        $checkForfait = $this->em->getRepository('CABCourseBundle:Forfait')->getForfaitByLongAndLat(
            $aParams['coordinateAddr']['lngDepart'],
            $aParams['coordinateAddr']['latDepart'],
            $aParams['coordinateAddr']['lngArrival'],
            $aParams['coordinateAddr']['latArrival']
        );

        $aForfait = array();
        if (0 !== count($checkForfait)) {
            foreach ($checkForfait as $forfait) {
                $oForfait = $this->em->getRepository('CABCourseBundle:Forfait')->find($forfait['id']);

                $aForfait = array(
                    $forfait['id'] => $oForfait,
                );
            }
        }
        return array(
            'personNumber' => $aParams['nbPerson'],
            'depDate' => $aParams['dateDeparture'],
            'depTime' => $aParams['timeDeparture'],
            'coordinate' => $aParams['coordinateAddr'],
            'departAddress' => $aParams['departureAddress'],
            'arrivalAddress' => $aParams['arrivalAddress'],
            'estimatedTime' => $aParams['estimatedTime'],
            'estimatedDistance' => $aParams['estimatedDistance'],
            'withBack' => $aParams['withBack'],
            'arrivalAddressBack' => $aParams['arrivalAddressRet'],
            'departAddressBack' => $aParams['departureAddressRet'],
            'dateBack' => $aParams['dateDepartureBack'],
            'timeBack' => $aParams['timeDepartureBack'],
            'estimatedTimeBack' => $aParams['estimatedTimeBack'],
            'estimatedDistanceBack' => $aParams['estimatedDistanceBack'],
            'coordinateBack' => $aParams['coordinateAddrBack'],
            'tarifID' => $idTarif,
            'totalDistance' => $aParams['totalDistance'],
            'company' => $aCompany,
            'forfait' => $aForfait,
            'durationText' => $aParams['estimatedTimeText'],
        );
    }

}