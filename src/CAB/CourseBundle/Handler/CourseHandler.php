<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 20/01/2016
 * Time: 22:20
 */

namespace CAB\CourseBundle\Handler;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\Tarif;
use CAB\CourseBundle\Manager\CabUserManager;
use CAB\CourseBundle\Manager\CourseManager;
use CAB\CourseBundle\Manager\DetailsTarifManager;
use CAB\CourseBundle\Manager\PlaningManager;
use CAB\CourseBundle\Manager\TarifManager;
use CAB\CourseBundle\Manager\ThreadManager;
use CAB\CourseBundle\PushNotification\Push;
use CAB\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use FOS\MessageBundle\Composer\Composer;
use FOS\MessageBundle\Sender\Sender;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use CAB\CourseBundle\Manager\ForfaitManager;

class CourseHandler
{
    protected $em;
    protected $logger;
    private $planingManager;
    private $messageSender;
    private $trans;
    private $tarifManager;
    private $courseManager;
    private $detailTarifManager;
    private $userManager;
    private $messageComposer;
    private $threadManager;
    private $geoHandler;
    private $price;
    private $planingHandler;
    private $push;
    private $environement;

    /**
     * Constructor
     *
     * @param EntityManager   $em     entity manager
     * @param LoggerInterface $logger logger
     */
    public function __construct(
        EntityManager $em,
        LoggerInterface $logger,
        PlaningManager $pm,
        Sender $ms,
        TranslatorInterface $trans,
        TarifManager $tm,
        CourseManager $cm,
        DetailsTarifManager $dtm,
        CabUserManager $cum,
        Composer $composer,
        ThreadManager $thread,
        GeolocationHandler $gh,
        CalculPriceHandler $price,
        PlaningHandler $planingHandler,
        Push $push,
        $environement
    ) {
        $this->em = $em;
        $this->logger = $logger;
        $this->planingManager = $pm;
        $this->messageSender = $ms;
        $this->trans = $trans;
        $this->tarifManager = $tm;
        $this->courseManager = $cm;
        $this->detailTarifManager = $dtm;
        $this->userManager = $cum;
        $this->messageComposer = $composer;
        $this->threadManager = $thread;
        $this->geoHandler = $gh;
        $this->price = $price;
        $this->planingHandler = $planingHandler;
        $this->push = $push;
        $this->environement = $environement;
    }

    /**
     * Search Available drivers and send internal message to each driver
     *
     * @param \DateTime $departureDate
     * @param \DateTime $arrivalDate
     * @param Course    $course
     * @param boolean   $edit
     * @param string    $payment
     */
    public function sendInternalMessage($departureDate, $arrivalDate, Course $course, $edit = false, $payment = 'card')
    {

        $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:*****');
        $availableDrivers = $this->planingManager
            ->checkAvailability($departureDate, $arrivalDate);
        foreach ($availableDrivers as $key => $avDriver) {
            /** @var User $oDriver */
            $oDriver = $this->userManager->loadUser($avDriver);

            if ($oDriver->getDriverCompany() != null) {
                if ($oDriver->getDriverCompany()->getId() != $course->getCompany()->getId()) {
                    unset($availableDrivers[$key]);
                }
            } else {
                unset($availableDrivers[$key]);
            }

        }

        $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php  $availableDrivers: $availableDrivers');
        $sender = $this->messageSender;
        $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php $sender: $sender');

        //Send notification to driver
        if (!empty($availableDrivers)) {
            $clientName = $course->getClient()->getUsedName();

            $this->logger->info(
                'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** clientName: '.$clientName
            );

            if (!$edit) {
                $subject = $this->trans->trans("New course is just created");
                $body = $this->trans->trans(
                    "A new course created by %name% from %departure% to %arrival% on %datetime%.<br/>
                To check the course click on this link below:<br/>
                ",
                    array(
                        '%name%' => $clientName,
                        '%departure%' => $course->getStartedAddress(),
                        '%arrival%' => $course->getArrivalAddress(),
                        '%datetime%' => $course->getDepartureTime()->format('Y-m-d H:i:s'),
                    )
                );
            } else {
                $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** $subject: ');
                $subject = $this->trans->trans("Course is modified by customer");
                $this->logger->info(
                    'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** $subject: '.$subject
                );
                $body = $this->trans->trans(
                    "A course has been modified. Below all information of the course<br/>
                    Modified by  %name% from %departure% to %arrival% on %datetime%.<br/>
                    To check the course click on this link below:<br/>
                    ",
                    array(
                        '%name%' => $clientName,
                        '%departure%' => $course->getStartedAddress(),
                        '%arrival%' => $course->getArrivalAddress(),
                        '%datetime%' => $course->getDepartureDate()->format('Y-m-d').' '.$course->getDepartureTime(
                            )->format('H:i:s'),
                    )
                );
            }

            $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** $body: '.$body);
            if ($payment == 'cash') {
                $this->logger->info(
                    'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** cach start '
                );
                $body .= "<br> Note: Cash payement";
                $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** cach fin ');
            }
            $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** env start ');
            if ($this->environement == 'dev') {
                $this->logger->info(
                    'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** env dev start '
                );
                $body .= ' <a href="/app_dev.php/admin/en/course/'.$course->getId().'/show">Course</a>';
                $this->logger->info(
                    'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** env dev end '
                );
            } else {
                $this->logger->info(
                    'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** env prod start '
                );
                $body .= ' <a href="/admin/en/course/'.$course->getId().'/show">Course</a>';
                $this->logger->info(
                    'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** env prod end '
                );
            }
            $this->logger->info(
                'CAB/CourseBundle/Handler/CourseHandler.php  sendInternalMessage:***** before ParseDriversAndSendMessage '
            );


            $this->ParseDriversAndSendMessage($course, $availableDrivers, $subject, $body, $sender);

            $this->logger->info('CAB/CourseBundle/Handler/CourseHandler.php after ParseDriversAndSendMessage:***** : ');
        }
    }

    /**
     * Send internal message to each driver in given list
     *
     * @param Course $course
     * @param array  $availableDrivers
     * @param string $subject
     * @param string $body
     */
    public function sendInternalMessageWithoutCheck(
        Course $course,
        array $availableDrivers,
        $subject = null,
        $body = null
    ) {
        $sender = $this->messageSender;
        //Send notification to driver
        if (!empty($availableDrivers)) {
            $clientName = $course->getClient()->getUsedName();

            if (!$subject) {
                $subject = $this->trans->trans("Course has been modified by customer");
            }
            if (!$body) {
                $body = $this->trans->trans(
                    "A course has been modified. Below all information of the course<br/>
                Modified by  %name% from %departure% to %arrival% on %datetime%.<br/>
                To check the course click on this link below:<br/>
                ",
                    array(
                        '%name%' => $clientName,
                        '%departure%' => $course->getStartedAddress(),
                        '%arrival%' => $course->getArrivalAddress(),
                        '%datetime%' => $course->getDepartureDate()->format('Y-m-d').' '.$course->getDepartureTime(
                            )->format('H:i:s'),
                    )
                );

                if ($this->environement == 'dev') {
                    $body .= ' <a href="/app_dev.php/admin/en/course/'.$course->getId().'/show">Course</a>';
                } else {
                    $body .= ' <a href="/admin/en/course/'.$course->getId().'/show">Course</a>';
                }
            }

            $this->ParseDriversAndSendMessage($course, $availableDrivers, $subject, $body, $sender, $subject);
        }
    }

    /**
     * Send internal message to each driver in given list
     *
     * @param Course $course
     * @param array  $availableDrivers
     */
    public function doNotification(Course $course, array $receivers, $subject, $body)
    {
        $sender = $this->messageSender;
        //Send notification to driver
        if (!empty($availableDrivers)) {
            $clientName = $course->getClient()->getUsedName();

            $this->ParseDriversAndSendMessage($course, $availableDrivers, $subject, $body, $sender);
        }
    }

    public function checkUpdateDateTimeCourse(Course $course, $lastDepDate, $lastDepTime)
    {
        $lastDepDate = $lastDepDate." 00:00:00";
        $lastDepTime = '1970-01-01 '.$lastDepTime;
        $departureDate = \dateTime::createFromFormat('Y-m-d H:i:s', $lastDepDate);
        $departureTime = \dateTime::createFromFormat('Y-m-d H:i:s', $lastDepTime);

        if ($course->getDepartureDate() == $departureDate && $course->getDepartureTime() == $departureTime) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Method description
     *
     * @param Course        $course
     * @param array         $availableDrivers
     * @param string        $subject
     * @param string        $body
     * @param messageSender $sender
     */
    public function ParseDriversAndSendMessage(
        Course $course,
        array $availableDrivers,
        $subject,
        $body,
        $sender,
        $messagePush = null
    ) {

        $data = array(
            'type_notification' => 'driver_notification_course',
            'subject_notification' => $subject,
            'type' => 1,
            'sound' => 'aaaaa',
            'alert' => 'Merci de repondre'
        );

        $serviceUser = $this->userManager;

        //$availableDrivers = array_slice($availableDrivers, 0, 3);

        foreach ($availableDrivers as $item) {
            $oDriver = $serviceUser->loadUser($item);
            if ($oDriver->getDriverCompany()) {
                $adminCompany = $oDriver->getDriverCompany()->getContact();
                if ($adminCompany === null) {
                    $adminCompany = $this->userManager->loadUser(1);
                }
                $threadBuilder = $this->messageComposer->newThread();
                $threadBuilder
                    ->addRecipient($oDriver)// Retrieved from your backend, your user manager or ...
                    ->setSender($adminCompany)
                    ->setSubject($subject)
                    ->setBody($body);

                $sender->send($threadBuilder->getMessage());
                $thread = $threadBuilder->getMessage()->getThread();
                $this->threadManager->setAttr($thread, 'course', $course);
                //Push notification
                if (!$messagePush) {
                    $messagePush = 'A new course is pending';
                }

                $this->push->getServiceByOS($oDriver->getDevices()->toArray(), $messagePush, $data);
            }
        }
    }

    /**
     * create the course and manage the next step switch the result.
     *  1. success                => redirect to finalizebookingAction
     *  2. error                  => redirect to home page
     *  3. user not authenticated => redirect to page register
     *
     * @param array  $params
     * @param array  $user
     * @param Course $course
     *
     * @return array
     *
     * @throws \Exception
     */
    public function confirmBooking(array $params, array $user, Course $course)
    {
        $oTarif = null;
        if ($params['company']) {
            $oCompany = $params['company'];
            /** @var PersistentCollection $oTarif */
            $oTarif = $oCompany->getCompanyTarif();
        }
        if ($user['authorizationChecker']->isGranted('ROLE_USER')) {
            list($departureDate, $arrivalDate) = $this->initConfirmBooking($params);

            $availableDrivers = $this->planingManager->checkAvailability($departureDate, $arrivalDate);
            if (count($params) !== 0 && count($availableDrivers) !== 0) {
                if ($course->getClient() === null) {
                    if ($oTarif instanceof PersistentCollection && $oTarif->count() > 0) {
                        $oTarif = $oTarif->first();
                    }
                    $course = $this->courseManager->manageCourse($course, $params, $user['tokenStorage'], $oTarif);
                } else {
                    $course->setClient(null);
                }

                $paramsDetailsTarif = array(
                    'tarifTotalHT' => $params['totalPrice'],
                    'tarifTotalTTC' => $params['totalPriceTTC'],
                    'estimatedTarif' => $params['estimatedTarif'],
                    'isForfait' => $params['isForfait'],
                );

                $result = $this->detailTarifManager->saveObject($paramsDetailsTarif);

                if ($result['status']) {
                    $course->setCourseDetailsTarif($result['response']);
                    if ($course->getDriver() !== null) {
                        $oVehicle = $course->getDriver()->getVehicle();
                        $course->setVehicle($oVehicle);
                    }
                    $this->em->persist($course);
                    $this->em->flush();
                   // $this->em->persist($user['tokenStorage']);
                } else {
                    $result = array(
                        'status' => 0,
                        'value' => 'Error saving course',
                    );
                }
            } else {
                $course = null;
            }
            if (!$course) {
                $result = array(
                    'status' => 0,
                    'value' => 'Error course',
                );
            } else {
                $result = array(
                    'status' => 1,
                    'value' => 'finalize_booking',
                    'course' => $course,
                );
            }
        } else {
            if (count($params) !== 0) {
                if ($params['company']) {
                    $course = $this->courseManager->manageCourse($course, $params, null, $oTarif);

                    $paramsDetailsTarif = array(
                        'tarifTotalHT' => $params['totalPrice'],
                        'tarifTotalTTC' => $params['totalPriceTTC'],
                        'estimatedTarif' => $params['estimatedTarif'],
                        'isForfait' => $params['isForfait'],
                    );

                    $result = $this->detailTarifManager->saveObject($paramsDetailsTarif);
                    if ($result['status']) {
                        $course->setCourseDetailsTarif($result['response']);
                        if ($course->getDriver() !== null) {
                            $oVehicle = $course->getDriver()->getVehicle();
                            $course->setVehicle($oVehicle);
                        }
                        $this->em->persist($course);
                        $this->em->flush();
                    }
                    $result = array(
                        'status' => 1,
                        'company' => $params['company'],
                        'course' => $course,
                    );
                } else {
                    $result = array(
                        'status' => 0,
                        'value' => 'Error company ID',
                    );
                }
            } else {
                $result = array(
                    'status' => 0,
                    'value' => 'Error params course',
                );
            }
        }

        return $result;
    }

    /**
     * Method description
     *
     * @param array $params
     *
     * @return array
     */
    protected function initConfirmBooking(array $params)
    {
        $depDate = $params['depDate'];
        $depTime = $params['depTime'];
        $estimTime = $params['estimatedTime'];
        $departureDate = \dateTime::createFromFormat('Y-m-d H:i', $depDate.' '.$depTime);
        $arrivalDate = \dateTime::createFromFormat('Y-m-d H:i', $depDate.' '.$depTime);
        $intervall = 'PT'.$estimTime.'S';
        $arrivalDate = $arrivalDate->add(new \DateInterval($intervall));

        return array($departureDate, $arrivalDate);
    }

    public function saveCourse(Course $course) {
        $this->em->persist($course);
    }

    public function getCourseManager()
    {
        return $this->courseManager;
    }
}