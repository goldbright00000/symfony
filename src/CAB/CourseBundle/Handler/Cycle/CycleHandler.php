<?php

/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 14/12/2016
 * Time: 19:54
 */
namespace CAB\CourseBundle\Handler\Cycle;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Manager\CourseManager;

class CycleHandler
{

    /**
     * @var CourseManager
     */
    private $courseManager;

    /**
     * CycleHandler constructor.
     * @param CourseManager $courseManager
     */
    public function __construct(CourseManager $courseManager)
    {
        $this->courseManager = $courseManager;
    }

    /**
     * @param $course
     * @return array
     */
    public function initCycle($course)
    {
        $firstDateOfCycle = $course->getDepartureDate();
        $lastDateOfCycle = $course->getCycleEndDate();

        $isMondayInCycle = $course->getMondayCycle();
        $isTuesdayInCycle = $course->getTuesdayCycle();
        $isWednesdayInCycle = $course->getWednesdayCycle();
        $isThursdayInCycle = $course->getThursdayCycle();
        $isFridayInCycle = $course->getFridayCycle();
        $isSaturdayInCycle = $course->getSaturdayCycle();
        $isSundayInCycle = $course->getSundayCycle();

        $aDatesOfCycle = $this->dateRange($firstDateOfCycle->format("Y-m-d"), $lastDateOfCycle->format("Y-m-d"), "+1 day", "Y-m-d");

        if (!$isMondayInCycle) {
            unset($aDatesOfCycle['Monday']);
        }
        if (!$isTuesdayInCycle) {
            unset($aDatesOfCycle['Tuesday']);
        }
        if (!$isWednesdayInCycle) {
            unset($aDatesOfCycle['Wednesday']);
        }
        if (!$isThursdayInCycle) {
            unset($aDatesOfCycle['Thursday']);
        }
        if (!$isFridayInCycle) {
            unset($aDatesOfCycle['Friday']);
        }
        if (!$isSaturdayInCycle) {
            unset($aDatesOfCycle['Saturday']);
        }
        if (!$isSundayInCycle) {
            unset($aDatesOfCycle['Sunday']);
        }

        $aDays = array();
        foreach ($aDatesOfCycle as $day => $date) {
            $aDays[] = $day;
        }

        return array($aDatesOfCycle, $aDays);
    }

    public function getRange($first, $last, $step, $output_format = 'd/m/Y', $aSpecDays)
    {
        return $this->dateRange($first, $last, $step, $output_format, $aSpecDays);
    }

    /**
     * @param string $first 2016-01-31
     * @param string $last 2016-01-31
     * @param string $step +1 day
     * @param string $output_format d/m/Y
     * @return array
     */
    private function dateRange($first, $last, $step = '+1 day', $output_format = 'd/m/Y', array $aSpecDays = array() )
    {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
        while( $current < $last ) {
            $current = strtotime($step, $current);
            $key = date('l', $current);
            if (count($aSpecDays)) {
                foreach ($aSpecDays as $specDay) {
                    if ($specDay == $key) {
                        $dates[$key][] = date($output_format, $current);
                    }
                }
            } else {
                $dates[$key][] = date($output_format, $current);
            }
        }

        return $dates;
    }

    /**
     * @param Course $course
     * @param \DateTime $dateDeparture
     */
    public function setDuplicateCourse($course, $dateDeparture)
    {
        $courseDuplicated = clone $course;
        $courseDuplicated->setDepartureDate($dateDeparture);
        $courseDuplicated->setCycleParent($course->getId());
        $courseDuplicated->setCourseStatus(Course::STATUS_AFFECTED);
        $courseDuplicated->setIsCycle(false);
        $this->courseManager->saveCourse($courseDuplicated);
    }

    public function setChangeCycle ($courseChild) {
        $this->courseManager->saveCourse($courseChild);
    }

}