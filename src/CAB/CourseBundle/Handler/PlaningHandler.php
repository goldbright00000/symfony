<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 20/01/2016
 * Time: 22:20
 */

namespace CAB\CourseBundle\Handler;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\Planing;
use CAB\CourseBundle\Manager\PlaningManager;
use FOS\UserBundle\Model\UserInterface;

/**
 * Class PlaningHandler
 *
 * @package CAB\CourseBundle\Handler
 */
class PlaningHandler extends BaseHandler
{
    /**
     * @var PlaningManager
     */
    protected $planingManager;

    protected $geolocationHandler;

    /**
     * Method description
     *
     * @param PlaningManager $planingManager
     */
    public function setService(PlaningManager $planingManager)
    {
        $this->planingManager = $planingManager;
    }

    public function setGeolocation(GeolocationHandler $geolocationHandler)
    {
        $this->geolocationHandler = $geolocationHandler;
    }

    /**
     * create|edit planing object
     *
     * @param UserInterface|null $oDriver
     * @param Course             $course
     * @param string             $planingName
     * @param Planing|null       $planing
     *
     * @return mixed
     */
    public function handlerEvent($oDriver, Course $course, $planingName = 'Accepted by driver', $planing = null)
    {
        $oDepartureDate = $course->getDepartureDate();
        $oDepartureTime = $course->getDepartureTime();
        $duration = $course->getCourseTime();
        $fDate = $oDepartureDate->format('Y-m-d').' '.$oDepartureTime->format('H:i:s');

        $departureDate = \dateTime::createFromFormat('Y-m-d H:i:s', $fDate);
        $arrivalDate = \dateTime::createFromFormat('Y-m-d H:i:s', $fDate);
        $intervall = 'PT'.$duration.'S';

        $arrivalDate = $arrivalDate->add(new \DateInterval($intervall));

        $params =  array(
            'planingName' => $planingName,
            'course' => $course,
            'start' => $departureDate,
            'end' => $arrivalDate,
            'driver' => $oDriver,
        );
        $status = $this->planingManager->saveObject($params, $planing);

        return $status;
    }

    /**
     * find the availables drivers
     *
     * @param array $params
     *  $params(
     *      'departureDate' => '2016-01-25',
     *      'departureTime' => '10:00:00',
     *      'startedAddress' => "31000 Toulouse, France",
     *      'arrivalAddress' => "75001 Paris, France",
     * )
     *
     * @return array
     */
    public function checkAvailableDriver($params)
    {
        $depDate = $params['departureDate'];
        $depTime = $params['departureTime'];

        list($distance, $estimTime) = $this->geolocationHandler->getDistanceMatrix(
            $params['startedAddress'],
            $params['arrivalAddress']
        );
        $fDate = $depDate.' '.$depTime;

        $departureDate = \dateTime::createFromFormat('Y-m-d H:i', $fDate);
        $arrivalDate = \dateTime::createFromFormat('Y-m-d H:i', $fDate);
        $intervall = 'PT'.$estimTime.'S';
        $arrivalDate = $arrivalDate->add(new \DateInterval($intervall));
        $availableDrivers = $this->planingManager->checkAvailability($departureDate, $arrivalDate);

        return array($availableDrivers);
    }
}