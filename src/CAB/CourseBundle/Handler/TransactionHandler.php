<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 28/01/2016
 * Time: 00:20
 */

namespace CAB\CourseBundle\Handler;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Manager\TransactionPaymentManager;
use FOS\UserBundle\Model\UserInterface;
use CAB\CourseBundle\Exception\InvalidCreationTransactionException;
use FOS\UserBundle\Model\UserManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class TransactionHandler
 *
 * @package CAB\CourseBundle\Handler
 */
class TransactionHandler
{
    /**
     * @var TransactionPaymentManager
     */
    private $transactionManager;

    /**
     * @var CourseHandler
     */
    private $courseHandler;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var LoggerInterface
     */
    private $userManager;

    /**
     * @param TransactionPaymentManager $transactionManager
     * @param LoggerInterface           $logger
     */
    public function __construct(
        TransactionPaymentManager $transactionManager,
        LoggerInterface $logger,
        CourseHandler $courseHandler,
        UserManager $userManager
    ) {
        $this->transactionManager = $transactionManager;
        $this->courseHandler = $courseHandler;
        $this->logger = $logger;
        $this->userManager = $userManager;
    }

    /**
     * Send Internal message when the payement is authorised
     *
     * @param Course $oCourse
     */
    public function sendMessageAfterPayment(Course $oCourse, $paymentMode = 'cach')
    {

        $this->logger->info('CAB/CourseBundle/Handler/TransactionHandler.php sendMessageAfterPayment:*****');
        // Send internal message to all driver availables
        $depDate = $oCourse->getDepartureDate()->format('Y-m-d');
        $depTime = $oCourse->getDepartureTime()->format('H:i:s');
        $estimTime = $oCourse->getCourseTime();
        $fDate = $depDate.' '.$depTime;

        $this->logger->info('CAB/CourseBundle/Handler/TransactionHandler.php sendMessageAfterPayment:***** '.$fDate);
        $departureDate = \dateTime::createFromFormat('Y-m-d H:i:s', $fDate);
        $this->logger->info(
            'CAB/CourseBundle/Handler/TransactionHandler.php $intervall:***** '.$departureDate->format('Y-m-d H.i.s')
        );
        $arrivalDate = \dateTime::createFromFormat('Y-m-d H:i:s', $fDate);
        $this->logger->info(
            'CAB/CourseBundle/Handler/TransactionHandler.php $intervall:***** '.$arrivalDate->format('Y-m-d H.i.s')
        );
        $intervall = 'PT'.$estimTime.'S';
        $this->logger->info('CAB/CourseBundle/Handler/TransactionHandler.php $intervall:***** '.$intervall);
        $arrivalDate = $arrivalDate->add(new \DateInterval($intervall));

        $this->logger->info('CAB/CourseBundle/Handler/TransactionHandler.php before sendInternalMessage:***** ');
        $this->courseHandler->sendInternalMessage($departureDate, $arrivalDate, $oCourse, false, $paymentMode);
    }

    /**
     * Method description
     *
     * @param Course           $oCourse
     * @param string           $paymentMode
     * @param array            $systempay
     * @param UserInterface    $user
     * @param SessionInterface $session
     *
     * @return array
     */
    public function doPayement(
        Course $oCourse,
        $paymentMode,
        array $systempay,
        UserInterface $user,
        SessionInterface $session
    ) {
        $tarifTTC = $oCourse->getCourseDetailsTarif()->getEstimatedTarif();

        $this->logger->info('mode payment : '.$paymentMode);
        $this->sendMessageAfterPayment($oCourse, $paymentMode);
        return array(
            'type' => $paymentMode,
            'course' => $oCourse,
            'tarif_ttc' => $tarifTTC,
            'user' => $user,
        );
        $this->logger->info('*********************************begin payment****************************************');
        $session->getFlashBag()->add('course_run_payment', $oCourse);
        $tarifTTC = round($tarifTTC, 2) * 100;
        $dateTransaction = new \DateTime('now', new \DateTimeZone('UTC'));
        $dateTransactionFormat = $dateTransaction->format('YmdHis');

        $this->logger->info('begin payment: amount - '.$tarifTTC);

        //delay
        $oCourse->getDepartureDate();
        $interval = $oCourse->getDepartureDate()->diff($dateTransaction);
        $delay = $interval->format('%a days');
        $delay = explode(' ', $delay)[0];
        $oTransaction = $this->transactionManager->createTransaction(
            array(
                'amount' => $tarifTTC,
                'transactionPaymentCourse' => $oCourse,
                'transactionDate' => $dateTransactionFormat,
                'modeAction' => 'INTERACTIVE',
                'currency' => 978,
                'pageAction' => 'PAYMENT',
                'version' => 'V2',
                'paymentConfig' => 'SINGLE',
                'captureDelay' => $delay,
                'ctxMode' => 'test',
            )
        );

        if (is_object($oTransaction)) {
            $countID = strlen($oTransaction->getId());
        } else {
            $this->logger->error('throw transaction - '.$oTransaction['message']);
            throw new InvalidCreationTransactionException($oTransaction['message'], $oTransaction['code']);
        }
        $diff = 6 - $countID;
        $reqId = '';

        for ($i = 0; $i < $diff; $i++) {
            $reqId .= 0;
        }
        $reqId .= $oTransaction->getId();

        $shopId = $systempay['vads_site_id'];
        $cert = $systempay['certificat'];

        $paramCustomer['vads_cust_email'] = $user->getEmail();
        $paramCustomer['vads_redirect_error_message'] = "You will be redirected to marchand site";
        $paramCustomer['vads_redirect_error_timeout'] = 0;
        $paramCustomer['vads_redirect_success_message'] = "You will be redirected to marchand site";
        $paramCustomer['vads_redirect_success_timeout'] = 0;
        $paramCustomer['vads_return_mode'] = 'POST';
        $paramCustomer['vads_order_id'] = $oCourse->getId();
        $paramCustomer['vads_order_info'] = "Course from ".$oCourse->getStartedAddress(
            )." to ".$oCourse->getArrivalAddress();
        $paramCustomer['vads_order_info2'] = "On ".$oCourse->getDepartureDate()->format(
                'Y-m-d'
            )." at ".$oCourse->getDepartureTime()->format('h:i');
        $paramCustomer['vads_order_info3'] = '';
        // The delay + 3 days
        $paramCustomer['vads_capture_delay'] = $delay + 3;
        if ($user->getPaymentId() == null) {
            $vadsPageAction = "REGISTER_PAY";
            $paramCustomer['vads_cust_legal_name'] = $user->getNameCompany();
            $paramCustomer['vads_cust_first_name'] = $user->getFirstName();
            $paramCustomer['vads_cust_last_name'] = $user->getLastName();
            $paramCustomer['vads_cust_id'] = $user->getId();
            $paramCustomer['vads_cust_title'] = $user->getSexe();
            $paramCustomer['vads_cust_status'] = $user->getIsCompany() ? 'COMPANY' : 'PRIVATE';
            $valueFields = "INTERACTIVE+$tarifTTC+".$paramCustomer['vads_capture_delay']."+TEST+978+".
                $paramCustomer['vads_cust_email']."+".
                $paramCustomer['vads_cust_first_name']."+".
                $paramCustomer['vads_cust_id']."+".
                $paramCustomer['vads_cust_last_name']."+".
                $paramCustomer['vads_cust_legal_name']."+".
                $paramCustomer['vads_cust_status']."+".
                $paramCustomer['vads_cust_title']."+".
                $paramCustomer['vads_order_id']."+".
                $paramCustomer['vads_order_info']."+".
                $paramCustomer['vads_order_info2']."+".
                $paramCustomer['vads_order_info3']."+".
                "$vadsPageAction+SINGLE+".
                $paramCustomer['vads_redirect_error_message']."+".
                $paramCustomer['vads_redirect_error_timeout']."+".
                $paramCustomer['vads_redirect_success_message']."+".
                $paramCustomer['vads_redirect_success_timeout']."+".
                $paramCustomer['vads_return_mode']."+".
                "$shopId+$dateTransactionFormat+$reqId+V2+$cert";
        } else {
            $vadsPageAction = "PAYMENT";

            $paramCustomer['vads_identifier'] = $paymentMode;
            $valueFields = "INTERACTIVE+$tarifTTC+".$paramCustomer['vads_capture_delay']."+TEST+978+".
                $paramCustomer['vads_cust_email']."+".
                $paramCustomer['vads_identifier']."+".
                $paramCustomer['vads_order_id']."+".
                $paramCustomer['vads_order_info']."+".
                $paramCustomer['vads_order_info2']."+".
                $paramCustomer['vads_order_info3']."+".
                "$vadsPageAction+SINGLE+".
                $paramCustomer['vads_redirect_error_message']."+".
                $paramCustomer['vads_redirect_error_timeout']."+".
                $paramCustomer['vads_redirect_success_message']."+".
                $paramCustomer['vads_redirect_success_timeout']."+".
                $paramCustomer['vads_return_mode']."+".
                "$shopId+$dateTransactionFormat+$reqId+V2+$cert";
        }

        $signature = sha1($valueFields);
        $this->transactionManager->setAttr($oTransaction, 'signature', $signature);
        $this->logger->info('begin payment: valueFields - '.$valueFields);

        return array(
            'type' => $paymentMode,
            'course' => $oCourse,
            'transactionDate' => $dateTransactionFormat,
            'reqId' => $reqId,
            'signature' => $signature,
            'customer_params' => $paramCustomer,
            'vads_page_action' => $vadsPageAction,
            'tarif_ttc' => $tarifTTC,
        );
    }

    /**
     * Method description
     *
     * @param UserInterface $user
     * @param array         $systemPay
     *
     * @return array
     */
    public function calculSignature(UserInterface $user, array $systemPay)
    {
        $dateTransaction = new \DateTime('now', new \DateTimeZone('UTC'));
        $dateTransactionFormat = $dateTransaction->format('YmdHis');

        $shopId = $systemPay['vads_site_id'];
        $cert = $systemPay['certificat'];
        $params['vads_redirect_error_message'] = "You will be redirected to marchand site";
        $params['vads_redirect_error_timeout'] = 0;
        $params['vads_redirect_success_message'] = "You will be redirected to marchand site";
        $params['vads_redirect_success_timeout'] = 0;
        $params['dateTransactionFormat'] = $dateTransactionFormat;
        $valueFields = 'INTERACTIVE+TEST+978+'.$user->getCity().'+'.$user->getCountry().'+'.$user->getEmail().'+'.$user->getFirstName().'+'.$user->getId().'+'.$user->getLastName().'+'.$user->getNameCompany().'+'.$user->getIsCompany().'+'.$user->getSexe().'+'.$user->getPostCode().'+'.'REGISTER+'.
            $params['vads_redirect_error_message'].'+'.
            $params['vads_redirect_error_timeout'].'+'.
            $params['vads_redirect_success_message'].'+'.
            $params['vads_redirect_success_timeout'].'+'.
            'POST+'."$shopId+$dateTransactionFormat+V2+$cert";

        $signature = sha1($valueFields);

        return array($signature, $params);
    }

    public function updateIdentifiantUser($vadsCustEmail, $vadsIdentifier)
    {
        $userCustomer = $this->userManager->findUserByEmail($vadsCustEmail);
        $userCustomer->setPaymentId($vadsIdentifier);
        $this->userManager->updateUser($userCustomer);

        return $userCustomer;
    }

}