<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 20/01/2016
 * Time: 22:20
 */

namespace CAB\CourseBundle\Handler;

use CAB\AdminBundle\Admin\AbstractAdmin;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class GeolocationHandler
 *
 * @package CAB\CourseBundle\Handler
 */
class GeolocationHandler extends BaseHandler
{
    /**
     * @var
     */
    protected $google;
    /**
     * @var
     */
    protected $om;

    /**
     * set argument google
     *
     * @param array $google
     */
    public function setArgument(array $google)
    {
        $this->google = $google;
    }

    /**
     * Method description
     *
     * @param ObjectManager $om
     */
    public function setDoctrineMongoDB(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Get the distance and the duration by calling google API. the origin and the destination params can be string or
     * float
     *
     * @param string|float $originAddress
     * @param string|float $destinationAddress
     *
     * @return array
     */
    public function getDistanceMatrix($originAddress, $destinationAddress, $courseLatLongDep = false,
                                      $courseLatLongArr = false,
                                      $durationFromatText = false)
    {

        $google = $this->google;
        $keyGoogleMap = $google['map_api_key'];
        $keyServerGoogleMap = $google['server_key'];
        $origin = urlencode($originAddress);
        $destination = urlencode($destinationAddress);
        if (!$courseLatLongDep || !$courseLatLongArr) {

            $distancematrix = json_decode(
                file_get_contents(
                    "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&serverkey=$keyServerGoogleMap"
                )
            );
        } else {
            $distancematrix = json_decode(
                file_get_contents(
                    "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$courseLatLongDep&destinations=$courseLatLongArr&serverkey=$keyServerGoogleMap"
                )
            );
        }

        $rows = $distancematrix->rows[0];
        $aDataDistance = 0;
        $aDataDuration = 0;

        if (property_exists($rows->elements[0], 'distance')) {
            $aDataDistance = $rows->elements[0]->distance;
        }

        if (property_exists($rows->elements[0], 'duration')) {
            $aDataDuration = $rows->elements[0]->duration;
        }

        if (is_object($aDataDistance)) {
            $distance = round($aDataDistance->value / 1000, 1);
        } else {
            $distance = $aDataDistance;
        }

        if (is_object($durationFromatText)){
            $duration = $aDataDuration->text;
        } elseif(is_object($aDataDuration)) {
            $duration = $aDataDuration->value;
        } else {
            $duration = $aDataDuration;
        }

        return array($distance, $duration);
    }


    /**
     * Get the distance and the duration by calling google API. the origin and the destination params can be string or
     * float
     *
     * @param string|float $driverLatLong
     * @param string|float $raceLatLong
     *
     * @return array
     */
    public function getAddressByDriver($driverLat,$driverLong)
    {

        $keylocationiq = '48f1b1c0fb78f2';
        $lat = urlencode($driverLat);
        $long = urlencode($driverLong);

        if (isset($lat)) {
            $curl = curl_init('https://eu1.locationiq.com/v1/reverse.php?key='.$keylocationiq.'&lat='.$lat.'&lon='.$long.'&format=json');

            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER    =>  true,
                CURLOPT_FOLLOWLOCATION    =>  true,
                CURLOPT_MAXREDIRS         =>  10,
                CURLOPT_TIMEOUT           =>  30,
                CURLOPT_CUSTOMREQUEST     =>  'GET',
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;

            } else {
                $road = ' ';
                $postcode = ' ';
                $village = ' ';
                $country = ' ';

            $addr = $response ;
            $value = json_decode($addr, true);

            if (isset($value['address']['road'])){
                $road = $value['address']['road'];
            }
            if (isset($value['address']['postcode'])){
                $postcode = $value['address']['postcode'];
            }
            if (isset($value['address']['village'])){
                $village = $value['address']['village'];
            }
            if (isset($value['address']['country'])){
                $country = $value['address']['country'];
            }
            }

        }
        return array('road'=>$road, 'postcode'=>$postcode, 'village'=>$village,'country'=> $country);
    }



    /**
     * Get the distance and the duration by calling google API. the origin and the destination params can be string or
     * float
     *
     * @param string|float $driverLatLong
     * @param string|float $raceLatLong
     *
     * @return array
     */
    public function getDistanceMatrixDriver($driverLatLong, $raceLatLong)
    {

        $app_id = 'SwwoDXGzVt198ybVyZ6l';
        $app_code = 'VRl28m4nCLieuds47Ei8iw';
        $origin = urlencode($driverLatLong);
        $destination = urlencode($raceLatLong);


        if (isset($driverLatLong)) {
            $curl = curl_init(
                'https://route.api.here.com/routing/7.2/calculateroute.json?waypoint0='.$origin.'&waypoint1='.$destination.'&mode=fastest%3Bcar%3Btraffic%3Aenabled&app_id='.$app_id.'&app_code='.$app_code.'&departure=now'
            );


            curl_setopt_array(
                $curl,
                array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                )
            );
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            $matrix = $response ;
            $value = json_decode($matrix, true);


            $matrixdistance = ' ';
            $matrixtime = ' ';
            $matrixtimetraffic = '';
            $matrix = $value['response']['route'];
            $matrixvalue = $matrix[0];
            $matrixdistance = round($matrixvalue['summary']['distance']/1000,2);
            $matrixtime = round($matrixvalue['summary']['baseTime']/60);
            $matrixtimetraffic = round($matrixvalue['summary']['trafficTime']/60);



            return array('matrixdistance'=>$matrixdistance, 'matrixtime'=>$matrixtime, 'matrixtimetraffic'=>$matrixtimetraffic);
        }else {
            return array('matrixdistance'=>0, 'matrixtime'=>0, 'matrixtimetraffic'=>0);
        }
    }

    /**
     * Get the distance and the duration by calling google API. the origin and the destination params can be string or
     * float
     *
     * @param string|float $originAddress
     * @param string|float $destinationAddress
     *
     * @return array
     */
    public function getDistanceMatrixCommand($originAddress, $destinationAddress, $durationFromatText = false)
    {

        $google = $this->google;
        $keyGoogleMap = $google['map_api_key'];
        $keyServerGoogleMap = $google['server_key'];
        $origin = urlencode($originAddress);
        $destination = urlencode($destinationAddress);
        $distancematrix = json_decode(
            file_get_contents(
                "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$origin&destinations=$destination&serverkey=$keyServerGoogleMap"
            )
        );

        $rows = $distancematrix->rows[0];
        $aDataDistance = 0;
        $aDataDuration = 0;

        if (property_exists($rows->elements[0], 'distance')) {
            $aDataDistance = $rows->elements[0]->distance;
        }

        if (property_exists($rows->elements[0], 'duration')) {
            $aDataDuration = $rows->elements[0]->duration;
        }

        if (is_object($aDataDistance)) {
            $distance = round($aDataDistance->value / 1000, 1);
        } else {
            $distance = $aDataDistance;
        }

        if (is_object($durationFromatText)){
            $duration = $aDataDuration->text;
        } elseif(is_object($aDataDuration)) {
            $duration = $aDataDuration->value;
        } else {
            $duration = $aDataDuration;
        }

        return array($distance, $duration);
    }


    /**
     * Get customer's lat and long to be compatible with url
     *
     * @param array $params
     *
     * @return bool|string
     */
    public function customerCoords(array $params = array())
    {
        if (!isset($params['latitude']) || !isset($params['longitude'])) {
            return false;
        }

        return urlencode($params['latitude'].','.$params['longitude']);
    }


    /**
     * Method description
     *
     * @param Geolocation|GeolocationCustomer $dGeolocation
     * @param array                           $params
     * @param string                          $type
     *
     * @return mixed
     */
    public function processForm($dGeolocation, $params, $type)
    {
        $method = 'set'.ucfirst($type);
        $dGeolocation->$method($params[$type]);
        $dGeolocation->setAccuracy($params['accuracy']);
        $dGeolocation->setAltitude($params['altitude']);
        $dGeolocation->setLongitude($params['longitude']);
        $dGeolocation->setLatitude($params['latitude']);
        $dGeolocation->setHeading($params['heading']);
        if ($type == 'driver') {
            $dGeolocation->setSpeed($params['speed']);
            $dGeolocation->setAvailibility($params['availibility']);
        }
        //$dGeolocation->setDateGeolocate($params['date_geolocate']);
        //@TODO check if the userId have a role driver
        $this->om->persist($dGeolocation);
        $this->om->flush();

        return $dGeolocation;
    }


}