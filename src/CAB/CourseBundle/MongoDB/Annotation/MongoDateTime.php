<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 18/01/2016
 * Time: 21:10
 */

namespace CAB\CourseBundle\MongoDB\Annotation;

use Doctrine\ODM\MongoDB\Mapping\Annotations\AbstractField;

/**
 * @Annotation
 */
class MongoDateTime extends AbstractField
{
    public $type = 'mongodatetimetype';
}