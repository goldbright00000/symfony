<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 18/01/2016
 * Time: 20:44
 */

namespace CAB\CourseBundle\MongoDB\Types;

use Doctrine\ODM\MongoDB\Types\Type;
use Doctrine\ORM\EntityManager;

/**
 * DateTime ustom datatype.
 */
class MongoDateTimeType extends Type
{
    private $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * This function is only called when your custom type is used as an identifier.
     * For other cases, closureToPHP() will be called.
     *
     * @param mixed $value
     *
     * @return \DateTime
     */
    public function convertToPHPValue($value)
    {
        return new \DateTime('@'.$value->sec);
    }

    /**
     * Return the string body of a PHP closure that will receive $value
     * and store the result of a conversion in a $return variable
     *
     * @return string
     */
    public function closureToPHP()
    {
        return '$return = new \DateTime($value);';
    }

    /**
     * This is called to convert a PHP value to its Mongo equivalent
     *
     * @param mixed $value
     *
     * @return \MongoDate
     */
    public function convertToDatabaseValue($value)
    {
        return new \MongoDate($value);
    }
}