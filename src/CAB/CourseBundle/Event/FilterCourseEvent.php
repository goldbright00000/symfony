<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:11
 */

namespace CAB\CourseBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use CAB\CourseBundle\Entity\Course;

/**
 * Allows to filter a Course object.
 *
 * You can call getCourse() to retrieve the current course.
 * With setCourse() you can set a new course that will be returned.
 *
 */
class FilterCourseEvent extends Event
{
    /**
     * @var Course
     */
    private $course;

    /**
     * @param Course $course
     */
    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    /**
     * Method description
     *
     * @return Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Method description
     *
     * @param Course $course
     *
     * @return Course
     */
    public function setCourse(Course $course)
    {
        $this->course = $course;

        return $this->course;
    }
}