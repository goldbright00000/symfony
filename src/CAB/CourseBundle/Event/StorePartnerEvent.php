<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:11
 */

namespace CAB\CourseBundle\Event;

use CAB\MainBundle\Entity\Partner;
use Symfony\Component\EventDispatcher\Event;

class StorePartnerEvent extends Event
{
    protected $partner;
    protected $rm;

    public function __construct(Partner $partner, $repositoryManager)
    {
        $this->partner = $partner;
        $this->rm = $repositoryManager;
    }

    public function getPartner()
    {
        return $this->$partner;
    }
}