<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 16/01/2017
 * Time: 11:24
 */

namespace CAB\CourseBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use CAB\UserBundle\Entity\User;

class VehicleAffectVoter extends Voter
{
    const VIEW = 'VIEW';
    const EDIT = 'EDIT';
    const DELETE = 'DELETE';
    const CREATE = 'CREATE';
    const LLIST = 'LIST';
    private $container;

    public function __construct (ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function getSupportedAttributes ()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::CREATE,
            self::LLIST,
        );
    }

    protected function getSupportedClasses ()
    {
        return 'CAB\CourseBundle\Entity\VehiculeAffect';
    }

    public function supportsClass ($class)
    {
        if (is_string($class)) {
            if ($class === $this->getSupportedClasses()) {
                return true;
            } else {
                return false;
            }
        }

        return $this->getSupportedClasses() === get_class($class);
    }

    protected function isGranted ($attribute, $object, $user = null)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof UserInterface) {
            return false;
        }

        // double-check that the User object is the expected entity (this
        // only happens when you did not configure the security system properly)
        if (!$user instanceof User) {
            throw new \LogicException('The user is somehow not our User class!');
        }

        if (!$this->supportsClass($object)) {
            return false;
        }

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            return true;
        }
        // if the user is admin company
        if ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
            if ($attribute === self::CREATE) {
                return true;
            } else {
                // list Company for this admin:
                $aCompanies = $user->getContacts()->getValues();
                foreach ($aCompanies as $company) {
                    if ($object->getCompany()) {
                        if ($object->getCompany()->getId() === $company->getId()) {
                            return true;
                        }
                    } //elseif
                }
            }
        } elseif ($authChecker->isGranted('ROLE_DRIVER')) {

            switch ($attribute) {
                case self::CREATE:
                    return true;
                    break;
                case self::VIEW:
                    if ($user->getDriverCompany()->getId() === $object->getCompany()->getId()) {
                        return true;
                    }
                    if ($user->getId() === $object->getId()) {
                        return true;
                    }
                    break;
                case self::EDIT:
                    if ($object->getDriver() === null) {
                        return false;
                    }
                    if ($user->getId() === $object->getDriver()->getId()) {
                        return true;
                    }

                    break;
            }
        }

        return false;
    }

    public function checkGranted ($attribute, $object, $user = null)
    {
        return $this->isGranted($attribute, $object, $user);
    }
	public function supports($attribute, $subject){}
	public function voteOnAttribute($attribute, $subject, TokenInterface $token){}
}