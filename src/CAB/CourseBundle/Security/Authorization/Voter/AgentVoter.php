<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 05/04/2016
 * Time: 13:14
 */

namespace CAB\CourseBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use CAB\UserBundle\Entity\User;

class AgentVoter extends Voter
{
    const VIEW      = 'VIEW';
    const EDIT      = 'EDIT';
    const DELETE    = 'DELETE';
    const CREATE    = 'CREATE';
    const LLIST     = 'LIST';
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::CREATE,
            self::LLIST,
        );
    }

    protected function getSupportedClasses()
    {
        return 'CAB\UserBundle\Entity\User';
	    //return 'CAB\ArticleBundle\Entity\Price';
    }

    public function supportsClass($class)
    {
        if (is_string($class)) {
            if ($class === $this->getSupportedClasses()) {
                return true;
            } else {
                return false;
            }
        } elseif ($class === false) {
            return false;
        }

        return $this->getSupportedClasses() === get_class($class);
    }

    protected function isGranted($attribute, $object, $user = null)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof UserInterface) {
            return false;
        }

        // double-check that the User object is the expected entity (this
        // only happens when you did not configure the security system properly)
        if (!$user instanceof User) {
            throw new \LogicException('The user is somehow not our User class!');
        }

        if ($attribute === self::LLIST) {
            $ret = true;

            if (!$authChecker->isGranted('ROLE_ADMINCOMPANY')
                && (
                   $authChecker->isGranted('ROLE_DRIVER')
                   || $authChecker->isGranted('ROLE_CUSTOMER'))) {
                $ret =  false;
            }

            return $ret;
        }

        if ($authChecker->isGranted('ROLE_SUPER_ADMIN')) {
            return true;
        }

        // if the user is agent company
        if ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
            if ($attribute === self::CREATE) {
                return true;
            } else {
            	if($object != null)
	            {
		            // list Company for this agent:
		            $company = $object->getAgentCompany();
		            // list Company for the current compnay admin (current user)
		            $companyAdmin = $user->getContacts()->getIterator();
		            $aCompany     = array();
		            while($companyAdmin->valid())
		            {
			            $aCompany[] = $companyAdmin->current()->getId();
			            $companyAdmin->next();
		            }

		            if($company && in_array($company->getId(), $aCompany))
		            {
			            return true;
		            }
		            elseif(!$company)
		            {
			            // Case agent dont affected to any company
			            return true;
		            }
	            }
	            elseif($attribute == self::VIEW)
	            {
	            	return true;
	            }
            }

        } elseif ($authChecker->isGranted('ROLE_AGENT')) { // if the user is agent
            if ($attribute === self::CREATE) {
                return false;
            } elseif (in_array($attribute, array(self::EDIT, self::VIEW))) {
                // list Company for this agent:
                if($attribute == self::VIEW)
                {
                	return true;
                }
	            elseif($object->getId() === $user->getId()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function checkGranted($attribute, $object, $user = null)
    {
        return $this->isGranted($attribute, $object, $user);
    }
	public function supports($attribute, $subject){}
	public function voteOnAttribute($attribute, $subject, TokenInterface $token){}

}