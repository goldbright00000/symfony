<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 05/04/2016
 * Time: 13:14
 */

namespace CAB\CourseBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use CAB\UserBundle\Entity\User;

class DriverVoter extends Voter
{
    const VIEW      = 'VIEW';
    const EDIT      = 'EDIT';
    const DELETE    = 'DELETE';
    const CREATE    = 'CREATE';
    const LLIST     = 'LIST';
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::CREATE,
            self::LLIST,
        );
    }

    protected function getSupportedClasses()
    {
        return 'CAB\UserBundle\Entity\User';
    }



    public function supportsClass($class)
    {

        if (is_string($class)) {
            if ($class === $this->getSupportedClasses()) {
                return true;
            } else {
                return false;
            }
        }

        return $this->getSupportedClasses() === get_class($class);
    }

    protected function isGranted($attribute, $object, $user = null)
    {
        $authChecker = $this->container->get('security.authorization_checker');


	    // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof UserInterface) {
            return false;
        }

        // double-check that the User object is the expected entity (this
        // only happens when you did not configure the security system properly)
        if (!$user instanceof User) {
            throw new \LogicException('The user is somehow not our User class!');
        }

        if ($object != null && !$this->supportsClass($object)) {
            return false;
        }

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            return true;
        }
        // if the user is admin company
        if ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
            if ($attribute === self::CREATE) {
                return true;
            } else
            {
	            // list Company for this admin:
	            $aCompanies = $user->getContacts()->getValues();
	            if($object != null)
	            {
		            if($object->getDriverCompany())
		            {
			            foreach($aCompanies as $company)
			            {

				            if($object->getDriverCompany()->getId() === $company->getId())
				            {
					            return true;
				            }

			            }
		            }
	            }
	            elseif($attribute === self::VIEW || $attribute === self::LLIST){
	            	return true;
            }
            }
        } elseif ($authChecker->isGranted('ROLE_AGENT')) { // if the user is agent
            if ($attribute === self::CREATE) {
                return true;
            } else {
                // list Company for this agent:
                $company = $user->getAgentCompany();
                if($object != null)
                {
	                if($object->getDriverCompany()->getId() === $company->getId())
	                {
		                return true;
	                }
                }
            }
        } elseif ($authChecker->isGranted('ROLE_DRIVER')) {
            switch ($attribute) {
                case self::VIEW:
                    if ($user->getDriverCompany()->getId() === $object->getDriverCompany()->getId()) {
                        return true;
                    }
                    if ($user->getId() === $object->getId()) {
                        return true;
                    }
                    break;
                case self::EDIT:
                    if ($user->getId() === $object->getId()) {
                        return true;
                    }

                    break;
            }
        }

        return false;
    }

    public function checkGranted($attribute, $object, $user = null)
    {
        return $this->isGranted($attribute, $object, $user);
    }
	public function supports($attribute, $subject){}
	public function voteOnAttribute($attribute, $subject, TokenInterface $token){}
}