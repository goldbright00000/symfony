<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 05/04/2016
 * Time: 13:14
 */

namespace CAB\CourseBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\Course;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\User\UserInterface;
use CAB\UserBundle\Entity\User;

class CourseVoter extends Voter
{
    const VIEW      = 'VIEW';
    const EDIT      = 'EDIT';
    const DELETE    = 'DELETE';
    const CREATE    = 'CREATE';
    const LLIST     = 'LIST';

    private $roleHierarchy;

    public function __construct(RoleHierarchy $roleHierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
    }

    public function checkGranted($attribute, $object, $token = null)
    {
        return $this->isGranted($attribute, $object, $token);
    }

    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::DELETE,
            self::CREATE,
            self::LLIST,
        );
    }

    protected function getSupportedClasses()
    {
        return 'CAB\CourseBundle\Entity\Course';
    }

    public function supportsClass($class)
    {
        if (is_string($class)) {
            if ($class === $this->getSupportedClasses()) {
                return true;
            } else {
                return false;
            }
        }

        return $this->getSupportedClasses() === get_class($class);
    }

    protected function isGranted($attribute, $object, $token = null)
    {
        $user = $token->getUser();

        // make sure there is a user object (i.e. that the user is logged in)
        if (!$user instanceof UserInterface) {
            return false;
        }

        // double-check that the User object is the expected entity (this
        // only happens when you did not configure the security system properly)
        if (!$user instanceof User) {
            throw new \LogicException('The user is somehow not our User class!');
        }


        if ($object != null && !$this->supportsClass($object)) {
            return false;
        }

        if ($this->hasRole($token, 'ROLE_ADMIN')) {
            return true;
        }

        // if the user is admin company
        if ($this->hasRole($token, 'ROLE_ADMINCOMPANY')) {
            if ($attribute === self::CREATE) {
                return true;
            } elseif($attribute === self::EDIT) {
                // list Company for this admin:
                $aCompanies = $user->getContacts()->getValues();
                foreach ($aCompanies as $company) {
                    if ($object->getCompany() instanceof Company) {
                        if ($object->getCompany()->getId() === $company->getId()) {
                            return true;
                        }
                    }
                }
            }
            else{
            	return true;
            }
        } elseif ($this->hasRole($token, 'ROLE_DRIVER')) {
            switch ($attribute) {
                case self::CREATE:
                    return true;
                    break;
                case self::VIEW:
                    if (in_array($object->getCourseStatus(),  array(Course::STATUS_CREATED, Course::STATUS_PAYED))) {
                        return true;
                    }

                    if ($user === $object->getDriver()
                        || $user === $object->getDriverBack()
                        || $user === $object->getCreatedBy()
                    ) {
                        return true;
                    }
                    break;
                case self::EDIT:
                    if ($user === $object->getDriver()
                        || $user === $object->getDriverBack()
                        || $user === $object->getCreatedBy()
                    ) {
                        return true;
                    }
                    break;
            }
        } elseif ($this->hasRole($token, 'ROLE_AGENT')) {
            switch ($attribute) {
                case self::CREATE:
                    return true;
                    break;
                case self::VIEW:
                    return true;
                    break;
                case self::EDIT:
                    return true;
                    break;
            }
        }

        return false;
    }

    protected function hasRole($token, $targetRole)
    {
        $reachableRoles = $this->roleHierarchy->getReachableRoles($token->getRoles());
        foreach ($reachableRoles as $role) {
            if ($role->getRole() == $targetRole) {
                return true;
            }
        }

        return false;
    }
	public function supports($attribute, $subject){}
	public function voteOnAttribute($attribute, $subject, TokenInterface $token){}
}