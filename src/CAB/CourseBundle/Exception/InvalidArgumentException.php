<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 12/01/2016
 * Time: 13:26
 */

namespace CAB\CourseBundle\Exception;


/**
 * Base InvalidArgumentException for the Form component.
 *
 * @author Mohamed Ben Henda <benhenda.medgmail.com>
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}