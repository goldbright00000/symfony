<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 12/01/2016
 * Time: 13:24
 */

namespace CAB\CourseBundle\Exception;


/**
 * Base ExceptionInterface for the Form component.
 *
 * @author Mohamed Ben Henda <benhenda.medgmail.com>
 */
interface ExceptionInterface
{
}