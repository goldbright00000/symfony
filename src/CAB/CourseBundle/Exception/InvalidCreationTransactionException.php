<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 12/01/2016
 * Time: 13:23
 */

namespace CAB\CourseBundle\Exception;


class InvalidCreationTransactionException extends InvalidArgumentException {

    public function __construct($message, $code)
    {
        parent::__construct($message, $code);
    }
}