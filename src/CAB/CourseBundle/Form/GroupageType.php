<?php

namespace CAB\CourseBundle\Form;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\CourseRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GroupageType
 *
 * @package CAB\AdminBundle\Form
 */
class GroupageType extends AbstractType
{
    /**
     * Build form Groupage type
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Course $course */
        $course = $options['course'];
        $courseId = $course->getId();
        $builder
            ->add(
                'baseCourse',
                'entity',
                array(
                    'class' => 'CABCourseBundle:Course',
                    'choice_label' => 'displayName',
                    'query_builder' => function (CourseRepository $cr) use ($courseId) {

                        return $cr->createQueryBuilder('c')->where("c.id = $courseId")->setCacheable(true)->setCacheRegion('cache_long_time');;
                    },
                    'required' => true,
                )
            )
            ->add(
                'linkedCourses',
                null,
                array(
                    'required' => false,
                    'query_builder' => function (CourseRepository $cr) use ($course) {
                        $dateCourse = $course->getDepartureDate()->format('Y-m-d') . ' ' . $course->getDepartureTime()->format('H:i');
                        $oDateCourse = \DateTime::createFromFormat('Y-m-d H:i', $dateCourse);
                        return $cr->getCoursesForGroupage($course->getLongDep(), $course->getLongArr(), $oDateCourse, $course->getCompany()->getId());
                    },
                )
            )
            ->add('save', 'submit', array(
                    'attr' => array('class' => 'save'),
                )
            );
    }

    /**
     * It allows to create an options system
     * @param OptionsResolver $resolver OptionsResolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Groupage',
                'course' => null,
            )
        );
    }


    /**
     * Define the name of the class type
     * @return string
     */
    public function getName()
    {
        return 'cab_coursebundle_groupage';
    }

	public function getBlockPrefix() {

		return "cab_coursebundle_groupage";
	}
}
