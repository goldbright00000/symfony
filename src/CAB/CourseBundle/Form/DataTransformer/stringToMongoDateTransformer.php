<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 19/01/2016
 * Time: 15:34
 */

namespace CAB\CourseBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class StringToMongoDateTransformer implements DataTransformerInterface {

    /**
     * Transforms a string date to a DateTime date.
     *
     * @param  string|null MongoDB date
     * @return DateTime date
     */
    public function transform($strDate)
    {
        if (null === $strDate) {
            return new \DateTime();
        }

        return new \DateTime($strDate);
    }

    /**
     * Transforms a DateTime date to a string date.
     *
     * @param  DateTime $dateMongo
     * @return string|null
     */
    public function reverseTransform($dateObj)
    {
        if (!$dateObj) {
            return;
        }

        $stringDate = $dateObj->format('Y-m-d H:i:s');

        return $stringDate;
    }
}