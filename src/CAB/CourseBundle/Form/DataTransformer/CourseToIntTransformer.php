<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 12/04/2016
 * Time: 23:00
 */
namespace CAB\CourseBundle\Form\DataTransformer;

use CAB\CourseBundle\Entity\Course;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CourseToIntTransformer implements DataTransformerInterface
{
    private $manager;
    private $obj;

    public function __construct(ObjectManager $manager, $objectCourse = null)
    {
        $this->manager = $manager;
        $this->obj = $objectCourse;
    }

    /**
     * Transforms an object (course) to a integer (number).
     *
     * @param  Course|null $course
     * @return string
     */
    public function transform($course)
    {
        if (null === $course) {

            //find the company object
            return $this->obj->getId();
        }

        return $course->getId();
    }

    /**
     * Transforms a integer (number) to an object (Course).
     *
     * @param  integer $courseId
     * @return Course|null
     * @throws TransformationFailedException if object (Course) is not found.
     */
    public function reverseTransform($courseId)
    {
        if (!$courseId) {
            return;
        }

        $oCourse = $this->manager
            ->getRepository('CABCourseBundle:Company')
            // query for the issue with this id
            ->find($courseId);

        if (null === $oCourse) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A course with number "%s" does not exist!',
                $courseId
            ));
        }

        return $oCourse;
    }
}