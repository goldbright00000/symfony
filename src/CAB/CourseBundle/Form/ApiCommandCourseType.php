<?php

namespace CAB\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ApiCommandCourseType
 *
 * @package CAB\CourseBundle\Form
 */
class ApiCommandCourseType extends AbstractType
{
    /**
     * Build form Course type for API
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'client',
                'entity',
                array(
                    'class' => 'CABUserBundle:User',
                    'required' => true,
                )
            )
            ->add(
                'driver',
                'entity',
                array(
                    'class' => 'CABUserBundle:User',
                    'required' => true,
                )
            )
            ->add(
                'company',
                'entity',
                array(
                    'class' => 'CABCourseBundle:Company',
                    'required' => true,
                )
            )
            ->add(
                'personNumber',
                'integer',
                array(
                    'required' => false,
                )
            )
            ->add(
                'departureDate',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'startedAddress',
                'text',
                array(
                    'required' => true,
                )
            )
            ->add(
                'arrivalAddress',
                'text',
                array(
                    'required' => true,
                )
            )
            ->add(
                'longDep',
                'hidden',
                array(
                    'required' => true,
                )
            )
            ->add(
                'latDep',
                'hidden',
                array(
                    'required' => true,
                )
            )
            ->add(
                'longArr',
                'hidden',
                array(
                    'required' => true,
                )
            )
            ->add(
                'latArr',
                'hidden',
                array(
                    'required' => true,
                )
            );
    }

    /**
     * It allows to create an options system
     *
     * @param OptionsResolver $resolver OptionsResolver
     *
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Course',
                'csrf_protection' => false,
            )
        );
    }

    /**
     * Define the name of the class type
     *
     * @return string
     */
    public function getName()
    {
        return 'cab_api_coursebundle_course';
    }

	public function getBlockPrefix() {

		return "cab_api_coursebundle_course";
	}
}
