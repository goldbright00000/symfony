<?php

namespace CAB\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ApiCourseType
 *
 * @package CAB\CourseBundle\Form
 */
class ApiCourseType extends AbstractType
{
    /**
     * Build form Course type for API
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'client',
                'entity',
                array(
                    'class' => 'CABUserBundle:User',
                    'required' => true,
                )
            )
            ->add(
                'driver',
                'entity',
                array(
                    'class' => 'CABUserBundle:User',
                    'required' => true,
                )
            )
            ->add(
                'startedAddress',
                'text',
                array(
                    'required' => true,
                )
            )
            ->add(
                'arrivalAddress',
                'text',
                array(
                    'required' => true,
                )
            )
            ->add(
                'longDep',
                'hidden',
                array(
                    'required' => true,
                )
            )
            ->add(
                'latDep',
                'hidden',
                array(
                    'required' => true,
                )
            )
            ->add(
                'longArr',
                'hidden',
                array(
                    'required' => true,
                )
            )
            ->add(
                'latArr',
                'hidden',
                array(
                    'required' => true,
                )
            );
    }

    /**
     * It allows to create an options system
     *
     * @param OptionsResolver $resolver OptionsResolver
     *
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Course',
                'csrf_protection' => false,
            )
        );
    }

    /**
     * Define the name of the class type
     *
     * @return string
     */
    public function getName()
    {
        return 'cab_api_coursebundle_course';
    }

	public function getBlockPrefix() {

		return "cab_api_coursebundle_course";
	}
}
