<?php

namespace CAB\CourseBundle\Form;

use CAB\CourseBundle\Form\DataTransformer\StringToMongoDateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GeolocationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('accuracy')
            ->add('altitude')
            ->add('latitude')
            ->add('longitude')
            ->add('heading')
            ->add('speed')
            ->add('driver',null, array('required'=> true))
            ->add('dateGeolocate')
            ->add('availibility', 'text')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CAB\CourseBundle\Document\Geolocation',
            'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cab_coursebundle_geolocation';
    }

	public function getBlockPrefix() {

		return "cab_coursebundle_geolocation";
	}
}
