<?php

namespace CAB\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Class CourseType
 *
 * @package CAB\CourseBundle\Form
 */
class CourseType extends AbstractType
{
    /**
     * Build form Course type
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'typeVehicleCourse',
                'entity',
                array(
                    'class' => 'CABCourseBundle:TypeVehicule',
                    'choice_label' => 'nameType',
                    'required' => false,
                )
            )
            ->add(
                'vehicle',
                'entity',
                array(
                    'class' => 'CABCourseBundle:Vehicule',
                    'choice_label' => 'vehiculeName',
                    'required' => false,
                )
            )
            ->add(
                'startedAddress',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'indicatorStartedAddress',
                'text',
                array(
                    'attr' => array(
                        'class' => 'field-date',
                        'required' => false,
                    )
                )
            )
            ->add(
                'arrivalAddress',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'indicatorArrivalAddress',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'departureDate',
                'datePicker',
                array(
                    'label' => 'Departure date',
                    'translation_domain' => 'frontend',
                    'widget' => 'single_text',
                    'attr' => array('class' => 'field-date')
                )
            )
            ->add(
                'departureTime',
                null,
                array(
                    'widget' => 'single_text',
                    'label' => 'Departure time',
                    'translation_domain' => 'frontend'
                )
            )
            ->add(
                'targetType',
                'checkbox', array(
                    'label' => 'Return trip',
                    'translation_domain' => 'frontend',
                    'required' => false,
                )
            )
            ->add('personNumber', 'integer')
            ->add('nbChildren', 'integer')
            ->add(
                'arrivalHour',
                null,
                array(
                    'widget' => 'single_text',
                    'required' => false,
                )
            )
            ->add(
                'forceHourArrived',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'laguage',
                'integer',
                array(
                    'required' => false,
                )
            )
            ->add(
                'nbWheelchair',
                'integer',
                array(
                    'required' => false,
                )
            )
            ->add(
                'isHandicap',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'isGroup',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'isCycle',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'mondayCycle',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'thursdayCycle',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'wednesdayCycle',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'tuesdayCycle',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'fridayCycle',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add('saturdayCycle', 'checkbox', array('required' => false))
            ->add('sundayCycle', 'checkbox', array('required' => false))
            ->add('cycleEndDate', null, array('widget' => 'single_text', 'attr' => array('class' => 'field-date')))
            ->add(
                'longDep',
                'hidden',
                array(
                    'required' => false,
                )
            )
            ->add(
                'latDep',
                'hidden',
                array(
                    'required' => false,
                )
            )
            ->add(
                'longArr',
                'hidden',
                array(
                    'required' => false,
                )
            )
            ->add(
                'latArr',
                'hidden',
                array(
                    'required' => false,
                )
            )
            ->add(
                'backDate',
                null,
                array(
                    'label' => 'Departure date return',
                    'translation_domain' => 'frontend',
                    'widget' => 'single_text',
                    'required' => false,
                    'attr' => array(
                        'class' => 'field-date'
                    )
                )
            )
            ->add(
                'backTime',
                null,
                array(
                    'label' => 'Departure time return',
                    'translation_domain' => 'frontend',
                    'widget' => 'single_text',
                    'required' => false,
                )
            )
            ->add(
                'forceHourArrivedBack',
                'checkbox',
                array(
                    'required' => false,
                )
            )
            ->add(
                'arrivalHourBack',
                null,
                array(
                    'widget' => 'single_text',
                )
            )
            ->add(
                'backAddressDeparture',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'backAddressArrival',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'indicatorBackAddressDeparture',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'indicatorBackAddressArrival',
                'text',
                array(
                    'required' => false,
                )
            )
            ->add(
                'personNumberBack',
                'integer',
                array(
                    'required' => false,
                )
            )
            ->add(
                'nbChildrenBack',
                'integer',
                array(
                    'required' => false,
                )
            )
            ->add(
                'laguageBack',
                'integer',
                array(
                    'required' => false,
                )
            )
            ->add(
                'nbWheelchairBack',
                'integer',
                array(
                    'required' => false,
                )
            )
            ->add(
                'descriptionChildrenBack',
                'textarea',
                array(
                    'required' => false,
                )
            )
            ->add(
                'longDepBack',
                'hidden',
                array(
                    'required' => false,
                )
            )
            ->add(
                'latDepBack',
                'hidden',
                array(
                    'required' => false,
                )
            )
            ->add(
                'longArrBack',
                'hidden',
                array(
                    'required' => false,
                )
            )
            ->add(
                'latArrBack',
                'hidden',
                array(
                    'required' => false,
                )
            )
        ;
        /* $builder->get('typeVehicleCourse')->addModelTransformer(new UserToIntTransformer($em, $currentUser));
                     $this->addDriverToForm($formMapper, 'driverBack');*/
    }

    /**
     * It allows to create an options system
     * @param OptionsResolver $resolver OptionsResolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Course',
            )
        );
    }

    /**
     * Define the name of the class type
     * @return string
     */
    public function getName()
    {
        return 'cab_coursebundle_course';
    }

	public function getBlockPrefix() {

		return "cab_coursebundle_course";
	}
}
