<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 09/01/2016
 * Time: 09:14
 */

namespace CAB\CourseBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

class FormErrors {

    private $container;

    /**
     * @param \Symfony\Component\Form\FormInterface $form
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @return array
     */
    public function getFormErrors(FormInterface $form)
    {
        $errors = array();
        if ($err = $this->childErrors($form)) {
            $errors["form"] = $err;
        }
        //
        foreach ($form->all() as $key => $child) {
            if ($err = $this->childErrors($child)) {
                $errors[$key] = $err;
            }
        }

        return $errors;
    }
    /**
     * @param \Symfony\Component\Form\FormInterface $form
     * @return array
     */
    public function childErrors(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $message = $this->container->get('translator')->trans($error->getMessage(), array(), 'validators');
            array_push($errors, $message);
        }

        return $errors;
    }
}