<?php

namespace CAB\CourseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

class GeolocationCustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('accuracy')
            ->add('altitude')
            ->add('heading')
            ->add('latitude')
            ->add('longitude')
            ->add('customer')
            ->add('dateGeolocate');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CAB\CourseBundle\Document\GeolocationCustomer',
            'csrf_protection' => false,

        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cab_coursebundle_geolocation_customer';
    }

	public function getBlockPrefix() {

		return "cab_coursebundle_geolocation_customer";
	}
}
