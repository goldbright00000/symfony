<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 21:25
 */

namespace CAB\CourseBundle\Payment;


class OrderRequest {
    public $orderId; // string
    public $extInfo; // extInfo
}