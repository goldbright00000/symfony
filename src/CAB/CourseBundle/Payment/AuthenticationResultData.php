<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:08
 */

namespace CAB\CourseBundle\Payment;


class AuthenticationResultData {
    public $brand; // string
    public $enrolled; // string
    public $status; // string
    public $eci; // string
    public $xid; // string
    public $cavv; // string
    public $cavvAlgorithm; // string
    public $signValid; // string
    public $transactionCondition; // string
}