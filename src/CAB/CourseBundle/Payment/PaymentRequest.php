<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:02
 */

namespace CAB\CourseBundle\Payment;


class PaymentRequest {
    public $transactionId; // string
    public $amount; // long
    public $currency; // int
    public $expectedCaptureDate; // dateTime
    public $manualValidation; // int
    public $paymentOptionCode; // string
}