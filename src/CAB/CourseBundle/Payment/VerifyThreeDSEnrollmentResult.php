<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:13
 */

namespace CAB\CourseBundle\Payment;


class VerifyThreeDSEnrollmentResult {
    public $commonResponse; // commonResponse
    public $threeDSResponse; // threeDSResponse
}