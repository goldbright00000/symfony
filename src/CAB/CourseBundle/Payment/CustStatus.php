<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:15
 */

namespace CAB\CourseBundle\Payment;


class CustStatus {
    const _PRIVATE = 'PRIVATE';
    const COMPANY = 'COMPANY';
}