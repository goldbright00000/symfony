<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 16:55
 */

namespace CAB\CourseBundle\Payment;


class CommonResponse {
    public $responseCode; // int
    public $responseCodeDetail; // string
    public $transactionStatusLabel; // string
    public $shopId; // string
    public $paymentSource; // string
    public $submissionDate; // dateTime
    public $contractNumber; // string
    public $paymentToken; // string
}