<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:09
 */

namespace CAB\CourseBundle\Payment;


class ThreeDSRequest {
    public $mode; // threeDSMode
    public $requestId; // string
    public $pares; // string
    public $brand; // string
    public $enrolled; // string
    public $status; // string
    public $eci; // string
    public $xid; // string
    public $cavv; // string
    public $algorithm; // string
}