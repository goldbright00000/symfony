<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:05
 */

namespace CAB\CourseBundle\Payment;


class customerResponse {
    public $billingDetails; // billingDetailsResponse
    public $shippingDetails; // shippingDetailsResponse
    public $extraDetails; // extraDetailsResponse
}