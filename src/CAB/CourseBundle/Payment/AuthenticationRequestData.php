<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:07
 */

namespace CAB\CourseBundle\Payment;


class AuthenticationRequestData {
    public $threeDSAcctId; // string
    public $threeDSAcsUrl; // string
    public $threeDSBrand; // string
    public $threeDSEncodedPareq; // string
    public $threeDSEnrolled; // string
    public $threeDSRequestId; // string
}