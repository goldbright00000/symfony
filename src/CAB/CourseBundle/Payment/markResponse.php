<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:06
 */

namespace CAB\CourseBundle\Payment;


class markResponse {
    public $amount; // long
    public $currency; // int
    public $date; // dateTime
    public $number; // string
    public $result; // int
}