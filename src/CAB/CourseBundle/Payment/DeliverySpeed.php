<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:16
 */

namespace CAB\CourseBundle\Payment;


class DeliverySpeed {
    const STANDARD = 'STANDARD';
    const EXPRESS = 'EXPRESS';
}