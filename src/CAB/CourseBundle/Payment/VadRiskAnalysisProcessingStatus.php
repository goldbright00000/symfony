<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:17
 */

namespace CAB\CourseBundle\Payment;


class VadRiskAnalysisProcessingStatus {
    const P_TO_SEND = 'P_TO_SEND';
    const P_SEND_KO = 'P_SEND_KO';
    const P_PENDING_AT_ANALYZER = 'P_PENDING_AT_ANALYZER';
    const P_SEND_OK = 'P_SEND_OK';
    const P_MANUAL = 'P_MANUAL';
    const P_SKIPPED = 'P_SKIPPED';
    const P_SEND_EXPIRED = 'P_SEND_EXPIRED';
}