<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:04
 */

namespace CAB\CourseBundle\Payment;


class captureResponse {
    public $date; // dateTime
    public $number; // int
    public $reconciliationStatus; // int
    public $refundAmount; // long
    public $refundCurrency; // int
    public $chargeback; // boolean
}