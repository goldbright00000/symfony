<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:17
 */

namespace CAB\CourseBundle\Payment;


class ThreeDSMode {
    const DISABLED = 'DISABLED';
    const ENABLED_CREATE = 'ENABLED_CREATE';
    const ENABLED_FINALIZE = 'ENABLED_FINALIZE';
    const MERCHANT_3DS = 'MERCHANT_3DS';
}