<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:10
 */

namespace CAB\CourseBundle\Payment;


class QueryRequest {
    public $uuid; // string
    public $orderId; // string
    public $subscriptionId; // string
    public $paymentToken; // string
}