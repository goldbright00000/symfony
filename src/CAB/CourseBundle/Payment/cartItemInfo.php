<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:02
 */

namespace CAB\CourseBundle\Payment;


class cartItemInfo {
    public $productLabel; // string
    public $productType; // productType
    public $productRef; // string
    public $productQty ; // int
    public $productAmount; // string
    public $productVat; // string
}