<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:01
 */

namespace CAB\CourseBundle\Payment;


class extraDetailsRequest {
    public $ipAddress; // string
    public $fingerPrintId; // string
}