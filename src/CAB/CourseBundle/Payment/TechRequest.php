<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 23:03
 */

namespace CAB\CourseBundle\Payment;


class TechRequest {
    public $browserUserAgent; // string
    public $browserAccept; // string
}