<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:10
 */

namespace CAB\CourseBundle\Payment;


class CreatePaymentResult {
    public $commonResponse; // commonResponse
    public $paymentResponse; // paymentResponse
    public $orderResponse; // orderResponse
    public $cardResponse; // cardResponse
    public $authorizationResponse; // authorizationResponse
    public $captureResponse; // captureResponse
    public $customerResponse; // customerResponse
    public $markResponse; // markResponse
    public $threeDSResponse; // threeDSResponse
    public $extraResponse; // extraResponse
    public $subscriptionResponse; // subscriptionResponse
    public $fraudManagementResponse; // fraudManagementResponse
    public $shoppingCartResponse; // shoppingCartResponse
}