<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 16:55
 */

namespace CAB\CourseBundle\Payment;


class CustomerRequest {
    public $billingDetails; // billingDetailsRequest
    public $shippingDetails; // shippingDetailsRequest
    public $extraDetails; // extraDetailsRequest
}