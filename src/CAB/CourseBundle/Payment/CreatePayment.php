<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:09
 */

namespace CAB\CourseBundle\Payment;


class CreatePayment {
    public $commonRequest; // commonRequest
    public $threeDSRequest; // threeDSRequest
    public $paymentRequest; // paymentRequest
    public $orderRequest; // orderRequest
    public $cardRequest; // cardRequest
    public $customerRequest; // customerRequest
    public $techRequest; // techRequest
    public $shoppingCartRequest; // shoppingCartRequest
}