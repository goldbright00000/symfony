<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:07
 */

namespace CAB\CourseBundle\Payment;


class ThreeDSResponse {
    public $authenticationRequestData; // authenticationRequestData
    public $authenticationResultData; // authenticationResultData
}