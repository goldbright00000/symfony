<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:15
 */

namespace CAB\CourseBundle\Payment;


class PaymentType {
    const SINGLE = 'SINGLE';
    const INSTALLMENT = 'INSTALLMENT';
    const SPLIT = 'SPLIT';
    const SUBSCRIPTION = 'SUBSCRIPTION';
    const RETRY = 'RETRY';
}