<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 16:56
 */

namespace CAB\CourseBundle\Payment;


class BillingDetailsRequest {
    public $reference; // string
    public $title; // string
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $email; // string
    public $streetNumber; // string
    public $address; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $language; // string
    public $cellPhoneNumber; // string
    public $legalName; // string
    public $identityCode; // string
}