<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:12
 */

namespace CAB\CourseBundle\Payment;


class TransactionItem {
    public $transactionUuid; // string
    public $transactionStatusLabel; // string
    public $amount; // long
    public $currency; // int
    public $expectedCaptureDate; // dateTime
}