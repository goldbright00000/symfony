<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 16:55
 */

namespace CAB\CourseBundle\Payment;


class CardRequest {
    public $number; // string
    public $scheme; // string
    public $expiryMonth; // int
    public $expiryYear; // int
    public $cardSecurityCode; // string
    public $cardHolderBirthDay; // dateTime
    public $paymentToken; // string
}