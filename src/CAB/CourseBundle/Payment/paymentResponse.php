<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:02
 */

namespace CAB\CourseBundle\Payment;


class paymentResponse {
    public $transactionId; // string
    public $amount; // long
    public $currency; // int
    public $effectiveAmount; // long
    public $effectiveCurrency; // int
    public $expectedCaptureDate; // dateTime
    public $manualValidation; // int
    public $operationType; // int
    public $creationDate; // dateTime
    public $externalTransactionId; // string
    public $liabilityShift; // string
    public $sequenceNumber; // int
    public $paymentType; // paymentType
    public $paymentError; // int
}