<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 16:56
 */

namespace CAB\CourseBundle\Payment;


class shippingDetailsRequest {
    public $type; // custStatus
    public $firstName; // string
    public $lastName; // string
    public $phoneNumber; // string
    public $streetNumber; // string
    public $address; // string
    public $address2; // string
    public $district; // string
    public $zipCode; // string
    public $city; // string
    public $state; // string
    public $country; // string
    public $deliveryCompanyName; // string
    public $shippingSpeed; // deliverySpeed
    public $shippingMethod; // deliveryType
    public $legalName; // string
    public $identityCode; // string
}