<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 16:54
 */

namespace CAB\CourseBundle\Payment;


class CommonRequest {
    public $paymentSource; // string
    public $submissionDate; // dateTime
    public $contractNumber; // string
    public $comment; // string
}