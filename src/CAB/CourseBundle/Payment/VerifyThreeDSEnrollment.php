<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:12
 */

namespace CAB\CourseBundle\Payment;


class VerifyThreeDSEnrollment {
    public $commonRequest; // commonRequest
    public $paymentRequest; // paymentRequest
    public $cardRequest; // cardRequest
    public $techRequest; // techRequest
}