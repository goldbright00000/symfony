<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 31/12/2015
 * Time: 17:16
 */

namespace CAB\CourseBundle\Payment;


class DeliveryType {
    const RECLAIM_IN_SHOP = 'RECLAIM_IN_SHOP';
    const RELAY_POINT = 'RELAY_POINT';
    const RECLAIM_IN_STATION = 'RECLAIM_IN_STATION';
    const PACKAGE_DELIVERY_COMPANY = 'PACKAGE_DELIVERY_COMPANY';
    const ETICKET = 'ETICKET';
}