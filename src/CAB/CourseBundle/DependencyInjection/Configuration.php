<?php

namespace CAB\CourseBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cab_course');
        $rootNode
            ->children()
                ->arrayNode('systempay')
                    ->children()
                        ->scalarNode('vads_site_id')->end()
                        ->scalarNode('certificat')->end()
                    ->end()
                ->end()
            ->end()
            ->children()
                ->arrayNode('google')
                    ->children()
                        ->scalarNode('map_api_key')->end()
                        ->scalarNode('server_key')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
