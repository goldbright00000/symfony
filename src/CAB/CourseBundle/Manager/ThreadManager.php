<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 15/12/2015
 * Time: 17:42
 */

namespace CAB\CourseBundle\Manager;


use CAB\MessageBundle\Entity\Thread;
use Doctrine\ORM\EntityManager;

class ThreadManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager  $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param integer $objectID
     *
     * @return Thread object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()
            ->findOneBy(array('id' => $objectID));
    }

    /**
     * @param Thread $thread
     * @param string $attr
     * @param string $value
     */
    public function setAttr(Thread $thread, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $thread->$attributeSetter($value);
        $this->save($thread);
    }
    /**
     * Save Thread entity
     *
     * @param array $params
     */
    public function save(Thread $thread)
    {
        if (!$thread) {
            $thread = new Thread();
        }
        try {
            $this->persistAndFlush($thread);

            return array(
                'status' => true,
                'response' => '',
                );
        } catch (\Exception $e) {
            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * @return \CAB\MessageBundle\Entity\ThreadRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABMessageBundle:Thread');
    }
}