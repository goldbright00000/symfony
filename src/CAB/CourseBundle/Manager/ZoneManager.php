<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;


use CAB\CourseBundle\Entity\Vehicule;
use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Manager\BaseManager;
use CAB\CourseBundle\Entity\Zone;
use Symfony\Component\HttpFoundation\Session\Session;

class ZoneManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param integer $zoneId
     *
     * @return Zone
     */
    public function loadZone($zoneId)
    {

        return $this->getRepository()
            ->findOneBy(array('id' => $zoneId));
    }

    /**
     * @param array $options
     *
     * @return array
     */
    public function getBy(array $options)
    {
        $result =  $this->getRepository()->findBy($options);
        $return = array();
        foreach ($result as $item) {
            $return[] = array('val' => $item->getId(), 'text' => $item->getZoneName());
        }

        return $return;
    }

    /**
     * @param string $long
     * @param string $lat
     * @param string $nbPserson
     * @param string $distance
     *
     * @return array
     */
    public function getDataByLangLat($long, $lat, $nbPserson, $distance)
    {
        $result =  $this->getRepository()->findZoneByLatLng($long, $lat, $nbPserson, $distance);
        $return = array();
        foreach ($result as $item) {
            $return[] = array('type_vehicle' => $item['nameType'], 'price' => '');
        }

        return $return;
    }

    /**
     * @param Zone       $zone
     * @param string     $attr
     * @param string|int $value
     */
    public function setAttr(Zone $zone, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $zone->$attributeSetter($value);
        $this->saveZone($zone);
    }

    /**
     * Find users by location
     *
     * @param int $lat
     * @param int $lng
     *
     * @return mixed
     */
    public function getUsersByLocation($lat = 0, $lng = 0)
    {
        return $this->getRepository()->findUserByZone($lat = 0, $lng = 0);
    }
    /**
     * Save Zone entity
     *
     * @param Zone $zone
     */
    public function saveZone(Zone $zone)
    {
        $this->persistAndFlush($zone);
    }

    /**
     * @return \CAB\CourseBundle\Entity\ZoneRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Zone');
    }

}