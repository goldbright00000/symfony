<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 23/02/2017
 * Time: 16:26
 */

namespace CAB\CourseBundle\Manager;


use CAB\CourseBundle\Entity\Groupage;
use Doctrine\ORM\EntityManager;

class GroupageManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $id
     *
     * @return Groupage
     */
    public function loadGroupage($id)
    {
        $oGroupage = $this->getRepository()->find($id);

        return $oGroupage;
    }

    /**
     * @param Groupage $groupage
     * @param string $attr
     * @param int|string $value
     */
    public function setAttr(Groupage $groupage, $attr, $value)
    {
        $attributeSetter = 'set' . ucfirst($attr);
        $groupage->$attributeSetter($value);
        $this->save($groupage);
    }

    /**
     * Save Groupage entity
     *
     * @param Groupage $groupage
     */
    public function save(Groupage $groupage)
    {
        $this->persistAndFlush($groupage);
    }

    /**
     * @return \CAB\CourseBundle\Entity\GroupageRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Groupage');
    }

    /**
     * @param $entity
     */
    public function removeAndFlush($entity)
    {
        $this->removeAndFlush($entity);
    }

    /**
     * @param $courseId
     * @return null|object
     */
    public function findByBaseCourse($courseId)
    {
        return $this->getRepository()->findOneBy(array('baseCourse' => $courseId));
    }


}