<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:44
 */
namespace CAB\CourseBundle\Manager;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Events\HistoryCourseEvents;
use CAB\CourseBundle\Events\UpdateStatusCourseEvents;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class BaseManager
 *
 * @package CAB\CourseBundle\Manager
 */
abstract class BaseManager
{
	/**
	 * Persist and flush entity
	 *
	 * @param $entity
	 * @param bool $dispatchEvent
	 */
    protected function persistAndFlush($entity, $dispatchEvent = true)
    {
        if (!$this->em->isOpen()) {
            $this->entityManager = $this->em->create(
                $this->em->getConnection(),
                $this->em->getConfiguration()
            );
        }

        $this->em->persist($entity);
        $this->em->flush();

        //dispatch an event course saved if the entity stored is a course
        if ($entity instanceof Course && $dispatchEvent) {
            $event = new GenericEvent($entity, array('course_history' => 1, 'status_course' => $entity->getCourseStatus()));
            $this->dispatcher->dispatch(HistoryCourseEvents::HISTORY_COURSE, $event);

            $this->dispatcher->dispatch(UpdateStatusCourseEvents::UPDATE_STATUS_COURSE, $event);
        }
    }

    protected function removeAndFlush($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * merge And Flush entity
     *
     * @param $entity
     */
    protected function mergeAndFlush($entity)
    {
        $this->em->merge($entity);
        $this->em->flush();
    }

    /**
     * Return all properties of class
     *
     * @param $class
     *
     * @return array
     */
    protected function getClassProperties($class, $getType = false)
    {
        $oClass = new $class();
        $res = array();
        $reflect = new \ReflectionClass($oClass);
        $props   = $reflect->getProperties();

        if ($getType) {
            foreach ($props as $prop) {
                $docReader = new AnnotationReader();
                $docInfos = $docReader->getPropertyAnnotations($reflect->getProperty($prop->getName()));
                $type = "";
                if (isset($docInfos[0]) && $docInfos[0] instanceof \Doctrine\ORM\Mapping\Column) {
                    $type = $docInfos[0]->type;
                }

                $aPropertyName[] = $prop->getName();
                $aPropertyType[$prop->getName()] = $type;
            }

            return array($aPropertyName, $aPropertyType);
        } else {
            foreach ($props as $prop) {
                $res[] = $prop->getName();
            }
        }

        return $res;
    }
}