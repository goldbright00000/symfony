<?php

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;

/**
 * CountryManager
 *
 * Add your own custom manager methods below.
 */
class CountryManager extends BaseManager
{
    protected $entityClass;

    /**
     * @param EntityManager $em
     * @param string        $entityClass
     */
    public function __construct(EntityManager $em, $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
    }

    /**
     * @param int $countryId
     *
     * @return CountryManager
     */
    public function loadCountryManager($countryId)
    {
        return $this->getRepository()->find($countryId);
    }

    /**
     * @param string $name
     *
     * @return CountryManager
     */
    public function getCountryByName($name)
    {
        return $this->getRepository()->findBy(array('name', $name));
    }

    /**
     * @param CountryManager $country
     * @param string     $attr
     * @param int|string $value
     */
    public function setAttr(CountryManager $country, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $country->$attributeSetter($value);
        $this->saveCountryManager($country);
    }

    /**
     * Save CountryManager entity
     *
     * @param CountryManager $country
     */
    public function saveCountryManager(CountryManager $country)
    {
        $this->persistAndFlush($country);
    }

    /**
     * @return //CAB\CourseBundle\Entity\CountryRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository($this->entityClass);
    }

}
