<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\Order;

/**
 * Class OrderManager
 *
 * @package CAB\CourseBundle\Manager
 */
class OrderManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $orderId
     *
     * @return Order
     */
    public function loadOrder($orderId)
    {
        return $this->getRepository()->find($orderId);
    }

    /**
     * @return Order
     */
    public function newOrder()
    {
        return new Order();
    }

    /**
     * @param Order  $order
     * @param string $attr
     * @param string $value
     */
    public function setAttr(Order $order, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $order->$attributeSetter($value);
        $this->saveOrder($order);
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return array
     */
    public function getOrderBy($criteria, $creteriaValue)
    {
        $result = null;
        //$aKeys = $this->getAttributeClass();

        $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));

        return $result;
    }

    /**
     * Save Order entity
     *
     * @param Order $order
     */
    public function saveOrder($order)
    {
        $this->persistAndFlush($order);
    }

    /**
     * @return \CAB\CourseBundle\Entity\OrderRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Order');
    }
}