<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\TarifFrenchTaxi;

class TarifFrenchTaxiManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $dep
     *
     * @return \CAB\CourseBundle\Entity\TarifFrenchTaxi
     */
    public function loadTarifsByDepartement($dep)
    {
        $result =  $this->getRepository()
            ->findBy(array('departement' => $dep));
        $return = array();
        foreach ($result as $item) {
            $return[] = array(
                'val' => $item->getDepartement(),
                'text' => array(
                    'name' => $item->getName(),
                    'tarif_pc' => $item->getTarifPc(),
                    'tarif_a' => $item->getTarifA(),
                    'tarif_b' => $item->getTarifB(),
                    'tarif_c' => $item->getTarifC(),
                    'tarif_d' => $item->getTarifD(),
                    'slowWalkPerHour' => $item->getSlowWalkPerHour(),
                    'luggage' => $item->getLuggage(),
                    'person' => $item->getPerson(),
                    'minPrice' => $item->getMinPrice(),
                    'packageApproach' => $item->getPackageApproach(),
                ),
            );
        }

        return $return;
    }

    /**
     * @param array $options
     *
     * @return array
     */
    public function getBy(array $options)
    {
        $result =  $this->getRepository()->findBy($options);
        $return = array();
        foreach ($result as $item) {
            $return[] = array('val' => $item->getDepartement(), 'text' => $item->getName());
        }

        return $return;
    }

    /**
     * @param TarifFrenchTaxi $tarifFrenchTaxi
     * @param string          $attr
     * @param string|int      $value
     */
    public function setAttr(TarifFrenchTaxi $tarifFrenchTaxi, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $tarifFrenchTaxi->$attributeSetter($value);
        $this->saveTarifFrenchTaxi($tarifFrenchTaxi);
    }

    /**
     * Save TarifFrenchTaxi entity
     *
     * @param TarifFrenchTaxi $tarifFrenchTaxi
     */
    public function saveTarifFrenchTaxi(TarifFrenchTaxi $tarifFrenchTaxi)
    {
        $this->persistAndFlush($tarifFrenchTaxi);
    }

    /**
     * @return \CAB\CourseBundle\Entity\TarifFrenchTaxiRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:TarifFrenchTaxi');
    }

}