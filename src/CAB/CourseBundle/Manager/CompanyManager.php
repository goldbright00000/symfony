<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;


use CAB\CourseBundle\Entity\Vehicule;
use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Manager\BaseManager;
use CAB\CourseBundle\Entity\Company;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class CompanyManager
 *
 * @package CAB\CourseBundle\Manager
 */
class CompanyManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var authorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @param EntityManager        $em
     * @param TokenStorage         $tokenStorage
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(EntityManager $em, TokenStorage $tokenStorage, AuthorizationChecker $authorizationChecker)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function loadDefaultCompany() {
        return $this->getRepository()
            ->findOneBy(array('id' => 10));
    }
    /**
     * @param int $companyId
     *
     * @return Company
     */
    public function loadCompany($companyId)
    {
        return $this->getRepository()
            ->findOneBy(array('id' => $companyId));
    }

    /**
     * @param Company    $company
     * @param string     $attr
     * @param int|string $value
     */
    public function setAttr(Company $company, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $company->$attributeSetter($value);
        $this->saveCompany($company);
    }

    /**
     * Save Company entity
     *
     * @param Company $company
     */
    public function saveCompany(Company $company)
    {
        $this->persistAndFlush($company);
    }

    /**
     * Method description
     *
     * @param $long
     * @param $lat
     *
     * @return array
     */
    public function getPriceCourseByCompany($long, $lat) {
        return $this->getRepository()->getCompanyByLongAndLat($long, $lat);
    }

    /**
     * Get companies following the role of current user
     * If the current user is super admin the returned value is an array
     * If the current user is adminCompany and the param $returnedArrayForAdminCompany is true then
     *   the returned value is an array else is a string
     * If the current user is an agent the returned value is a company object
     *
     * @return array|object Company|string
     */
    public function getCompaniesByRole($returnedArrayForAdminCompany = false, $returnedID = false)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            // return array
            $companies = $this->getRepository()->getActiveCompanies();

            return $companies;
        } elseif ($this->authorizationChecker->isGranted('ROLE_ADMINCOMPANY')) {
            $companyAdmin = $currentUser->getContacts()->getIterator();
            if ($returnedArrayForAdminCompany) {
                $company = array();
            } else {
                //return string to use it in query (admin Zone, admin Tarif...)
                $company = '';
            }
            while ($companyAdmin->valid()) {
                if ($returnedArrayForAdminCompany) {
                    $company[$companyAdmin->key()] = $companyAdmin->current();
                } else {
                    $company .= $companyAdmin->current()->getId();
                    if ($companyAdmin->key() < $companyAdmin->count() - 1) {
                        $company .= ',';
                    }
                }
                $companyAdmin->next();
            }

            return $company;
        } elseif ($this->authorizationChecker->isGranted('ROLE_AGENT')) {
            // return object company
            $oCompany = $currentUser->getAgentCompany();
            if ($returnedID) {
                return $oCompany->getId();
            }

            return $oCompany;
        }  elseif ($this->authorizationChecker->isGranted('ROLE_COMPANYBUSINESS')) {
            // return object company
            $oCompany = $currentUser->getBusinessCompany();
            if ($returnedID) {
                return $oCompany->getId();
            }
            
            return $oCompany;
        } elseif ($this->authorizationChecker->isGranted('ROLE_DRIVER')) {
            // return object company
            $oCompany = $currentUser->getDriverCompany();
            if ($returnedID) {
                return $oCompany->getId();
            }
            return $oCompany;
        }
    }

    /**
     * @return \CAB\CourseBundle\Entity\CompanyRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Company');
    }

}