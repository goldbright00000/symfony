<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 15/12/2015
 * Time: 17:42
 */

namespace CAB\CourseBundle\Manager;


use CAB\CourseBundle\Entity\Course;
use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\Planing;

/**
 * Class PlaningManager
 *
 * @package CAB\CourseBundle\Manager
 */
class PlaningManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var CabUserManager
     */
    protected $userManager;

    /**
     * @param EntityManager  $em
     * @param CabUserManager $userManager
     */
    public function __construct(EntityManager $em, CabUserManager $userManager)
    {
        $this->em = $em;
        $this->userManager = $userManager;
    }

    /**
     * @param integer $objectID
     *
     * @return Planing object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()
            ->findOneBy(array('id' => $objectID));
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return array
     */
    public function getPlaningsBy($criteria, $creteriaValue)
    {
        list($aPropertyName, $aPropertyType) = $this->getClassProperties('CAB\CourseBundle\Entity\Planing', true);

        if (in_array($criteria, $aPropertyName)) {
            if ('datetime' === $aPropertyType[$criteria]) {
                $result = $this->getPlaningByDate($criteria, $creteriaValue, null, null);
            } else {
                $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));
            }
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * fetch planings by date
     *
     * @param string      $criteria
     * @param mixte       $value
     * @param string|null $criteria2
     * @param mixte|null  $valueCriteria2
     *
     * @return array
     */
    public function getPlaningByDate($criteria, $value, $criteria2 = null, $valueCriteria2 = null)
    {
        $result = array();
        list($aPropertyName, $aPropertyType) = $this->getClassProperties('CAB\CourseBundle\Entity\Planing', true);
        if (in_array($criteria, $aPropertyName)) {
            if ('datetime' === $aPropertyType[$criteria]) {
                $valueFrom = \DateTime::createFromFormat('d-m-Y', $value);
                $valueTo = \DateTime::createFromFormat('d-m-Y', $value);
                $valueFrom->setTime(0, 0, 0);
                $valueTo->setTime(23, 59, 59);
                $result = $this->getRepository()->findPlaningByCriteria($criteria, $valueFrom, $valueTo, $criteria2, $valueCriteria2);
            }
        }

        return $result;
    }

    /**
     * fetch all planings
     *
     * @return Collection of Planing object
     */
    public function loadAllObjects()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Fetch planings
     *
     * @param Driver|null $driver
     * @return array
     */
    public function getArrayPlaning($driver = null)
    {
        
        $aResult = array();
        if ($driver === null) {
            $result = $this->loadAllObjects();
        } else {
            $result = $this->getPlaningsBy('driver', $driver->getId());
        }
        /** @var Planing $item */
        foreach ($result as $item) {
            /** @var Course $course */
            $course = $item->getCourse();
            $client = '';
            $price = '';
            $dep = '';
            $dateDep = '';
            $dateArr = '';
            $arr = '';
            $idCourse = $item->getCourse()->getId();
            $duration = 0;
            if ($course instanceof Course) {
                if ($course->getClient() !== null) {
                    $client = $course->getClient()->getUsedName();
                    //$price = $course->getCourseDetailsTarif()->getTarifTotalTTC();
                    $dep = $course->getStartedAddress();
                    $arr = $course->getArrivalAddress();
                    $duration = $course->getCourseTime();
                    $dateDep = $course->getDepartureTime()->format('H:i');
                    //$dateArr = $course->getArrivalHour()->format('H:i');
                }
            }
            $aResult[] = array(
                'id' => $item->getId(),
                'title' => $item->getPlaningName(),
                'start' => $item->getStart()->format('Y-m-d H:i:s'),
                'end' => $item->getEnd()->format('Y-m-d H:i:s'),
                'client' => $client,
                //'price' => $price,
                'departure_address' => $dep,
                'arrival_address' => $arr,
                'course_id' => $idCourse,
                'duration' => $duration,
                'date_dep' => $dateDep,
                'date_dep' => $dateArr,
            );
        }


        return $aResult;
    }

    /**
     * Return all drivers availables
     *
     * @param \DateTime $depDate
     * @param \DateTime $arrDate
     *
     * @return array
     */
    public function checkAvailability(\DateTime $depDate, \DateTime $arrDate)
    {
        $aDriversNotAvailable = $this->getRepository()->getDriversNotAvailable($depDate, $arrDate);
        $allDrivers = $this->userManager->getUsersByRole('ROLE_DRIVER');
        foreach ($allDrivers as $driver) {
            $aDrivers[] = $driver->getId();
        }

        $aNotAvailableDrivers = array();
        if (!empty($aDriversNotAvailable)) {
            foreach ($aDriversNotAvailable as $item) {
                if (!in_array($item->getDriver()->getId(), $aNotAvailableDrivers)) {
                    $aNotAvailableDrivers[] = $item->getDriver()->getId();
                }
            }
        }

        $availableDriver = array_diff($aDrivers, $aNotAvailableDrivers);

        return $availableDriver;
    }

    /**
     * Return all drivers availables
     *
     * @param \DateTime $depDate
     * @param \DateTime $arrDate
     *
     * @return array
     */
    public function getNotAvailableDrivers(\DateTime $depDate, \DateTime $arrDate)
    {
        $aDriversNotAvailable = $this->getRepository()->getDriversNotAvailable($depDate, $arrDate);
        
        $allDrivers = $this->userManager->getUsersByRole('ROLE_DRIVER');
        foreach ($allDrivers as $driver) {
            $aDrivers[] = $driver->getId();
        }

        $aNotAvailableDrivers = array();
        if (!empty($aDriversNotAvailable)) {
            foreach ($aDriversNotAvailable as $item) {
                if (!in_array($item->getDriver()->getId(), $aNotAvailableDrivers)) {
                    $aNotAvailableDrivers[] = $item->getDriver()->getId();
                }
            }
        }

        return $aNotAvailableDrivers;
    }



    /**
     * Save Planing entity
     *
     * @param array        $params
     * @param Planing|null $oPlaning
     *
     * @return array
     */
    public function saveObject(array $params, $oPlaning = null)
    {
        if (!$oPlaning) {
            $oPlaning = new Planing();
        }

        foreach ($params as $key => $value) {
            if (property_exists('CAB\CourseBundle\Entity\Planing', $key)) {
                $mSetter = 'set'.ucfirst($key);
                $oPlaning->$mSetter($value);
            }
        }
        try {
            
            $this->persistAndFlush($oPlaning);

            return array(
                'status' => true,
                'response' => '',
                );
        } catch (\Exception $e) {
            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * Remove a planing
     *
     * @param Planing $oPlaning
     */
    public function removePlaning($oPlaning)
    {
        $this->em->remove($oPlaning);
        $this->em->flush();
    }

    /**
     * @return \CAB\CourseBundle\Entity\PlaningRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Planing');
    }
}