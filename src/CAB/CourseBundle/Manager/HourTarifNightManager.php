<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;


use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Manager\BaseManager;
use CAB\CourseBundle\Entity\HourTarifNight;

class HourTarifNightManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param integer $objectID
     *
     * @return HourTarifNight object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()
            ->findOneBy(array('id' => $objectID));
    }

    /**
     * @return Collection of HourTarifNight object
     */
    public function loadAllObjects()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * @return array
     */
    public function getArrayHourTarif()
    {
        $aResult = array();
        $result = $this->loadAllObjects();
        foreach ($result as $item) {
            $aResult[$item->getId()] = $item->getLabelTarif();
        }

        return $aResult;
    }

    /**
     * Save HourTarifNight entity
     *
     * @param HourTarifNight $obj
     */
    public function saveVehicle(HourTarifNight $obj)
    {
        $this->persistAndFlush($obj);
    }

    /**
     * @return \CAB\CourseBundle\Entity\HourTarifNightRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:HourTarifNight');
    }
}