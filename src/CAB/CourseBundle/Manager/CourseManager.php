<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\Tarif;
use CAB\CourseBundle\Event\HistoryCourseEvent;
use CAB\CourseBundle\Events\HistoryCourseEvents;
use CAB\CourseBundle\Events\UpdateStatusCourseEvents;
use Doctrine\Common\CommonException;
use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\Course;
use CAB\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManager;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * Class CourseManager
 *
 * @package CAB\CourseBundle\Manager
 */
class  CourseManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    protected $userManager;

    private $tarifManager;
    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher, UserManager $userManager, TaxManager $taxManager)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->userManager = $userManager;
        $this->taxManager = $taxManager;
    }

    /**
     * @param int $courseId
     *
     * @return Course
     */
    public function loadCourse($courseId)
    {
        return $this->getRepository()->find($courseId);
    }

    /**
     * set attribute
     *
     * @param Course $course
     * @param string $attr
     * @param string $value
     *
     * @throws CommonException
     */
    public function setAttr(Course $course, $attr, $value, $exception = true)
    {
        $aProperties = $this->getClassProperties('CAB\CourseBundle\Entity\Course');

        if (in_array($attr, $aProperties)) {
            $attributeSetter = 'set'.ucfirst($attr);
            $course->$attributeSetter($value);
            // dispatch event if the status of the course is changed
            if ($attr == 'courseStatus') {
                /* save the course when the status is updated. For other attributes should call saveCourse method when
                    you call setAttr
                */

                $this->em->persist($course);
                $this->em->flush();

                $event = new GenericEvent(
                    $course,
                    array(
                        'status_course' => $value,
                    )
                );

                $eventHistory = new GenericEvent(
                    $course,
                    array(
                        'course_history' => $value,
                    )
                );

                $this->dispatcher->dispatch(UpdateStatusCourseEvents::UPDATE_STATUS_COURSE, $event);
                $this->dispatcher->dispatch(HistoryCourseEvents::HISTORY_COURSE, $eventHistory);
            }
        } elseif ($exception){
            throw new CommonException('The key provided isn\'t a correct property of the class');
        }
    }
    /**
     * Manage Course entity
     *
     * @param Course    $course
     * @param array     $params
     * @param User|null $user
     * @param Tarif     $tarif
     *
     * @return Course $course
     *
     * @throws CommonException
     */
    public function manageCourse(Course $course, array $params, $user = null, Tarif $tarif = null)
    {
        if (!$params['departAddress'] || !$params['arrivalAddress'] || $params['withBack'] == '' || !$params['personNumber'] || !$params['company']) {
            // @TODO Throws exception course can't created
            return null;
        }

        /*
         * @TODO add serviceTransport
         */
        $aValuesCourse = $this->initValueCourses($params, $user, $tarif);

        foreach ($aValuesCourse as $attr => $value) {
            if ($attr != 'cllient' && $value !== null) {
                $attributeSetter = 'set'.ucfirst($attr);
                $course->$attributeSetter($value);
            }
        }

        try {
            if ($course->getId()) {
                $this->em->persist($course);
            } else {
                $this->persistAndFlush($course);
            }
        } catch (CommonException $e) {
            throw new CommonException($e->getMessage());
        }

        return $course;
    }

    /**
     * @param User   $user
     * @param string $criteria
     * @param array  $sort
     *
     * @return Course
     */
    public function loadCourseByContext($user, $criteria,  $sort = array())
    {
        if (!empty($sort)) {
            return $this->getRepository()->findOneBy(array($criteria => $user), $sort);
        }

        return $this->getRepository()->findOneBy(array($criteria => $user));
    }

    public function getCycleChildCourse(Course $course)
    {
        return $this->getRepository()->findCycleChildCoursesByParentAndDate($course);
    }

    /**
     * @param User    $user
     * @param string  $role
     * @param string  $criteria
     * @param integer $criteriaValue
     *
     * @return array
     */
    public function getCourseBy(User $user, $role, $criteria, $criteriaValue)
    {
        list($aPropertyName, $aPropertyType) = $this->getClassProperties(Course::class, true);

        if (in_array($criteria, $aPropertyName)) {
            if (in_array($aPropertyType[$criteria], array('date', 'datetime', 'time'))) {
                $valueFrom = \DateTime::createFromFormat('d-m-Y', $criteriaValue);
                $valueTo = \DateTime::createFromFormat('d-m-Y', $criteriaValue);
                $valueFrom->setTime(0, 0, 0);
                $valueTo->setTime(23, 59, 59);
                $result = $this->getRepository()->findCourseByDates($user, $role, $criteria, $valueFrom, $valueTo);
            } elseif (in_array($aPropertyType[$criteria], array('text', 'string'))) {
                $result = $this->getRepository()->findByCriteria($user, $role, $criteria, $criteriaValue, true);
            } else {
                $result = $this->getRepository()->findByCriteria($user, $role, $criteria, $criteriaValue);
            }
        } elseif ($criteria == "null") {
            $result = $this->getRepository()->findBy(array($role => $user));
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * Method description
     *
     * @param User   $user
     * @param string $role
     * @param string $valueDateFrom format 18-12-2017
     * @param string $valueDateTo format 18-12-2017
     * @param string $valueTimeFrom the format of time e.g 09:56:37
     * @param string $valueTimeTo the format of time e.g 10:28:42
     *
     * @return array
     */
    public function getCourseByDate(User $user, $role, $valueDateFrom, $valueDateTo, $valueTimeFrom = null, $valueTimeTo = null)
    {
        $aDateFrom = explode('-', $valueDateFrom);
        $aDateTo = explode('-', $valueDateTo);
        if (count($aDateFrom) < 3 || count($aDateTo) < 3) {
            return array(
                'status' => 0,
                'response' => 'You should give a date with this format: d-m-Y',
            );
        }

        $checkDateFrom = checkdate($aDateFrom[1], $aDateFrom[0], $aDateFrom[2]);
        $checkDateTo = checkdate($aDateTo[1], $aDateTo[0], $aDateTo[2]);
        if ($checkDateFrom && $checkDateTo) {
            $valueFrom = \DateTime::createFromFormat('d-m-Y', $valueDateFrom);
            $valueTo = \DateTime::createFromFormat('d-m-Y', $valueDateTo);
            if ($valueTimeFrom) {
                $aDateTimeFrom = explode(':', $valueTimeFrom);
                $valueFrom->setTime($aDateTimeFrom[0], $aDateTimeFrom[1], $aDateTimeFrom[2]);
            } else {
                $valueFrom->setTime(0, 0, 0);
            }
            if ($valueTimeTo) {
                $aDateTimeTo = explode(':', $valueTimeTo);
                $valueTo->setTime($aDateTimeTo[0], $aDateTimeTo[1], $aDateTimeTo[2]);
            } else {
                $valueTo->setTime(23, 59, 59);
            }

            $result = $this->getRepository()->findCourseByDatesInterval($user, $valueFrom, $valueTo, $role);

            return $result;
        } else {
            return array(
                'status' => 0,
                'response' => 'You should give a valid date with this format: d-m-Y',
            );
        }
    }

    /**
     * Method description
     *
     * @param User   $user
     *
     * @return array
     */
    public function getCourseByDateArrival(User $user)
    {

            $result = $this->getRepository()->findCourseRuningByDatesAndDriver($user);

            return $result;
    }

    /**
     * Method description
     *
     * @param $compagnyId
     *
     * @return array
     */
    public function getCourseByCompagny($companyId)
    {

        $result = $this->getRepository()->findCourseRuningByDatesAndCompagny($companyId);

        return $result;
    }


    /**
     * get not affected course for the company of the driver
     *
     * @param Company $company
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getAllCourseNotAffected(Company $company)
    {

        $status = array(Course::STATUS_PAYED, Course::STATUS_ATTENTE_PAY, Course::STATUS_CACH_PAY);

        return $this->getRepository()->findNotAffectedCoursesByCompany($company);
    }

    /**
     * Get courses of a given company business
     *
     * @param Company $companyBusiness
     *
     * @return array
     */
    public function getCoursesOfBusiness(Company $companyBusiness)
    {
        $result = $this->getRepository()->getCourseOfBusinessCompany($companyBusiness);

        $courses = array();
        foreach ($result as $item) {
            $courses[] = $item['id'];
        }

        return $courses;
    }

    /**
     * Save Course entity
     *
     * @param Course $course
     */
    public function saveCourse(Course $course, $isDateTimeUpdated = false, $aDrivers = array(), $oldPrice = 0, $actionDriver = false)
    {
        $this->persistAndFlush($course);
    }

    /**
     * Save Course entity
     *
     * @param Course $course
     */
    public function saveCourseWithMerge(Course $course)
    {
        $this->mergeAndFlush($course);
    }

    /**
     * Method description
     *
     * @param Course $course
     */
    public function save(Course $course, $dispatchEvent = true)
    {
        $this->persistAndFlush($course, $dispatchEvent);
    }

    /**
     * Remove course.
     *
     * @param Course $course
     *
     * @return array
     */
    public function cancelCourse(Course $course, User $user)
    {
        try {
            if ($user === $course->getClient()) {
                $this->setAttr($course, 'courseStatus', Course::STATUS_CANCELLED_BY_CLIENT);
                $this->save($course);

                return array(
                    'status' => 1,
                    'response' => 'Cette course est annulée',
                );
            } else {
                return array(
                    'status' => 0,
                    'response' => 'Your are not allowed to remove this course',
                );
            }
        } catch (\Exception $e) {
            return array(
                'status' => 0,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * Remove course.
     *
     * @param Course $course
     *
     * @return array
     */
    public function cancelCourseByAgent(Course $course, User $user)
    {
        try {
            $roles = $user->getRoles();
            if (in_array('ROLE_AGENT', $roles, true) && $user->getAgentCompany() === $course->getCompany()) {
                $this->setAttr($course, 'courseStatus', Course::STATUS_CANCELLED_BY_AGENT);
                $this->save($course);

                return array(
                    'status' => 1,
                    'response' => 'Course canceled.',
                );
            } else {
                return array(
                    'status' => 0,
                    'response' => 'Your are not allowed to cancel this course',
                );
            }
        } catch (\Exception $e) {
            return array(
                'status' => 0,
                'response' => $e->getMessage(),
            );
        }
    }

    public function deleteCourse($course)
    {
        $this->removeAndFlush($course);
    }

    /**
     * Remove course.
     *
     * @param Course $course
     *
     * @return array
     */
    public function cancelDriverCourse(Course $course, User $user)
    {
        try {
            if ($user === $course->getDriver()) {
                $this->setAttr($course, 'courseStatus', Course::STATUS_CANCELLED_BY_DRIVER);
                $this->save($course);

                return array(
                    'status' => 1,
                    'response' => 'Course cancel',
                );
            } else {
                return array(
                    'status' => 0,
                    'response' => 'Your are not allowed to remove this course',
                );
            }
        } catch (\Exception $e) {
            return array(
                'status' => 0,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * Method description
     *
     * @return Company
     */
    protected function getTax($taxcompany) {
        $tax = $this->taxManager->loadTax($taxcompany);

        return $tax;
    }

    public function getCoursesForGroupage($departureAddress, $arrivalAddress, \DateTime $date = null, $companyId = null)
    {
        return $this->getRepository()->getCoursesForGroupage($departureAddress, $arrivalAddress, $date, $companyId);
    }


    /**
     * Find course by command sncf
     * @param string $commandSncf
     * @return mixed
     */
    public function findByCommandSncf($commandSncf) {
        return $this->getRepository()->getCourseByCommandSncf($commandSncf);
    }

    public function updatePriceAndPaymentModeApi(array $params)
    {
	    $payment_mode = isset($params['payment_mode'])?$params['payment_mode']:'';
	    $course_status = isset($params['course_status'])?$params['course_status']:'';
        $course = $this->loadCourse($params['course_id']);
        $driver = $this->userManager->findUserBy(array('id' => $params['driver_id']));
        if ($course->getDriver() && $driver === $course->getDriver()) {
            $priceTTC = $params['price'];
            //calcul price ht:
            $taxcompany = $course->getCompany()->getId();
            $taxdata = $this->taxManager->loadTax($taxcompany);

             if ($taxdata != null ) {

                foreach ($taxdata as $item) {
                $tax = $item->getValue();
                }
                $priceHT = $priceTTC * (100/(100+$tax));
                $priceHT = number_format($priceHT,2);
                }else {
                $priceHT = $priceTTC ;
                 }

            //Update price in detailsTarifCourse
            //$oDetailsTarif = $course->getCourseDetailsTarif();
            //$oDetailsTarif->setTarifHT($priceHT);
            //$oDetailsTarif->setTarifTotalTTC($priceTTC);

            $this->setAttr($course, 'priceTTC', $priceTTC);
            $this->setAttr($course, 'priceHT', $priceHT);
            $this->setAttr($course, 'paymentMode', $payment_mode);
            if ($course_status != 6) {
                return array(
                    'status' => 0,
                    'response' => 'The course status should be equal 6.',
                );
            }
            $this->setAttr($course, 'courseStatus', $course_status);
            //$course->setCourseDetailsTarif($oDetailsTarif);

            $this->em->persist($course);
            $this->em->flush();

            return array(
                'status' => 1,
                'response' => $course,
            );
        } else {
            return array(
                'status' => 0,
                'response' => 'The driver isn\'t affected to this course.',
            );
        }

    }

    /*
     * Get list of course not completed that have status_course {13, 9, 8, 4}
     */
    /**
     * @param $driver int ID driver
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getCourseNotCompletedByDriver ($driver) {
        $listCourse = $this->getRepository()->getCourseByStatus(array(12, 13, 9, 4), $driver);
        return $listCourse;
    }

    /**
     * @return \CAB\CourseBundle\Entity\CourseRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Course');
    }

    /**
     * @param array $params
     * @param $user
     * @param Tarif|null $tarif
     * @return array
     */
    protected function initValueCourses(array $params, $user, $tarif, $createdBy = null)
    {
        $aValuesCourse = array(
            'client' => $user,
            'createdBy' => $createdBy?:$user,
            'serviceTransportCourses' => null,
            'startedAddress' => $params['departAddress'],
            'arrivalAddress' => $params['arrivalAddress'],
            'departureDate' => new \DateTime($params['depDate']),
            'departureTime' => new \DateTime($params['depTime']),
            'targetType' => $params['withBack'],
            'courseTime' => $params['estimatedTime'],
            'distance' => $params['estimatedDistance'],
            'personNumber' => $params['personNumber'],
            'priceHT' => $params['depPrice'],
            'priceTTC' => $params['totalPriceTTC'],
            'longDep' => $params['coordinate']['lngDepart'],
            'latDep' => $params['coordinate']['latDepart'],
            'longArr' => $params['coordinate']['latArrival'],
            'latArr' => $params['coordinate']['lngArrival'],
            'applicableTarif' => $tarif,
            'company' => $params['company'],
        );

        if ($params['withBack'] != 0) {
            $aValuesCourse['distanceBack'] = $params['estimatedDistance'];
            $aValuesCourse['backDate'] = new \DateTime($params['depDate']);
            $aValuesCourse['backTime'] = new \DateTime($params['depTime']);
            $aValuesCourse['backAddressDeparture'] = $params['departAddressBack'];
            $aValuesCourse['backAddressArrival'] = $params['arrivalAddressBack'];
            $aValuesCourse['courseTimeBack'] = $params['estimatedTime'];
            $aValuesCourse['personNumberBack'] = $params['personNumber'];
            $aValuesCourse['priceHTBack'] = $params['totalPrice'] - $params['depPrice'];
            $aValuesCourse['longDepBack'] = $params['coordinateBack']['lngDepart'];
            $aValuesCourse['latDepBack'] = $params['coordinateBack']['latDepart'];
            $aValuesCourse['longArrBack'] = $params['coordinateBack']['latArrival'];
            $aValuesCourse['latArrBack'] = $params['coordinateBack']['lngArrival'];
            return $aValuesCourse;
        }
        return $aValuesCourse;
    }



    public function getPlaningsByDate($driver, $start, $end)
    {
        if ($start && $end) {
            $result = $this->getRepository()->findCourseByDatesIntervalPlaning($driver, $end, $start);
            return $result;
        } else {
            return array(
                'status' => 0,
                'response' => 'You should give a valid date with this format: d-m-Y',
            );
        }
    }



    /**
     * Fetch course
     *
     * @param Driver $driver
     * @return array
     */
    public function getArrayPlaning($driver, $start, $end)
    {
        //$valueFrom = \DateTime::createFromFormat('d-m-Y', $valueDateFrom);
        //$valueTo = \DateTime::createFromFormat('d-m-Y', $valueDateTo);
        //$valueFrom->setTime(0, 0, 0);
        //$valueTo->setTime(23, 59, 59);

        //$start = $start->format('d-m-Y');
        //$end =  $end->format('d-m-Y');
        $result = $this->getPlaningsByDate($driver,$start,$end);


        $aResult = array();


        foreach ($result as $item) {
            /** @var Course $course */
            $idCourse = $item->getId();
            $prenom = $item->getClient()->getfirstName();
            $nom = $item->getClient()->getlastName();
            $dep = $item->getStartedAddress();
            $arr = $item->getArrivalAddress();
            $DateDep = $item->getDepartureDate()->format('Y-m-d');
            $HeureDep = $item->getDepartureTime()->format('H:i');
            $datetime = $DateDep. ' ' .$HeureDep;


            $aResult[] = array(
                'id' => $idCourse,
                //'date' => $DateDep,
                'start' => $datetime,
                'end' => $datetime,
                'client' => $nom.' '.$prenom,
                'departure_address' => $dep,
                'arrival_address' => $arr,

            );

        }

        return $aResult;
    }

    /**
     * @param User $user
     * @param \DateTime $valueFrom
     * @param string $role
     * @param bool $oneResult
     * @return array
     */
    public function findCourseByDatesDepArrival(User $user, \DateTime $valueFrom, $role = 'driver', $oneResult = false, $interval = 'PT3600S', $sortField = 'departureDate', $order = 'DESC')
    {
        return $this->getRepository()->findCourseByDatesDepArrival($user, $valueFrom, $role, $oneResult, $interval, $sortField, $order);
    }

    /**
     * @param  $aDriverId
     * @return array
     */
    public function findNextRaceByDriver($aDriverId)
    {
        return $this->getRepository()->findTheNextRace($aDriverId);
    }



    public function createCourseForSNCF($params, $commandSncf = null)
    {
	    $aValuesCourse = $this->initValueCourses($params, null, null);
	    $course = new Course();
	    foreach ($aValuesCourse as $attr => $value) {
		    if ($attr != 'client' && $value !== null) {
			    $attributeSetter = 'set'.ucfirst($attr);
			    $course->$attributeSetter($value);
		    }
	    }
	    if ($commandSncf) {
	    	$course->setCommandSncf($commandSncf);
	    }
	    $this->save($course);

	    return [$course->getId(), $course->getCommandSncf()?$course->getCommandSncf()->getId():null];
    }

}