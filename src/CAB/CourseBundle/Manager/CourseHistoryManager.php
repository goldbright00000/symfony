<?php

namespace CAB\CourseBundle\Manager;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\CourseHistory;
use CAB\UserBundle\Entity\User;
use Doctrine\Common\CommonException;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;

/**
 * CourseHistoryManager
 *
 * Add your own custom manager methods below.
 */
class CourseHistoryManager extends BaseManager
{
    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var LoggerInterface
     */

    private $logger;
    /**
     * @param EntityManager   $em
     * @param string          $entityClass
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManager $em, $entityClass, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
        $this->logger = $logger;
    }

    /**
     * Method description
     *
     * @param array $params
     *
     * @throws \Exception
     */
    public function createHistory(array $params)
    {
        try {
            $courseHistory = new CourseHistory();
            foreach ($params as $attr => $value) {
                $this->setAttr($courseHistory, $attr, $value);
            }
            $this->persistAndFlush($courseHistory);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage().' '.$e->getFile().' '.$e->getLine());
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Method description
     *
     * @param Course $course
     *
     *
     * @throws CommonException
     */
    public function createHistoryFromCourse(Course $course)
    {

        $aParams = array(
            'course' => $course,
            'status' => $course->getCourseStatus(),
            'driver' => $course->getDriver(),
            'departureAddress' => $course->getStartedAddress(),
            'arrivalAddress' => $course->getArrivalAddress(),
            'priceTTC' => $course->getPriceTTC(),
            'departureDate' => $course->getDepartureDate(),
            'departureTime' => $course->getDepartureTime(),
            'targetType' => $course->getTargetType(),
            'agent' => $course->getcreatedBy()

        );

        $this->createHistory($aParams);
    }

    /**
     * @param int $courseHistoryId
     *
     * @return CourseHistoryManager
     */
    public function loadCourseHistoryManager($courseHistoryId)
    {
        return $this->getRepository()->find($courseHistoryId);
    }

    /**
     * Find history of given course
     *
     * @param int $courseID
     *
     * @return array
     */
    public function getHitoryByCourse($courseID)
    {
        $result = $this->getRepository()->findBy(array('course' => $courseID));

        return $result;
    }

    /**
     * Find history of given course
     *
     * @param int $course
     * @param int $status
     *
     * @return array
     */
    public function getHitoryByCourseAndStatus($course, $status)
    {
        $result = null;
        if ($course instanceof Course) {
            $result = $this->getRepository()->findOneBy(array('course' => $course, 'status' => $status), array('id' => 'DESC'));
        }

        return $result;
    }

    /**
     * Find history of given course
     *
     * @param Course $course
     * @param User $driver
     *
     * @return array
     */
    public function getHitoryByCourseAndDriver(Course $course, User $driver)
    {
        $result = null;
        if ($course instanceof Course) {
            $result = $this->getRepository()->findBy(array('course' => $course, 'driver' => $driver));
        }

        return $result;
    }

    public function getHitoryByDriver(User $driver)
    {
        $result = $this->getRepository()->findBy(array('driver' => $driver));

        return $result;
    }

    /**
     * @param CourseHistory $courseHistory
     * @param string        $attr
     * @param int|string    $value
     *
     * @throws CommonException
     */
    public function setAttr(CourseHistory $courseHistory, $attr, $value)
    {
        $aProperties = $this->getClassProperties('CAB\CourseBundle\Entity\CourseHistory');

        if (in_array($attr, $aProperties)) {
            $attributeSetter = 'set'.ucfirst($attr);

            $courseHistory->$attributeSetter($value);
        } else {
            $this->logger->error(sprintf('The key provided \'%s\' isn\'t a correct property of the class \'CAB\CourseBundle\Entity\CourseHistory\'', $attr));
            throw new CommonException(sprintf('The key provided \'%s\' isn\'t a correct property of the class \'CAB\CourseBundle\Entity\CourseHistory\'', $attr));
        }
    }

    /**
     * Save CourseHistoryManager entity
     *
     * @param CourseHistoryManager $courseHistory
     */
    public function saveCourseHistoryManager(CourseHistoryManager $courseHistory)
    {
        $this->persistAndFlush($courseHistory);
    }

    /**
     * @return \CAB\CourseBundle\Entity\CourseHistoryRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository($this->entityClass);
    }

	/**
	 * Persist and flush entity
	 *
	 * @param $entity
	 * @param bool $dispatchEvent
	 */
    protected function persistAndFlush($entity, $dispatchEvent = true)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

}
