<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 29/12/2015
 * Time: 21:55
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\DetailsTarif;

/**
 * Class DetailsTarifManager
 *
 * @package CAB\CourseBundle\Manager
 */
class DetailsTarifManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param integer $objectID
     *
     * @return DetailsTarif object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()->find($objectID);
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return array
     */
    public function getDetailsBy($criteria, $creteriaValue)
    {
        $result = null;
        //$aKeys = $this->getAttributeClass();

        $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));

        return $result;
    }
    /**
     * @return Collection of DetailsTarif object
     */
    public function loadAllObjects()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Save DetailsTarif entity
     *
     * @param array $params
     *
     * @return array
     */
    public function saveObject(array $params, $oDetailsTarif = false)
    {
        $aProperties = $this->getClassProperties('CAB\CourseBundle\Entity\DetailsTarif');
        if (!$oDetailsTarif) {
            $oDetailsTarif = new DetailsTarif();
        }

        foreach ($params as $key => $value) {
            if (in_array($key, $aProperties)) {
                $mSetter = 'set'.ucfirst($key);
                $oDetailsTarif->$mSetter($value);
            }
        }
        try {
            $this->persistAndFlush($oDetailsTarif);

            return array(
                'status' => true,
                'response' => $oDetailsTarif,
                );
        } catch (\Exception $e) {
            
            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * @return \CAB\CourseBundle\Entity\DetailsTarifRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:DetailsTarif');
    }

    /**
     * @return array
     */
    private function getAttributeClass()
    {
        $oDetailsTarif = new DetailsTarif();

        return array_keys(get_object_vars($oDetailsTarif));
    }
}