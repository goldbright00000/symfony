<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:44
 */
namespace CAB\CourseBundle\Manager\Vehicle;

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Annotations\AnnotationReader;
use CAB\CourseBundle\Entity\VehiculeFuel;
use CAB\CourseBundle\Entity\VehiculeClean;
use CAB\CourseBundle\Entity\VehiculeMaintenance;

/**
 * Class BaseManager
 *
 * @property EntityManager em
 * @package CAB\CourseBundle\Manager\Vehicle
 */
class BaseVehicleManager
{
    protected $em;
    protected $entityFuel;
    protected $entityClean;
    protected $entityMaintenance;

    /**
     * @param EntityManager $em
     * @param string        $entityFuel
     * @param string        $entityClean
     * @param string        $entityMaintenance
     */
    public function __construct(EntityManager $em, $entityFuel, $entityClean, $entityMaintenance)
    {
        $this->em = $em;
        $this->entityFuel = $entityFuel;
        $this->entityClean = $entityClean;
        $this->entityMaintenance = $entityMaintenance;
    }

    /**
     * @param int $entityId
     *
     * @return Vehiculefuel|VehiculeClean|VehiculeMaintenance
     */
    public function loadEntity($entityId)
    {
        return $this->getRepository('')->find($entityId);
    }

    /**
     * @param Vehiculefuel|VehiculeClean|VehiculeMaintenance $entity
     * @param string $attr
     * @param int|string $value
     * @throws \Exception
     */
    public function setAttr($entity, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $entity->$attributeSetter($value);
        $this->saveEntity($entity);
    }

    /**
     * Save entity.
     *
     * @param   Vehiculefuel|VehiculeClean|VehiculeMaintenance $entity
     * @throws \Exception
     */
    public function saveEntity($entity)
    {
        $this->persistAndFlush($entity);
    }

    /**
     * @param string $entityClass
     * @return  \CAB\CourseBundle\Entity\VehiculeFuleRepository | \CAB\CourseBundle\Entity\VehiculeCleanRepository | \CAB\CourseBundle\Entity\VehiculeMaintenanceRepository
     */
    public function getRepository($entityClass)
    {
        return $this->em->getRepository($entityClass);
    }

    /**
     * Persist and flush entity
     *
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function persistAndFlush($entity)
    {
        if (!$this->em->isOpen()) {
            $this->entityManager = $this->em->create(
                $this->em->getConnection(),
                $this->em->getConfiguration()
            );
        }
        $this->em->persist($entity);
        $this->em->flush();
    }

    protected function removeAndFlush($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * merge And Flush entity
     *
     * @param $entity
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function mergeAndFlush($entity)
    {
        $this->em->merge($entity);
        $this->em->flush();
    }
    /**
     * Return all properties of class
     *
     * @param $class
     *
     * @return array
     */
    protected function getClassProperties($class, $getType = false)
    {
        $oClass = new $class();
        $res = array();
        $reflect = new \ReflectionClass($oClass);
        $props   = $reflect->getProperties();

        if ($getType) {
            foreach ($props as $prop) {
                $docReader = new AnnotationReader();
                $docInfos = $docReader->getPropertyAnnotations($reflect->getProperty($prop->getName()));
                $type = '';
                if ($docInfos[0] instanceof \Doctrine\ORM\Mapping\Column) {
                    $type = $docInfos[0]->type;
                }

                $aPropertyName[] = $prop->getName();
                $aPropertyType[$prop->getName()] = $type;
            }

            return array($aPropertyName, $aPropertyType);
        } else {
            foreach ($props as $prop) {
                $res[] = $prop->getName();
            }
        }

        return $res;
    }
}