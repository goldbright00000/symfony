<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/12/2016
 * Time: 20:26
 */

namespace CAB\CourseBundle\Manager;


use CAB\CourseBundle\Entity\Forfait;
use Doctrine\ORM\EntityManager;

class ForfaitManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $id
     *
     * @return Forfait
     */
    public function loadForfait($id)
    {
        $oForfait =  $this->getRepository()->find($id);

        return $oForfait;
    }

    /**
     * @param Forfait    $forfait
     * @param string     $attr
     * @param int|string $value
     */
    public function setAttr(Forfait $forfait, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $forfait->$attributeSetter($value);
        $this->save($forfait);
    }

    /**
     * Save Forfait entity
     *
     * @param Forfait $forfait
     */
    public function save(Forfait $forfait)
    {
        $this->persistAndFlush($forfait);
    }

    /**
     * @return \CAB\CourseBundle\Entity\ForfaitRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Forfait');
    }

    /**
     * @param $longDep
     * @param $latDep
     * @param $longArr
     * @param $latArr
     * @param $company
     * @return array
     */
    public function checkForfait ($longDep, $latDep, $longArr, $latArr, $company)
    {
        $resultCompanyForfait = $this->getRepository()->getForfaitByLongAndLat(
            $longDep,
            $latDep,
            $longArr,
            $latArr,
            $company->getId()
        );
        return $resultCompanyForfait;
    }

}