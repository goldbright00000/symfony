<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 15/12/2015
 * Time: 17:42
 */

namespace CAB\CourseBundle\Manager;


use CAB\CourseBundle\Document\GeolocationCustomer;
use Doctrine\ORM\EntityManager;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Psr\Log\LoggerInterface;
use CAB\CourseBundle\Document\Geolocation;

/**
 * Class GeolocationManager
 *
 * @package CAB\CourseBundle\Manager
 */
class GeolocationManager extends BaseManager
{
    /**
     * @var ManagerRegistry
     */
    protected $dm;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var
     */
    protected $class;

    /**
     * Constructor
     *
     * @param ManagerRegistry $dm     ManagerRegistry
     * @param LoggerInterface $logger logger
     */
    public function __construct(ManagerRegistry $dm, LoggerInterface $logger)
    {
        $this->dm = $dm;
        $this->logger = $logger;
    }

    /**
     * @param integer $objectID
     *
     * @return Geolocation object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()
            ->findOneBy(array('id' => $objectID));
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return array
     */
    public function getGeolocationsBy($criteria, $creteriaValue, $geoCustomer = false)
    {
        // @TODO checkif $creteria is an attribute of Geolocation entity
        if (!$geoCustomer) {
            $result = $this->getRepository()->findOneBy(array($criteria => $creteriaValue));
        } else {
            $result = $this->getRepositoryGeolocationCustomer()->findOneBy(array($criteria => $creteriaValue));
        }

        return $result;
    }

    /**
     * get the last geolocation of driver or customer
     *
     * @param string $criteria
     * @param int    $userId
     *
     * @return Geolocation|GeolocationCustomer
     */
    public function getLastGeolocation($criteria, $userId) {
        /** @var array $result */
        $result = $this->getRepository()->findBy(array($criteria => $userId), array('dateGeolocate' => 'DESC'), 1);

        if (count($result)) {
            return $result[0];
        } else {
            return null;
        }
    }

    /**
     * @param array $aDrivers
     *
     * @return array
     */
    public function getGeolocationsByDrivers(array $aDrivers)
    {
        $result = $this->getRepository()->findGeolocationByDriver($aDrivers);
        return $result;
    }

    /**
     * @param $aDriver
     *
     * @return array
     */
    public function getGeolocationsByDriver($aDriver)
    {
        $result = $this->getRepository()->findGeolocationByDriverId($aDriver);
        return $result;
    }
    

    /**
     * @return Collection of Geolocation object
     */
    public function loadAllObjects($geoCustomer = false)
    {
        if (!$geoCustomer) {
            return $this->getRepository()->findAll();
        } else {
            return $this->getRepositoryGeolocationCustomer()->findAll();
        }
    }

    /**
     * Save Geolocation entity
     *
     * @param array $params
     */
    public function saveObject(array $params, $geoCustomer = false)
    {
        //$aKeys = array('GeolocationName', 'start', 'end', 'driver');
        if (!$geoCustomer) {
            $oGeolocation = new Geolocation();
        } else {
            $oGeolocation = new GeolocationCustomer();
        }
        foreach ($params as $key => $value) {
            //if (in_array($key, $aKeys)) {
                $mSetter = 'set'.ucfirst($key);
                $oGeolocation->$mSetter($value);
            //}
        }
        try {
            $this->persistAndFlush($oGeolocation);

            return array(
                'status' => true,
                'response' => '',
                );
        } catch (\Exception $e) {
            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }

    }

    /**
     * @return \CAB\CourseBundle\Document\Repository\GeolocationRepository
     */
    public function getRepository()
    {
        return $this->dm->getRepository('CABCourseBundle:Geolocation');
    }

    /**
     * @return \CAB\CourseBundle\Document\GeolocationRepository
     */
    public function getRepositoryGeolocationCustomer()
    {
        return $this->dm->getRepository('CABCourseBundle:GeolocationCustomer');
    }
}