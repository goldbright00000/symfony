<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\ServiceTransport;

class ServiceTransportManager extends BaseManager
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function loadServiceTransport($serviceTransportId) {
        return $this->getRepository()
            ->findOneBy(array('id' => $serviceTransportId));
    }

    public function getBy(array $options) {
        $result =  $this->getRepository()->findBy($options);
        $return = array();
        foreach($result as $item)
            $return[] = array('val' => $item->getId(),  'text' => $item->getServiceName());

        return $return;
    }

    public function setAttr(ServiceTransport $serviceTransport, $attr, $value){
        $attributeSetter = 'set' . ucfirst($attr);
        $serviceTransport->$attributeSetter($value);
        $this->saveServiceTransport($serviceTransport);
    }

    /**
     * Save ServiceTransport entity
     *
     * @param ServiceTransport $serviceTransport
     */
    public function saveServiceTransport(ServiceTransport $serviceTransport)
    {
        $this->persistAndFlush($serviceTransport);
    }

    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:ServiceTransport');
    }

}