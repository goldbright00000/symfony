<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\Tax;
use CAB\CourseBundle\Handler\GeolocationHandler;
use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\Tarif;

class TarifManager extends BaseManager {

    protected $em;
    private $geolocationHandler;
    private $taxManager;
    private $dTarifmanager;
    private $geolocationManager;

    /**
     * @param EntityManager      $em
     * @param GeolocationHandler $geolocationHandler
     * @param TaxManager         $taxManager
     * @param GeolocationManager $geolocationManager
     */
    public function __construct(EntityManager $em, GeolocationHandler $geolocationHandler, TaxManager $taxManager,
                                DetailsTarifManager $dTarifmanager, GeolocationManager $geolocationManager
    ) {
        $this->em = $em;
        $this->geolocationHandler = $geolocationHandler;
        $this->taxManager = $taxManager;
        $this->dTarifmanager = $dTarifmanager;
        $this->geolocationManager = $geolocationManager;
    }

    /**
     * @param integer $tarifID
     *
     * @return Tarif object
     */
    public function loadTarif($tarifID) {
        return $this->getRepository()->find($tarifID); 
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return array
     */
    public function getTarifBy($criteria, $creteriaValue) {
        // @TODO checkif $creteria is an attribute of tarif entity
        $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));
        return $result;
    }


    public function saveDetailsTarif($detailsTarif)
    {
        return $this->dTarifmanager->saveObject($detailsTarif);
    }
    /**
     * @param array         $aParams
     * @param Course|null   $course
     * @param int|null      $idTarif
     *
     * @return array
     */
    public function getTarifCourse(array $aParams, Course $course = null, $idTarif = null)
    {
        if (!$course instanceof Course && count($aParams) === 0) {
            return array(
                'status' => 0,
                'response' => 'The first or the second parameter shouldn\'t be null.',
            );
        } elseif (count($aParams) === 0) {
            /** @var array $aParams */
            $aParams = $this->getDetailsCourse($course);

            if ($aParams === false) {
                return array(
                    'status' => 0,
                    'response' => 'Can\'t find company for this course',
                );
            }
        }

        if ($idTarif) {
            // Type vehicle is getted (from api course/new)
            $result = $this->loadTarif($idTarif);
        } else {
            // No vehicle type is getted.
            $result = $this->getTarifBy('company', $aParams['companyId']);
        }

        $sTimeDep = strtotime($aParams['timeDeparture']);

        if (strpos($aParams['estimatedDistance'], ' ') !== false) {
            list($intDistance, $unite) = explode(' ', $aParams['estimatedDistance']);
        } else {
            $intDistance = $aParams['estimatedDistance'];
            $unite = 'km';
        }

        if ($result instanceof Tarif) {
            // Type vehicle is getted (from api course/new)
            /** @var Tarif $item */
            $item = $result;
        } else {
            /** @var Tarif $item */
            $item = $result[0];
        }

        $basicTarif = 0;

        //get the applicable tarif (tarif PC or tarif A or tarif B or tarif C or tarif D)
        $oTypeTarifNight = $item->getHourNightTarif();

        $fromNightTarif = $sTimeDep;
        $toNightTarif = $sTimeDep;
        if ($oTypeTarifNight->getFrom() && $oTypeTarifNight->getTo()) {
            $fromNightTarif = strtotime($oTypeTarifNight->getFrom()->format('H:i:s'));
            $toNightTarif = strtotime($oTypeTarifNight->getTo()->format('H:i:s'));
        }

        $aHolidays = $this->getHolidays();
        list($timestampDeparture, $oDate) = $this->checkHolidays($aParams['dateDeparture']);
        // if the time of course is in night or is holiday
        if (($fromNightTarif > $sTimeDep && $sTimeDep < $toNightTarif) || in_array($timestampDeparture, $aHolidays)
            || $oDate->format('N') == 7
        ) {
            $basicTarif += $item->getTarifD();
        } else {
            $basicTarif += $item->getTarifC();
        }


        // calcul tarif (price * distance)
        $pricePerDistance = $basicTarif * $intDistance;
        $basicTarif = $pricePerDistance;
        $priceFromCompanyToDepAddr = 0;

        /** @var Company $oCompany */
        $oCompany = $item->getCompany();
        // Calcul of tarif from the company address to the depart address
        if ($course !== null && $course->getDriver() !== null) {
            //get the driver geolocation
            $gelocationDriver = $this->geolocationManager->getLastGeolocation('driver', $course->getDriver()->getId());
            $distanceFromCompany = 0;
            if ($gelocationDriver) {
                $paramLatLong = $gelocationDriver->getLatitude() . ',' . $gelocationDriver->getLongitude();

                $courseLatLongDep = $course->getLatDep() . ',' . $course->getLongDep();
                $courseLatLongArr = $course->getLatArr() . ',' . $course->getLongArr();

                list($distanceFromCompany, $durationFromCompany) = $this->geolocationHandler->getDistanceMatrix(
                    $paramLatLong, $course->getArrivalAddress(), $paramLatLong, $courseLatLongArr
                );
                $course->setApproachTime($durationFromCompany);
            }
        } else {
            /** @var Company $oCompany */
            $oCompany = $item->getCompany();
            //$paramLatLong = $oCompany->getLat() . ',' . $oCompany->getLng();
            if ($oCompany->getAddress() != null ) {
                list($distanceFromCompany, $durationFromCompany) = $this->geolocationHandler->getDistanceMatrixCommand(
                        $oCompany->getAddress(), $aParams['departureAddress']
                );
            }
        }

        $priceFromCompanyToDepAddr = $distanceFromCompany * $item->getTarifA();
        $basicTarif += $priceFromCompanyToDepAddr;
        //add supportedTarif
        $basicTarif += $item->getTarifPC();

        //get the day from date
        /* $priceByDate = $this->increasePriceByDateTime($aParams['dateDeparture'], $item, $sTimeDep);
          $basicTarif += $basicTarif  + (($basicTarif * $priceByDate) / 100);
         */

        // add tarif laguage
        $laguagePrice = 0;
        if (array_key_exists('luggage', $aParams)) {
            $laguagePrice = $item->getLuggage() * $aParams['luggage'];
            $basicTarif += $laguagePrice;
        }
        // add tarif nbperson
        if (array_key_exists('nbPerson', $aParams)) {
            if ($aParams['nbPerson'] >= 3) {
                $basicTarif += $item->getPerson();
            }
        }

        // add tarif wheelchair
        $handicapWheelchairTarif = 0;
        if (array_key_exists('wheelchair', $aParams)) {
            $handicapWheelchairTarif = $item->getTarifHandicapWheelchair() * $aParams['wheelchair'];
            $basicTarif += $handicapWheelchairTarif;
        }

        // add tarif for baby seat departure
        $babySeatPrice = 0;
        if (array_key_exists('babySeat', $aParams)) {
            $babySeatPrice = $item->getTarifBabySeat() * $aParams['babySeat'];
            $basicTarif += $babySeatPrice;
        }
        $tax = $this->getTaxes($oCompany);

        $priceTTC = $basicTarif + ($basicTarif * $tax / 100);

        if ($priceTTC < $item->getMinPrice()) {
            $priceTTC = $item->getMinPrice();
            $basicTarif = $priceTTC - ($priceTTC * $tax / 100);
        }
        $aPrice = array(
            'priceBasically' => $item->getTarifPC(),
            'pricePerDistance' => $pricePerDistance,
            'pricePerDate' => 0,
            'pricePerPerson' => 0,
            'pricePerLuggage' => $laguagePrice,
            'pricePerWheelChair' => $handicapWheelchairTarif,
            'pricePerBabySeat' => $babySeatPrice,
            'priceFromCompanyToDepAddr' => $priceFromCompanyToDepAddr,
            'priceTTC' => $priceTTC,
            'totalPrice' => $basicTarif,
            'isForfait' => 0,
        );

        //Calcul tarif Back
        $aPriceBack = array();
        $priceBackTTC = 0;

        if (array_key_exists('estimatedDistanceBack', $aParams) && array_key_exists('nbPersonBack', $aParams) &&
                array_key_exists('dateDepartureBack', $aParams) && array_key_exists('timeDepartureBack', $aParams)
        ) {
            if ($aParams['estimatedDistanceBack'] != 0 && $aParams['nbPersonBack'] != 0
                && $aParams['dateDepartureBack'] != 0 && $aParams['timeDepartureBack'] != 0
            ) {
                $resultRet = $this->getTarifBy('company', $aParams['companyId']);
                $item = $resultRet[0];
                $sTimeBack = strtotime($aParams['timeDepartureBack']);
                list($timestampDepartureBack, $oDateBack) = $this->checkHolidays($aParams['dateDepartureBack']);
                if (($fromNightTarif > $sTimeBack && $sTimeBack < $toNightTarif) || in_array(
                                $timestampDepartureBack, $aHolidays
                        ) || $oDateBack->format('N') == 7
                ) {
                    $basicTarifBack = $item->getTarifD();
                } else {
                    $basicTarifBack = $item->getTarifC();
                }

                if (strpos($aParams['estimatedDistanceBack'], ' ') !== false) {
                    list($intDistanceBack, $unite) = explode(' ', $aParams['estimatedDistanceBack']);
                } else {
                    $intDistanceBack = $aParams['estimatedDistanceBack'];
                    $unite = 'km';
                }

                // calcul tarif (price * distance)
                $pricePerDistanceBack = $basicTarifBack * $intDistanceBack;
                $basicTarifBack = $pricePerDistanceBack;
                if ($oCompany->getAddress() != null) {
                    $paramLatLong = $oCompany->getLat() . ',' . $oCompany->getLng();
                    $courseLatLongArr = $course->getLatArr() . ',' . $course->getLongArr();
                    list($distanceFromCompanyBack, $durationFromCompanyBack) = $this->geolocationHandler->getDistanceMatrix(
                        $oCompany->getAddress(), $aParams['departureAddressRet'], $paramLatLong, $courseLatLongArr
                    );

                    $priceFromCompanyToDepAddrBack = $distanceFromCompanyBack * $item->getTarifA();
                    $basicTarifBack += $priceFromCompanyToDepAddrBack;
                }

                //get the day from date
                /* if (!$aParams['dateDepartureBack']) {
                  $priceByDateBack = $this->increasePriceByDateTime($aParams['dateDepartureBack'], $item, $sTimeBack);
                  $basicTarifBack = $basicTarifBack + (($basicTarifBack * $priceByDateBack) / 100);
                  } else {
                  $priceByDateBack = 0;
                  $basicTarifBack = 0;
                  }
                 */

                // add tarif laguage back
                $laguagePriceback = 0;
                $laguagePriceback = $item->getLuggage() * $aParams['luggageBack'];
                $basicTarifBack += $laguagePriceback;

                // add tarif nbperson back
                if ($aParams['nbPersonBack'] >= 4) {
                    $basicTarifBack += $item->getPerson();
                }

                // add tarif wheelchair back
                $handicapWheelchairTarifBack = 0;
                if (array_key_exists('wheelchair_back', $aParams)) {
                    $handicapWheelchairTarifBack = $item->getTarifHandicapWheelchair() * $aParams['wheelchair_back'];
                    $basicTarifBack += $handicapWheelchairTarifBack;
                }

                // add tarif for baby seat  back
                $babySeatPriceback = 0;
                if (array_key_exists('babySeat_back', $aParams)) {
                    $babySeatPriceBack = $item->getTarifBabySeat() * $aParams['babySeat_back'];
                    $basicTarifBack += $babySeatPriceBack;
                }

                if ($basicTarifBack < $item->getMinPrice()) {
                    $basicTarifBack = $item->getMinPrice();
                }
                $priceBackTTC = $basicTarifBack + ($basicTarifBack * $tax->getValue() / 100);

                if ($priceBackTTC < $item->getMinPrice()) {
                    $priceBackTTC = $item->getMinPrice();
                    $basicTarifBack = $priceBackTTC - ($priceBackTTC * $tax->getValue() / 100);
                }

                $basicTarif += $basicTarifBack;
                $aPriceBack = array(
                    'priceBasically' => $item->getTarifPC(),
                    'pricePerDistance' => $pricePerDistanceBack,
                    'pricePerDate' => 0,
                    'pricePerPerson' => 0,
                    'pricePerLuggage' => $laguagePriceback,
                    'pricePerWheelChair' => $handicapWheelchairTarifBack,
                    'pricePerbabyseat' => $babySeatPriceBack,
                    'priceFromCompanyToDepAddrBack' => $priceFromCompanyToDepAddrBack,
                    'priceBackTTC' => $priceBackTTC,
                    'totalPrice' => $basicTarifBack,
                    'isForfait' => 0,
                );
            }
        }

        $priceTotalTTC = $priceBackTTC + $priceTTC;

        $hoursdeparture = date($aParams['timeDeparture']); #recuperer l'heure de prise en charge

        $morningTime = '07:00';
        $morningTime2 = '09:00';
        $afternoonTime = '16:30';
        $afternoonTime2 = '18:30';
        $nighTime = '19:00';
        $nightTime2 = '22:00';
        $augmentation = null;
        if ($hoursdeparture >= $morningTime && $hoursdeparture <= $morningTime2) {
            $augmentation = 0.4;
        } else if ($hoursdeparture >= $afternoonTime && $hoursdeparture <= $afternoonTime2) {
            $augmentation = 0.4;
        } else if ($hoursdeparture >= $nighTime && $hoursdeparture <= $nightTime2) {
            $augmentation = 0.3;
        } else {
            $augmentation = 0.15;
        }
        $estimatedPrice = $priceTotalTTC + $priceTotalTTC * $augmentation;

        if ($course !== null) {
            // for create course from app phone
            $course->setApplicableTarif($item);
            $course->setPriceHT(number_format($basicTarif, 0, '.', ''));
            $course->setPriceTTC(number_format($priceTTC, 0, '.', ''));

            // Details tarif
            $detailsTarif = array(
                'tarifTotalHT' => number_format($basicTarif, 0, '.', ''),
                'tarifTotalTTC' => number_format($priceTTC, 0, '.', ''),
                'tarifHT' => number_format($basicTarif, 0, '.', ''),
                'tarifApproachTime' => number_format($priceFromCompanyToDepAddr, 0, '.', ''),
                'estimatedTarif' => number_format($estimatedPrice, 0, '.', ''),
                'isForfait' => 0,
            );

            $aDetailsTarif = $this->dTarifmanager->saveObject($detailsTarif);
            if ($aDetailsTarif['status']) {
                $course->setCourseDetailsTarif($aDetailsTarif['response']);
            }

            return $course;
        }

        return array(
            'price_total' => number_format($basicTarif, 2, '.', ''),
            'price_total_ttc' => number_format($priceTotalTTC, 2, '.', ''),
            'details' => array(
                'departure' => $aPrice,
                'back' => $aPriceBack,
            ),
            'tarif_id' => $item->getId(),
            'estimated_tarif' => number_format($estimatedPrice, 2, '.', ''),
        );
    }

    /**
     * Save Tarif entity
     *
     * @param Tarif $tarif
     */
    public function saveTarif(Tarif $tarif) {
        $this->persistAndFlush($tarif);
    }

    /**
     * @return \CAB\CourseBundle\Entity\TarifRepository
     */
    public function getRepository() {
        return $this->em->getRepository('CABCourseBundle:Tarif');
    }

    /**
     * Calcul tarif of course in finalizebookingstep2Action (course controller)
     *
     * @param array  $params
     * @param Course $course
     *
     * @return array
     */
    public function calculTarifCourseFront(array $params, Course $course) {
        // 1. Calclu tarif single
        $aTarifSingle = $this->calculTarifSingle($params, $course);
        //2. Calcul tarif back
        $aTarifBack['basicTarif'] = 0;
        $aTarifBack['laguagePrice'] = 0;
        $aTarifBack['equipmentPrice'] = 0;
        $aTarifBack['handicapWheelchairTarif'] = 0;
        $aTarifBack['babySeatPrice'] = 0;
        if ($course->getTargetType()) {
            $aTarifBack = $this->calculTarifSingle($params, $course, 'back');
        }
        $totalTarif = $aTarifSingle['basicTarif'] + $aTarifBack['basicTarif'];
        if ($totalTarif < $aTarifSingle['item']->getMinPrice()) {
            $totalTarif = $aTarifSingle['item']->getMinPrice();
        }

        $taxcompany = $course->getCompany()->getId();
        $taxdata = $this->getTax($taxcompany);

        if (count($taxdata)) {
            foreach ($taxdata as $item) {
                $tax = $item->getValue();
                $taxname = $item->getTaxName();
            }
        } else {
            $tax = 0;
            $taxname = 'No tax';
        }

        //calcul tarif HT
        $totalTarifHT = $totalTarif - (($totalTarif * $tax) / 100);


        $taxcompany = $course->getCompany()->getId();
        $taxdata = $this->taxManager->loadTax($taxcompany);

        if (count($taxdata) > 1 ) {

            if (count($taxdata)) {
                foreach ($taxdata as $item) {
                    $tax = $item->getValue();
                    $taxname = $item->getTaxName();
                }
            } else {
                $tax = 0;
                $taxname = 'No tax';
            }
            $totalTarifHT = $totalTarif - (($totalTarif * $tax) / 100);

        }else {
            $totalTarifHT = $totalTarif ;
        }



        $tarifBackHt = $aTarifBack['basicTarif'];

        if (count($taxdata) > 1 ) {
        if ($aTarifBack['basicTarif'] > ($totalTarif * $tax) / 100) {
            $tarifBackHt = $aTarifBack['basicTarif'] - (($totalTarif * $tax) / 100);
        }else{
            $tarifBackHt = $aTarifBack['basicTarif'];
        }
        }



        return array(
            'tarifTotalHT' => $totalTarifHT,
            'tarifTotalTTC' => $totalTarif,
            'tarifHT' => $totalTarifHT,
            'tarifBackHT' => $tarifBackHt,
            'tarifLuggage' => $aTarifSingle['laguagePrice'],
            'tarifEquipment' => $aTarifSingle['equipmentPrice'],
            'tarifHandicapWheelchair' => $aTarifSingle['handicapWheelchairTarif'],
            'tarifBabySeat' => $aTarifSingle['babySeatPrice'],
            'tarifLuggageBack' => $aTarifBack['laguagePrice'],
            'tarifEquipmentBack' => $aTarifBack['equipmentPrice'],
            'tarifHandicapWheelchairBack' => $aTarifBack['handicapWheelchairTarif'],
            'tarifBabySeatBack' => $aTarifBack['babySeatPrice'],
            'tax' => $tax,
            'taxname' => $taxname,
        );
    }

    /**
     * @param $dateDep
     * @param $item
     * @param $sTimeDep
     *
     * @return mixed
     */
    protected function increasePriceByDateTime($dateDep, $item, $sTimeDep) {
        $datetime = \DateTime::createFromFormat('Y-m-d', $dateDep);
        $day = $datetime->format('l');

        // increase price by day
        $fDayFromAttribute = 'get' . $day . 'MorningFrom';
        $fDayToAttribute = 'get' . $day . 'MorningTo';

        $fNightFromAttribute = 'get' . $day . 'MorningFrom';
        $fNightToAttribute = 'get' . $day . 'MorningTo';
        $fDayIncreaseAttribute = 'get' . $day . 'MorningIncrease';
        $fNightIncreaseAttribute = 'get' . $day . 'NightIncrease';

        $timeFrom = strtotime($item->$fDayFromAttribute()->format('H:i:s'));
        $timeTo = strtotime($item->$fDayToAttribute()->format('H:i:s'));
        $timeFromNight = strtotime($item->$fNightFromAttribute()->format('H:i:s'));
        $timeToNight = strtotime($item->$fNightToAttribute()->format('H:i:s'));

        $tarifByDate = 0;
        if ($sTimeDep > $timeFrom && $sTimeDep < $timeTo) {
            $tarifByDate = $item->$fDayIncreaseAttribute();
        } elseif ($sTimeDep > $timeFromNight && $sTimeDep < $timeToNight) {
            $tarifByDate = $item->$fNightIncreaseAttribute();
        }

        return $tarifByDate;
    }

    /**
     * Method description
     *
     * @param array $params
     * @param Course $course
     *
     * @return array
     */
    protected function calculTarifSingle(array $params, Course $course, $opt = 'dep') {
        if ($course->getApplicableTarif()) {
            $item = $course->getApplicableTarif();
        } else {
            $result = $this->getTarifBy('company', $course->getCompany());
            $item = $result[0];
        }

        $attrLuggage = $opt == 'dep' ? 'luggage_dep' : 'luggage_back';
        $attrEquipment = $opt == 'dep' ? 'equipment_dep' : 'equipment_back';
        $attrBabySeat = $opt == 'dep' ? 'baby_seat_dep' : 'baby_seat_back';
        $attrWheelchair = $opt == 'dep' ? 'handicap_wheelchair_dep' : 'handicap_wheelchair_back';
        $basicTarif = $course->getPriceTTC();
        $handicapWheelchairTarif = 0;
        // add tarif laguage departure
        $laguagePrice = $item->getLuggage() * $params[$attrLuggage];
        // add tarif for equipement departure
        $equipmentPrice = $item->getTarifEquipment() * $params[$attrEquipment];
        // add tarif for baby seat departure
        $babySeatPrice = $item->getTarifBabySeat() * $params[$attrBabySeat];
        // add tarif for handicap wheelchair departure
        if ($course->getIsHandicap()) {
            $handicapWheelchairTarif = $item->getTarifHandicapWheelchair() * $params[$attrWheelchair];
        }
        $basicTarif += $laguagePrice + $equipmentPrice + $babySeatPrice + $handicapWheelchairTarif;

        return array(
            'basicTarif' => $basicTarif,
            'handicapWheelchairTarif' => $handicapWheelchairTarif,
            'laguagePrice' => $laguagePrice,
            'equipmentPrice' => $equipmentPrice,
            'babySeatPrice' => $babySeatPrice,
            'item' => $item,
        );
    }

    /**
     * Cette fonction retourne un tableau de timestamp correspondant
     * aux jours fériés en France pour une année donnée.
     *
     * @param null $year
     *
     * @return array
     */
    protected function getHolidays($year = null) {
        if ($year === null) {
            $year = intval(date('Y'));
        }

        $easterDate = easter_date($year);
        $easterDay = date('j', $easterDate);
        $easterMonth = date('n', $easterDate);
        $easterYear = date('Y', $easterDate);

        $holidays = array(
            mktime(0, 0, 0, 1, 1, $year), // 1er janvier
            mktime(0, 0, 0, 5, 1, $year), // Fête du travail
            mktime(0, 0, 0, 5, 8, $year), // Victoire des alliés
            mktime(0, 0, 0, 7, 14, $year), // Fête nationale
            mktime(0, 0, 0, 8, 15, $year), // Assomption
            mktime(0, 0, 0, 11, 1, $year), // Toussaint
            mktime(0, 0, 0, 11, 11, $year), // Armistice
            mktime(0, 0, 0, 12, 25, $year), // Noel
            mktime(0, 0, 0, $easterMonth, $easterDay + 1, $easterYear),
            mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear),
            mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear),
        );

        sort($holidays);

        return $holidays;
    }

    /**
     * Method description
     *
     * @param string $depDate
     *
     * @return array
     */
    protected function checkHolidays($depDate) {
        list($yearDep, $monthDep, $dayDep) = explode('-', $depDate);
        $timestampDeparture = mktime(0, 0, 0, $monthDep, $dayDep, $yearDep);

        //check if the day is sunday (7)
        $oDate = \DateTime::createFromFormat('Y-m-d', $depDate);

        return array($timestampDeparture, $oDate);
    }

    /**
     * Method description
     *
     * @return Company
     */
    protected function getTax($taxcompany) {
        $tax = $this->taxManager->loadTax($taxcompany);

        return $tax;
    }

    public function getDetailsCourse(Course $course, $command = false) {
        if ($course->getCompany()) {
            $aParams['companyId'] = $course->getCompany();
            if ($course->getCompany()->getIsBusiness()) {
                $adminCompanyUser = $course->getCompany()->getContact();
                $companyParent = $adminCompanyUser->getContacts()->first();
                $aParams['companyId'] = $companyParent;
            }
        } elseif (!$command) {
            return false;
        }

        $courseLatLongDep = $course->getLatDep() . ',' . $course->getLongDep();
        $courseLatLongArr = $course->getLatArr() . ',' . $course->getLongArr();

        $aParams['timeDeparture'] = $course->getDepartureTime()->format('H:i:s');
        $aParams['dateDeparture'] = $course->getDepartureDate()->format('Y-m-d');
        $aParams['departureAddress'] = $course->getStartedAddress();

        $aParams['longArr'] = $course->getLongArr();
        $aParams['latArr'] = $course->getLatArr();

        $aParams['nbPerson'] = $course->getPersonNumber();
        list($distance, $duration) = $this->geolocationHandler->getDistanceMatrix(
            $course->getStartedAddress(), $course->getArrivalAddress(), $courseLatLongDep, $courseLatLongArr
        );

        $aParams['estimatedDistance'] = $distance;
        $aParams['duration'] = $duration;

        $course->setCourseTime($duration / 60);
        $course->setDistance($distance);

        return $aParams;
    }

    /**
     * @param $oCompany
     * @return int|string
     */
    private function getTaxes($oCompany)
    {
        $tax = 0;
        if ($oCompany) {
            $taxesCompany = $oCompany->getTaxes();
            /** @var Tax $taxCompany */
            if (count($taxesCompany)) {
                foreach ( $taxesCompany as $taxCompany ) {
                    $tax += $taxCompany->getValue();
                }
                return $tax;
            }
            return $tax;
        }
        return $tax;
    }

}
