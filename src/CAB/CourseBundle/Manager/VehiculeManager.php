<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;


use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Manager\BaseManager;
use CAB\CourseBundle\Entity\Vehicule;

class VehiculeManager extends BaseManager
{
    protected $em;

    private $tarifManager;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, TarifManager $tarifManager)
    {
        $this->em = $em;
        $this->tarifManager = $tarifManager;
    }

    /**
     * @param integer $idVehicle
     *
     * @return Vehicule
     */
    public function loadVehicle($idVehicle)
    {
        return $this->getRepository()->find($idVehicle);
    }

    /**
     * @param string $long
     * @param string $lat
     * @param string $nbPerson
     * @param string $distance
     *
     * @return array
     */
    public function getVehiclesByLangLat($long, $lat, $nbPerson, $distance)
    {
        return $this->getRepository()
            ->findByLangLat($long, $lat, $nbPerson, $distance);
    }

    /**
     * @param string $long
     * @param string $lat
     * @param string $nbPerson
     *
     * @return array
     */
    public function getPriceCourseByVehicle($long, $lat, $nbPerson, $typeVehicle = null, $withBack = 0)
    {
        return $this->getRepository()->getPriceByVehicle($long, $lat, $nbPerson, $typeVehicle);
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return array|\CAB\CourseBundle\Entity\VehiculeModel[]|\CAB\CourseBundle\Entity\Zone[]
     */
    public function getVehiclesBy($criteria, $creteriaValue)
    {
        $return = array();

        if (property_exists(Vehicule::class, $criteria)) {
            $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));
            foreach ($result as $item) {
                $return[] = array('val' => $item->getId(), 'text' => $item->getVehiculeName());
            }
        }

        return $return;
    }

    /**
     * Save Vehicule entity
     *
     * @param Vehicule $vehicle
     */
    public function saveVehicle(Vehicule $vehicle)
    {
        $this->persistAndFlush($vehicle);
    }

    /**
     * @return \CAB\CourseBundle\Entity\VehiculeRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Vehicule');
    }
}