<?php

namespace CAB\CourseBundle\Manager;

use CAB\CourseBundle\Entity\CommandSncf;
use Doctrine\ORM\EntityManager;

/**
 * CommandSncfManager
 *
 * Add your own custom manager methods below.
 */
class CommandSncfManager extends BaseManager
{
    protected $entityClass;

    /**
     * @param EntityManager $em
     * @param string        $entityClass
     */
    public function __construct(EntityManager $em, $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
    }

    /**
     * @param int $commandSncfId
     *
     * @return CommandSncfManager
     */
    public function loadCommandSncfManager($commandSncfId)
    {
        return $this->getRepository()->find($commandSncfId);
    }

    /**
     * @param CommandSncfManager $commandSncf
     * @param string     $attr
     * @param int|string $value
     */
    public function setAttr(CommandSncfManager $commandSncf, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $commandSncf->$attributeSetter($value);
        $this->saveCommandSncfManager($commandSncf);
    }

    /**
     * Save CommandSncfManager entity
     *
     * @param CommandSncfManager $commandSncf
     */
    public function saveCommandSncfManager(CommandSncfManager $commandSncf)
    {
        $this->persistAndFlush($commandSncf);
    }

    /**
     * @return //CAB\CourseBundle\Entity\CommandSncfRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository($this->entityClass);
    }

	/**
	 * Create a new CommandSncf object
	 *
	 * @param array $params
	 *
	 * @return \CAB\CourseBundle\Entity\CommandSncf
	 */
	public function createCommandSncf(array $params)
    {
    	$commandSncf = new CommandSncf();
	    $aProperties = $this->getClassProperties(CommandSncf::class);
	    foreach ($params as $attr => $value) {
		    if ($value !== null && in_array($attr, $aProperties)) {
			    $attributeSetter = 'set'.ucfirst($attr);
			    $commandSncf->$attributeSetter($value);
		    }
	    }

	    $this->persistAndFlush($commandSncf);
	    return $commandSncf;
    }
    /**
     * Persist and flush entity
     *
     * @param $entity
     */
    protected function persistAndFlush($entity, $dispatchEvent = true)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

}
