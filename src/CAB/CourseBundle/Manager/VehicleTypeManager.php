<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 29/12/2015
 * Time: 21:55
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\TypeVehicule;

/**
 * Class VehicleType
 *
 * @package CAB\CourseBundle\Manager
 */
class VehicleTypeManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param integer $objectID
     *
     * @return TypeVehicule object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()->find($objectID);
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return array
     */
    public function getVehicleTypeBy($criteria, $creteriaValue)
    {
        $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));

        return $result;
    }
    /**
     * @return Collection of TypeVehicule object
     */
    public function loadAllObjects()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Save TypeVehicule entity
     *
     * @param array $params
     *
     * @return array
     */
    public function saveObject(array $params)
    {
        $oTypeVehicule = new TypeVehicule();

        foreach ($params as $key => $value) {
            $mSetter = 'set'.ucfirst($key);
            $oTypeVehicule->$mSetter($value);
        }
        try {
            $this->persistAndFlush($oTypeVehicule);

            return array(
                'status' => true,
                'response' => $oTypeVehicule,
                );
        } catch (\Exception $e) {
            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * @return \CAB\CourseBundle\Entity\TypeVehiculeRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:TypeVehicule');
    }

}