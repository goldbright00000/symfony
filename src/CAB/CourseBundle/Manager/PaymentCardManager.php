<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 01/02/2016
 * Time: 00:16
 */

namespace CAB\CourseBundle\Manager;


use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use CAB\CourseBundle\Entity\PaymentCard;

/**
 * Class PaymentCardManager
 *
 * @package CAB\CourseBundle\Manager
 */
class PaymentCardManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param EntityManager   $em     entity manager
     * @param LoggerInterface $logger logger
     */
    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @param integer $objectID
     *
     * @return PaymentCard object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()
            ->findOneBy(array('id' => $objectID));
    }

    /**
     * @param string  $criteria
     * @param integer $creteriaValue
     *
     * @return null|array
     */
    public function getCardBy($criteria, $creteriaValue)
    {
        $aProperties = $this->getClassProperties('CAB\CourseBundle\Entity\PaymentCard');
        foreach($aProperties as $prop) {
            $this->logger->notice('CAB/CourseBundle/Manager/PaymentCardManager.php Property class : '.$prop);
        }
        $result = null;
        if (in_array($criteria, $aProperties)) {
            $this->logger->notice('CAB/CourseBundle/Manager/PaymentCardManager.php criteria : '.$criteria.' is a prperty');
            try {
                $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));
            } catch(\Exception $e) {
                $this->logger->notice('CAB/CourseBundle/Manager/PaymentCardManager.php end criteria : '.$e->getMessage());
            }
            $this->logger->notice('CAB/CourseBundle/Manager/PaymentCardManager.php end criteria : '.$criteria);
        }

        return $result;
    }

    /**
     * @return Collection of PaymentCard object
     */
    public function loadAllObjects()
    {
        return $this->getRepository()->findAll();
    }


    /**
     * Method description
     *
     * @param array $params
     * @param null  $oPaymentCard
     *
     * @return array
     */
    public function saveObject(array $params, $oPaymentCard = null)
    {
        $this->logger->info('CAB/CourseBundle/Manager/PaymentCardManager.php saveobject : ');
        if ($oPaymentCard === null) {
            $oPaymentCard = new PaymentCard();
        }
        $aProperties = $this->getClassProperties('CAB\CourseBundle\Entity\PaymentCard');

        foreach ($aProperties as $pr) {
            $this->logger->info('CAB/CourseBundle/Manager/PaymentCardManager.php property : '.$pr);
        }
        try {
            foreach ($params as $key => $value) {
                if (in_array($key, $aProperties)) {
                    $this->logger->info('CAB/CourseBundle/Manager/PaymentCardManager.php key : '.$key);

                    $mSetter = 'set'.ucfirst($key);
                    $oPaymentCard->$mSetter($value);
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('CAB/CourseBundle/Manager/PaymentCardManager.php Exception1 : '.$e->getMessage());
        }

        try {
            $this->persistAndFlush($oPaymentCard);

            $this->logger->info('CAB/CourseBundle/Manager/PaymentCardManager.php $oPaymentCard success: ');

            return array(
                'status' => true,
                'response' => $oPaymentCard,
                );
        } catch (\Exception $e) {
            $this->logger->error('CAB/CourseBundle/Manager/PaymentCardManager.php Exception : '.$e->getMessage());

            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }

    }

    /**
     * @return \CAB\CourseBundle\Entity\PaymentCardRepository
     */
    public function getRepository()
    {
        return $this->dm->getRepository('CABCourseBundle:PaymentCard');
    }
}