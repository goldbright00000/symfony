<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 10/01/2016
 * Time: 23:55
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\TransactionResponse;
use Psr\Log\LoggerInterface;

/**
 * Class TransactionPaymentManager
 *
 * @package CAB\CourseBundle\Manager
 */
class TransactionResponseManager extends BaseManager
{
    protected $em;

    protected $logger;

    /**
     * Constructor
     *
     * @param EntityManager   $em     entity manager
     * @param LoggerInterface $logger logger
     */
    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * Find one object
     *
     * @param integer $objectID id object
     *
     * @return TransactionResponse object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()->find($objectID);
    }

    /**
     * Find transaction response by criteria
     *
     * @param string  $criteria      the name of class attribute
     * @param integer $creteriaValue the value
     *
     * @return array
     */
    public function getTransactionBy($criteria, $creteriaValue)
    {
        $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));

        return $result;
    }

    /**
     * Find all objects
     *
     * @return Collection of TransactionResponse object
     */
    public function loadAllObjects()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Save TransactionResponse entity
     *
     * @param array $params class attribute
     *
     * @return array
     */
    public function saveObject(array $params)
    {
        $oTransactionResponse = new TransactionResponse();
        foreach ($params as $key => $value) {
            try {
                $mSetter = 'set'.ucfirst($key);
                $oTransactionResponse->$mSetter($value);
            } catch (\Exception $e) {
                $this->logger->error('set attribute '.$e->getMessage());

                return array(
                    'status' => false,
                    'response' => $e->getMessage(),
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                );
            }
        }
        try {
            $this->persistAndFlush($oTransactionResponse);

            return $oTransactionResponse;
        } catch (\Exception $e) {
            $this->logger->error('save transaction-----'.$e->getMessage());

            return array(
                'status' => false,
                'response' => $e->getMessage(),
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            );
        }
    }


    /**
     * Method description
     *
     * @param array $params
     *
     * @return array|TransactionResponse
     */
    public function createTransaction($params = array())
    {
        $this->logger->notice('CAB/CourseBundle/Manager/TransactionResponseManager.php: Create transaction response');
        try {
            if (!empty($params)) {
                $result = $this->saveObject($params);

                return $result;
            } else {
                $this->logger->notice('CAB/CourseBundle/Manager/TransactionResponseManager.php empty params');

                return new TransactionResponse();
            }
        } catch (\Exception $e) {
            $this->logger->error('Create transaction response : '.$e->getMessage());

            return array(
                'status' => false,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            );
        }
    }

    /**
     * Setting attribute
     *
     * @param TransactionResponse $transaction object
     * @param string              $attr        attr
     * @param string              $value       value
     * @param boolean             $merge       merge|persist
     *
     * @throws exception
     * @return mixte
     */
    public function setAttr(TransactionResponse $transaction, $attr, $value, $merge = false)
    {
        try {
            $attributeSetter = 'set'.ucfirst($attr);
            $transaction->$attributeSetter($value);

            if ($merge) {
                $this->mergeAndFlush($transaction);
            } else {
                $this->persistAndFlush($transaction);
            }
        } catch (\Exception $e) {
            $this->logger->error('save attribute transaction : '.$e->getMessage());

            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * Repository
     *
     * @return \CAB\CourseBundle\Entity\TransactionResponseRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:TransactionResponse');
    }
}