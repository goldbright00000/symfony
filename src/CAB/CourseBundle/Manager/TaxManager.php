<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 29/02/2016
 * Time: 23:26
 */

namespace CAB\CourseBundle\Manager;


use CAB\CourseBundle\Entity\Tax;
use Doctrine\ORM\EntityManager;

class TaxManager extends BaseManager
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $taxId
     *
     * @return Tax
     */
    public function loadTax($taxcompany)
    {
        $oTax =  $this->getRepository()->findByCompany($taxcompany);

        return $oTax;
    }

    /**
     * @param Tax    $tax
     * @param string     $attr
     * @param int|string $value
     */
    public function setAttr(Tax $tax, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $tax->$attributeSetter($value);
        $this->saveTax($tax);
    }

    /**
     * Save Tax entity
     *
     * @param Tax $tax
     */
    public function saveTax(Tax $tax)
    {
        $this->persistAndFlush($tax);
    }

    /**
     * @return \CAB\CourseBundle\Entity\TaxRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:Tax');
    }

}