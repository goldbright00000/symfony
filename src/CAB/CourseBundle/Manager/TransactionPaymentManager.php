<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 10/01/2016
 * Time: 23:55
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use CAB\CourseBundle\Entity\TransactionPayment;
use Psr\Log\LoggerInterface;

/**
 * Class TransactionPaymentManager
 *
 * @package CAB\CourseBundle\Manager
 */
class TransactionPaymentManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Constructor
     *
     * @param EntityManager   $em     entity manager
     * @param LoggerInterface $logger logger
     */
    public function __construct(EntityManager $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * Find one object
     *
     * @param integer $objectID id object
     *
     * @return TransactionPayment object
     */
    public function loadObject($objectID)
    {
        return $this->getRepository()->find($objectID);
    }

    /**
     * Find transaction by criteria
     *
     * @param string  $criteria      the name of class attribute
     * @param integer $creteriaValue the value
     *
     * @return array
     */
    public function getTransactionBy($criteria, $creteriaValue, $one = false)
    {
        $result = null;
        if ($one){
            $result = $this->getRepository()->findOneBy(array($criteria => $creteriaValue));
        } else {
            $result = $this->getRepository()->findBy(array($criteria => $creteriaValue));
        }

        return $result;
    }

    /**
     * Find alll objects
     *
     * @return Collection of TransactionPayment object
     */
    public function loadAllObjects()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Save TransactionPayment entity
     *
     * @param array $params class attribute
     *
     * @return array
     */
    public function saveObject(array $params)
    {
        $aProperties = $this->getClassProperties('CAB\CourseBundle\Entity\TransactionPayment');
        //check if a transaction is already exists for this course
        $oCourse = $params['transactionPaymentCourse'];
        $oTransaction = $this->getTransactionBy('transactionPaymentCourse', $oCourse->getId(), true);
        if (!$oTransaction instanceof TransactionPayment) {
            $oTransaction = new TransactionPayment();
            foreach ($params as $key => $value) {
                if (in_array($key, $aProperties)) {
                    $mSetter = 'set'.ucfirst($key);
                    $oTransaction->$mSetter($value);
                }
            }
            try {
                $this->persistAndFlush($oTransaction);

                return array(
                    'status' => true,
                    'response' => $oTransaction,
                );
            } catch (\Exception $e) {
                echo $e->getMessage();
                echo '...'.$e->getFile();
                echo '...'.$e->getLine();
                $this->logger->error('save transaction-----'.$e->getMessage());

                return array(
                    'status' => false,
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                );
            }
        } else {
            return array(
                'status' => true,
                'response' => $oTransaction,
            );
        }
    }


    /**
     * Method description
     *
     * @param array $params
     *
     * @return array|TransactionPayment
     */
    public function createTransaction($params = array())
    {
        try {
            if (!empty($params)) {
                $result = $this->saveObject($params);

                return $result['response'];
            } else {
                return new TransactionPayment();
            }
            $this->logger->notice('Create transaction success-----');
        } catch (\Exception $e) {
            $this->logger->error('Create transaction-----'.$e->getMessage());

            return array(
                'status' => false,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            );
        }
    }

    /**
     * Setting attribute
     *
     * @param TransactionPayment $transaction object
     * @param string             $attr        attr
     * @param string             $value       value
     * @param boolean            $merge       merge|persist
     *
     * @throws exception
     * @return mixte
     */
    public function setAttr(TransactionPayment $transaction, $attr, $value, $merge = false)
    {
        try {
            $attributeSetter = 'set'.ucfirst($attr);
            $transaction->$attributeSetter($value);

            if ($merge) {
                $this->mergeAndFlush($transaction);
            } else {
                $this->persistAndFlush($transaction);
            }
        } catch (\Exception $e) {
            $this->logger->error('save attribute transaction-----'.$e->getMessage());

            return array(
                'status' => false,
                'response' => $e->getMessage(),
            );
        }
    }

    /**
     * Repository
     *
     * @return \CAB\CourseBundle\Entity\TransactionPaymentRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABCourseBundle:TransactionPayment');
    }
}