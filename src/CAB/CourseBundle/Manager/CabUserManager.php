<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 15:46
 */

namespace CAB\CourseBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use FOS\UserBundle\Doctrine\UserManager;
use Psr\Log\LoggerInterface;
use CAB\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;


class CabUserManager extends BaseManager
{
    protected $em;
    protected $logger;
    protected $userManager;

    /**
     * Constructor
     *
     * @param EntityManager   $em      entity manager
     * @param LoggerInterface $logger  logger
     * @param UserManager     $manager user manager
     */
    public function __construct(EntityManager $em, LoggerInterface $logger, UserManager $manager)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->userManager = $manager;
    }

    /**
     * Find user by Id, Use the default userManager injected to retreive the user
     * @param integer $userID
     *
     * @return UserInterface object
     */
    public function loadUser($userID)
    {
        return $this->userManager->findUserBy(array('id' => $userID));
    }

    /**
     * Find one user by the given criteria
     * @param string $criteria      attribute user class
     * @param string $creteriaValue value of attribute user class
     *
     * @return UserInterface
     */
    public function getUserBy($criteria, $creteriaValue)
    {
        // @TODO checkif $creteria is an attribute of tarif entity

        return $this->userManager->findUserBy(array($criteria => $creteriaValue));
    }

    /**
     * Find all users by the given role
     * @param string $role name of role like as ROLE_DRIVER
     *
     * @return Collection
     */
    public function getUsersByRole($rolee)
    {
        $users = $this->getRepository()->getUsersByRole($rolee);

        return $users->getQuery()->getResult();
    }

    /**
     * Qet query to find all users by the given role
     *
     * @param string $role name of role like as ROLE_DRIVER
     *
     * @return QueryBuilder
     */
    public function getUsersByRoleQuery($rolee)
    {
        return $this->getRepository()->getUsersByRole($rolee);
    }

    /**
     * Updates a user.
     *
     * @param UserInterface $user
     * @param Boolean       $andFlush Whether to flush the changes (default true)
    */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        $this->userManager->updateUser($user, $andFlush);
    }

    /**
     * @return \CAB\UserBundle\Entity\UserRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('CABUserBundle:User');
    }

    /**
     * Find all drivers available in the date and time given and at correspond localisation.
     *
     * @param \DateTime $date
     * @param \DateTime $time
     * @param float     $lng
     * @param float     $lat
     */
    public function findDriversForCourse(\DateTime $date, \DateTime $time, $lng, $lat)
    {
        //$this->getRepository()->findDriversForCourse($date, $time, $lng, $lat);
    }
}