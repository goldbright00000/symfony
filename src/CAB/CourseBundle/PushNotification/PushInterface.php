<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 26/01/2016
 * Time: 01:38
 */
namespace CAB\CourseBundle\PushNotification;

/**
 * Interface PushInterface
 *
 * @package CAB\CourseBundle\Composer
 */
interface PushInterface
{
    /**
     * Method description
     *
     * @param string $message
     * @param string $deviceIdentifier
     * @param array  $data
     *
     * @return mixed
     */
    public function setParams($message, $deviceIdentifier, array $data = array());

    /**
     * Method description
     *
     * @return mixed
     */
    public function sendMessage();
}