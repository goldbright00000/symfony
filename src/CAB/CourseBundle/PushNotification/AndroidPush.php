<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 26/01/2016
 * Time: 01:18
 */

namespace CAB\CourseBundle\PushNotification;


use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Service\OS\AndroidGCMNotification;
use Psr\Log\LoggerInterface;

class AndroidPush implements PushInterface
{

    private $rmsPushNotifications;

    private $message;

    private $logger;

    /**
     * @param AndroidGCMNotification $androidGcmNotification
     */
    public function __construct(AndroidGCMNotification $androidGcmNotification, LoggerInterface $logger)
    {
        $this->rmsPushNotifications = $androidGcmNotification;
        $this->logger = $logger;
        $this->message = new AndroidMessage();
    }

    /**
     * Method description
     *
     * @param string $type
     */
    public function setType($type = 'GCM')
    {
        $functionType = 'set'.$type;
        $this->message->$functionType(true);
    }

    /**
     * Method description
     *
     * @param string $message
     * @param string $deviceIdentifier
     * @param array  $data
     *
     * @return mixed
     */
    public function setParams($message, $deviceIdentifier, array $data = array())
    {
        $this->message->setData($data);
        $this->message->setMessage($message);
        $this->message->setDeviceIdentifier($deviceIdentifier);
    }

    /**
     * Method description
     *
     * @return bool
     */
    public function sendMessage()
    {
        try {
            $this->logger->info('CAB/CourseBundle/PushNotification/AndroidPush.php. getMessage : '.$this->message->getMessage());
            $this->logger->info('CAB/CourseBundle/PushNotification/AndroidPush.php. getTargetOS : '.$this->message->getTargetOS());
            $this->logger->info('CAB/CourseBundle/PushNotification/AndroidPush.php. getDeviceIdentifier : '.$this->message->getDeviceIdentifier());
            $this->logger->info('CAB/CourseBundle/PushNotification/AndroidPush.php. isGCM : '.$this->message->isGCM());
            $this->rmsPushNotifications->send($this->message);
            $result = $this->rmsPushNotifications->getResponses();
            $this->logger->info('CAB/CourseBundle/PushNotification/AndroidPush.php. rmpushnotification done : result =>  '.$result[0]->getContent());
        } catch (\Exception $e) {
            $this->logger->info('CAB/CourseBundle/PushNotification/AndroidPush.php. Exception : '.$e->getMessage());
        }

        return $result;
    }
}