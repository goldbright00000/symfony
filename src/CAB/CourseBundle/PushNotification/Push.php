<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 26/01/2016
 * Time: 02:28
 */

namespace CAB\CourseBundle\PushNotification;

use CAB\CourseBundle\Entity\Device;
use Psr\Log\LoggerInterface;

/**
 * Class Push
 *
 * @package CAB\CourseBundle\PushNotification
 */
class Push
{
    /**
     * @var
     */
    private $service;

    /**
     * @var IosPush
     */
    private $serviceIOS;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param AndroidPush     $ap
     * @param IosPush         $iosPush
     * @param LoggerInterface $logger
     */
    public function __construct(AndroidPush $ap, IosPush $iosPush, LoggerInterface $logger)
    {
        $this->service = $ap;
        $this->serviceIOS = $iosPush;
        $this->logger = $logger;
    }

    /**
     * Create instance
     *
     * @param Device $device
     * @param string $message
     * @param array  $data
     */
    public function instance(Device $device, $message, array $data, $pem = 'driver')
    {
        $deviceOS = $device->getOs();
        $this->logger->info('CAB/CourseBundle/PushNotification/Push.php. OS : '.$deviceOS);
        switch ($deviceOS) {
            case "android":
                try {
                    $this->logger->info('CAB/CourseBundle/PushNotification/Push.php. getDeviceToken'.$device->getDeviceToken());
                    $this->service->setParams($message, $device->getDeviceToken(), $data);
                    $this->logger->info('CAB/CourseBundle/PushNotification/Push.php. set parameters done');
                    $this->service->sendMessage();
                    $this->logger->info('CAB/CourseBundle/PushNotification/Push.php. send message done');
                } catch (\Exception $e) {
                    $this->logger->info('CAB/CourseBundle/PushNotification/Push.php. exception : '.$e->getMessage());
                }
                break;
            case "iOS":
                 //$message = $data;
                try {
                    $this->serviceIOS->setParams($message, $device->getDeviceToken(), $data, $pem);
                    $this->logger->info('message : ' . $message . ' - deviceTocken' . $device->getDeviceToken() . ' - pem : ' . $pem);
                    $this->serviceIOS->sendMessage();
                } catch (\Exception $e) {
                    $this->logger->info('CAB/CourseBundle/PushNotification/Push.php. exception : '.$e->getMessage());
                }
                break;
            case "windows":
                //$this->service = new ();
                break;
        }
    }

    /**
     * Create instance depending on operating system of the device.
     *
     * @param array  $devices
     * @param string $message
     * @param array  $data
     */
    public function getServiceByOs(array $devices, $message, array $data, $pem = 'driver')
    {
        foreach ($devices as $device) {
            $this->instance($device, $message, $data, $pem);
        }
    }
}
