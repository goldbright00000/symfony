<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 26/01/2016
 * Time: 01:18
 */

namespace CAB\CourseBundle\PushNotification;


use RMS\PushNotificationsBundle\Message\iOSMessage;
use Psr\Log\LoggerInterface;
use CAB\IosPushBundle\Service\OS\AppleNotification;

/**
 * Class IosPush
 *
 * @package CAB\CourseBundle\PushNotification
 */
class IosPush implements PushInterface
{
    /**
     * @var AppleNotification
     */
    private $rmsPushNotifications;

    /**
     * @var iOSMessage
     */
    private $message;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param AppleNotification $rmsPushNotifications apple rms notification service
     * @param LoggerInterface   $logger               log
     */
    public function __construct(AppleNotification $rmsPushNotifications, LoggerInterface $logger)
    {
        $this->rmsPushNotifications = $rmsPushNotifications;
        $this->logger = $logger;
        $this->message = new IosMessage();
    }

    /**
     * Setting params
     *
     * @param string $message
     * @param string $deviceIdentifier
     * @param array  $data
     *
     * @return mixed
     */
    public function setParams($message, $deviceIdentifier, array $data = array(), $pem = 'driver')
    {

        if (array_key_exists('id_course', $data)) {
            $message = array('title' => $data['subject_notification'], "body" => $data['alert'], 'id_course' => $data['id_course'], 'type' => $data['type']);
        } else {
            $message = array('title' => $data['subject_notification'], "body" => $data['alert'], 'type' => $data['type']);
        }

        
        
        $this->message->setMessage($message);
        $this->message->setAPSSound($data['sound']);
        $this->message->setAPSBadge(1);
        $this->rmsPushNotifications->setPem($pem);
        $this->message->setDeviceIdentifier($deviceIdentifier);
    }

    /**
     * Send push notification
     *
     * @return bool
     */
    public function sendMessage()
    {
        try {
            $this->rmsPushNotifications->send($this->message);
            $result = $this->rmsPushNotifications->getResponses();
            $this->logger->error('CAB/CourseBundle/PushNotification/IosPush.php. result send notif : ');
        } catch (\Exception $e) {
            $this->logger->error('CAB/CourseBundle/PushNotification/IosPush.php. Exception : '.$e->getMessage().'. Line :'.$e->getLine());
        }

        return $result;
    }
}