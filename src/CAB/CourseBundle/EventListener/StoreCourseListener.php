<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:24
 */

namespace CAB\CourseBundle\EventListener;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Handler\CourseHandler;
use CAB\CourseBundle\Manager\PlaningManager;
use CAB\CourseBundle\Manager\TransactionPaymentManager;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\EventDispatcher\GenericEvent;
use CAB\CourseBundle\Manager\CabUserManager;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class StoreCourseListener
 *
 * @package CAB\CourseBundle\EventListener
 */
class StoreCourseListener
{
    /**
     * @var PlaningManager
     */
    private $planingManager;

    /**
     * @var CourseHandler
     */
    private $courseHandler;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    private $userManager;

    private $trans;

    private $environement;

    /**
     * @param PlaningManager $planingManager
     * @param CourseHandler  $courseHandler
     */
    public function __construct(PlaningManager $planingManager, CourseHandler $courseHandler,
        TransactionPaymentManager $transactionManager, CabUserManager $userManager, TranslatorInterface $trans, $environement)
    {
        $this->planingManager = $planingManager;
        $this->courseHandler = $courseHandler;
        $this->transactionManager = $transactionManager;
        $this->userManager = $userManager;
        $this->trans = $trans;
        $this->environement = $environement;
    }

    /**
     * Listen the generic event dispatched when the course is saved
     *
     * @param GenericEvent $event
     */
    public function onStoreCourse(GenericEvent $event)
    {
        if ($event->getSubject() instanceof Course) {
            /** @var Course $course */
            $course = $event->getSubject();

            if ($course->getIsGroupage()) {
                $course->setIsGroupage(true);

                $groupageCourse = $course->getIsGroupage()->getCourse()->getIterator();
                while ($groupageCourse->valid()) {
                    $courseGrouped = $groupageCourse->current();
                    $courseGrouped->setIsGroupage(true);
                    $courseGrouped->setGroupage($course->getGroupage());
                    $this->courseHandler->saveCourse($courseGrouped);
                    $groupageCourse->next();
                }
            }
            if (!$event->hasArgument('groupage')) {
                if ($course->getIsCycle()) {
                    $firstDateOfCycle = $course->getDepartureDate();
                    $lastDateOfCycle = $course->getCycleEndDate();

                    $isMondayInCycle = $course->getMondayCycle();
                    $isTuesdayInCycle = $course->getTuesdayCycle();
                    $isWednesdayInCycle = $course->getWednesdayCycle();
                    $isThursdayInCycle = $course->getThursdayCycle();
                    $isFridayInCycle = $course->getFridayCycle();
                    $isSaturdayInCycle = $course->getSaturdayCycle();
                    $isSundayInCycle = $course->getSundayCycle();

                    $aDatesOfCycle = $this->dateRange($firstDateOfCycle->format("Y-m-d"), $lastDateOfCycle->format("Y-m-d"), "+1 day", "Y-m-d");
                    $sDates = '';
                    if (!$isMondayInCycle) {
                        unset($aDatesOfCycle['Mon']);
                    } else {
                        $sDates .= 'Monday-';
                    }
                    if (!$isTuesdayInCycle) {
                        unset($aDatesOfCycle['Tue']);
                    } else {
                        $sDates .= 'Tuesday-';
                    }
                    if (!$isWednesdayInCycle) {
                        unset($aDatesOfCycle['Wed']);
                    } else {
                        $sDates .= 'Wednesday-';
                    }
                    if (!$isThursdayInCycle) {
                        unset($aDatesOfCycle['Thu']);
                    } else {
                        $sDates .= 'Thursday-';
                    }
                    if (!$isFridayInCycle) {
                        unset($aDatesOfCycle['Fri']);
                    } else {
                        $sDates .= 'Friday-';
                    }
                    if (!$isSaturdayInCycle) {
                        unset($aDatesOfCycle['Sat']);
                    } else {
                        $sDates .= 'Saturday-';
                    }
                    if (!$isSundayInCycle) {
                        unset($aDatesOfCycle['Sun']);
                    } else {
                        $sDates .= 'Sunday-';
                    }
                    $fullDayDepDate = $course->getDepartureDate()->format('l');
                    $checkDay = 'is'.$fullDayDepDate.'InCycle';
                    $course->setDaysCycle($sDates);
                    $course->setDateStartParent($firstDateOfCycle);
                    $course->setDateEndParent($lastDateOfCycle);
                    $course->setTimeCycleParent($course->getDepartureTime());
                    //get Exception date
                    /** @var PersistentCollection $exeptPeriod */
                    $exeptPeriod = $course->getServiceParticularPeriodCourses();
                    /** @var array $aExepts */
                    $aExepts = $exeptPeriod->getValues();

                    $DateTimeExclus = array();
                    foreach ($aExepts as $period) {
                        $startedDate = $period->getStartedDate();
                        /** @var \DateTime $endDate */
                        $endDate = $period->getEndDate();
                        $endDate->add(new \DateInterval('PT1H'));
                        /** @var \DatePeriod $periodRange */
                        $periodRange = new \DatePeriod(
                            $startedDate,
                            new \DateInterval('P1D'),
                            $endDate
                        );

                        foreach($periodRange as $key => $value) {
                            if (!in_array($value, $DateTimeExclus)) {
                                $DateTimeExclus [] = $value;
                            }
                        }
                    }
                    $changeFirstCourse = false;
                    foreach ($aDatesOfCycle as $key => $aValue) {
                        foreach ($aValue as $item => $value) {
                            $value = $value . ' 00:00:00';
                            $dateDeparture = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
                            if (count($DateTimeExclus)) {
                                // si le premier jour de la date de depart correspond à un jour qui n'est pas couché dans la liste des jours
                                if (!$$checkDay && !$changeFirstCourse) {
                                    $changeFirstCourse = true;
                                    $course->setDepartureDate($dateDeparture);
                                    $course->setDateStartParent($dateDeparture);
                                } elseif (!in_array($dateDeparture, $DateTimeExclus)) {
                                    $this->setDuplicateCourse($course, $dateDeparture);
                                }
                            } else {
                                if (!$$checkDay && !$changeFirstCourse) {
                                    $changeFirstCourse = true;
                                    $course->setDepartureDate($dateDeparture);
                                    $course->setDateStartParent($dateDeparture);
                                } else {
                                    $this->setDuplicateCourse($course, $dateDeparture);
                                }
                            }
                        }
                    }

                }
            }

            //set status course affected if the driver is defined
            if ($course->getDriver() !== null) {
                $course->setCourseStatus(course::STATUS_AFFECTED);
            }
            if (isset($event['is_date_time_updated']) && $event['is_date_time_updated'] === true) {
                $aPlaning = $this->planingManager->getPlaningsBy('course', $course);
                if ($aPlaning) {
                    $oPlaning = $aPlaning[0];
                    $statusUpdatePlaning = $this->planingManager->removePlaning($oPlaning);
                }
                // 2-Change status of course
                $course->setCourseStatus(Course::STATUS_UPDATED_BY_CUSTOMER);

                // 3-send internal message to all availables drivers
                //$transactionByCourse = $this->transactionManager->getTransactionBy('transactionPaymentCourse', $course);
                //if ($transactionByCourse && isset($event['available_drivers']) && !empty($event['available_drivers'])) {
                    $this->courseHandler->sendInternalMessageWithoutCheck(
                        $course,
                        $event['available_drivers']
                    );
                //}
            }
        }

    }

    /**
     * @param string $first 2016-01-01
     * @param string $last 2016-01-01
     * @param string $step +1 day
     * @param string $output_format d/m/Y
     * @return array
     */
    private function dateRange($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
        while( $current < $last ) {
            $current = strtotime($step, $current);
            $key = date('D', $current);
            $dates[$key][] = date($output_format, $current);
        }

        return $dates;
    }

    /**
     * @param Course $course
     * @param \DateTime $dateDeparture
     */
    protected function setDuplicateCourse($course, $dateDeparture)
    {
        $courseDuplicated = clone $course;
        $courseDuplicated->setDepartureDate($dateDeparture);
        $courseDuplicated->setCycleParent($course->getId());
        $courseDuplicated->setDepartureTime($course->getDepartureTime());
        $courseDuplicated->setCourseStatus(Course::STATUS_AFFECTED);
        $courseDuplicated->setIsCycle(false);
        $this->courseHandler->saveCourse($courseDuplicated);
    }

}