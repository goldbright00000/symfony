<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 04/03/2015
 * Time: 17:48
 */

namespace CAB\CourseBundle\EventListener;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Handler\CourseHandler;
use CAB\CourseBundle\Manager\PlaningManager;
use CAB\CourseBundle\Manager\TransactionPaymentManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\EventDispatcher\GenericEvent;
use CAB\CourseBundle\Manager\CabUserManager;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Psr\Log\LoggerInterface;
use DotSmart\SmsBundle\Services\SendSmsService;

/**
 * Class UpdateStatusCourseListener
 *
 * @package CAB\CourseBundle\EventListener
 */
class UpdateStatusCourseListener
{
    /**
     * @var PlaningManager
     */
    private $planingManager;

    /**
     * @var CourseHandler
     */
    private $courseHandler;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var CabUserManager
     */
    private $userManager;

    /**
     * @var TranslatorInterface
     */
    private $trans;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var SendSmsService
     */
    private $sendSmsService;

    protected $twig;
    protected $mailer;


    /**
     * @param PlaningManager            $planingManager
     * @param CourseHandler             $courseHandler
     * @param TransactionPaymentManager $transactionManager
     * @param CabUserManager            $userManager
     * @param TranslatorInterface       $trans
     * @param Router                    $router
     */
    public function __construct(
        PlaningManager $planingManager,
        CourseHandler $courseHandler,
        TransactionPaymentManager $transactionManager,
        CabUserManager $userManager,
        TranslatorInterface $trans,
        Router $router,
        LoggerInterface $logger,
        SendSmsService $sendSmsService,
        \Swift_Mailer $mailer,
        \Twig_Environment $twig

    ) {
        $this->planingManager = $planingManager;
        $this->courseHandler = $courseHandler;
        $this->transactionManager = $transactionManager;
        $this->userManager = $userManager;
        $this->trans = $trans;
        $this->router = $router;
        $this->logger = $logger;
        $this->sendSmsService = $sendSmsService;
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * Listen the generic event dispatched when the status course is updated
     *
     * @param GenericEvent $event
     *
     * @throws \RuntimeException
     * @throws \ErrorException
     */
    public function onUpdateStatusCourse(GenericEvent $event)
    {
        if ($event->getSubject() instanceof Course) {
            /** @var Course $course */
            $course = $event->getSubject();

            if ($event->hasArgument('status_course')) {
                if ($event->getArgument('status_course') == Course::STATUS_AFFECTED) {
                    // 1- check if the course is already planned
                    $aPlaning = $this->planingManager->getPlaningsBy('course', $course);
                    if ($aPlaning) {
                        try {
                            $oPlaning = $aPlaning[0];
                            $this->planingManager->removePlaning($oPlaning);
                        } catch (\Exception $e) {
                            throw new Exception($e->getMessage());
                        }
                    }
                    // 2- planned the course
                    try {
                        $oDriver = $course->getDriver();
                        $depDateTime = $course->getDepartureDate()->format('Y-m-d') . ' ' . $course->getDepartureTime()->format(
                                'H:i:s'
                            );

                        $courseTime = 'PT' . ceil($course->getCourseTime()) . 'S';

                        $oDepDateTime = new \DateTime($depDateTime);
                        $oArrivalDateTime = new \DateTime($depDateTime);
                        $oArrivalDateTime->add(new \DateInterval($courseTime));
                        $oStart = $oDepDateTime;
                        $oEnd = $oArrivalDateTime;

                        $status = $this->planingManager->saveObject(
                            array(
                                'planingName' => 'Course (' . $course->getId() . ') accepted by the driver',
                                'start' => $oStart,
                                'end' => $oEnd,
                                'driver' => $oDriver,
                                'course' => $course,
                            )
                        );


                        // 3-send internal message to affected driver
                        try {
                            $body = $this->trans->trans(
                                "A course has been affected to you. Below all information of the course<br/>
                            To check the course click on this link below:<br/>
                            ",
                                array(
                                    '%name%' => $course->getClient() ? $course->getClient()->getUsedName() : '',
                                    '%departure%' => $course->getStartedAddress(),
                                    '%arrival%' => $course->getArrivalAddress(),
                                    '%datetime%' => $course->getDepartureDate()->format('Y-m-d') . " &nbsp;" . $course->getDepartureTime()
                                            ->format('H:i:s'),
                                )
                            );
                        } catch (Exception $e) {
                            throw new RuntimeException();
                        }
                        $url = $this->router->generate('sonata_course_show', array('id' => $course->getId()));
                        $body .= ' <a href="' . $url . '">Course</a>';
                        $dateCourseAffected = '';
                        if ($course->getDepartureDate() != null && $course->getDepartureTime() != null) {
                            $dateCourseAffected = $course->getDepartureDate()->format('d-m-Y') . ' à '.
                                $course->getDepartureTime()->format('H:i');
                        }
                        $subject = 'Ajout d\'une course le ' . $dateCourseAffected;
                        if ($oDriver) {
                            $this->courseHandler->sendInternalMessageWithoutCheck(
                                $course,
                                array($oDriver->getId()),
                                $subject,
                                $body
                            );
                        }
                    } catch (\ErrorException $e) {
                        throw new \ErrorException(
                            $e->getMessage(),
                            $e->getCode(),
                            $e->getSeverity(),
                            $e->getFile(),
                            $e->getLine()
                        );
                    }
                }
                elseif ($event->getArgument('status_course') == Course::STATUS_ARRIVED_DRIVER) {
                     /*
                    $customer = $course->getClient();
                    $smsNotification = $customer->getNotificationSms();
                    if ($customer && $customer->getPhoneMobile() !== null && $smsNotification && $smsNotification->getStatusArrivedDriver()) {
                        $messagesms = $this->trans->trans(
                            'The driver is arrived.', array(), 'frontend'
                        );

                        $result = $this->sendSmsService->send(
                                array(
                                    'user_id' => $customer->getId(),
                                    'designation' => 'Arrival Driver',
                                    'message' => $messagesms,
                                    'numbers' => trim($customer->getPhoneMobile())
                                )
                        );
                        $this->logger->notice('-----CAB/CourseBundle/EventListener/UpdateStatusCourseListener.php :'.$result);
                        //@TODO : if the result['etat'] = 0. send a mail to the admin to notify it with the info returned
                     }
                     */

                } elseif ($event->getArgument('status_course') == Course::STATUS_CANCEL_BY_SNCF) {
                    $service = $course->getServiceTransportCourses()->first();
	                $serviceEmail = null;
	                if ($service) {
		                $serviceEmail = $service->getServiceEmail();
	                }
                    $id_sncf = null;
                    if ($course->getCommandSncf()) {
                        $id_sncf = $course->getCommandSncf()->getCommandeCourse();
                    }

	                if ($serviceEmail){

                        $status = $course->getCourseStatus();
                        if ($status==21){
                            $status = 'annulation Galapagos' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();

                        $message = new \Swift_Message('bc n°'.$id_sncf.' est annulé');

                            $message->setFrom('no-reply@up.taxi')
                            ->setTo($serviceEmail);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race_sncf.html.twig', array('id_sncf' => $id_sncf, 'date'=>$date, 'time'=>$time, 'status'=>$status)),
                            'text/html'
                        );
                        $this->mailer->send($message);

                    }
                } elseif ($event->getArgument('status_course') == Course::STATUS__CANCEL_OUT_TIME) {
                    $service = $course->getServiceTransportCourses()->first();
	                $serviceEmail = null;
	                if ($service) {
		                $serviceEmail = $service->getServiceEmail();
	                }

                    $id_sncf = null;
                    if ($course->getCommandSncf()) {
                        $id_sncf = $course->getCommandSncf()->getCommandeCourse();
                    }

	                if ($serviceEmail && $service){

	                    $status = $course->getCourseStatus();
	                    if ($status == 22) {
		                    $status = 'annulation délai';
	                    }
	                    $date = $course->getDepartureDateFormatted();
	                    $time = $course->getDepartureTimeFormatted();

	                    $message = new \Swift_Message('bc n°' . $id_sncf . ' est en annulation delai');

		                $message->setFrom('no-reply@up.taxi')
		                    ->setTo($serviceEmail)
		                    ->setBody(
			                    $this->twig->render('CABMainBundle:Mail:status_race.html.twig', ['id_sncf' => $id_sncf, 'date' => $date, 'time' => $time, 'status' => $status]),
			                    'text/html'
		                    );

	                    $this->mailer->send($message);
                    }
                }
                elseif ($event->getArgument('status_course') == Course::STATUS_CREATED) {
                    $service = $course->getServiceTransportCourses()->first();
	                $serviceEmail = null;
                    $EmailCustomer = null;
                    if ($course->getClient()->getEmail()) {
                        $EmailCustomer = $course->getClient()->getEmail();
                    }
                    $id_sncf = null;
                    if ($course->getCommandSncf()) {
                        $id_sncf = $course->getCommandSncf()->getCommandeCourse();
                    }
                    $EmailNotificationCustomer = $course->getClient()->getNotificationEmail();

                    if ($service) {
	                    $serviceEmail = $service->getServiceEmail();
                    }
                    if ($course->getClient()->getCustomerCompany()){
                        $companyCustomer = $course->getClient()->getCustomerCompany()->getCompanyName();
                        $companyPhone = $course->getClient()->getCustomerCompany()->getPhone();
                    }




                    if ($serviceEmail && $id_sncf){
                        /** @var Course $course */
                        $service = $course->getServiceTransportCourses()->first();
                        $serviceEmail = $service->getServiceEmail();
                        $status = $course->getCourseStatus();
                        if ($status==0){
                            $status = 'création' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();


                        $message = new \Swift_Message('bc n°'.$id_sncf.' est crée');

                            $message->setFrom('no-reply@up.taxi')
                            ->setTo($serviceEmail);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race_sncf.html.twig', array('id_sncf' => $id_sncf, 'date'=>$date, 'time'=>$time, 'status'=>$status)),
                            'text/html'
                        );
                        $this->mailer->send($message);


                    } elseif ($serviceEmail){
                        /** @var Course $course */
                        $service = $course->getServiceTransportCourses()->first();
                        $serviceEmail = $service->getServiceEmail();

                        $status = $course->getCourseStatus();
                        if ($status==0){
                            $status = 'créée' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();
                        $id_race = $course->getId();
                        $client = $client = $course->getClient()->getLastName().' '.$course->getClient()->getFirstName() ;

                        $message = new \Swift_Message('votre commande '.$id_race.' est crée');
                            //->setSubject('votre commande '.$id_race.' est crée')
                            $message->setFrom('no-reply@up.taxi')
                            ->setTo($serviceEmail);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race.html.twig', array('id_race' => $id_race, 'date'=>$date, 'time'=>$time, 'status'=>$status, 'client'=>$client, 'course'=>$course, 'service'=>$service->getserviceName())),
                            'text/html'
                        );
                        $this->mailer->send($message);


                    }
                    if ($EmailCustomer && !$EmailNotificationCustomer && $EmailNotificationCustomer->getStatusAffected()){
                        /** @var Course $course */
                        $service = $course->getServiceTransportCourses()->first();
                        $serviceEmail = $service->getServiceEmail();
                        $priceview = $service->getpriceView();

                        $status = $course->getCourseStatus();
                        if ($status==0){
                            $status = 'créée' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();
                        $id_race = $course->getId();
                        $client = $client = $course->getClient()->getLastName().' '.$course->getClient()->getFirstName() ;

                        $message = new \Swift_Message('votre commande '.$id_race.' est crée');

                            $message->setFrom('no-reply@up.taxi')
                            ->setTo($EmailCustomer);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race_customer.html.twig', array('id_race' => $id_race, 'date'=>$date, 'time'=>$time, 'status'=>$status, 'client'=>$client, 'course'=>$course, 'service'=>$service->getserviceName(), 'priceview' =>$priceview, 'companyPhone'=>$companyPhone,'companyCustomer'=>$companyCustomer)),
                            'text/html'
                        );
                        $this->mailer->send($message);
                    }

                } elseif ($event->getArgument('status_course') == Course::STATUS_CANCELLED_BY_CLIENT) {
                    $serviceEmail = null;
                    $EmailCustomer = null;
                    $id_sncf = null;
                    $BusinessCompanyCustomer = null;
                    if($course->getServiceTransportCourses()->first()){
                        $service = $course->getServiceTransportCourses()->first();
                        $serviceEmail = $service->getServiceEmail();
                    }
                    if ($course->getClient()->getEmail()) {
                        $EmailCustomer = $course->getClient()->getEmail();
                    }
                    if ($course->getCommandSncf()) {
                        $id_sncf = $course->getCommandSncf()->getCommandeCourse();
                    }
                    if ($course->getClient()->getBusinessCompany()){
                        $BusinessCompanyCustomer = $course->getClient()->getBusinessCompany()->getCompanyName();
                    }
                    if ($course->getClient()->getCustomerCompany()){
                        $companyCustomer = $course->getClient()->getCustomerCompany()->getCompanyName();
                        $companyPhone = $course->getClient()->getCustomerCompany()->getPhone();
                    }

                    if ($serviceEmail){
                        $id_sncf = null;
                        if ($course->getCommandSncf()) {
                            $id_sncf = $course->getCommandSncf()->getCommandeCourse();
                        }

                        /** @var Course $course */
                        $service = $course->getServiceTransportCourses()->first();
                        $serviceEmail = $service->getServiceEmail();

                        $status = $course->getCourseStatus();
                        $client = $course->getClient()->getLastName().' '.$course->getClient()->getFirstName() ;
                        if ($status==3){
                            $status = 'annulée' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();
                        $id_race = $course->getId();

                        $message = new \Swift_Message('votre commande '.$id_race.' est annulée');
                        $message->setFrom('no-reply@up.taxi')
                            ->setTo($serviceEmail);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race.html.twig', array('id_race' => $id_race, 'date'=>$date, 'time'=>$time, 'status'=>$status, 'course'=>$course, 'client'=>$client, 'service'=>$service->getserviceName())),
                            'text/html'
                        );
                        $this->mailer->send($message);


                    }

                    if ($EmailCustomer){
                        /** @var Course $course */
                        $serviceEmail = null;
                        $priceview = null;
                        if($course->getServiceTransportCourses()->first()){
                            $service = $course->getServiceTransportCourses()->first();
                            $serviceEmail = $service->getServiceEmail();
                            $priceview = $service->getpriceView();
                        }

                        $status = $course->getCourseStatus();
                        if ($status==25){
                            $status = 'annulée' ;
                        }
                        if ($status==3){
                            $status = 'annulée' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();
                        $id_race = $course->getId();
                        $client = $course->getClient()->getLastName().' '.$course->getClient()->getFirstName() ;

                        $message = new \Swift_Message('votre commande '.$id_race.' est annulée');

                           $message->setFrom('no-reply@up.taxi')
                            ->setTo($EmailCustomer);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race_customer.html.twig', array('id_race' => $id_race, 'date'=>$date, 'time'=>$time, 'status'=>$status, 'client'=>$client, 'course'=>$course, 'service'=>$service->getserviceName(), 'priceview' =>$priceview, 'BusinessCompanyCustomer' => $BusinessCompanyCustomer, 'companyCustomer'=>$companyCustomer,'companyPhone'=>$companyPhone)),
                            'text/html'
                        );
                        $this->mailer->send($message);
                    }


                }
                elseif ($event->getArgument('status_course') == Course::STATUS__CANCEL_BY_DO) {

                    $serviceEmail = null;
                    $EmailCustomer = null;
                    $id_sncf = null;
                    $EmailNotificationCustomer = $course->getClient()->getNotificationEmail();
                    if($course->getServiceTransportCourses()->first()){
                        $service = $course->getServiceTransportCourses()->first();
                        $serviceEmail = $service->getServiceEmail();
                    }
                    if ($course->getClient()->getEmail()) {
                        $EmailCustomer = $course->getClient()->getEmail();
                    }
                    if ($course->getCommandSncf()) {
                        $id_sncf = $course->getCommandSncf()->getCommandeCourse();
                    }
                    if ($course->getClient()->getBusinessCompany()){
                        $BusinessCompanyCustomer = $course->getClient()->getBusinessCompany()->getCompanyName();
                    }
                    if ($course->getClient()->getCustomerCompany()){
                        $companyCustomer = $course->getClient()->getCustomerCompany()->getCompanyName();
                        $companyPhone = $course->getClient()->getCustomerCompany()->getPhone();
                    }
	                //$EmailCustomer = 'social.oequal@gmail.com';
	                //$serviceEmail = 'social.oequal@gmail.com';

                    if ($serviceEmail){

                        $id_sncf = null;

                        /** @var Course $course */
                        $service = $course->getServiceTransportCourses()->first();
                        $serviceEmail = $service->getServiceEmail();
                        if ($course->getCommandSncf()) {
                            $id_sncf = $course->getCommandSncf()->getCommandeCourse();
                        }
                        $status = $course->getCourseStatus();
                        $client = $course->getClient()->getLastName().' '.$course->getClient()->getFirstName() ;
                        if ($status==25){
                            $status = 'annulée' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();
                        $id_race = $course->getId();

                        $message = new \Swift_Message('votre commande '.$id_race.' est annulée');

	                    $message->setFrom('no-reply@up.taxi')
                            ->setTo($serviceEmail);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race.html.twig', array('id_race' => $id_race, 'date'=>$date, 'time'=>$time, 'status'=>$status, 'course'=>$course, 'client'=>$client, 'service'=>$service->getserviceName())),
                            'text/html'
                        );
                        $this->mailer->send($message);


                    }

                    if ($EmailCustomer && !$EmailNotificationCustomer && ($EmailNotificationCustomer == null || ($EmailNotificationCustomer != null && $EmailNotificationCustomer->getStatusCancelByClient()))){
                        /** @var Course $course */

                        $service = $course->getServiceTransportCourses()->first();
                        //$serviceEmail = $service->getServiceEmail();
	                    if($service != false)
                             $priceview = $service->getpriceView();
	                    else
		                    $priceview = '';

                        $status = $course->getCourseStatus();
                        if ($status==25){
                            $status = 'annulée' ;
                        }
                        $date = $course->getDepartureDateFormatted();
                        $time = $course->getDepartureTimeFormatted();
                        $id_race = $course->getId();
                        $client = $course->getClient()->getLastName().' '.$course->getClient()->getFirstName() ;

                        $message = new \Swift_Message('votre commande '.$id_race.' est annulée');

                            $message->setFrom('no-reply@up.taxi')
                            ->setTo($EmailCustomer);
                        $message->setBody(
                            $this->twig->render('CABMainBundle:Mail:status_race_customer.html.twig', array('id_race' => $id_race, 'date'=>$date, 'time'=>$time, 'status'=>$status, 'client'=>$client, 'course'=>$course, 'service'=>$service != false ?$service->getserviceName():'', 'priceview' =>$priceview, 'BusinessCompanyCustomer' => isset($BusinessCompanyCustomer)?$BusinessCompanyCustomer:'', 'companyCustomer'=>isset($companyCustomer)?$companyCustomer:"",'companyPhone'=>isset($companyPhone)?$companyPhone:'')),
                            'text/html'
                        );
                        $this->mailer->send($message);
                    }


                }

                else {
                    $this->logger->notice('-----CAB/CourseBundle/EventListener/UpdateStatusCourseListener.php : event status_course not equal to affected-----');
                }
            }
        } else {
            $this->logger->notice('-----CAB/CourseBundle/EventListener/UpdateStatusCourseListener.php : event status_course not found-----');
        }
    }
}