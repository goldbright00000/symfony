<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 04/03/2015
 * Time: 17:48
 */

namespace CAB\CourseBundle\EventListener;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Manager\CourseHistoryManager;
use Symfony\Component\EventDispatcher\GenericEvent;
use CAB\CourseBundle\Manager\CabUserManager;
use Psr\Log\LoggerInterface;

/**
 * Class HistoryCourseListener
 *
 * @package CAB\CourseBundle\HistoryCourseListener
 */
class HistoryCourseListener
{
    /**
     * @var CourseHistoryManager
     */
    private $courseHistoryManager;

    /**
     * @var CabUserManager
     */
    private $userManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CourseHistoryManager $courseHistoryManager
     * @param CabUserManager       $userManager
     * @param LoggerInterface      $logger
     */
    public function __construct(
        CourseHistoryManager $courseHistoryManager,
        CabUserManager $userManager,
        LoggerInterface $logger
    ) {
        $this->courseHistoryManager = $courseHistoryManager;
        $this->userManager = $userManager;
        $this->logger = $logger;
    }


    /**
     * Listen the generic event dispatched when the course is updated
     *
     * @param GenericEvent $event
     *
     * @throws \ErrorException
     */
    public function onHistoryCourse(GenericEvent $event)
    {
        try {
            if ($event->getSubject() instanceof Course && $event->hasArgument('course_history')) {
                $course = $event->getSubject();
                $existHistory = null;
                // check if the history is already exists for the given course with rhe same status.
                if ($course instanceof Course) {
                    $existHistory = $this->courseHistoryManager->getHitoryByCourseAndStatus($course, $course->getCourseStatus());
                }

                if (!$existHistory) {
                    $this->courseHistoryManager->createHistoryFromCourse($course);
                }
            }
        } catch (\ErrorException $e) {
            $this->logger->error($e->getMessage().' '.$e->getFile().' '.$e->getLine());
            throw new \ErrorException(
                $e->getMessage(),
                $e->getCode(),
                $e->getSeverity(),
                $e->getFile(),
                $e->getLine()
            );
        }
    }
}