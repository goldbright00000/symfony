<?php
/**
 * Created by PhpStorm.
 * User: viasyst
 * Date: 15/06/2016
 * Time: 03:18
 */

namespace CAB\CourseBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use CAB\CourseBundle\Entity\Company;

class CompanyListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->setFormatCompany($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->setFormatCompany($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    protected function setFormatCompany(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Company) {
            $countryRepository = $entityManager->getRepository('CABCourseBundle:Country');
            if ($entity->getCountry() !== null) {
                $countryName = $entity->getCountry();
                $country = $countryRepository->findOneBy(array('iso31661Alpha2' => $countryName));
            } else {
                $country = $countryRepository->find(76);
            }

            $entity->setFormat($country);
            $entityManager->persist($entity);
            $entityManager->flush();
        }
    }
}