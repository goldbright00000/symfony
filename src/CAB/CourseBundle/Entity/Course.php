<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * CAB\CourseBundle\Entity\Course
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_course")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\CourseRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */


class Course {

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_REJECTED = 2;
    const STATUS_CANCELLED_BY_CLIENT = 3;
    const STATUS_AFFECTED = 4;
    const STATUS_UPDATED_BY_CUSTOMER = 5;
    const STATUS_DONE = 6; # RACE FINISH
    const STATUS_PAYED_AND_UPDATED_BY_CUSTOMER = 7;
    const STATUS_CANCELLED_BY_ADMIN = 8;
    const STATUS_ON_RACE_DRIVER = 9;
    const STATUS_CACH_PAY = 10;
    const STATUS_ATTENTE_PAY = 11;
    const STATUS_ACCEPTED_DRIVER = 12;
    const STATUS_ARRIVED_DRIVER = 13;
    const STATUS_NO_REPONSE = 14;
    const STATUS_CANCELLED_BY_DRIVER = 15;
    const STATUS_UPDATED_BY_DRIVER = 16;
    const STATUS_COMMAND = 17;
    const STATUS_DRIVER_GO_TO_CLIENT = 18; #j'y vais
    const STATUS_CANCELLED_BY_AGENT = 19;
    const STATUS_CREATED_BY_CALLC = 20;
    const STATUS_CANCEL_BY_SNCF = 21;
    const STATUS__CANCEL_OUT_TIME = 22;
    const STATUS__PRE_COMMAND = 23;
    const STATUS__DEPLACEMENT_INUTILE = 24;
    const STATUS__CANCEL_BY_DO = 25;



    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="TransactionPayment", mappedBy="transactionPaymentCourse", cascade={"persist", "remove"}, fetch="EXTRA_LAZY")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $courseTransactionPayment;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="courses", cascade={"persist"})
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="course", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public", "Course"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="courseCreator", cascade={"persist"})
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="courseDriver")
     * @ORM\JoinColumn(name="driver", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Tarif", inversedBy="coursesTarif", cascade={"persist"})
     * @ORM\JoinColumn(name="applicable_tarif_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $applicableTarif;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Planing", mappedBy="course", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $planingCourse;

    /**
     * @ORM\OneToMany(targetEntity="CAB\MessageBundle\Entity\Thread", mappedBy="course", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $courseThread;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\DetailsTarif", inversedBy="detailsTarifCourse", cascade={"remove", "persist"})
     * @ORM\JoinColumn(name="details_tarif_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public", "Course"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $courseDetailsTarif;


    /**
     * @ORM\OneToMany(targetEntity="CAB\TicketBundle\Entity\Ticket", mappedBy="course", cascade={"persist"}, fetch="LAZY")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $ticket;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\CourseHistory", mappedBy="course", cascade={"remove", "persist"}, fetch="LAZY")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $courseHistory;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Order", mappedBy="courseOrder", cascade={"remove", "persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $orderCourse;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Vehicule", inversedBy="courses", fetch="LAZY")
     * @ORM\JoinColumn(name="vehicle_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicle;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\TypeVehicule", inversedBy="courses", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="type_vehicle_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $typeVehicleCourse;

    /**
     * @ORM\ManyToMany(targetEntity="CAB\CourseBundle\Entity\ServiceTransport", inversedBy="course", fetch="LAZY")
     * @ORM\JoinTable(name="cab_service_courses")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $serviceTransportCourses;

    /**
     * @ORM\ManyToMany(targetEntity="CAB\CourseBundle\Entity\ParticularPeriod", inversedBy="course", fetch="LAZY")
     * @ORM\JoinTable(name="cab_services_transport_particular_periods")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $serviceParticularPeriodCourses;

    /**
     * @var string $startedAddress
     *
     * @ORM\Column(name="started_address", type="text", nullable=false)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $startedAddress;

    /**
     * @var string $startedAddressCity
     *
     * @ORM\Column(name="started_address_city", type="text", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $startedAddressCity;

    /**
     * @var string $indicatorStartedAddress
     *
     * @ORM\Column(name="indicator_started_address", type="text", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $indicatorStartedAddress;

    /**
     * @var string $arrivalAddress
     *
     * @ORM\Column(name="arrival_address", type="text", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $arrivalAddress;

    /**
     * @var string $arrivalAddressCity
     *
     * @ORM\Column(name="arrival_address_city", type="text", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $arrivalAddressCity;

    /**
     * @var string $indicatorArrivalAddress
     *
     * @ORM\Column(name="indicator_arrival_address", type="text", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $indicatorArrivalAddress;

    /**
     * var datetime $departureDate
     *
     * @ORM\Column(name="departure_date", type="date", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $departureDate;

    /**
     * var datetime $departureTime
     *
     * @ORM\Column(name="departure_time", type="time", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $departureTime;

    /**
     * @var string $targetType
     *
     * @ORM\Column(name="target_type", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $targetType;

    /**
     * @var string $courseTime
     *
     * @ORM\Column(name="course_time", type="string", length=128, nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $courseTime;

    /**
     * @var string $distance
     *
     * @ORM\Column(name="distance", type="string", length=50, nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $distance;

    /**
     * @var integer $personNumber
     *
     * @ORM\Column(name="person_number", type="string", length=255, nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $personNumber;

    /**
     * @var integer $forceHourArrived
     *
     * @ORM\Column(name="force_hour_arrived", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $forceHourArrived;

    /**
     * @var integer $arrivalHour
     *
     * @ORM\Column(name="arrival_hour", type="time", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $arrivalHour;

    /**
     * @var float $priceHT
     *
     * @ORM\Column(name="price_ht", type="decimal", precision=8, scale=2,  nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $priceHT;

    /**
     * @var float $priceTTC
     *
     * @ORM\Column(name="price_TTC", type="decimal", precision=8, scale=2, nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $priceTTC;

    /**
     * @var string $courseStatus
     *
     * @ORM\Column(name="course_status", type="string", length=255, nullable=true)
     *
     * @Expose
     * @Groups({"Public", "Course"})
     *
     */
    protected $courseStatus;

    /**
     * @var boolean $isHandicap
     *
     * @ORM\Column(name="is_handicap", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $isHandicap;

    /**
     * @var string $laguage
     *
     * @ORM\Column(name="laguage", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $laguage;

    /**
     * @var string $nbWheelchair
     *
     * @ORM\Column(name="nb_wheelchair", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $nbWheelchair;

    /**
     * @var string $nbChildren
     *
     * @ORM\Column(name="nb_children", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $nbChildren;

    /**
     * @var string $descriptionChildren
     *
     * @ORM\Column(name="description_children", type="text", nullable=true)
     * @Expose
     *
     */
    protected $descriptionChildren;

    /**
     * @var string $isGroup
     *
     * @ORM\Column(name="is_group", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $isGroup;

    /**
     * @var string $isCycle
     *
     * @ORM\Column(name="is_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $isCycle;

    /**
     * @var string $cycleParent
     *
     * @ORM\Column(name="cycle_parent", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $cycleParent;

    /**
     * @var string $dateStartParent
     *
     * @ORM\Column(name="date_start_parent", type="datetime", nullable=true)
     *
     */
    protected $dateStartParent;

    /**
     * @var string $dateEndParent
     *
     * @ORM\Column(name="date_end_parent", type="datetime", nullable=true)
     *
     */
    protected $dateEndParent;

    /**
     * @var string $timeCycleParent
     *
     * @ORM\Column(name="time_cycle_parent", type="time", nullable=true)
     *
     */
    protected $timeCycleParent;

    /**
     * @var string $daysCycle
     *
     * @ORM\Column(name="days_cycle", type="string", length=64, nullable=true)
     *
     */
    protected $daysCycle;

    /**
     * @var string $mondayCycle
     *
     * @ORM\Column(name="monday_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $mondayCycle;


    /**
     * @var string $tuesdayCycle
     *
     * @ORM\Column(name="tuesday_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $tuesdayCycle;

    /**
     * @var string $wednesdayCycle
     *
     * @ORM\Column(name="wednesday_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $wednesdayCycle;


    /**
     * @var string $thursdayCycle
     *
     * @ORM\Column(name="thursday_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $thursdayCycle;

    /**
     * @var string $fridayCycle
     *
     * @ORM\Column(name="friday_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $fridayCycle;

    /**
     * @var string $saturdayCycle
     *
     * @ORM\Column(name="saturday_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $saturdayCycle;

    /**
     * @var string $mondayCycle
     *
     * @ORM\Column(name="sunday_cycle", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $sundayCycle;

    /**
     * @var datetime $cycleEndDate
     *
     * @ORM\Column(type="datetime", name="cycle_end_date", nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    private $cycleEndDate;

    /**
     * @var decimal $longDep
     *
     * @ORM\Column(name="long_dep", type="decimal", precision=17, scale=14, nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    private $longDep;

    /**
     * @var decimal $latDep
     *
     * @ORM\Column(name="lat_dep", type="decimal", precision=17, scale=14, nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    private $latDep;

    /**
     * @var decimal $longArr
     *
     * @ORM\Column(name="long_arr", type="decimal", precision=17, scale=14, nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    private $longArr;

    /**
     * @var decimal $latArr
     *
     * @ORM\Column(name="lat_arr", type="decimal", precision=17, scale=14, nullable=true)
     * @Expose
     * @Groups({"Public"})
     *
     */
    private $latArr;



    /*
     * *******************************
     * Back
     * *******************************
     */
    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="courseDriverBack", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="driver_back", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     */
    private $driverBack;

    /**
     * @var string $distanceBack
     *
     * @ORM\Column(name="distance_back", type="string", length=50, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $distanceBack;

    /**
     * @ORM\ManyToMany(targetEntity="CAB\CourseBundle\Entity\ServiceTransport", inversedBy="courseBack", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="cab_service_courses_back")
     */
    private $serviceTransportCoursesBack;

    /**
     * var datetime $backDate
     *
     * @ORM\Column(name="back_date", type="date", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $backDate;

    /**
     * var datetime $backTime
     *
     * @ORM\Column(name="back_time", type="time", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $backTime;

    /**
     * @var integer $forceHourArrivedBack
     *
     * @ORM\Column(name="force_hour_arrived_back", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $forceHourArrivedBack;

    /**
     * @var integer $arrivalHourBack
     *
     * @ORM\Column(name="arrival_hour_back", type="time", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $arrivalHourBack;

    /**
     * @var string $backAddressDeparture
     *
     * @ORM\Column(name="back_address_departure", type="text", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $backAddressDeparture;

    /**
     * @var string $indicatorBackAddressDeparture
     *
     * @ORM\Column(name="indicator_back_address_departure", type="text", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $indicatorBackAddressDeparture;

    /**
     * @var string $backAddressArrival
     *
     * @ORM\Column(name="back_address_arrival", type="text", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $backAddressArrival;

    /**
     * @var string $indicatorBackAddressArrival
     *
     * @ORM\Column(name="indicator_back_address_arrival", type="text", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $indicatorBackAddressArrival;

    /**
     * @var string $courseTimeBack
     *
     * @ORM\Column(name="course_time_back", type="string", length=128, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $courseTimeBack;

    /**
     * @var integer $personNumberBack
     *
     * @ORM\Column(name="person_number_back", type="integer", nullable=true)
     *
     * @Expose
     * @Groups({"Public"})
     */
    protected $personNumberBack;

    /**
     * @var string $laguageBack
     *
     * @ORM\Column(name="laguage_back", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $laguageBack;

    /**
     * @var string $nbChildren
     *
     * @ORM\Column(name="nb_children_back", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $nbChildrenBack;

    /**
     * @var string $nbWheelchairBack
     *
     * @ORM\Column(name="nb_wheelchair_back", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $nbWheelchairBack;

    /**
     * @var string $descriptionChildrenBack
     *
     * @ORM\Column(name="description_children_back", type="text", nullable=true)
     * @Expose
     */
    protected $descriptionChildrenBack;

    /**
     * @var float $priceHTBack
     *
     * @ORM\Column(name="price_ht_back", type="decimal", precision=8, scale=2, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $priceHTBack;

    /**
     * @var decimal $longDepBack
     *
     * @ORM\Column(name="long_dep_back", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $longDepBack;

    /**
     * @var decimal $latDepBack
     *
     * @ORM\Column(name="lat_dep_back", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     */
    private $latDepBack;

    /**
     * @var decimal $longArrBack
     *
     * @ORM\Column(name="long_arr_back", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $longArrBack;

    /**
     * @var decimal $latArrBack
     *
     * @ORM\Column(name="lat_arr_back", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $latArrBack;

    /**
     * @var string $approachTime
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $approachTime;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     * @Expose
     * @Groups({"Public"})
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Rating", mappedBy="course", cascade={"remove"}, fetch="EXTRA_LAZY")
     * @Expose
     * @Groups({"Public"})
     */
    private $ratings;

    /**
     * @var string $paymentMode
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $paymentMode;

    /**
     * @var string $isGroupage
     *
     * @ORM\Column(name="is_groupage", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $isGroupage;

    /**
     * @var $groupage
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Groupage", mappedBy="baseCourse", cascade={"remove", "persist"}, fetch="EXTRA_LAZY")
     * @Groups({"Public"})
     *
     */

    private $baseGroupage;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Groupage", inversedBy="linkedCourses", cascade={"remove", "persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="linked_groupage__id", referencedColumnName="id")
     */
    private $linkedGroupage;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CallcBundle\Entity\Callc", mappedBy="course", cascade={"remove"}, fetch="EXTRA_LAZY")
     */
    private $callc;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\CommandSncf", inversedBy="course", cascade={"remove", "persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="command_sncf_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $commandSncf;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Zone", inversedBy="course", cascade={"remove", "persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="zone_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $zone;



    /**
     * @var string $timeStartCourse;
     */
    private $timeStartCourse;

    /**
     * @ORM\Column(type="string", length=16, nullable=true);
     */
    private $timeBetweenArrivedAndStarted;



    /**
     * @ORM\PrePersist
     */
    public function setPrePersistCourse ()
    {
        $this->createdAt = new \DateTime();
        if ($this->getCourseStatus() == null) {
            $this->setCourseStatus(self::STATUS_CREATED);
        }

        if ($this->getDriver() !== null) {
	        if($this->getDriver()->getVehicle()->first() == false)
	        {
		        $this->setVehicle(null);
	        }
	        else{
		        $this->setVehicle($this->getDriver()->getVehicle()->first());
	        }
        }
    }

    /**
     *
     * @ORM\PostUpdate()
     */
    public function setPostUpdateCourse ()
    {
        if ($this->getDriver() !== null) {
	        if($this->getDriver()->getVehicle()->first() == false)
	        {
		        $this->setVehicle(null);
	        }
	        else
	        {
		        if(is_array($this->getDriver()->getVehicle()) && count($this->getDriver()->getVehicle()) == 1)
		        {
			        $this->setVehicle($this->getDriver()->getVehicle()->first());
		        }
	        }

        }
    }

    public function __toString()
    {
        if($this->getClient() !== null){
            $firstname = $this->getClient()->getFirstName();
            $lastname = $this->getClient()->getLastName() ;

        }else{
            $firstname = 'ERROR CLIENT';
            $lastname = 'ERROR CLIENT';
        }

        if ($this->getDepartureDate() !== null){
            $departureDate = $this->getDepartureDate()->format('d-m-Y');

        }else{
            $departureDate = 'ERROR DATE';
        }
        if ($this->getDepartureTime() !== null){
            $departureTime = $this->getDepartureTime()->format('H:i');

        }else{
            $departureTime = 'ERROR TIME';
        }
        if ($this->getStartedAddress() !== null){
            $startAddress = $this->getStartedAddress();

        }else{
            $startAddress = 'ERROR START ADDRESS';
        }

        if ($this->getArrivalAddress() !== null){
            $arrivalAddress = $this->getStartedAddress();

        }else{
            $arrivalAddress = 'ERROR ARRIVAL ADDRESS';
        }

        return 'COURSE (' . $this->getId() . ') ' .$firstname. ' ' .$lastname. ' DE ' .$departureDate. ' À ' .$departureTime. ' de ' .$startAddress. ' VERS ' .$arrivalAddress   ;
    }

    public static function toString($id)
    {
        return 'course'.$id;
    }

    public function getTimeStartCourse()
    {
        return $this->getDepartureTime()->format('H:i:s');
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startedAddress
     *
     * @param string $startedAddress
     * @return Course
     */
    public function setStartedAddress($startedAddress)
    {
        $this->startedAddress = $startedAddress;

        return $this;
    }

    /**
     * Get startedAddress
     *
     * @return string
     */
    public function getStartedAddress()
    {
        return $this->startedAddress;
    }

    /**
     * Set arrivalAddress
     *
     * @param string $arrivalAddress
     * @return Course
     */
    public function setArrivalAddress($arrivalAddress)
    {
        $this->arrivalAddress = $arrivalAddress;

        return $this;
    }

    /**
     * Get arrivalAddress
     *
     * @return string
     */
    public function getArrivalAddress()
    {
        return $this->arrivalAddress;
    }

    /**
     * Set courseTime
     *
     * @param string $courseTime
     * @return Course
     */
    public function setCourseTime($courseTime)
    {
        $this->courseTime = $courseTime;

        return $this;
    }

    /**
     * Get courseTime
     *
     * @return string
     */
    public function getCourseTime()
    {
        return $this->courseTime;
    }
    /**
     * Get courseTime
     *
     * @return string
     */
    public function getCourseTimeFormatted()
    {
        $time = round($this->courseTime,2);
        return $time;
    }

    /**
     * Set personNumber
     *
     * @param string $personNumber
     * @return Course
     */
    public function setPersonNumber($personNumber)
    {
        $this->personNumber = $personNumber;

        return $this;
    }

    /**
     * Get personNumber
     *
     * @return string
     */
    public function getPersonNumber()
    {
        return $this->personNumber;
    }

    /**
     * Set imperativeArrivalHour
     *
     * @param \DateTime $imperativeArrivalHour
     * @return Course
     */
    public function setImperativeArrivalHour($imperativeArrivalHour)
    {
        $this->imperativeArrivalHour = $imperativeArrivalHour;

        return $this;
    }

    /**
     * Get imperativeArrivalHour
     *
     * @return \DateTime
     */
    public function getImperativeArrivalHour()
    {
        return $this->imperativeArrivalHour;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Course
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set targetType
     *
     * @param string $targetType
     * @return Course
     */
    public function setTargetType($targetType)
    {
        $this->targetType = $targetType;

        return $this;
    }

    /**
     * Get targetType
     *
     * @return string
     */
    public function getTargetType()
    {
        return $this->targetType;
    }

    /**
     * Set courseStatus
     *
     * @param string $courseStatus
     * @return Course
     */
    public function setCourseStatus($courseStatus)
    {
        $this->courseStatus = $courseStatus;

        return $this;
    }

    /**
     * Get courseStatus
     *
     * @return string
     */
    public function getCourseStatus()
    {
        return $this->courseStatus;
    }

    /**
     * Get courseStatus
     *
     * @return string
     */
    public function getCourseStatusFormatted()
    {

        $status=  $this->courseStatus ;

        if ($status == 3){
            $status = 'Annulé par le client';
        } elseif ($status == 4 ) {
            $status = 'Affecté';
        } elseif ($status == 6 ) {
            $status = 'Réalisé';
        } elseif ($status == 8 ) {
            $status = 'Annulation Admin';
        } elseif ($status == 9 ) {
            $status = 'En course';
        } elseif ($status == 13 ) {
            $status = 'Chauffeur en attente';
        } elseif ($status == 15 ) {
            $status = 'Annulation par le chauffeur';
        } elseif ($status == 18 ) {
            $status = 'Chauffeur en route vers pec';
        } elseif ($status == 19 ) {
            $status = 'Annulation par Agent';
        } elseif ($status == 21 ) {
            $status = 'Annulation Galapagoss';
        } elseif ($status == 22 ) {
            $status = 'Annulation Délai';
        } elseif ($status == 24 ) {
                $status = 'Déplacement Inutile';
        } else {
            $statut = 'voir admin';
        }
        return $status;
    }




    /**
     * Set isHandicap
     *
     * @param boolean $isHandicap
     * @return Course
     */
    public function setIsHandicap($isHandicap)
    {
        $this->isHandicap = $isHandicap;

        return $this;
    }

    /**
     * Get isHandicap
     *
     * @return boolean
     */
    public function getIsHandicap()
    {
        return $this->isHandicap;
    }

    /**
     * Set transportType
     *
     * @param string $transportType
     * @return Course
     */
    public function setTransportType($transportType)
    {
        $this->transportType = $transportType;

        return $this;
    }

    /**
     * Get transportType
     *
     * @return string
     */
    public function getTransportType()
    {
        return $this->transportType;
    }

    /**
     * Set client
     *
     * @param \CAB\UserBundle\Entity\User $client
     * @return Course
     */
    public function setClient(\CAB\UserBundle\Entity\User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set vehicle
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicle
     * @return Course
     */
    public function setVehicle(\CAB\CourseBundle\Entity\Vehicule $vehicle = null)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return \CAB\CourseBundle\Entity\Vehicule
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set departureDate
     *
     * @param \DateTime $departureDate
     * @return Course
     */
    public function setDepartureDate($departureDate)
    {
        $this->departureDate = $departureDate;

        return $this;
    }

    /**
     * Get departureDate
     *
     * @return \DateTime
     */
    public function getDepartureDate()
    {
        return $this->departureDate;
    }

    public function getDepartureDateFormatted() {
        return ($this->departureDate instanceof \DateTime) ? $this->departureDate->format("d/m/Y") : "";
    }

    /**
     * Set backDate
     *
     * @param \DateTime $backDate
     * @return Course
     */
    public function setBackDate($backDate)
    {
        $this->backDate = $backDate;

        return $this;
    }

    /**
     * Get backDate
     *
     * @return \DateTime
     */
    public function getBackDate()
    {
        return $this->backDate;
    }

    /**
     * Set distance
     *
     * @param string $distance
     * @return Course
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return string
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set priceHT
     *
     * @param float $priceHT
     * @return Course
     */
    public function setPriceHT($priceHT)
    {
        $this->priceHT = $priceHT;

        return $this;
    }

    /**
     * Get priceHT
     *
     * @return float
     */
    public function getPriceHT()
    {
        return $this->priceHT;
    }

    /**
     * Set createdBy
     *
     * @param \CAB\UserBundle\Entity\User $createdBy
     * @return Course
     */
    public function setCreatedBy(\CAB\UserBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return Course
     */
    public function setDriver(\CAB\UserBundle\Entity\User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->serviceTransportCourses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->serviceTransportCoursesBack = new \Doctrine\Common\Collections\ArrayCollection();
        $this->serviceParticularPeriodCourses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set backAddressDeparture
     *
     * @param string $backAddressDeparture
     * @return Course
     */
    public function setBackAddressDeparture($backAddressDeparture)
    {
        $this->backAddressDeparture = $backAddressDeparture;

        return $this;
    }

    /**
     * Get backAddressDeparture
     *
     * @return string
     */
    public function getBackAddressDeparture()
    {
        return $this->backAddressDeparture;
    }

    /**
     * Set backAddressArrival
     *
     * @param string $backAddressArrival
     * @return Course
     */
    public function setBackAddressArrival($backAddressArrival)
    {
        $this->backAddressArrival = $backAddressArrival;

        return $this;
    }

    /**
     * Get backAddressArrival
     *
     * @return string
     */
    public function getBackAddressArrival()
    {
        return $this->backAddressArrival;
    }

    /**
     * Set courseTimeBack
     *
     * @param \DateTime $courseTimeBack
     * @return Course
     */
    public function setCourseTimeBack($courseTimeBack)
    {
        $this->courseTimeBack = $courseTimeBack;

        return $this;
    }

    /**
     * Get courseTimeBack
     *
     * @return string
     */
    public function getCourseTimeBack()
    {
        return $this->courseTimeBack;
    }

    /**
     * Set distanceBack
     *
     * @param string $distanceBack
     * @return Course
     */
    public function setDistanceBack($distanceBack)
    {
        $this->distanceBack = $distanceBack;

        return $this;
    }

    /**
     * Get distanceBack
     *
     * @return string
     */
    public function getDistanceBack()
    {
        return $this->distanceBack;
    }

    /**
     * Set forceHourArrived
     *
     * @param boolean $forceHourArrived
     * @return Course
     */
    public function setForceHourArrived($forceHourArrived)
    {
        $this->forceHourArrived = $forceHourArrived;

        return $this;
    }

    /**
     * Get forceHourArrived
     *
     * @return boolean
     */
    public function getForceHourArrived()
    {
        return $this->forceHourArrived;
    }

    /**
     * Set arrivalHour
     *
     * @param \DateTime $arrivalHour
     * @return Course
     */
    public function setArrivalHour($arrivalHour)
    {
        $this->arrivalHour = $arrivalHour;

        return $this;
    }

    /**
     * Get arrivalHour
     *
     * @return \DateTime
     */
    public function getArrivalHour()
    {
        return $this->arrivalHour;
    }

    /**
     * Set laguage
     *
     * @param integer $laguage
     * @return Course
     */
    public function setLaguage($laguage)
    {
        $this->laguage = $laguage;

        return $this;
    }

    /**
     * Get laguage
     *
     * @return integer
     */
    public function getLaguage()
    {
        return $this->laguage;
    }

    /**
     * Set nbWheelchair
     *
     * @param integer $nbWheelchair
     * @return Course
     */
    public function setNbWheelchair($nbWheelchair)
    {
        $this->nbWheelchair = $nbWheelchair;

        return $this;
    }

    /**
     * Get nbWheelchair
     *
     * @return integer
     */
    public function getNbWheelchair()
    {
        return $this->nbWheelchair;
    }

    /**
     * Set nbChildren
     *
     * @param integer $nbChildren
     * @return Course
     */
    public function setNbChildren($nbChildren)
    {
        $this->nbChildren = $nbChildren;

        return $this;
    }

    /**
     * Get nbChildren
     *
     * @return integer
     */
    public function getNbChildren()
    {
        return $this->nbChildren;
    }

    /**
     * Set descriptionChildren
     *
     * @param string $descriptionChildren
     * @return Course
     */
    public function setDescriptionChildren($descriptionChildren)
    {
        $this->descriptionChildren = $descriptionChildren;

        return $this;
    }

    /**
     * Get descriptionChildren
     *
     * @return string
     */
    public function getDescriptionChildren()
    {
        return $this->descriptionChildren;
    }

    /**
     * Set isGroup
     *
     * @param boolean $isGroup
     * @return Course
     */
    public function setIsGroup($isGroup)
    {
        $this->isGroup = $isGroup;

        return $this;
    }

    /**
     * Get isGroup
     *
     * @return boolean
     */
    public function getIsGroup()
    {
        return $this->isGroup;
    }

    /**
     * Set isCycle
     *
     * @param boolean $isCycle
     * @return Course
     */
    public function setIsCycle($isCycle)
    {
        $this->isCycle = $isCycle;

        return $this;
    }

    /**
     * Get isCycle
     *
     * @return boolean
     */
    public function getIsCycle()
    {
        return $this->isCycle;
    }

    /**
     * Set mondayCycle
     *
     * @param boolean $mondayCycle
     * @return Course
     */
    public function setMondayCycle($mondayCycle)
    {
        $this->mondayCycle = $mondayCycle;

        return $this;
    }

    /**
     * Get mondayCycle
     *
     * @return boolean
     */
    public function getMondayCycle()
    {
        return $this->mondayCycle;
    }

    /**
     * Set thurdayCycle
     *
     * @param boolean $thurdayCycle
     * @return Course
     */
    public function setThurdayCycle($thurdayCycle)
    {
        $this->thurdayCycle = $thurdayCycle;

        return $this;
    }

    /**
     * Get thurdayCycle
     *
     * @return boolean
     */
    public function getThurdayCycle()
    {
        return $this->thurdayCycle;
    }

    /**
     * Set wednesdayCycle
     *
     * @param boolean $wednesdayCycle
     * @return Course
     */
    public function setWednesdayCycle($wednesdayCycle)
    {
        $this->wednesdayCycle = $wednesdayCycle;

        return $this;
    }

    /**
     * Get wednesdayCycle
     *
     * @return boolean
     */
    public function getWednesdayCycle()
    {
        return $this->wednesdayCycle;
    }

    /**
     * Set tuesdayCycle
     *
     * @param boolean $tuesdayCycle
     * @return Course
     */
    public function setTuesdayCycle($tuesdayCycle)
    {
        $this->tuesdayCycle = $tuesdayCycle;

        return $this;
    }

    /**
     * Get tuesdayCycle
     *
     * @return boolean
     */
    public function getTuesdayCycle()
    {
        return $this->tuesdayCycle;
    }

    /**
     * Set fridayCycle
     *
     * @param boolean $fridayCycle
     * @return Course
     */
    public function setFridayCycle($fridayCycle)
    {
        $this->fridayCycle = $fridayCycle;

        return $this;
    }

    /**
     * Get fridayCycle
     *
     * @return boolean
     */
    public function getFridayCycle()
    {
        return $this->fridayCycle;
    }

    /**
     * Set saturdayCycle
     *
     * @param boolean $saturdayCycle
     * @return Course
     */
    public function setSaturdayCycle($saturdayCycle)
    {
        $this->saturdayCycle = $saturdayCycle;

        return $this;
    }

    /**
     * Get saturdayCycle
     *
     * @return boolean
     */
    public function getSaturdayCycle()
    {
        return $this->saturdayCycle;
    }

    /**
     * Set sundayCycle
     *
     * @param boolean $sundayCycle
     * @return Course
     */
    public function setSundayCycle($sundayCycle)
    {
        $this->sundayCycle = $sundayCycle;

        return $this;
    }

    /**
     * Get sundayCycle
     *
     * @return boolean
     */
    public function getSundayCycle()
    {
        return $this->sundayCycle;
    }

    /**
     * Set cycleEndDate
     *
     * @param \DateTime $cycleEndDate
     * @return Course
     */
    public function setCycleEndDate($cycleEndDate)
    {
        $this->cycleEndDate = $cycleEndDate;

        return $this;
    }

    /**
     * Get cycleEndDate
     *
     * @return \DateTime
     */
    public function getCycleEndDate()
    {
        return $this->cycleEndDate;
    }

    /**
     * Set driverBack
     *
     * @param \CAB\UserBundle\Entity\User $driverBack
     * @return Course
     */
    public function setDriverBack(\CAB\UserBundle\Entity\User $driverBack = null)
    {
        $this->driverBack = $driverBack;

        return $this;
    }

    /**
     * Get driverBack
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriverBack()
    {
        return $this->driverBack;
    }

    /**
     * Add serviceTransportCourses
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCourses
     * @return Course
     */
    public function addServiceTransportCourse(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCourses)
    {
        $this->serviceTransportCourses[] = $serviceTransportCourses;

        return $this;
    }

    /**
     * Remove serviceTransportCourses
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCourses
     */
    public function removeServiceTransportCourse(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCourses)
    {
        $this->serviceTransportCourses->removeElement($serviceTransportCourses);
    }

    /**
     * Get serviceTransportCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransportCourses()

    {
    	return $this->serviceTransportCourses;

    }

    /**
     * Get serviceTransportCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransportCoursesCtr()

    {
        $serviceCtr = $this->getServiceTransportCourses();
        $valueCtr = '';

        foreach ($serviceCtr as $serviceCtrs) {

            if ($serviceCtrs->getCtr()) {
                $valueCtr = $serviceCtrs->getctr();
            }
        }

        return $valueCtr;
    }

    /**
     * Get serviceTransportCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransportCoursesRef()

    {
        $serviceCtr = $this->getServiceTransportCourses();
        $valueCtr = '';

        foreach ($serviceCtr as $serviceCtrs) {

            if ($serviceCtrs->getRef()) {
                $valueCtr = $serviceCtrs->getref();
            }
        }

        return $valueCtr;
    }

    /**
     * Get serviceTransportCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransportCoursesMessage()

    {
        $serviceCtr = $this->getServiceTransportCourses();
        $value = '';

        foreach ($serviceCtr as $serviceCtrs) {

            if ($serviceCtrs->getMessage()) {
                $value = $serviceCtrs->getmessage();
            }
        }

        return $value;
    }

    /**
     * Get serviceTransportCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransportCoursesTel()

    {
        $service = $this->getServiceTransportCourses();
        $value = '';

        foreach ($service as $serviceCtrs) {

            if ($serviceCtrs->getTel()) {
                $value = $serviceCtrs->gettel();
            }
        }

        return $value;
    }

    /**
     * Get serviceTransportCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransportCoursesserviceEmailCompta()

    {
        $serviceCtr = $this->getServiceTransportCourses();
        $valueCtr = '';

        foreach ($serviceCtr as $serviceCtrs) {

            if ($serviceCtrs->getserviceEmailCompta()) {
                $valueCtr = $serviceCtrs->getserviceEmailCompta();
            }
        }

        return $valueCtr;
    }

    /**
     * Add serviceTransportCoursesBack
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCoursesBack
     * @return Course
     */
    public function addServiceTransportCoursesBack(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCoursesBack)
    {
        $this->serviceTransportCoursesBack[] = $serviceTransportCoursesBack;

        return $this;
    }

    /**
     * Remove serviceTransportCoursesBack
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCoursesBack
     */
    public function removeServiceTransportCoursesBack(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransportCoursesBack)
    {
        $this->serviceTransportCoursesBack->removeElement($serviceTransportCoursesBack);
    }

    /**
     * Get serviceTransportCoursesBack
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransportCoursesBack()
    {
        return $this->serviceTransportCoursesBack;
    }

    /**
     * Set thursdayCycle
     *
     * @param boolean $thursdayCycle
     * @return Course
     */
    public function setThursdayCycle($thursdayCycle)
    {
        $this->thursdayCycle = $thursdayCycle;

        return $this;
    }

    /**
     * Get thursdayCycle
     *
     * @return boolean
     */
    public function getThursdayCycle()
    {
        return $this->thursdayCycle;
    }

    /**
     * Set personNumberBack
     *
     * @param integer $personNumberBack
     * @return Course
     */
    public function setPersonNumberBack($personNumberBack)
    {
        $this->personNumberBack = $personNumberBack;

        return $this;
    }

    /**
     * Get personNumberBack
     *
     * @return integer
     */
    public function getPersonNumberBack()
    {
        return $this->personNumberBack;
    }

    /**
     * Set laguageBack
     *
     * @param integer $laguageBack
     * @return Course
     */
    public function setLaguageBack($laguageBack)
    {
        $this->laguageBack = $laguageBack;

        return $this;
    }

    /**
     * Get laguageBack
     *
     * @return integer
     */
    public function getLaguageBack()
    {
        return $this->laguageBack;
    }

    /**
     * Set nbChildrenBack
     *
     * @param integer $nbChildrenBack
     * @return Course
     */
    public function setNbChildrenBack($nbChildrenBack)
    {
        $this->nbChildrenBack = $nbChildrenBack;

        return $this;
    }

    /**
     * Get nbChildrenBack
     *
     * @return integer
     */
    public function getNbChildrenBack()
    {
        return $this->nbChildrenBack;
    }

    /**
     * Set nbWheelchairBack
     *
     * @param integer $nbWheelchairBack
     * @return Course
     */
    public function setNbWheelchairBack($nbWheelchairBack)
    {
        $this->nbWheelchairBack = $nbWheelchairBack;

        return $this;
    }

    /**
     * Get nbWheelchairBack
     *
     * @return integer
     */
    public function getNbWheelchairBack()
    {
        return $this->nbWheelchairBack;
    }

    /**
     * Set descriptionChildrenBack
     *
     * @param string $descriptionChildrenBack
     * @return Course
     */
    public function setDescriptionChildrenBack($descriptionChildrenBack)
    {
        $this->descriptionChildrenBack = $descriptionChildrenBack;

        return $this;
    }

    /**
     * Get descriptionChildrenBack
     *
     * @return string
     */
    public function getDescriptionChildrenBack()
    {
        return $this->descriptionChildrenBack;
    }

    /**
     * Set priceHTBack
     *
     * @param float $priceHTBack
     * @return Course
     */
    public function setPriceHTBack($priceHTBack)
    {
        $this->priceHTBack = $priceHTBack;

        return $this;
    }

    /**
     * Get priceHTBack
     *
     * @return float
     */
    public function getPriceHTBack()
    {
        return $this->priceHTBack;
    }

    /**
     * Set forceHourArrivedBack
     *
     * @param boolean $forceHourArrivedBack
     * @return Course
     */
    public function setForceHourArrivedBack($forceHourArrivedBack)
    {
        $this->forceHourArrivedBack = $forceHourArrivedBack;

        return $this;
    }

    /**
     * Get forceHourArrivedBack
     *
     * @return boolean
     */
    public function getForceHourArrivedBack()
    {
        return $this->forceHourArrivedBack;
    }

    /**
     * Set arrivalHourBack
     *
     * @param \DateTime $arrivalHourBack
     * @return Course
     */
    public function setArrivalHourBack($arrivalHourBack)
    {
        $this->arrivalHourBack = $arrivalHourBack;

        return $this;
    }

    /**
     * Get arrivalHourBack
     *
     * @return \DateTime
     */
    public function getArrivalHourBack()
    {
        return $this->arrivalHourBack;
    }

    /**
     * Set longDep
     *
     * @param string $longDep
     * @return Course
     */
    public function setLongDep($longDep)
    {
        $this->longDep = $longDep;

        return $this;
    }

    /**
     * Get longDep
     *
     * @return string
     */
    public function getLongDep()
    {
        return $this->longDep;
    }

    /**
     * Set latDep
     *
     * @param string $latDep
     * @return Course
     */
    public function setLatDep($latDep)
    {
        $this->latDep = $latDep;

        return $this;
    }

    /**
     * Get latDep
     *
     * @return string
     */
    public function getLatDep()
    {
        return $this->latDep;
    }

    /**
     * Set longArr
     *
     * @param string $longArr
     * @return Course
     */
    public function setLongArr($longArr)
    {
        $this->longArr = $longArr;

        return $this;
    }

    /**
     * Get longArr
     *
     * @return string
     */
    public function getLongArr()
    {
        return $this->longArr;
    }

    /**
     * Set latArr
     *
     * @param string $latArr
     * @return Course
     */
    public function setLatArr($latArr)
    {
        $this->latArr = $latArr;

        return $this;
    }

    /**
     * Get latArr
     *
     * @return string
     */
    public function getLatArr()
    {
        return $this->latArr;
    }

    /**
     * Set longDepBack
     *
     * @param string $longDepBack
     * @return Course
     */
    public function setLongDepBack($longDepBack)
    {
        $this->longDepBack = $longDepBack;

        return $this;
    }

    /**
     * Get longDepBack
     *
     * @return string
     */
    public function getLongDepBack()
    {
        return $this->longDepBack;
    }

    /**
     * Set latDepBack
     *
     * @param string $latDepBack
     * @return Course
     */
    public function setLatDepBack($latDepBack)
    {
        $this->latDepBack = $latDepBack;

        return $this;
    }

    /**
     * Get latDepBack
     *
     * @return string
     */
    public function getLatDepBack()
    {
        return $this->latDepBack;
    }

    /**
     * Set longArrBack
     *
     * @param string $longArrBack
     * @return Course
     */
    public function setLongArrBack($longArrBack)
    {
        $this->longArrBack = $longArrBack;

        return $this;
    }

    /**
     * Get longArrBack
     *
     * @return string
     */
    public function getLongArrBack()
    {
        return $this->longArrBack;
    }

    /**
     * Set latArrBack
     *
     * @param string $latArrBack
     * @return Course
     */
    public function setLatArrBack($latArrBack)
    {
        $this->latArrBack = $latArrBack;

        return $this;
    }

    /**
     * Get latArrBack
     *
     * @return string
     */
    public function getLatArrBack()
    {
        return $this->latArrBack;
    }

    /**
     * Set typeVehicleCourse
     *
     * @param \CAB\CourseBundle\Entity\TypeVehicule $typeVehicleCourse
     * @return Course
     */
    public function setTypeVehicleCourse(\CAB\CourseBundle\Entity\TypeVehicule $typeVehicleCourse = null)
    {
        $this->typeVehicleCourse = $typeVehicleCourse;

        return $this;
    }

    /**
     * Get typeVehicleCourse
     *
     * @return \CAB\CourseBundle\Entity\TypeVehicule
     */
    public function getTypeVehicleCourse()
    {
        return $this->typeVehicleCourse;
    }

    /**
     * Set departureTime
     *
     * @param \DateTime $departureTime
     * @return Course
     */
    public function setDepartureTime($departureTime)
    {
        $this->departureTime = $departureTime;

        return $this;
    }

    /**
     * Get departureTime
     *
     * @return \DateTime
     */
    public function getDepartureTime()
    {
        return $this->departureTime;
    }

    public function getDepartureTimeFormatted() {
        return ($this->departureTime instanceof \DateTime) ? $this->departureTime->format("H:i") : "";
    }

    /**
     * Set backTime
     *
     * @param \DateTime $backTime
     * @return Course
     */
    public function setBackTime($backTime)
    {
        $this->backTime = $backTime;

        return $this;
    }

    /**
     * Get backTime
     *
     * @return \DateTime
     */
    public function getBackTime()
    {
        return $this->backTime;
    }

    /**
     * Set indicatorStartedAddress
     *
     * @param string $indicatorStartedAddress
     * @return Course
     */
    public function setIndicatorStartedAddress($indicatorStartedAddress)
    {
        $this->indicatorStartedAddress = $indicatorStartedAddress;

        return $this;
    }

    /**
     * Get indicatorStartedAddress
     *
     * @return string
     */
    public function getIndicatorStartedAddress()
    {
        return $this->indicatorStartedAddress;
    }

    /**
     * Set indicatorArrivalAddress
     *
     * @param string $indicatorArrivalAddress
     * @return Course
     */
    public function setIndicatorArrivalAddress($indicatorArrivalAddress)
    {
        $this->indicatorArrivalAddress = $indicatorArrivalAddress;

        return $this;
    }

    /**
     * Get indicatorArrivalAddress
     *
     * @return string
     */
    public function getIndicatorArrivalAddress()
    {
        return $this->indicatorArrivalAddress;
    }

    /**
     * Set indicatorBackAddressDeparture
     *
     * @param string $indicatorBackAddressDeparture
     * @return Course
     */
    public function setIndicatorBackAddressDeparture($indicatorBackAddressDeparture)
    {
        $this->indicatorBackAddressDeparture = $indicatorBackAddressDeparture;

        return $this;
    }

    /**
     * Get indicatorBackAddressDeparture
     *
     * @return string
     */
    public function getIndicatorBackAddressDeparture()
    {
        return $this->indicatorBackAddressDeparture;
    }

    /**
     * Set indicatorBackAddressArrival
     *
     * @param string $indicatorBackAddressArrival
     * @return Course
     */
    public function setIndicatorBackAddressArrival($indicatorBackAddressArrival)
    {
        $this->indicatorBackAddressArrival = $indicatorBackAddressArrival;

        return $this;
    }

    /**
     * Get indicatorBackAddressArrival
     *
     * @return string
     */
    public function getIndicatorBackAddressArrival()
    {
        return $this->indicatorBackAddressArrival;
    }

    /**
     * Set applicableTarif
     *
     * @param \CAB\CourseBundle\Entity\Tarif $applicableTarif
     * @return Course
     */
    public function setApplicableTarif(\CAB\CourseBundle\Entity\Tarif $applicableTarif = null)
    {
        $this->applicableTarif = $applicableTarif;

        return $this;
    }

    /**
     * Get applicableTarif
     *
     * @return \CAB\CourseBundle\Entity\Tarif
     */
    public function getApplicableTarif()
    {
        return $this->applicableTarif;
    }

    /**
     * Get slowWalkPerHour Tarif
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSlowWalkPerHourTarif()
    {
        $value = 25.00 ;
        return $value;
    }



    /**
     * Set courseDetailsTarif
     *
     * @param \CAB\CourseBundle\Entity\DetailsTarif $courseDetailsTarif
     * @return Course
     */
    public function setCourseDetailsTarif(\CAB\CourseBundle\Entity\DetailsTarif $courseDetailsTarif = null)
    {
        $this->courseDetailsTarif = $courseDetailsTarif;

        return $this;
    }

    /**
     * Get courseDetailsTarif
     *
     * @return \CAB\CourseBundle\Entity\DetailsTarif
     */
    public function getCourseDetailsTarif()
    {
        return $this->courseDetailsTarif;
    }

    /**
     * Add orderCourse
     *
     * @param \CAB\CourseBundle\Entity\Order $orderCourse
     * @return Course
     */
    public function addOrderCourse(\CAB\CourseBundle\Entity\Order $orderCourse)
    {
        $this->orderCourse[] = $orderCourse;

        return $this;
    }

    /**
     * Remove orderCourse
     *
     * @param \CAB\CourseBundle\Entity\Order $orderCourse
     */
    public function removeOrderCourse(\CAB\CourseBundle\Entity\Order $orderCourse)
    {
        $this->orderCourse->removeElement($orderCourse);
    }

    /**
     * Get orderCourse
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderCourse()
    {
        return $this->orderCourse;
    }

    /**
     * Set courseTransactionPayment
     *
     * @param \CAB\CourseBundle\Entity\TransactionPayment $courseTransactionPayment
     * @return Course
     */
    public function setCourseTransactionPayment(\CAB\CourseBundle\Entity\TransactionPayment $courseTransactionPayment = null)
    {
        $this->courseTransactionPayment = $courseTransactionPayment;

        return $this;
    }

    /**
     * Get courseTransactionPayment
     *
     * @return \CAB\CourseBundle\Entity\TransactionPayment
     */
    public function getCourseTransactionPayment()
    {
        return $this->courseTransactionPayment;
    }

    /**
     * Add planingCourse
     *
     * @param \CAB\CourseBundle\Entity\Planing $planingCourse
     * @return Course
     */
    public function addPlaningCourse(\CAB\CourseBundle\Entity\Planing $planingCourse)
    {
        $this->planingCourse[] = $planingCourse;

        return $this;
    }

    /**
     * Remove planingCourse
     *
     * @param \CAB\CourseBundle\Entity\Planing $planingCourse
     */
    public function removePlaningCourse(\CAB\CourseBundle\Entity\Planing $planingCourse)
    {
        $this->planingCourse->removeElement($planingCourse);
    }

    /**
     * Get planingCourse
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlaningCourse()
    {
        return $this->planingCourse;
    }

    /**
     * Add courseThread
     *
     * @param \CAB\MessageBundle\Entity\Thread $courseThread
     * @return Course
     */
    public function addCourseThread(\CAB\MessageBundle\Entity\Thread $courseThread)
    {
        $this->courseThread[] = $courseThread;

        return $this;
    }

    /**
     * Remove courseThread
     *
     * @param \CAB\MessageBundle\Entity\Thread $courseThread
     */
    public function removeCourseThread(\CAB\MessageBundle\Entity\Thread $courseThread)
    {
        $this->courseThread->removeElement($courseThread);
    }

    /**
     * Get courseThread
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseThread()
    {
        return $this->courseThread;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return Course
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set priceTTC
     *
     * @param float $priceTTC
     * @return Course
     */
    public function setPriceTTC($priceTTC)
    {
        $this->priceTTC = $priceTTC;

        return $this;
    }

    /**
     * Get priceTTC
     *
     * @return float
     */
    public function getPriceTTC()
    {
        return $this->priceTTC;
    }

    /**
     * Add ratings
     *
     * @param \CAB\CourseBundle\Entity\Rating $ratings
     * @return Course
     */
    public function addRating(Rating $ratings)
    {
        $this->ratings[] = $ratings;

        return $this;
    }

    /**
     * Remove ratings
     *
     * @param \CAB\CourseBundle\Entity\Rating $ratings
     */
    public function removeRating(Rating $ratings)
    {
        $this->ratings->removeElement($ratings);
    }

    /**
     * Get ratings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRatings()
    {
        return $this->ratings;
    }

    /**
     * Set approachTime
     *
     * @param string $approachTime
     * @return Course
     */
    public function setApproachTime($approachTime)
    {
        $this->approachTime = $approachTime;

        return $this;
    }

    /**
     * Get approachTime
     *
     * @return string
     */
    public function getApproachTime()
    {
        return $this->approachTime;
    }

    /**
     * Add courseHistory
     *
     * @param \CAB\CourseBundle\Entity\CourseHistory $courseHistory
     * @return Course
     */
    public function addCourseHistory(\CAB\CourseBundle\Entity\CourseHistory $courseHistory)
    {
        $this->courseHistory[] = $courseHistory;

        return $this;
    }

    /**
     * Remove courseHistory
     *
     * @param \CAB\CourseBundle\Entity\CourseHistory $courseHistory
     */
    public function removeCourseHistory(\CAB\CourseBundle\Entity\CourseHistory $courseHistory)
    {
        $this->courseHistory->removeElement($courseHistory);
    }

    /**
     * Get courseHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseHistory()
    {
        return $this->courseHistory;
    }

    /**
     * Set paymentMode
     *
     * @param string $paymentMode
     * @return Course
     */
    public function setPaymentMode($paymentMode)
    {
        $this->paymentMode = $paymentMode;

        return $this;
    }

    /**
     * Get paymentMode
     *
     * @return string
     */
    public function getPaymentMode()
    {
        return $this->paymentMode;
    }

    public function getCourseDescription()
    {
        return self::courseDescription($this->courseStatus);
    }

    public static function courseDescription($status)
    {
        switch ($status)
        {
            case self::STATUS_CREATED:
                return 'status_created_label';
            case self::STATUS_PAYED:
                return 'status_payed_label';
            case self::STATUS_REJECTED:
                return 'status_rejected_label';
            case self::STATUS_CANCELLED_BY_CLIENT:
                return 'status_cancelled_by_client_label';
            case self::STATUS_AFFECTED:
                return 'status_affected_label';
            case self::STATUS_UPDATED_BY_CUSTOMER:
                return 'status_updated_by_customer_label';
            case self::STATUS_DONE:
                return 'status_done_label';
            case self::STATUS_PAYED_AND_UPDATED_BY_CUSTOMER:
                return 'status_payed_and_updated_by_customer_label';
            case self::STATUS_CANCELLED_BY_ADMIN:
                return 'status_cancelled_by_admin_label';
            case self::STATUS_ON_RACE_DRIVER:
                return 'status_on_race_label';
            case self::STATUS_CACH_PAY:
                return 'status_cach_pay_label';
            case self::STATUS_ATTENTE_PAY:
                return 'status_attente_pay_label';
            case self::STATUS_ACCEPTED_DRIVER:
                return 'status_accepted_label';
            case self::STATUS_ARRIVED_DRIVER:
                return 'status_arrived_label';
            case self::STATUS_NO_REPONSE:
                return 'status_no_reponse_label';
            case self::STATUS_CANCELLED_BY_DRIVER:
                return 'status_cancelled_by_driver_label';
            default:
                return 'status_unknown_label';
        }
    }

    /**
     * Add serviceParticularPeriodCourses
     *
     * @param \CAB\CourseBundle\Entity\ParticularPeriod $serviceParticularPeriodCourses
     * @return Course
     */
    public function addServiceParticularPeriodCourse(\CAB\CourseBundle\Entity\ParticularPeriod $serviceParticularPeriodCourses)
    {
        $this->serviceParticularPeriodCourses[] = $serviceParticularPeriodCourses;

        return $this;
    }

    /**
     * Remove serviceParticularPeriodCourses
     *
     * @param \CAB\CourseBundle\Entity\ParticularPeriod $serviceParticularPeriodCourses
     */
    public function removeServiceParticularPeriodCourse(\CAB\CourseBundle\Entity\ParticularPeriod $serviceParticularPeriodCourses)
    {
        $this->serviceParticularPeriodCourses->removeElement($serviceParticularPeriodCourses);
    }

    /**
     * Get serviceParticularPeriodCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceParticularPeriodCourses()
    {
        return $this->serviceParticularPeriodCourses;
    }

    /**
     * Set cycleParent
     *
     * @param integer $cycleParent
     * @return Course
     */
    public function setCycleParent($cycleParent)
    {
        $this->cycleParent = $cycleParent;

        return $this;
    }

    /**
     * Get cycleParent
     *
     * @return integer
     */
    public function getCycleParent()
    {
        return $this->cycleParent;
    }

    /**
     * Set dateStartParent
     *
     * @param \DateTime $dateStartParent
     * @return Course
     */
    public function setDateStartParent($dateStartParent)
    {
        $this->dateStartParent = $dateStartParent;

        return $this;
    }

    /**
     * Get dateStartParent
     *
     * @return \DateTime
     */
    public function getDateStartParent()
    {
        return $this->dateStartParent;
    }

    /**
     * Set dateEndParent
     *
     * @param \DateTime $dateEndParent
     * @return Course
     */
    public function setDateEndParent($dateEndParent)
    {
        $this->dateEndParent = $dateEndParent;

        return $this;
    }

    /**
     * Get dateEndParent
     *
     * @return \DateTime
     */
    public function getDateEndParent()
    {
        return $this->dateEndParent;
    }

    /**
     * Set timeCycleParent
     *
     * @param \DateTime $timeCycleParent
     * @return Course
     */
    public function setTimeCycleParent($timeCycleParent)
    {
        $this->timeCycleParent = $timeCycleParent;

        return $this;
    }

    /**
     * Get timeCycleParent
     *
     * @return \DateTime
     */
    public function getTimeCycleParent()
    {
        return $this->timeCycleParent;
    }

    /**
     * Set daysCycle
     *
     * @param string $daysCycle
     * @return Course
     */
    public function setDaysCycle($daysCycle)
    {
        $this->daysCycle = $daysCycle;

        return $this;
    }

    /**
     * Get daysCycle
     *
     * @return string
     */
    public function getDaysCycle()
    {
        return $this->daysCycle;
    }

    /**
     * Set isGroupage
     *
     * @param boolean $isGroupage
     * @return Course
     */
    public function setIsGroupage ($isGroupage)
    {
        $this->isGroupage = $isGroupage;

        return $this;
    }

    /**
     * Get isGroupage
     *
     * @return boolean
     */
    public function getIsGroupage ()
    {
        return $this->isGroupage;
    }

    /**
     * Set baseGroupage
     *
     * @param \CAB\CourseBundle\Entity\Groupage $baseGroupage
     * @return Course
     */
    public function setBaseGroupage(\CAB\CourseBundle\Entity\Groupage $baseGroupage = null)
    {
        $this->baseGroupage = $baseGroupage;

        return $this;
    }

    /**
     * Get baseGroupage
     *
     * @return \CAB\CourseBundle\Entity\Groupage
     */
    public function getBaseGroupage()
    {
        return $this->baseGroupage;
    }

    /**
     * Set linkedGroupage
     *
     * @param \CAB\CourseBundle\Entity\Groupage $linkedGroupage
     * @return Course
     */
    public function setLinkedGroupage(\CAB\CourseBundle\Entity\Groupage $linkedGroupage = null)
    {
        $this->linkedGroupage = $linkedGroupage;

        return $this;
    }

    /**
     * Get linkedGroupage
     *
     * @return \CAB\CourseBundle\Entity\Groupage
     */
    public function getLinkedGroupage()
    {
        return $this->linkedGroupage;
    }

    public function displayName()
    {
        return $this->getId() . ' From ' . $this->getStartedAddress() . ' on ' .
        $this->getDepartureDate()->format('Y-m-d') .
        ' at ' . $this->getDeparturetime()->format('H:i');
    }

    /**
     * Set ticket
     *
     * @param \CAB\TicketBundle\Entity\Ticket $ticket
     * @return Course
     */
    public function setTicket(\CAB\TicketBundle\Entity\Ticket $ticket = null)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return \CAB\TicketBundle\Entity\Ticket
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Add callc
     *
     * @param \CAB\CallcBundle\Entity\Callc $callc
     * @return Course
     */
    public function addCallc(\CAB\CallcBundle\Entity\Callc $callc)
    {
        $this->callc[] = $callc;

        return $this;
    }

    /**
     * Remove callc
     *
     * @param \CAB\CallcBundle\Entity\Callc $callc
     */
    public function removeCallc(\CAB\CallcBundle\Entity\Callc $callc)
    {
        $this->callc->removeElement($callc);
    }

    /**
     * Get callc
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCallc()
    {
        return $this->callc;
    }

    /**
     * Set commandSncf
     *
     * @param \CAB\CourseBundle\Entity\CommandSncf $commandSncf
     * @return Course
     */
    public function setCommandSncf(\CAB\CourseBundle\Entity\CommandSncf $commandSncf = null)
    {
        $this->commandSncf = $commandSncf;

        return $this;
    }

    /**
     * Get commandSncf
     *
     * @return \CAB\CourseBundle\Entity\CommandSncf
     */
    public function getCommandSncf()
    {
        return $this->commandSncf;
    }

    /**
     * Set timeBetweenArrivedAndStarted
     *
     * @param string $timeBetweenArrivedAndStarted
     * @return Course
     */
    public function setTimeBetweenArrivedAndStarted($timeBetweenArrivedAndStarted)
    {
        $this->timeBetweenArrivedAndStarted = $timeBetweenArrivedAndStarted;

        return $this;
    }

    /**
     * Get HourEstimatedArrival
     *
     * @return string
     */
    public function getDelayRace()
    {
        $courseHistories = $this->getCourseHistory();
        // get the arrival time of driver to customer
        $dateTimeArival = null;
        $dateTimeOn = null;
        $timeElapsed = 'N/A';
        /** @var CourseHistory $courseHistory */
        if (!$courseHistories){
            foreach ( $courseHistories as $courseHistory) {
                if ($courseHistory->getStatus() == self::STATUS_ON_RACE_DRIVER) {
                    $dateTimeOn = $courseHistory->getCreatedAt();
                    $departure_time = $courseHistory->getDepartureTime();
                }
                if ($courseHistory->getStatus() == self::STATUS_ARRIVED_DRIVER) {
                    $dateTimeArival = $courseHistory->getCreatedAt();
                }
            }
            if ($dateTimeArival && $departure_time) {
                $d_start = strtotime($departure_time->format('H:i'));
                $d_end =  strtotime($dateTimeArival->format('H:i'));
                $diff = gmdate('H:i',$d_end-$d_start);
                $timesplit=explode(':',$diff);
                $min=($timesplit[0]*60)+($timesplit[1]);
                if ($min > 0 && $min < 240)
                {
                    $timeElapsed = $min;
                }else{
                    $timeElapsed = '-';
                }
            }
        }

        return $timeElapsed;
    }

    /**
     * Get timeBetweenArrivedAndStarted
     *
     * @return string
     */
    public function getTimeBetweenArrivedAndStarted()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();
        // get the arrival time of driver to customer
        $dateTimeArival = null;
        $dateTimeOn = null;
        $timeElapsed = 'N/A';

        if ($this->getDelayRace() < 5){
            /** @var CourseHistory $courseHistory */
            foreach ( $courseHistories as $courseHistory) {
                if ($courseHistory->getStatus() == self::STATUS_ON_RACE_DRIVER) {
                    $dateTimeOn = $courseHistory->getCreatedAt();
                    $departure_time = $courseHistory->getDepartureTime();
                }
                if ($courseHistory->getStatus() == self::STATUS_ARRIVED_DRIVER) {
                    $dateTimeArival = $courseHistory->getCreatedAt();
                }
            }
            if ($dateTimeArival && $dateTimeOn) {
                $d_start = strtotime($departure_time->format('H:i'));
                $d_end =  strtotime($dateTimeOn->format('H:i'));
                $diff = gmdate('H:i',$d_end-$d_start);
                $timesplit=explode(':',$diff);
                $min=($timesplit[0]*60)+($timesplit[1]);

                $timeElapsed = $min - 15 ; // 15 minutes d'attente offert
            }
        }
        return $timeElapsed;
    }


    /**
     * Get TimeRaceCreated
     *
     * @return string
     */
    public function getTimeRaceCreated()
    {
        $courseHistories = $this->getCourseHistory();
        $interval=null;

        if (!$courseHistories){
            /** @var CourseHistory $courseHistory */
            foreach ( $courseHistories as $courseHistory) {
                if ($courseHistory->getStatus() == self::STATUS_CREATED) {
                    $dateTimeOn = $courseHistory->getCreatedAt();
                    $departure_date = $courseHistory->getDepartureDate()->format('Y-m-d');
                    $departure_time = $courseHistory->getDepartureTime()->format('H:i:s');
                    $dateTimeOn = $dateTimeOn->format('Y-m-d H:i:s');
                    $combinedDT = date('Y-m-d H:i:s', strtotime("$departure_date $departure_time"));
                    $dateDiff = intval((strtotime($combinedDT)-strtotime($dateTimeOn))/60);
                    $hours = intval($dateDiff/60);
                    $minutes = $dateDiff%60;
                    $interval = $hours*60 + $minutes;
                }
            }
        }
            return $interval;
    }


    /**
     * Get PriceHTattente
     *
     * @return string
     */
    public function getPriceHTattente()
    {
        $PriceHTattente = '';
        $value = $this->getSlowWalkPerHourTarif()/60;

        $attente = $this->getTimeBetweenArrivedAndStarted();

        if ($attente > 0){
            $PriceHTattente = $attente * $value;
        }
        return $PriceHTattente;
    }

    /**
     * Get timeBetweenArrivedAndStarted
     *
     * @return string
     */
    public function getPriceTTCattente()
    {
        $PriceTTCattente = $this->getPriceHTattente() ;

        if ($PriceTTCattente > 0){
            $PriceTTCattente = $PriceTTCattente * 1.1;
            $PriceTTCattente = round($PriceTTCattente,2);
        }
        return $PriceTTCattente;
    }

    /**
     * Get timeBetweenArrivedAndStarted
     *
     * @return string
     */
    public function getPriceTotalHTrace()
    {
        $PriceHTrace = $this->getPriceHT() + $this->getPriceHTattente();

        return $PriceHTrace;
    }

    /**
     * Get timeBetweenArrivedAndStarted
     *
     * @return string
     */
    public function getPriceTotalTTCrace()
    {
        $PriceTTCrace = $this->getPriceTTC() + $this->getPriceTTCattente();

        return $PriceTTCrace;
    }

    /**
     * Get HourEstimatedArrival
     *
     * @return string
     */
    public function getHourEstimatedArrival()
    {
        $timesplit=explode('.',$this->courseTime);
        $min=$timesplit[0];
        $minutes = '+'.$min.'minutes';
        $d_start = $this->departureTime->format('H:i');
        $HourEstimatedArrival=  date('H:i',strtotime($minutes,strtotime($d_start)));
        return $HourEstimatedArrival;
    }

    /**
     * Get HourEstimatedArrivalTLSE
     *
     * @return string
     */
    public function getHourEstimatedArrivalTLSE()
    {
        $timesplit=explode('.',$this->courseTime);
        $min=$timesplit[0];
        $min = $min*2;
        $minutes = '+'.$min.'minutes';
        $d_start = $this->departureTime->format('H:i');
        $HourEstimatedArrival=  date('H:i',strtotime($minutes,strtotime($d_start)));
        return $HourEstimatedArrival;
    }





    public function intersection($s1, $e1, $s2, $e2)
    {
        if ($e1 < $s2)
            return 0;
        if ($s1 > $e2)
            return 0;
        if ($s1 < $s2)
            $s1 = $s2;
        if ($e1 > $e2)
            $e1 = $e2;
        return $e1 - $s1;
    }

    /**
     * Get HourNightWorking
     *
     * @return string
     */
    public function getHourNightWorking()
    {
        $status = $this->courseStatus ;
        $HourNightWorhing =" ";

        if ($status==6){
            $night_start = strtotime("21:00");
            $night_end = strtotime("23:59");
            $start_race = strtotime($this->getDepartureTimeFormatted());
            $time_start = date('H',strtotime($this->getDepartureTimeFormatted()));
            $time = date('H',strtotime($this->getHourEstimatedArrivalTLSE()));
            $finish_race = strtotime($this->getHourEstimatedArrivalTLSE());
            if ($time >= "0" && $time < "6") {
                $finish_race = strtotime($this->getHourEstimatedArrivalTLSE()) + 3600*24 ;
                $night_end = strtotime("06:00") + 3600*24; // 07:00 of next day, add 3600*24 seconds
            }

            if ($time_start >= "0" && $time_start < "6") {
                $finish_race = strtotime($this->getHourEstimatedArrivalTLSE());
                $night_start = strtotime("00:00");
                $night_end = strtotime("06:00"); // 07:00 of next day, add 3600*24 seconds
            }
            $HourNightWorhing = $this->intersection($start_race, $finish_race, $night_start, $night_end)/3600;


        }elseif ($status==24){
            $night_start = strtotime("21:00");
            $night_end = strtotime("23:59");
            $start_race = strtotime($this->getDepartureTimeFormatted());
            $time = date('H',strtotime($this->getHourEstimatedArrivalTLSE()));
            $time_start = date('H',strtotime($this->getDepartureTimeFormatted()));
            $finish_race = strtotime($this->getHourEstimatedArrivalTLSE());
            if ($time >= "0" && $time < "6") {
                $finish_race = strtotime($this->getHourEstimatedArrivalTLSE()) + 3600 * 24;
                $night_end = strtotime("06:00") + 3600*24; // 07:00 of next day, add 3600*24 seconds
            }
            if ($time_start >= "0" && $time_start < "6") {
                $finish_race = strtotime($this->getHourEstimatedArrivalTLSE());
                $night_start = strtotime("00:00");
                $night_end = strtotime("06:00"); // 07:00 of next day, add 3600*24 seconds
            }
            $HourNightWorhing = $this->intersection($start_race, $finish_race, $night_start, $night_end)/3600;
            $HourNightWorhing = $HourNightWorhing/2;
        }

        $HourNightWorhing = round($HourNightWorhing, 2);

        return $HourNightWorhing;
    }

    /**
     * Get HourDayWorking
     *
     * @return string
     */
    public function getHourDayWorking()
    {

        $status = $this->courseStatus ;
        $HourDayWorhing =" ";

        if ($status==6){
            $day_start = strtotime("06:00");
            $day_end = strtotime("21:00");
            $start_race = strtotime($this->getDepartureTimeFormatted());
            $finish_race = strtotime($this->getHourEstimatedArrivalTLSE());

            $HourDayWorhing = $this->intersection($start_race, $finish_race, $day_start, $day_end)/3600;
        } elseif ($status==24){
            $day_start = strtotime("06:00");
            $day_end = strtotime("21:00");
            $start_race = strtotime($this->getDepartureTimeFormatted());
            $finish_race = strtotime($this->getHourEstimatedArrivalTLSE());

            $HourDayWorhing = $this->intersection($start_race, $finish_race, $day_start, $day_end)/3600;
            $HourDayWorhing = $HourDayWorhing/2;
        }

        $HourDayWorhing = round($HourDayWorhing,2);
        return $HourDayWorhing;
    }


    /**
     * Get isGroupageFormatted
     *
     * @return string
     */
    public function getIsGroupageFormatted ()
    {
        if($this->isGroupage == TRUE){
            $groupage = 'OUI';
        }else{
            $groupage = 'NON';
        }
        return $groupage ;
    }


    /**
     * Set zone
     *
     * @param \CAB\CourseBundle\Entity\Zone $zone
     * @return Course
     */
    public function setZone(\CAB\CourseBundle\Entity\Zone $zone = null)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return \CAB\CourseBundle\Entity\Zone
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set startedAddressCity
     *
     * @param string $startedAddressCity
     * @return Course
     */
    public function setStartedAddressCity($startedAddressCity)
    {
        $this->startedAddressCity = $startedAddressCity;

        return $this;
    }

    /**
     * Get startedAddressCity
     *
     * @return string
     */
    public function getStartedAddressCity()
    {
        return $this->startedAddressCity;
    }

    /**
     * Set arrivalAddressCity
     *
     * @param string $arrivalAddressCity
     * @return Course
     */
    public function setArrivalAddressCity($arrivalAddressCity)
    {
        $this->arrivalAddressCity = $arrivalAddressCity;

        return $this;
    }

    /**
     * Get arrivalAddressCity
     *
     * @return string
     */
    public function getArrivalAddressCity()
    {
        return $this->arrivalAddressCity;
    }


    /**
     * Get
     *
     * @return string
     */
    public function getHoursDriverGotoClientTime()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();
        // get the arrival time of driver to customer
        $dateTimeCreated = null;
        $dateTimeOn = null;


        /** @var CourseHistory $courseHistory */
        foreach ( $courseHistories as $courseHistory) {

            if ($courseHistory->getStatus() == self::STATUS_DRIVER_GO_TO_CLIENT) {
                $dateTimeOn = $courseHistory->getCreatedAt();
                $dateTimeOn = $dateTimeOn->format('H:i:s') ;

            }
        }

        return $dateTimeOn;
    }


    /**
     * Get
     *
     * @return string
     */
    public function getHoursArrivalDriverTime()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();
        // get the arrival time of driver to customer
        $dateTimeCreated = null;
        $dateTimeOn = null;


        /** @var CourseHistory $courseHistory */
        foreach ( $courseHistories as $courseHistory) {

            if ($courseHistory->getStatus() == self::STATUS_ARRIVED_DRIVER) {
                $dateTimeOn = $courseHistory->getCreatedAt();
                $dateTimeOn = $dateTimeOn->format('H:i:s') ;

            }
        }

        return $dateTimeOn;
    }




    /**
     * Get
     *
     * @return string
     */
    public function getHoursArrivalDriverEcart()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();

        // get the arrival time of driver to customer

        $dateTimeCreated = null;
        $dateTimeArival = null;
        $dateTimeOn = null;


        $datedeparturerace = $this->getDepartureTime();

            /** @var CourseHistory $courseHistory */
            foreach ( $courseHistories as $courseHistory) {

                if ($courseHistory->getStatus() == self::STATUS_ARRIVED_DRIVER) {
                    $dateTimeOn = $courseHistory->getCreatedAt();
                }
            }

        if (isset($dateTimeOn)){
            if ($dateTimeOn > $datedeparturerace ){
            }else{
                $d_start = strtotime($datedeparturerace->format('H:i'));
                $d_end =  strtotime($dateTimeOn->format('H:i'));
                $diff = gmdate('H:i',$d_end-$d_start);
                $timesplit=explode(':',$diff);
                $min=($timesplit[0]*60)+($timesplit[1]);

                if ($min > 0 && $min < 240)
                {
                    $interval = $min;
                    if ($min > 0 && $min < 7){
                        return $interval.' min';
                    }
                    return '<b style="color: red">'.$interval.' min</b>';
                }else{
                    $d_start = strtotime($dateTimeOn->format('H:i'));
                    $d_end =  strtotime($datedeparturerace->format('H:i'));
                    $diff = gmdate('H:i',$d_end-$d_start);
                    $timesplit=explode(':',$diff);
                    $min=($timesplit[0]*60)+($timesplit[1]);
                    $interval = $min;

                    if ($min > 0 && $min < 7) {

                        return $min.' min' ;
                    }else{
                        return '<b style="color: #589613">'.$interval.' min</b>';
                    }
                }
            }
        }
    }




    /**
     * Get
     *
     * @return string
     */
    public function getHoursRaceOnRaceDriverTime()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();
        // get the arrival time of driver to customer
        $dateTimeCreated = null;
        $dateTimeOn = null;

        /** @var CourseHistory $courseHistory */
        foreach ( $courseHistories as $courseHistory) {

            if ($courseHistory->getStatus() == self::STATUS_ON_RACE_DRIVER) {
                $dateTimeOn = $courseHistory->getCreatedAt();
                $dateTimeOn = $dateTimeOn->format('H:i:s') ;

            }
        }

        return $dateTimeOn;
    }






    /**
     * Get
     *
     * @return string
     */
    public function getHoursRaceOnRaceDriverEcart()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();

        // get the arrival time of driver to customer

        $dateTimeCreated = null;
        $dateTimeArival = null;
        $dateTimeOn = null;


        $datedeparturerace = $this->getDepartureTime();

        /** @var CourseHistory $courseHistory */
        foreach ( $courseHistories as $courseHistory) {

            if ($courseHistory->getStatus() == self::STATUS_ON_RACE_DRIVER) {
                $dateTimeOn = $courseHistory->getCreatedAt();
            }
        }

        if (isset($dateTimeOn)){
            if ($dateTimeOn > $datedeparturerace ){
            }else{
                $d_start = strtotime($datedeparturerace->format('H:i'));
                $d_end =  strtotime($dateTimeOn->format('H:i'));
                $diff = gmdate('H:i',$d_end-$d_start);
                $timesplit=explode(':',$diff);
                $min=($timesplit[0]*60)+($timesplit[1]);

                if ($min > 0 && $min < 240)
                {
                    $interval = $min;
                    if ($min > 0 && $min < 15){
                        return $interval.' min';
                    }
                    return '<b style="color: red">'.$interval.' min </b>';
                }else{
                    $d_start = strtotime($dateTimeOn->format('H:i'));
                    $d_end =  strtotime($datedeparturerace->format('H:i'));
                    $diff = gmdate('H:i',$d_end-$d_start);
                    $timesplit=explode(':',$diff);
                    $min=($timesplit[0]*60)+($timesplit[1]);
                    $interval = $min;

                    if ($min > 0 && $min < 7) {

                        return $min.' min' ;
                    }else{
                        return '<b style="color: #589613">'.$interval.' min</b>';
                    }
                }
            }
        }
    }





    /**
     * Get
     *
     * @return string
     */
    public function getHoursFinishDriverTime()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();
        // get the arrival time of driver to customer
        $dateTimeCreated = null;
        $dateTimeOn = null;


        /** @var CourseHistory $courseHistory */
        foreach ( $courseHistories as $courseHistory) {

            if ($courseHistory->getStatus() == self::STATUS_DONE) {
                $dateTimeOn = $courseHistory->getCreatedAt();
                $dateTimeOn = $dateTimeOn->format('H:i:s') ;

            }
        }

        return $dateTimeOn;
    }



    /**
     * Get
     *
     * @return string
     */
    public function getHoursFinishDriverEcart()
    {
        //calcul attente course
        $courseHistories = $this->getCourseHistory();

        // get the arrival time of driver to customer

        $dateTimeCreated = null;
        $dateTimeArival = null;
        $dateTimeOn = null;


        $date = $this->getDepartureDate()->format('d-m-Y');
        $time = $this->getDepartureTime()->format('H:i:s');
        $dt = $date . " " . $time;
        $merge = date_create_from_format( "d-m-Y H:i:s", $dt ); // convert from string to PHP DateTime

        if(isset($this->courseTime)){
            $time_race= $this->courseTime ;

            $datefinshrace_estimate = $merge->modify("+{$time_race} minutes");
        }else{
            $datefinshrace_estimate = $merge;
        }



        /** @var CourseHistory $courseHistory */
        foreach ( $courseHistories as $courseHistory) {

            if ($courseHistory->getStatus() == self::STATUS_DONE) {
                $dateTimeOn = $courseHistory->getCreatedAt();
            }
        }

        if (isset($dateTimeOn)){
            if ($datefinshrace_estimate > $dateTimeOn ){
                $d_start = strtotime($dateTimeOn->format('H:i'));
                $d_end =  strtotime($datefinshrace_estimate->format('H:i'));
                $diff = gmdate('H:i',$d_end-$d_start);
                $timesplit=explode(':',$diff);
                $min=($timesplit[0]*60)+($timesplit[1]);
                $interval = $min;

                if ($min > 0 && $min < 7) {

                    return $min.' min' ;
                }else{
                    return '<b style="color: #589613">'.$interval.' min</b>';
                }

            }else{
                $d_start = strtotime($datefinshrace_estimate->format('H:i'));
                $d_end =  strtotime($dateTimeOn->format('H:i'));
                $diff = gmdate('H:i',$d_end-$d_start);
                $timesplit=explode(':',$diff);
                $min=($timesplit[0]*60)+($timesplit[1]);

                if ($min > 0 && $min < 240)
                {
                    $interval = $min;
                    if ($min > 0 && $min < 7){
                        return $interval.' min';
                    }
                    return '<b style="color: red">'.$interval.' min</b>';
                }
            }
        }
    }

}
