<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * VehiculeMarkRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VehiculeMarkRepository extends EntityRepository
{

    public function getAllMark(){
        $query = $this->createQueryBuilder('v')
                     ->distinct();

        $query->setMaxResults(100)
	    ->setCacheable(true)->setCacheRegion('cache_long_time');
        //$result = $query->getResult();

        return $query;
    }
}
