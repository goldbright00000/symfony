<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CAB\UserBundle\Entity\User;
use CAB\CourseBundle\Entity\Course;
use JMS\Serializer\Annotation;

/**
 * Rating
 *
 * @ORM\Table(name="cab_rating")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\RatingRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Annotation\ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Rating
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     * @Annotation\Groups({"Public"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="voter", type="smallint")
     */
    private $voter;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate_ponctualite", type="smallint", nullable=true)
     */
    private $ratePonctualite = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate_confort", type="smallint", nullable=true)
     */
    private $rateConfort = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate_conduite", type="smallint", nullable=true)
     */
    private $rateConduite = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate_courtoisie", type="smallint", nullable=true)
     */
    private $rateCourtoisie = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate_general", type="smallint", nullable=true)
     */
    private $rateGeneral = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     * @Annotation\Expose
     * @Annotation\Groups({"Public"})
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="dvratings")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="clratings")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Course", inversedBy="ratings")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", onDelete="CASCADE")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $course;

    /**
     * @var datetime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Annotation\Expose
     * @Annotation\Groups({"Public"})
     */
    private $createdAt;

    /**
     * @var datetime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set voter
     *
     * @param integer $voter
     * @return Rating
     */
    public function setVoter($voter)
    {
        $this->voter = $voter;

        return $this;
    }

    /**
     * Get voter
     *
     * @return integer
     */
    public function getVoter()
    {
        return $this->voter;
    }

    /**
     * Set ratePonctualite
     *
     * @param integer $ratePonctualite
     * @return Rating
     */
    public function setRatePonctualite($ratePonctualite)
    {
        $this->ratePonctualite = $ratePonctualite;

        return $this;
    }

    /**
     * Get ratePonctualite
     *
     * @return integer
     */
    public function getRatePonctualite()
    {
        return $this->ratePonctualite;
    }

    /**
     * Set rateConfort
     *
     * @param integer $rateConfort
     * @return Rating
     */
    public function setRateConfort($rateConfort)
    {
        $this->rateConfort = $rateConfort;

        return $this;
    }

    /**
     * Get rateConfort
     *
     * @return integer
     */
    public function getRateConfort()
    {
        return $this->rateConfort;
    }

    /**
     * Set rateConduite
     *
     * @param integer $rateConduite
     * @return Rating
     */
    public function setRateConduite($rateConduite)
    {
        $this->rateConduite = $rateConduite;

        return $this;
    }

    /**
     * Get rateConduite
     *
     * @return integer
     */
    public function getRateConduite()
    {
        return $this->rateConduite;
    }

    /**
     * Set rateCourtoisie
     *
     * @param integer $rateCourtoisie
     * @return Rating
     */
    public function setRateCourtoisie($rateCourtoisie)
    {
        $this->rateCourtoisie = $rateCourtoisie;

        return $this;
    }

    /**
     * Get rateCourtoisie
     *
     * @return integer
     */
    public function getRateCourtoisie()
    {
        return $this->rateCourtoisie;
    }

    /**
     * Set rateGeneral
     *
     * @param integer $rateGeneral
     * @return Rating
     */
    public function setRateGeneral($rateGeneral)
    {
        $this->rateGeneral = $rateGeneral;

        return $this;
    }

    /**
     * Get rateGeneral
     *
     * @return integer
     */
    public function getRateGeneral()
    {
        return $this->rateGeneral;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return Rating
     */
    public function setDriver(User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set client
     *
     * @param \CAB\UserBundle\Entity\User $client
     * @return Rating
     */
    public function setClient(User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return Rating
     */
    public function setCourse(Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Rating
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return void
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

        /**
     * Set updatedAt
     *
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return void
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get voter name: 1 => driver, 2 => customer
     *
     * @Annotation\VirtualProperty
     * @Annotation\Groups({"Public"})
     */
    public function getVoterName()
    {
        return $this->getVoter() === 1 ? 'driver' : 'customer';
    }

    /**
     * Get average of given rates
     *
     * @Annotation\VirtualProperty
     * @Annotation\Groups({"Public"})
     */
    public function getAverageRate()
    {
        return round(($this->getRateGeneral()+$this->getRateCourtoisie()+$this->getRateConduite()+$this->getRateConfort()+$this->getRatePonctualite())/5, 2);
    }
}
