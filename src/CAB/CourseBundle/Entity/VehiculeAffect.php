<?php
/**
 * User: Yassine Belkaid <yassine.belkaid87@gmail.com>
 * Date: 12/02/2016
 * Time: 22:31
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CAB\UserBundle\Entity\User;
use CAB\CourseBundle\Entity\Vehicule;
use JMS\Serializer\Annotation;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * CAB\CourseBundle\Entity\VehiculeAffect
 *
 * @ORM\Table(name="cab_vehicule_affect")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\VehiculeAffectRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Annotation\ExclusionPolicy("all")
 * @Vich\Uploadable
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class VehiculeAffect
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="useraffects")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="vehicleFuel")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Vehicule", inversedBy="vehiculeaffects")
     * @ORM\JoinColumn(name="vehicule_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicule;

    /**
     * @ORM\Column(name="km", type="integer", nullable=true)
     * @Annotation\Expose
     */
    protected $km;

    /**
     * @ORM\Column(name="ecart", type="integer", nullable=true)
     * @Annotation\Expose
     */
    protected $ecart;


    /**
     * @var boolean $debutservice
     *
     * @ORM\Column(name="debut_service", type="boolean")
     */
    private $debutservice = false;

    /**
     * @var boolean $finservice
     *
     * @ORM\Column(name="fin_service", type="boolean")
     */
    private $finservice = false;

    /**
     * @var string $latitude
     * @ORM\Column(name="latitude", type="decimal", precision=14, scale=8, nullable=true)
     */
    private $latitude;

    /**
     * @var string $longitude
     * @ORM\Column(name="longitude", type="decimal", precision=14, scale=8, nullable=true)
     */
    private $longitude;

    /**
     * @var string $numberlic
     *
     * @ORM\Column(name="number_lic", type="text", nullable=true)
     */
    private $numberlic;

    /**
     * @var string $numberlicpager
     *
     * @ORM\Column(name="number_lic_pager", type="text", nullable=true)
     */
    private $numberlicpager;

    /**
     * @var string $numberlicpagerfile
     *
     * @ORM\Column(name="number_lic_pager_file", type="text", nullable=true)
     */
    protected $numberlicpagerfile;

    /**
     * @var string $imageName
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $fileName = null;






    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->km = 0;
    }

    /**
     * Set km
     *
     * @param integer $km
     * @return Vehicule
     */
    public function setKm($km)
    {
        $this->km = $km;

        return $this;
    }

    /**
     * Get km
     *
     * @return integer
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set driver
     *
     * @param \CAB\CourseBundle\Entity\User $driver
     * @return Vehicule
     */
    public function setDriver(User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\CourseBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set vehicule
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicule
     * @return Vehicule
     */
    public function setVehicule(Vehicule $vehicule = null)
    {
        $this->vehicule = $vehicule;

        return $this;
    }

    /**
     * Get vehicule
     *
     * @return \CAB\CourseBundle\Entity\Vehicule
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        if ($this->getVehicule() !== null) {
            $this->setCompany($this->getVehicule()->getCompany());
//            $this->setDriver($this->getVehicule()->getDriver());
        }
    }


    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return VehiculeAffect
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @ORM\PreUpdate
     * @param \DateTime $updatedAt
     * @return Vehicule
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ecart
     *
     * @param integer $ecart
     * @return VehiculeAffect
     */
    public function setEcart($ecart)
    {
        $this->ecart = $ecart;

        return $this;
    }

    /**
     * Get ecart
     *
     * @return integer
     */
    public function getEcart()
    {
        return $this->ecart;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return VehiculeAffect
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set debutservice
     *
     * @param boolean $debutservice
     * @return VehiculeAffect
     */
    public function setDebutservice($debutservice)
    {
        $this->debutservice = $debutservice;

        return $this;
    }

    /**
     * Get debutservice
     *
     * @return boolean
     */
    public function getDebutservice()
    {
        return $this->debutservice;
    }

    /**
     * Set finservice
     *
     * @param boolean $finservice
     * @return VehiculeAffect
     */
    public function setFinservice($finservice)
    {
        $this->finservice = $finservice;

        return $this;
    }

    /**
     * Get finservice
     *
     * @return boolean
     */
    public function getFinservice()
    {
        return $this->finservice;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return VehiculeAffect
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return VehiculeAffect
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set numberlic
     *
     * @param string $numberlic
     * @return VehiculeAffect
     */
    public function setNumberlic($numberlic)
    {
        $this->numberlic = $numberlic;

        return $this;
    }

    /**
     * Get numberlic
     *
     * @return string
     */
    public function getNumberlic()
    {
        return $this->numberlic;
    }

    /**
     * Set numberlicpager
     *
     * @param string $numberlicpager
     * @return VehiculeAffect
     */
    public function setNumberlicpager($numberlicpager)
    {
        $this->numberlicpager = $numberlicpager;

        return $this;
    }

    /**
     * Get numberlicpager
     *
     * @return string
     */
    public function getNumberlicpager()
    {
        return $this->numberlicpager;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return VehiculeAffect
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set numberlicpagerfile
     *
     * @param string $numberlicpagerfile
     * @return VehiculeAffect
     */
    public function setNumberlicpagerfile($numberlicpagerfile)
    {
        $this->numberlicpagerfile = $numberlicpagerfile;

        return $this;
    }

    /**
     * Get numberlicpagerfile
     *
     * @return string
     */
    public function getNumberlicpagerfile()
    {
        return $this->numberlicpagerfile;
    }

    /**
     * @param $vehiculeAffect VehiculeAffect
     */
    private function uploadDocs($vehiculeAffect)
    {
        $numberLicPagerFile = '';
        if (isset($_REQUEST[$_REQUEST['uniqid']]['numberlicpagerfile']['base64']) && !empty($_REQUEST[$_REQUEST['uniqid']]['numberlicpagerfile']['base64'])):
            $vehicule_img = rand().'.jpeg';
            $path = __DIR__.'/../../../../web/uploads/vehicule/affect/'.$vehicule_img;
            $explode_string = explode(',', $_REQUEST[$_REQUEST['uniqid']]['numberlicpagerfile']['base64']);
            $img = str_replace(' ', '+', $explode_string[1]);
            $image_base64 = base64_decode($img);
            file_put_contents($path, $image_base64);
            $numberLicPagerFile = $vehicule_img;
        endif;
        if ($this->getForm()->get('fileType')->getData() == 1 && $this->getForm()->get('nameFilePDF')->getData()) {
            $vehiculeAffect->setnumberlicpagerfile($numberLicPagerFile);
//            $vehiculeAffect->setnumberlicpagerfile($this->getForm()->get('numberlicpagerfile')->getData());
        } elseif ($this->getForm()->get('fileType')->getData() == 2 && $this->getForm()->get(
                'numberlicpagerfile'
            )->getData()
        ) {
            $vehiculeAffect->setnumberlicpagerfile($numberLicPagerFile);
//            $vehiculeAffect->setnumberlicpagerfile($this->getForm()->get('numberlicpagerfile')->getData());
        }
    }


}
