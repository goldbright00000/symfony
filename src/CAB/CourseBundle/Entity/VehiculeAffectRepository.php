<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * VehiculeAffectRepository
 *
 */
class VehiculeAffectRepository extends EntityRepository
{
    /**
     * @param integer $driver_id
     * @param string  $order
     *
     * @return array
     */
    public function getVehicleAffectByDriverId($driver_id, $order = "DESC", $checkOtherDate = '')
    {
        if (empty($checkOtherDate)) {
            $timeZone       = new \DateTimeZone('GMT');
            $requestedDate  = new \DateTime('now');
            $requestedDate->setTimezone($timeZone);
            $today  = $requestedDate->format('Y-m-d');
        } else {
            list($day, $mon, $year)  = explode("/", $checkOtherDate);
            $today  = $year.'-'.$mon.'-'.$day;
        }

        $dql = "SELECT v FROM CABCourseBundle:VehiculeAffect v WHERE v.driver = :driver_id AND DATE_FORMAT(v.createdAt, '%Y-%m-%d') = :today ORDER BY v.createdAt ". $order;

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameters(array(
                'driver_id' => $driver_id,
                'today'     => $today
            ))->setMaxResults(1);
	    $query->setCacheable(true)->setCacheRegion('cache_long_time');
        try {
            return $query->getOneOrNullResult();
        } catch(\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * @param integer $driver_id
     * @param string  $order
     *
     * @return array
     */
    public function getDriversByVehiculeId($vehicule_id, $order = "DESC", $checkOtherDate = '')
    {
        if (empty($checkOtherDate)) {
            $timeZone       = new \DateTimeZone('GMT');
            $requestedDate  = new \DateTime('now');
            $requestedDate->setTimezone($timeZone);
            $today  = $requestedDate->format('Y-m-d');
        } else {
            list($day, $mon, $year)  = explode("/", $checkOtherDate);
            $today  = $year.'-'.$mon.'-'.$day;
        }

        $dql = "SELECT v FROM CABCourseBundle:VehiculeAffect v WHERE v.vehicule = :vehicule_id AND DATE_FORMAT(v.createdAt, '%Y-%m-%d') = :today ORDER BY v.createdAt ". $order;

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameters(array(
                'vehicule_id' => $vehicule_id,
                'today'       => $today
            ));

        $query->setCacheable(true)->setCacheRegion('cache_long_time');
        try {
            return $query->getResult();
        } catch(\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    public function getCountDriversByVehiculeId($vehicule_id)
    {

        $dql = "SELECT COUNT(DISTINCT v.driver) FROM CABCourseBundle:VehiculeAffect v  WHERE v.vehicule = :vehicule_id  ";
        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameters(array(
                'vehicule_id' => $vehicule_id,
            ));


        try {
            return $query->getSingleScalarResult();
        } catch(\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }


    public function getListDriversByVehiculeId($vehicule_id)
    {
        $dql = "
          SELECT DATE_FORMAT(v.createdAt, '%Y-%m-%d') AS DATE, t.firstName 
          FROM CABCourseBundle:VehiculeAffect v 
          INNER JOIN CABUserBundle:User t 
          WHERE v.vehicule = :vehicule_id  
              AND v.driver = t.id 
          ORDER BY v.createdAt DESC ";
        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameters(array(
                'vehicule_id' => $vehicule_id,
            ))
          ->setMaxResults(10);
	   // $query->setCacheable(true)->setCacheRegion('cache_long_time');
        try {
            return $query->getResult();
        } catch(\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }


  /**
   * @param $vehicule_id
   * @param $date
   *
   * @return array|int
   */
  public function getListDriversByVehiculeIdAndDate($vehicule_id, $date)
  {
    $dql = "
          SELECT DATE_FORMAT(v.createdAt, '%Y-%m-%d') AS DATE, t.firstName 
          FROM CABCourseBundle:VehiculeAffect v 
          INNER JOIN CABUserBundle:User t 
          WHERE v.vehicule = :vehicule_id  
              AND DATE_FORMAT(v.createdAt, '%Y-%m-%d') = :date
              AND v.driver = t.id 
          ORDER BY v.createdAt DESC 
          ";
    $query = $this->getEntityManager()->createQuery($dql)
      ->setParameters(array(
        'vehicule_id' => $vehicule_id,
        'date' => $date,
      ));
	  $query->setCacheable(true)->setCacheRegion('cache_long_time');
    try {
      return $query->getResult();
    } catch(\Doctrine\ORM\NoResultException $e) {
      return 0;
    }
  }

     /**
     * @param object $vehicule_id
     *
     * @return array
     */
    public function getEcartByVehicule($vehicule_id) {
        $dql = "SELECT va.km as km FROM CABCourseBundle:VehiculeAffect as va INNER JOIN va.vehicule as v WHERE v.id = :vehicule_id ORDER BY va.createdAt DESC";
        $query = $this->getEntityManager()->createQuery($dql)
                        ->setParameters(array(
                            'vehicule_id' => $vehicule_id
                        ))->setMaxResults(1);
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    /**
     * @param object $vehicule_id
     *
     * @return array
     */
    public function getSumEcartByVehicule($vehicule_id) {


        $dql = "SELECT SUM(va.ecart) FROM CABCourseBundle:VehiculeAffect as va INNER JOIN va.vehicule as v WHERE v.id = :vehicule_id ORDER BY va.createdAt DESC";
        $query = $this->getEntityManager()->createQuery($dql)
                        ->setParameters(array(
                            'vehicule_id' => $vehicule_id
                        ))->setMaxResults(1);
        try {
            return $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    public function getLastAffectVehiculeByDriverId($driver_id)
    {
        $dql = "SELECT IDENTITY(v.vehicule) FROM CABCourseBundle:VehiculeAffect v WHERE v.driver = :driver ORDER BY v.id DESC";

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameters(array(
                'driver' => $driver_id,
            ))->setMaxResults(1);

        try {
            $result = $query->getSingleScalarResult();

            return (int)$result;
        } catch(\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }



}
