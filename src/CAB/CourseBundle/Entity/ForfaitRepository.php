<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ForfaitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ForfaitRepository extends EntityRepository
{
    public function getForfaitByLongAndLat($long, $lat, $longArr, $latArr, $company = null, $vehicleType = null)
    {
        $query = "SELECT f.id, f.radius, f.tarifTTC, SQRT(
                POW(69.1 * (f.latDep - $lat), 2) +
                POW(69.1 * ($long - f.longDep) * COS(f.latDep / 57.3), 2)) AS distance, 
                SQRT(
                POW(69.1 * (f.latArr - $latArr), 2) +
                POW(69.1 * ($longArr - f.longArr) * COS(f.latArr / 57.3), 2)) AS distance2
                FROM CABCourseBundle:Forfait f
                where f.tarifHT IS NOT NULL 
                ";
        if ($company) {
            $query .= " AND f.company = $company";
        }
        if ($vehicleType) {
            $query .= " AND f.vehicleType = $vehicleType";
        }
        $query .= " HAVING (distance <= f.radius/100 and distance2 <= f.radius / 100) ORDER BY distance";

        return $this->getEntityManager()->createQuery($query )->getResult();

    }
}
