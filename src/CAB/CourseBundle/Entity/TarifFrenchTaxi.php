<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 30/11/2015
 * Time: 16:35
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\TarifFrenchTaxi
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_tarif_french_taxi")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\TarifFrenchTaxiRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class TarifFrenchTaxi {

    /**
     * @var string $departement
     * @ORM\Id
     * @ORM\Column(name="departement", type="string", length=4)
     */
    protected $departement;

    /**
     * @var string $name
     * @ORM\Column(name="name", type="string", length=128)
     */
    protected $name;

    /**
     * @var string $tarifPC
     * @ORM\Column(name="tarif_pc", type="string", length=5)
     */
    protected $tarifPC;

    /**
     * @var string $tarifA
     * @ORM\Column(name="tarif_a", type="string", length=5)
     */
    protected $tarifA;

    /**
     * @var string $tarifB
     * @ORM\Column(name="tarif_b", type="string", length=5)
     */
    protected $tarifB;

    /**
     * @var string $tarifC
     * @ORM\Column(name="tarif_c", type="string", length=5)
     */
    protected $tarifC;

    /**
     * @var string $tarifD
     * @ORM\Column(name="tarif_d", type="string", length=5)
     */
    protected $tarifD;

    /**
     * @var decimal $slowWalkPerHour
     *
     * @ORM\Column(name="slow_walk_per_hour", type="decimal", precision=4, scale=2, nullable=true)
     */
    protected $slowWalkPerHour;

    /**
     * @var string $luggage
     *
     * @ORM\Column(name="luggage", type="string", length=100, nullable=true)
     */
    protected $luggage;

    /**
     * tarif fot group (8 persons for example)
     * @var string $person
     *
     * @ORM\Column(name="person", type="string", length=100, nullable=true)
     */
    protected $person;

    /**
     * @var decimal $minPrice
     *
     * @ORM\Column(name="min_price", type="decimal", precision=4, scale=2, nullable=true)
     */
    protected $minPrice;

    /**
     * @var string $packageApproach
     *
     * @ORM\Column(name="package_approach", type="string", length=20, nullable=true)
     */
    protected $packageApproach;


    /**
     * Set departement
     *
     * @param string $departement
     * @return TarifFrenchTaxi
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return string
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return TarifFrenchTaxi
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tarifPC
     *
     * @param string $tarifPC
     * @return TarifFrenchTaxi
     */
    public function setTarifPC($tarifPC)
    {
        $this->tarifPC = $tarifPC;

        return $this;
    }

    /**
     * Get tarifPC
     *
     * @return string
     */
    public function getTarifPC()
    {
        return $this->tarifPC;
    }

    /**
     * Set tarifA
     *
     * @param string $tarifA
     * @return TarifFrenchTaxi
     */
    public function setTarifA($tarifA)
    {
        $this->tarifA = $tarifA;

        return $this;
    }

    /**
     * Get tarifA
     *
     * @return string
     */
    public function getTarifA()
    {
        return $this->tarifA;
    }

    /**
     * Set tarifB
     *
     * @param string $tarifB
     * @return TarifFrenchTaxi
     */
    public function setTarifB($tarifB)
    {
        $this->tarifB = $tarifB;

        return $this;
    }

    /**
     * Get tarifB
     *
     * @return string
     */
    public function getTarifB()
    {
        return $this->tarifB;
    }

    /**
     * Set tarifC
     *
     * @param string $tarifC
     * @return TarifFrenchTaxi
     */
    public function setTarifC($tarifC)
    {
        $this->tarifC = $tarifC;

        return $this;
    }

    /**
     * Get tarifC
     *
     * @return string
     */
    public function getTarifC()
    {
        return $this->tarifC;
    }

    /**
     * Set tarifD
     *
     * @param string $tarifD
     * @return TarifFrenchTaxi
     */
    public function setTarifD($tarifD)
    {
        $this->tarifD = $tarifD;

        return $this;
    }

    /**
     * Get tarifD
     *
     * @return string
     */
    public function getTarifD()
    {
        return $this->tarifD;
    }

    /**
     * Set slowWalkPerHour
     *
     * @param string $slowWalkPerHour
     * @return TarifFrenchTaxi
     */
    public function setSlowWalkPerHour($slowWalkPerHour)
    {
        $this->slowWalkPerHour = $slowWalkPerHour;

        return $this;
    }

    /**
     * Get slowWalkPerHour
     *
     * @return string
     */
    public function getSlowWalkPerHour()
    {
        return $this->slowWalkPerHour;
    }

    /**
     * Set luggage
     *
     * @param string $luggage
     * @return TarifFrenchTaxi
     */
    public function setLuggage($luggage)
    {
        $this->luggage = $luggage;

        return $this;
    }

    /**
     * Get luggage
     *
     * @return string
     */
    public function getLuggage()
    {
        return $this->luggage;
    }

    /**
     * Set person
     *
     * @param string $person
     * @return TarifFrenchTaxi
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return string
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set minPrice
     *
     * @param string $minPrice
     * @return TarifFrenchTaxi
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /**
     * Get minPrice
     *
     * @return string
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * Set packageApproach
     *
     * @param string $packageApproach
     * @return TarifFrenchTaxi
     */
    public function setPackageApproach($packageApproach)
    {
        $this->packageApproach = $packageApproach;

        return $this;
    }

    /**
     * Get packageApproach
     *
     * @return string
     */
    public function getPackageApproach()
    {
        return $this->packageApproach;
    }
}
