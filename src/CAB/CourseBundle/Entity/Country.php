<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * CAB\CourseBundle\Entity\Country
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_country")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\CountryRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Country
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Public"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(name="iso31661alpha2", type="string", length=255, nullable=true)
     */
    protected $iso31661Alpha2;

    /**
     * @ORM\Column(name="iso31661alpha3", type="string", length=255, nullable=true)
     */
    protected $iso31661Alpha3;

    /**
     * @ORM\Column(name="iso31661numeric", type="string", length=255, nullable=true)
     */
    protected $iso31661Numeric;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $currencyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $currencyCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $currencyCodeNumeric;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $currencyMajor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $currencyMinor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $currencyDecimals;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"Public", "Course"})
     */
    protected $currencyFormat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cultureName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cultureLanguageName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $cultureCode;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Company", mappedBy="format")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set iso31661Alpha2
     *
     * @param string $iso31661Alpha2
     * @return Country
     */
    public function setIso31661Alpha2($iso31661Alpha2)
    {
        $this->iso31661Alpha2 = $iso31661Alpha2;

        return $this;
    }

    /**
     * Get iso31661Alpha2
     *
     * @return string
     */
    public function getIso31661Alpha2()
    {
        return $this->iso31661Alpha2;
    }

    /**
     * Set iso31661Alpha3
     *
     * @param string $iso31661Alpha3
     * @return Country
     */
    public function setIso31661Alpha3($iso31661Alpha3)
    {
        $this->iso31661Alpha3 = $iso31661Alpha3;

        return $this;
    }

    /**
     * Get iso31661Alpha3
     *
     * @return string
     */
    public function getIso31661Alpha3()
    {
        return $this->iso31661Alpha3;
    }

    /**
     * Set iso31661Numeric
     *
     * @param string $iso31661Numeric
     * @return Country
     */
    public function setIso31661Numeric($iso31661Numeric)
    {
        $this->iso31661Numeric = $iso31661Numeric;

        return $this;
    }

    /**
     * Get iso31661Numeric
     *
     * @return string
     */
    public function getIso31661Numeric()
    {
        return $this->iso31661Numeric;
    }

    /**
     * Set currencyName
     *
     * @param string $currencyName
     * @return Country
     */
    public function setCurrencyName($currencyName)
    {
        $this->currencyName = $currencyName;

        return $this;
    }

    /**
     * Get currencyName
     *
     * @return string
     */
    public function getCurrencyName()
    {
        return $this->currencyName;
    }

    /**
     * Set currencyCode
     *
     * @param string $currencyCode
     * @return Country
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * Get currencyCode
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * Set currencyCodeNumeric
     *
     * @param string $currencyCodeNumeric
     * @return Country
     */
    public function setCurrencyCodeNumeric($currencyCodeNumeric)
    {
        $this->currencyCodeNumeric = $currencyCodeNumeric;

        return $this;
    }

    /**
     * Get currencyCodeNumeric
     *
     * @return string
     */
    public function getCurrencyCodeNumeric()
    {
        return $this->currencyCodeNumeric;
    }

    /**
     * Set currencyMajor
     *
     * @param string $currencyMajor
     * @return Country
     */
    public function setCurrencyMajor($currencyMajor)
    {
        $this->currencyMajor = $currencyMajor;

        return $this;
    }

    /**
     * Get currencyMajor
     *
     * @return string
     */
    public function getCurrencyMajor()
    {
        return $this->currencyMajor;
    }

    /**
     * Set currencyMinor
     *
     * @param string $currencyMinor
     * @return Country
     */
    public function setCurrencyMinor($currencyMinor)
    {
        $this->currencyMinor = $currencyMinor;

        return $this;
    }

    /**
     * Get currencyMinor
     *
     * @return string
     */
    public function getCurrencyMinor()
    {
        return $this->currencyMinor;
    }

    /**
     * Set currencyDecimals
     *
     * @param string $currencyDecimals
     * @return Country
     */
    public function setCurrencyDecimals($currencyDecimals)
    {
        $this->currencyDecimals = $currencyDecimals;

        return $this;
    }

    /**
     * Get currencyDecimals
     *
     * @return string
     */
    public function getCurrencyDecimals()
    {
        return $this->currencyDecimals;
    }

    /**
     * Set currencyFormat
     *
     * @param string $currencyFormat
     * @return Country
     */
    public function setCurrencyFormat($currencyFormat)
    {
        $this->currencyFormat = $currencyFormat;

        return $this;
    }

    /**
     * Get currencyFormat
     *
     * @return string
     */
    public function getCurrencyFormat()
    {
        return $this->currencyFormat;
    }

    /**
     * Set cultureName
     *
     * @param string $cultureName
     * @return Country
     */
    public function setCultureName($cultureName)
    {
        $this->cultureName = $cultureName;

        return $this;
    }

    /**
     * Get cultureName
     *
     * @return string
     */
    public function getCultureName()
    {
        return $this->cultureName;
    }

    /**
     * Set cultureLanguageName
     *
     * @param string $cultureLanguageName
     * @return Country
     */
    public function setCultureLanguageName($cultureLanguageName)
    {
        $this->cultureLanguageName = $cultureLanguageName;

        return $this;
    }

    /**
     * Get cultureLanguageName
     *
     * @return string
     */
    public function getCultureLanguageName()
    {
        return $this->cultureLanguageName;
    }

    /**
     * Set cultureCode
     *
     * @param string $cultureCode
     * @return Country
     */
    public function setCultureCode($cultureCode)
    {
        $this->cultureCode = $cultureCode;

        return $this;
    }

    /**
     * Get cultureCode
     *
     * @return string
     */
    public function getCultureCode()
    {
        return $this->cultureCode;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->company = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return Country
     */
    public function addCompany(\CAB\CourseBundle\Entity\Company $company)
    {
        $this->company[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     */
    public function removeCompany(\CAB\CourseBundle\Entity\Company $company)
    {
        $this->company->removeElement($company);
    }

    /**
     * Get company
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompany()
    {
        return $this->company;
    }
}
