<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda
 * Date: 10/01/2016
 * Time: 23:44
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\TransactionPayment
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_transaction_payment")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\TransactionPaymentRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class TransactionPayment
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Course", inversedBy="courseTransactionPayment")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $transactionPaymentCourse;

    /**
     * @ORM\OneToOne(targetEntity="CAB\CourseBundle\Entity\TransactionResponse", mappedBy="reponseTransactionPayment", cascade={"persist", "remove"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $transactionResponse;

    /**
     * @var string $amount
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $amount;

    /**
     * @var string $signature
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $signature;

    /**
     * @var string $modeAction
     *
     * @Assert\NotBlank(message = "not.blank.modeAction")
     */
    protected $modeAction;

    /**
     * @var string $currency
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $currency;

    /**
     * @var string $pageAction
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $pageAction;

    /**
     * @var string $version
     *
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $version;

    /**
     * @var string $paymentConfig
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $paymentConfig;

    /**
     * @var string $captureDelay
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $captureDelay;

    /**
     * @var string $ctxMode
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $ctxMode;

    /**
     * @var string $transactionDate
     *
     * @ORM\Column(type="string", name="transaction_date", nullable=true)
     */
    private $transactionDate;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * Prepersist function
     *
     * @ORM\PrePersist
     *
     * @return Transaction
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount amount
     *
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt creation date
     *
     * @return Transaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set transactionDate
     *
     * @param string $transactionDate
     *  date of transaction
     * @return Transaction
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;

        return $this;
    }

    /**
     * Get transactionDate
     *
     * @return string
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * Set signature
     *
     * @param string $signature signature
     *
     * @return Transaction
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set modeAction
     *
     * @param string $modeAction mode action
     *
     * @return Transaction
     */
    public function setModeAction($modeAction)
    {
        $this->modeAction = $modeAction;

        return $this;
    }

    /**
     * Get modeAction
     *
     * @return string
     */
    public function getModeAction()
    {
        return $this->modeAction;
    }

    /**
     * Set currency
     *
     * @param integer $currency currency
     *
     * @return Transaction
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return integer
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set pageAction
     *
     * @param string $pageAction page action
     *
     * @return Transaction
     */
    public function setPageAction($pageAction)
    {
        $this->pageAction = $pageAction;

        return $this;
    }

    /**
     * Get pageAction
     *
     * @return string
     */
    public function getPageAction()
    {
        return $this->pageAction;
    }

    /**
     * Set version
     *
     * @param string $version version
     *
     * @return Transaction
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set paymentConfig
     *
     * @param string $paymentConfig config
     *
     * @return Transaction
     */
    public function setPaymentConfig($paymentConfig)
    {
        $this->paymentConfig = $paymentConfig;

        return $this;
    }

    /**
     * Get paymentConfig
     *
     * @return string
     */
    public function getPaymentConfig()
    {
        return $this->paymentConfig;
    }

    /**
     * Set captureDelay
     *
     * @param integer $captureDelay delay capture
     *
     * @return Transaction
     */
    public function setCaptureDelay($captureDelay)
    {
        $this->captureDelay = $captureDelay;

        return $this;
    }

    /**
     * Get captureDelay
     *
     * @return integer
     */
    public function getCaptureDelay()
    {
        return $this->captureDelay;
    }

    /**
     * Set ctxMode
     *
     * @param string $ctxMode ctx mode
     *
     * @return Transaction
     */
    public function setCtxMode($ctxMode)
    {
        $this->ctxMode = $ctxMode;

        return $this;
    }

    /**
     * Get ctxMode
     *
     * @return string
     */
    public function getCtxMode()
    {
        return $this->ctxMode;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->courseTransactionPayment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set transactionPaymentCourse
     *
     * @param \CAB\CourseBundle\Entity\Course $transactionPaymentCourse
     * @return TransactionPayment
     */
    public function setTransactionPaymentCourse(\CAB\CourseBundle\Entity\Course $transactionPaymentCourse = null)
    {
        $this->transactionPaymentCourse = $transactionPaymentCourse;

        return $this;
    }

    /**
     * Get transactionPaymentCourse
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getTransactionPaymentCourse()
    {
        return $this->transactionPaymentCourse;
    }

    /**
     * Set transactionResponse
     *
     * @param \CAB\CourseBundle\Entity\TransactionResponse $transactionResponse
     * @return TransactionPayment
     */
    public function setTransactionResponse(\CAB\CourseBundle\Entity\TransactionResponse $transactionResponse = null)
    {
        $this->transactionResponse = $transactionResponse;

        return $this;
    }

    /**
     * Get transactionResponse
     *
     * @return \CAB\CourseBundle\Entity\TransactionResponse
     */
    public function getTransactionResponse()
    {
        return $this->transactionResponse;
    }
}
