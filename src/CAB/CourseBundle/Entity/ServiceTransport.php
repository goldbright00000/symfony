<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\ServiceTransport
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_service_transport")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\ServicetransportRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class ServiceTransport {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $from
     *
     * @ORM\Column(name="service_name", type="string", length=255, nullable=false)
     */
    protected $serviceName;

    /**
     * @var integer $from
     *
     * @ORM\Column(name="price_view", type="boolean")
     */
    protected $priceView;

    /**
     * @ORM\ManyToMany(targetEntity="CAB\CourseBundle\Entity\Zone", inversedBy="serviceTransports")
     * @ORM\JoinTable(name="cab_services_transport_zones")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $zones;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="serviceTransport")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $company;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\UserBundle\Entity\User", mappedBy="servicesTransport")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $users;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\CourseBundle\Entity\Vehicule", mappedBy="servicesTransportVehicles")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $vehicles;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\CourseBundle\Entity\Course", mappedBy="serviceTransportCourses")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $course;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\CourseBundle\Entity\Course", mappedBy="serviceTransportCoursesBack")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $courseBack;

    /**
     * @var integer $serviceEmail
     *
     * @ORM\Column(name="service_email", type="string", length=255, nullable=true)
     */
    protected $serviceEmail;

    /**
     * @var integer $message
     *
     * @ORM\Column(name="service_message", type="text", length=255, nullable=true)
     */
    protected $message;

    /**
     * @var integer $tel
     *
     * @ORM\Column(name="service_tel", type="text", length=255, nullable=true)
     */
    protected $tel;

    /**
     * @var integer $ctr
     *
     * @ORM\Column(name="service_ctr", type="text", length=255, nullable=true)
     */
    protected $ctr;

    /**
     * @var integer $ref
     *
     * @ORM\Column(name="service_ref", type="text", length=255, nullable=true)
     */
    protected $ref;

    /**
     * @var integer $serviceEmailCompta
     *
     * @ORM\Column(name="service_emailcompta", type="text", length=255, nullable=true)
     */
    protected $serviceEmailCompta;

    /**
     * @var integer $lotnumber
     *
     * @ORM\Column(name="service_lotnumber", type="text", length=255, nullable=true)
     */
    protected $lotnumber;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    /**
     * Method description
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getServiceName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     * @return ServiceTransport
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ServiceTransport
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ServiceTransport
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->particularPeriods = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add zones
     *
     * @param \CAB\CourseBundle\Entity\Zone $zones
     * @return ServiceTransport
     */
    public function addZone(\CAB\CourseBundle\Entity\Zone $zones)
    {
        $this->zones[] = $zones;

        return $this;
    }

    /**
     * Remove zones
     *
     * @param \CAB\CourseBundle\Entity\Zone $zones
     */
    public function removeZone(\CAB\CourseBundle\Entity\Zone $zones)
    {
        $this->zones->removeElement($zones);
    }

    /**
     * Get zones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * @var string $dateStart
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=true)
     */
    protected $dateStart;

    /**
     * @var string $dateEnd
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    protected $dateEnd;


    /**
     * Add users
     *
     * @param \CAB\UserBundle\Entity\User $users
     * @return ServiceTransport
     */
    public function addUser(\CAB\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \CAB\UserBundle\Entity\User $users
     */
    public function removeUser(\CAB\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add vehicles
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicles
     * @return ServiceTransport
     */
    public function addVehicle(\CAB\CourseBundle\Entity\Vehicule $vehicles)
    {
        $this->vehicles[] = $vehicles;

        return $this;
    }

    /**
     * Remove vehicles
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicles
     */
    public function removeVehicle(\CAB\CourseBundle\Entity\Vehicule $vehicles)
    {
        $this->vehicles->removeElement($vehicles);
    }

    /**
     * Get vehicles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return ServiceTransport
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return ServiceTransport
     */
    public function addCourse(\CAB\CourseBundle\Entity\Course $course)
    {
        $this->course[] = $course;

        return $this;
    }

    /**
     * Remove course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     */
    public function removeCourse(\CAB\CourseBundle\Entity\Course $course)
    {
        $this->course->removeElement($course);
    }

    /**
     * Get course
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Add courseBack
     *
     * @param \CAB\CourseBundle\Entity\Course $courseBack
     * @return ServiceTransport
     */
    public function addCourseBack(\CAB\CourseBundle\Entity\Course $courseBack)
    {
        $this->courseBack[] = $courseBack;

        return $this;
    }

    /**
     * Remove courseBack
     *
     * @param \CAB\CourseBundle\Entity\Course $courseBack
     */
    public function removeCourseBack(\CAB\CourseBundle\Entity\Course $courseBack)
    {
        $this->courseBack->removeElement($courseBack);
    }

    /**
     * Get courseBack
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseBack()
    {
        return $this->courseBack;
    }

    /**
     * Set priceView
     *
     * @param boolean $priceView
     * @return ServiceTransport
     */
    public function setPriceView($priceView)
    {
        $this->priceView = $priceView;

        return $this;
    }

    /**
     * Get priceView
     *
     * @return boolean
     */
    public function getPriceView()
    {
        return $this->priceView;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return ServiceTransport
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return ServiceTransport
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set serviceEmail
     *
     * @param string $serviceEmail
     * @return ServiceTransport
     */
    public function setServiceEmail($serviceEmail)
    {
        $this->serviceEmail = $serviceEmail;

        return $this;
    }

    /**
     * Get serviceEmail
     *
     * @return string
     */
    public function getServiceEmail()
    {
        return $this->serviceEmail;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return ServiceTransport
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set ctr
     *
     * @param string $ctr
     * @return ServiceTransport
     */
    public function setCtr($ctr)
    {
        $this->ctr = $ctr;

        return $this;
    }

    /**
     * Get ctr
     *
     * @return string
     */
    public function getCtr()
    {
        return $this->ctr;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return ServiceTransport
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set serviceEmailCompta
     *
     * @param string $serviceEmailCompta
     * @return ServiceTransport
     */
    public function setServiceEmailCompta($serviceEmailCompta)
    {
        $this->serviceEmailCompta = $serviceEmailCompta;

        return $this;
    }

    /**
     * Get serviceEmailCompta
     *
     * @return string
     */
    public function getServiceEmailCompta()
    {
        return $this->serviceEmailCompta;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return ServiceTransport
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set lotnumber
     *
     * @param string $lotnumber
     * @return ServiceTransport
     */
    public function setLotnumber($lotnumber)
    {
        $this->lotnumber = $lotnumber;

        return $this;
    }

    /**
     * Get lotnumber
     *
     * @return string
     */
    public function getLotnumber()
    {
        return $this->lotnumber;
    }
}
