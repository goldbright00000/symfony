<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 09/12/2015
 * Time: 18:44
 */

namespace CAB\CourseBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\HourTarifNight
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_hour_tarif_night")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\HourTarifNightRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */

class HourTarifNight {

    /**
    * @var integer $id
    *
    * @ORM\Column(name="id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
    * @var integer $labelTarif
    *
    * @ORM\Column(name="label_tarif", type="string", length=64, nullable=false)
     * @Assert\NotBlank(message = "not.blank.label_tarif")
    */
    protected $labelTarif;

    /**
     * @var time $from
     *
     * @ORM\Column(name="from", type="time", nullable=false)
     * @Assert\NotBlank(message = "not.blank.from")
     */
    protected $from;

    /**
     * @var time $to
     *
     * @ORM\Column(name="to", type="time", nullable=false)
     * @Assert\NotBlank(message = "not.blank.to")
     */
    protected $to;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Tarif", mappedBy="hourNightTarif")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $tarif;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLabelTarif();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set labelTarif
     *
     * @param string $labelTarif
     * @return HourTarifNight
     */
    public function setLabelTarif($labelTarif)
    {
        $this->labelTarif = $labelTarif;

        return $this;
    }

    /**
     * Get labelTarif
     *
     * @return string
     */
    public function getLabelTarif()
    {
        return $this->labelTarif;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tarif = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set from
     *
     * @param \DateTime $from
     * @return HourTarifNight
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \DateTime $to
     * @return HourTarifNight
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Add tarif
     *
     * @param \CAB\CourseBundle\Entity\Tarif $tarif
     * @return HourTarifNight
     */
    public function addTarif(\CAB\CourseBundle\Entity\Tarif $tarif)
    {
        $this->tarif[] = $tarif;

        return $this;
    }

    /**
     * Remove tarif
     *
     * @param \CAB\CourseBundle\Entity\Tarif $tarif
     */
    public function removeTarif(\CAB\CourseBundle\Entity\Tarif $tarif)
    {
        $this->tarif->removeElement($tarif);
    }

    /**
     * Get tarif
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTarif()
    {
        return $this->tarif;
    }

}
