<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CAB\UserBundle\Entity\User;
use JMS\Serializer\Annotation;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * VehiculeClean
 *
 * @ORM\Table(name="cab_vehicule_clean")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\VehiculeCleanRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Annotation\ExclusionPolicy("all")
 * @Vich\Uploadable
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class VehiculeClean
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="useraffects")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Vehicule", inversedBy="vehiculeaffects", fetch="EAGER")
     * @ORM\JoinColumn(name="vehicule_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicule;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="vehicleClean")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="interior", type="boolean", length=255)
     */
    private $interior = false;

    /**
     * @var string
     *
     * @ORM\Column(name="exterior", type="boolean", length=255)
     */
    private $exterior = false;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string", length=255)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="km", type="string", length=255)
     */
    private $km;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;


    /**
     * @Vich\UploadableField(mapping="vehicule_clean", fileNameProperty="fileName")
     *
     * @var File
     */
    private $file;

    /**
     * @var string $imageName
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $fileName = null;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        if ($this->getVehicule() !== null) {
            $this->setCompany($this->getVehicule()->getCompany());
            $this->setDriver($this->getVehicule()->getDriver());
        }
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set interior
     *
     * @param string $interior
     * @return VehiculeClean
     */
    public function setInterior($interior)
    {
        $this->interior = $interior;

        return $this;
    }

    /**
     * Get interior
     *
     * @return string
     */
    public function getInterior()
    {
        return $this->interior;
    }

    /**
     * Set exterior
     *
     * @param string $exterior
     * @return VehiculeClean
     */
    public function setExterior($exterior)
    {
        $this->exterior = $exterior;

        return $this;
    }

    /**
     * Get exterior
     *
     * @return string
     */
    public function getExterior()
    {
        return $this->exterior;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return VehiculeClean
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set km
     *
     * @param string $km
     * @return VehiculeClean
     */
    public function setKm($km)
    {
        $this->km = $km;

        return $this;
    }

    /**
     * Get km
     *
     * @return string
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return VehiculeClean
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return VehiculeClean
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return VehiculeClean
     */
    public function setDriver(\CAB\UserBundle\Entity\User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set vehicule
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicule
     * @return VehiculeClean
     */
    public function setVehicule(\CAB\CourseBundle\Entity\Vehicule $vehicule = null)
    {
        $this->vehicule = $vehicule;

        return $this;
    }

    /**
     * Get vehicule
     *
     * @return \CAB\CourseBundle\Entity\Vehicule
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return VehiculeClean
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return VehiculeClean
     */
    public function setFile(File $file = null)
    {
      $this->file = $file;

      if ($file) {
        // It is required that at least one field changes if you are using doctrine
        // otherwise the event listeners won't be called and the file is lost
        $this->updatedAt = new \DateTimeImmutable();
      }

      return $this;
    }

    /**
     * @return File|null
     */
    public function getFile()
    {
      return $this->file;
    }


    /**
     * Set fileName
     *
     * @param string $fileName
     * @return VehiculeClean
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }
}
