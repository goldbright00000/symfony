<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * VehiculeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class VehiculeRepository extends EntityRepository {

    /**
     * @param decimal $long
     * @param decimal $lat
     * @param integer $nbPerson
     * @param string  $distance
     *
     * @return array
     */
    public function findByLangLat($long, $lat, $nbPerson, $distance) {
        return $this->getEntityManager()->createQuery(
                                "SELECT distinct v.id vihiculeID, v.nbPerson, c.companyName,
                tv.id typeVehicleID, tv.nameType, SQRT(
                POW(69.1 * (z.latitude - $lat), 2) +
                POW(69.1 * ($long - z.longitude) * COS(z.latitude / 57.3), 2)) AS distance
                FROM CABCourseBundle:Vehicule v
                join CABCourseBundle:Company c WHERE v.company = c.id
                join CABCourseBundle:Zone z WHERE z.companyZone = c.id
                join CABCourseBundle:Tarif t WHERE t.company = c.id
                join CABCourseBundle:TypeVehicule tv WHERE tv.id = v.typeVehicule
                where v.nbPerson >=  ?2
                ORDER BY distance ASC
                "
                        )
                        ->setParameter(2, $nbPerson)
	                    ->setCacheable(true)->setCacheRegion('cache_long_time')
                        ->getResult();
    }

    /**
     * @param decimal $long
     * @param decimal $lat
     * @param integer $nbPerson
     *
     * @return array
     */
    public function getPriceByVehicle($long, $lat, $nbPerson, $typeVehicle = null) {
        //$formule="(6366*acos(cos(radians($lat))*cos(radians('lat'))*cos(radians('lon') -radians($long))+sin(radians($lat))*sin(radians('lat'))))";
        $query = "SELECT distinct v.id vihiculeID, v.nbPerson, c.companyName,
                tv.id typeVehicleID, tv.nameType, z.zoneRaduis, SQRT(
                POW(69.1 * (z.latitude - $lat), 2) +
                POW(69.1 * ($long - z.longitude) * COS(z.latitude / 57.3), 2)) AS distance
                FROM CABCourseBundle:Vehicule v
                join CABCourseBundle:Company c WHERE v.company = c.id
                join CABCourseBundle:Zone z WHERE z.companyZone = c.id
                join CABCourseBundle:TypeVehicule tv WHERE tv.id = v.typeVehicule
                join CABCourseBundle:Tarif t WHERE t.typeVehiculeTarif = tv.id
                where v.nbPerson >=  ?1
                ";
        if ($typeVehicle !== null) {
            $query .= " AND tv.id =  $typeVehicle";
        }

        $query .= " ORDER BY distance ASC";


        return $this->getEntityManager()->createQuery($query)
                        ->setParameter(1, $nbPerson)
	                    ->setCacheable(true)->setCacheRegion('cache_long_time')
                        ->getResult();
    }

    /**
     * @param object $driver_id
     *
     * @return array
     */
    public function getVehicleAffectByDriver($driver_id) {
        $dql = "SELECT 
        va.km as vkm, 
        v.vehiculeName as name, 
        v.licensePlateNumber,
        v.id 
        FROM CABCourseBundle:VehiculeAffect  as va INNER JOIN va.vehicule as v 
        WHERE va.driver = :driver_id
        ORDER BY va.id DESC";

        $query = $this->getEntityManager()->createQuery($dql)
                        ->setParameters(array(
                            'driver_id' => $driver_id,
                        ))->setMaxResults(1);
	    $query->setCacheable(true)->setCacheRegion('cache_long_time');

        try {
            return $query->getOneOrNullResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }


     /**
     * @param object $vehicule_id
     *
     * @return array
     */
    public function getVehicleById($vehicule_id) {
        $dql = "SELECT va.km as vkm, v.vehiculeName as name, v.licensePlateNumber,v.id FROM CABCourseBundle:VehiculeAffect as va INNER JOIN va.vehicule as v WHERE v.id = :vehicule_id ORDER BY va.createdAt DESC";
        $query = $this->getEntityManager()->createQuery($dql)
                        ->setParameters(array(
                            'vehicule_id' => $vehicule_id
                        ))->setMaxResults(1);
	    $query->setCacheable(true)->setCacheRegion('cache_long_time');
        try {
            return $query->getOneOrNullResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

    /**
     * Get total vehicle
     *
     *
     * @return bool|int
     */
    public function getTotalvehicle() {
        return $this->createQueryBuilder('a')
                        ->select('COUNT(a)')
                        ->getQuery()
	                    ->getSingleScalarResult();


    }

    public function getVehiclesByCompany(array $aCompany)
    {
        $query = $this->createQueryBuilder('c')
            ->Where('c.company in (:company)')
            ->setParameter('company', $aCompany)
            ->orderBy('c.createdAt', 'DESC');
	    $query->setCacheable(true)->setCacheRegion('cache_long_time');
        return $query;
    }


    /**
     *
     * @return array
     */
    public function getVehiculeByExpiredDocsAll($user_id) {
        $dql = "
            SELECT 
              va.expiredocGreyRegistrationCard, 
              va.expiredocInsuranceCard, 
              va.expiredocVehicleTechCertif, 
              va.id, 
              va.licensePlateNumber
            FROM CABCourseBundle:Vehicule as va 
            INNER JOIN va.company as c 
            WHERE 
              c.contact = :user_id
              AND (
                  va.expiredocGreyRegistrationCard IS NOT NULL
                  OR va.expiredocInsuranceCard IS NOT NULL
                  OR va.expiredocVehicleTechCertif IS NOT NULL
              )
            ";
        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameters([
                'user_id' => $user_id
            ]);
	    $query->setCacheable(true)->setCacheRegion('cache_long_time');
        try {
            return $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }
}
