<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * CAB\CourseBundle\Entity\Company
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_company")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\CompanyRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Company implements CompanyInterface{

    /*
     * @const
     */
    const SERVER_PATH_TO_IMAGE_FOLDER = 'uploads/partner/logo';
    const SERVER_PATH_TO_DOC_FOLDER = 'uploads/partner/doc';
    const COMPANY_BUSINESS = 1;
    const COMPANY_NORMAL = 0;
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Course", mappedBy="company", cascade={"all"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $course;
    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Forfait", mappedBy="company", cascade={"all"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $forfait;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\VehiculeFuel", mappedBy="company", cascade={"all"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicleFuel;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\VehiculeClean", mappedBy="company", cascade={"all"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicleClean;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\VehiculeMaintenance", mappedBy="companyVehicleMaintenance", cascade={"all"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicleMaintenance;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="contacts")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * @Groups({"User"})
     * MaxDepth(2)
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $contact;

    /**
     * @ORM\OneToMany(targetEntity="CAB\UserBundle\Entity\User", mappedBy="agentCompany")
     * @Groups({"User"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $companyAgents;

    /**
     * @var $businessUser
     * @ORM\OneToMany(targetEntity="CAB\UserBundle\Entity\User", mappedBy="businessCompany")
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $businessUser;

    /**
     * @ORM\OneToMany(targetEntity="CAB\UserBundle\Entity\User", mappedBy="customerCompany")
     * @Groups({"User"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $companyCustomers;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Tax", mappedBy="company")
     * @Serializer\Exclude()
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $taxes;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\CompanyType", inversedBy="companiesType")
     * @ORM\JoinColumn(name="company_type_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $companyType;

    /**
     * @var string $siret
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.siret")
     */
    protected $siret;

    /**
     * @var string $licenceDRE
     *
     * @ORM\Column(name="licence_dre", type="string", length=255, nullable=true)
     */
    protected $licenceDRE;


    /**
    * @var string inscriptionRegistreVTC
    *
    * @ORM\Column(name="inscription_registre_VTC", type="string", length=255, nullable=true)
    */
    protected $inscriptionRegistreVTC;

    /**
     * @var string $stationnementLicence
     *
     * @ORM\Column(name="stationnement_licence", type="string", length=255, nullable=true)
     */
    protected $stationnementLicence;

    /**
     * @var string $assurance
     *
     * @ORM\Column(name="assurance", type="string", length=255, nullable=true)
     */
    protected $assurance;

    /**
     * @var string $kbis
     *
     * @ORM\Column(name="kbis", type="string", length=255, nullable=true)
     */
    protected $kbis;

    /**
     * @var string $identite_piece
     *
     * @ORM\Column(name="identite_piece", type="string", length=255, nullable=true)
     */
    protected $identitePiece;

    /**
     * @var string $companyName
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.companyName")
     */
    protected $companyName;

    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    protected $address;

    /**
     * @var string $city
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var integer $postCode
     *
     * @ORM\Column(name="post_code", type="string", length=255, nullable=true)
     */
    protected $postCode;

    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     * @Assert\NotBlank(message = "not.blank.country")
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $lng;

    /**
     * @ORM\Column(type="string",length=20, nullable=false)
     * @Assert\NotBlank(message = "not.blank.phone")
     */
    private $phone = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $mobile = null;

    /**
     * @var string $contactMail
     * @Assert\NotBlank(message = "not.blank.contact")
     * @Assert\Email()
     * @ORM\Column(type="string", name="contact_mail", nullable=false, unique=true)
     */
    protected $contactMail;

    /**
     * @var string $status
     *
     * @ORM\Column(name="status_company", type="string", nullable=true)
     */
    protected $statusCompany;

    /**
     * @var boolean $viewlic
     *
     * @ORM\Column(name="view_lic", type="boolean")
     */
    private $viewlic= false;



    /**
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $logo = null;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $fileAssurance;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $fileKbis;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $fileIdentitePiece;

    /**
     * @var boolean $isActive
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive = TRUE;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Vehicule", mappedBy="company", cascade={"remove", "persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $companiesVehicule;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Country", inversedBy="company")
     * @ORM\JoinColumn(name="format_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public", "Course"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $format;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Tarif", mappedBy="company", cascade={"remove", "persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $companyTarif;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Zone", mappedBy="companyZone")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $companyZones;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\ParticularPeriod", mappedBy="company")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $particularPeriods;

    /**
     * @ORM\OneToMany(targetEntity="ServiceTransport", mappedBy="company")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $serviceTransport;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\UserBundle\Entity\User", mappedBy="driverCompany")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $users;

    /**
     * @var integer $isBusiness
     *
     * @ORM\Column(name="is_business", type="integer")
     */
    private $isBusiness;

    /**
     * Only for business company
     * @var boolean $isCredit
     *
     * @ORM\Column(name="is_credit", type="boolean", nullable=true)
     */
    private $isCredit;

    /**
     * Method __tostring
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->isBusiness) {
            return $this->getCompanyName().' (Business Company)';
        }

        return $this->getCompanyName();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        if ($this->getCountry() !== null) {

        }
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    /**
     * Lifecycle callback to upload the file to the server
     */
    public function lifecycleFileUpload() {
        $this->upload();
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile($file = null)
    {
        if (isset($this->file)) {
            $this->file = $file;
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        if (isset($this->file)) {
            return $this->file;
        }
    }

    /**
     * Sets fileAssurance.
     *
     * @param UploadedFile $fileAssurance
     */
    public function setFileAssurance($fileAssurance = null)
    {
        $this->fileAssurance = $fileAssurance;
    }

    /**
     * Get fileAssurance.
     *
     * @return UploadedFile
     */
    public function getFileAssurance()
    {
        return $this->fileAssurance;
    }

    /**
     * Sets fileKbis.
     *
     * @param UploadedFile $fileKbis
     */
    public function setFileKbis($fileKbis = null)
    {
        $this->fileKbis = $fileKbis;
    }

    /**
     * Get fileKbis.
     *
     * @return UploadedFile
     */
    public function getFileKbis()
    {
        return $this->fileKbis;
    }

    /**
     * Sets fileIdentitePiece.
     *
     * @param UploadedFile $fileIdentitePiece
     */
    public function setFileIdentitePiece($fileIdentitePiece = null)
    {
        $this->fileIdentitePiece = $fileIdentitePiece;
    }

    /**
     * Get fileIdentitePiece.
     *
     * @return UploadedFile
     */
    public function getFileIdentitePiece()
    {
        return $this->fileIdentitePiece;
    }

    protected function getUploadRootDir(){
        // le chemin absolu du répertoire où les avatar uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadRootDocDir(){
        // le chemin absolu du répertoire où les avatar uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/' . $this->getUploadDocDir();
    }

    protected function getUploadDir(){
        return self::SERVER_PATH_TO_IMAGE_FOLDER;
    }

    protected function getUploadDocDir()
    {
        return self::SERVER_PATH_TO_DOC_FOLDER;
    }

    /**
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if(null !== $this->file){
            $this->logo = sha1(uniqid(mt_rand(), true)).'.'.$this->file->guessExtension();
        }
        if(null !== $this->fileAssurance){
            $this->assurance = sha1(uniqid(mt_rand(), true)).'.'.$this->fileAssurance->guessExtension();
        }

        if(null !== $this->fileKbis){
            $this->kbis = sha1(uniqid(mt_rand(), true)).'.'.$this->fileKbis->guessExtension();
        }

        if(null !== $this->fileIdentitePiece){
            $this->identitePiece = sha1(uniqid(mt_rand(), true)).'.'.$this->fileIdentitePiece->guessExtension();
        }
    }

    /**
     *
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if(null === $this->file){
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->logo);

        unset($this->file);

        if(null === $this->fileAssurance){
            return;
        }

        $this->fileAssurance->move($this->getUploadRootDocDir(), $this->assurance);

        unset($this->fileAssurance);


        if(null === $this->identitePiece){
            return;
        }

        $this->fileIdentitePiece->move($this->getUploadRootDocDir(), $this->identitePiece);

        unset($this->fileIdentitePiece);

        if(null === $this->identitePiece){
            return;
        }

        $this->fileKbis->move($this->getUploadRootDocDir(), $this->kbis);

        unset($this->fileKbis);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload(){
        if($this->file != ""){
            if($file = $this->getAbsolutePath()){
                unlink($file);
            }
        }

        if($this->fileAssurance != ""){
            if($fileAssurance = $this->getAssuranceAbsolutePath()){
                unlink($fileAssurance);
            }
        }

        if($this->fileKbis != ""){
            if($fileKbis = $this->getKbisAbsolutePath()){
                unlink($fileKbis);
            }
        }

        if($this->fileIdentitePiece != ""){
            if($fileIdentitePiece = $this->getIdentiteAbsolutePath()){
                unlink($fileAssurance);
            }
        }
    }
    public function getAbsolutePath()
    {
        return null === $this->logo ? null : $this->getUploadRootDir().'/'.$this->logo;
    }
    public function getAssuranceAbsolutePath()
    {
        return null === $this->assurance ? null : $this->getUploadRootDocDir().'/'.$this->assurance;
    }
    public function getKbisAbsolutePath()
    {
        return null === $this->kbis ? null : $this->getUploadRootDocDir().'/'.$this->kbis;
    }
    public function getIdentiteAbsolutePath()
    {
        return null === $this->identitePiece ? null : $this->getUploadRootDocDir().'/'.$this->identitePiece;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Company
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Company
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set contactMail
     *
     * @param string $contactMail
     * @return Company
     */
    public function setContactMail($contactMail)
    {
        $this->contactMail = $contactMail;

        return $this;
    }

    /**
     * Get contactMail
     *
     * @return string
     */
    public function getContactMail()
    {
        return $this->contactMail;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Company
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Company
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Company
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Company
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set contact
     *
     * @param \CAB\UserBundle\Entity\User $contact
     * @return Company
     */
    public function setContact(\CAB\UserBundle\Entity\User $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return Company
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Add companiesVehicule
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $companiesVehicule
     * @return Company
     */
    public function addCompaniesVehicule(\CAB\CourseBundle\Entity\Vehicule $companiesVehicule)
    {
        $this->companiesVehicule[] = $companiesVehicule;

        return $this;
    }

    /**
     * Remove companiesVehicule
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $companiesVehicule
     */
    public function removeCompaniesVehicule(\CAB\CourseBundle\Entity\Vehicule $companiesVehicule)
    {
        $this->companiesVehicule->removeElement($companiesVehicule);
    }

    /**
     * Get companiesVehicule
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompaniesVehicule()
    {
        return $this->companiesVehicule;
    }

    /**
     * Set lng
     *
     * @param string $lng
     * @return Company
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set statusCompany
     *
     * @param string $statusCompany
     * @return Company
     */
    public function setStatusCompany($statusCompany)
    {
        $this->statusCompany = $statusCompany;

        return $this;
    }

    /**
     * Get statusCompany
     *
     * @return string
     */
    public function getStatusCompany()
    {
        return $this->statusCompany;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Company
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set companyType
     *
     * @param \CAB\CourseBundle\Entity\CompanyType $companyType
     * @return Company
     */
    public function setCompanyType(\CAB\CourseBundle\Entity\CompanyType $companyType = null)
    {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Get companyType
     *
     * @return \CAB\CourseBundle\Entity\CompanyType
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * Set siret
     *
     * @param string $siret
     * @return Company
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set licenceDRE
     *
     * @param string $licenceDRE
     * @return Company
     */
    public function setLicenceDRE($licenceDRE)
    {
        $this->licenceDRE = $licenceDRE;

        return $this;
    }

    /**
     * Get licenceDRE
     *
     * @return string
     */
    public function getLicenceDRE()
    {
        return $this->licenceDRE;
    }

    /**
     * Set inscriptionRegistreVTC
     *
     * @param string $inscriptionRegistreVTC
     * @return Company
     */
    public function setInscriptionRegistreVTC($inscriptionRegistreVTC)
    {
        $this->inscriptionRegistreVTC = $inscriptionRegistreVTC;

        return $this;
    }

    /**
     * Get inscriptionRegistreVTC
     *
     * @return string
     */
    public function getInscriptionRegistreVTC()
    {
        return $this->inscriptionRegistreVTC;
    }

    /**
     * Set stationnementLicence
     *
     * @param string $stationnementLicence
     * @return Company
     */
    public function setStationnementLicence($stationnementLicence)
    {
        $this->stationnementLicence = $stationnementLicence;

        return $this;
    }

    /**
     * Get stationnementLicence
     *
     * @return string
     */
    public function getStationnementLicence()
    {
        return $this->stationnementLicence;
    }

    /**
     * Set assurance
     *
     * @param string $assurance
     * @return Company
     */
    public function setAssurance($assurance)
    {
        $this->assurance = $assurance;

        return $this;
    }

    /**
     * Get assurance
     *
     * @return string
     */
    public function getAssurance()
    {
        return $this->assurance;
    }

    /**
     * Set kbis
     *
     * @param string $kbis
     * @return Company
     */
    public function setKbis($kbis)
    {
        $this->kbis = $kbis;

        return $this;
    }

    /**
     * Get kbis
     *
     * @return string
     */
    public function getKbis()
    {
        return $this->kbis;
    }

    /**
     * Set identite_piece
     *
     * @param string $identitePiece
     * @return Company
     */
    public function setIdentitePiece($identitePiece)
    {
        $this->identite_piece = $identitePiece;

        return $this;
    }

    /**
     * Get identite_piece
     *
     * @return string
     */
    public function getIdentitePiece()
    {
        return $this->identite_piece;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Company
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Add particularPeriods
     *
     * @param \CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods
     * @return Company
     */
    public function addParticularPeriod(\CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods)
    {
        $this->particularPeriods[] = $particularPeriods;

        return $this;
    }

    /**
     * Remove particularPeriods
     *
     * @param \CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods
     */
    public function removeParticularPeriod(\CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods)
    {
        $this->particularPeriods->removeElement($particularPeriods);
    }

    /**
     * Get particularPeriods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticularPeriods()
    {
        return $this->particularPeriods;
    }

    /**
     * Add serviceTransport
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransport
     * @return Company
     */
    public function addServiceTransport(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransport)
    {
        $this->serviceTransport[] = $serviceTransport;

        return $this;
    }

    /**
     * Remove serviceTransport
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransport
     */
    public function removeServiceTransport(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransport)
    {
        $this->serviceTransport->removeElement($serviceTransport);
    }

    /**
     * Get serviceTransport
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransport()
    {
        return $this->serviceTransport;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     * @return Company
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Add companyZones
     *
     * @param \CAB\CourseBundle\Entity\Zone $companyZones
     * @return Company
     */
    public function addCompanyZone(\CAB\CourseBundle\Entity\Zone $companyZones)
    {
        $this->companyZones[] = $companyZones;

        return $this;
    }

    /**
     * Remove companyZones
     *
     * @param \CAB\CourseBundle\Entity\Zone $companyZones
     */
    public function removeCompanyZone(\CAB\CourseBundle\Entity\Zone $companyZones)
    {
        $this->companyZones->removeElement($companyZones);
    }

    /**
     * Get companyZones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyZones()
    {
        return $this->companyZones;
    }

    /**
     * Add users
     *
     * @param \CAB\UserBundle\Entity\User $users
     * @return Company
     */
    public function addUser(\CAB\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \CAB\UserBundle\Entity\User $users
     */
    public function removeUser(\CAB\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return Company
     */
    public function addCourse(\CAB\CourseBundle\Entity\Course $course)
    {
        $this->course[] = $course;

        return $this;
    }

    /**
     * Remove course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     */
    public function removeCourse(\CAB\CourseBundle\Entity\Course $course)
    {
        $this->course->removeElement($course);
    }

    /**
     * Get course
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set companyTarif
     *
     * @param \CAB\CourseBundle\Entity\Tarif $companyTarif
     * @return Company
     */
    public function setCompanyTarif(\CAB\CourseBundle\Entity\Tarif $companyTarif = null)
    {
        $this->companyTarif = $companyTarif;

        return $this;
    }

    /**
     * Get companyTarif
     *
     * @return \CAB\CourseBundle\Entity\Tarif
     */
    public function getCompanyTarif()
    {
        return $this->companyTarif;
    }

    /**
     * Add companyAgents
     *
     * @param \CAB\UserBundle\Entity\User $companyAgents
     * @return Company
     */
    public function addCompanyAgent(\CAB\UserBundle\Entity\User $companyAgents)
    {
        $this->companyAgents[] = $companyAgents;

        return $this;
    }

    /**
     * Remove companyAgents
     *
     * @param \CAB\UserBundle\Entity\User $companyAgents
     */
    public function removeCompanyAgent(\CAB\UserBundle\Entity\User $companyAgents)
    {
        $this->companyAgents->removeElement($companyAgents);
    }

    /**
     * Get companyAgents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyAgents()
    {
        return $this->companyAgents;
    }

    /**
     * Add companyCustomers
     *
     * @param \CAB\UserBundle\Entity\User $companyCustomers
     * @return Company
     */
    public function addCompanyCustomer(\CAB\UserBundle\Entity\User $companyCustomers)
    {
        $this->companyCustomers[] = $companyCustomers;

        return $this;
    }

    /**
     * Remove companyCustomers
     *
     * @param \CAB\UserBundle\Entity\User $companyCustomers
     */
    public function removeCompanyCustomer(\CAB\UserBundle\Entity\User $companyCustomers)
    {
        $this->companyCustomers->removeElement($companyCustomers);
    }

    /**
     * Get companyCustomers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyCustomers()
    {
        return $this->companyCustomers;
    }

    /**
     * Set format
     *
     * @param \CAB\CourseBundle\Entity\Country $format
     * @return Company
     */
    public function setFormat(\CAB\CourseBundle\Entity\Country $format = null)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return \CAB\CourseBundle\Entity\Country
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Add taxes
     *
     * @param \CAB\CourseBundle\Entity\Company $taxes
     * @return Company
     */
    public function addTax(\CAB\CourseBundle\Entity\Company $taxes)
    {
        $this->taxes[] = $taxes;

        return $this;
    }

    /**
     * Remove taxes
     *
     * @param \CAB\CourseBundle\Entity\Company $taxes
     */
    public function removeTax(\CAB\CourseBundle\Entity\Company $taxes)
    {
        $this->taxes->removeElement($taxes);
    }

    /**
     * Get taxes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * Set isBusiness
     *
     * @param boolean $isBusiness
     * @return Company
     */
    public function setIsBusiness($isBusiness)
    {
        $this->isBusiness = $isBusiness;

        return $this;
    }

    /**
     * Get isBusiness
     *
     * @return boolean
     */
    public function getIsBusiness()
    {
        return $this->isBusiness;
    }

    /**
     * Add vehicleFuel
     *
     * @param \CAB\CourseBundle\Entity\Course $vehicleFuel
     * @return Company
     */
    public function addVehicleFuel(\CAB\CourseBundle\Entity\Course $vehicleFuel)
    {
        $this->vehicleFuel[] = $vehicleFuel;

        return $this;
    }

    /**
     * Remove vehicleFuel
     *
     * @param \CAB\CourseBundle\Entity\Course $vehicleFuel
     */
    public function removeVehicleFuel(\CAB\CourseBundle\Entity\Course $vehicleFuel)
    {
        $this->vehicleFuel->removeElement($vehicleFuel);
    }

    /**
     * Get vehicleFuel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicleFuel()
    {
        return $this->vehicleFuel;
    }

    /**
     * Add vehicleClean
     *
     * @param \CAB\CourseBundle\Entity\VehiculeClean $vehicleClean
     * @return Company
     */
    public function addVehicleClean(\CAB\CourseBundle\Entity\VehiculeClean $vehicleClean)
    {
        $this->vehicleClean[] = $vehicleClean;

        return $this;
    }

    /**
     * Remove vehicleClean
     *
     * @param \CAB\CourseBundle\Entity\VehiculeClean $vehicleClean
     */
    public function removeVehicleClean(\CAB\CourseBundle\Entity\VehiculeClean $vehicleClean)
    {
        $this->vehicleClean->removeElement($vehicleClean);
    }

    /**
     * Get vehicleClean
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicleClean()
    {
        return $this->vehicleClean;
    }

    /**
     * Add vehicleMaintenance
     *
     * @param \CAB\CourseBundle\Entity\VehiculeMaintenance $vehicleMaintenance
     * @return Company
     */
    public function addVehicleMaintenance(\CAB\CourseBundle\Entity\VehiculeMaintenance $vehicleMaintenance)
    {
        $this->vehicleMaintenance[] = $vehicleMaintenance;

        return $this;
    }

    /**
     * Remove vehicleMaintenance
     *
     * @param \CAB\CourseBundle\Entity\VehiculeMaintenance $vehicleMaintenance
     */
    public function removeVehicleMaintenance(\CAB\CourseBundle\Entity\VehiculeMaintenance $vehicleMaintenance)
    {
        $this->vehicleMaintenance->removeElement($vehicleMaintenance);
    }

    /**
     * Get vehicleMaintenance
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicleMaintenance()
    {
        return $this->vehicleMaintenance;
    }


    /**
     * Set businessUser
     *
     * @param \CAB\UserBundle\Entity\User $businessUser
     * @return Company
     */
    public function setBusinessUser(\CAB\UserBundle\Entity\User $businessUser = null)
    {
        $this->businessUser = $businessUser;

        return $this;
    }

    /**
     * Get businessUser
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getBusinessUser()
    {
        return $this->businessUser;
    }


    /**
     * Set isCredit
     *
     * @param boolean $isCredit
     * @return Company
     */
    public function setIsCredit ($isCredit)
    {
        $this->isCredit = $isCredit;

        return $this;
    }

    /**
     * Get isCredit
     *
     * @return boolean
     */
    public function getIsCredit ()
    {
        return $this->isCredit;
    }

    /**
     * Add forfait
     *
     * @param \CAB\CourseBundle\Entity\Forfait $forfait
     * @return Company
     */
    public function addForfait (\CAB\CourseBundle\Entity\Forfait $forfait)
    {
        $this->forfait[] = $forfait;

        return $this;
    }

    /**
     * Remove forfait
     *
     * @param \CAB\CourseBundle\Entity\Forfait $forfait
     */
    public function removeForfait (\CAB\CourseBundle\Entity\Forfait $forfait)
    {
        $this->forfait->removeElement($forfait);
    }

    /**
     * Get forfait
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForfait ()
    {
        return $this->forfait;
    }

    /**
     * Add companyTarif
     *
     * @param \CAB\CourseBundle\Entity\Tarif $companyTarif
     * @return Company
     */
    public function addCompanyTarif(\CAB\CourseBundle\Entity\Tarif $companyTarif)
    {
        $this->companyTarif[] = $companyTarif;

        return $this;
    }

    /**
     * Remove companyTarif
     *
     * @param \CAB\CourseBundle\Entity\Tarif $companyTarif
     */
    public function removeCompanyTarif(\CAB\CourseBundle\Entity\Tarif $companyTarif)
    {
        $this->companyTarif->removeElement($companyTarif);
    }

    /**
     * Set viewlic
     *
     * @param boolean $viewlic
     * @return Company
     */
    public function setViewlic($viewlic)
    {
        $this->viewlic = $viewlic;

        return $this;
    }

    /**
     * Get viewlic
     *
     * @return boolean
     */
    public function getViewlic()
    {
        return $this->viewlic;
    }
}
