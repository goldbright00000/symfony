<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 26/01/2016
 * Time: 02:00
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation;

/**
 * @ORM\Entity
 * @ORM\Table(name="cab_device")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\DeviceRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Device
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="devices")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="set null",unique=true))
     */
    private $user;

    /**
     * Android: UUID is an immutable representation of a 128-bit universally unique identifier (UUID).
     * IOS: UDID is short for Unique Device Identifier. It is a 40-character long hex value (20 bytes).
     * @Annotation\Expose
     * @ORM\Column(type="string",length=255, unique=false)
     */
    private $registerId;

    /**
     * @ORM\Column(type="string",length=255)
     * @Annotation\Expose
     */
    private $brand;

    /**
     * @ORM\Column(type="string",length=100)
     * @Annotation\Expose
     */
    private $model;

    /**
     * @ORM\Column(type="string",length=100)
     * @Annotation\Expose
     */
    private $os;

    /**
     * The International Mobile Station Equipment Identity or IMEI is a number, usually unique.
     * It is usually found printed inside the battery compartment of the phone, but can also be displayed on-screen
     * on most phones by entering *#06# on the dialpad
     * @ORM\Column(type="string",length=80)
     * @Annotation\Expose
     */
    private $imeiDevice;

    /**
     * @ORM\Column(type="string",length=20, name="apple_imei", nullable=true)
     * @Annotation\Expose
     */
    private $appleImei;

    /**
     *  IMSI is used to identify the user of a cellular network and is a unique identification associated with all
     *  cellular networks. It is stored as a 64 bit field and is sent by the phone to the network
     * @ORM\Column(type="string",length=64)
     * @Annotation\Expose
     */
    private $imsiDevice;

    /**
     * @ORM\Column(type="string",length=255, name="device_token", nullable=true)
     * @Annotation\Expose
     */
    private $deviceToken;

    /**
     * @var string $gateway
     *
     * @ORM\Column(name="gateway", type="boolean", nullable=true)
     * @Annotation\Expose
     */
    protected $gateway;

    /**
     *  @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     *  @ORM\Column(name="update_at", type="datetime")
     */
    private $updateAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @Annotation\VirtualProperty
     *
     */
    public function getUserId()
    {
        return $this->user->getId();
    }

    /**
     * Set regId
     *
     * @param string $regId
     * @return Device
     */
    public function setRegId($regId)
    {
        $this->regId = $regId;

        return $this;
    }

    /**
     * Get regId
     *
     * @return string
     */
    public function getRegId()
    {
        return $this->regId;
    }

    /**
     * Set brand
     *
     * @param string $brand
     * @return Device
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Device
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set os
     *
     * @param string $os
     * @return Device
     */
    public function setOs($os)
    {
        $this->os = $os;

        return $this;
    }

    /**
     * Get os
     *
     * @return string
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * Set imeiDevice
     *
     * @param string $imeiDevice
     * @return Device
     */
    public function setImeiDevice($imeiDevice)
    {
        $this->imeiDevice = $imeiDevice;

        return $this;
    }

    /**
     * Get imeiDevice
     *
     * @return string
     */
    public function getImeiDevice()
    {
        return $this->imeiDevice;
    }

    /**
     * Set appleImei
     *
     * @param string $appleImei
     * @return Device
     */
    public function setAppleImei($appleImei)
    {
        $this->appleImei = $appleImei;

        return $this;
    }

    /**
     * Get appleImei
     *
     * @return string
     */
    public function getAppleImei()
    {
        return $this->appleImei;
    }

    /**
     * Set imsiDevice
     *
     * @param string $imsiDevice
     * @return Device
     */
    public function setImsiDevice($imsiDevice)
    {
        $this->imsiDevice = $imsiDevice;

        return $this;
    }

    /**
     * Get imsiDevice
     *
     * @return string
     */
    public function getImsiDevice()
    {
        return $this->imsiDevice;
    }

    /**
     * Set createdAt
     *
     * @ORM\PrePersist
     * @param \DateTime $createdAt
     * @return Device
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime();
        $this->updateAt = new \DateTime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \CAB\UserBundle\Entity\User $user
     * @return Device
     */
    public function setUser(\CAB\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * Set registerId
     *
     * @param string $registerId
     * @return Device
     */
    public function setRegisterId($registerId)
    {
        $this->registerId = $registerId;

        return $this;
    }

    /**
     * Get registerId
     *
     * @return string
     */
    public function getRegisterId()
    {
        return $this->registerId;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     * @return Device
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * Set updateAt
     *
     * @ORM\PreUpdate
     * @param \DateTime $updateAt
     * @return Device
     */
    public function setUpdateAt()
    {
        $this->updateAt = new \DateTime();

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set gateway
     *
     * @param boolean $gateway
     * @return Device
     */
    public function setGateway($gateway)
    {
        $this->gateway = $gateway;

        return $this;
    }

    /**
     * Get gateway
     *
     * @return boolean
     */
    public function getGateway()
    {
        return $this->gateway;
    }
}
