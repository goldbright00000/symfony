<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\Zone
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_zone")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\ZoneRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Zone implements ZoneInterface {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $zoneName
     *
     * @ORM\Column(name="zone_name", type="string", length=255, nullable=false)
     */
    protected $zoneName;

    /**
     * @var string $address
     *
     * @ORM\Column(name="zone_address", type="string", length=255, nullable=false)
     */
    protected $zoneAddress;

    /**
     * @var decimal $longitude
     *
     * @ORM\Column(name="longitude", type="decimal", precision=14, scale=8, nullable=true)
     */
    protected $longitude;

    /**
     * @var decimal $latitude
     *
     * @ORM\Column(name="latitude", type="decimal", precision=14, scale=8, nullable=true)
     */
    protected $latitude;

    /**
     * @var integer $zoneRaduis
     *
     * @ORM\Column(name="zone_raduis", type="integer", nullable=false)
     */
    protected $zoneRaduis;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="companyZones")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $companyZone;

    /**
     * @ORM\ManyToMany(targetEntity="ServiceTransport", mappedBy="zones")
     */
    private $serviceTransports;

    /**
     * @ORM\ManyToMany(targetEntity="ParticularPeriod", mappedBy="zones")
     */
    protected $particularPeriods;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\UserBundle\Entity\User", mappedBy="zones")
     */
    protected $users;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Course", mappedBy="zone", fetch="EXTRA_LAZY")
     */
    private $course;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;



	/**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    /*
     * Get zone name
     *
     * @return srting
     */
    public function __toString(){
        return $this->getZoneName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zoneName
     *
     * @param string $zoneName
     * @return Zone
     */
    public function setZoneName($zoneName)
    {
        $this->zoneName = $zoneName;

        return $this;
    }

    /**
     * Get zoneName
     *
     * @return string
     */
    public function getZoneName()
    {
        return $this->zoneName;
    }

    /**
     * Set zoneAddress
     *
     * @param string $zoneAddress
     * @return Zone
     */
    public function setZoneAddress($zoneAddress)
    {
        $this->zoneAddress = $zoneAddress;

        return $this;
    }

    /**
     * Get zoneAddress
     *
     * @return string
     */
    public function getZoneAddress()
    {
        return $this->zoneAddress;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Zone
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Zone
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set zoneRaduis
     *
     * @param integer $zoneRaduis
     * @return Zone
     */
    public function setZoneRaduis($zoneRaduis)
    {
        $this->zoneRaduis = $zoneRaduis;

        return $this;
    }

    /**
     * Get zoneRaduis
     *
     * @return integer
     */
    public function getZoneRaduis()
    {
        return $this->zoneRaduis;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Zone
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Zone
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set companyZone
     *
     * @param \CAB\CourseBundle\Entity\Company $companyZone
     * @return Zone
     */
    public function setCompanyZone(\CAB\CourseBundle\Entity\Company $companyZone = null)
    {
        $this->companyZone = $companyZone;

        return $this;
    }

    /**
     * Get companyZone
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompanyZone()
    {
        return $this->companyZone;
    }


    /**
     * Add particularPeriods
     *
     * @param \CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods
     * @return Zone
     */
    public function addParticularPeriod(\CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods)
    {
        $this->particularPeriods[] = $particularPeriods;

        return $this;
    }

    /**
     * Remove particularPeriods
     *
     * @param \CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods
     */
    public function removeParticularPeriod(\CAB\CourseBundle\Entity\ParticularPeriod $particularPeriods)
    {
        $this->particularPeriods->removeElement($particularPeriods);
    }

    /**
     * Get particularPeriods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticularPeriods()
    {
        return $this->particularPeriods;
    }

    /**
     * Add serviceTransports
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransports
     * @return Zone
     */
    public function addServiceTransport(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransports)
    {
        $this->serviceTransports[] = $serviceTransports;

        return $this;
    }

    /**
     * Remove serviceTransports
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $serviceTransports
     */
    public function removeServiceTransport(\CAB\CourseBundle\Entity\ServiceTransport $serviceTransports)
    {
        $this->serviceTransports->removeElement($serviceTransports);
    }

    /**
     * Get serviceTransports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceTransports()
    {
        return $this->serviceTransports;
    }

    /**
     * Add users
     *
     * @param \CAB\UserBundle\Entity\User $users
     * @return Zone
     */
    public function addUser(\CAB\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \CAB\UserBundle\Entity\User $users
     */
    public function removeUser(\CAB\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return Zone
     */
    public function setCourse(\CAB\CourseBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }
}
