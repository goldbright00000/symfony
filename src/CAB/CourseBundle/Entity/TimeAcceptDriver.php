<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation;

/**
 * TimeAcceptDriver
 *
 * @ORM\Table()
 * @Annotation\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\TimeAcceptDriverRepository")
 */
class TimeAcceptDriver {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Annotation\Expose
     * @ORM\Column(name="secondaccept", type="string", length=255)
     */
    private $secondaccept;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set secondaccept
     *
     * @param string $secondaccept
     * @return TimeAcceptDriver
     */
    public function setSecondaccept($secondaccept) {
        $this->secondaccept = $secondaccept;

        return $this;
    }

    /**
     * Get secondaccept
     *
     * @return string 
     */
    public function getSecondaccept() {
        return $this->secondaccept;
    }

}
