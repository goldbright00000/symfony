<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 02/09/2015
 * Time: 22:34
 */

namespace CAB\CourseBundle\Entity;


interface ZoneInterface {

    public function getId();
}