<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 17/03/2016
 * Time: 23:49
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use CAB\UserBundle\Entity\User;

/**
 * CAB\CourseBundle\Entity\CourseHistory
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_course_history")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\CourseHistoryRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */

class CourseHistory
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Public"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Course", inversedBy="courseHistory", cascade={"persist"})
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $course;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="courseHistory", cascade={"persist"})
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="courseHistoryClient", cascade={"persist"})
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="courseHistoryAgent", cascade={"persist"})
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $agent;

    /**
     * @var string $latitude
     * @ORM\Column(name="latitude", type="decimal", precision=14, scale=8, nullable=true)
     */
    private $latitude;

    /**
     * @var string $longitude
     * @ORM\Column(name="longitude", type="decimal", precision=14, scale=8, nullable=true)
     */
    private $longitude;

    /**
     * @var string $departureAddress
     *
     * @ORM\Column(length=255, type="string", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $departureAddress;

    /**
     * @var string $arrivalAddress
     *
     * @ORM\Column(length=255, type="string", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $arrivalAddress;

    /**
     * @var decimal $priceTTC
     *
     * @ORM\Column(name="price_ttc", type="decimal", precision=8, scale=4, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $priceTTC;

    /**
     * var datetime $departureDate
     *
     * @ORM\Column(name="departure_date", type="date", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $departureDate;

    /**
     * var datetime $departureTime
     *
     * @ORM\Column(name="departure_time", type="time", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $departureTime;

    /**
     * @var string $targetType
     *
     * @ORM\Column(name="target_type", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $targetType;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $status;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $createdAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CourseHistory
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CourseHistory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return CourseHistory
     */
    public function setCourse(\CAB\CourseBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

     /**
     * Set latitude
     *
     * @param string $latitude
     * @return CourseHistory
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return CourseHistory
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set departureAddress
     *
     * @param integer $departureAddress
     * @return CourseHistory
     */
    public function setDepartureAddress($departureAddress)
    {
        $this->departureAddress = $departureAddress;

        return $this;
    }

    /**
     * Get departureAddress
     *
     * @return integer
     */
    public function getDepartureAddress()
    {
        return $this->departureAddress;
    }

    /**
     * Set arrivalAddress
     *
     * @param integer $arrivalAddress
     * @return CourseHistory
     */
    public function setArrivalAddress($arrivalAddress)
    {
        $this->arrivalAddress = $arrivalAddress;

        return $this;
    }

    /**
     * Get arrivalAddress
     *
     * @return integer
     */
    public function getArrivalAddress()
    {
        return $this->arrivalAddress;
    }

    /**
     * Set priceTTC
     *
     * @param string $priceTTC
     * @return CourseHistory
     */
    public function setPriceTTC($priceTTC)
    {
        $this->priceTTC = $priceTTC;

        return $this;
    }

    /**
     * Get priceTTC
     *
     * @return string
     */
    public function getPriceTTC()
    {
        return $this->priceTTC;
    }

    /**
     * Set departureDate
     *
     * @param \DateTime $departureDate
     * @return CourseHistory
     */
    public function setDepartureDate($departureDate)
    {
        $this->departureDate = $departureDate;

        return $this;
    }

    /**
     * Get departureDate
     *
     * @return \DateTime
     */
    public function getDepartureDate()
    {
        return $this->departureDate;
    }

    /**
     * Set departureTime
     *
     * @param \DateTime $departureTime
     * @return CourseHistory
     */
    public function setDepartureTime($departureTime)
    {
        $this->departureTime = $departureTime;

        return $this;
    }

    /**
     * Get departureTime
     *
     * @return \DateTime
     */
    public function getDepartureTime()
    {
        return $this->departureTime;
    }

    /**
     * Set targetType
     *
     * @param boolean $targetType
     * @return CourseHistory
     */
    public function setTargetType($targetType)
    {
        $this->targetType = $targetType;

        return $this;
    }

    /**
     * Get targetType
     *
     * @return boolean
     */
    public function getTargetType()
    {
        return $this->targetType;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return CourseHistory
     */
    public function setDriver(\CAB\UserBundle\Entity\User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set client
     *
     * @param \CAB\UserBundle\Entity\User $client
     * @return CourseHistory
     */
    public function setClient(\CAB\UserBundle\Entity\User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getClient()
    {
        return $this->client;
    }

    public function getCourseDescription()
    {
        return Course::courseDescription($this->status);
    }

    /**
     * Set agent
     *
     * @param \CAB\UserBundle\Entity\User $agent
     * @return CourseHistory
     */
    public function setAgent(\CAB\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getAgent()
    {
        return $this->agent;
    }
}
