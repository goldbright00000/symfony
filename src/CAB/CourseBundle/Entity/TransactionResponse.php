<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda
 * Date: 11/01/2016
 * Time: 17:0*
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\TransactionResponse
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_transaction_response")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\TransactionResponseRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class TransactionResponse
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\PaymentCard", mappedBy="transactionResponse")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $paymentCard;

    /**
     * @ORM\OneToOne(targetEntity="CAB\CourseBundle\Entity\TransactionPayment", inversedBy="transactionResponse")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $reponseTransactionPayment;

    /**
     * @var integer $vadsIdentifier
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsIdentifier;
    /**
     * @var integer $vadsAmount
     *
     * @ORM\Column(name="vads_amount", type="integer", nullable=true)
     */
    protected $vadsAmount;

    /**
     * @var integer $vadsCaptureDelay
     *
     * @ORM\Column(name="vads_capture_delay", type="integer", nullable=true)
     */
    protected $vadsCaptureDelay;

    /**
     * @var string $vadsPaymentCertificate
     *
     * @ORM\Column(name="vads_payment_certificate", type="string", length=255, nullable=true)
     */
    protected $vadsPaymentCertificate;

    /**
     * @var integer $vadsSiteId
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $vadsSiteId;

    /**
     * @var string $vadsTransDate
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $vadsTransDate;

    /**
     * @var integer $vadsTransId
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $vadsTransId;

    /**
     * @var integer $vadsVersion
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $vadsVersion;

    /**
     * @var integer $vadsActionMode
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $vadsActionMode;

    /**
     * @var integer $vadsPaymentConfig
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $vadsPaymentConfig;

    /**
     * @var integer $vadsPageAction
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $vadsPageAction;

    /**
     * @var string $signature
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $signature;

    /**
     * @var string $vadsCtxMode
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    protected $vadsCtxMode;

    /**
     * @var string $vadsCurrency
     *
     * @ORM\Column(type="integer", nullable=true)
     */
        protected $vadsCurrency;

    /**
     * @var string $vadsAuthNumber
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
        protected $vadsAuthNumber;

    /**
     * @var string $vadsAuthmode
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
        protected $vadsAuthmode;

    /**
     * @var string $vadsCardBrand
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsCardBrand;

    /**
     * @var string $vadsAuthResult
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
        protected $vadsAuthResult;
    /**
     * @var string $vadsCardNumber
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsCardNumber;

    /**
     * @var string $vadsEffectiveAmount
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsEffectiveAmount;

    /**
     * @var string $vadsValidationMode
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsValidationMode;

    /**
     * @var string $vadsTransUuid
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsTransUuid;

    /**
     * @var string $vadsWarrantyResult
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsWarrantyResult;

    /**
     * @var string $vadsPaymentSrc
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsPaymentSrc;

    /**
     * @var string $vadsSequenceNumber
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsSequenceNumber;

    /**
     * @var string $vadsContractUsed
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsContractUsed;

    /**
     * @var string $vadsTransStatus
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsTransStatus;

    /**
     * @var string $vadsExpiryMonth
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    protected $vadsExpiryMonth;

    /**
     * @var string $vadsExpiryYear
     *
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    protected $vadsExpiryYear;

    /**
     * @var string $vadsBankCode
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsBankCode;

    /**
     * @var string $vadsBankProduct
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsBankProduct;

    /**
     * @var string $vadsPaysIp
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $vadsPaysIp;

    /**
     * @var string $vadsPresentationDate
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $vadsPresentationDate;

    /**
     * @var string $vadsEffectiveCreationDate
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsEffectiveCreationDate;

    /**
     * @var string $vadsOperationType
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsOperationType;

    /**
     * @var string $vadsThreedsEnrolled
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsEnrolled;

    /**
     * @var string $vadsThreedsCavv
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsCavv;

    /**
     * @var string $vadsThreedsEci
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsEci;

    /**
     * @var string $vadsThreedsXid
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsXid;

    /**
     * @var string $vadsThreedsCavvAlgorithm
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsCavvAlgorithm;

    /**
     * @var string $vadsThreedsStatus
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsStatus;

    /**
     * @var string $vadsThreedsSignValid
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsSignValid;

    /**
     * @var string $vadsThreedsErrorCode
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsErrorCode;

    /**
     * @var string $vadsThreedsExitStatus
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsThreedsExitStatus;

    /**
     * @var string $vadsRiskControl
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsRiskControl;

    /**
     * @var string $vadsResult
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsResult;

    /**
     * @var string $vadsExtraResult
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsExtraResult;

    /**
     * @var string $vadsCardCountry
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsCardCountry;

    /**
     * @var string $vadsLanguage
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsLanguage;

    /**
     * @var string $vadsHash
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsHash;

    /**
     * @var string $vadsUrlCheckSrc
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $vadsUrlCheckSrc;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vadsAmount
     *
     * @param integer $vadsAmount
     * @return TransactionResponse
     */
    public function setVadsAmount($vadsAmount)
    {
        $this->vadsAmount = $vadsAmount;

        return $this;
    }

    /**
     * Get vadsAmount
     *
     * @return integer
     */
    public function getVadsAmount()
    {
        return $this->vadsAmount;
    }

    /**
     * Set vadsCaptureDelay
     *
     * @param integer $vadsCaptureDelay
     * @return TransactionResponse
     */
    public function setVadsCaptureDelay($vadsCaptureDelay)
    {
        $this->vadsCaptureDelay = $vadsCaptureDelay;

        return $this;
    }

    /**
     * Get vadsCaptureDelay
     *
     * @return integer
     */
    public function getVadsCaptureDelay()
    {
        return $this->vadsCaptureDelay;
    }

    /**
     * Set vadsPaymentCertificate
     *
     * @param string $vadsPaymentCertificate
     * @return TransactionResponse
     */
    public function setVadsPaymentCertificate($vadsPaymentCertificate)
    {
        $this->vadsPaymentCertificate = $vadsPaymentCertificate;

        return $this;
    }

    /**
     * Get vadsPaymentCertificate
     *
     * @return string
     */
    public function getVadsPaymentCertificate()
    {
        return $this->vadsPaymentCertificate;
    }

    /**
     * Set vadsSiteId
     *
     * @param integer $vadsSiteId
     * @return TransactionResponse
     */
    public function setVadsSiteId($vadsSiteId)
    {
        $this->vadsSiteId = $vadsSiteId;

        return $this;
    }

    /**
     * Get vadsSiteId
     *
     * @return integer
     */
    public function getVadsSiteId()
    {
        return $this->vadsSiteId;
    }

    /**
     * Set vadsTransDate
     *
     * @param string $vadsTransDate
     * @return TransactionResponse
     */
    public function setVadsTransDate($vadsTransDate)
    {
        $this->vadsTransDate = $vadsTransDate;

        return $this;
    }

    /**
     * Get vadsTransDate
     *
     * @return string
     */
    public function getVadsTransDate()
    {
        return $this->vadsTransDate;
    }

    /**
     * Set vadsTransId
     *
     * @param integer $vadsTransId
     * @return TransactionResponse
     */
    public function setVadsTransId($vadsTransId)
    {
        $this->vadsTransId = $vadsTransId;

        return $this;
    }

    /**
     * Get vadsTransId
     *
     * @return integer
     */
    public function getVadsTransId()
    {
        return $this->vadsTransId;
    }

    /**
     * Set vadsVersion
     *
     * @param string $vadsVersion
     * @return TransactionResponse
     */
    public function setVadsVersion($vadsVersion)
    {
        $this->vadsVersion = $vadsVersion;

        return $this;
    }

    /**
     * Get vadsVersion
     *
     * @return string
     */
    public function getVadsVersion()
    {
        return $this->vadsVersion;
    }

    /**
     * Set vadsActionMode
     *
     * @param string $vadsActionMode
     * @return TransactionResponse
     */
    public function setVadsActionMode($vadsActionMode)
    {
        $this->vadsActionMode = $vadsActionMode;

        return $this;
    }

    /**
     * Get vadsActionMode
     *
     * @return string
     */
    public function getVadsActionMode()
    {
        return $this->vadsActionMode;
    }

    /**
     * Set vadsPaymentConfig
     *
     * @param string $vadsPaymentConfig
     * @return TransactionResponse
     */
    public function setVadsPaymentConfig($vadsPaymentConfig)
    {
        $this->vadsPaymentConfig = $vadsPaymentConfig;

        return $this;
    }

    /**
     * Get vadsPaymentConfig
     *
     * @return string
     */
    public function getVadsPaymentConfig()
    {
        return $this->vadsPaymentConfig;
    }

    /**
     * Set vadsPageAction
     *
     * @param string $vadsPageAction
     * @return TransactionResponse
     */
    public function setVadsPageAction($vadsPageAction)
    {
        $this->vadsPageAction = $vadsPageAction;

        return $this;
    }

    /**
     * Get vadsPageAction
     *
     * @return string
     */
    public function getVadsPageAction()
    {
        return $this->vadsPageAction;
    }

    /**
     * Set signature
     *
     * @param string $signature
     * @return TransactionResponse
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set vadsCtxMode
     *
     * @param string $vadsCtxMode
     * @return TransactionResponse
     */
    public function setVadsCtxMode($vadsCtxMode)
    {
        $this->vadsCtxMode = $vadsCtxMode;

        return $this;
    }

    /**
     * Get vadsCtxMode
     *
     * @return string
     */
    public function getVadsCtxMode()
    {
        return $this->vadsCtxMode;
    }

    /**
     * Set vadsCurrency
     *
     * @param integer $vadsCurrency
     * @return TransactionResponse
     */
    public function setVadsCurrency($vadsCurrency)
    {
        $this->vadsCurrency = $vadsCurrency;

        return $this;
    }

    /**
     * Get vadsCurrency
     *
     * @return integer
     */
    public function getVadsCurrency()
    {
        return $this->vadsCurrency;
    }

    /**
     * Set vadsAuthNumber
     *
     * @param string $vadsAuthNumber
     * @return TransactionResponse
     */
    public function setVadsAuthNumber($vadsAuthNumber)
    {
        $this->vadsAuthNumber = $vadsAuthNumber;

        return $this;
    }

    /**
     * Get vadsAuthNumber
     *
     * @return string
     */
    public function getVadsAuthNumber()
    {
        return $this->vadsAuthNumber;
    }

    /**
     * Set vadsCardBrand
     *
     * @param string $vadsCardBrand
     * @return TransactionResponse
     */
    public function setVadsCardBrand($vadsCardBrand)
    {
        $this->vadsCardBrand = $vadsCardBrand;

        return $this;
    }

    /**
     * Get vadsCardBrand
     *
     * @return string
     */
    public function getVadsCardBrand()
    {
        return $this->vadsCardBrand;
    }

    /**
     * Set vadsAuthResult
     *
     * @param string $vadsAuthResult
     * @return TransactionResponse
     */
    public function setVadsAuthResult($vadsAuthResult)
    {
        $this->vadsAuthResult = $vadsAuthResult;

        return $this;
    }

    /**
     * Get vadsAuthResult
     *
     * @return string
     */
    public function getVadsAuthResult()
    {
        return $this->vadsAuthResult;
    }

    /**
     * Set vadsCardNumber
     *
     * @param string $vadsCardNumber
     * @return TransactionResponse
     */
    public function setVadsCardNumber($vadsCardNumber)
    {
        $this->vadsCardNumber = $vadsCardNumber;

        return $this;
    }

    /**
     * Get vadsCardNumber
     *
     * @return string
     */
    public function getVadsCardNumber()
    {
        return $this->vadsCardNumber;
    }

    /**
     * Set vadsEffectiveAmount
     *
     * @param string $vadsEffectiveAmount
     * @return TransactionResponse
     */
    public function setVadsEffectiveAmount($vadsEffectiveAmount)
    {
        $this->vadsEffectiveAmount = $vadsEffectiveAmount;

        return $this;
    }

    /**
     * Get vadsEffectiveAmount
     *
     * @return string
     */
    public function getVadsEffectiveAmount()
    {
        return $this->vadsEffectiveAmount;
    }

    /**
     * Set vadsValidationMode
     *
     * @param string $vadsValidationMode
     * @return TransactionResponse
     */
    public function setVadsValidationMode($vadsValidationMode)
    {
        $this->vadsValidationMode = $vadsValidationMode;

        return $this;
    }

    /**
     * Get vadsValidationMode
     *
     * @return string
     */
    public function getVadsValidationMode()
    {
        return $this->vadsValidationMode;
    }

    /**
     * Set vadsWarrantyResult
     *
     * @param string $vadsWarrantyResult
     * @return TransactionResponse
     */
    public function setVadsWarrantyResult($vadsWarrantyResult)
    {
        $this->vadsWarrantyResult = $vadsWarrantyResult;

        return $this;
    }

    /**
     * Get vadsWarrantyResult
     *
     * @return string
     */
    public function getVadsWarrantyResult()
    {
        return $this->vadsWarrantyResult;
    }

    /**
     * Set vadsPaymentSrc
     *
     * @param string $vadsPaymentSrc
     * @return TransactionResponse
     */
    public function setVadsPaymentSrc($vadsPaymentSrc)
    {
        $this->vadsPaymentSrc = $vadsPaymentSrc;

        return $this;
    }

    /**
     * Get vadsPaymentSrc
     *
     * @return string
     */
    public function getVadsPaymentSrc()
    {
        return $this->vadsPaymentSrc;
    }

    /**
     * Set vadsSequenceNumber
     *
     * @param string $vadsSequenceNumber
     * @return TransactionResponse
     */
    public function setVadsSequenceNumber($vadsSequenceNumber)
    {
        $this->vadsSequenceNumber = $vadsSequenceNumber;

        return $this;
    }

    /**
     * Get vadsSequenceNumber
     *
     * @return string
     */
    public function getVadsSequenceNumber()
    {
        return $this->vadsSequenceNumber;
    }

    /**
     * Set vadsContractUsed
     *
     * @param string $vadsContractUsed
     * @return TransactionResponse
     */
    public function setVadsContractUsed($vadsContractUsed)
    {
        $this->vadsContractUsed = $vadsContractUsed;

        return $this;
    }

    /**
     * Get vadsContractUsed
     *
     * @return string
     */
    public function getVadsContractUsed()
    {
        return $this->vadsContractUsed;
    }

    /**
     * Set vadsTransStatus
     *
     * @param string $vadsTransStatus
     * @return TransactionResponse
     */
    public function setVadsTransStatus($vadsTransStatus)
    {
        $this->vadsTransStatus = $vadsTransStatus;

        return $this;
    }

    /**
     * Get vadsTransStatus
     *
     * @return string
     */
    public function getVadsTransStatus()
    {
        return $this->vadsTransStatus;
    }

    /**
     * Set vadsExpiryMonth
     *
     * @param string $vadsExpiryMonth
     * @return TransactionResponse
     */
    public function setVadsExpiryMonth($vadsExpiryMonth)
    {
        $this->vadsExpiryMonth = $vadsExpiryMonth;

        return $this;
    }

    /**
     * Get vadsExpiryMonth
     *
     * @return string
     */
    public function getVadsExpiryMonth()
    {
        return $this->vadsExpiryMonth;
    }

    /**
     * Set vadsExpiryYear
     *
     * @param string $vadsExpiryYear
     * @return TransactionResponse
     */
    public function setVadsExpiryYear($vadsExpiryYear)
    {
        $this->vadsExpiryYear = $vadsExpiryYear;

        return $this;
    }

    /**
     * Get vadsExpiryYear
     *
     * @return string
     */
    public function getVadsExpiryYear()
    {
        return $this->vadsExpiryYear;
    }

    /**
     * Set vadsBankCode
     *
     * @param string $vadsBankCode
     * @return TransactionResponse
     */
    public function setVadsBankCode($vadsBankCode)
    {
        $this->vadsBankCode = $vadsBankCode;

        return $this;
    }

    /**
     * Get vadsBankCode
     *
     * @return string
     */
    public function getVadsBankCode()
    {
        return $this->vadsBankCode;
    }

    /**
     * Set vadsBankProduct
     *
     * @param string $vadsBankProduct
     * @return TransactionResponse
     */
    public function setVadsBankProduct($vadsBankProduct)
    {
        $this->vadsBankProduct = $vadsBankProduct;

        return $this;
    }

    /**
     * Get vadsBankProduct
     *
     * @return string
     */
    public function getVadsBankProduct()
    {
        return $this->vadsBankProduct;
    }

    /**
     * Set vadsPaysIp
     *
     * @param string $vadsPaysIp
     * @return TransactionResponse
     */
    public function setVadsPaysIp($vadsPaysIp)
    {
        $this->vadsPaysIp = $vadsPaysIp;

        return $this;
    }

    /**
     * Get vadsPaysIp
     *
     * @return string
     */
    public function getVadsPaysIp()
    {
        return $this->vadsPaysIp;
    }

    /**
     * Set vadsPresentationDate
     *
     * @param string $vadsPresentationDate
     * @return TransactionResponse
     */
    public function setVadsPresentationDate($vadsPresentationDate)
    {
        $this->vadsPresentationDate = $vadsPresentationDate;

        return $this;
    }

    /**
     * Get vadsPresentationDate
     *
     * @return string
     */
    public function getVadsPresentationDate()
    {
        return $this->vadsPresentationDate;
    }

    /**
     * Set vadsEffectiveCreationDate
     *
     * @param string $vadsEffectiveCreationDate
     * @return TransactionResponse
     */
    public function setVadsEffectiveCreationDate($vadsEffectiveCreationDate)
    {
        $this->vadsEffectiveCreationDate = $vadsEffectiveCreationDate;

        return $this;
    }

    /**
     * Get vadsEffectiveCreationDate
     *
     * @return string
     */
    public function getVadsEffectiveCreationDate()
    {
        return $this->vadsEffectiveCreationDate;
    }

    /**
     * Set vadsOperationType
     *
     * @param string $vadsOperationType
     * @return TransactionResponse
     */
    public function setVadsOperationType($vadsOperationType)
    {
        $this->vadsOperationType = $vadsOperationType;

        return $this;
    }

    /**
     * Get vadsOperationType
     *
     * @return string
     */
    public function getVadsOperationType()
    {
        return $this->vadsOperationType;
    }

    /**
     * Set vadsThreedsEnrolled
     *
     * @param string $vadsThreedsEnrolled
     * @return TransactionResponse
     */
    public function setVadsThreedsEnrolled($vadsThreedsEnrolled)
    {
        $this->vadsThreedsEnrolled = $vadsThreedsEnrolled;

        return $this;
    }

    /**
     * Get vadsThreedsEnrolled
     *
     * @return string
     */
    public function getVadsThreedsEnrolled()
    {
        return $this->vadsThreedsEnrolled;
    }

    /**
     * Set vadsThreedsCavv
     *
     * @param string $vadsThreedsCavv
     * @return TransactionResponse
     */
    public function setVadsThreedsCavv($vadsThreedsCavv)
    {
        $this->vadsThreedsCavv = $vadsThreedsCavv;

        return $this;
    }

    /**
     * Get vadsThreedsCavv
     *
     * @return string
     */
    public function getVadsThreedsCavv()
    {
        return $this->vadsThreedsCavv;
    }

    /**
     * Set vadsThreedsEci
     *
     * @param string $vadsThreedsEci
     * @return TransactionResponse
     */
    public function setVadsThreedsEci($vadsThreedsEci)
    {
        $this->vadsThreedsEci = $vadsThreedsEci;

        return $this;
    }

    /**
     * Get vadsThreedsEci
     *
     * @return string
     */
    public function getVadsThreedsEci()
    {
        return $this->vadsThreedsEci;
    }

    /**
     * Set vadsThreedsXid
     *
     * @param string $vadsThreedsXid
     * @return TransactionResponse
     */
    public function setVadsThreedsXid($vadsThreedsXid)
    {
        $this->vadsThreedsXid = $vadsThreedsXid;

        return $this;
    }

    /**
     * Get vadsThreedsXid
     *
     * @return string
     */
    public function getVadsThreedsXid()
    {
        return $this->vadsThreedsXid;
    }

    /**
     * Set vadsThreedsCavvAlgorithm
     *
     * @param string $vadsThreedsCavvAlgorithm
     * @return TransactionResponse
     */
    public function setVadsThreedsCavvAlgorithm($vadsThreedsCavvAlgorithm)
    {
        $this->vadsThreedsCavvAlgorithm = $vadsThreedsCavvAlgorithm;

        return $this;
    }

    /**
     * Get vadsThreedsCavvAlgorithm
     *
     * @return string
     */
    public function getVadsThreedsCavvAlgorithm()
    {
        return $this->vadsThreedsCavvAlgorithm;
    }

    /**
     * Set vadsThreedsStatus
     *
     * @param string $vadsThreedsStatus
     * @return TransactionResponse
     */
    public function setVadsThreedsStatus($vadsThreedsStatus)
    {
        $this->vadsThreedsStatus = $vadsThreedsStatus;

        return $this;
    }

    /**
     * Get vadsThreedsStatus
     *
     * @return string
     */
    public function getVadsThreedsStatus()
    {
        return $this->vadsThreedsStatus;
    }

    /**
     * Set vadsThreedsSignValid
     *
     * @param string $vadsThreedsSignValid
     * @return TransactionResponse
     */
    public function setVadsThreedsSignValid($vadsThreedsSignValid)
    {
        $this->vadsThreedsSignValid = $vadsThreedsSignValid;

        return $this;
    }

    /**
     * Get vadsThreedsSignValid
     *
     * @return string
     */
    public function getVadsThreedsSignValid()
    {
        return $this->vadsThreedsSignValid;
    }

    /**
     * Set vadsThreedsErrorCode
     *
     * @param string $vadsThreedsErrorCode
     * @return TransactionResponse
     */
    public function setVadsThreedsErrorCode($vadsThreedsErrorCode)
    {
        $this->vadsThreedsErrorCode = $vadsThreedsErrorCode;

        return $this;
    }

    /**
     * Get vadsThreedsErrorCode
     *
     * @return string
     */
    public function getVadsThreedsErrorCode()
    {
        return $this->vadsThreedsErrorCode;
    }

    /**
     * Set vadsThreedsExitStatus
     *
     * @param string $vadsThreedsExitStatus
     * @return TransactionResponse
     */
    public function setVadsThreedsExitStatus($vadsThreedsExitStatus)
    {
        $this->vadsThreedsExitStatus = $vadsThreedsExitStatus;

        return $this;
    }

    /**
     * Get vadsThreedsExitStatus
     *
     * @return string
     */
    public function getVadsThreedsExitStatus()
    {
        return $this->vadsThreedsExitStatus;
    }

    /**
     * Set vadsRiskControl
     *
     * @param string $vadsRiskControl
     * @return TransactionResponse
     */
    public function setVadsRiskControl($vadsRiskControl)
    {
        $this->vadsRiskControl = $vadsRiskControl;

        return $this;
    }

    /**
     * Get vadsRiskControl
     *
     * @return string
     */
    public function getVadsRiskControl()
    {
        return $this->vadsRiskControl;
    }

    /**
     * Set vadsResult
     *
     * @param string $vadsResult
     * @return TransactionResponse
     */
    public function setVadsResult($vadsResult)
    {
        $this->vadsResult = $vadsResult;

        return $this;
    }

    /**
     * Get vadsResult
     *
     * @return string
     */
    public function getVadsResult()
    {
        return $this->vadsResult;
    }

    /**
     * Set vadsExtraResult
     *
     * @param string $vadsExtraResult
     * @return TransactionResponse
     */
    public function setVadsExtraResult($vadsExtraResult)
    {
        $this->vadsExtraResult = $vadsExtraResult;

        return $this;
    }

    /**
     * Get vadsExtraResult
     *
     * @return string
     */
    public function getVadsExtraResult()
    {
        return $this->vadsExtraResult;
    }

    /**
     * Set vadsCardCountry
     *
     * @param string $vadsCardCountry
     * @return TransactionResponse
     */
    public function setVadsCardCountry($vadsCardCountry)
    {
        $this->vadsCardCountry = $vadsCardCountry;

        return $this;
    }

    /**
     * Get vadsCardCountry
     *
     * @return string
     */
    public function getVadsCardCountry()
    {
        return $this->vadsCardCountry;
    }

    /**
     * Set vadsLanguage
     *
     * @param string $vadsLanguage
     * @return TransactionResponse
     */
    public function setVadsLanguage($vadsLanguage)
    {
        $this->vadsLanguage = $vadsLanguage;

        return $this;
    }

    /**
     * Get vadsLanguage
     *
     * @return string
     */
    public function getVadsLanguage()
    {
        return $this->vadsLanguage;
    }

    /**
     * Set vadsHash
     *
     * @param string $vadsHash
     * @return TransactionResponse
     */
    public function setVadsHash($vadsHash)
    {
        $this->vadsHash = $vadsHash;

        return $this;
    }

    /**
     * Get vadsHash
     *
     * @return string
     */
    public function getVadsHash()
    {
        return $this->vadsHash;
    }

    /**
     * Set vadsUrlCheckSrc
     *
     * @param string $vadsUrlCheckSrc
     * @return TransactionResponse
     */
    public function setVadsUrlCheckSrc($vadsUrlCheckSrc)
    {
        $this->vadsUrlCheckSrc = $vadsUrlCheckSrc;

        return $this;
    }

    /**
     * Get vadsUrlCheckSrc
     *
     * @return string
     */
    public function getVadsUrlCheckSrc()
    {
        return $this->vadsUrlCheckSrc;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return TransactionResponse
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * Set reponseTransactionPayment
     *
     * @param \CAB\CourseBundle\Entity\TransactionPayment $reponseTransactionPayment
     * @return TransactionResponse
     */
    public function setReponseTransactionPayment(\CAB\CourseBundle\Entity\TransactionPayment $reponseTransactionPayment = null)
    {
        $this->reponseTransactionPayment = $reponseTransactionPayment;

        return $this;
    }

    /**
     * Get reponseTransactionPayment
     *
     * @return \CAB\CourseBundle\Entity\TransactionPayment
     */
    public function getReponseTransactionPayment()
    {
        return $this->reponseTransactionPayment;
    }

    /**
     * Set vadsTransUuid
     *
     * @param string $vadsTransUuid
     * @return TransactionResponse
     */
    public function setVadsTransUuid($vadsTransUuid)
    {
        $this->vadsTransUuid = $vadsTransUuid;

        return $this;
    }

    /**
     * Get vadsTransUuid
     *
     * @return string
     */
    public function getVadsTransUuid()
    {
        return $this->vadsTransUuid;
    }

    /**
     * Set vadsAuthmode
     *
     * @param string $vadsAuthmode
     * @return TransactionResponse
     */
    public function setVadsAuthmode($vadsAuthmode)
    {
        $this->vadsAuthmode = $vadsAuthmode;

        return $this;
    }

    /**
     * Get vadsAuthmode
     *
     * @return string
     */
    public function getVadsAuthmode()
    {
        return $this->vadsAuthmode;
    }

    /**
     * Set vadsIdentifier
     *
     * @param string $vadsIdentifier
     * @return TransactionResponse
     */
    public function setVadsIdentifier($vadsIdentifier)
    {
        $this->vadsIdentifier = $vadsIdentifier;

        return $this;
    }

    /**
     * Get vadsIdentifier
     *
     * @return string
     */
    public function getVadsIdentifier()
    {
        return $this->vadsIdentifier;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paymentCard = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add paymentCard
     *
     * @param \CAB\CourseBundle\Entity\PaymentCard $paymentCard
     * @return TransactionResponse
     */
    public function addPaymentCard(\CAB\CourseBundle\Entity\PaymentCard $paymentCard)
    {
        $this->paymentCard[] = $paymentCard;

        return $this;
    }

    /**
     * Remove paymentCard
     *
     * @param \CAB\CourseBundle\Entity\PaymentCard $paymentCard
     */
    public function removePaymentCard(\CAB\CourseBundle\Entity\PaymentCard $paymentCard)
    {
        $this->paymentCard->removeElement($paymentCard);
    }

    /**
     * Get paymentCard
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentCard()
    {
        return $this->paymentCard;
    }
}
