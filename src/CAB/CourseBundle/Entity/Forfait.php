<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 26/12/2016
 * Time: 16:59
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * CAB\CourseBundle\Entity\Forfait
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_forfait")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\ForfaitRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Forfait
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Public"})
     *
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="forfait", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\TypeVehicule", inversedBy="forfait", cascade={"persist"})
     * @ORM\JoinColumn(name="vehicle_type_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicleType;

    /**
     * @var string $depAddress
     *
     * @ORM\Column(name="dep_address", type="string", length=255, nullable=false)
     * @Expose
     * @Groups({"Public"})
     */
    private $depAddress;

    /**
     * @var string $arrAddress
     *
     * @ORM\Column(name="arr_address", type="string", length=255, nullable=false)
     * @Expose
     * @Groups({"Public"})
     */
    private $arrAddress;

    /**
     * @var integer $radius
     *
     * @ORM\Column(name="radius", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $radius;

    /**
     * @var float $longDep
     *
     * @ORM\Column(name="long_dep", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $longDep;

    /**
     * @var float $latDep
     *
     * @ORM\Column(name="lat_dep", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $latDep;

    /**
     * @var float $longArr
     *
     * @ORM\Column(name="long_arr", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $longArr;

    /**
     * @var float $latArr
     *
     * @ORM\Column(name="lat_arr", type="decimal", precision=14, scale=8, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    private $latArr;

    /**
     * @var integer $tax
     *
     * @ORM\Column(name="tax", type="integer", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $tax;

    /**
     * @var float $tarifHT
     *
     * @ORM\Column(name="tarif_ht", type="decimal", precision=8, scale=2,  nullable=false)
     * @Expose
     * @Groups({"Public"})
     */
    protected $tarifHT;

    /**
     * @var float $tarifTTC
     *
     * @ORM\Column(name="tarif_TTC", type="decimal", precision=8, scale=2, nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $tarifTTC;

    /**
     * @var \Datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     * @Expose
     * @Groups({"Public"})
     */
    private $createdAt;

    /**
     * @var \Datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set longDep
     *
     * @param string $longDep
     * @return Forfait
     */
    public function setLongDep($longDep)
    {
        $this->longDep = $longDep;

        return $this;
    }

    /**
     * Get longDep
     *
     * @return string
     */
    public function getLongDep()
    {
        return $this->longDep;
    }

    /**
     * Set latDep
     *
     * @param string $latDep
     * @return Forfait
     */
    public function setLatDep($latDep)
    {
        $this->latDep = $latDep;

        return $this;
    }

    /**
     * Get latDep
     *
     * @return string
     */
    public function getLatDep()
    {
        return $this->latDep;
    }

    /**
     * Set longArr
     *
     * @param string $longArr
     * @return Forfait
     */
    public function setLongArr($longArr)
    {
        $this->longArr = $longArr;

        return $this;
    }

    /**
     * Get longArr
     *
     * @return string
     */
    public function getLongArr()
    {
        return $this->longArr;
    }

    /**
     * Set latArr
     *
     * @param string $latArr
     * @return Forfait
     */
    public function setLatArr($latArr)
    {
        $this->latArr = $latArr;

        return $this;
    }

    /**
     * Get latArr
     *
     * @return string
     */
    public function getLatArr()
    {
        return $this->latArr;
    }

    /**
     * Set tax
     *
     * @param integer $tax
     * @return Forfait
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return integer
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set tarifHT
     *
     * @param string $tarifHT
     * @return Forfait
     */
    public function setTarifHT($tarifHT)
    {
        $this->tarifHT = $tarifHT;

        return $this;
    }

    /**
     * Get tarifHT
     *
     * @return string
     */
    public function getTarifHT()
    {
        return $this->tarifHT;
    }

    /**
     * Set tarifTTC
     *
     * @param string $tarifTTC
     * @return Forfait
     */
    public function setTarifTTC($tarifTTC)
    {
        $this->tarifTTC = $tarifTTC;

        return $this;
    }

    /**
     * Get tarifTTC
     *
     * @return string
     */
    public function getTarifTTC()
    {
        return $this->tarifTTC;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Forfait
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Forfait
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return Forfait
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set depAddress
     *
     * @param string $depAddress
     * @return Forfait
     */
    public function setDepAddress($depAddress)
    {
        $this->depAddress = $depAddress;

        return $this;
    }

    /**
     * Get depAddress
     *
     * @return string
     */
    public function getDepAddress()
    {
        return $this->depAddress;
    }

    /**
     * Set arrAddress
     *
     * @param string $arrAddress
     * @return Forfait
     */
    public function setArrAddress($arrAddress)
    {
        $this->arrAddress = $arrAddress;

        return $this;
    }

    /**
     * Get arrAddress
     *
     * @return string
     */
    public function getArrAddress()
    {
        return $this->arrAddress;
    }

    /**
     * Set radius
     *
     * @param integer $radius
     * @return Forfait
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;

        return $this;
    }

    /**
     * Get radius
     *
     * @return integer
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * Set vehicleType
     *
     * @param \CAB\CourseBundle\Entity\TypeVehicule $vehicleType
     * @return Forfait
     */
    public function setVehicleType(\CAB\CourseBundle\Entity\TypeVehicule $vehicleType = null)
    {
        $this->vehicleType = $vehicleType;

        return $this;
    }

    /**
     * Get vehicleType
     *
     * @return \CAB\CourseBundle\Entity\TypeVehicule
     */
    public function getVehicleType()
    {
        return $this->vehicleType;
    }
}
