<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CAB\UserBundle\Entity\User;
use CAB\CourseBundle\Entity\Vehicule;
use JMS\Serializer\Annotation;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;




/**
 * VehiculeMaintenance
 *
 * @ORM\Table(name="cab_vehicule_maintenance")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\VehiculeMaintenanceRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Annotation\ExclusionPolicy("all")
 * @Vich\Uploadable
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class VehiculeMaintenance
{

    const SERVER_PATH_TO_IMAGE_FOLDER = '/uploads/vehicule/documents';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="useraffects")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Vehicule")
     * @ORM\JoinColumn(name="vehicule_id", referencedColumnName="id")
     */
    private $vehicule;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="vehicleFuel")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;
    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="vehicleMaintenance")
     * @ORM\JoinColumn(name="company_vm_id", referencedColumnName="id")
     */
    private $companyVehicleMaintenance;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="string", length=255)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="km", type="string", length=255)
     */
    private $km;

    /**
     * @var string
     *
     * @ORM\Column(name="oilchange", type="boolean", length=255)
     */
    private $oilchange;

    /**
     * @var string
     *
     * @ORM\Column(name="oilfilter", type="boolean", length=255)
     */
    private $oilfilter;

    /**
     * @var string
     *
     * @ORM\Column(name="oilajust", type="boolean", length=255, nullable=true)
     */
    private $oilajust;

    /**
     * @var string
     *
     * @ORM\Column(name="liquidwindowsajust", type="boolean", length=255, nullable=true)
     */
    private $liquidwindiwsajust;

    /**
     * @var string
     *
     * @ORM\Column(name="fronttyres", type="boolean", length=255)
     */
    private $fronttyres;

    /**
     * @var string
     *
     * @ORM\Column(name="reartyres", type="boolean", length=255)
     */
    private $reartyres;

    /**
     * @var string
     *
     * @ORM\Column(name="battery", type="boolean", length=255)
     */
    private $battery;

    /**
     * @var string
     *
     * @ORM\Column(name="chassis", type="boolean", length=255)
     */
    private $chassis;

    /**
     * @var string
     *
     * @ORM\Column(name="brokenwindows", type="boolean", length=255)
     */
    private $brokenwindows;

    /**
     * @var string
     *
     * @ORM\Column(name="bulbs", type="boolean", length=255)
     */
    private $bulbs;

    /**
     * @var string
     *
     * @ORM\Column(name="frontbrakepads", type="boolean", length=255)
     */
    private $frontbrakepads;

    /**
     * @var string
     *
     * @ORM\Column(name="rearbrakepads", type="boolean", length=255)
     */
    private $rearbrakepads;

    /**
     * @var string
     *
     * @ORM\Column(name="frontbrakediscs", type="boolean", length=255)
     */
    private $frontbrakediscs;

    /**
     * @var string
     *
     * @ORM\Column(name="rearbrakediscs", type="boolean", length=255)
     */
    private $rearbrakediscs;

    /**
     * @var string
     *
     * @ORM\Column(name="tow", type="boolean", length=255)
     */
    private $tow;

    /**
     * @var string
     *
     * @ORM\Column(name="detail", type="text", nullable=true)
     */
    private $detail;

    /**
     * @var string $photoFront
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $photoInvoice = null;

    /**
     *
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @var File
     *
     */
    private $fileInvoice;

    /**
     * @var datetime $startAt
     *
     * @ORM\Column(type="datetime", name="start_at", nullable=true)
     */
    private $startAt;

    /**
     * @var datetime $endAt
     *
     * @ORM\Column(type="datetime", name="end_at", nullable=true)
     */
    private $endAt;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;


  /**
   * @Vich\UploadableField(mapping="vehicule_maintenance", fileNameProperty="fileName")
   *
   * @var File
   */
  private $file;

  /**
   * @var string $imageName
   * @ORM\Column(type="string",length=250, nullable=true)
   */
  private $fileName = null;


    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        if ($this->getVehicule() !== null) {
            $this->setCompany($this->getVehicule()->getCompany());
            $this->setDriver($this->getVehicule()->getDriver());
        }
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {

        if (null !== $this->fileInvoice) {
            $this->photoInvoice = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileInvoice->guessExtension();
        }
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and target filename as params
        $this->getFile()->move(
            self::SERVER_PATH_TO_IMAGE_FOLDER,
            $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->filename = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->fileInvoice != "") {
            if ($file = $this->getAbsolutePath('photoInvoice'))
                unlink($file);
        }
    }

    /**
     * Sets fileInvoice.
     *
     * @param UploadedFile $file
     */
    public function setFileFront($file = null)
    {
        $this->fileInvoice = $file;
        if (null !== $this->fileInvoice) {
            $this->photoInvoice = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileInvoice->guessExtension();
        }
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return VehiculeMaintenance
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set km
     *
     * @param string $km
     * @return VehiculeMaintenance
     */
    public function setKm($km)
    {
        $this->km = $km;

        return $this;
    }

    /**
     * Get km
     *
     * @return string
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Set oilchange
     *
     * @param boolean $oilchange
     * @return VehiculeMaintenance
     */
    public function setOilchange($oilchange)
    {
        $this->oilchange = $oilchange;

        return $this;
    }

    /**
     * Get oilchange
     *
     * @return boolean
     */
    public function getOilchange()
    {
        return $this->oilchange;
    }

    /**
     * Set oilfilter
     *
     * @param boolean $oilfilter
     * @return VehiculeMaintenance
     */
    public function setOilfilter($oilfilter)
    {
        $this->oilfilter = $oilfilter;

        return $this;
    }

    /**
     * Get oilfilter
     *
     * @return boolean
     */
    public function getOilfilter()
    {
        return $this->oilfilter;
    }

    /**
     * Set oilajust
     *
     * @param boolean $oilajust
     * @return VehiculeMaintenance
     */
    public function setOilajust($oilajust)
    {
        $this->oilajust = $oilajust;

        return $this;
    }

    /**
     * Get oilajust
     *
     * @return boolean
     */
    public function getOilajust()
    {
        return $this->oilajust;
    }

    /**
     * Set fronttyres
     *
     * @param boolean $fronttyres
     * @return VehiculeMaintenance
     */
    public function setFronttyres($fronttyres)
    {
        $this->fronttyres = $fronttyres;

        return $this;
    }

    /**
     * Get fronttyres
     *
     * @return boolean
     */
    public function getFronttyres()
    {
        return $this->fronttyres;
    }

    /**
     * Set reartyres
     *
     * @param boolean $reartyres
     * @return VehiculeMaintenance
     */
    public function setReartyres($reartyres)
    {
        $this->reartyres = $reartyres;

        return $this;
    }

    /**
     * Get reartyres
     *
     * @return boolean
     */
    public function getReartyres()
    {
        return $this->reartyres;
    }

    /**
     * Set battery
     *
     * @param boolean $battery
     * @return VehiculeMaintenance
     */
    public function setBattery($battery)
    {
        $this->battery = $battery;

        return $this;
    }

    /**
     * Get battery
     *
     * @return boolean
     */
    public function getBattery()
    {
        return $this->battery;
    }

    /**
     * Set chassis
     *
     * @param boolean $chassis
     * @return VehiculeMaintenance
     */
    public function setChassis($chassis)
    {
        $this->chassis = $chassis;

        return $this;
    }

    /**
     * Get chassis
     *
     * @return boolean
     */
    public function getChassis()
    {
        return $this->chassis;
    }

    /**
     * Set brokenwindows
     *
     * @param boolean $brokenwindows
     * @return VehiculeMaintenance
     */
    public function setBrokenwindows($brokenwindows)
    {
        $this->brokenwindows = $brokenwindows;

        return $this;
    }

    /**
     * Get brokenwindows
     *
     * @return boolean
     */
    public function getBrokenwindows()
    {
        return $this->brokenwindows;
    }

    /**
     * Set bulbs
     *
     * @param boolean $bulbs
     * @return VehiculeMaintenance
     */
    public function setBulbs($bulbs)
    {
        $this->bulbs = $bulbs;

        return $this;
    }

    /**
     * Get bulbs
     *
     * @return boolean
     */
    public function getBulbs()
    {
        return $this->bulbs;
    }

    /**
     * Set frontbrakepads
     *
     * @param boolean $frontbrakepads
     * @return VehiculeMaintenance
     */
    public function setFrontbrakepads($frontbrakepads)
    {
        $this->frontbrakepads = $frontbrakepads;

        return $this;
    }

    /**
     * Get frontbrakepads
     *
     * @return boolean
     */
    public function getFrontbrakepads()
    {
        return $this->frontbrakepads;
    }

    /**
     * Set rearbrakepads
     *
     * @param boolean $rearbrakepads
     * @return VehiculeMaintenance
     */
    public function setRearbrakepads($rearbrakepads)
    {
        $this->rearbrakepads = $rearbrakepads;

        return $this;
    }

    /**
     * Get rearbrakepads
     *
     * @return boolean
     */
    public function getRearbrakepads()
    {
        return $this->rearbrakepads;
    }

    /**
     * Set frontbrakediscs
     *
     * @param boolean $frontbrakediscs
     * @return VehiculeMaintenance
     */
    public function setFrontbrakediscs($frontbrakediscs)
    {
        $this->frontbrakediscs = $frontbrakediscs;

        return $this;
    }

    /**
     * Get frontbrakediscs
     *
     * @return boolean
     */
    public function getFrontbrakediscs()
    {
        return $this->frontbrakediscs;
    }

    /**
     * Set rearbrakediscs
     *
     * @param boolean $rearbrakediscs
     * @return VehiculeMaintenance
     */
    public function setRearbrakediscs($rearbrakediscs)
    {
        $this->rearbrakediscs = $rearbrakediscs;

        return $this;
    }

    /**
     * Get rearbrakediscs
     *
     * @return boolean
     */
    public function getRearbrakediscs()
    {
        return $this->rearbrakediscs;
    }

    /**
     * Set tow
     *
     * @param boolean $tow
     * @return VehiculeMaintenance
     */
    public function setTow($tow)
    {
        $this->tow = $tow;

        return $this;
    }

    /**
     * Get tow
     *
     * @return boolean
     */
    public function getTow()
    {
        return $this->tow;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return VehiculeMaintenance
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return VehiculeMaintenance
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return VehiculeMaintenance
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return VehiculeMaintenance
     */
    public function setDriver(\CAB\UserBundle\Entity\User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set vehicule
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicule
     * @return VehiculeMaintenance
     */
    public function setVehicule(\CAB\CourseBundle\Entity\Vehicule $vehicule = null)
    {
        $this->vehicule = $vehicule;

        return $this;
    }

    /**
     * Get vehicule
     *
     * @return \CAB\CourseBundle\Entity\Vehicule
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return VehiculeMaintenance
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set liquidwindiwsajust
     *
     * @param boolean $liquidwindiwsajust
     * @return VehiculeMaintenance
     */
    public function setLiquidwindiwsajust($liquidwindiwsajust)
    {
        $this->liquidwindiwsajust = $liquidwindiwsajust;

        return $this;
    }

    /**
     * Get liquidwindiwsajust
     *
     * @return boolean
     */
    public function getLiquidwindiwsajust()
    {
        return $this->liquidwindiwsajust;
    }

    /**
     * Set photoInvoice
     *
     * @param UploadedFile $photoInvoice
     * @return VehiculeMaintenance
     */
    public function setPhotoInvoice(UploadedFile $photoInvoice= null)
    {
        $this->photoInvoice = $photoInvoice;

        return $this;
    }

    /**
     * Get photoInvoice
     *
     * @return string
     */
    public function getPhotoInvoice()
    {
        return $this->photoInvoice;
    }



    /**
     * Set startdAt
     *
     * @param \DateTime $startdAt
     * @return VehiculeMaintenance
     */
    public function setStartdAt($startdAt)
    {
        $this->startdAt = $startdAt;

        return $this;
    }

    /**
     * Get startdAt
     *
     * @return \DateTime
     */
    public function getStartdAt()
    {
        return $this->startdAt;
    }

    /**
     * Set enddAt
     *
     * @param \DateTime $enddAt
     * @return VehiculeMaintenance
     */
    public function setEnddAt($enddAt)
    {
        $this->enddAt = $enddAt;

        return $this;
    }

    /**
     * Get enddAt
     *
     * @return \DateTime
     */
    public function getEnddAt()
    {
        return $this->enddAt;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     * @return VehiculeMaintenance
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     * @return VehiculeMaintenance
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }


  /**
   * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
   *
   * @return VehiculeMaintenance
   */
  public function setFile(File $file = null)
  {
    $this->file = $file;

    if ($file) {
      // It is required that at least one field changes if you are using doctrine
      // otherwise the event listeners won't be called and the file is lost
      $this->updatedAt = new \DateTimeImmutable();
    }

    return $this;
  }

  /**
   * @return File|null
   */
  public function getFile()
  {
    return $this->file;
  }


    /**
     * Set fileName
     *
     * @param string $fileName
     * @return VehiculeMaintenance
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set companyVehicleMaintenance
     *
     * @param \CAB\CourseBundle\Entity\Company $companyVehicleMaintenance
     * @return VehiculeMaintenance
     */
    public function setCompanyVehicleMaintenance(\CAB\CourseBundle\Entity\Company $companyVehicleMaintenance = null)
    {
        $this->companyVehicleMaintenance = $companyVehicleMaintenance;

        return $this;
    }

    /**
     * Get companyVehicleMaintenance
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompanyVehicleMaintenance()
    {
        return $this->companyVehicleMaintenance;
    }

    /**
     * Get Day immobbilisation car
     *
     * @return VehiculeMaintenance
     */
    public function getDayImmo()
    {
        $start = $this->startAt;
        $end = $this->endAt;
        if(isset($start,$end)) {
            $DayImmo= $start->diff($end);
        }else{
            $DayImmo = 0;
        }

        return $DayImmo;
    }
}
