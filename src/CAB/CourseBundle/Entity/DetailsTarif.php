<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 09:17
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * CAB\CourseBundle\Entity\DetailsTarif
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_details_tarif")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\DetailsTarifRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class DetailsTarif
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Course", mappedBy="courseDetailsTarif", cascade={"persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $detailsTarifCourse;

    /**
     * @var decimal $tarifHT
     *
     * @ORM\Column(name="tarif_total_ht", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifTotalHT;

    /**
     * @var decimal $tarifTotalTTC
     *
     * @ORM\Column(name="tarif_total_ttc", type="decimal", precision=8, scale=2, nullable=true)
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Course")
     * @Expose
     * @Groups({"Public", "Course"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $tarifTotalTTC;

    /**
     * @var decimal $tarifHT
     *
     * @ORM\Column(name="tarif_ht", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifHT;

    /**
     * @var decimal $tarifBackHT
     *
     * @ORM\Column(name="tarif_back_ht", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifBackHT;

    /**
     * @var decimal $tarifLuggage
     *
     * @ORM\Column(name="tarif_luggage", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifLuggage;

    /**
     * @var decimal $tarifEquipment
     *
     * @ORM\Column(name="tarif_equipment", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifEquipment;

    /**
     * @var decimal $tarifHandicapWheelchair
     *
     * @ORM\Column(name="tarif_handicap_wheelchair", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifHandicapWheelchair;

    /**
     * @var decimal $tarifBabySeat
     *
     * @ORM\Column(name="tarif_baby_seat", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifBabySeat;

    /**
     * @var decimal $tarifApproachTime
     *
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $tarifApproachTime;

    /**
     * @var decimal $tarifLuggageBack
     *
     * @ORM\Column(name="tarif_luggage_back", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifLuggageBack;

    /**
     * @var decimal $tarifEquipmentBack
     *
     * @ORM\Column(name="tarif_equipment_back", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifEquipmentBack;

    /**
     * @var decimal $tarifHandicapWheelchairBack
     *
     * @ORM\Column(name="tarif_handicap_wheelchair_back", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifHandicapWheelchairBack;

    /**
     * @var decimal $tarifBabySeatBack
     *
     * @ORM\Column(name="tarif_baby_seat_back", type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifBabySeatBack;

    /**
     * @var decimal $tarifApproachTimeBack
     *
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     */
    protected $tarifApproachTimeBack;

    /**
     * @var decimal $estimatedTarif
     *
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $estimatedTarif;

    /**
     * @var boolean $isForfait
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $isForfait;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tarifHT
     *
     * @param string $tarifHT
     * @return DetailsTarif
     */
    public function setTarifHT($tarifHT)
    {
        $this->tarifHT = $tarifHT;

        return $this;
    }

    /**
     * Get tarifHT
     *
     * @return string
     */
    public function getTarifHT()
    {
        return $this->tarifHT;
    }

    /**
     * Set tarifBackHT
     *
     * @param string $tarifBackHT
     * @return DetailsTarif
     */
    public function setTarifBackHT($tarifBackHT)
    {
        $this->tarifBackHT = $tarifBackHT;

        return $this;
    }

    /**
     * Get tarifBackHT
     *
     * @return string
     */
    public function getTarifBackHT()
    {
        return $this->tarifBackHT;
    }

    /**
     * Set tarifLuggage
     *
     * @param string $tarifLuggage
     * @return DetailsTarif
     */
    public function setTarifLuggage($tarifLuggage)
    {
        $this->tarifLuggage = $tarifLuggage;

        return $this;
    }

    /**
     * Get tarifLuggage
     *
     * @return string
     */
    public function getTarifLuggage()
    {
        return $this->tarifLuggage;
    }

    /**
     * Set tarifEquipment
     *
     * @param string $tarifEquipment
     * @return DetailsTarif
     */
    public function setTarifEquipment($tarifEquipment)
    {
        $this->tarifEquipment = $tarifEquipment;

        return $this;
    }

    /**
     * Get tarifEquipment
     *
     * @return string
     */
    public function getTarifEquipment()
    {
        return $this->tarifEquipment;
    }

    /**
     * Set tarifHandicapWheelchair
     *
     * @param string $tarifHandicapWheelchair
     * @return DetailsTarif
     */
    public function setTarifHandicapWheelchair($tarifHandicapWheelchair)
    {
        $this->tarifHandicapWheelchair = $tarifHandicapWheelchair;

        return $this;
    }

    /**
     * Get tarifHandicapWheelchair
     *
     * @return string
     */
    public function getTarifHandicapWheelchair()
    {
        return $this->tarifHandicapWheelchair;
    }

    /**
     * Set tarifBabySeat
     *
     * @param string $tarifBabySeat
     * @return DetailsTarif
     */
    public function setTarifBabySeat($tarifBabySeat)
    {
        $this->tarifBabySeat = $tarifBabySeat;

        return $this;
    }

    /**
     * Get tarifBabySeat
     *
     * @return string
     */
    public function getTarifBabySeat()
    {
        return $this->tarifBabySeat;
    }

    /**
     * Set tarifLuggageBack
     *
     * @param string $tarifLuggageBack
     * @return DetailsTarif
     */
    public function setTarifLuggageBack($tarifLuggageBack)
    {
        $this->tarifLuggageBack = $tarifLuggageBack;

        return $this;
    }

    /**
     * Get tarifLuggageBack
     *
     * @return string
     */
    public function getTarifLuggageBack()
    {
        return $this->tarifLuggageBack;
    }

    /**
     * Set tarifEquipmentBack
     *
     * @param string $tarifEquipmentBack
     * @return DetailsTarif
     */
    public function setTarifEquipmentBack($tarifEquipmentBack)
    {
        $this->tarifEquipmentBack = $tarifEquipmentBack;

        return $this;
    }

    /**
     * Get tarifEquipmentBack
     *
     * @return string
     */
    public function getTarifEquipmentBack()
    {
        return $this->tarifEquipmentBack;
    }

    /**
     * Set tarifHandicapWheelchairBack
     *
     * @param string $tarifHandicapWheelchairBack
     * @return DetailsTarif
     */
    public function setTarifHandicapWheelchairBack($tarifHandicapWheelchairBack)
    {
        $this->tarifHandicapWheelchairBack = $tarifHandicapWheelchairBack;

        return $this;
    }

    /**
     * Get tarifHandicapWheelchairBack
     *
     * @return string
     */
    public function getTarifHandicapWheelchairBack()
    {
        return $this->tarifHandicapWheelchairBack;
    }

    /**
     * Set tarifBabySeatBack
     *
     * @param string $tarifBabySeatBack
     * @return DetailsTarif
     */
    public function setTarifBabySeatBack($tarifBabySeatBack)
    {
        $this->tarifBabySeatBack = $tarifBabySeatBack;

        return $this;
    }

    /**
     * Get tarifBabySeatBack
     *
     * @return string
     */
    public function getTarifBabySeatBack()
    {
        return $this->tarifBabySeatBack;
    }

    /**
     * Set detailsTarifCourse
     *
     * @param \CAB\CourseBundle\Entity\Course $detailsTarifCourse
     * @return DetailsTarif
     */
    public function setDetailsTarifCourse(\CAB\CourseBundle\Entity\Course $detailsTarifCourse = null)
    {
        $this->detailsTarifCourse = $detailsTarifCourse;

        return $this;
    }

    /**
     * Get detailsTarifCourse
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getDetailsTarifCourse()
    {
        return $this->detailsTarifCourse;
    }

    /**
     * Set tarifTotalHT
     *
     * @param string $tarifTotalHT
     * @return DetailsTarif
     */
    public function setTarifTotalHT($tarifTotalHT)
    {
        $this->tarifTotalHT = $tarifTotalHT;

        return $this;
    }

    /**
     * Get tarifTotalHT
     *
     * @return string
     */
    public function getTarifTotalHT()
    {
        return $this->tarifTotalHT;
    }

    /**
     * Set tarifTotalTTC
     *
     * @param string $tarifTotalTTC
     * @return DetailsTarif
     */
    public function setTarifTotalTTC($tarifTotalTTC)
    {
        $this->tarifTotalTTC = $tarifTotalTTC;

        return $this;
    }

    /**
     * Get tarifTotalTTC
     *
     * @return string
     */
    public function getTarifTotalTTC()
    {
        return $this->tarifTotalTTC;
    }

    /**
     * Set tarifApproachTime
     *
     * @param string $tarifApproachTime
     * @return DetailsTarif
     */
    public function setTarifApproachTime($tarifApproachTime)
    {
        $this->tarifApproachTime = $tarifApproachTime;

        return $this;
    }

    /**
     * Get tarifApproachTime
     *
     * @return string
     */
    public function getTarifApproachTime()
    {
        return $this->tarifApproachTime;
    }

    /**
     * Set tarifApproachTimeBack
     *
     * @param string $tarifApproachTimeBack
     * @return DetailsTarif
     */
    public function setTarifApproachTimeBack($tarifApproachTimeBack)
    {
        $this->tarifApproachTimeBack = $tarifApproachTimeBack;

        return $this;
    }

    /**
     * Get tarifApproachTimeBack
     *
     * @return string
     */
    public function getTarifApproachTimeBack()
    {
        return $this->tarifApproachTimeBack;
    }

    /**
     * Set estimatedTarif
     *
     * @param string $estimatedTarif
     * @return DetailsTarif
     */
    public function setEstimatedTarif($estimatedTarif)
    {
        $this->estimatedTarif = $estimatedTarif;

        return $this;
    }

    /**
     * Get estimatedTarif
     *
     * @return string
     */
    public function getEstimatedTarif()
    {
        return $this->estimatedTarif;
    }

    /**
     * Set isForfait
     *
     * @param boolean $isForfait
     * @return DetailsTarif
     */
    public function setIsForfait ($isForfait)
    {
        $this->isForfait = $isForfait;

        return $this;
    }

    /**
     * Get isForfait
     *
     * @return boolean
     */
    public function getIsForfait ()
    {
        return $this->isForfait;
    }
}
