<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\ParticularPeriod
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_particular_period")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\ParticularPeriodRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class ParticularPeriod {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="particularPeriods", fetch="LAZY")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * @var string $namePeriod
     *
     * @ORM\Column(name="name_period", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.namePeriod")
     */
    protected $namePeriod;

    /**
     * @var \DateTime $startedDate
     *
     * @ORM\Column(type="datetime", name="started_date", nullable=false)
     */
    private $startedDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(type="datetime", name="end_date", nullable=false)
     */
    private $endDate;

    /**
     * @ORM\ManyToMany(targetEntity="CAB\CourseBundle\Entity\Zone", inversedBy="particularPeriods", fetch="LAZY")
     * @ORM\JoinTable(name="cab_zones_particular_periods")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $zones;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\CourseBundle\Entity\Course", mappedBy="serviceParticularPeriodCourses")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $course;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\CourseBundle\Entity\Course", mappedBy="serviceTransportCoursesBack", fetch="EXTRA_LAZY")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $courseBack;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    public function __toString(){
        return $this->getNamePeriod();
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zone = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->serviceTransports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set namePeriod
     *
     * @param string $namePeriod
     * @return ParticularPeriod
     */
    public function setNamePeriod($namePeriod)
    {
        $this->namePeriod = $namePeriod;

        return $this;
    }

    /**
     * Get namePeriod
     *
     * @return string
     */
    public function getNamePeriod()
    {
        return $this->namePeriod;
    }

    /**
     * Set startedDate
     *
     * @param \DateTime $startedDate
     * @return ParticularPeriod
     */
    public function setStartedDate($startedDate)
    {
        $this->startedDate = $startedDate;

        return $this;
    }

    /**
     * Get startedDate
     *
     * @return \DateTime
     */
    public function getStartedDate()
    {
        return $this->startedDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return ParticularPeriod
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ParticularPeriod
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ParticularPeriod
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return ParticularPeriod
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add zones
     *
     * @param \CAB\CourseBundle\Entity\Zone $zones
     * @return ParticularPeriod
     */
    public function addZones(\CAB\CourseBundle\Entity\Zone $zones)
    {
        $this->zones[] = $zones;

        return $this;
    }

    /**
     * Remove zones
     *
     * @param \CAB\CourseBundle\Entity\Zone $zones
     */
    public function removeZones(\CAB\CourseBundle\Entity\Zone $zones)
    {
        $this->zones->removeElement($zones);
    }


    /**
     * Add zones
     *
     * @param \CAB\CourseBundle\Entity\Zone $zones
     * @return ParticularPeriod
     */
    public function addZone(\CAB\CourseBundle\Entity\Zone $zones)
    {
        $this->zones[] = $zones;

        return $this;
    }

    /**
     * Remove zones
     *
     * @param \CAB\CourseBundle\Entity\Zone $zones
     */
    public function removeZone(\CAB\CourseBundle\Entity\Zone $zones)
    {
        $this->zones->removeElement($zones);
    }

    /**
     * Get zones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * Add course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return ParticularPeriod
     */
    public function addCourse(\CAB\CourseBundle\Entity\Course $course)
    {
        $this->course[] = $course;

        return $this;
    }

    /**
     * Remove course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     */
    public function removeCourse(\CAB\CourseBundle\Entity\Course $course)
    {
        $this->course->removeElement($course);
    }

    /**
     * Get course
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Add courseBack
     *
     * @param \CAB\CourseBundle\Entity\Course $courseBack
     * @return ParticularPeriod
     */
    public function addCourseBack(\CAB\CourseBundle\Entity\Course $courseBack)
    {
        $this->courseBack[] = $courseBack;

        return $this;
    }

    /**
     * Remove courseBack
     *
     * @param \CAB\CourseBundle\Entity\Course $courseBack
     */
    public function removeCourseBack(\CAB\CourseBundle\Entity\Course $courseBack)
    {
        $this->courseBack->removeElement($courseBack);
    }

    /**
     * Get courseBack
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourseBack()
    {
        return $this->courseBack;
    }
}
