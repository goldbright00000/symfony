<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\CompanyType
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_company_type")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\CompanyTypeRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class CompanyType {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $companyTypeName
     *
     * @ORM\Column(name="company_type_name", type="string", length=255, nullable=false)
     */
    protected $companyTypeName;

    /**
    * @var string $description
    *
    * @ORM\Column(name="description", type="text",  nullable=true)
    */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Company", mappedBy="companyType")
     */
    private $companiesType;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    public function __toString(){
        return $this->getCompanyTypeName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyTypeName
     *
     * @param string $companyTypeName
     * @return CompanyType
     */
    public function setCompanyTypeName($companyTypeName)
    {
        $this->companyTypeName = $companyTypeName;

        return $this;
    }

    /**
     * Get companyTypeName
     *
     * @return string
     */
    public function getCompanyTypeName()
    {
        return $this->companyTypeName;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CompanyType
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CompanyType
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companiesType = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add companiesType
     *
     * @param \CAB\CourseBundle\Entity\Company $companiesType
     * @return CompanyType
     */
    public function addCompaniesType(\CAB\CourseBundle\Entity\Company $companiesType)
    {
        $this->companiesType[] = $companiesType;

        return $this;
    }

    /**
     * Remove companiesType
     *
     * @param \CAB\CourseBundle\Entity\Company $companiesType
     */
    public function removeCompaniesType(\CAB\CourseBundle\Entity\Company $companiesType)
    {
        $this->companiesType->removeElement($companiesType);
    }

    /**
     * Get companiesType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompaniesType()
    {
        return $this->companiesType;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CompanyType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
