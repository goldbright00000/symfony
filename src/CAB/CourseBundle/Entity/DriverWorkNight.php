<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\DriverWorkNight
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_driver_work_night")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\DriverWorkNightRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class DriverWorkNight {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="driverWorkNight")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driverNight;

    /**
     * @var boolean $monday
     *
     * @ORM\Column(name="monday", type="boolean", nullable=true)
     */
    protected $monday = 1;

    /**
     * @var boolean $tuesday
     *
     * @ORM\Column(name="tuesday", type="boolean", nullable=true)
     */
    protected $tuesday = 1;

    /**
     * @var boolean $wednesday
     *
     * @ORM\Column(name="wednesday", type="boolean", nullable=true)
     */
    protected $wednesday = 1;

    /**
     * @var boolean $thursday
     *
     * @ORM\Column(name="thursday", type="boolean", nullable=true)
     */
    protected $thursday = 1;

    /**
     * @var boolean $friday
     *
     * @ORM\Column(name="friday", type="boolean", nullable=true)
     */
    protected $friday = 1;

    /**
     * @var boolean $saturday
     *
     * @ORM\Column(name="saturday", type="boolean", nullable=true)
     */
    protected $saturday = 1;

    /**
     * @var boolean $sunday
     *
     * @ORM\Column(name="sunday", type="boolean", nullable=true)
     */
    protected $sunday = 1;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_start_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_start_pause_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_end_break_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_end_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_start_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_start_pause_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_end_break_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_end_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_start_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_start_pause_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_end_break_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_end_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_start_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_start_pause_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_end_break_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_end_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_start_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_start_pause_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_end_break_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_end_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_start_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_start_pause_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_end_break_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_end_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_start_time_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_start_pause_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_end_break_night;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_end_time_night;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString(){
        return (string)$this->getId();

    }

    /**
     * Set monday
     *
     * @param boolean $monday
     * @return DriverWorkNight
     */
    public function setMonday($monday)
    {
        $this->monday = $monday;

        return $this;
    }

    /**
     * Get monday
     *
     * @return boolean
     */
    public function getMonday()
    {
        return $this->monday;
    }

    /**
     * Set tuesday
     *
     * @param boolean $tuesday
     * @return DriverWorkNight
     */
    public function setTuesday($tuesday)
    {
        $this->tuesday = $tuesday;

        return $this;
    }

    /**
     * Get tuesday
     *
     * @return boolean
     */
    public function getTuesday()
    {
        return $this->tuesday;
    }

    /**
     * Set wednesday
     *
     * @param boolean $wednesday
     * @return DriverWorkNight
     */
    public function setWednesday($wednesday)
    {
        $this->wednesday = $wednesday;

        return $this;
    }

    /**
     * Get wednesday
     *
     * @return boolean
     */
    public function getWednesday()
    {
        return $this->wednesday;
    }

    /**
     * Set thursday
     *
     * @param boolean $thursday
     * @return DriverWorkNight
     */
    public function setThursday($thursday)
    {
        $this->thursday = $thursday;

        return $this;
    }

    /**
     * Get thursday
     *
     * @return boolean
     */
    public function getThursday()
    {
        return $this->thursday;
    }

    /**
     * Set friday
     *
     * @param boolean $friday
     * @return DriverWorkNight
     */
    public function setFriday($friday)
    {
        $this->friday = $friday;

        return $this;
    }

    /**
     * Get friday
     *
     * @return boolean
     */
    public function getFriday()
    {
        return $this->friday;
    }

    /**
     * Set saturday
     *
     * @param boolean $saturday
     * @return DriverWorkNight
     */
    public function setSaturday($saturday)
    {
        $this->saturday = $saturday;

        return $this;
    }

    /**
     * Get saturday
     *
     * @return boolean
     */
    public function getSaturday()
    {
        return $this->saturday;
    }

    /**
     * Set sunday
     *
     * @param boolean $sunday
     * @return DriverWorkNight
     */
    public function setSunday($sunday)
    {
        $this->sunday = $sunday;

        return $this;
    }

    /**
     * Get sunday
     *
     * @return boolean
     */
    public function getSunday()
    {
        return $this->sunday;
    }

    /**
     * Set monday_start_time_night
     *
     * @param \DateTime $mondayStartTimeNight
     * @return DriverWorkNight
     */
    public function setMondayStartTimeNight($mondayStartTimeNight)
    {
        $this->monday_start_time_night = $mondayStartTimeNight;

        return $this;
    }

    /**
     * Get monday_start_time_night
     *
     * @return \DateTime
     */
    public function getMondayStartTimeNight()
    {
        return $this->monday_start_time_night;
    }

    /**
     * Set monday_start_pause_night
     *
     * @param \DateTime $mondayStartPauseNight
     * @return DriverWorkNight
     */
    public function setMondayStartPauseNight($mondayStartPauseNight)
    {
        $this->monday_start_pause_night = $mondayStartPauseNight;

        return $this;
    }

    /**
     * Get monday_start_pause_night
     *
     * @return \DateTime
     */
    public function getMondayStartPauseNight()
    {
        return $this->monday_start_pause_night;
    }

    /**
     * Set monday_end_break_night
     *
     * @param \DateTime $mondayEndBreakNight
     * @return DriverWorkNight
     */
    public function setMondayEndBreakNight($mondayEndBreakNight)
    {
        $this->monday_end_break_night = $mondayEndBreakNight;

        return $this;
    }

    /**
     * Get monday_end_break_night
     *
     * @return \DateTime
     */
    public function getMondayEndBreakNight()
    {
        return $this->monday_end_break_night;
    }

    /**
     * Set monday_end_time_night
     *
     * @param \DateTime $mondayEndTimeNight
     * @return DriverWorkNight
     */
    public function setMondayEndTimeNight($mondayEndTimeNight)
    {
        $this->monday_end_time_night = $mondayEndTimeNight;

        return $this;
    }

    /**
     * Get monday_end_time_night
     *
     * @return \DateTime
     */
    public function getMondayEndTimeNight()
    {
        return $this->monday_end_time_night;
    }

    /**
     * Set tuesday_start_time_night
     *
     * @param \DateTime $tuesdayStartTimeNight
     * @return DriverWorkNight
     */
    public function setTuesdayStartTimeNight($tuesdayStartTimeNight)
    {
        $this->tuesday_start_time_night = $tuesdayStartTimeNight;

        return $this;
    }

    /**
     * Get tuesday_start_time_night
     *
     * @return \DateTime
     */
    public function getTuesdayStartTimeNight()
    {
        return $this->tuesday_start_time_night;
    }

    /**
     * Set tuesday_start_pause_night
     *
     * @param \DateTime $tuesdayStartPauseNight
     * @return DriverWorkNight
     */
    public function setTuesdayStartPauseNight($tuesdayStartPauseNight)
    {
        $this->tuesday_start_pause_night = $tuesdayStartPauseNight;

        return $this;
    }

    /**
     * Get tuesday_start_pause_night
     *
     * @return \DateTime
     */
    public function getTuesdayStartPauseNight()
    {
        return $this->tuesday_start_pause_night;
    }

    /**
     * Set tuesday_end_break_night
     *
     * @param \DateTime $tuesdayEndBreakNight
     * @return DriverWorkNight
     */
    public function setTuesdayEndBreakNight($tuesdayEndBreakNight)
    {
        $this->tuesday_end_break_night = $tuesdayEndBreakNight;

        return $this;
    }

    /**
     * Get tuesday_end_break_night
     *
     * @return \DateTime
     */
    public function getTuesdayEndBreakNight()
    {
        return $this->tuesday_end_break_night;
    }

    /**
     * Set tuesday_end_time_night
     *
     * @param \DateTime $tuesdayEndTimeNight
     * @return DriverWorkNight
     */
    public function setTuesdayEndTimeNight($tuesdayEndTimeNight)
    {
        $this->tuesday_end_time_night = $tuesdayEndTimeNight;

        return $this;
    }

    /**
     * Get tuesday_end_time_night
     *
     * @return \DateTime
     */
    public function getTuesdayEndTimeNight()
    {
        return $this->tuesday_end_time_night;
    }

    /**
     * Set wednesday_start_time_night
     *
     * @param \DateTime $wednesdayStartTimeNight
     * @return DriverWorkNight
     */
    public function setWednesdayStartTimeNight($wednesdayStartTimeNight)
    {
        $this->wednesday_start_time_night = $wednesdayStartTimeNight;

        return $this;
    }

    /**
     * Get wednesday_start_time_night
     *
     * @return \DateTime
     */
    public function getWednesdayStartTimeNight()
    {
        return $this->wednesday_start_time_night;
    }

    /**
     * Set wednesday_start_pause_night
     *
     * @param \DateTime $wednesdayStartPauseNight
     * @return DriverWorkNight
     */
    public function setWednesdayStartPauseNight($wednesdayStartPauseNight)
    {
        $this->wednesday_start_pause_night = $wednesdayStartPauseNight;

        return $this;
    }

    /**
     * Get wednesday_start_pause_night
     *
     * @return \DateTime
     */
    public function getWednesdayStartPauseNight()
    {
        return $this->wednesday_start_pause_night;
    }

    /**
     * Set wednesday_end_break_night
     *
     * @param \DateTime $wednesdayEndBreakNight
     * @return DriverWorkNight
     */
    public function setWednesdayEndBreakNight($wednesdayEndBreakNight)
    {
        $this->wednesday_end_break_night = $wednesdayEndBreakNight;

        return $this;
    }

    /**
     * Get wednesday_end_break_night
     *
     * @return \DateTime
     */
    public function getWednesdayEndBreakNight()
    {
        return $this->wednesday_end_break_night;
    }

    /**
     * Set wednesday_end_time_night
     *
     * @param \DateTime $wednesdayEndTimeNight
     * @return DriverWorkNight
     */
    public function setWednesdayEndTimeNight($wednesdayEndTimeNight)
    {
        $this->wednesday_end_time_night = $wednesdayEndTimeNight;

        return $this;
    }

    /**
     * Get wednesday_end_time_night
     *
     * @return \DateTime
     */
    public function getWednesdayEndTimeNight()
    {
        return $this->wednesday_end_time_night;
    }

    /**
     * Set thursday_start_time_night
     *
     * @param \DateTime $thursdayStartTimeNight
     * @return DriverWorkNight
     */
    public function setThursdayStartTimeNight($thursdayStartTimeNight)
    {
        $this->thursday_start_time_night = $thursdayStartTimeNight;

        return $this;
    }

    /**
     * Get thursday_start_time_night
     *
     * @return \DateTime
     */
    public function getThursdayStartTimeNight()
    {
        return $this->thursday_start_time_night;
    }

    /**
     * Set thursday_start_pause_night
     *
     * @param \DateTime $thursdayStartPauseNight
     * @return DriverWorkNight
     */
    public function setThursdayStartPauseNight($thursdayStartPauseNight)
    {
        $this->thursday_start_pause_night = $thursdayStartPauseNight;

        return $this;
    }

    /**
     * Get thursday_start_pause_night
     *
     * @return \DateTime
     */
    public function getThursdayStartPauseNight()
    {
        return $this->thursday_start_pause_night;
    }

    /**
     * Set thursday_end_break_night
     *
     * @param \DateTime $thursdayEndBreakNight
     * @return DriverWorkNight
     */
    public function setThursdayEndBreakNight($thursdayEndBreakNight)
    {
        $this->thursday_end_break_night = $thursdayEndBreakNight;

        return $this;
    }

    /**
     * Get thursday_end_break_night
     *
     * @return \DateTime
     */
    public function getThursdayEndBreakNight()
    {
        return $this->thursday_end_break_night;
    }

    /**
     * Set thursday_end_time_night
     *
     * @param \DateTime $thursdayEndTimeNight
     * @return DriverWorkNight
     */
    public function setThursdayEndTimeNight($thursdayEndTimeNight)
    {
        $this->thursday_end_time_night = $thursdayEndTimeNight;

        return $this;
    }

    /**
     * Get thursday_end_time_night
     *
     * @return \DateTime
     */
    public function getThursdayEndTimeNight()
    {
        return $this->thursday_end_time_night;
    }

    /**
     * Set friday_start_time_night
     *
     * @param \DateTime $fridayStartTimeNight
     * @return DriverWorkNight
     */
    public function setFridayStartTimeNight($fridayStartTimeNight)
    {
        $this->friday_start_time_night = $fridayStartTimeNight;

        return $this;
    }

    /**
     * Get friday_start_time_night
     *
     * @return \DateTime
     */
    public function getFridayStartTimeNight()
    {
        return $this->friday_start_time_night;
    }

    /**
     * Set friday_start_pause_night
     *
     * @param \DateTime $fridayStartPauseNight
     * @return DriverWorkNight
     */
    public function setFridayStartPauseNight($fridayStartPauseNight)
    {
        $this->friday_start_pause_night = $fridayStartPauseNight;

        return $this;
    }

    /**
     * Get friday_start_pause_night
     *
     * @return \DateTime
     */
    public function getFridayStartPauseNight()
    {
        return $this->friday_start_pause_night;
    }

    /**
     * Set friday_end_break_night
     *
     * @param \DateTime $fridayEndBreakNight
     * @return DriverWorkNight
     */
    public function setFridayEndBreakNight($fridayEndBreakNight)
    {
        $this->friday_end_break_night = $fridayEndBreakNight;

        return $this;
    }

    /**
     * Get friday_end_break_night
     *
     * @return \DateTime
     */
    public function getFridayEndBreakNight()
    {
        return $this->friday_end_break_night;
    }

    /**
     * Set friday_end_time_night
     *
     * @param \DateTime $fridayEndTimeNight
     * @return DriverWorkNight
     */
    public function setFridayEndTimeNight($fridayEndTimeNight)
    {
        $this->friday_end_time_night = $fridayEndTimeNight;

        return $this;
    }

    /**
     * Get friday_end_time_night
     *
     * @return \DateTime
     */
    public function getFridayEndTimeNight()
    {
        return $this->friday_end_time_night;
    }

    /**
     * Set saturday_start_time_night
     *
     * @param \DateTime $saturdayStartTimeNight
     * @return DriverWorkNight
     */
    public function setSaturdayStartTimeNight($saturdayStartTimeNight)
    {
        $this->saturday_start_time_night = $saturdayStartTimeNight;

        return $this;
    }

    /**
     * Get saturday_start_time_night
     *
     * @return \DateTime
     */
    public function getSaturdayStartTimeNight()
    {
        return $this->saturday_start_time_night;
    }

    /**
     * Set saturday_start_pause_night
     *
     * @param \DateTime $saturdayStartPauseNight
     * @return DriverWorkNight
     */
    public function setSaturdayStartPauseNight($saturdayStartPauseNight)
    {
        $this->saturday_start_pause_night = $saturdayStartPauseNight;

        return $this;
    }

    /**
     * Get saturday_start_pause_night
     *
     * @return \DateTime
     */
    public function getSaturdayStartPauseNight()
    {
        return $this->saturday_start_pause_night;
    }

    /**
     * Set saturday_end_break_night
     *
     * @param \DateTime $saturdayEndBreakNight
     * @return DriverWorkNight
     */
    public function setSaturdayEndBreakNight($saturdayEndBreakNight)
    {
        $this->saturday_end_break_night = $saturdayEndBreakNight;

        return $this;
    }

    /**
     * Get saturday_end_break_night
     *
     * @return \DateTime
     */
    public function getSaturdayEndBreakNight()
    {
        return $this->saturday_end_break_night;
    }

    /**
     * Set saturday_end_time_night
     *
     * @param \DateTime $saturdayEndTimeNight
     * @return DriverWorkNight
     */
    public function setSaturdayEndTimeNight($saturdayEndTimeNight)
    {
        $this->saturday_end_time_night = $saturdayEndTimeNight;

        return $this;
    }

    /**
     * Get saturday_end_time_night
     *
     * @return \DateTime
     */
    public function getSaturdayEndTimeNight()
    {
        return $this->saturday_end_time_night;
    }

    /**
     * Set sunday_start_time_night
     *
     * @param \DateTime $sundayStartTimeNight
     * @return DriverWorkNight
     */
    public function setSundayStartTimeNight($sundayStartTimeNight)
    {
        $this->sunday_start_time_night = $sundayStartTimeNight;

        return $this;
    }

    /**
     * Get sunday_start_time_night
     *
     * @return \DateTime
     */
    public function getSundayStartTimeNight()
    {
        return $this->sunday_start_time_night;
    }

    /**
     * Set sunday_start_pause_night
     *
     * @param \DateTime $sundayStartPauseNight
     * @return DriverWorkNight
     */
    public function setSundayStartPauseNight($sundayStartPauseNight)
    {
        $this->sunday_start_pause_night = $sundayStartPauseNight;

        return $this;
    }

    /**
     * Get sunday_start_pause_night
     *
     * @return \DateTime
     */
    public function getSundayStartPauseNight()
    {
        return $this->sunday_start_pause_night;
    }

    /**
     * Set sunday_end_break_night
     *
     * @param \DateTime $sundayEndBreakNight
     * @return DriverWorkNight
     */
    public function setSundayEndBreakNight($sundayEndBreakNight)
    {
        $this->sunday_end_break_night = $sundayEndBreakNight;

        return $this;
    }

    /**
     * Get sunday_end_break_night
     *
     * @return \DateTime
     */
    public function getSundayEndBreakNight()
    {
        return $this->sunday_end_break_night;
    }

    /**
     * Set sunday_end_time_night
     *
     * @param \DateTime $sundayEndTimeNight
     * @return DriverWorkNight
     */
    public function setSundayEndTimeNight($sundayEndTimeNight)
    {
        $this->sunday_end_time_night = $sundayEndTimeNight;

        return $this;
    }

    /**
     * Get sunday_end_time_night
     *
     * @return \DateTime
     */
    public function getSundayEndTimeNight()
    {
        return $this->sunday_end_time_night;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return DriverWorkNight
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return DriverWorkNight
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set driverNight
     *
     * @param \CAB\UserBundle\Entity\User $driverNight
     * @return DriverWorkNight
     */
    public function setDriverNight(\CAB\UserBundle\Entity\User $driverNight = null)
    {
        $this->driverNight = $driverNight;

        return $this;
    }

    /**
     * Get driverNight
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriverNight()
    {
        return $this->driverNight;
    }
}
