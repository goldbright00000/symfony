<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\VehiculeModel
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_vehicule_model")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\VehiculeModelRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class VehiculeModel
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\VehiculeMake", inversedBy="models")
     * @ORM\JoinColumn(name="vehicule_make_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicleMake;

    /**
     * @var string $modUtac
     *
     * @ORM\Column(name="mod_utac", type="string", length=255, nullable=false)
     */
    private $modUtac;

    /**
     * @var string $dscom
     *
     * @ORM\Column(name="dscom", type="string", length=255, nullable=false)
     */
    private $dscom;

    /**
     * @var boolean $cnit
     *
     * @ORM\Column(name="cnit", type="string", length=255, nullable=false)
     */
    private $cnit;

    /**
     * @var string $tvv
     *
     * @ORM\Column(name="tvv", type="string", length=255, nullable=false)
     */
    private $tvv;

    /**
     * @var string $energ
     *
     * @ORM\Column(name="energ", type="string", length=255, nullable=false)
     */
    private $energ;

    /**
     * @var string $hybride
     *
     * @ORM\Column(name="hybride", type="string", length=255, nullable=false)
     */
    private $hybride;

    /**
     * @var decimal $puissAdmin
     *
     * @ORM\Column(name="puiss_admin", type="decimal", precision=5, scale=1, nullable=true)
     */
    private $puissAdmin;

    /**
     * @var string $puissMax
     *
     * @ORM\Column(name="puiss_max", type="decimal", precision=4, scale=1, nullable=true)
     */
    private $puissMax;

    /**
     * @var boolean $puissHeure
     *
     * @ORM\Column(name="puiss_heure", type="float", precision=4, scale=1, nullable=true)
     */
    private $puissHeure;

    /**
     * @var boolean $typBoiteNbRapp
     *
     * @ORM\Column(name="typ_boite_nb_rapp", type="string", length=4, nullable=true)
     */
    private $typBoiteNbRapp = false;

    /**
     * @var boolean $consoUrb93
     *
     * @ORM\Column(name="conso_urb_93", type="float", precision=4, scale=1, nullable=true)
     */
    private $consoUrb93;

    /**
     * @var boolean $consoExurb
     *
     * @ORM\Column(name="conso_exurb", type="float", precision=4, scale=1, nullable=true)
     */
    private $consoExurb;

    /**
     * @var boolean $consoMixte
     *
     * @ORM\Column(name="conso_mixte", type="float", precision=4, scale=1, nullable=true)
     */
    private $consoMixte;

    /**
     * @var string $co2Mixte
     * @ORM\Column(type="integer", nullable=true)
     */
    private $co2Mixte;

    /**
     * @var boolean $coTyp1
     *
     * @ORM\Column(name="co_typ_1", type="float", precision=5, scale=3, nullable=true)
     */
    private $coTyp1;

    /**
     * @var boolean $hc
     *
     * @ORM\Column(name="hc", type="float", precision=5, scale=3, nullable=true)
     */

    private $hc;


    /**
     * @var boolean $nox
     *
     * @ORM\Column(name="nox", type="float", precision=5, scale=3, nullable=true)
     */

    private $nox;

    /**
     * @var boolean $hcnox
     *
     * @ORM\Column(name="hcnox", type="float", precision=5, scale=3, nullable=true)
     */

    private $hcnox;

    /**
     * @var boolean $ptcl
     *
     * @ORM\Column(name="ptcl", type="float", precision=5, scale=3, nullable=true)
     */

    private $ptcl;

    /**
     * @var string $masseOrdmaMin
     * @ORM\Column(type="integer", name="masse_ordma_min", nullable=true)
     */
    private $masseOrdmaMin;

    /**
     * @var string $masseOrdmaMax
     * @ORM\Column(type="integer", name="masse_ordma_max", nullable=true)
     */
    private $masseOrdmaMax;

    /**
     * @var string $champv9
     * @ORM\Column(type="integer", name="champ_v9", nullable=true)
     */
    private $champv9;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Vehicule", mappedBy="vehicleModel")
     */
    private $vehicles;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime('0000-00-00');
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }


    public function __toString()
    {
        return $this->getModUtac().' - '.$this->getDscom().' - '.$this->getCnit();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modUtac
     *
     * @param string $modUtac
     * @return VehiculeModel
     */
    public function setModUtac($modUtac)
    {
        $this->modUtac = $modUtac;

        return $this;
    }

    /**
     * Get modUtac
     *
     * @return string
     */
    public function getModUtac()
    {
        return $this->modUtac;
    }

    /**
     * Set dscom
     *
     * @param string $dscom
     * @return VehiculeModel
     */
    public function setDscom($dscom)
    {
        $this->dscom = $dscom;

        return $this;
    }

    /**
     * Get dscom
     *
     * @return string
     */
    public function getDscom()
    {
        return $this->dscom;
    }

    /**
     * Set cnit
     *
     * @param string $cnit
     * @return VehiculeModel
     */
    public function setCnit($cnit)
    {
        $this->cnit = $cnit;

        return $this;
    }

    /**
     * Get cnit
     *
     * @return string
     */
    public function getCnit()
    {
        return $this->cnit;
    }

    /**
     * Set tvv
     *
     * @param string $tvv
     * @return VehiculeModel
     */
    public function setTvv($tvv)
    {
        $this->tvv = $tvv;

        return $this;
    }

    /**
     * Get tvv
     *
     * @return string
     */
    public function getTvv()
    {
        return $this->tvv;
    }

    /**
     * Set energ
     *
     * @param string $energ
     * @return VehiculeModel
     */
    public function setEnerg($energ)
    {
        $this->energ = $energ;

        return $this;
    }

    /**
     * Get energ
     *
     * @return string
     */
    public function getEnerg()
    {
        return $this->energ;
    }

    /**
     * Set hybride
     *
     * @param string $hybride
     * @return VehiculeModel
     */
    public function setHybride($hybride)
    {
        $this->hybride = $hybride;

        return $this;
    }

    /**
     * Get hybride
     *
     * @return string
     */
    public function getHybride()
    {
        return $this->hybride;
    }

    /**
     * Set puissAdmin
     *
     * @param string $puissAdmin
     * @return VehiculeModel
     */
    public function setPuissAdmin($puissAdmin)
    {
        $this->puissAdmin = $puissAdmin;

        return $this;
    }

    /**
     * Get puissAdmin
     *
     * @return string
     */
    public function getPuissAdmin()
    {
        return $this->puissAdmin;
    }

    /**
     * Set puissMax
     *
     * @param string $puissMax
     * @return VehiculeModel
     */
    public function setPuissMax($puissMax)
    {
        $this->puissMax = $puissMax;

        return $this;
    }

    /**
     * Get puissMax
     *
     * @return string
     */
    public function getPuissMax()
    {
        return $this->puissMax;
    }

    /**
     * Set puissHeure
     *
     * @param float $puissHeure
     * @return VehiculeModel
     */
    public function setPuissHeure($puissHeure)
    {
        $this->puissHeure = $puissHeure;

        return $this;
    }

    /**
     * Get puissHeure
     *
     * @return float
     */
    public function getPuissHeure()
    {
        return $this->puissHeure;
    }

    /**
     * Set typBoiteNbRapp
     *
     * @param string $typBoiteNbRapp
     * @return VehiculeModel
     */
    public function setTypBoiteNbRapp($typBoiteNbRapp)
    {
        $this->typBoiteNbRapp = $typBoiteNbRapp;

        return $this;
    }

    /**
     * Get typBoiteNbRapp
     *
     * @return string
     */
    public function getTypBoiteNbRapp()
    {
        return $this->typBoiteNbRapp;
    }

    /**
     * Set consoUrb93
     *
     * @param float $consoUrb93
     * @return VehiculeModel
     */
    public function setConsoUrb93($consoUrb93)
    {
        $this->consoUrb93 = $consoUrb93;

        return $this;
    }

    /**
     * Get consoUrb93
     *
     * @return float
     */
    public function getConsoUrb93()
    {
        return $this->consoUrb93;
    }

    /**
     * Set consoExurb
     *
     * @param float $consoExurb
     * @return VehiculeModel
     */
    public function setConsoExurb($consoExurb)
    {
        $this->consoExurb = $consoExurb;

        return $this;
    }

    /**
     * Get consoExurb
     *
     * @return float
     */
    public function getConsoExurb()
    {
        return $this->consoExurb;
    }

    /**
     * Set consoMixte
     *
     * @param float $consoMixte
     * @return VehiculeModel
     */
    public function setConsoMixte($consoMixte)
    {
        $this->consoMixte = $consoMixte;

        return $this;
    }

    /**
     * Get consoMixte
     *
     * @return float
     */
    public function getConsoMixte()
    {
        return $this->consoMixte;
    }

    /**
     * Set co2Mixte
     *
     * @param integer $co2Mixte
     * @return VehiculeModel
     */
    public function setCo2Mixte($co2Mixte)
    {
        $this->co2Mixte = $co2Mixte;

        return $this;
    }

    /**
     * Get co2Mixte
     *
     * @return integer
     */
    public function getCo2Mixte()
    {
        return $this->co2Mixte;
    }

    /**
     * Set coTyp1
     *
     * @param float $coTyp1
     * @return VehiculeModel
     */
    public function setCoTyp1($coTyp1)
    {
        $this->coTyp1 = $coTyp1;

        return $this;
    }

    /**
     * Get coTyp1
     *
     * @return float
     */
    public function getCoTyp1()
    {
        return $this->coTyp1;
    }

    /**
     * Set hc
     *
     * @param float $hc
     * @return VehiculeModel
     */
    public function setHc($hc)
    {
        $this->hc = $hc;

        return $this;
    }

    /**
     * Get hc
     *
     * @return float
     */
    public function getHc()
    {
        return $this->hc;
    }

    /**
     * Set nox
     *
     * @param float $nox
     * @return VehiculeModel
     */
    public function setNox($nox)
    {
        $this->nox = $nox;

        return $this;
    }

    /**
     * Get nox
     *
     * @return float
     */
    public function getNox()
    {
        return $this->nox;
    }

    /**
     * Set hcnox
     *
     * @param float $hcnox
     * @return VehiculeModel
     */
    public function setHcnox($hcnox)
    {
        $this->hcnox = $hcnox;

        return $this;
    }

    /**
     * Get hcnox
     *
     * @return float
     */
    public function getHcnox()
    {
        return $this->hcnox;
    }

    /**
     * Set ptcl
     *
     * @param float $ptcl
     * @return VehiculeModel
     */
    public function setPtcl($ptcl)
    {
        $this->ptcl = $ptcl;

        return $this;
    }

    /**
     * Get ptcl
     *
     * @return float
     */
    public function getPtcl()
    {
        return $this->ptcl;
    }

    /**
     * Set masseOrdmaMin
     *
     * @param integer $masseOrdmaMin
     * @return VehiculeModel
     */
    public function setMasseOrdmaMin($masseOrdmaMin)
    {
        $this->masseOrdmaMin = $masseOrdmaMin;

        return $this;
    }

    /**
     * Get masseOrdmaMin
     *
     * @return integer
     */
    public function getMasseOrdmaMin()
    {
        return $this->masseOrdmaMin;
    }

    /**
     * Set masseOrdmaMax
     *
     * @param integer $masseOrdmaMax
     * @return VehiculeModel
     */
    public function setMasseOrdmaMax($masseOrdmaMax)
    {
        $this->masseOrdmaMax = $masseOrdmaMax;

        return $this;
    }

    /**
     * Get masseOrdmaMax
     *
     * @return integer
     */
    public function getMasseOrdmaMax()
    {
        return $this->masseOrdmaMax;
    }

    /**
     * Set champv9
     *
     * @param integer $champv9
     * @return VehiculeModel
     */
    public function setChampv9($champv9)
    {
        $this->champv9 = $champv9;

        return $this;
    }

    /**
     * Get champv9
     *
     * @return integer
     */
    public function getChampv9()
    {
        return $this->champv9;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return VehiculeModel
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return VehiculeModel
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add vehicules
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicules
     * @return VehiculeModel
     */
    public function addVehicule(\CAB\CourseBundle\Entity\Vehicule $vehicules)
    {
        $this->vehicules[] = $vehicules;

        return $this;
    }

    /**
     * Remove vehicules
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicules
     */
    public function removeVehicule(\CAB\CourseBundle\Entity\Vehicule $vehicules)
    {
        $this->vehicules->removeElement($vehicules);
    }

    /**
     * Get vehicules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicules()
    {
        return $this->vehicules;
    }

    /**
     * Add vehicles
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicles
     * @return VehiculeModel
     */
    public function addVehicle(\CAB\CourseBundle\Entity\Vehicule $vehicles)
    {
        $this->vehicles[] = $vehicles;

        return $this;
    }

    /**
     * Remove vehicles
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicles
     */
    public function removeVehicle(\CAB\CourseBundle\Entity\Vehicule $vehicles)
    {
        $this->vehicles->removeElement($vehicles);
    }

    /**
     * Get vehicles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * Set vehicleMake
     *
     * @param \CAB\CourseBundle\Entity\VehiculeMake $vehicleMake
     * @return VehiculeModel
     */
    public function setVehicleMake(\CAB\CourseBundle\Entity\VehiculeMake $vehicleMake = null)
    {
        $this->vehicleMake = $vehicleMake;

        return $this;
    }

    /**
     * Get vehicleMake
     *
     * @return \CAB\CourseBundle\Entity\VehiculeMake
     */
    public function getVehicleMake()
    {
        return $this->vehicleMake;
    }
}
