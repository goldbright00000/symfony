<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 09:17
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\OptionsHandicap
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_options_handicap")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\OptionsHandicapRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class OptionsHandicap
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="CAB\UserBundle\Entity\User", mappedBy="optionsHandicap", cascade={"persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $user;

    /**
     * @var boolean $electricChair
     *
     * @ORM\Column(name="electric_chair", type="boolean", nullable=true)
     */
    protected $electricChair;

    /**
     * @var boolean $manualChair
     *
     * @ORM\Column(name="manual_chair", type="boolean", nullable=true)
     */
    protected $manualChair;

    /**
     * @var boolean $transferable
     *
     * @ORM\Column(name="transferable", type="boolean", nullable=true)
     */
    protected $transferable;

    /**
     * @var boolean $visuallyImpaired
     *
     * @ORM\Column(name="visually_impaired", type="boolean", nullable=true)
     */
    protected $visuallyImpaired;

    /**
     * @var boolean $semiValid
     *
     * @ORM\Column(name="semi_valid", type="boolean", nullable=true)
     */
    protected $semiValid;

    /**
     * @var boolean $autisme
     *
     * @ORM\Column(name="autisme", type="boolean", nullable=true)
     */
    protected $autisme;

    /**
     * @var time timeAccompagnement
     *
     * @ORM\Column(type="datetime", name="time_accompagnement", nullable=true)
     */
    private $timeAccompagnement;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return OptionsHandicap
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return OptionsHandicap
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set electricChair
     *
     * @param boolean $electricChair
     * @return OptionsHandicap
     */
    public function setElectricChair($electricChair)
    {
        $this->electricChair = $electricChair;

        return $this;
    }

    /**
     * Get electricChair
     *
     * @return boolean
     */
    public function getElectricChair()
    {
        return $this->electricChair;
    }

    /**
     * Set manualChair
     *
     * @param boolean $manualChair
     * @return OptionsHandicap
     */
    public function setManualChair($manualChair)
    {
        $this->manualChair = $manualChair;

        return $this;
    }

    /**
     * Get manualChair
     *
     * @return boolean
     */
    public function getManualChair()
    {
        return $this->manualChair;
    }

    /**
     * Set transferable
     *
     * @param boolean $transferable
     * @return OptionsHandicap
     */
    public function setTransferable($transferable)
    {
        $this->transferable = $transferable;

        return $this;
    }

    /**
     * Get transferable
     *
     * @return boolean
     */
    public function getTransferable()
    {
        return $this->transferable;
    }

    /**
     * Set visuallyImpaired
     *
     * @param boolean $visuallyImpaired
     * @return OptionsHandicap
     */
    public function setVisuallyImpaired($visuallyImpaired)
    {
        $this->visuallyImpaired = $visuallyImpaired;

        return $this;
    }

    /**
     * Get visuallyImpaired
     *
     * @return boolean
     */
    public function getVisuallyImpaired()
    {
        return $this->visuallyImpaired;
    }

    /**
     * Set semiValid
     *
     * @param boolean $semiValid
     * @return OptionsHandicap
     */
    public function setSemiValid($semiValid)
    {
        $this->semiValid = $semiValid;

        return $this;
    }

    /**
     * Get semiValid
     *
     * @return boolean
     */
    public function getSemiValid()
    {
        return $this->semiValid;
    }

    /**
     * Set autisme
     *
     * @param boolean $autisme
     * @return OptionsHandicap
     */
    public function setAutisme($autisme)
    {
        $this->autisme = $autisme;

        return $this;
    }

    /**
     * Get autisme
     *
     * @return boolean
     */
    public function getAutisme()
    {
        return $this->autisme;
    }

    /**
     * Set user
     *
     * @param \CAB\UserBundle\Entity\User $user
     * @return OptionsHandicap
     */
    public function setUser(\CAB\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set timeAccompagnement
     *
     * @param \DateTime $timeAccompagnement
     * @return OptionsHandicap
     */
    public function setTimeAccompagnement($timeAccompagnement)
    {
        $this->timeAccompagnement = $timeAccompagnement;

        return $this;
    }

    /**
     * Get timeAccompagnement
     *
     * @return \DateTime
     */
    public function getTimeAccompagnement()
    {
        return $this->timeAccompagnement;
    }
}
