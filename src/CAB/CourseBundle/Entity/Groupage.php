<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 28/01/2017
 * Time: 23:23
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use CAB\CourseBundle\Entity\Rating;

/**
 * CAB\CourseBundle\Entity\Course
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_groupage")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\GroupageRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Groupage
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Public", "Course"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Course", inversedBy="baseGroupage", cascade={"remove", "persist"})
     * * @ORM\JoinColumn(referencedColumnName="id", name="base_course_id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $baseCourse;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Course", mappedBy="linkedGroupage", cascade={"remove", "persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $linkedCourses;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->linkedCourses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set baseCourse
     *
     * @param \CAB\CourseBundle\Entity\Course $baseCourse
     * @return Groupage
     */
    public function setBaseCourse(\CAB\CourseBundle\Entity\Course $baseCourse = null)
    {
        $this->baseCourse = $baseCourse;

        return $this;
    }

    /**
     * Get baseCourse
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getBaseCourse()
    {
        return $this->baseCourse;
    }

    /**
     * Add linkedCourses
     *
     * @param \CAB\CourseBundle\Entity\Course $linkedCourses
     * @return Groupage
     */
    public function addLinkedCourse(\CAB\CourseBundle\Entity\Course $linkedCourses)
    {
        $this->linkedCourses[] = $linkedCourses;

        return $this;
    }

    /**
     * Remove linkedCourses
     *
     * @param \CAB\CourseBundle\Entity\Course $linkedCourses
     */
    public function removeLinkedCourse(\CAB\CourseBundle\Entity\Course $linkedCourses)
    {
        $this->linkedCourses->removeElement($linkedCourses);
    }

    /**
     * Get linkedCourses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinkedCourses()
    {
        return $this->linkedCourses;
    }
}
