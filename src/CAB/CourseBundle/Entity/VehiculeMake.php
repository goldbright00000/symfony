<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\VehiculeMake
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_vehicule_make")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\VehiculeMakeRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class VehiculeMake
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="make_name", type="string", length=255, nullable=false)
     */
    private $makeName;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Vehicule", mappedBy="vehicleMaker")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicles;

    /**
     * @var string $logo
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $logo;

    /**
     *
     * @var File
     *
     */
    private $file;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\VehiculeModel", mappedBy="vehicleMake")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $models;


    public function __toString()
    {
        return $this->getMakeName();
    }

    protected function getUploadRootDir($dir = 'uploads/vehicule/make')
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir($dir);
    }

    protected function getUploadDir($dir = 'uploads/vehicule/make')
    {
        return $dir;
    }

    public function getAbsolutePath($dir = null)
    {
        if ($dir == null) {
            $dir = $this->getUploadRootDir();
        }

        return null === $this->logo ? null : $dir.'/'.$this->logo;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {

        if (null !== $this->file) {
            $this->logo = sha1(uniqid(mt_rand(), true)).'.'.$this->file->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null !== $this->file) {
            $this->file->move($this->getUploadRootDir(), $this->logo);
            unset($this->file);
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->file != "") {
            if ($file = $this->getAbsolutePath()) {
                unlink($file);
            }
        }
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile($file = null)
    {
        $this->file = $file;
        if (null !== $this->file) {
            $this->logo = sha1(uniqid(mt_rand(), true)).'.'.$this->file->guessExtension();
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set makeName
     *
     * @param string $makeName
     * @return VehiculeMake
     */
    public function setMakeName($makeName)
    {
        $this->makeName = $makeName;

        return $this;
    }

    /**
     * Get makeName
     *
     * @return string
     */
    public function getMakeName()
    {
        return $this->makeName;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return VehiculeMake
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Add models
     *
     * @param \CAB\CourseBundle\Entity\VehiculeModel $models
     * @return VehiculeMake
     */
    public function addModel(\CAB\CourseBundle\Entity\VehiculeModel $models)
    {
        $this->models[] = $models;

        return $this;
    }

    /**
     * Remove models
     *
     * @param \CAB\CourseBundle\Entity\VehiculeModel $models
     */
    public function removeModel(\CAB\CourseBundle\Entity\VehiculeModel $models)
    {
        $this->models->removeElement($models);
    }

    /**
     * Get models
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModels()
    {
        return $this->models;
    }

    /**
     * Add vehicles
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicles
     * @return VehiculeMake
     */
    public function addVehicle(\CAB\CourseBundle\Entity\Vehicule $vehicles)
    {
        $this->vehicles[] = $vehicles;

        return $this;
    }

    /**
     * Remove vehicles
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicles
     */
    public function removeVehicle(\CAB\CourseBundle\Entity\Vehicule $vehicles)
    {
        $this->vehicles->removeElement($vehicles);
    }

    /**
     * Get vehicles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }
}
