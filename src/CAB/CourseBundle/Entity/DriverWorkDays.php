<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\DriverWorkDays
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_driver_work_day")
 *
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\DriverWorkDaysRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class DriverWorkDays {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="driverWorkDay")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driverDay;

    /**
     * @var boolean $monday
     *
     * @ORM\Column(name="monday", type="boolean", nullable=true)
     */
    protected $monday = 1;

    /**
     * @var boolean $tuesday
     *
     * @ORM\Column(name="tuesday", type="boolean", nullable=true)
     */
    protected $tuesday = 1;

    /**
     * @var boolean $wednesday
     *
     * @ORM\Column(name="wednesday", type="boolean", nullable=true)
     */
    protected $wednesday = 1;

    /**
     * @var boolean $thursday
     *
     * @ORM\Column(name="thursday", type="boolean", nullable=true)
     */
    protected $thursday = 1;

    /**
     * @var boolean $friday
     *
     * @ORM\Column(name="friday", type="boolean", nullable=true)
     */
    protected $friday = 1;

    /**
     * @var boolean $saturday
     *
     * @ORM\Column(name="saturday", type="boolean", nullable=true)
     */
    protected $saturday = 1;

    /**
     * @var boolean $sunday
     *
     * @ORM\Column(name="sunday", type="boolean", nullable=true)
     */
    protected $sunday = 1;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_start_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_start_pause;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_end_break;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $monday_end_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_start_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_start_pause;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_end_break;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $tuesday_end_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_start_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_start_pause;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_end_break;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $wednesday_end_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_start_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_start_pause;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_end_break;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $thursday_end_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_start_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_start_pause;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_end_break;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $friday_end_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_start_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_start_pause;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_end_break;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $saturday_end_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_start_time_day;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_start_pause;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_end_break;

    /**
     * @ORM\Column(type="time", length=20, nullable=true)
     */
    private $sunday_end_time_day;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    public function __toString(){
        return (string)$this->getId();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set monday
     *
     * @param boolean $monday
     * @return DriverWorkDays
     */
    public function setMonday($monday)
    {
        $this->monday = $monday;

        return $this;
    }

    /**
     * Get monday
     *
     * @return boolean
     */
    public function getMonday()
    {
        return $this->monday;
    }

    /**
     * Set tuesday
     *
     * @param boolean $tuesday
     * @return DriverWorkDays
     */
    public function setTuesday($tuesday)
    {
        $this->tuesday = $tuesday;

        return $this;
    }

    /**
     * Get tuesday
     *
     * @return boolean
     */
    public function getTuesday()
    {
        return $this->tuesday;
    }

    /**
     * Set wednesday
     *
     * @param boolean $wednesday
     * @return DriverWorkDays
     */
    public function setWednesday($wednesday)
    {
        $this->wednesday = $wednesday;

        return $this;
    }

    /**
     * Get wednesday
     *
     * @return boolean
     */
    public function getWednesday()
    {
        return $this->wednesday;
    }

    /**
     * Set thursday
     *
     * @param boolean $thursday
     * @return DriverWorkDays
     */
    public function setThursday($thursday)
    {
        $this->thursday = $thursday;

        return $this;
    }

    /**
     * Get thursday
     *
     * @return boolean
     */
    public function getThursday()
    {
        return $this->thursday;
    }

    /**
     * Set friday
     *
     * @param boolean $friday
     * @return DriverWorkDays
     */
    public function setFriday($friday)
    {
        $this->friday = $friday;

        return $this;
    }

    /**
     * Get friday
     *
     * @return boolean
     */
    public function getFriday()
    {
        return $this->friday;
    }

    /**
     * Set saturday
     *
     * @param boolean $saturday
     * @return DriverWorkDays
     */
    public function setSaturday($saturday)
    {
        $this->saturday = $saturday;

        return $this;
    }

    /**
     * Get saturday
     *
     * @return boolean
     */
    public function getSaturday()
    {
        return $this->saturday;
    }

    /**
     * Set sunday
     *
     * @param boolean $sunday
     * @return DriverWorkDays
     */
    public function setSunday($sunday)
    {
        $this->sunday = $sunday;

        return $this;
    }

    /**
     * Get sunday
     *
     * @return boolean
     */
    public function getSunday()
    {
        return $this->sunday;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return DriverWorkDays
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return DriverWorkDays
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return DriverWorkDays
     */
    public function setDriver(\CAB\UserBundle\Entity\User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Set monday_start_time_day
     *
     * @param \DateTime $mondayStartTimeDay
     * @return DriverWorkDays
     */
    public function setMondayStartTimeDay($mondayStartTimeDay)
    {
        $this->monday_start_time_day = $mondayStartTimeDay;

        return $this;
    }

    /**
     * Get monday_start_time_day
     *
     * @return \DateTime
     */
    public function getMondayStartTimeDay()
    {
        return $this->monday_start_time_day;
    }

    /**
     * Set monday_start_pause
     *
     * @param \DateTime $mondayStartPause
     * @return DriverWorkDays
     */
    public function setMondayStartPause($mondayStartPause)
    {
        $this->monday_start_pause = $mondayStartPause;

        return $this;
    }

    /**
     * Get monday_start_pause
     *
     * @return \DateTime
     */
    public function getMondayStartPause()
    {
        return $this->monday_start_pause;
    }

    /**
     * Set monday_end_break
     *
     * @param \DateTime $mondayEndBreak
     * @return DriverWorkDays
     */
    public function setMondayEndBreak($mondayEndBreak)
    {
        $this->monday_end_break = $mondayEndBreak;

        return $this;
    }

    /**
     * Get monday_end_break
     *
     * @return \DateTime
     */
    public function getMondayEndBreak()
    {
        return $this->monday_end_break;
    }

    /**
     * Set monday_end_time_day
     *
     * @param \DateTime $mondayEndTimeDay
     * @return DriverWorkDays
     */
    public function setMondayEndTimeDay($mondayEndTimeDay)
    {
        $this->monday_end_time_day = $mondayEndTimeDay;

        return $this;
    }

    /**
     * Get monday_end_time_day
     *
     * @return \DateTime
     */
    public function getMondayEndTimeDay()
    {
        return $this->monday_end_time_day;
    }

    /**
     * Set tuesday_start_time_day
     *
     * @param \DateTime $tuesdayStartTimeDay
     * @return DriverWorkDays
     */
    public function setTuesdayStartTimeDay($tuesdayStartTimeDay)
    {
        $this->tuesday_start_time_day = $tuesdayStartTimeDay;

        return $this;
    }

    /**
     * Get tuesday_start_time_day
     *
     * @return \DateTime
     */
    public function getTuesdayStartTimeDay()
    {
        return $this->tuesday_start_time_day;
    }

    /**
     * Set tuesday_start_pause
     *
     * @param \DateTime $tuesdayStartPause
     * @return DriverWorkDays
     */
    public function setTuesdayStartPause($tuesdayStartPause)
    {
        $this->tuesday_start_pause = $tuesdayStartPause;

        return $this;
    }

    /**
     * Get tuesday_start_pause
     *
     * @return \DateTime
     */
    public function getTuesdayStartPause()
    {
        return $this->tuesday_start_pause;
    }

    /**
     * Set tuesday_end_break
     *
     * @param \DateTime $tuesdayEndBreak
     * @return DriverWorkDays
     */
    public function setTuesdayEndBreak($tuesdayEndBreak)
    {
        $this->tuesday_end_break = $tuesdayEndBreak;

        return $this;
    }

    /**
     * Get tuesday_end_break
     *
     * @return \DateTime
     */
    public function getTuesdayEndBreak()
    {
        return $this->tuesday_end_break;
    }

    /**
     * Set tuesday_end_time_day
     *
     * @param \DateTime $tuesdayEndTimeDay
     * @return DriverWorkDays
     */
    public function setTuesdayEndTimeDay($tuesdayEndTimeDay)
    {
        $this->tuesday_end_time_day = $tuesdayEndTimeDay;

        return $this;
    }

    /**
     * Get tuesday_end_time_day
     *
     * @return \DateTime
     */
    public function getTuesdayEndTimeDay()
    {
        return $this->tuesday_end_time_day;
    }

    /**
     * Set wednesday_start_time_day
     *
     * @param \DateTime $wednesdayStartTimeDay
     * @return DriverWorkDays
     */
    public function setWednesdayStartTimeDay($wednesdayStartTimeDay)
    {
        $this->wednesday_start_time_day = $wednesdayStartTimeDay;

        return $this;
    }

    /**
     * Get wednesday_start_time_day
     *
     * @return \DateTime
     */
    public function getWednesdayStartTimeDay()
    {
        return $this->wednesday_start_time_day;
    }

    /**
     * Set wednesday_start_pause
     *
     * @param \DateTime $wednesdayStartPause
     * @return DriverWorkDays
     */
    public function setWednesdayStartPause($wednesdayStartPause)
    {
        $this->wednesday_start_pause = $wednesdayStartPause;

        return $this;
    }

    /**
     * Get wednesday_start_pause
     *
     * @return \DateTime
     */
    public function getWednesdayStartPause()
    {
        return $this->wednesday_start_pause;
    }

    /**
     * Set wednesday_end_break
     *
     * @param \DateTime $wednesdayEndBreak
     * @return DriverWorkDays
     */
    public function setWednesdayEndBreak($wednesdayEndBreak)
    {
        $this->wednesday_end_break = $wednesdayEndBreak;

        return $this;
    }

    /**
     * Get wednesday_end_break
     *
     * @return \DateTime
     */
    public function getWednesdayEndBreak()
    {
        return $this->wednesday_end_break;
    }

    /**
     * Set wednesday_end_time_day
     *
     * @param \DateTime $wednesdayEndTimeDay
     * @return DriverWorkDays
     */
    public function setWednesdayEndTimeDay($wednesdayEndTimeDay)
    {
        $this->wednesday_end_time_day = $wednesdayEndTimeDay;

        return $this;
    }

    /**
     * Get wednesday_end_time_day
     *
     * @return \DateTime
     */
    public function getWednesdayEndTimeDay()
    {
        return $this->wednesday_end_time_day;
    }

    /**
     * Set thursday_start_time_day
     *
     * @param \DateTime $thursdayStartTimeDay
     * @return DriverWorkDays
     */
    public function setThursdayStartTimeDay($thursdayStartTimeDay)
    {
        $this->thursday_start_time_day = $thursdayStartTimeDay;

        return $this;
    }

    /**
     * Get thursday_start_time_day
     *
     * @return \DateTime
     */
    public function getThursdayStartTimeDay()
    {
        return $this->thursday_start_time_day;
    }

    /**
     * Set thursday_start_pause
     *
     * @param \DateTime $thursdayStartPause
     * @return DriverWorkDays
     */
    public function setThursdayStartPause($thursdayStartPause)
    {
        $this->thursday_start_pause = $thursdayStartPause;

        return $this;
    }

    /**
     * Get thursday_start_pause
     *
     * @return \DateTime
     */
    public function getThursdayStartPause()
    {
        return $this->thursday_start_pause;
    }

    /**
     * Set thursday_end_break
     *
     * @param \DateTime $thursdayEndBreak
     * @return DriverWorkDays
     */
    public function setThursdayEndBreak($thursdayEndBreak)
    {
        $this->thursday_end_break = $thursdayEndBreak;

        return $this;
    }

    /**
     * Get thursday_end_break
     *
     * @return \DateTime
     */
    public function getThursdayEndBreak()
    {
        return $this->thursday_end_break;
    }

    /**
     * Set thursday_end_time_day
     *
     * @param \DateTime $thursdayEndTimeDay
     * @return DriverWorkDays
     */
    public function setThursdayEndTimeDay($thursdayEndTimeDay)
    {
        $this->thursday_end_time_day = $thursdayEndTimeDay;

        return $this;
    }

    /**
     * Get thursday_end_time_day
     *
     * @return \DateTime
     */
    public function getThursdayEndTimeDay()
    {
        return $this->thursday_end_time_day;
    }

    /**
     * Set friday_start_time_day
     *
     * @param \DateTime $fridayStartTimeDay
     * @return DriverWorkDays
     */
    public function setFridayStartTimeDay($fridayStartTimeDay)
    {
        $this->friday_start_time_day = $fridayStartTimeDay;

        return $this;
    }

    /**
     * Get friday_start_time_day
     *
     * @return \DateTime
     */
    public function getFridayStartTimeDay()
    {
        return $this->friday_start_time_day;
    }

    /**
     * Set friday_start_pause
     *
     * @param \DateTime $fridayStartPause
     * @return DriverWorkDays
     */
    public function setFridayStartPause($fridayStartPause)
    {
        $this->friday_start_pause = $fridayStartPause;

        return $this;
    }

    /**
     * Get friday_start_pause
     *
     * @return \DateTime
     */
    public function getFridayStartPause()
    {
        return $this->friday_start_pause;
    }

    /**
     * Set friday_end_break
     *
     * @param \DateTime $fridayEndBreak
     * @return DriverWorkDays
     */
    public function setFridayEndBreak($fridayEndBreak)
    {
        $this->friday_end_break = $fridayEndBreak;

        return $this;
    }

    /**
     * Get friday_end_break
     *
     * @return \DateTime
     */
    public function getFridayEndBreak()
    {
        return $this->friday_end_break;
    }

    /**
     * Set friday_end_time_day
     *
     * @param \DateTime $fridayEndTimeDay
     * @return DriverWorkDays
     */
    public function setFridayEndTimeDay($fridayEndTimeDay)
    {
        $this->friday_end_time_day = $fridayEndTimeDay;

        return $this;
    }

    /**
     * Get friday_end_time_day
     *
     * @return \DateTime
     */
    public function getFridayEndTimeDay()
    {
        return $this->friday_end_time_day;
    }

    /**
     * Set saturday_start_time_day
     *
     * @param \DateTime $saturdayStartTimeDay
     * @return DriverWorkDays
     */
    public function setSaturdayStartTimeDay($saturdayStartTimeDay)
    {
        $this->saturday_start_time_day = $saturdayStartTimeDay;

        return $this;
    }

    /**
     * Get saturday_start_time_day
     *
     * @return \DateTime
     */
    public function getSaturdayStartTimeDay()
    {
        return $this->saturday_start_time_day;
    }

    /**
     * Set saturday_start_pause
     *
     * @param \DateTime $saturdayStartPause
     * @return DriverWorkDays
     */
    public function setSaturdayStartPause($saturdayStartPause)
    {
        $this->saturday_start_pause = $saturdayStartPause;

        return $this;
    }

    /**
     * Get saturday_start_pause
     *
     * @return \DateTime
     */
    public function getSaturdayStartPause()
    {
        return $this->saturday_start_pause;
    }

    /**
     * Set saturday_end_break
     *
     * @param \DateTime $saturdayEndBreak
     * @return DriverWorkDays
     */
    public function setSaturdayEndBreak($saturdayEndBreak)
    {
        $this->saturday_end_break = $saturdayEndBreak;

        return $this;
    }

    /**
     * Get saturday_end_break
     *
     * @return \DateTime
     */
    public function getSaturdayEndBreak()
    {
        return $this->saturday_end_break;
    }

    /**
     * Set saturday_end_time_day
     *
     * @param \DateTime $saturdayEndTimeDay
     * @return DriverWorkDays
     */
    public function setSaturdayEndTimeDay($saturdayEndTimeDay)
    {
        $this->saturday_end_time_day = $saturdayEndTimeDay;

        return $this;
    }

    /**
     * Get saturday_end_time_day
     *
     * @return \DateTime
     */
    public function getSaturdayEndTimeDay()
    {
        return $this->saturday_end_time_day;
    }

    /**
     * Set sunday_start_time_day
     *
     * @param \DateTime $sundayStartTimeDay
     * @return DriverWorkDays
     */
    public function setSundayStartTimeDay($sundayStartTimeDay)
    {
        $this->sunday_start_time_day = $sundayStartTimeDay;

        return $this;
    }

    /**
     * Get sunday_start_time_day
     *
     * @return \DateTime
     */
    public function getSundayStartTimeDay()
    {
        return $this->sunday_start_time_day;
    }

    /**
     * Set sunday_start_pause
     *
     * @param \DateTime $sundayStartPause
     * @return DriverWorkDays
     */
    public function setSundayStartPause($sundayStartPause)
    {
        $this->sunday_start_pause = $sundayStartPause;

        return $this;
    }

    /**
     * Get sunday_start_pause
     *
     * @return \DateTime
     */
    public function getSundayStartPause()
    {
        return $this->sunday_start_pause;
    }

    /**
     * Set sunday_end_break
     *
     * @param \DateTime $sundayEndBreak
     * @return DriverWorkDays
     */
    public function setSundayEndBreak($sundayEndBreak)
    {
        $this->sunday_end_break = $sundayEndBreak;

        return $this;
    }

    /**
     * Get sunday_end_break
     *
     * @return \DateTime
     */
    public function getSundayEndBreak()
    {
        return $this->sunday_end_break;
    }

    /**
     * Set sunday_end_time_day
     *
     * @param \DateTime $sundayEndTimeDay
     * @return DriverWorkDays
     */
    public function setSundayEndTimeDay($sundayEndTimeDay)
    {
        $this->sunday_end_time_day = $sundayEndTimeDay;

        return $this;
    }

    /**
     * Get sunday_end_time_day
     *
     * @return \DateTime
     */
    public function getSundayEndTimeDay()
    {
        return $this->sunday_end_time_day;
    }

    /**
     * Set driverDay
     *
     * @param \CAB\UserBundle\Entity\User $driverDay
     * @return DriverWorkDays
     */
    public function setDriverDay(\CAB\UserBundle\Entity\User $driverDay = null)
    {
        $this->driverDay = $driverDay;

        return $this;
    }

    /**
     * Get driverDay
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriverDay()
    {
        return $this->driverDay;
    }
}
