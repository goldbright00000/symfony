<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Geolocation
 * CAB\CourseBundle\Entity\Geolocation
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_geolocation")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\GeolocationRepository")
 *  * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Geolocation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="geolocationDriver")
     * @ORM\JoinColumn(name="driver", referencedColumnName="id")
     * @Expose
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $driver;

    /**
     * @var integer
     *
     * @ORM\Column(name="accuracy", type="integer")
     */
    private $accuracy;

    /**
     * @var integer
     *
     * @ORM\Column(name="altitude", type="integer")
     * @Expose
     */
    private $altitude;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=14, scale=8)
     * @Expose
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitute", type="decimal", precision=14, scale=8)
     * @Expose
     */
    private $longitute;

    /**
     * @var string
     *
     * @ORM\Column(name="heading", type="string", length=255)
     * @Expose
     */
    private $heading;

    /**
     * @var string
     *
     * @ORM\Column(name="speed", type="string", length=255)
     * @Expose
     */
    private $speed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_geolocate", type="datetime", nullable=true)
     */
    private $dateGeolocate;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setCreatedAtValue()
    {
        $this->dateGeolocate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Geolocation
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitute
     *
     * @param string $longitute
     * @return Geolocation
     */
    public function setLongitute($longitute)
    {
        $this->longitute = $longitute;

        return $this;
    }

    /**
     * Get longitute
     *
     * @return string
     */
    public function getLongitute()
    {
        return $this->longitute;
    }

    /**
     * Set satellite
     *
     * @param string $satellite
     * @return Geolocation
     */
    public function setSatellite($satellite)
    {
        $this->satellite = $satellite;

        return $this;
    }

    /**
     * Get satellite
     *
     * @return string
     */
    public function getSatellite()
    {
        return $this->satellite;
    }

    /**
     * Set speed
     *
     * @param string $speed
     * @return Geolocation
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return string
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set dateGeolocate
     *
     * @param \DateTime $dateGeolocate
     * @return Geolocation
     */
    public function setDateGeolocate($dateGeolocate)
    {
        $this->dateGeolocate = $dateGeolocate;

        return $this;
    }

    /**
     * Get dateGeolocate
     *
     * @return \DateTime
     */
    public function getDateGeolocate()
    {
        return $this->dateGeolocate;
    }

    /**
     * Set accuracy
     *
     * @param integer $accuracy
     * @return Geolocation
     */
    public function setAccuracy($accuracy)
    {
        $this->accuracy = $accuracy;

        return $this;
    }

    /**
     * Get accuracy
     *
     * @return integer
     */
    public function getAccuracy()
    {
        return $this->accuracy;
    }

    /**
     * Set altitude
     *
     * @param integer $altitude
     * @return Geolocation
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;

        return $this;
    }

    /**
     * Get altitude
     *
     * @return integer
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * Set heading
     *
     * @param string $heading
     * @return Geolocation
     */
    public function setHeading($heading)
    {
        $this->heading = $heading;

        return $this;
    }

    /**
     * Get heading
     *
     * @return string
     */
    public function getHeading()
    {
        return $this->heading;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return Geolocation
     */
    public function setDriver(\CAB\UserBundle\Entity\User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }
}
