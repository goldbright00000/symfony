<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * VehiculeCleanRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VehiculeCleanRepository extends EntityRepository
{
    public function findStatsDriverCleanCount($driver, $valueFrom, $valueTo)
    {

        $q = $this->createQueryBuilder('c')
            ->select('count(DISTINCT c.createdAt)')
            ->where("c.createdAt >= :fromDate")
            ->andWhere("c.createdAt <= :toDate")
            ->andWhere("c.driver = :driver")
            ->setParameter(':fromDate', $valueFrom)
            ->setParameter('toDate', $valueTo)
            ->setParameter('driver', $driver);
        $result = $result = $q->getQuery()->getSingleScalarResult();

        return $result;
    }


}
