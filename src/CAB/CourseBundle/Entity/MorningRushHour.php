<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda
 * Date: 29/11/2015
 * Time: 19:38
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\MorningRushHour
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_morning_rush_hour")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\MorningRushHourRepository")
 */
class MorningRushHour {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Tarif", inversedBy="tarifMorning")
     * @ORM\JoinColumn(name="tarif_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $morningRushTarif;

    /**
     * @var time $mondayFrom
     *
     * @ORM\Column(type="time", name="monday_from", nullable=true)
     */
    private $mondayFrom;

    /**
     * @var time $mondayTo
     *
     * @ORM\Column(type="time", name="monday_to", nullable=true)
     */
    private $mondayTo;

    /**
     * @var decimal $mondayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="monday_morning_increase", nullable=true)
     */
    private $mondayMorningIncrease;

    /**
     * @var time $thursdayFrom
     *
     * @ORM\Column(type="time", name="thursday_from", nullable=true)
     */
    private $thursdayFrom;

    /**
     * @var time $thursdayTo
     *
     * @ORM\Column(type="time", name="thursday_to", nullable=true)
     */
    private $thursdayTo;

    /**
     * @var decimal $thursdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="thursday_morning_increase", nullable=true)
     */
    private $thursdayMorningIncrease;

    /**
     * @var time $wednesdayFrom
     *
     * @ORM\Column(type="time", name="wednesday_from", nullable=true)
     */
    private $wednesdayFrom;

    /**
     * @var time $wednesdayTo
     *
     * @ORM\Column(type="time", name="wednesday_to", nullable=true)
     */
    private $wednesdayTo;

    /**
     * @var decimal $wednesdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="wednesday_morning_increase", nullable=true)
     */
    private $wednesdayMorningIncrease;

    /**
     * @var time $tuesdayFrom
     *
     * @ORM\Column(type="time", name="tuesday_from", nullable=true)
     */
    private $tuesdayFrom;

    /**
     * @var time $tuesdayTo
     *
     * @ORM\Column(type="time", name="tuesday_to", nullable=true)
     */
    private $tuesdayTo;

    /**
     * @var decimal $tuesdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="tuesday_morning_increase", nullable=true)
     */
    private $tuesdayMorningIncrease;

    /**
     * @var time $fridayFrom
     *
     * @ORM\Column(type="time", name="friday_from", nullable=true)
     */
    private $fridayFrom;

    /**
     * @var time $fridayTo
     *
     * @ORM\Column(type="time", name="friday_to", nullable=true)
     */
    private $fridayTo;

    /**
     * @var decimal $fridayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="friday_morning_increase", nullable=true)
     */
    private $fridayMorningIncrease;

    /**
     * @var time $saturdayFrom
     *
     * @ORM\Column(type="time", name="saturday_from", nullable=true)
     */
    private $saturdayrom;

    /**
     * @var time $saturdayTo
     *
     * @ORM\Column(type="time", name="saturday_to", nullable=true)
     */
    private $saturdayTo;

    /**
     * @var decimal $saturdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="saturday_morning_increase", nullable=true)
     */
    private $saturdayMorningIncrease;

    /**
     * @var time $sundayFrom
     *
     * @ORM\Column(type="time", name="sunday_from", nullable=true)
     */
    private $sundayFrom;

    /**
     * @var time $sundayTo
     *
     * @ORM\Column(type="time", name="sunday_to", nullable=true)
     */
    private $sundayTo;

    /**
     * @var decimal $sundayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="sunday_morning_increase", nullable=true)
     */
    private $sundayMorningIncrease;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mondayFrom
     *
     * @param \DateTime $mondayFrom
     * @return MorningRushHour
     */
    public function setMondayFrom($mondayFrom)
    {
        $this->mondayFrom = $mondayFrom;

        return $this;
    }

    /**
     * Get mondayFrom
     *
     * @return \DateTime
     */
    public function getMondayFrom()
    {
        return $this->mondayFrom;
    }

    /**
     * Set mondayTo
     *
     * @param \DateTime $mondayTo
     * @return MorningRushHour
     */
    public function setMondayTo($mondayTo)
    {
        $this->mondayTo = $mondayTo;

        return $this;
    }

    /**
     * Get mondayTo
     *
     * @return \DateTime
     */
    public function getMondayTo()
    {
        return $this->mondayTo;
    }

    /**
     * Set mondayMorningIncrease
     *
     * @param string $mondayMorningIncrease
     * @return MorningRushHour
     */
    public function setMondayMorningIncrease($mondayMorningIncrease)
    {
        $this->mondayMorningIncrease = $mondayMorningIncrease;

        return $this;
    }

    /**
     * Get mondayMorningIncrease
     *
     * @return string
     */
    public function getMondayMorningIncrease()
    {
        return $this->mondayMorningIncrease;
    }

    /**
     * Set thursdayFrom
     *
     * @param \DateTime $thursdayFrom
     * @return MorningRushHour
     */
    public function setThursdayFrom($thursdayFrom)
    {
        $this->thursdayFrom = $thursdayFrom;

        return $this;
    }

    /**
     * Get thursdayFrom
     *
     * @return \DateTime
     */
    public function getThursdayFrom()
    {
        return $this->thursdayFrom;
    }

    /**
     * Set thursdayTo
     *
     * @param \DateTime $thursdayTo
     * @return MorningRushHour
     */
    public function setThursdayTo($thursdayTo)
    {
        $this->thursdayTo = $thursdayTo;

        return $this;
    }

    /**
     * Get thursdayTo
     *
     * @return \DateTime
     */
    public function getThursdayTo()
    {
        return $this->thursdayTo;
    }

    /**
     * Set thursdayMorningIncrease
     *
     * @param string $thursdayMorningIncrease
     * @return MorningRushHour
     */
    public function setThursdayMorningIncrease($thursdayMorningIncrease)
    {
        $this->thursdayMorningIncrease = $thursdayMorningIncrease;

        return $this;
    }

    /**
     * Get thursdayMorningIncrease
     *
     * @return string
     */
    public function getThursdayMorningIncrease()
    {
        return $this->thursdayMorningIncrease;
    }

    /**
     * Set wednesdayFrom
     *
     * @param \DateTime $wednesdayFrom
     * @return MorningRushHour
     */
    public function setWednesdayFrom($wednesdayFrom)
    {
        $this->wednesdayFrom = $wednesdayFrom;

        return $this;
    }

    /**
     * Get wednesdayFrom
     *
     * @return \DateTime
     */
    public function getWednesdayFrom()
    {
        return $this->wednesdayFrom;
    }

    /**
     * Set wednesdayTo
     *
     * @param \DateTime $wednesdayTo
     * @return MorningRushHour
     */
    public function setWednesdayTo($wednesdayTo)
    {
        $this->wednesdayTo = $wednesdayTo;

        return $this;
    }

    /**
     * Get wednesdayTo
     *
     * @return \DateTime
     */
    public function getWednesdayTo()
    {
        return $this->wednesdayTo;
    }

    /**
     * Set wednesdayMorningIncrease
     *
     * @param string $wednesdayMorningIncrease
     * @return MorningRushHour
     */
    public function setWednesdayMorningIncrease($wednesdayMorningIncrease)
    {
        $this->wednesdayMorningIncrease = $wednesdayMorningIncrease;

        return $this;
    }

    /**
     * Get wednesdayMorningIncrease
     *
     * @return string
     */
    public function getWednesdayMorningIncrease()
    {
        return $this->wednesdayMorningIncrease;
    }

    /**
     * Set tuesdayFrom
     *
     * @param \DateTime $tuesdayFrom
     * @return MorningRushHour
     */
    public function setTuesdayFrom($tuesdayFrom)
    {
        $this->tuesdayFrom = $tuesdayFrom;

        return $this;
    }

    /**
     * Get tuesdayFrom
     *
     * @return \DateTime
     */
    public function getTuesdayFrom()
    {
        return $this->tuesdayFrom;
    }

    /**
     * Set tuesdayTo
     *
     * @param \DateTime $tuesdayTo
     * @return MorningRushHour
     */
    public function setTuesdayTo($tuesdayTo)
    {
        $this->tuesdayTo = $tuesdayTo;

        return $this;
    }

    /**
     * Get tuesdayTo
     *
     * @return \DateTime
     */
    public function getTuesdayTo()
    {
        return $this->tuesdayTo;
    }

    /**
     * Set tuesdayMorningIncrease
     *
     * @param string $tuesdayMorningIncrease
     * @return MorningRushHour
     */
    public function setTuesdayMorningIncrease($tuesdayMorningIncrease)
    {
        $this->tuesdayMorningIncrease = $tuesdayMorningIncrease;

        return $this;
    }

    /**
     * Get tuesdayMorningIncrease
     *
     * @return string
     */
    public function getTuesdayMorningIncrease()
    {
        return $this->tuesdayMorningIncrease;
    }

    /**
     * Set fridayFrom
     *
     * @param \DateTime $fridayFrom
     * @return MorningRushHour
     */
    public function setFridayFrom($fridayFrom)
    {
        $this->fridayFrom = $fridayFrom;

        return $this;
    }

    /**
     * Get fridayFrom
     *
     * @return \DateTime
     */
    public function getFridayFrom()
    {
        return $this->fridayFrom;
    }

    /**
     * Set fridayTo
     *
     * @param \DateTime $fridayTo
     * @return MorningRushHour
     */
    public function setFridayTo($fridayTo)
    {
        $this->fridayTo = $fridayTo;

        return $this;
    }

    /**
     * Get fridayTo
     *
     * @return \DateTime
     */
    public function getFridayTo()
    {
        return $this->fridayTo;
    }

    /**
     * Set fridayMorningIncrease
     *
     * @param string $fridayMorningIncrease
     * @return MorningRushHour
     */
    public function setFridayMorningIncrease($fridayMorningIncrease)
    {
        $this->fridayMorningIncrease = $fridayMorningIncrease;

        return $this;
    }

    /**
     * Get fridayMorningIncrease
     *
     * @return string
     */
    public function getFridayMorningIncrease()
    {
        return $this->fridayMorningIncrease;
    }

    /**
     * Set saturdayrom
     *
     * @param \DateTime $saturdayrom
     * @return MorningRushHour
     */
    public function setSaturdayrom($saturdayrom)
    {
        $this->saturdayrom = $saturdayrom;

        return $this;
    }

    /**
     * Get saturdayrom
     *
     * @return \DateTime
     */
    public function getSaturdayrom()
    {
        return $this->saturdayrom;
    }

    /**
     * Set saturdayTo
     *
     * @param \DateTime $saturdayTo
     * @return MorningRushHour
     */
    public function setSaturdayTo($saturdayTo)
    {
        $this->saturdayTo = $saturdayTo;

        return $this;
    }

    /**
     * Get saturdayTo
     *
     * @return \DateTime
     */
    public function getSaturdayTo()
    {
        return $this->saturdayTo;
    }

    /**
     * Set saturdayMorningIncrease
     *
     * @param string $saturdayMorningIncrease
     * @return MorningRushHour
     */
    public function setSaturdayMorningIncrease($saturdayMorningIncrease)
    {
        $this->saturdayMorningIncrease = $saturdayMorningIncrease;

        return $this;
    }

    /**
     * Get saturdayMorningIncrease
     *
     * @return string
     */
    public function getSaturdayMorningIncrease()
    {
        return $this->saturdayMorningIncrease;
    }

    /**
     * Set sundayFrom
     *
     * @param \DateTime $sundayFrom
     * @return MorningRushHour
     */
    public function setSundayFrom($sundayFrom)
    {
        $this->sundayFrom = $sundayFrom;

        return $this;
    }

    /**
     * Get sundayFrom
     *
     * @return \DateTime
     */
    public function getSundayFrom()
    {
        return $this->sundayFrom;
    }

    /**
     * Set sundayTo
     *
     * @param \DateTime $sundayTo
     * @return MorningRushHour
     */
    public function setSundayTo($sundayTo)
    {
        $this->sundayTo = $sundayTo;

        return $this;
    }

    /**
     * Get sundayTo
     *
     * @return \DateTime
     */
    public function getSundayTo()
    {
        return $this->sundayTo;
    }

    /**
     * Set sundayMorningIncrease
     *
     * @param string $sundayMorningIncrease
     * @return MorningRushHour
     */
    public function setSundayMorningIncrease($sundayMorningIncrease)
    {
        $this->sundayMorningIncrease = $sundayMorningIncrease;

        return $this;
    }

    /**
     * Get sundayMorningIncrease
     *
     * @return string
     */
    public function getSundayMorningIncrease()
    {
        return $this->sundayMorningIncrease;
    }

    /**
     * Set morningRushTarif
     *
     * @param \CAB\CourseBundle\Entity\Tarif $morningRushTarif
     * @return MorningRushHour
     */
    public function setMorningRushTarif(\CAB\CourseBundle\Entity\Tarif $morningRushTarif = null)
    {
        $this->morningRushTarif = $morningRushTarif;

        return $this;
    }

    /**
     * Get morningRushTarif
     *
     * @return \CAB\CourseBundle\Entity\Tarif
     */
    public function getMorningRushTarif()
    {
        return $this->morningRushTarif;
    }
}
