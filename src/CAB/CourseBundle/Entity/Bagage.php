<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 21/08/2015
 * Time: 09:17
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\Bagage
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_bagage")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\BgageRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Bagage
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $bagageName
     *
     * @ORM\Column(name="bagage_name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.bagageName")
     */
    protected $bagageName;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @var decimal $price
     *
     * @ORM\Column(name="price", type="decimal", precision=2, scale=2, nullable=false)
     */
    protected $price;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime('0000-00-00');
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bagageName
     *
     * @param string $bagageName
     * @return Bagage
     */
    public function setBagageName($bagageName)
    {
        $this->bagageName = $bagageName;

        return $this;
    }

    /**
     * Get bagageName
     *
     * @return string
     */
    public function getBagageName()
    {
        return $this->bagageName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Bagage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Bagage
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Bagage
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Bagage
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
