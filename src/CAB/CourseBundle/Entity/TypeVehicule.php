<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/08/2015
 * Time: 18:03
 */

namespace CAB\CourseBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\TypeVehicule
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_type_vehicule")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\TypeVehiculeRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class TypeVehicule {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $nameType
     *
     * @ORM\Column(name="name_type", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "not.blank.name_type")
     */
    protected $nameType;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Vehicule", mappedBy="typeVehicule")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicules;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Forfait", mappedBy="vehicleType", cascade={"all"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $forfait;

    /**
     * @ORM\OneToOne(targetEntity="\CAB\CourseBundle\Entity\Tarif", inversedBy="typeVehiculeTarif", cascade={"remove", "persist"})
     * @ORM\JoinColumn(name="tarif_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $tarifTypeVehicle;

    /**
     * @ORM\ManyToMany(targetEntity="\CAB\MainBundle\Entity\Partner", mappedBy="vehiculesType")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $partners;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Course", mappedBy="typeVehicleCourse")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $courses;

    public function __toString() {
        return $this->getNameType() . ' ('.$this->getTarifTypeVehicle()->getCompany()->getCompanyName().')';
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->vehicules = new ArrayCollection();
        $this->partners = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nameType
     *
     * @param string $nameType
     * @return TypeVehicule
     */
    public function setNameType($nameType) {
        $this->nameType = $nameType;

        return $this;
    }

    /**
     * Get nameType
     *
     * @return string
     */
    public function getNameType() {
        return $this->nameType;
    }

    /**
     * Add vehicules
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicules
     * @return TypeVehicule
     */
    public function addVehicule(\CAB\CourseBundle\Entity\Vehicule $vehicules) {
        $this->vehicules[] = $vehicules;

        return $this;
    }

    /**
     * Remove vehicules
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $vehicules
     */
    public function removeVehicule(\CAB\CourseBundle\Entity\Vehicule $vehicules) {
        $this->vehicules->removeElement($vehicules);
    }

    /**
     * Get vehicules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicules() {
        return $this->vehicules;
    }

    /**
     * Add partners
     *
     * @param \CAB\MainBundle\Entity\Partner $partners
     * @return TypeVehicule
     */
    public function addPartner(\CAB\MainBundle\Entity\Partner $partners) {
        $this->partners[] = $partners;

        return $this;
    }

    /**
     * Remove partners
     *
     * @param \CAB\MainBundle\Entity\Partner $partners
     */
    public function removePartner(\CAB\MainBundle\Entity\Partner $partners) {
        $this->partners->removeElement($partners);
    }

    /**
     * Get partners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartners() {
        return $this->partners;
    }

    /**
     * Add courses
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $courses
     * @return TypeVehicule
     */
    public function addCourse(\CAB\CourseBundle\Entity\Vehicule $courses) {
        $this->courses[] = $courses;

        return $this;
    }

    /**
     * Remove courses
     *
     * @param \CAB\CourseBundle\Entity\Vehicule $courses
     */
    public function removeCourse(\CAB\CourseBundle\Entity\Vehicule $courses) {
        $this->courses->removeElement($courses);
    }

    /**
     * Get courses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourses() {
        return $this->courses;
    }

    /**
     * Set tarifTypeVehicle
     *
     * @param \CAB\CourseBundle\Entity\Tarif $tarifTypeVehicle
     * @return TypeVehicule
     */
    public function setTarifTypeVehicle(\CAB\CourseBundle\Entity\Tarif $tarifTypeVehicle = null)
    {
        $this->tarifTypeVehicle = $tarifTypeVehicle;

        return $this;
    }

    /**
     * Get tarifTypeVehicle
     *
     * @return \CAB\CourseBundle\Entity\Tarif
     */
    public function getTarifTypeVehicle()
    {
        return $this->tarifTypeVehicle;
    }

    /**
     * Add forfait
     *
     * @param \CAB\CourseBundle\Entity\Forfait $forfait
     * @return TypeVehicule
     */
    public function addForfait(\CAB\CourseBundle\Entity\Forfait $forfait)
    {
        $this->forfait[] = $forfait;

        return $this;
    }

    /**
     * Remove forfait
     *
     * @param \CAB\CourseBundle\Entity\Forfait $forfait
     */
    public function removeForfait(\CAB\CourseBundle\Entity\Forfait $forfait)
    {
        $this->forfait->removeElement($forfait);
    }

    /**
     * Get forfait
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForfait()
    {
        return $this->forfait;
    }
}
