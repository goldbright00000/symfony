<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\Tarif
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_tarif")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\TarifRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Tarif {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="companyTarif")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * @ORM\OneToOne(targetEntity="\CAB\CourseBundle\Entity\TypeVehicule", mappedBy="tarifTypeVehicle", cascade={"persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $typeVehiculeTarif;

    /**
     * @ORM\OneToMany(targetEntity="\CAB\CourseBundle\Entity\Course", mappedBy="applicableTarif", cascade={"remove", "persist"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $coursesTarif;

    /**
     * @var time $mondayMorningFrom
     *
     * @ORM\Column(type="time", name="monday_morning_from", nullable=true)
     */
    private $mondayMorningFrom;

    /**
     * @var time $mondayMorningTo
     *
     * @ORM\Column(type="time", name="monday_morning_to", nullable=true)
     */
    private $mondayMorningTo;

    /**
     * @var decimal $mondayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="monday_morning_increase", nullable=true)
     */
    private $mondayMorningIncrease;

    /**
     * @var time $thursdayMorningFrom
     *
     * @ORM\Column(type="time", name="thursday_morning_from", nullable=true)
     */
    private $thursdayMorningFrom;

    /**
     * @var time $thursdayMorningTo
     *
     * @ORM\Column(type="time", name="thursday_morning_to", nullable=true)
     */
    private $thursdayMorningTo;

    /**
     * @var decimal $thursdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="thursday_morning_increase", nullable=true)
     */
    private $thursdayMorningIncrease;

    /**
     * @var time $wednesdayMorningFrom
     *
     * @ORM\Column(type="time", name="wednesday_morning_from", nullable=true)
     */
    private $wednesdayMorningFrom;

    /**
     * @var time $wednesdayMorningTo
     *
     * @ORM\Column(type="time", name="wednesday_morning_to", nullable=true)
     */
    private $wednesdayMorningTo;

    /**
     * @var decimal $wednesdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="wednesday_morning_increase", nullable=true)
     */
    private $wednesdayMorningIncrease;

    /**
     * @var time $tuesdayMorningFrom
     *
     * @ORM\Column(type="time", name="tuesday_morning_from", nullable=true)
     */
    private $tuesdayMorningFrom;

    /**
     * @var time $tuesdayMorningTo
     *
     * @ORM\Column(type="time", name="tuesday_morning_to", nullable=true)
     */
    private $tuesdayMorningTo;

    /**
     * @var decimal $tuesdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="tuesday_morning_increase", nullable=true)
     */
    private $tuesdayMorningIncrease;

    /**
     * @var time $fridayMorningFrom
     *
     * @ORM\Column(type="time", name="friday_morning_from", nullable=true)
     */
    private $fridayMorningFrom;

    /**
     * @var time $fridayMorningTo
     *
     * @ORM\Column(type="time", name="friday_morning_to", nullable=true)
     */
    private $fridayMorningTo;

    /**
     * @var decimal $fridayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="friday_morning_increase", nullable=true)
     */
    private $fridayMorningIncrease;

    /**
     * @var time $saturdayMorningFrom
     *
     * @ORM\Column(type="time", name="saturday_morning_from", nullable=true)
     */
    private $saturdayMorningFrom;

    /**
     * @var time $saturdayMorningTo
     *
     * @ORM\Column(type="time", name="saturday_morning_to", nullable=true)
     */
    private $saturdayMorningTo;

    /**
     * @var decimal $saturdayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="saturday_morning_increase", nullable=true)
     */
    private $saturdayMorningIncrease;

    /**
     * @var time $sundayMorningFrom
     *
     * @ORM\Column(type="time", name="sunday_morning_from", nullable=true)
     */
    private $sundayMorningFrom;

    /**
     * @var time $sundayMorningTo
     *
     * @ORM\Column(type="time", name="sunday_morning_to", nullable=true)
     */
    private $sundayMorningTo;

    /**
     * @var decimal $sundayMorningIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="sunday_morning_increase", nullable=true)
     */
    private $sundayMorningIncrease;

    /**
     * @var time $mondayNightFrom
     *
     * @ORM\Column(type="time", name="monday_night_from", nullable=true)
     */
    private $mondayNightFrom;

    /**
     * @var time $mondayNightTo
     *
     * @ORM\Column(type="time", name="monday_night_to", nullable=true)
     */
    private $mondayNightTo;

    /**
     * @var decimal $mondayNightIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="monday_night_increase", nullable=true)
     */
    private $mondayNightIncrease;

    /**
     * @var time $thursdayNightFrom
     *
     * @ORM\Column(type="time", name="thursday_night_from", nullable=true)
     */
    private $thursdayNightFrom;

    /**
     * @var time $thursdayNightTo
     *
     * @ORM\Column(type="time", name="thursday_night_to", nullable=true)
     */
    private $thursdayNightTo;

    /**
     * @var decimal $thursdayNightIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="thursday_night_increase", nullable=true)
     */
    private $thursdayNightIncrease;

    /**
     * @var time $wednesdayNightFrom
     *
     * @ORM\Column(type="time", name="wednesday_night_from", nullable=true)
     */
    private $wednesdayNightFrom;

    /**
     * @var time $wednesdayNightTo
     *
     * @ORM\Column(type="time", name="wednesday_night_to", nullable=true)
     */
    private $wednesdayNightTo;

    /**
     * @var decimal $wednesdayNightIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="wednesday_night_increase", nullable=true)
     */
    private $wednesdayNightIncrease;

    /**
     * @var time $tuesdayNightFrom
     *
     * @ORM\Column(type="time", name="tuesday_night_from", nullable=true)
     */
    private $tuesdayNightFrom;

    /**
     * @var time $tuesdayNightTo
     *
     * @ORM\Column(type="time", name="tuesday_night_to", nullable=true)
     */
    private $tuesdayNightTo;

    /**
     * @var decimal $tuesdayNightIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="tuesday_night_increase", nullable=true)
     */
    private $tuesdayNightIncrease;

    /**
     * @var time $fridayNightFrom
     *
     * @ORM\Column(type="time", name="friday_night_from", nullable=true)
     */
    private $fridayNightFrom;

    /**
     * @var time $fridayNightTo
     *
     * @ORM\Column(type="time", name="friday_night_to", nullable=true)
     */
    private $fridayNightTo;

    /**
     * @var decimal $fridayNightIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="friday_night_increase", nullable=true)
     */
    private $fridayNightIncrease;

    /**
     * @var time $saturdayNightFrom
     *
     * @ORM\Column(type="time", name="saturday_night_from", nullable=true)
     */
    private $saturdayNightFrom;

    /**
     * @var time $saturdayNightTo
     *
     * @ORM\Column(type="time", name="saturday_night_to", nullable=true)
     */
    private $saturdayNightTo;

    /**
     * @var decimal $saturdayNightIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="saturday_night_increase", nullable=true)
     */
    private $saturdayNightIncrease;

    /**
     * @var time $sundayNightFrom
     *
     * @ORM\Column(type="time", name="sunday_night_from", nullable=true)
     */
    private $sundayNightFrom;

    /**
     * @var time $sundayNightTo
     *
     * @ORM\Column(type="time", name="sunday_night_to", nullable=true)
     */
    private $sundayNightTo;

    /**
     * @var decimal $sundayNightIncrease
     *
     * @ORM\Column(type="decimal", precision=4, scale=2, name="sunday_night_increase", nullable=true)
     */
    private $sundayNightIncrease;
    /**
     * @var integer $tarifType
     *
     * @ORM\Column(name="tarif_type", type="string", length=255, nullable=false)
     */
    private $tarifType;

    /**************************************
     * ################################## *
     * ########### Tarif taxi ########### *
     * ################################## *
     *************************************/

    /**
     * @var decimal $tarifPC
     *
     * @ORM\Column(name="tarif_pc", type="decimal", precision=6, scale=2, nullable=true)
     */
    protected $tarifPC;

    /**
     * @var decimal $rateA
     *
     * @ORM\Column(name="tarif_a", type="decimal", precision=6, scale=2, nullable=true)
     */
    protected $tarifA;

    /**
     * @var decimal $rateB
     *
     * @ORM\Column(name="tarif_b", type="decimal", precision=6, scale=2, nullable=true)
     */
    protected $tarifB;

    /**
     * @var decimal $rateC
     *
     * @ORM\Column(name="tarif_c", type="decimal", precision=6, scale=2, nullable=true)
     */
    protected $tarifC;

    /**
     * @var decimal $rateD
     *
     * @ORM\Column(name="tarif_d", type="decimal", precision=6, scale=2, nullable=true)
     */
    protected $tarifD;

    /**
     * @var decimal $slowWalkPerHour
     *
     * @ORM\Column(name="slow_walk_per_hour", type="decimal", precision=6, scale=2, nullable=true)
     */
    protected $slowWalkPerHour;

    /**
     * @var integer $luggage
     *
     * @ORM\Column(name="luggage", type="integer", length=100, nullable=true)
     */
    protected $luggage;

    /**
     * For 4 persons
     * @var string $person
     *
     * @ORM\Column(name="person", type="string", length=100, nullable=true)
     */
    protected $person;

    /**
     * @var decimal $minPrice
     *
     * @ORM\Column(name="min_price", type="decimal", precision=4, scale=2, nullable=true)
     */
    protected $minPrice;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\HourTarifNight", inversedBy="tarif", fetch="EAGER")
     * @ORM\JoinColumn(name="hour_night_id", referencedColumnName="id")
     */
    protected $hourNightTarif;

    /**
     * @var string $packageApproach
     *
     * @ORM\Column(name="package_approach", type="string", length=20, nullable=true)
     */
    protected $packageApproach;

    //morning rush hour
    /**************************************
     * ################################## *
     * ###### Tarif au km parcourus ##### *
     * ################################## *
     *************************************/

    /**************************************
     * ################################## *
     * # Tarif au forfait kilométriques # *
     * ################################## *
     *************************************/

    /**
     * @var decimal $tarifEquipment
     *
     * @ORM\Column(name="tarif_equipment", type="decimal", precision=4, scale=2, nullable=true)
     */
    protected $tarifEquipment;

    /**
     * @var decimal $tarifHandicapWheelchair
     *
     * @ORM\Column(name="tarif_handicap_wheelchair", type="decimal", precision=4, scale=2, nullable=true)
     */
    protected $tarifHandicapWheelchair;

    /**
     * @var decimal $tarifBabySeat
     *
     * @ORM\Column(name="tarif_baby_seat", type="decimal", precision=4, scale=2, nullable=true)
     */
    protected $tarifBabySeat;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Tarif
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Tarif
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set TarifFrom
     *
     * @param integer $tarifFrom
     * @return Tarif
     */
    public function setTarifFrom($tarifFrom)
    {
        $this->tarifFrom = $tarifFrom;

        return $this;
    }

    /**
     * Get TarifFrom
     *
     * @return integer
     */
    public function getTarifFrom()
    {
        return $this->tarifFrom;
    }

    /**
     * Set TarifTo
     *
     * @param integer $tarifTo
     * @return Tarif
     */
    public function setTarifTo($tarifTo)
    {
        $this->tarifTo = $tarifTo;

        return $this;
    }

    /**
     * Get TarifTo
     *
     * @return integer
     */
    public function getTarifTo()
    {
        return $this->tarifTo;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return Tarif
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set tarifType
     *
     * @param string $tarifType
     * @return Tarif
     */
    public function setTarifType($tarifType)
    {
        $this->tarifType = $tarifType;

        return $this;
    }

    /**
     * Get tarifType
     *
     * @return string
     */
    public function getTarifType()
    {
        return $this->tarifType;
    }
    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Set tarifPC
     *
     * @param float $tarifPC
     * @return Tarif
     */
    public function setTarifPC($tarifPC)
    {
        $this->tarifPC = $tarifPC;

        return $this;
    }

    /**
     * Get tarifPC
     *
     * @return float
     */
    public function getTarifPC()
    {
        return $this->tarifPC;
    }

    /**
     * Set tarifA
     *
     * @param string $tarifA
     * @return Tarif
     */
    public function setTarifA($tarifA)
    {
        $this->tarifA = $tarifA;

        return $this;
    }

    /**
     * Get tarifA
     *
     * @return string
     */
    public function getTarifA()
    {
        return $this->tarifA;
    }

    /**
     * Set tarifB
     *
     * @param float $tarifB
     * @return Tarif
     */
    public function setTarifB($tarifB)
    {
        $this->tarifB = $tarifB;

        return $this;
    }

    /**
     * Get tarifB
     *
     * @return float
     */
    public function getTarifB()
    {
        return $this->tarifB;
    }

    /**
     * Set tarifC
     *
     * @param float $tarifC
     * @return Tarif
     */
    public function setTarifC($tarifC)
    {
        $this->tarifC = $tarifC;

        return $this;
    }

    /**
     * Get tarifC
     *
     * @return string
     */
    public function getTarifC()
    {
        return $this->tarifC;
    }

    /**
     * Set tarifD
     *
     * @param float $tarifD
     * @return Tarif
     */
    public function setTarifD($tarifD)
    {
        $this->tarifD = $tarifD;

        return $this;
    }

    /**
     * Get tarifD
     *
     * @return float
     */
    public function getTarifD()
    {
        return $this->tarifD;
    }

    /**
     * Set slowWalkPerHour
     *
     * @param string $slowWalkPerHour
     * @return Tarif
     */
    public function setSlowWalkPerHour($slowWalkPerHour)
    {
        $this->slowWalkPerHour = $slowWalkPerHour;

        return $this;
    }

    /**
     * Get slowWalkPerHour
     *
     * @return string
     */
    public function getSlowWalkPerHour()
    {
        return $this->slowWalkPerHour;
    }

    /**
     * Set packageApproach
     *
     * @param string $packageApproach
     * @return Tarif
     */
    public function setPackageApproach($packageApproach)
    {
        $this->packageApproach = $packageApproach;

        return $this;
    }

    /**
     * Get packageApproach
     *
     * @return string
     */
    public function getPackageApproach()
    {
        return $this->packageApproach;
    }

    /**
     * Set person
     *
     * @param string $person
     * @return Tarif
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return string
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set minPrice
     *
     * @param string $minPrice
     * @return Tarif
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /**
     * Get minPrice
     *
     * @return string
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * Set luggage
     *
     * @param integer $luggage
     * @return Tarif
     */
    public function setLuggage($luggage)
    {
        $this->luggage = $luggage;

        return $this;
    }

    /**
     * Get luggage
     *
     * @return integer
     */
    public function getLuggage()
    {
        return $this->luggage;
    }

    /**
     * Set mondayMorningFrom
     *
     * @param \DateTime $mondayMorningFrom
     * @return Tarif
     */
    public function setMondayMorningFrom($mondayMorningFrom)
    {
        $this->mondayMorningFrom = $mondayMorningFrom;

        return $this;
    }

    /**
     * Get mondayMorningFrom
     *
     * @return \DateTime
     */
    public function getMondayMorningFrom()
    {
        return $this->mondayMorningFrom;
    }

    /**
     * Set mondayMorningTo
     *
     * @param \DateTime $mondayMorningTo
     * @return Tarif
     */
    public function setMondayMorningTo($mondayMorningTo)
    {
        $this->mondayMorningTo = $mondayMorningTo;

        return $this;
    }

    /**
     * Get mondayMorningTo
     *
     * @return \DateTime
     */
    public function getMondayMorningTo()
    {
        return $this->mondayMorningTo;
    }

    /**
     * Set mondayMorningIncrease
     *
     * @param string $mondayMorningIncrease
     * @return Tarif
     */
    public function setMondayMorningIncrease($mondayMorningIncrease)
    {
        $this->mondayMorningIncrease = $mondayMorningIncrease;

        return $this;
    }

    /**
     * Get mondayMorningIncrease
     *
     * @return string
     */
    public function getMondayMorningIncrease()
    {
        return $this->mondayMorningIncrease;
    }

    /**
     * Set thursdayMorningFrom
     *
     * @param \DateTime $thursdayMorningFrom
     * @return Tarif
     */
    public function setThursdayMorningFrom($thursdayMorningFrom)
    {
        $this->thursdayMorningFrom = $thursdayMorningFrom;

        return $this;
    }

    /**
     * Get thursdayMorningFrom
     *
     * @return \DateTime
     */
    public function getThursdayMorningFrom()
    {
        return $this->thursdayMorningFrom;
    }

    /**
     * Set thursdayMorningTo
     *
     * @param \DateTime $thursdayMorningTo
     * @return Tarif
     */
    public function setThursdayMorningTo($thursdayMorningTo)
    {
        $this->thursdayMorningTo = $thursdayMorningTo;

        return $this;
    }

    /**
     * Get thursdayMorningTo
     *
     * @return \DateTime
     */
    public function getThursdayMorningTo()
    {
        return $this->thursdayMorningTo;
    }

    /**
     * Set thursdayMorningIncrease
     *
     * @param string $thursdayMorningIncrease
     * @return Tarif
     */
    public function setThursdayMorningIncrease($thursdayMorningIncrease)
    {
        $this->thursdayMorningIncrease = $thursdayMorningIncrease;

        return $this;
    }

    /**
     * Get thursdayMorningIncrease
     *
     * @return string
     */
    public function getThursdayMorningIncrease()
    {
        return $this->thursdayMorningIncrease;
    }

    /**
     * Set wednesdayMorningFrom
     *
     * @param \DateTime $wednesdayMorningFrom
     * @return Tarif
     */
    public function setWednesdayMorningFrom($wednesdayMorningFrom)
    {
        $this->wednesdayMorningFrom = $wednesdayMorningFrom;

        return $this;
    }

    /**
     * Get wednesdayMorningFrom
     *
     * @return \DateTime
     */
    public function getWednesdayMorningFrom()
    {
        return $this->wednesdayMorningFrom;
    }

    /**
     * Set wednesdayMorningTo
     *
     * @param \DateTime $wednesdayMorningTo
     * @return Tarif
     */
    public function setWednesdayMorningTo($wednesdayMorningTo)
    {
        $this->wednesdayMorningTo = $wednesdayMorningTo;

        return $this;
    }

    /**
     * Get wednesdayMorningTo
     *
     * @return \DateTime
     */
    public function getWednesdayMorningTo()
    {
        return $this->wednesdayMorningTo;
    }

    /**
     * Set wednesdayMorningIncrease
     *
     * @param string $wednesdayMorningIncrease
     * @return Tarif
     */
    public function setWednesdayMorningIncrease($wednesdayMorningIncrease)
    {
        $this->wednesdayMorningIncrease = $wednesdayMorningIncrease;

        return $this;
    }

    /**
     * Get wednesdayMorningIncrease
     *
     * @return string
     */
    public function getWednesdayMorningIncrease()
    {
        return $this->wednesdayMorningIncrease;
    }

    /**
     * Set tuesdayMorningFrom
     *
     * @param \DateTime $tuesdayMorningFrom
     * @return Tarif
     */
    public function setTuesdayMorningFrom($tuesdayMorningFrom)
    {
        $this->tuesdayMorningFrom = $tuesdayMorningFrom;

        return $this;
    }

    /**
     * Get tuesdayMorningFrom
     *
     * @return \DateTime
     */
    public function getTuesdayMorningFrom()
    {
        return $this->tuesdayMorningFrom;
    }

    /**
     * Set tuesdayMorningTo
     *
     * @param \DateTime $tuesdayMorningTo
     * @return Tarif
     */
    public function setTuesdayMorningTo($tuesdayMorningTo)
    {
        $this->tuesdayMorningTo = $tuesdayMorningTo;

        return $this;
    }

    /**
     * Get tuesdayMorningTo
     *
     * @return \DateTime
     */
    public function getTuesdayMorningTo()
    {
        return $this->tuesdayMorningTo;
    }

    /**
     * Set tuesdayMorningIncrease
     *
     * @param string $tuesdayMorningIncrease
     * @return Tarif
     */
    public function setTuesdayMorningIncrease($tuesdayMorningIncrease)
    {
        $this->tuesdayMorningIncrease = $tuesdayMorningIncrease;

        return $this;
    }

    /**
     * Get tuesdayMorningIncrease
     *
     * @return string
     */
    public function getTuesdayMorningIncrease()
    {
        return $this->tuesdayMorningIncrease;
    }

    /**
     * Set fridayMorningFrom
     *
     * @param \DateTime $fridayMorningFrom
     * @return Tarif
     */
    public function setFridayMorningFrom($fridayMorningFrom)
    {
        $this->fridayMorningFrom = $fridayMorningFrom;

        return $this;
    }

    /**
     * Get fridayMorningFrom
     *
     * @return \DateTime
     */
    public function getFridayMorningFrom()
    {
        return $this->fridayMorningFrom;
    }

    /**
     * Set fridayMorningTo
     *
     * @param \DateTime $fridayMorningTo
     * @return Tarif
     */
    public function setFridayMorningTo($fridayMorningTo)
    {
        $this->fridayMorningTo = $fridayMorningTo;

        return $this;
    }

    /**
     * Get fridayMorningTo
     *
     * @return \DateTime
     */
    public function getFridayMorningTo()
    {
        return $this->fridayMorningTo;
    }

    /**
     * Set fridayMorningIncrease
     *
     * @param string $fridayMorningIncrease
     * @return Tarif
     */
    public function setFridayMorningIncrease($fridayMorningIncrease)
    {
        $this->fridayMorningIncrease = $fridayMorningIncrease;

        return $this;
    }

    /**
     * Get fridayMorningIncrease
     *
     * @return string
     */
    public function getFridayMorningIncrease()
    {
        return $this->fridayMorningIncrease;
    }

    /**
     * Set saturdayMorningFrom
     *
     * @param \DateTime $saturdayMorningFrom
     * @return Tarif
     */
    public function setSaturdayMorningFrom($saturdayMorningFrom)
    {
        $this->saturdayMorningFrom = $saturdayMorningFrom;

        return $this;
    }

    /**
     * Get saturdayMorningFrom
     *
     * @return \DateTime
     */
    public function getSaturdayMorningFrom()
    {
        return $this->saturdayMorningFrom;
    }

    /**
     * Set saturdayMorningTo
     *
     * @param \DateTime $saturdayMorningTo
     * @return Tarif
     */
    public function setSaturdayMorningTo($saturdayMorningTo)
    {
        $this->saturdayMorningTo = $saturdayMorningTo;

        return $this;
    }

    /**
     * Get saturdayMorningTo
     *
     * @return \DateTime
     */
    public function getSaturdayMorningTo()
    {
        return $this->saturdayMorningTo;
    }

    /**
     * Set saturdayMorningIncrease
     *
     * @param string $saturdayMorningIncrease
     * @return Tarif
     */
    public function setSaturdayMorningIncrease($saturdayMorningIncrease)
    {
        $this->saturdayMorningIncrease = $saturdayMorningIncrease;

        return $this;
    }

    /**
     * Get saturdayMorningIncrease
     *
     * @return string
     */
    public function getSaturdayMorningIncrease()
    {
        return $this->saturdayMorningIncrease;
    }

    /**
     * Set sundayMorningFrom
     *
     * @param \DateTime $sundayMorningFrom
     * @return Tarif
     */
    public function setSundayMorningFrom($sundayMorningFrom)
    {
        $this->sundayMorningFrom = $sundayMorningFrom;

        return $this;
    }

    /**
     * Get sundayMorningFrom
     *
     * @return \DateTime
     */
    public function getSundayMorningFrom()
    {
        return $this->sundayMorningFrom;
    }

    /**
     * Set sundayMorningTo
     *
     * @param \DateTime $sundayMorningTo
     * @return Tarif
     */
    public function setSundayMorningTo($sundayMorningTo)
    {
        $this->sundayMorningTo = $sundayMorningTo;

        return $this;
    }

    /**
     * Get sundayMorningTo
     *
     * @return \DateTime
     */
    public function getSundayMorningTo()
    {
        return $this->sundayMorningTo;
    }

    /**
     * Set sundayMorningIncrease
     *
     * @param string $sundayMorningIncrease
     * @return Tarif
     */
    public function setSundayMorningIncrease($sundayMorningIncrease)
    {
        $this->sundayMorningIncrease = $sundayMorningIncrease;

        return $this;
    }

    /**
     * Get sundayMorningIncrease
     *
     * @return string
     */
    public function getSundayMorningIncrease()
    {
        return $this->sundayMorningIncrease;
    }

    /**
     * Set mondayNightFrom
     *
     * @param \DateTime $mondayNightFrom
     * @return Tarif
     */
    public function setMondayNightFrom($mondayNightFrom)
    {
        $this->mondayNightFrom = $mondayNightFrom;

        return $this;
    }

    /**
     * Get mondayNightFrom
     *
     * @return \DateTime
     */
    public function getMondayNightFrom()
    {
        return $this->mondayNightFrom;
    }

    /**
     * Set mondayNightTo
     *
     * @param \DateTime $mondayNightTo
     * @return Tarif
     */
    public function setMondayNightTo($mondayNightTo)
    {
        $this->mondayNightTo = $mondayNightTo;

        return $this;
    }

    /**
     * Get mondayNightTo
     *
     * @return \DateTime
     */
    public function getMondayNightTo()
    {
        return $this->mondayNightTo;
    }

    /**
     * Set mondayNightIncrease
     *
     * @param string $mondayNightIncrease
     * @return Tarif
     */
    public function setMondayNightIncrease($mondayNightIncrease)
    {
        $this->mondayNightIncrease = $mondayNightIncrease;

        return $this;
    }

    /**
     * Get mondayNightIncrease
     *
     * @return string
     */
    public function getMondayNightIncrease()
    {
        return $this->mondayNightIncrease;
    }

    /**
     * Set thursdayNightFrom
     *
     * @param \DateTime $thursdayNightFrom
     * @return Tarif
     */
    public function setThursdayNightFrom($thursdayNightFrom)
    {
        $this->thursdayNightFrom = $thursdayNightFrom;

        return $this;
    }

    /**
     * Get thursdayNightFrom
     *
     * @return \DateTime
     */
    public function getThursdayNightFrom()
    {
        return $this->thursdayNightFrom;
    }

    /**
     * Set thursdayNightTo
     *
     * @param \DateTime $thursdayNightTo
     * @return Tarif
     */
    public function setThursdayNightTo($thursdayNightTo)
    {
        $this->thursdayNightTo = $thursdayNightTo;

        return $this;
    }

    /**
     * Get thursdayNightTo
     *
     * @return \DateTime
     */
    public function getThursdayNightTo()
    {
        return $this->thursdayNightTo;
    }

    /**
     * Set thursdayNightIncrease
     *
     * @param string $thursdayNightIncrease
     * @return Tarif
     */
    public function setThursdayNightIncrease($thursdayNightIncrease)
    {
        $this->thursdayNightIncrease = $thursdayNightIncrease;

        return $this;
    }

    /**
     * Get thursdayNightIncrease
     *
     * @return string
     */
    public function getThursdayNightIncrease()
    {
        return $this->thursdayNightIncrease;
    }

    /**
     * Set wednesdayNightFrom
     *
     * @param \DateTime $wednesdayNightFrom
     * @return Tarif
     */
    public function setWednesdayNightFrom($wednesdayNightFrom)
    {
        $this->wednesdayNightFrom = $wednesdayNightFrom;

        return $this;
    }

    /**
     * Get wednesdayNightFrom
     *
     * @return \DateTime
     */
    public function getWednesdayNightFrom()
    {
        return $this->wednesdayNightFrom;
    }

    /**
     * Set wednesdayNightTo
     *
     * @param \DateTime $wednesdayNightTo
     * @return Tarif
     */
    public function setWednesdayNightTo($wednesdayNightTo)
    {
        $this->wednesdayNightTo = $wednesdayNightTo;

        return $this;
    }

    /**
     * Get wednesdayNightTo
     *
     * @return \DateTime
     */
    public function getWednesdayNightTo()
    {
        return $this->wednesdayNightTo;
    }

    /**
     * Set wednesdayNightIncrease
     *
     * @param string $wednesdayNightIncrease
     * @return Tarif
     */
    public function setWednesdayNightIncrease($wednesdayNightIncrease)
    {
        $this->wednesdayNightIncrease = $wednesdayNightIncrease;

        return $this;
    }

    /**
     * Get wednesdayNightIncrease
     *
     * @return string
     */
    public function getWednesdayNightIncrease()
    {
        return $this->wednesdayNightIncrease;
    }

    /**
     * Set tuesdayNightFrom
     *
     * @param \DateTime $tuesdayNightFrom
     * @return Tarif
     */
    public function setTuesdayNightFrom($tuesdayNightFrom)
    {
        $this->tuesdayNightFrom = $tuesdayNightFrom;

        return $this;
    }

    /**
     * Get tuesdayNightFrom
     *
     * @return \DateTime
     */
    public function getTuesdayNightFrom()
    {
        return $this->tuesdayNightFrom;
    }

    /**
     * Set tuesdayNightTo
     *
     * @param \DateTime $tuesdayNightTo
     * @return Tarif
     */
    public function setTuesdayNightTo($tuesdayNightTo)
    {
        $this->tuesdayNightTo = $tuesdayNightTo;

        return $this;
    }

    /**
     * Get tuesdayNightTo
     *
     * @return \DateTime
     */
    public function getTuesdayNightTo()
    {
        return $this->tuesdayNightTo;
    }

    /**
     * Set tuesdayNightIncrease
     *
     * @param string $tuesdayNightIncrease
     * @return Tarif
     */
    public function setTuesdayNightIncrease($tuesdayNightIncrease)
    {
        $this->tuesdayNightIncrease = $tuesdayNightIncrease;

        return $this;
    }

    /**
     * Get tuesdayNightIncrease
     *
     * @return string
     */
    public function getTuesdayNightIncrease()
    {
        return $this->tuesdayNightIncrease;
    }

    /**
     * Set fridayNightFrom
     *
     * @param \DateTime $fridayNightFrom
     * @return Tarif
     */
    public function setFridayNightFrom($fridayNightFrom)
    {
        $this->fridayNightFrom = $fridayNightFrom;

        return $this;
    }

    /**
     * Get fridayNightFrom
     *
     * @return \DateTime
     */
    public function getFridayNightFrom()
    {
        return $this->fridayNightFrom;
    }

    /**
     * Set fridayNightTo
     *
     * @param \DateTime $fridayNightTo
     * @return Tarif
     */
    public function setFridayNightTo($fridayNightTo)
    {
        $this->fridayNightTo = $fridayNightTo;

        return $this;
    }

    /**
     * Get fridayNightTo
     *
     * @return \DateTime
     */
    public function getFridayNightTo()
    {
        return $this->fridayNightTo;
    }

    /**
     * Set fridayNightIncrease
     *
     * @param string $fridayNightIncrease
     * @return Tarif
     */
    public function setFridayNightIncrease($fridayNightIncrease)
    {
        $this->fridayNightIncrease = $fridayNightIncrease;

        return $this;
    }

    /**
     * Get fridayNightIncrease
     *
     * @return string
     */
    public function getFridayNightIncrease()
    {
        return $this->fridayNightIncrease;
    }

    /**
     * Set saturdayNightFrom
     *
     * @param \DateTime $saturdayNightFrom
     * @return Tarif
     */
    public function setSaturdayNightFrom($saturdayNightFrom)
    {
        $this->saturdayNightFrom = $saturdayNightFrom;

        return $this;
    }

    /**
     * Get saturdayNightFrom
     *
     * @return \DateTime
     */
    public function getSaturdayNightFrom()
    {
        return $this->saturdayNightFrom;
    }

    /**
     * Set saturdayNightTo
     *
     * @param \DateTime $saturdayNightTo
     * @return Tarif
     */
    public function setSaturdayNightTo($saturdayNightTo)
    {
        $this->saturdayNightTo = $saturdayNightTo;

        return $this;
    }

    /**
     * Get saturdayNightTo
     *
     * @return \DateTime
     */
    public function getSaturdayNightTo()
    {
        return $this->saturdayNightTo;
    }

    /**
     * Set saturdayNightIncrease
     *
     * @param string $saturdayNightIncrease
     * @return Tarif
     */
    public function setSaturdayNightIncrease($saturdayNightIncrease)
    {
        $this->saturdayNightIncrease = $saturdayNightIncrease;

        return $this;
    }

    /**
     * Get saturdayNightIncrease
     *
     * @return string
     */
    public function getSaturdayNightIncrease()
    {
        return $this->saturdayNightIncrease;
    }

    /**
     * Set sundayNightFrom
     *
     * @param \DateTime $sundayNightFrom
     * @return Tarif
     */
    public function setSundayNightFrom($sundayNightFrom)
    {
        $this->sundayNightFrom = $sundayNightFrom;

        return $this;
    }

    /**
     * Get sundayNightFrom
     *
     * @return \DateTime
     */
    public function getSundayNightFrom()
    {
        return $this->sundayNightFrom;
    }

    /**
     * Set sundayNightTo
     *
     * @param \DateTime $sundayNightTo
     * @return Tarif
     */
    public function setSundayNightTo($sundayNightTo)
    {
        $this->sundayNightTo = $sundayNightTo;

        return $this;
    }

    /**
     * Get sundayNightTo
     *
     * @return \DateTime
     */
    public function getSundayNightTo()
    {
        return $this->sundayNightTo;
    }

    /**
     * Set sundayNightIncrease
     *
     * @param string $sundayNightIncrease
     * @return Tarif
     */
    public function setSundayNightIncrease($sundayNightIncrease)
    {
        $this->sundayNightIncrease = $sundayNightIncrease;

        return $this;
    }

    /**
     * Get sundayNightIncrease
     *
     * @return string
     */
    public function getSundayNightIncrease()
    {
        return $this->sundayNightIncrease;
    }

    /**
     * Set hourNightTarif
     *
     * @param \CAB\CourseBundle\Entity\HourTarifNight $hourNightTarif
     * @return Tarif
     */
    public function setHourNightTarif(\CAB\CourseBundle\Entity\HourTarifNight $hourNightTarif = null)
    {
        $this->hourNightTarif = $hourNightTarif;

        return $this;
    }

    /**
     * Get hourNightTarif
     *
     * @return \CAB\CourseBundle\Entity\HourTarifNight
     */
    public function getHourNightTarif()
    {
        return $this->hourNightTarif;
    }

    /**
     * Set tarifEquipment
     *
     * @param string $tarifEquipment
     * @return Tarif
     */
    public function setTarifEquipment($tarifEquipment)
    {
        $this->tarifEquipment = $tarifEquipment;

        return $this;
    }

    /**
     * Get tarifEquipment
     *
     * @return string
     */
    public function getTarifEquipment()
    {
        return $this->tarifEquipment;
    }

    /**
     * Set tarifHandicapWheelchair
     *
     * @param string $tarifHandicapWheelchair
     * @return Tarif
     */
    public function setTarifHandicapWheelchair($tarifHandicapWheelchair)
    {
        $this->tarifHandicapWheelchair = $tarifHandicapWheelchair;

        return $this;
    }

    /**
     * Get tarifHandicapWheelchair
     *
     * @return string
     */
    public function getTarifHandicapWheelchair()
    {
        return $this->tarifHandicapWheelchair;
    }

    /**
     * Set tarifBabySeat
     *
     * @param string $tarifBabySeat
     * @return Tarif
     */
    public function setTarifBabySeat($tarifBabySeat)
    {
        $this->tarifBabySeat = $tarifBabySeat;

        return $this;
    }

    /**
     * Get tarifBabySeat
     *
     * @return string
     */
    public function getTarifBabySeat()
    {
        return $this->tarifBabySeat;
    }

    /**
     * Add coursesTarif
     *
     * @param \CAB\CourseBundle\Entity\Course $coursesTarif
     * @return Tarif
     */
    public function addCoursesTarif(\CAB\CourseBundle\Entity\Course $coursesTarif)
    {
        $this->coursesTarif[] = $coursesTarif;

        return $this;
    }

    /**
     * Remove coursesTarif
     *
     * @param \CAB\CourseBundle\Entity\Course $coursesTarif
     */
    public function removeCoursesTarif(\CAB\CourseBundle\Entity\Course $coursesTarif)
    {
        $this->coursesTarif->removeElement($coursesTarif);
    }

    /**
     * Get coursesTarif
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCoursesTarif()
    {
        return $this->coursesTarif;
    }

    /**
     * Set typeVehiculeTarif
     *
     * @param \CAB\CourseBundle\Entity\TypeVehicule $typeVehiculeTarif
     * @return Tarif
     */
    public function setTypeVehiculeTarif(\CAB\CourseBundle\Entity\TypeVehicule $typeVehiculeTarif = null)
    {
        $this->typeVehiculeTarif = $typeVehiculeTarif;

        return $this;
    }

    /**
     * Get typeVehiculeTarif
     *
     * @return \CAB\CourseBundle\Entity\TypeVehicule
     */
    public function getTypeVehiculeTarif()
    {
        return $this->typeVehiculeTarif;
    }
}
