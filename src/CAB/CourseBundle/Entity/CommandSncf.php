<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 15/12/2017
 * Time: 22:07
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Class CommandSncf
 * @package CAB\CourseBundle\Entity
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\CommandSncfRepository")
 * @ORM\Table("cab_commande_sncf")
 * @UniqueEntity(fields={"commandeCourse"},message="Ce numéro de commande existe déjà")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class CommandSncf
{
    /**
     * @var int $id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $codeBupo
     * @ORM\Column(type="string", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $codeBupo;

    /**
     * @var string $rlt
     * @ORM\Column(type="string", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $rlt;

    /**
     * @var string $js
     * @ORM\Column(type="string", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $js;

    /**
     * @var string $commandeCourse
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $commandeCourse;

    /**
     * @var boolean
     *
     * @ORM\Column(name="tarif_ref_sncf", type="boolean", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $tarifrefSncf;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Course", mappedBy="commandSncf", fetch="EXTRA_LAZY")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $course;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=true)
     * @Expose
     * @Groups({"Public", "Course"})
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeBupo
     *
     * @param string $codeBupo
     * @return CommandSncf
     */
    public function setCodeBupo($codeBupo)
    {
        $this->codeBupo = $codeBupo;

        return $this;
    }

    /**
     * Get codeBupo
     *
     * @return string
     */
    public function getCodeBupo()
    {
        return $this->codeBupo;
    }

    /**
     * Set rlt
     *
     * @param string $rlt
     * @return CommandSncf
     */
    public function setRlt($rlt)
    {
        $this->rlt = $rlt;

        return $this;
    }

    /**
     * Get rlt
     *
     * @return string
     */
    public function getRlt()
    {
        return $this->rlt;
    }

    /**
     * Set js
     *
     * @param string $js
     * @return CommandSncf
     */
    public function setJs($js)
    {
        $this->js = $js;

        return $this;
    }

    /**
     * Get js
     *
     * @return string
     */
    public function getJs()
    {
        return $this->js;
    }

    /**
     * Set commandeCourse
     *
     * @param string $commandeCourse
     * @return CommandSncf
     */
    public function setCommandeCourse($commandeCourse)
    {
        $this->commandeCourse = $commandeCourse;

        return $this;
    }

    /**
     * Get commandeCourse
     *
     * @return string
     */
    public function getCommandeCourse()
    {
        return $this->commandeCourse;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CommandSncf
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CommandSncf
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return CommandSncf
     */
    public function setCourse(\CAB\CourseBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set tarifrefSncf
     *
     * @param boolean $tarifrefSncf
     * @return CommandSncf
     */
    public function setTarifrefSncf($tarifrefSncf)
    {
        $this->tarifrefSncf = $tarifrefSncf;

        return $this;
    }

    /**
     * Get tarifrefSncf
     *
     * @return boolean
     */
    public function getTarifrefSncf()
    {
        return $this->tarifrefSncf;
    }

    /**
     * Get tarifrefSncf
     *
     * @return boolean
     */
    public function getTarifrefSncfFormatted()
    {
       $var= $this->tarifrefSncf;
        if ( $var === true ) {
            $var = 'OUI';
        }else{
            $var = 'NON';
        }
        return $var;
    }
}
