<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/07/2015
 * Time: 18:58
 */
namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * CAB\CourseBundle\Entity\Vehicule
 * @ORM\Entity
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_vehicule")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\VehiculeRepository")
 * @Annotation\ExclusionPolicy("all")
 * @Vich\Uploadable
 *
 */
class Vehicule
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     * @Annotation\Groups({"Details"})
     */
    protected $id;

    /** @var \CAB\UserBundle\Entity\User
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="vehicle")
     * @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     *
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Company", inversedBy="companiesVehicule")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\TypeVehicule", inversedBy="vehicules")
     * @ORM\JoinColumn(name="type_vehicule_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $typeVehicule;


    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\VehiculeMake", inversedBy="vehicles")
     * @ORM\JoinColumn(name="vehicule_maker_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicleMaker;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\VehiculeModel", inversedBy="vehicles")
     * @ORM\JoinColumn(name="vehicule_model_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $vehicleModel;

    /**
     * @var string $name
     *
     * @ORM\Column(name="vehicule_name", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Annotation\Groups({"Details"})
     */
    private $vehiculeName;

    /**
     * @var string $nbPerson
     *
     * @ORM\Column(name="nb_person", type="integer", nullable=false)
     * @Annotation\Expose
     * @Annotation\Groups({"Details"})
     */
    private $nbPerson = 1;

    /**
     * @var string $suitcases
     *
     * @ORM\Column(name="suitcases", type="integer", nullable=false)
     */
    private $suitcases = 1;

    /**
     * @var string $km
     *
     * @ORM\Column(name="km", type="integer", nullable=false)
     * @Annotation\Expose
     * @Annotation\Groups({"Details"})
     */
    private $km = 1;

    /**
     * @var string $numberlicence
     *
     * @ORM\Column(name="number_licence", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Annotation\Groups({"Details"})
     */
    private $numberlicence ;

    /**
     * @var boolean $isActive
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive = TRUE;

    /**
     * @var string $vin
     *
     * @ORM\Column(name="vin", type="string", length=255, nullable=true)
     */
    private $vin;

    /**
     * @var string $license_plate_number
     *
     * @ORM\Column(name="license_plate_number", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Annotation\Groups({"Details"})
     */
    private $licensePlateNumber;

    /**
     * @var boolean $isOutOfService
     *
     * @ORM\Column(name="is_out_of_service", type="boolean")
     */
    private $isOutOfService = false;

    /**
     * @var boolean $isForHandicap
     *
     * @ORM\Column(name="is_for_handicap", type="boolean")
     */
    private $isForHandicap = false;

    /**
     * @var string $status_truck
     * @Assert\NotNull()
     * @ORM\Column(name="status_car", type="string", length=64, nullable=true)
     */
    private $statusCar;

    /**
     * @var string $photoFront
     * @ORM\Column(type="string",length=250, nullable=false)
     */
    private $photoFront = null;

    /**
     * @Vich\UploadableField(mapping="vehicle_image", fileNameProperty="photoFront")
     *
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @var File
     *
     */
    private $fileFront;

    /**
     * @var string $photoBack
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $photoBack = null;

    /**
     * @Vich\UploadableField(mapping="vehicle_image", fileNameProperty="photoBack")
     *
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @var File
     *
     */
    private $fileBack;

    /**
     * @var string $photoInterior
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $photoInterior = null;

    /**
     * @Vich\UploadableField(mapping="vehicle_image", fileNameProperty="photoInterior")
     *
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @var File
     *
     */
    private $fileInterior;

    /**
     * @var string $docGreyRegistrationCard
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $docGreyRegistrationCard = null;

    /**
     * @var datetime $expiredocGreyRegistrationCard
     *
     * @ORM\Column(type="datetime", name="expire_docGreyRegistrationCard", nullable=true)
     */
    private $expiredocGreyRegistrationCard;

    /**
     * @Vich\UploadableField(mapping="vehicle_image", fileNameProperty="docGreyRegistrationCard")
     *
     * @var File
     */
    private $fileGreyRegistrationCard;

    /**
     * @var string $docInsuranceCard
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $docInsuranceCard = null;

    /**
     * @var datetime $expiredocInsuranceCard
     *
     * @ORM\Column(type="datetime", name="expire_docInsuranceCard", nullable=true)
     */
    private $expiredocInsuranceCard;

    /**
     * @Vich\UploadableField(mapping="vehicle_image", fileNameProperty="docInsuranceCard")
     *
     * @var File
     */
    private $fileInsuranceCard;

    /**
     * @var string $docVehicleTechCertif
     * @ORM\Column(type="string",length=250, nullable=true)
     */
    private $docVehicleTechCertif = null;

    /**
     * @var datetime $expiredocVehicleTechCertif
     *
     * @ORM\Column(type="datetime", name="expire_docVehicleTechCertif", nullable=true)
     */
    private $expiredocVehicleTechCertif;


    /**
     * @Vich\UploadableField(mapping="vehicle_image", fileNameProperty="docVehicleTechCertif")
     *
     * @var File
     */
    private $fileVehicleTechCertif;

    /**
     * @var boolean $cpam
     *
     * @ORM\Column(name="cpam_medical", type="boolean")
     */
    private $cpam = false;

    /**
     * @var boolean $partageCompany
     *
     * @ORM\Column(name="partage_company", type="boolean")
     */
    private $partageCompany = false;

    /**
     * @var boolean $soustraintant
     *
     * @ORM\Column(name="sous_traitant", type="boolean")
     */
    private $soustraintant = false;


    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\Course", mappedBy="vehicle")
     */
    private $courses;

    /**
     * @ORM\OneToMany(targetEntity="CAB\CourseBundle\Entity\VehiculeAffect", mappedBy="vehicule")
     */
    private $vehiculeaffects;

    /**
     * @ORM\ManyToMany(targetEntity="CAB\CourseBundle\Entity\ServiceTransport", inversedBy="vehicles")
     * @ORM\JoinTable(name="cab_services_vehicles")
     */
    private $servicesTransportVehicles;

    public function __toString()
    {
        return $this->getVehiculeName();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    protected function getUploadRootDir($dir = 'uploads/vehicule')
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir($dir);
    }

    protected function getUploadDir($dir = 'uploads/vehicule')
    {
        return $dir;
    }

    public function getAbsolutePath($typePhoto, $dir = null)
    {
        if ($dir == null)
            $dir = $this->getUploadRootDir();
        else
            $dir = $this->getUploadRootDir('uploads/vehicule/documents');

        return null === $this->$typePhoto ? null : $dir . '/' . $this->$typePhoto;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {

        if (null !== $this->fileFront) {
            $this->photoFront = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileFront->guessExtension();
        }

        if (null !== $this->fileBack) {

            $this->photoBack = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileBack->guessExtension();
        }

        if (null !== $this->fileInterior) {
            $this->photoInterior = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileInterior->guessExtension();
        }

        if (null !== $this->fileGreyRegistrationCard) {
            $this->docGreyRegistrationCard = sha1(uniqid(mt_rand(), true)) . '.' .
                $this->fileGreyRegistrationCard->guessExtension();
        }

        if (null !== $this->fileInsuranceCard) {
            $this->docInsuranceCard = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileInsuranceCard->guessExtension();
        }

        if (null !== $this->fileVehicleTechCertif) {
            $this->docVehicleTechCertif = sha1(uniqid(mt_rand(), true)) . '.' .
                $this->fileVehicleTechCertif->guessExtension();
        }

    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {

        if (null !== $this->fileFront) {
            $this->fileFront->move($this->getUploadRootDir(), $this->photoFront);
            unset($this->fileFront);
        }

        if (null !== $this->fileBack) {
            $this->fileBack->move($this->getUploadRootDir(), $this->photoBack);
            unset($this->fileBack);
        }
        if (null !== $this->fileInterior) {

            $this->fileInterior->move($this->getUploadRootDir(), $this->photoInterior);
            unset($this->fileInterior);
        }


        if (null !== $this->fileGreyRegistrationCard) {
            $this->fileGreyRegistrationCard->move($this->getUploadRootDir('uploads/vehicule/documents'),
                $this->docGreyRegistrationCard);
            unset($this->fileGreyRegistrationCard);
        }

        if (null !== $this->fileInsuranceCard) {
            $this->fileInsuranceCard->move($this->getUploadRootDir('uploads/vehicule/documents'),
                $this->docInsuranceCard);
            unset($this->fileInsuranceCard);
        }
        if (null !== $this->fileVehicleTechCertif) {

            $this->fileVehicleTechCertif->move($this->getUploadRootDir('uploads/vehicule/documents'),
                $this->docVehicleTechCertif);
            unset($this->fileVehicleTechCertif);
        }

    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->fileFront != "") {
            if ($file = $this->getAbsolutePath('photoFront'))
                unlink($file);
        }

        if ($this->fileBack != "") {
            if ($file = $this->getAbsolutePath('photoBack'))
                unlink($file);
        }

        if ($this->fileInterior != "") {
            if ($file = $this->getAbsolutePath('photoInterior'))
                unlink($file);
        }


        if ($this->fileGreyRegistrationCard != "") {
            if ($file = $this->getAbsolutePath('docGreyRegistrationCard'))
                unlink($file);
        }

        if ($this->fileInsuranceCard != "") {
            if ($file = $this->getAbsolutePath('docVehicleTechCertif'))
                unlink($file);
        }

        if ($this->fileVehicleTechCertif != "") {
            if ($file = $this->getAbsolutePath('photoInterior'))
                unlink($file);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Vehicule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Vehicule
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Get isActiveformatted
     *
     * @return boolean
     */
    public function getIsActiveformatted()
    {
        if ($this->isActive == 1)
        {
            return 'YES';
        } else
        {
            return 'NO';
        }

    }




    /**
     * Set vin
     *
     * @param string $vin
     * @return Vehicule
     */
    public function setVin($vin)
    {
        $this->vin = $vin;

        return $this;
    }

    /**
     * Get vin
     *
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * Set licensePlateNumber
     *
     * @param string $licensePlateNumber
     * @return Vehicule
     */
    public function setLicensePlateNumber($licensePlateNumber)
    {
        $this->licensePlateNumber = $licensePlateNumber;

        return $this;
    }

    /**
     * Get licensePlateNumber
     *
     * @return string
     */
    public function getLicensePlateNumber()
    {
        return $this->licensePlateNumber;
    }

    /**
     * Set isOutOfService
     *
     * @param boolean $isOutOfService
     * @return Vehicule
     */
    public function setIsOutOfService($isOutOfService)
    {
        $this->isOutOfService = $isOutOfService;

        return $this;
    }

    /**
     * Get isOutOfService
     *
     * @return boolean
     */
    public function getIsOutOfService()
    {
        return $this->isOutOfService;
    }

    /**
     * Set statusCar
     *
     * @param string $statusCar
     * @return Vehicule
     */
    public function setStatusCar($statusCar)
    {
        $this->statusCar = $statusCar;

        return $this;
    }

    /**
     * Get statusCar
     *
     * @return string
     */
    public function getStatusCar()
    {
        return $this->statusCar;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Vehicule
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Vehicule
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set company
     *
     * @param \CAB\CourseBundle\Entity\Company $company
     * @return Vehicule
     */
    public function setCompany(\CAB\CourseBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CAB\CourseBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set nbPerson
     *
     * @param integer $nbPerson
     * @return Vehicule
     */
    public function setNbPerson($nbPerson)
    {
        $this->nbPerson = $nbPerson;

        return $this;
    }

    /**
     * Get nbPerson
     *
     * @return integer
     */
    public function getNbPerson()
    {
        return $this->nbPerson;
    }

    /**
     * Set suitcases
     *
     * @param integer $suitcases
     * @return Vehicule
     */
    public function setSuitcases($suitcases)
    {
        $this->suitcases = $suitcases;

        return $this;
    }

    /**
     * Get suitcases
     *
     * @return integer
     */
    public function getSuitcases()
    {
        return $this->suitcases;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add vehicules
     *
     * @param \CAB\CourseBundle\Entity\Tarif $vehicules
     * @return Vehicule
     */
    public function addVehicule(\CAB\CourseBundle\Entity\Tarif $vehicules)
    {
        $this->vehicules[] = $vehicules;

        return $this;
    }

    /**
     * Remove vehicules
     *
     * @param \CAB\CourseBundle\Entity\Tarif $vehicules
     */
    public function removeVehicule(\CAB\CourseBundle\Entity\Tarif $vehicules)
    {
        $this->vehicules->removeElement($vehicules);
    }

    /**
     * Get vehicules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehicules()
    {
        return $this->vehicules;
    }

    /**
     * Set vehiculeName
     *
     * @param string $vehiculeName
     * @return Vehicule
     */
    public function setVehiculeName($vehiculeName)
    {
        $this->vehiculeName = $vehiculeName;

        return $this;
    }

    /**
     * Get vehiculeName
     *
     * @return string
     */
    public function getVehiculeName()
    {
        return $this->vehiculeName;
    }

    /**
     * Set typeVehicule
     *
     * @param \CAB\CourseBundle\Entity\TypeVehicule $typeVehicule
     * @return Vehicule
     */
    public function setTypeVehicule(\CAB\CourseBundle\Entity\TypeVehicule $typeVehicule = null)
    {
        $this->typeVehicule = $typeVehicule;

        return $this;
    }

    /**
     * Get typeVehicule
     *
     * @return \CAB\CourseBundle\Entity\TypeVehicule
     */
    public function getTypeVehicule()
    {
        return $this->typeVehicule;
    }

    /**
     * Add courses
     *
     * @param \CAB\CourseBundle\Entity\Course $courses
     * @return Vehicule
     */
    public function addCourse(\CAB\CourseBundle\Entity\Course $courses)
    {
        $this->courses[] = $courses;

        return $this;
    }

    /**
     * Remove courses
     *
     * @param \CAB\CourseBundle\Entity\Course $courses
     */
    public function removeCourse(\CAB\CourseBundle\Entity\Course $courses)
    {
        $this->courses->removeElement($courses);
    }

    /**
     * Get courses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCourses()
    {
        return $this->courses;
    }

    /**
     * Add servicesTransportVehicles
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $servicesTransportVehicles
     * @return Vehicule
     */
    public function addServicesTransportVehicle(\CAB\CourseBundle\Entity\ServiceTransport $servicesTransportVehicles)
    {
        $this->servicesTransportVehicles[] = $servicesTransportVehicles;

        return $this;
    }

    /**
     * Remove servicesTransportVehicles
     *
     * @param \CAB\CourseBundle\Entity\ServiceTransport $servicesTransportVehicles
     */
    public function removeServicesTransportVehicle(\CAB\CourseBundle\Entity\ServiceTransport $servicesTransportVehicles)
    {
        $this->servicesTransportVehicles->removeElement($servicesTransportVehicles);
    }

    /**
     * Get servicesTransportVehicles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServicesTransportVehicles()
    {
        return $this->servicesTransportVehicles;
    }

    /**
     * Set isForHandicap
     *
     * @param boolean $isForHandicap
     * @return Vehicule
     */
    public function setIsForHandicap($isForHandicap)
    {
        $this->isForHandicap = $isForHandicap;

        return $this;
    }

    /**
     * Get isForHandicap
     *
     * @return boolean
     */
    public function getIsForHandicap()
    {
        return $this->isForHandicap;
    }


    /**
     * Set photoFront
     *
     * @param string $photoFront
     * @return Vehicule
     */
    public function setPhotoFront($photoFront)
    {
        $this->photoFront = $photoFront;

        return $this;
    }

    /**
     * Get photoFront
     *
     * @return string
     */
    public function getPhotoFront()
    {
        return $this->photoFront;
    }

    /**
     * Set photoBack
     *
     * @param string $photoBack
     * @return Vehicule
     */
    public function setPhotoBack($photoBack)
    {
        $this->photoBack = $photoBack;

        return $this;
    }

    /**
     * Get photoBack
     *
     * @return string
     */
    public function getPhotoBack()
    {
        return $this->photoBack;
    }

    /**
     * Set photoInterior
     *
     * @param string $photoInterior
     * @return Vehicule
     */
    public function setPhotoInterior($photoInterior)
    {
        $this->photoInterior = $photoInterior;

        return $this;
    }

    /**
     * Get photoInterior
     *
     * @return string
     */
    public function getPhotoInterior()
    {
        return $this->photoInterior;
    }

    /**
     * Sets fileFront.
     *
     * @param UploadedFile $file
     */
    public function setFileFront($file = null)
    {
        $this->fileFront = $file;
        if (null !== $this->fileFront) {
            $this->photoFront = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileFront->guessExtension();
        }
    }

    /**
     * Get fileFront.
     *
     * @return UploadedFile
     */
    public function getFileFront()
    {
        return $this->fileFront;
    }

    /**
     * Sets fileBack.
     *
     * @param UploadedFile $file
     */
    public function setFileBack($file = null)
    {
        $this->fileBack = $file;

        if (null !== $this->fileBack) {
            $this->photoBack = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileBack->guessExtension();
        }

    }

    /**
     * Get fileBack.
     *
     * @return UploadedFile
     */
    public function getFileBack()
    {
        return $this->fileBack;
    }

    /**
     * Sets fileInterioir.
     *
     * @param UploadedFile $file
     */
    public function setFileInterior($file = null)
    {
        $this->fileInterior = $file;

        if (null !== $this->fileInterior) {
            $this->photoInterior = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileInterior->guessExtension();
        }
    }

    /**
     * Get fileInterioir.
     *
     * @return UploadedFile
     */
    public function getFileInterior()
    {
        return $this->fileInterior;
    }

    /**
     * Set driver
     *
     * @param \CAB\UserBundle\Entity\User $driver
     * @return Vehicule
     */
    public function setDriver(\CAB\UserBundle\Entity\User $driver = null)
    {
        $this->driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /*
     * ******************************************************
     * ******************************************************
     ***** Setters and getters Documents (mapped)
     * ******************************************************
     * ******************************************************
     */

    /**
     * Set docGreyRegistrationCard
     *
     * @param string $docGreyRegistrationCard
     * @return Vehicule
     */
    public function setDocGreyRegistrationCard($docGreyRegistrationCard)
    {
        $this->docGreyRegistrationCard = $docGreyRegistrationCard;

        return $this;
    }

    /**
     * Get docGreyRegistrationCard
     *
     * @return string
     */
    public function getDocGreyRegistrationCard()
    {
        return $this->docGreyRegistrationCard;
    }

    /**
     * Set docInsuranceCard
     *
     * @param string $docInsuranceCard
     * @return Vehicule
     */
    public function setDocInsuranceCard($docInsuranceCard)
    {
        $this->docInsuranceCard = $docInsuranceCard;

        return $this;
    }

    /**
     * Get docInsuranceCard
     *
     * @return string
     */
    public function getDocInsuranceCard()
    {
        return $this->docInsuranceCard;
    }

    /**
     * Set docVehicleTechCertif
     *
     * @param string $docVehicleTechCertif
     * @return Vehicule
     */
    public function setDocVehicleTechCertif($docVehicleTechCertif)
    {
        $this->docVehicleTechCertif = $docVehicleTechCertif;

        return $this;
    }

    /**
     * Get docVehicleTechCertif
     *
     * @return string
     */
    public function getDocVehicleTechCertif()
    {
        return $this->docVehicleTechCertif;
    }

    /*
     * ******************************************************
     * ******************************************************
     ***** Setters and getters files (not mapped)
     * *******************************************************
     * ******************************************************
     */

    /**
     * @return File
     */
    public function getFileGreyRegistrationCard()
    {
        return $this->fileGreyRegistrationCard;
    }

    /**
     * @param File $fileGreyRegistrationCard
     */
    public function setFileGreyRegistrationCard($fileGreyRegistrationCard)
    {
        $this->fileGreyRegistrationCard = $fileGreyRegistrationCard;
        if ($this->fileGreyRegistrationCard instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getFileInsuranceCard()
    {
        return $this->fileInsuranceCard;
    }

    /**
     * @param File $fileInsuranceCard
     */
    public function setFileInsuranceCard($fileInsuranceCard)
    {
        $this->fileInsuranceCard = $fileInsuranceCard;
        if (null !== $this->fileInsuranceCard) {
            $this->docInsuranceCard = sha1(uniqid(mt_rand(), true)) . '.' . $this->fileInsuranceCard->guessExtension();
        }
    }

    /**
     * @return File
     */
    public function getFileVehicleTechCertif()
    {
        return $this->fileVehicleTechCertif;
    }

    /**
     * @param File $fileVehicleTechCertif
     */
    public function setFileVehicleTechCertif($fileVehicleTechCertif)
    {
        $this->fileVehicleTechCertif = $fileVehicleTechCertif;
        if (null !== $this->fileVehicleTechCertif) {
            $this->docVehicleTechCertif = sha1(uniqid(mt_rand(), true)) . '.' .
                $this->fileVehicleTechCertif->guessExtension();
        }
    }

    /**
     * Set vehicleModel
     *
     * @param \CAB\CourseBundle\Entity\VehiculeModel $vehicleModel
     * @return Vehicule
     */
    public function setVehicleModel(\CAB\CourseBundle\Entity\VehiculeModel $vehicleModel = null)
    {
        $this->vehicleModel = $vehicleModel;

        return $this;
    }

    /**
     * Get vehicleModel
     *
     * @return \CAB\CourseBundle\Entity\VehiculeModel
     */
    public function getVehicleModel()
    {
        return $this->vehicleModel;
    }

    /**
     * Set km
     *
     * @param integer $km
     * @return Vehicule
     */
    public function setKm($km)
    {
        $this->km = $km;

        return $this;
    }

    /**
     * Get km
     *
     * @return integer
     */
    public function getKm()
    {
        return $this->km;
    }

    /**
     * Get company's name
     *
     * @return String
     * @Annotation\VirtualProperty
     */
    public function getCompanyName()
    {
        if (null !== $this->getCompany()) {
            return ucfirst($this->getCompany()->getCompanyName());
        }
    }

    /**
     * Add vehiculeaffects
     *
     * @param \CAB\CourseBundle\Entity\VehiculeAffect $vehiculeaffects
     * @return Vehicule
     */
    public function addVehiculeaffect(VehiculeAffect $vehiculeaffects)
    {
        $this->vehiculeaffects[] = $vehiculeaffects;

        return $this;
    }

    /**
     * Remove vehiculeaffects
     *
     * @param \CAB\CourseBundle\Entity\VehiculeAffect $vehiculeaffects
     */
    public function removeVehiculeaffect(VehiculeAffect $vehiculeaffects)
    {
        $this->vehiculeaffects->removeElement($vehiculeaffects);
    }

    /**
     * Get vehiculeaffects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVehiculeaffects()
    {
        return $this->vehiculeaffects;
    }

    /**
     * Set cpam
     *
     * @param boolean $cpam
     * @return Vehicule
     */
    public function setCpam($cpam)
    {
        $this->cpam = $cpam;

        return $this;
    }

    /**
     * Get cpam
     *
     * @return boolean
     */
    public function getCpam()
    {
        return $this->cpam;
    }

    /**
     * Get cpamformatted
     *
     * @return boolean
     */
    public function getCpamformatted()
    {
        if ($this->cpam == 1)
        {
            return 'YES';
        } else
        {
            return 'NO';
        }
    }




    /**
     * Set partageCompany
     *
     * @param boolean $partageCompany
     * @return Vehicule
     */
    public function setPartageCompany($partageCompany)
    {
        $this->partageCompany = $partageCompany;

        return $this;
    }

    /**
     * Get partageCompany
     *
     * @return boolean
     */
    public function getPartageCompany()
    {
        return $this->partageCompany;
    }

    /**
     * Get partageCompanyformatted
     *
     * @return boolean
     */
    public function getPartageCompanyformatted()
    {

        if ($this->partageCompany == 1)
        {
            return 'YES';
        } else
        {
            return 'NO';
        }
    }


    /**
     * Set expiredocGreyRegistrationCard
     *
     * @param \DateTime $expiredocGreyRegistrationCard
     * @return Vehicule
     */
    public function setExpiredocGreyRegistrationCard($expiredocGreyRegistrationCard)
    {
        $this->expiredocGreyRegistrationCard = $expiredocGreyRegistrationCard;

        return $this;
    }

    /**
     * Get expiredocGreyRegistrationCard
     *
     * @return \DateTime
     */
    public function getExpiredocGreyRegistrationCard()
    {
        return $this->expiredocGreyRegistrationCard;
    }

    /**
     * Set expiredocInsuranceCard
     *
     * @param \DateTime $expiredocInsuranceCard
     * @return Vehicule
     */
    public function setExpiredocInsuranceCard($expiredocInsuranceCard)
    {
        $this->expiredocInsuranceCard = $expiredocInsuranceCard;

        return $this;
    }

    /**
     * Get expiredocInsuranceCard
     *
     * @return \DateTime
     */
    public function getExpiredocInsuranceCard()
    {
        return $this->expiredocInsuranceCard;
    }

    /**
     * Set expiredocVehicleTechCertif
     *
     * @param \DateTime $expiredocVehicleTechCertif
     * @return Vehicule
     */
    public function setExpiredocVehicleTechCertif($expiredocVehicleTechCertif)
    {
        $this->expiredocVehicleTechCertif = $expiredocVehicleTechCertif;

        return $this;
    }

    /**
     * Get expiredocVehicleTechCertif
     *
     * @return \DateTime
     */
    public function getExpiredocVehicleTechCertif()
    {
        return $this->expiredocVehicleTechCertif;
    }

    /**
     * Set vehicleMaker
     *
     * @param \CAB\CourseBundle\Entity\VehiculeMake $vehicleMaker
     * @return Vehicule
     */
    public function setVehicleMaker(\CAB\CourseBundle\Entity\VehiculeMake $vehicleMaker = null)
    {
        $this->vehicleMaker = $vehicleMaker;

        return $this;
    }

    /**
     * Get vehicleMaker
     *
     * @return \CAB\CourseBundle\Entity\VehiculeMake
     */
    public function getVehicleMaker()
    {
        return $this->vehicleMaker;
    }

    /**
     * Set numberlicence
     *
     * @param string $numberlicence
     * @return Vehicule
     */
    public function setNumberlicence($numberlicence)
    {
        $this->numberlicence = $numberlicence;

        return $this;
    }

    /**
     * Get numberlicence
     *
     * @return string
     */
    public function getNumberlicence()
    {
        return $this->numberlicence;
    }

    /**
     * Set soustraintant
     *
     * @param boolean $soustraintant
     * @return Vehicule
     */
    public function setSoustraintant($soustraintant)
    {
        $this->soustraintant = $soustraintant;

        return $this;
    }

    /**
     * Get soustraintant
     *
     * @return boolean
     */
    public function getSoustraintant()
    {
        return $this->soustraintant;
    }

    /**
     * Get soustraintantformatted
     *
     * @return boolean
     */
    public function getSoustraintantformatted()
    {
        if ($this->soustraintant == 1)
        {
            return 'YES';
        } else
        {
            return 'NO';
        }
    }
}
