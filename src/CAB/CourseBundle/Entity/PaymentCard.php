<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 31/01/2016
 * Time: 22:10
 */

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CAB\CourseBundle\Entity\PaymentCard
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cab_payment_card")
 * @ORM\Entity(repositoryClass="CAB\CourseBundle\Entity\PaymentCardRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class PaymentCard
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $paymentId
     * @ORM\Column(type="string", nullable=true)
     */
    protected $paymentId;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="paymentCards")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\TransactionResponse", inversedBy="paymentCard")
     * @ORM\JoinColumn(name="transaction_response_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $transactionResponse;

    /**
     * @var string $cardNumber
     *
     * @ORM\Column(type="string", length=128, nullable=false, unique=true)
     */
    protected $cardNumber;

    /**
     * @var string $monthExpiry
     *
     * @ORM\Column(type="string", length=2, nullable=false)
     */
    protected $monthExpiry;

    /**
     * @var string $yearExpiry
     *
     * @ORM\Column(type="string", length=4, nullable=false)
     */
    protected $yearExpiry;

    /**
     * @var string $cardBrand
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $cardBrand;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime('0000-00-00');
    }

    /**
     *
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $currentDate = new \DateTime();
        $this->updatedAt = $currentDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardNumber
     *
     * @param string $cardNumber
     * @return PaymentCard
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set monthExpiry
     *
     * @param string $monthExpiry
     * @return PaymentCard
     */
    public function setMonthExpiry($monthExpiry)
    {
        $this->monthExpiry = $monthExpiry;

        return $this;
    }

    /**
     * Get monthExpiry
     *
     * @return string
     */
    public function getMonthExpiry()
    {
        return $this->monthExpiry;
    }

    /**
     * Set yearExpiry
     *
     * @param string $yearExpiry
     * @return PaymentCard
     */
    public function setYearExpiry($yearExpiry)
    {
        $this->yearExpiry = $yearExpiry;

        return $this;
    }

    /**
     * Get yearExpiry
     *
     * @return string
     */
    public function getYearExpiry()
    {
        return $this->yearExpiry;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PaymentCard
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return PaymentCard
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set transactionResponse
     *
     * @param \CAB\CourseBundle\Entity\TransactionResponse $transactionResponse
     * @return PaymentCard
     */
    public function setTransactionResponse(\CAB\CourseBundle\Entity\TransactionResponse $transactionResponse = null)
    {
        $this->transactionResponse = $transactionResponse;

        return $this;
    }

    /**
     * Get transactionResponse
     *
     * @return \CAB\CourseBundle\Entity\TransactionResponse
     */
    public function getTransactionResponse()
    {
        return $this->transactionResponse;
    }

    /**
     * Set user
     *
     * @param \CAB\UserBundle\Entity\User $user
     * @return PaymentCard
     */
    public function setUser(\CAB\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set cardBrand
     *
     * @param string $cardBrand
     * @return PaymentCard
     */
    public function setCardBrand($cardBrand)
    {
        $this->cardBrand = $cardBrand;

        return $this;
    }

    /**
     * Get cardBrand
     *
     * @return string
     */
    public function getCardBrand()
    {
        return $this->cardBrand;
    }

    /**
     * Set paymentId
     *
     * @param string $paymentId
     * @return PaymentCard
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }
}
