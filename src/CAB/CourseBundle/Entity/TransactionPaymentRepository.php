<?php

namespace CAB\CourseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TransactionPaymentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TransactionPaymentRepository extends EntityRepository
{
}
