<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 04/03/2016
 * Time: 17:09
 */

namespace CAB\CourseBundle\Events;


/**
 * Class UpdateStatusCourseEvents
 *
 * @package CAB\CourseBundle\Events
 */
final class UpdateStatusCourseEvents
{
    /**
     * The update.status..course event is thrown each time a course status is updated
     * in the system.
     *
     * The event listener receives an
     *  CAB\CourseBundle\Event\FilterCourseEvent instance.
     *
     * @var string
     */
    const UPDATE_STATUS_COURSE = 'cab.event.filtercourse';
}