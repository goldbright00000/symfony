<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 18/03/2016
 * Time: 00:05
 */

namespace CAB\CourseBundle\Events;


/**
 * Class HistoryCourseEvents
 *
 * @package CAB\CourseBundle\Events
 */
final class HistoryCourseEvents
{
    /**
     * The history.course event is thrown each time a course status is updated
     * in the system.
     *
     * The event listener receives an
     *  CAB\CourseBundle\Event\HistoryCourseEvent instance.
     *
     * @var string
     */
    const HISTORY_COURSE = 'cab.event.filtercourse_history';
}