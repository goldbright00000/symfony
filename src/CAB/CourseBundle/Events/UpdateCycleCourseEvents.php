<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 14/12/2016
 * Time: 17:19
 */

namespace CAB\CourseBundle\Events;


/**
 * Class UpdateCycleCourseEvents
 *
 * @package CAB\CourseBundle\Events
 */
final class UpdateCycleCourseEvents
{
    /**
     * The update.cycle.course event is thrown each time a course cycle is updated
     * in the system.
     *
     * The event listener receives an
     *  CAB\CourseBundle\Event\CycleCourseEvent instance.
     *
     * @var string
     */
    const UPDATE_CYCLE_COURSE = 'cab.event.cycle_course';
}