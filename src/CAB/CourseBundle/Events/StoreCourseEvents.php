<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:09
 */

namespace CAB\CourseBundle\Events;


/**
 * Class StoreCourseEvents
 *
 * @package CAB\CourseBundle\Events
 */
final class StoreCourseEvents {
    /**
     * The store.course event is thrown each time an course is created
     * in the system.
     *
     * The event listener receives an
     *  CAB\CourseBundle\Event\FilterCourseEvent instance.
     *
     * @var string
     */
    const STORE_COURSE = 'cab.event.storecourse';
}