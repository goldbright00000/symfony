<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 16/09/2017
 * Time: 19:19
 */

namespace CAB\CourseBundle\Events;


/**
 * Class RemoveCourseEvents
 *
 * @package CAB\CourseBundle\Events
 */
final class RemoveCourseEvents
{
    /**
     * The cab.event.remove_course event is thrown each time a course cycle is removed
     * in the system.
     *
     * The event listener receives an
     *  CAB\CourseBundle\Event\RemoveCourseEvent instance.
     *
     * @var string
     */
    const REMOVE_COURSE = 'cab.event.remove_course';
}