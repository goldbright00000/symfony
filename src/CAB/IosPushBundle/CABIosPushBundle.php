<?php

namespace CAB\IosPushBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CABIosPushBundle
 *
 * @package CAB\IosPushBundle
 */
class CABIosPushBundle extends Bundle
{
    /**
     * Use Bundle Inheritance to Override Parts of a RMSPushNotificationsBundle
     *
     * @return string
     */
    /*public function getParent()
    {
        return 'RMSPushNotificationsBundle';
    }*/
}
