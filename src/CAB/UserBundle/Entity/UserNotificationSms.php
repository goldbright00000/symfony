<?php

namespace CAB\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * UserNotificationEmail
 *
 * @ORM\Table(name="cab_user_notif_sms")
 * @ORM\Entity(repositoryClass="CAB\UserBundle\Entity\UserNotificationSmsRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class UserNotificationSms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var $clientSms
     * @ORM\OneToOne(targetEntity="CAB\UserBundle\Entity\User", mappedBy="notificationSms")
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $clientSms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_created", type="boolean")
     */
    private $statusCreated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_cancel_by_client", type="boolean")
     */
    private $statusCancelByClient;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_affected", type="boolean")
     */
    private $statusAffected;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_done", type="boolean")
     */
    private $statusDone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_accepted_by_driver", type="boolean")
     */
    private $statusAcceptedByDriver;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_arrived_driver", type="boolean")
     */
    private $statusArrivedDriver;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_cancel_by_driver", type="boolean")
     */
    private $statusCancelByDriver;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_driver_go_client", type="boolean")
     */
    private $statusDriverGoToClient;

    /**
     * @return array
     * @throws \ReflectionException
     */
    protected function getClassProperties()
    {
        $oClass = new self();
        $res = array();
        $reflect = new \ReflectionClass($oClass);
        $props   = $reflect->getProperties();

        foreach ($props as $prop) {
            $res[] = $prop->getName();
        }

        return $res;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setDefault()
    {
        $aProperties = $this->getClassProperties();
        foreach ($aProperties as $property) {
            if (!in_array($property, ['id', 'clientSms'], true)) {
                $set = 'set'.ucfirst($property);
                $this->$set(0);
            }
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusArrivedDriver
     *
     * @param boolean $statusArrivedDriver
     * @return UserNotificationSms
     */
    public function setStatusArrivedDriver($statusArrivedDriver)
    {
        $this->statusArrivedDriver = $statusArrivedDriver;

        return $this;
    }

    /**
     * Get statusArrivedDriver
     *
     * @return boolean
     */
    public function getStatusArrivedDriver()
    {
        return $this->statusArrivedDriver;
    }

    /**
     * Set clientSms
     *
     * @param \CAB\UserBundle\Entity\User $clientSms
     * @return UserNotificationSms
     */
    public function setClientSms(\CAB\UserBundle\Entity\User $clientSms = null)
    {
        $this->clientSms = $clientSms;

        return $this;
    }

    /**
     * Get clientSms
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getClientSms()
    {
        return $this->clientSms;
    }

    /**
     * Set statusCreated
     *
     * @param boolean $statusCreated
     * @return UserNotificationSms
     */
    public function setStatusCreated($statusCreated)
    {
        $this->statusCreated = $statusCreated;

        return $this;
    }

    /**
     * Get statusCreated
     *
     * @return boolean
     */
    public function getStatusCreated()
    {
        return $this->statusCreated;
    }

    /**
     * Set statusCancelByClient
     *
     * @param boolean $statusCancelByClient
     * @return UserNotificationSms
     */
    public function setStatusCancelByClient($statusCancelByClient)
    {
        $this->statusCancelByClient = $statusCancelByClient;

        return $this;
    }

    /**
     * Get statusCancelByClient
     *
     * @return boolean
     */
    public function getStatusCancelByClient()
    {
        return $this->statusCancelByClient;
    }

    /**
     * Set statusAffected
     *
     * @param boolean $statusAffected
     * @return UserNotificationSms
     */
    public function setStatusAffected($statusAffected)
    {
        $this->statusAffected = $statusAffected;

        return $this;
    }

    /**
     * Get statusAffected
     *
     * @return boolean
     */
    public function getStatusAffected()
    {
        return $this->statusAffected;
    }

    /**
     * Set statusDone
     *
     * @param boolean $statusDone
     * @return UserNotificationSms
     */
    public function setStatusDone($statusDone)
    {
        $this->statusDone = $statusDone;

        return $this;
    }

    /**
     * Get statusDone
     *
     * @return boolean
     */
    public function getStatusDone()
    {
        return $this->statusDone;
    }

    /**
     * Set statusAcceptedByDriver
     *
     * @param boolean $statusAcceptedByDriver
     * @return UserNotificationSms
     */
    public function setStatusAcceptedByDriver($statusAcceptedByDriver)
    {
        $this->statusAcceptedByDriver = $statusAcceptedByDriver;

        return $this;
    }

    /**
     * Get statusAcceptedByDriver
     *
     * @return boolean
     */
    public function getStatusAcceptedByDriver()
    {
        return $this->statusAcceptedByDriver;
    }

    /**
     * Set statusCancelByDriver
     *
     * @param boolean $statusCancelByDriver
     * @return UserNotificationSms
     */
    public function setStatusCancelByDriver($statusCancelByDriver)
    {
        $this->statusCancelByDriver = $statusCancelByDriver;

        return $this;
    }

    /**
     * Get statusCancelByDriver
     *
     * @return boolean
     */
    public function getStatusCancelByDriver()
    {
        return $this->statusCancelByDriver;
    }

    /**
     * Set statusDriverGoToClient
     *
     * @param boolean $statusDriverGoToClient
     * @return UserNotificationSms
     */
    public function setStatusDriverGoToClient($statusDriverGoToClient)
    {
        $this->statusDriverGoToClient = $statusDriverGoToClient;

        return $this;
    }

    /**
     * Get statusDriverGoToClient
     *
     * @return boolean
     */
    public function getStatusDriverGoToClient()
    {
        return $this->statusDriverGoToClient;
    }
}
