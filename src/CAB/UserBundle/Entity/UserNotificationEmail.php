<?php

namespace CAB\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * UserNotificationEmail
 *
 * @ORM\Table(name="cab_user_notif_email")
 * @ORM\Entity(repositoryClass="CAB\UserBundle\Entity\UserNotificationEmailRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class UserNotificationEmail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var $notificationEmail
     * @ORM\OneToOne(targetEntity="CAB\UserBundle\Entity\User", mappedBy="notificationEmail")
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $client;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_created", type="boolean")
     */
    private $statusCreated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_cancelbyclient", type="boolean")
     */
    private $statusCancelByClient;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_affected", type="boolean")
     */
    private $statusAffected;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_done", type="boolean")
     */
    private $statusDone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_acceptedbydriver", type="boolean")
     */
    private $statusAcceptedByDriver;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_arriveddriver", type="boolean")
     */
    private $statusArrivedDriver;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_cancelbydriver", type="boolean")
     */
    private $statusCancelByDriver;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status_drivergotoclient", type="boolean")
     */
    private $statusDriverGoToClient;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusCreated
     *
     * @param boolean $statusCreated
     * @return UserNotificationEmail
     */
    public function setStatusCreated($statusCreated)
    {
        $this->statusCreated = $statusCreated;

        return $this;
    }

    /**
     * Get statusCreated
     *
     * @return boolean
     */
    public function getStatusCreated()
    {
        return $this->statusCreated;
    }

    /**
     * Set statusCancelByClient
     *
     * @param boolean $statusCancelByClient
     * @return UserNotificationEmail
     */
    public function setStatusCancelByClient($statusCancelByClient)
    {
        $this->statusCancelByClient = $statusCancelByClient;

        return $this;
    }

    /**
     * Get statusCancelByClient
     *
     * @return boolean
     */
    public function getStatusCancelByClient()
    {
        return $this->statusCancelByClient;
    }

    /**
     * Set statusAffected
     *
     * @param boolean $statusAffected
     * @return UserNotificationEmail
     */
    public function setStatusAffected($statusAffected)
    {
        $this->statusAffected = $statusAffected;

        return $this;
    }

    /**
     * Get statusAffected
     *
     * @return boolean
     */
    public function getStatusAffected()
    {
        return $this->statusAffected;
    }

    /**
     * Set statusDone
     *
     * @param boolean $statusDone
     * @return UserNotificationEmail
     */
    public function setStatusDone($statusDone)
    {
        $this->statusDone = $statusDone;

        return $this;
    }

    /**
     * Get statusDone
     *
     * @return boolean
     */
    public function getStatusDone()
    {
        return $this->statusDone;
    }

    /**
     * Set statusAcceptedByDriver
     *
     * @param boolean $statusAcceptedByDriver
     * @return UserNotificationEmail
     */
    public function setStatusAcceptedByDriver($statusAcceptedByDriver)
    {
        $this->statusAcceptedByDriver = $statusAcceptedByDriver;

        return $this;
    }

    /**
     * Get statusAcceptedByDriver
     *
     * @return boolean
     */
    public function getStatusAcceptedByDriver()
    {
        return $this->statusAcceptedByDriver;
    }

    /**
     * Set statusArrivedDriver
     *
     * @param boolean $statusArrivedDriver
     * @return UserNotificationEmail
     */
    public function setStatusArrivedDriver($statusArrivedDriver)
    {
        $this->statusArrivedDriver = $statusArrivedDriver;

        return $this;
    }

    /**
     * Get statusArrivedDriver
     *
     * @return boolean
     */
    public function getStatusArrivedDriver()
    {
        return $this->statusArrivedDriver;
    }

    /**
     * Set statusCancelByDriver
     *
     * @param boolean $statusCancelByDriver
     * @return UserNotificationEmail
     */
    public function setStatusCancelByDriver($statusCancelByDriver)
    {
        $this->statusCancelByDriver = $statusCancelByDriver;

        return $this;
    }

    /**
     * Get statusCancelByDriver
     *
     * @return boolean
     */
    public function getStatusCancelByDriver()
    {
        return $this->statusCancelByDriver;
    }

    /**
     * Set statusDriverGoToClient
     *
     * @param boolean $statusDriverGoToClient
     * @return UserNotificationEmail
     */
    public function setStatusDriverGoToClient($statusDriverGoToClient)
    {
        $this->statusDriverGoToClient = $statusDriverGoToClient;

        return $this;
    }

    /**
     * Get statusDriverGoToClient
     *
     * @return boolean
     */
    public function getStatusDriverGoToClient()
    {
        return $this->statusDriverGoToClient;
    }

    /**
     * Set client
     *
     * @param \CAB\UserBundle\Entity\User $client
     * @return UserNotificationEmail
     */
    public function setClient(\CAB\UserBundle\Entity\User $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CAB\UserBundle\Entity\User
     */
    public function getClient()
    {
        return $this->client;
    }
}
