<?php
 
namespace CAB\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * UserNotificationEmailRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserNotificationEmailRepository extends EntityRepository
{
}
