<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 17/11/2017
 * Time: 14:09
 */

namespace CAB\UserBundle\Manager;


use CAB\ApiBundle\Manager\UserManager;
use CAB\CourseBundle\Manager\BaseManager;
use CAB\UserBundle\Entity\User;
use CAB\UserBundle\Entity\UserNotificationEmail;
use Doctrine\ORM\EntityManager;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class NotificationManager
 *
 * @package CAB\UserBundle\Manager
 */
class NotificationManager extends BaseManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $notificationId the notification ID
     *
     * @return UserNotificationEmail Object UserNotificationEmail
     */
    public function load($notificationId)
    {
        return $this->getRepository()
            ->findOneBy(['id' => $notificationId]);
    }

    /**
     * @param UserNotificationEmail $notification object notification
     * @param string     $attr the property name in UserNotificationEmail
     * @param int|string $value
     */
    public function setAttr(UserNotificationEmail $notification, $attr, $value)
    {
        $attributeSetter = 'set'.ucfirst($attr);
        $notification->$attributeSetter($value);
    }

    /**
     * Save UserNotificationEmail entity
     *
     * @param UserNotificationEmail $notification
     */
    public function save(UserNotificationEmail $notification)
    {
        $this->persistAndFlush($notification);
    }

    /**
     * @param bool $getType
     * @param bool $enabledNotification
     * @return array
     */
    public function getProperties($getType = false, $enabledNotification = false) {
        $aProperties = $this->getClassProperties(UserNotificationEmail::class, $getType);

        return array_fill_keys($aProperties, $enabledNotification);
    }

    /**
     * @param array $parameters
     * @param bool $enabledNotification
     * @return UserNotificationEmail
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function createNotificationEntity (array $parameters, $enabledNotification = false) {
        $properties = $this->getProperties(false, $enabledNotification);
        unset($properties['id']);
        $resolver = new OptionsResolver();
        $resolver->setDefaults($properties);
        $resolver->setRequired('client');
        $options = $resolver->resolve($parameters);

        /** @var UserNotificationEmail $this */
        return $this->create($options);
    }

    /**
     * @param array $parameters list of property of UserNotification entity
     * @return UserNotificationEmail
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function create(array $parameters) {
        /** @var User $user */
        $user = $parameters['client'];
        $oUserNotifMail = new UserNotificationEmail();
        foreach ($parameters as $property => $value) {
            $this->setAttr($oUserNotifMail, $property, $value);


        }
        $this->save($oUserNotifMail);
        $user->setNotificationEmail($oUserNotifMail);
        $this->em->persist($user);
        $this->em->flush();
        return $oUserNotifMail;
    }

    /**
     * Set the notification create dfor the given user
     * @param User $user
     * @return UserNotificationEmail
     */
    public function setNotificationForUser(User $user, $gettype = false, $enableNotification = true)
    {
        $propertiesNotification = $this->getProperties($gettype, $enableNotification);
        array_shift($propertiesNotification);
        $propertiesNotification['client'] = $user;
        $notifiation = $this->createNotificationEntity($propertiesNotification);

        return $notifiation;
    }
}