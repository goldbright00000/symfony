<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 17/11/2017
 * Time: 01:34
 */

namespace CAB\UserBundle\Controller;

use CAB\UserBundle\Entity\UserNotificationEmail;
use CAB\UserBundle\Entity\UserNotificationEmailRepository;
use CAB\UserBundle\Entity\UserNotificationSms;
use CAB\UserBundle\Form\Type\UserNotificationSMSType;
use CAB\UserBundle\Manager\NotificationManager;
use \Symfony\Component\HttpFoundation\Request;
use CAB\UserBundle\Entity\User;
use CAB\UserBundle\Form\Type\UserNotificationEmailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Controller is a simple implementation of a Controller.
 *
 * It provides methods to common features needed in controllers.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class NotificationSMSController extends Controller
{
    /**
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\OptionDefinitionException
     * @throws \Symfony\Component\OptionsResolver\Exception\MissingOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     * @throws \Symfony\Component\OptionsResolver\Exception\NoSuchOptionException
     * @throws \LogicException
     */
    public function manageAction(User $user) {
        $request = Request::createFromGlobals();

        $notificationSMS = $user->getNotificationSms();
        $em = $this->getDoctrine()->getRepository(UserNotificationSms::class);
        /** @var NotificationManager $notificationManager */
        $notificationManager = $this->get('cab_user.manager.notificationSMS_manager');

        if (!$notificationSMS) {
            $properties = $notificationManager->getProperties();
            unset($properties['id']);
            $properties['client'] = $user;

            $userNotification = $notificationManager->createNotificationEntity($properties);
        } else {
            $userNotification = $em->find($notificationSMS);
        }

        $form = $this->createForm(UserNotificationSMSType::class, $userNotification);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $data = $form->getData();
                $notificationManager->save($data);
            } else {
                $errors = $form->getErrors();
                $listErrors = $errors->getChildren();
                foreach ($listErrors as $err) {
                    dump($err);
                }
            }
        }

        return $this->render(
            'CABUserBundle:Notification:manageSMS.html.twig', array('form' => $form->createView())
        );
    }
}