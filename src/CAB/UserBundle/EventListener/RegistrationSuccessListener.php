<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 27/08/2015
 * Time: 15:24
 */

namespace CAB\UserBundle\EventListener;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Manager\CompanyManager;
use CAB\UserBundle\Entity\User;
use CAB\UserBundle\Manager\NotificationManager;
use DotSmart\SmsBundle\Services\SendSmsService;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment as Environment;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use CAB\CourseBundle\Manager\CourseManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class RegistrationSuccessListener
 *
 * @package CAB\UserBundle\EventListener
 */
class RegistrationSuccessListener implements EventSubscriberInterface
{
    /**
     * @var
     */
    private $um;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Translator
     */
    private $trans;

    /**
     * @var SendSmsService
     */
    private $sms;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var CourseManager
     */
    private $courseManager;

    /**
     * @var CourseManager
     */
    private $companyManager;

    /**
     * @var securityTokenStorage
     */
    private $securityTokenStorage;

    /**
     * @var AuthorizationCheckerInterface $authorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @var NotificationManager $notificationManager
     */
    protected $notificationManager;

    /**
     * @param UserManager         $userManager
     * @param \Swift_Mailer       $mailer
     * @param TranslatorInterface $translation
     * @param SendSmsService      $smsService
     * @param Environment         $twig
     * @param Router              $router
     * @param CourseManager       $courseManager
     * @param CompanyManager      $companyManager
     * @param TokenStorage        $securityTokenStorage
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param NotificationManager $notificationManager
     */
    public function __construct(
        UserManager $userManager,
        \Swift_Mailer $mailer,
        TranslatorInterface $translation,
        SendSmsService $smsService,
        Environment $twig,
        Router $router,
        CourseManager $courseManager,
        CompanyManager $companyManager,
        TokenStorage $securityTokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        NotificationManager $notificationManager
    ) {
        $this->um = $userManager;
        $this->mailer = $mailer;
        $this->trans = $translation;
        $this->sms = $smsService;
        $this->twig = $twig;
        $this->router = $router;
        $this->courseManager = $courseManager;
        $this->companyManager = $companyManager;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->notificationManager = $notificationManager;
    }

    /**
     * Method description
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
        );
    }

    /**
     * Method description
     *
     * @param FormEvent $event
     * @throws \Twig_Error_Loader
     * @throws \InvalidArgumentException
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws \OutOfBoundsException
     * @throws \Doctrine\Common\CommonException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        /** @var User $user */
        $user = $event->getForm()->getData();
        $request = $event->getRequest();
        $mailto = $user->getEmail();

        $role = $event->getForm()->get('rolesCreated')->getData();
        $message = new \Swift_Message('Your account has create');

	    $message->setFrom('no-reply@up.taxi')
            ->setTo($mailto);

        /** @var User $currentUser */
        $currentUser = $this->securityTokenStorage->getToken()->getUser();

        if ($role == 'ROLE_CUSTOMER') {
            if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                /** @var Company $company */
                $company = $this->companyManager->loadDefaultCompany();
            } elseif ($this->authorizationChecker->isGranted('ROLE_ADMINCOMPANY')) {
                /** @var Company $company */
                $company = $currentUser->getContacts()->first();
            } elseif ($this->authorizationChecker->isGranted('ROLE_AGENT')) {
                /** @var Company $company */
                $company = $currentUser->getAgentCompany();
            } elseif ($this->authorizationChecker->isGranted('ROLE_DRIVER')) {
                /** @var Company $company */
                $company = $currentUser->getDriverCompany();
            } elseif ($this->authorizationChecker->isGranted('ROLE_COMPANYBUSINESS')) {
                /** @var Company $company */
                $company = $currentUser->getBusinessCompany();
            } else {
                $company = null;
            }

            $user->setCustomerCompany($company);
            $user->setEnabled(true);
            // Mail render
            $message->setBody(
                $this->twig->render('CABUserBundle:Registration:email.html.twig', array('user' => $user)),
                'text/html'
            );

        } else {
            if($role == 'ROLE_DRIVER'){
                $user->setEnabled(false);
            }

            $message->setBody(
                $this->twig->render('CABUserBundle:Registration:email.driver.html.twig', array('user' => $user)),
                'text/html'
            );
        }

        $user->setRoles(array($role));

        $this->um->updateUser($user);
        //set NotificationMail
        $paramsNotification = [
            'client' => $user,
        ];

        $notification = $propertiesNotification = $this->notificationManager->setNotificationForUser($user);
        $user->setNotificationEmail($notification);

        $this->um->updateUser($user);
        $this->mailer->send($message);
        // Send sms to new registered client
        $messagesms = $this->trans->trans(
            'Welcome to UP, transfers easily and with professionals.',
            array(),
            'frontend'
        );

        /*if ($user && !empty($user->getPhoneMobile())) {
            $result = $this->sms->send(
                array(
                    'user_id' => $user->getId(),
                    'designation' => 'Registrar',
                    'message' => $messagesms,
                    'numbers' => str_replace("-", "", trim($user->getPhoneMobile())),
                )
            );
        }*/
        if($request->isXmlHttpRequest()) {
            $response = new JsonResponse();
            $textOption = $user->getFirstName().' '.$user->getLastName();
            $response->setData(
                array(
                    'status' => 1,
                    'id' => $user->getId(),
                    'text' => $textOption,
                    'role' => $role,
                )
            );

            $event->setResponse($response);
        }

        if ($request->request->has('reg_booking')) {
            $session = $request->getSession();
            if ($session->has('obj_course')) {
                $course = $session->get('obj_course');
                $this->courseManager->setAttr($course, 'client', $user);
                $this->courseManager->saveCourse($course);
                $user->setRoles(array('ROLE_CUSTOMER'));
                $this->um->updateUser($user);
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->securityTokenStorage->setToken($token);

                $url = $this->router->generate('finalize_booking', array('courseID' => $course->getId()));
            } else {
                $url = $this->router->generate('fos_user_registration_confirmed');
            }
            $response = new RedirectResponse($url);
            $event->setResponse($response);
        }
    }
}