<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 04/01/2016
 * Time: 11:55
 */

namespace CAB\UserBundle\Redirection;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // Get list of roles for current user
        $roles = $token->getRoles();
        $user = $token->getUser()->getId();
        // Tranform this list in array
        $rolesTab = array_map(
            function ($role) {
                return $role->getRole();
            },
            $roles
        );

        if (in_array('ROLE_DRIVER', $rolesTab, true)) {

            $redirection = new RedirectResponse($this->router->generate('sonata_driver_calendar', array('id' =>$user)));
        } else {
            // If is a admin or super admin we redirect to the backoffice area
            if (!in_array('ROLE_CUSTOMER', $rolesTab, true)) {
                if (strpos($request->get('_target_path'), 'admin') !== false) {
                    $redirection = new RedirectResponse($request->get('_target_path'));
                } else {
                    $redirection = new RedirectResponse($this->router->generate('sonata_admin_dashboard'));
                }
            } elseif (in_array('ROLE_CUSTOMER', $rolesTab, true)) {
                if ($request->getSession()->get('obj_course')) {
                    $oCourse = $request->getSession()->get('obj_course');
                    $redirection = new RedirectResponse(
                        $this->router->generate('finalize_booking', array('courseID' => $oCourse->getId()))
                    );
                } else {
                    $redirection = new RedirectResponse($this->router->generate('homepage'));
                }

            } elseif ($request->get('_target_path') != '') {

                $redirection = new RedirectResponse($request->get('_target_path'));
            } else {
                $redirection = new RedirectResponse($this->router->generate('homepage'));
            }
        }

        return $redirection;
    }
}

