<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CAB\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Tele type.
 *
 *
 */
class TelType extends AbstractType {
    public function getParent() {
	    return TextType::class;
    }
    public function getName() {
        return 'tel';
    }
	public function getBlockPrefix()
	{
		return 'tel';
	}

}