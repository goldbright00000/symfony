<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CAB\UserBundle\Form\Type;

use Application\Sonata\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

/**
 * Class ProfileFormType
 *
 * @package CAB\UserBundle\Form\Type
 */
class NotificationEmailFormType extends AbstractType
{

    /**
     * @var User
     */
    protected $loggedInUser;

    protected $idnotification;

    /**
     * Constructor.
     *
     * @param User $loggedInUser
     */
    public function __construct($loggedInUser,$idnotification) {
        $this->loggedInUser = $loggedInUser;
        $this->idnotification = $idnotification;

    }



    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildUserForm($builder, $options);

    }

    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder builder form
     * @param array                $options list of options
     *
     * @return void
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('statuscreated','checkbox')
            ->add('statuscancelbyclient','checkbox')
            ->add('statusaffected','checkbox')
            ->add('statusdone','checkbox')
            ->add('statusacceptedbydriver','checkbox')
            ->add('statusarriveddriver','checkbox')
            ->add('statuscancelbydriver','checkbox')
            ->add('statusdrivergotoclient','checkbox');
    }

    /**
     * Configures form's options.
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(
            [
                'data_class' => 'CAB\UserBundle\Entity\UserNotificationEmail',
            ]
        );
    }
}
