<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 14/08/2015
 * Time: 01:21
 */
namespace CAB\UserBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ApiRegistrationFormType extends BaseType
{
    private $tokenStorage;
    private $roleUser;
    private $api;

    public function __construct(TokenStorageInterface $tokenStorage, $role = null, $api = false)
    {
        $this->tokenStorage = $tokenStorage;
        $this->roleUser = $role;
        $this->api      = $api;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $role     = $this->roleUser !== null ? $this->roleUser : 'ROLE_USER';
        $textType = (false !== $this->api ? TextType::class : HiddenType::class);

        // add your custom field
        $builder->add('email')
            ->add('firstName')
            ->add('username')
            ->add('plainPassword')
            ->add('lastName')
            ->add('address')
            ->add('postCode',$textType)
            ->add('city',$textType)
            ->add('country',$textType)
            ->add('latuser',$textType)
            ->add('lnguser',$textType)
            ->add('phoneMobile','tel')
            ->add('rolesCreated', 'hidden', array('mapped' => false, 'data' => $role))
            ->add('submit', 'submit', array(
            'attr' => array('class' => 'btn color medium full green')
        ));

        if (false !== $this->api) {
            $builder->add('plainPassword', 'password');
        }

       //$builder->addEventSubscriber(new AddRoleFieldSubscriber());
        // grab the user, do a quick sanity check that one exists
        $currentuser = $this->tokenStorage !== null ? $this->tokenStorage->getToken()->getRoles() : '';
    }

    public function getName()
    {
        return 'cab_user_registration';
    }
	public function getBlockPrefix()
	{
		return 'cab_user_registration';
	}
    public function configureOptions(OptionsResolver $resolver)
    {
        if (false !== $this->api) {
            $resolver->setDefaults(array(
                'data_class' => 'CAB\UserBundle\Entity\User',
                'intention'  => 'registration',
                'csrf_protection' => false
            ));
        } else {
            $resolver->setDefaults(array(
                'data_class' => 'CAB\UserBundle\Entity\User'
            ));
        }

    }

}