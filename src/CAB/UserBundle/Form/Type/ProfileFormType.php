<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CAB\UserBundle\Form\Type;

use CAB\UserBundle\Form\TelType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

/**
 * Class ProfileFormType
 *
 * @package CAB\UserBundle\Form\Type
 */
class ProfileFormType extends AbstractType
{
    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildUserForm($builder, $options);

        #$builder->add('current_password', 'password', array(
        #    'label' => 'form.current_password',
        #    'translation_domain' => 'FOSUserBundle',
        #    'mapped' => false,
        #    'constraints' => new UserPassword(),
        #));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => $this->class,
                'intention' => 'profile',
            )
        );
    }

    // BC for SF < 2.7
    /**
     * Set default options method
     *
     * @param OptionsResolverInterface $resolver resolver
     *
     * @return void
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * Get name method
     * @return string
     */
    public function getName()
    {
        return 'cab_user_profile';
    }
	public function getBlockPrefix()
	{
		return 'cab_user_profile';
	}
    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder builder form
     * @param array                $options list of options
     *
     * @return void
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle','disabled' => 'disabled'))
            ->add('email', EmailType::class, array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('nameCompany', TextType::class,array('required' => false))
            ->add('siren', TextType::class,array('required' => false))
            ->add('phoneCompany', TelType::class,array('required' => false))
            ->add(
                'isHandicap',
                ChoiceType::class,
                array(
                    'expanded' => false,
                    'choices' => array('NO' => '0', 'YES' => '1'),
                )
            )
            ->add('address')
            ->add('addressComplement')
            ->add('city')
            ->add('postCode')
            ->add('country')
            ->add('latUser',HiddenType::class)
            ->add('lngUser',HiddenType::class)
            ->add('phoneMobile', TelType::class)
            ->add('phoneHome', TelType::class,array('required' => false))
            ->add('phoneWork', TelType::class,array('required' => false))
            ->add(
                'avatar',
                FileType::class,
                array(
                    'data_class' => null,
                    'property_path' => 'avatar',
                    'required' => false,
                )
            );;
    }
}
