<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 17/11/2017
 * Time: 01:37
 */

namespace CAB\UserBundle\Form\Type;

use CAB\UserBundle\Entity\UserNotificationSms;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UserNotificationSMSType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('statusCreated')
            ->add('statusCancelByClient')
            ->add('statusAffected')
            ->add('statusDone')
            ->add('statusAcceptedByDriver')
            ->add('statusCancelByDriver')
            ->add('statusArrivedDriver')
            ->add('statusDriverGoToClient')
            ->add('save', SubmitType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', UserNotificationSms::class);
    }
}