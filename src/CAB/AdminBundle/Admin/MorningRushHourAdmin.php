<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda (benhenda.med@gmail.com)
 * Date: 29/11/2015
 * Time: 20:20
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\AdminBundle\Admin\DataTransformer\TarifToIntTransformer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MorningRushHourAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;
    protected $baseRouteName = 'sonata_morning_rush_hour';
    protected $baseRoutePattern = 'morning_rush_hour';
    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create Tarif',
            'edit' => 'Edit Tarif',
            'list' => 'Tarif List',
            'show' => 'Show Tarif',
            'default' => 'Tarif dashboard',
        );
        $this->description = 'Manage tarifs. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Fields to be shown on create/edit forms
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        if ($this->getRoot()->getSubject()->getId()) {
            $subject = $this->getRoot()->getSubject()->getId();
            $oTarif = $em->getRepository('CABCourseBundle:Tarif')->find($subject);
            $formMapper->add('morningRushTarif;', 'hidden', array());
            $formBuilder = $formMapper->getFormBuilder();
            $formBuilder->get('morningRushTarif')->addModelTransformer(new TarifToIntTransformer($em, $oTarif));
        }
        $formBuilder->addEventSubscriber(new AddNameFieldSubscriber());
        $formMapper
            ->add('mondayFrom', 'time', array(
                'label' => false,
                'placeholder' => false,
                'input' => 'datetime',
                'widget' => 'choice',
            ))
            ->add('mondayTo', 'time', array(
                'label' => false,
                'placeholder' => false,
                'input' => 'datetime',
                'widget' => 'choice',
            ))
            ->add('mondayMorningIncrease', 'percent', array(
                'label' => false,
                'scale' => 2,
            ));
    }
}