<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\ServicetransportRepository;
use CAB\ApiBundle\Entity\TrackLogin;
use CAB\ApiBundle\Entity\TrackLoginRepository;
use CAB\UserBundle\Entity\User;
use CAB\UserBundle\Form\TelType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use CAB\CourseBundle\Entity\ZoneRepository;

use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Presta\ImageBundle\Form\Type\ImageType;
use Symfony\Component\HttpFoundation\Request;

class DriverAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;

    protected $baseRouteName = 'sonata_driver';
    protected $baseRoutePattern = 'driver';

    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage driver. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';
        $this->pageTitle = [
            'create'  => 'Create Driver',
            'edit'    => 'Edit driver',
            'list'    => 'Drivers List',
            'show'    => 'Show driver',
            'default' => 'Driver dashboard'
        ];
    }


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $name
     *
     * @return null|string
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'CABAdminBundle:CRUD:driver_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:driver_show.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }


    /**
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            [
                'CABAdminBundle:Form:form_admin_fields.html.twig',
            ]
        );
    }

    /**
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context = 'list');
        $alias = $query->getRootAliases()[0];
        $query->andWhere($alias . '.roles LIKE :role');
        $query->setParameter('role', '%ROLE_DRIVER%');


        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            //Get the company managed by the current user
            //$companyAdmin = $currentUser->getContacts();
            $companyAdmin = $currentUser->getContacts()->getIterator();

            $company = '';
            while ($companyAdmin->valid()) {
                $company .= $companyAdmin->current()->getId();
                if ($companyAdmin->key() < $companyAdmin->count() - 1) {
                    $company .= ',';
                }
                $companyAdmin->next();
            }
            $query->andWhere(
                $query->expr()->in(
                    $query->getRootAlias() . '.driverCompany',
                    $company
                )
            );

            return $query;
        }

        return $query;
    }

    /**
     * @param User $oDriver
     *
     * @return mixed|void
     */
    public function prePersist($oDriver)
    {
        $oDriver->setRoles(['ROLE_DRIVER']);

        $this->uploadDocs($oDriver);
    }

    /*public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            //Get the company managed by the current user
            $companyAdmin = $currentUser->getContacts();
            if (!$this->getSubject()) {
                $instance->setUserCompany($companyAdmin);
            }
        }

        return $instance;
    }*/

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'CAB\UserBundle\Entity\User',
            ]
        );
    }

    // Fields to be shown on create/edit forms

    /**
     * @param FormMapper $formMapper
     *
     * @throws \Exception
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this driver!', 'EDIT');
        } else {
            $this->checkAccess('You are not allowed to create a new driver!', 'CREATE');
        }
        $this->pageTitle = [
            'create' => 'Create Driver',
            'edit'   => 'Edit driver',
        ];
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            $formMapper->add('driverCompany', HiddenType::class, [], ['admin_code' => 'cab.admin.company',]);
            // if edit driver
            if ($this->getSubject()->getId()) {
                if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_ADMINCOMPANY')
                ) {
                    $companyAdmin = $currentUser->getContacts()->first();
                } else {
                    $companyAdmin = $currentUser->getDriverCompany();
                }
                if (!$companyAdmin) {
                    throw new \Exception('No company for current user!');
                }

                $formBuilder->get('driverCompany')->addModelTransformer(
                    new CompanyToIntTransformer($em, $companyAdmin)
                );
            } else {
                $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                    ->getToken()->getUser();

                $companyAdmin = $currentUser->getContacts()->first();

                $formBuilder->get('driverCompany')->addModelTransformer(
                    new CompanyToIntTransformer($em, $companyAdmin)
                );
            }
            $formMapper->add(
                'zones',
                null,
                [
                    'class'         => 'CABCourseBundle:Zone',
                    'query_builder' => function (ZoneRepository $cr) {
                        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                            ->getToken()->getUser();

                        return $cr->getZonesByCompany($currentUser);
                    },
                    'label'         => 'Zones',
                    'multiple'      => true,
                    'required'      => false,
                ]
            )->add(
                'servicesTransport',
                EntityType::class,
                [
                    'class'         => 'CABCourseBundle:ServiceTransport',
                    'label'         => 'Services transport',
                    'multiple'      => true,
                    'required'      => false,
                    'query_builder' => function (ServicetransportRepository $sr) use ($companyAdmin) {
                        return $sr
                            ->createQueryBuilder('s')
                            ->where('s.company = ?1')
                            ->setParameter(1, $companyAdmin)
	                        ->setCacheable(true)->setCacheRegion('cache_long_time');
                    },
                ]
            );
        } else {
            if ($this->getRoot()->getSubject()->getId()) {
                $defaultCompany = $this->getRoot()->getSubject()->getDriverCompany();
            } else {
                $defaultCompany = null;
            }
            $formMapper
                ->add(
                    'driverCompany',
                    null,
                    [
                        'class'    => 'CABCourseBundle:Company',
                        'label'    => 'Company',
                        'multiple' => false,
                        'required' => false,
                    ],
                    ['admin_code' => 'cab.admin.company',]
                );
            $formModifier = function (FormInterface $form, Company $oCompany = null, $defaultCompany) {
                $company = null === $oCompany ? $defaultCompany : $oCompany;

                $form->add(
                    'zones',
                    null,
                    [
                        'class'         => 'CABCourseBundle:Zone',
                        'label'         => 'Zones',
                        'multiple'      => true,
                        'label_attr'    => [
                            'class' => 'control-label required col-sm-3',
                        ],
                        'required'      => false,
                        'placeholder'   => '',
                        'query_builder' => function (ZoneRepository $zr) use ($company) {
                            return $zr
                                ->createQueryBuilder('z')
                                ->where('z.companyZone = ?1')
                                ->setParameter(1, $company)
	                            ->setCacheable(true)->setCacheRegion('cache_long_time');
                        },
                    ]
                )
                    ->add(
                        'servicesTransport',
                        EntityType::class,
                        [
                            'class'         => 'CABCourseBundle:ServiceTransport',
                            'label'         => 'Services transport',
                            'multiple'      => true,
                            'required'      => false,
                            'query_builder' => function (ServicetransportRepository $sr) use ($company) {
                                return $sr
                                    ->createQueryBuilder('s')
                                    ->where('s.company = ?1')
                                    ->setParameter(1, $company)
	                                ->setCacheable(true)->setCacheRegion('cache_long_time');
                            },
                        ]
                    );
            };
            $formBuilder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($formModifier, $defaultCompany) {
                    $data = $event->getData();

                    if ($data !== null) {
                        if ($data->getDriverCompany() === null) {
                            $oCompany = null;
                        } else {
                            $oCompany = $data->getDriverCompany();
                        }
                        $formModifier($event->getForm(), $oCompany, $defaultCompany);
                    }
                }
            );

            $formBuilder->get('driverCompany')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier, $defaultCompany) {
                    // It's important here to fetch $event->getForm()->getData(), as
                    // $event->getData() will get you the client data (that is, the ID)
                    $oCompany = $event->getForm()->getData();

                    // since we've added the listener to the child, we'll have to pass on
                    // the parent to the callback functions!
                    $formModifier($event->getForm()->getParent(), $oCompany, $defaultCompany);
                }
            );
        }

        $formMapper
            ->add('firstName', TextType::class, ['label' => 'Firstname'])
            ->add('lastName', TextType::class, ['label' => 'LastName'])
            ->add('enabled', null, ['label' => 'is Enabled', 'required' => false])
            ->add('email', TextType::class)
            ->add('username', TextType::class)
            ->add(
                'plainPassword',
                RepeatedType::class,
                [
                    'type'            => PasswordType::class,
                    'options'         => ['translation_domain' => 'FOSUserBundle'],
                    'first_options'   => ['label' => 'form.password'],
                    'second_options'  => ['label' => 'form.password_confirmation'],
                    'invalid_message' => 'fos_user.password.mismatch',
                    'required'        => false,
                ]
            )
            ->add('address', TextType::class, ['attr' => ['class' => 'maps_input']])
            ->add('city', TextType::class, ['label' => 'City'])
            ->add('postCode', TextType::class, ['label' => 'Postal code'])
            ->add('country', TextType::class, ['label' => 'Country'])
            ->add(
                'phoneMobile',
                TextType::class,
                [
                    'label'      => 'Mobile',
                    'label_attr' => ['class' => ' control-label col-sm-4'],
                    'attr'       => [
                        'placeholder' => 'Mobile phone',
                    ],
                ]
            )
            ->add(
                'phoneWork',
                TextType::class,
                [
                    'label'      => 'Phone work',
                    'label_attr' => ['class' => ' control-label col-sm-4'],
                    'attr'       => [
                        'placeholder' => 'Work phone',
                    ],
                ]
            )
            ->add(
                'phoneHome',
                TelType::class,
                [
                    'label'      => 'Phone home',
                    'label_attr' => ['class' => ' control-label col-sm-4'],
                    'attr'       => [
                        'placeholder' => 'Home phone',
                    ],
                ]
            )
            ->add('file', FileType::class, ['label' => 'Photo', 'required' => false])
            ->add('docDl', FileType::class, ['label' => 'Driver license', 'required' => false])
            ->add('docMe', FileType::class, ['label' => 'Mdical examination', 'required' => false])
            ->add('docLj', FileType::class, ['label' => 'Locker judiciare', 'required' => false])
            ->add('docPi', FileType::class, ['label' => 'Identity Piece', 'required' => false])
            ->add('privateNote', TextareaType::class, ['label' => 'Note privé', 'required' => false, 'attr' => ['class' => 'ckeditor', 'rows' => 10]])
            ->add('publicNote', TextareaType::class, ['label' => 'Note', 'required' => false, 'attr' => ['class' => 'ckeditor', 'rows' => 10]])
            ->add('isCredit', CheckboxType::class, ['label' => 'isCredit']);
        if ($this->getRoot()->getSubject()->getId()) {
            $formMapper->
            add('driverWorkDay', AdminType::class, ['delete' => false, 'label' => false])
                ->add(
                    'nightWorkingDriver',
                    CheckboxType::class,
                    [
                        "attr"     => ['class' => 'checkbox-night'],
                        'label'    => 'Night Working Driver',
                        'required' => false,
                    ]
                )
                ->add(
                    'driverWorkNight',
                    AdminType::class,
                    ['required' => false, 'delete' => false, 'label' => false]
                );
        }

        $formMapper
            ->add(
                'fileLicence',
                ImageType::class,
                [
                    'label'    => 'Doc licence',
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add(
                'fileLicencePDF',
                FileType::class,
                [
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add('fileLicenceType',
                ChoiceType::class,
                [
                    'choices'           => [
                        'PDF'   => '1',
                        'Image' => '2'
                    ],
                    'label'             => 'Type doc',
                    'required'          => true,
                    'choices_as_values' => true,
                    'multiple'          => false,
                    'expanded'          => true,
                    'mapped'            => false,
                    'data'              => 1
                ]
            )
            ->add(
                'fileVisitMed',
                ImageType::class,
                [
                    'label'    => 'Doc Visit med',
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add(
                'fileVisitMedPDF',
                FileType::class,
                [
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add('fileVisitMedType',
                  ChoiceType::class,
                [
                    'choices'           => [
                        'PDF'   => '1',
                        'Image' => '2'
                    ],
                    'label'             => 'Type doc',
                    'required'          => true,
                    'choices_as_values' => true,
                    'multiple'          => false,
                    'expanded'          => true,
                    'mapped'            => false,
                    'data'              => 1
                ]
            )
            ->add(
                'fileIdentityPiece',
                ImageType::class,
                [
                    'label'    => 'Doc identity piece',
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add(
                'fileIdentityPiecePDF',
                FileType::class,
                [
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add('fileIdentityPieceType',
                  ChoiceType::class,
                [
                    'choices'           => [
                        'PDF'   => '1',
                        'Image' => '2'
                    ],
                    'label'             => 'Type doc',
                    'required'          => true,
                    'choices_as_values' => true,
                    'multiple'          => false,
                    'expanded'          => true,
                    'mapped'            => false,
                    'data'              => 1
                ]
            )
            ->add(
                'fileCartePro',
                ImageType::class,
                [
                    'label'    => 'Doc carte pro',
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add(
                'fileCarteProPDF',
                FileType::class,
                [
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->add('fileCarteProType',
                  ChoiceType::class,
                [
                    'choices'           => [
                        'PDF'   => '1',
                        'Image' => '2'
                    ],
                    'label'             => 'Type doc',
                    'required'          => true,
                    'choices_as_values' => true,
                    'multiple'          => false,
                    'expanded'          => true,
                    'mapped'            => false,
                    'data'              => 1
                ]
            )
            ->add('expireDocLicence', DatePickerType::class, [
                'label'    => 'Date Exp licence',
                'required' => false,
                'format'   => 'dd/MM/yyyy'
            ])
            ->add('expireDocVisitMed', DatePickerType::class, [
                'label'    => 'Date Exp visit med',
                'widget'   => 'single_text',
                'required' => false,
                'format'   => 'dd/MM/yyyy'
            ])
            ->add('expireDocIdentityPiece', DatePickerType::class, [
                'label'    => 'Date Exp identity piece',
                'widget'   => 'single_text',
                'required' => false,
                'format'   => 'dd/MM/yyyy'
            ])
            ->add('expireDocCartePro', DatePickerType::class, [
                'label'    => 'Date Exp carte pro',
                'widget'   => 'single_text',
                'required' => false,
                'format'   => 'dd/MM/yyyy'
            ]);
    }

    // Fields to be shown on filter forms

    /**
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('phoneMobile')
            ->add('enabled')
            ->add('email');

    }

    // Fields to be shown on lists

    /**
     * @param ListMapper $listMapper
     *
     * @throws \RuntimeException
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id', null);

        $listMapper
            ->addIdentifier(
                'username',
                null,
                [
                    'route' => [
                        'name' => 'show',
                    ],
                ]
            )
            ->add(
                'driverCompany',
                'sonata_type_model',
                [
                    'label'      => 'Company',
                    'admin_code' => 'cab.admin.company',
                ]

            )
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('enabled')
            ->add('address')
            ->add(
                'avatar',
                null,
                [
                    'template' => 'CABAdminBundle:Custom:photo_list.html.twig',
                    'data'     => '',
                    'dir'      => '/uploads/avatar/',
                ]
            )
            ->add(
                'trackUser'
            )
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'show'     => [],
                        'edit'     => [],
                        'delete'   => [],
                        'calendar' => [],
                    ],
                ]
            );

    }

    /**
     * @param ShowMapper $showMapper
     *
     * @throws \Exception $exception
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to show this driver!', 'VIEW');
        }
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('firstName')
            ->add('lastName')
            ->add('driverCompany', null, ['admin_code' => 'cab.admin.company',])
            ->add('enabled')
            ->add('email')
            ->add('address')
            ->add('zones')
            ->add('servicesTransport')
            ->add(
                'avatar',
                null,
                [
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label'    => 'Photo',
                    'filter'   => 'logo',
                    'dir'      => '/uploads/avatar/',
                ]
            )
            ->add(
                'drivingLicense',
                null,
                [
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label'    => 'Driving license',
                    'filter'   => 'big_img',
                    'dir'      => '/uploads/doc_driver/',
                ]
            )
            ->add(
                'medicalExamination',
                null,
                [
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label'    => 'Medical examination',
                    'filter'   => 'big_img',
                    'dir'      => '/uploads/doc_driver/',
                ]
            )
            ->add(
                'lockerJudiciare',
                null,
                [
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label'    => 'Locker judiciare',
                    'filter'   => 'big_img',
                    'dir'      => '/uploads/doc_driver/',
                ]
            )
            ->add(
                'pieceIdentity',
                null,
                [
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label'    => 'Piece identity',
                    'filter'   => 'big_img',
                    'dir'      => '/uploads/doc_driver/',
                ]
            );

    }

    /**
     * {@inheritDoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('calendar', $this->getRouterIdParameter() . '/calendar');
        $collection->add('update_image', $this->getRouterIdParameter() . '/update_image');
    }


    public function getExportFields()
    {
        return ['id', 'email', 'username', 'firstName', 'lastName', 'enabled'];
    }

    public function checkAccess($message = " Access denied", $attribute = 'LIST')
    {
    	//dump($this->getRequest()->get('_sonata_admin'));die;
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();



        // if the user is not a super admin we check the access
			if($this->getSubject() != null)
			{
				if(!$this->getConfigurationPool()->getContainer()->get('cab_course.security_authorization_voter.driver_voter')->checkGranted($attribute, $this->getSubject(), $currentUser))
				{
					throw new AccessDeniedException($message);
				}
			}


    }


    /**
     * @param User $user
     *
     * @return mixed|void
     */
    public function preUpdate($user)
    {
        $this->uploadDocs($user);
    }

    /**
     * @param $user User
     */
    private function uploadDocs($user)
    {
        if ($this->getForm()->get('fileLicenceType')->getData() == 1 && $this->getForm()->get('fileLicencePDF')->getData()) {
            $user->setFileLicence($this->getForm()->get('fileLicencePDF')->getData());
        } elseif ($this->getForm()->get('fileLicenceType')->getData() == 2 && $this->getForm()->get('fileLicence')->getData()) {
            $user->setFileLicence($this->getForm()->get('fileLicence')->getData());
        }

        if ($this->getForm()->get('fileVisitMedType')->getData() == 1 && $this->getForm()->get('fileVisitMedPDF')->getData()) {
            $user->setFileVisitMed($this->getForm()->get('fileVisitMedPDF')->getData());
        } elseif ($this->getForm()->get('fileVisitMedType')->getData() == 2 && $this->getForm()->get('fileVisitMed')->getData()) {
            $user->setFileVisitMed($this->getForm()->get('fileVisitMed')->getData());
        }

        if ($this->getForm()->get('fileIdentityPieceType')->getData() == 1 && $this->getForm()->get('fileIdentityPiecePDF')->getData()) {
            $user->setFileIdentityPiece($this->getForm()->get('fileIdentityPiecePDF')->getData());
        } elseif ($this->getForm()->get('fileIdentityPieceType')->getData() == 2 && $this->getForm()->get('fileIdentityPiece')->getData()) {
            $user->setFileIdentityPiece($this->getForm()->get('fileIdentityPiece')->getData());
        }

        if ($this->getForm()->get('fileCarteProType')->getData() == 1 && $this->getForm()->get('fileCarteProPDF')->getData()) {
            $user->setFileCartePro($this->getForm()->get('fileCarteProPDF')->getData());
        } elseif ($this->getForm()->get('fileCarteProType')->getData() == 2 && $this->getForm()->get('fileCartePro')->getData()) {
            $user->setFileCartePro($this->getForm()->get('fileCartePro')->getData());
        }
    }
}