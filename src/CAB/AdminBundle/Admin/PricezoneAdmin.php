<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\CourseBundle\Entity\ServiceTransport;
use CAB\CourseBundle\Entity\ServicetransportRepository;
use CAB\CourseBundle\Entity\ZoneRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use CAB\CourseBundle\Entity\Zone;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\EqualType;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Exception\BadMethodCallException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CAB\AdminBundle\Admin\DataTransformer\UserToIntTransformer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\DoctrineORMAdminBundle\Datagrid\Pager as SonataPager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\AdminBundle\Model\ORM\ModelManager;



/**
 * Class PriceByZoneAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class PricezoneAdmin extends AbstractAdmin {

    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    protected $container;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_pricezone';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'pricezone';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName) {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create pricezone',
            'edit' => 'Edit pricezone',
            'list' => 'pricezone List',
            'show' => 'Show pricezone',
            'default' => 'pricezone Dashboard',
        );
    }

    /**
     * Method description
     *
     * @return array
     */
    public function getFormTheme() {
        return array_merge(
                parent::getFormTheme(), array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }

    /**
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $dispatcher
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function setContainer(ContainerInterface $container, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
    }

    /**
     * Method description
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name) {
        switch ($name) {
            default:
                return parent::getTemplate($name);
                break;
        }

        return parent::getTemplate($name);
    }

    // Fields to be shown on create/edit forms
    /**
     * Method description
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $this->pageTitle = array(
            'create' => 'Create pricezone sncf',
            'edit' => 'Edit pricezone sncf',
        );
        $container = $this->configurationPool->getContainer();
        $currentUser = $container->get('security.token_storage')->getToken()->getUser();

        /** @var User $currentUser */
        //$currentUser = $this->container->get('security.token_storage')->getToken()->getUser();
        //$authChecker = $this->container->get('security.authorization_checker');

        /** @var CompanyManager $companyManager */
        $companyManager = $container->get('cab.company.manager');
        $companies = $companyManager->getCompaniesByRole(false, true);

        $currentUserToken = $container->get('security.token_storage');
        $objectUser = $currentUserToken->getToken();

        $cCompany = $currentUser->getContacts();
        $oCompany = $cCompany->first();

     $formMapper

         ->add(
             'zone',
             'entity',
             [
                 'class'         => 'CABCourseBundle:Zone',
                 'label'         => 'Zone',
                 'multiple'      => false,
                 'required'      => false,
                 'query_builder' => function (ZoneRepository $sr) use ($oCompany) {
                     return $sr
                         ->createQueryBuilder('s')
                         ->where('s.companyZone = ?1')
                         ->setParameter(1, $oCompany)
	                    ->setCacheable(true)->setCacheRegion('cache_long_time');
                 },
             ]
         )
         /* A FAIRE
         ->add(
             'service',
             'entity',
             [
                 'class'         => 'CABCourseBundle:ServiceTransport',
                 'label'         => 'Service',
                 'multiple'      => true,
                 'required'      => false,
                 'query_builder' => function (ServicetransportRepository $sr) use ($oCompany) {
                     return $sr
                         ->createQueryBuilder('s')
                         ->where('s.company = ?1')
                         ->setParameter(1, $oCompany);
                 },
             ]
         )*/
         ->add('departure_SNCF', null, array('label' => 'departure', 'required' => true))
         ->add('arrival_SNCF', null, array('label' => 'arrival', 'required' => true))
         ->add('information',CKEditorType::class, array('label' => 'linkdescription', 'required' => true))
         ->add('priceday', 'text', array('label' => 'priceday HT', 'required' => true))
         ->add('hournightstart', 'time', array('label' => 'hour night start', 'required' => true))
         ->add('hournightend', 'time', array('label' => 'hour night end', 'required' => true))
         ->add('pricenight', 'text', array('label' => 'pricenight HT', 'required' => true))
         ->add('priceferie', 'text', array('label' => 'priceferie HT', 'required' => true))
         ->add('pricekmday', 'text', array('label' => 'price km day HT', 'required' => true))
         ->add('pricekmnight', 'text', array('label' => 'price km night HT', 'required' => true))
         ->add('priceattente', 'text', array('label' => 'price attente', 'required' => true));


        $formBuilder = $formMapper->getFormBuilder();
    }
    // Fields to be shown on lists
    /**
     * Method description
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id', null, array('route' => array('name' => 'show')))
            ->add('zone')
            ->add('departure_SNCF')
            ->add('arrival_SNCF')
            ->add('priceday')
            ->add('hournightstart')
            ->add('hournightend')
            ->add('pricenight')
            ->add('priceferie')
            ->add('pricekmday')
            ->add('pricekmnight')
            ->add('priceattente')
            ->add('information')
        ;
    }

    // Fields to be shown on filter forms.
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $container = $this->configurationPool->getContainer();
        $currentUser = $container->get('security.token_storage')->getToken()->getUser();


        /** @var CompanyManager $companyManager */
        $companyManager = $container->get('cab.company.manager');
        $companies = $companyManager->getCompaniesByRole(false, true);

        $currentUserToken = $container->get('security.token_storage');
        $objectUser = $currentUserToken->getToken();

        $cCompany = $currentUser->getContacts();
        $oCompany = $cCompany->first();

        $datagridMapper

            ->add('zone', null,array(
                    'field_options' => array(
                        'expanded' => true,
                        'multiple' => true,
                        'query_builder' => function (ZoneRepository $str) {
                            $currentUser = $this->container->get('security.token_storage');
                            $cCompany = $currentUser->getToken()->getUser()->getContacts();
                            $oCompany = $cCompany->first();
                            return $str->getZoneByCompany($oCompany);
                        }
                    )
                )
            )
            ->add('departure_SNCF')
            ->add('arrival_SNCF');
    }




}
