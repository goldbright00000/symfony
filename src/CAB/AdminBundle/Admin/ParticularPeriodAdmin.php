<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\CourseBundle\Entity\ZoneRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class ParticularPeriodAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;
    protected $baseRouteName = 'sonata_particular_period';
    protected $container;
    protected $dispatcher;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create Particular Period',
            'edit' => 'Edit Particular Period',
            'list' => 'Particular Periods List',
            'show' => 'Show Particular Period',
            'default' => 'Particular Period dashboard',
        );
        $this->description = 'Manage particular Period. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($oParticularDate)
    {
        $event = new GenericEvent($oParticularDate);
        $this->dispatcher->dispatch('cab.admin.event.create_particular_period', $event);
    }
    public function postUpdate($oParticularDate)
    {
        $event = new GenericEvent($oParticularDate);
        $this->dispatcher->dispatch('cab.admin.event.create_particular_period', $event);
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formBuilder = $formMapper->getFormBuilder();
        $currentUser = $this->container->get('security.token_storage');
        $em = $this->container->get('doctrine.orm.entity_manager');
        $serviceHourTarifNight = $this->container->get('cab.hour_tarif_night.manager');

        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        /*
         * Show the select option for the super admin or hidden for adminCompany role.
         */
        if ($authCheck->isGranted('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->add('company', null, array(
                    'class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) {
                        return $cr->getActiveCompanies();
                    },
                ),
                    array('admin_code' => 'cab.admin.company')
                );
        } elseif ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
            $companyManager = $this->getConfigurationPool()->getContainer()->get('cab.company.manager');
            $aCompanies = $companyManager->getCompaniesByRole(true);

            $formMapper
                ->add('company', 'entity', array(
                    'class' => 'CABCourseBundle:Company',
                    'choices' => $aCompanies,
                ),
                    array('admin_code' => 'cab.admin.company')
                );
        } elseif ($authCheck->isGranted('ROLE_AGENT')) {
            $companyAdmin = $currentUser->getAgentCompany();
            $formMapper->add('company', 'hidden', array(), array('admin_code' => 'cab.admin.company'));
            $formBuilder->get('company')->addModelTransformer(new CompanyToIntTransformer($em, $companyAdmin));
        }
        $formMapper
            ->add('namePeriod', 'text', array(
                'label' => 'Period name'
            ))
            ->add('startedDate', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'YYYY-MM-dd',
                    'model_timezone' => 'CET',
                    'required' => true,
                    'label' => false,
                    'attr' => array(
                        'class' => "input-datepicker",
                        'help' => "Date start",
                    ),
                        )
                )
                ->add('endDate', 'date', array(
                    'widget' => 'single_text',
                    'format' => 'YYYY-MM-dd',
                    'model_timezone' => 'CET',
                    'required' => true,
                    'label' => false,
                    'attr' => array(
                        'class' => "input-datepicker",
                        'help' => "Date end",
                    ),
                )
            )

            ->add('zones', 'entity', array(
                'multiple' => true,
                'attr' => array(
                    'class' => "zone-select"),
                'label_attr' => array(
                    'class' => 'control-label required col-sm-3'),
                'class' => 'CABCourseBundle:Zone',
                'placeholder' => '',
                'query_builder' => function (ZoneRepository $zr) {
                    $oUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                        ->getToken()->getUser();
                    $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');

                    return $zr->getZonesByCompany($oUser, $authCheck);
                },
            ));


    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('company', null, array('admin_code' => 'cab.admin.company'))
            ->add('startedDate')
            ->add('endDate')
            ->add('zones');
    }

    public function getExportFormats()
{
    return array_merge(parent::getExportFormats(), array('pdf'));
}

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            //->add('company', null, array('admin_code' => 'cab.admin.company'))
            ->add('namePeriod')
            ->add('startedDate')
            ->add('endDate')
            ->add('zones');
    }

     public function checkAccess($message = " Access denied",$attribute = '')
    {
        // if the user is not a super admin we check the access
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $currentObject = $this->getSubject();
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            if ($currentObject != null && $currentObject->getCompany()->getContact()->getId() != $currentUser->getId()) {
                throw new AccessDeniedException($message);
            }
        }
    }
}