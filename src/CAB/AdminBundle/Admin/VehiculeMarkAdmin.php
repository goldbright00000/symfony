<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\CourseBundle\Entity\TypeVehiculeRepository;
use CAB\CourseBundle\Entity\ServicetransportRepository;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CAB\AdminBundle\Admin\DataTransformer\VehiculeToIntTransformer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\AdminBundle\Validator\ErrorElement;
use Symfony\Component\Validator\Constraints\Image;

class VehiculeMarkAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;
    protected $baseRouteName = 'sonata_vehicule_mark';
    protected $baseRoutePattern = 'vehicle-mark';

    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create Vehicule Mark',
            'edit' => 'Edit Vehicule Mark',
            'list' => 'Vehicule Mark List',
            'show' => 'Show Vehicule Mark',
            'default' => 'Vehicule Mark dashboard'
        );
        $this->description = 'Manage Vehicule Mark. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            throw new AccessDeniedException('You are not allowed to edit this section');
        }
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();

        $formMapper
            ->add('licensePlateNumber', 'text', array('label' => 'license Plate Number', 'required' => false))
            ->add('typeVehicule', null, array(
                'class' => 'CABCourseBundle:TypeVehicule',
                'query_builder' => function (TypeVehiculeRepository $tvr) {
                    return $tvr
                        ->createQueryBuilder('tv')
                        ->orderBy('tv.nameType', 'ASC')
	                    ->setCacheable(true)->setCacheRegion('cache_long_time');
                }))
            ->add('vehiculeName', 'text', array('label' => 'Vehicule name'))
            ->add('nbPerson', 'integer', array('label' => 'Person number'))
            ->add('suitcases', 'integer', array('label' => 'Suitcases'))
            //->add('isActive', 'checkbox', array('label' => 'is Active'))
            ->add('make', 'text', array('label' => 'Make'))
            ->add('model', 'text', array('label' => 'Model', 'required' => false))
            ->add('vin', 'text', array('label' => 'VIN', 'required' => false))
            ->add('year', 'integer', array('label' => 'Year', 'required' => false))
            ->add('isOutOfService', 'checkbox', array('label' => 'Is Out Of Service', 'required' => false))
            ->add('isForHandicap', 'checkbox', array('label' => 'Adapted for handicap', 'required' => false))
            ->add('statusCar', 'choice', array('choices' => self::getCarStatus()))
            ->add('fileFront', 'file', array(
                    'label' => 'Front vehicle photo ', 'required' => true,
                    'attr' => array(
                        "accept" => "image/*"
                    )
                )
            )
            ->add('fileInterior', 'file', array(
                    'label' => 'Interior vehicle photo ', 'required' => false,
                    'attr' => array(
                        "accept" => "image/*"
                    )
                )
            )
            ->add('fileBack', 'file', array(
                    'label' => 'Back vehicle photo ', 'required' => false,
                    'attr' => array(
                        "accept" => "image/*"
                    )
                )
            )
            ->add('fileGreyRegistrationCard', 'file', array(
                    'label' => 'Grey Registration Card', 'required' => false,
                )
            )
            ->add('fileInsuranceCard', 'file', array(
                    'label' => 'Insurance Card', 'required' => false,
                )
            )
            ->add('fileVehicleTechCertif', 'file', array(
                    'label' => 'Vehicle technical certificate', 'required' => false,
                )
            );

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('vehiculeName')
            ->add('company')
            ->add('nbPerson')
            ->add('suitcases')
            //->add('isActive')
            ->add('make')
            ->add('model')
            ->add('year')
            ->add('isOutOfService')
            ->add('isForHandicap')
            ->add('statusCar');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('vehiculeName', null, array(
                'route' => array(
                    'name' => 'show'
                )
            ))
            ->add('company')
            ->add('typeVehicule')
            ->add('nbPerson')
            ->add('suitcases')
            //->add('isActive')
            ->add('make')
            ->add('model')
            ->add('year', 'text')
            ->add('isOutOfService')
            ->add('isForHandicap')
            ->add('statusCar')
            ->add('photoFront', null, array('template' => 'CABAdminBundle:Custom:photo_row.html.twig', 'data' => 'something', 'dir' => '/uploads/vehicule/'))
            ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    )
                )
            );
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to show this vehicule');
        }
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('vehiculeName')
            ->add('company')
            ->add('typeVehicule')
            ->add('nbPerson')
            ->add('suitcases')
            //->add('isActive')
            ->add('make')
            ->add('model')
            ->add('year')
            ->add('isOutOfService')
            ->add('isForHandicap')
            ->add('statusCar')
            ->add('photoFront', null, array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label' => 'Photo',
                    'filter' => 'big_img',
                    'dir' => '/uploads/vehicule/')
            )
            ->add('photoInterior', null, array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label' => 'Photo',
                    'filter' => 'big_img',
                    'dir' => '/uploads/vehicule/')
            )
            ->add('photoBack', null, array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label' => 'Photo',
                    'filter' => 'big_img',
                    'dir' => '/uploads/vehicule/')
            )
            ->add('docGreyRegistrationCard', null, array(
                    'template' => 'CABAdminBundle:Custom:document.html.twig',
                    'label' => 'Grey Registration Card',
                    'dir' => '/uploads/vehicule/documents/')
            )
            ->add('docInsuranceCard', null, array(
                    'template' => 'CABAdminBundle:Custom:document.html.twig',
                    'label' => 'Insurance Card',
                    'dir' => '/uploads/vehicule/documents/')
            )
            ->add('docVehicleTechCertif', null, array(
                    'template' => 'CABAdminBundle:Custom:document.html.twig',
                    'label' => 'Document Vehicle Technical Certificate',
                    'dir' => '/uploads/vehicule/documents/')
            );
    }

    static function getCarStatus()
    {
        return array('New' => 'New',
            'Old' => 'Old',
            'Crashed' => 'Crashed',
            'Broken down' => 'broken down');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('tarif', $this->getRouterIdParameter() . '/tarif');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Vehicule',
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cabbundle_vehicle';
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context = 'list');

        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {

            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            //get company
            $companyAdmin = $currentUser->getContacts()->first();
            $query->andWhere(
                $query->expr()->IN($query->getRootAlias() . '.company',
                    ':company')
            );
            $query->setParameter('company', $companyAdmin);

            return $query;
        } else {
            return $query;
        }

        return $query;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        if ($object->getId() === null) {
            $errorElement
                ->with('fileFront')
                ->assertNotNull(array('message' => 'The Front vehicle photo  is required, please make sure that you upload a photo'))
                ->addConstraint(new Image(array('mimeTypes' => "image/*", 'mimeTypesMessage' => 'You must upload an image !')))
                ->addConstraint(new Image(array('maxSize' => '1m')))
                ->end();
        } else {
            $errorElement
                ->with('fileFront')
                ->addConstraint(new Image(array('mimeTypes' => "image/*", 'mimeTypesMessage' => 'You must upload an image !')))
                ->addConstraint(new Image(array('maxSize' => '1m')))
                ->end();
        }
        //@TODO : Make validation on size and mimetypes for all files
    }

    public function getTemplate($name)
    {

        switch ($name) {
            case 'list':
                //return 'CABAdminBundle:CRUD:base_list.html.twig';
                return 'CABAdminBundle:CRUD:vehicle_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:vehicle_show.html.twig';
                break;
            case 'edit':
                return 'CABAdminBundle:CRUD:vehicle_edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
        return parent::getTemplate($name);
    }

}