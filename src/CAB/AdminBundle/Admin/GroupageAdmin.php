<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda
 * Date: 29/01/2017
 * Time: 14:18
 */

namespace CAB\AdminBundle\Admin;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\CourseRepository;
use CAB\CourseBundle\Entity\Tax;
use CAB\UserBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Exception\BadMethodCallException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Menage the Groupage admin.
 *
 * @author Ben Henda Mohamed <benhenda.med@gmail.com>
 */
class GroupageAdmin extends AbstractAdmin
{

    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;
    protected $dispatcher;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct ($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'sonata_groupage';
        $this->baseRoutePattern = 'groupage';

        $this->description = 'Manage groupage. Non ergo erunt homines deliciis diffluentes audiend, si quando de amicitia,
        quam nec usu nec ratione habent cognitam, disputabunt. Nam quis est, pro deorum fidem atque hominum! qui velit,
        ut neque diligat quemquam';
    }

    /**
     * @param EventDispatcherInterface $dispatcher
     *
     */
    public function setDispatcher (EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate ($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:groupage/groupage-edit.html.twig';
                break;
            case 'create':
                return 'CABAdminBundle:CRUD:groupage/groupage-edit.html.twig';
                break;
            case 'list':
                return 'CABAdminBundle:CRUD:groupage/groupage-list.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFormTheme ()
    {
        return array_merge(
            parent::getFormTheme(), array(
                'CABAdminBundle:Form:form_admin_fields.html.twig',
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Exception
     */
    public function configureOptions (OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Groupage',
                'sonata_field_description' => null,
                'label_render' => null,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist ($oCourse)
    {
    }

    public function postUpdate ($oCourse)
    {
    }

    /**
     * @param string $message
     *
     * @throws AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function checkAccess ($message = " Access denied", $attribute = 'EDIT')
    {
        $token = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken();
        // if the user is not a super admin we check the access
        /*
         if (!$this->getConfigurationPool()->getContainer()
            ->get('cab_course.security_authorization_voter.course_voter')
            ->checkGranted($attribute, $this->getSubject(), $token)
        ) {
            throw new AccessDeniedException($message);
        }
        */
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws BadMethodCallException
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException
     */
    protected function configureFormFields (FormMapper $formMapper)
    {
        $groupageAdmin = $this->getRoot();
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        $builder = $formMapper->getFormBuilder();
        /** @var User $currentUser */
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        $authChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');

        $companyAdmin = null;

        if (!$authChecker->isGranted('ROLE_ADMIN')) {
            if ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
                /** @var Company $companyAdmin */
                $companyAdmin = $currentUser->getContacts()->first();
            } elseif ($authChecker->isGranted('ROLE_AGENT')) {
                $companyAdmin = $currentUser->getAgentCompany();
            }
        }

        $formMapper
            ->add(
                'courses', null, array(
                    'label' => false,
                    'class' => 'CABCourseBundle:Course',
                    'query_builder' => function (CourseRepository $cr) use ($companyAdmin, $parentFieldDesc) {
                        $date = new \DateTime($parentFieldDesc['date_course']);
                        $address = $parentFieldDesc['address'];
                        $companyId = null;
                        if ($companyAdmin !== null) {
                            $companyId = $companyAdmin->getId();
                        }

                        return $cr->getCoursesForGroupage($address, $date, $companyId);
                    },
                )
            );
    }
}
