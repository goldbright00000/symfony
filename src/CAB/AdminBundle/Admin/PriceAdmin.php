<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Class AgentAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class PriceAdmin extends AbstractAdmin {

    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_articleprice';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'articleprice';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName) {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create articleprice',
            'edit' => 'Edit articleprice',
            'list' => 'articleprice List',
            'show' => 'Show articleprice',
            'default' => 'articleprice Dashboard',
        );
    }

    /**
     * Method description
     *
     * @return array
     */
    public function getFormTheme() {
        return array_merge(
                parent::getFormTheme(), array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * Method description
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:articleprice_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }

        return parent::getTemplate($name);
    }

    // Fields to be shown on create/edit forms
    /**
     * Method description
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $this->pageTitle = array(
            'create' => 'Create articleprice',
            'edit' => 'Edit articleprice',
        );

     $formMapper
         ->add(
             'position', 'text', array(
                 'label' => false,
                 'attr' => array(
                     'catalogue' => 'backend',
                     'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans(''),
                     'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('position'),
                 ),
                 'required' => true
             )
         )

                ->add(
                'departure', 'text', array(
                 'label' => false,
                 'attr' => array(
                     'catalogue' => 'backend',
                     'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the departure address'),
                     'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('departure address'),
                     'addon' => 'fa-map-marker',
                     'class' => 'maps_input',

                    ),
                 'required' => true
                    )
                )
                ->add('latDep', HiddenType::class, array('label' => 'latitude Departure', 'required' => true))
                ->add('longDep', HiddenType::class, array('label' => 'longiture departure', 'required' => true))
                ->add(
                    'arrival', 'text', array(
                    'label' => false,
                    'attr' => array(
                     'catalogue' => 'backend',
                     'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the arrival address'),
                     'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('arrival address'),
                     'addon' => 'fa-map-marker',
                     'class' => 'maps_input',

                    ),
                    'required' => true
                    )
                )


                ->add('latArr', HiddenType::class, array('label' => 'lattiude Arrival', 'required' => true))
                ->add('longArr', HiddenType::class, array('label' => 'longitude Arrival', 'required' => true))
                ->add('price4', 'text', array('label' => 'Price 4 pax', 'required' => true))
                ->add('price6', 'text', array('label' => 'Price 6 pax', 'required' => true))
                ->add('price12', HiddenType::class, array('label' => 'Price 12 pax', 'data' => '0', 'required' => false))
                ->add('price16', HiddenType::class, array('label' => 'Price 16 pax', 'data' => '0', 'required' => false))

                ->add('cible', 'choice', array('choices' => array(
	                'FRANCE' => '1',
                    'MAROC' => '2',
                    'ADD on PRICEADMIN' => '3',
                ), 'required' => true))

                ->add('language',  'choice', array('choices' => array(
                	'francaise' => 'fr',
                'anglais' => 'en',
                'espagnol' => 'es',
                'arabe' => 'ar',
                ), 'required' => true))

         ->add(
             'title', 'text', array(
                 'label' => false,
                 'attr' => array(
                     'catalogue' => 'backend',
                     'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('title'),
                     'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('title'),
                 ),
                 'required' => true
             )
         )
         ->add(
             'ref', 'text', array(
                 'label' => false,
                 'attr' => array(
                     'catalogue' => 'backend',
                     'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('code postal'),
                     'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('ref'),
                     ),
                 'required' => true
             )
         )
         ->add('time', 'text', array('label' => 'time', 'required' => true))
         ->add('distance', 'text', array('label' => 'distance', 'required' => true))
         ->add('metadescription', 'text', array('label' => 'metadescription', 'required' => true))
         ->add('metakeywords', 'text', array('label' => 'metakeywords', 'required' => true))

         ->add('countryrate', 'choice', array(
             'choices' => array(
                 'EUR' => 'EUR',
                 '$' => 'DOLLAR',
             ),

             'preferred_choices' => array('EUR', 'EUR'),
         ))

         ->add('linkdescription',CKEditorType::class, array('label' => 'linkdescription', 'required' => true));


        $formBuilder = $formMapper->getFormBuilder();
    }


    // Fields to be shown on lists
    /**
     * Method description
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('id', null, array('route' => array('name' => 'show')))
            ->add('ref')
            ->add('title')
            ->add('departure')
            ->add('arrival')
            ->add('time')
            ->add('distance')
            ->add('price4')
            ->add('price6')
            ->add('price12')
            ->add('price16')
            ->add('cible')
            ->add('metadescription')
            ->add('metakeywords')
            ->add('linkdescription')
            ->add('countryrate')
            ->add('language')
        ;
    }

    // Fields to be shown on filter forms.
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

        $datagridMapper
            ->add('title')
            ->add('ref');
    }


    public function checkAccess($message = ' Access denied', $attribute = 'LIST') {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                        ->getToken()->getUser();



        // if the user is not a super admin we check the access
        if (!$this->getConfigurationPool()->getContainer()
                        ->get('cab_course.security_authorization_voter.agent_voter')
                        ->checkGranted($attribute, 'CAB\UserBundle\Entity\User', $currentUser)
        ) {
            throw new AccessDeniedException($message);
        }
    }

}
