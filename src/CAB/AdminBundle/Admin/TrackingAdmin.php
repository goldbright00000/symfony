<?php

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TrackingAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'sonata_tracking';
    protected $baseRoutePattern = 'tracking';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('companyName')
            ->add('users', 'entity', array(
                'class' => 'CAB\UserBundle\Entity\User',
                'choice_label' => 'username'
            ));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {

    }

    /*
     * Set the template
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
            case 'edit':
            case 'create':
                return 'CABAdminBundle:CRUD:tracking.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}