<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CAB\AdminBundle\Admin\DataTransformer\ZoneToIntTransformer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ZoneAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class ZoneAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;
    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_zone';
    /**
     * @var string
     */
    protected $baseRoutePattern = 'zone';

    /**
     * @var
     */
    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create Zone',
            'edit' => 'Edit Zone',
            'list' => 'Zone List',
            'show' => 'Show Zone',
            'default' => 'Zone dashboard',
        );
        $this->description = 'Manage Zones. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';
    }

    /**
     * Method set container
     *
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Method get the status of car
     *
     * @return array
     */
    public static function getCarStatus()
    {
        return array('0' => 'New',
            '1' => 'Old',
            '2' => 'Accident',
            '3' => 'En panne',
        );
    }

    /**
     * set the form theme for add/edit zone
     *
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'CABAdminBundle:Form:form_admin_fields.html.twig',
            )
        );
    }

    /**
     * Method setting template for crud
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name)
    {
        //return parent::getTemplate($name);
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:zone-edit.html.twig';
                break;
            case 'create':
                return 'CABAdminBundle:CRUD:zone-edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }

    }

    /**
     * Query to filter the allowed objects in list view
     *
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context = 'list');

        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            //get company
            $companyAdmin = $currentUser->getContacts()->first();
            $query->andWhere(
                $query->expr()->IN(
                    $query->getRootAlias() . '.companyZone',
                    ':company'
                )
            );
            $query->setParameter('company', $companyAdmin);

            return $query;
        } else {
            return $query;
        }

        return $query;
    }

    /**
     * Fields to be shown on create/edit forms
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this zone!');
        }
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $formMapper->add('companyZone', 'hidden', array(), array('admin_code' => 'cab.admin.company'));
            // if edit driver
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($this->getSubject()->getId()) {
                $companyAdmin = $currentUser->getContacts()->first();
                $formBuilder->get('companyZone')->addModelTransformer(new ZoneToIntTransformer($em, $companyAdmin));
            } else {
                $companyAdmin = $currentUser->getContacts()->first();
                $formBuilder->get('companyZone')->addModelTransformer(new ZoneToIntTransformer($em, $companyAdmin));
            }
        } else {
            $formMapper
                ->add('companyZone', null, array(
                    'class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) {
                        return $cr->getActiveCompanies();
                    },
                    'label' => 'Company'
                ), array('admin_code' => 'cab.admin.company'));
        }
        $formMapper
            ->add('zoneName', 'text', array('label' => 'Zone name'))
            ->add('zoneAddress', 'text', array('label' => 'Zone Address', 'attr' => array('class' => 'maps_input')))
            ->add('longitude', 'hidden')
            ->add('latitude', 'hidden')
            ->add('zoneRaduis', 'integer', array('label' => 'Radius'));
    }

    // Fields to be shown on filter forms
    /**
     * Method description
     *
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('zoneName')
            ->add('companyZone', null, array('admin_code' => 'cab.admin.company'));
    }

    /**
     * Fields to be shown on lists
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('zoneName')
            ->add('companyZone', null, array('admin_code' => 'cab.admin.company'))
            ->add('zoneAddress')
            ->add('longitude')
            ->add('latitude')
            ->add('zoneRaduis');
    }

    /**
     * Method configure Fields in show object action
     *
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to show this zone');
        }
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('zoneName')
            ->add('companyZone', null, array(), array('admin_code' => 'cab.admin.company'))
            ->add('zoneAddress')
            ->add('longitude')
            ->add('latitude')
            ->add('zoneRaduis');
    }

    /**
     * Method description
     *
     * @param string $message
     */
    public function checkAccess($message = ' Access denied',$attribute = '')
    {
        // if the user is not a super admin we check the access
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $currentObject = $this->getSubject();
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($currentObject != null && $currentObject->getCompanyZone()->getId() != $currentUser->getContacts()->first()->getId()) {
                throw new AccessDeniedException($message);
            }
        }
    }

}