<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use CAB\AdminBundle\Form\DynamicData\FormItemBasedUserData;

/**
 * Class AgentAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class CommandSncfAdmin extends AbstractAdmin
{
    /**
     * Default Datagrid values
     *
     * @var array
     */
    protected $datagridValues = array(
        '_sort_by' => 'createdAt',
        '_sort_order' => 'DESC',

    );

    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    protected $container;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_command_sncf';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'command_sncf';


    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create command_sncf',
            'edit' => 'Edit command_sncf',
            'list' => 'command_sncf list',
            'show' => 'Show command_sncf',
            'default' => 'command_sncf Dashboard',
        );
    }

    /**
     * @param ContainerInterface $container
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Setting the service FormItemBasedUserData.
     *
     * @param FormItemBasedUserData $basedUserData
     */
    public function setDynamicData(FormItemBasedUserData $basedUserData)
    {
        $this->basedUserData = $basedUserData;
    }

    /**
     * Setting the template form
     *
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }
	/**
	 * {@inheritdoc}
	 * @throws \Exception
	 */
	public function createQuery($context = 'list')
	{
		$query = parent::createQuery($context = 'list');
		$query->andWhere(
			$query->expr()->isNotNull(
				$query->getRootAlias() . '.commandeCourse'
			)
		);

		return $query;
	}

    /**
     * Fields to be shown on create/edit forms
     *
     * @param FormMapper $formMapper
     * @throws \Exception
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->pageTitle = array(
            'create' => 'Create Command SNCF',
            'edit' => 'Edit Command SNCF',
        );

        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();

        $listComp = $companyAdmin = $currentUser->getContacts();
        $companyAdmin = array();
        foreach ($listComp as $item) {
            $companyAdmin [] = $item->getId();
        }


        $formMapper
            ->add('codeBupo', null, array(
                    'label' => false,
                    'attr' => array(
                        'help' => 'Sncf : Code BUPO',
                    ),
                )
            ) ->add('rlt', null, array(
                    'label' => false,
                    'attr' => array(
                        'help' => 'Sncf : RLT',
                    ),
                )
            ) ->add('js', null, array(
                    'label' => false,
                    'attr' => array(
                        'help' => 'Sncf : JS',
                    ),
                )
            ) ->add('commandeCourse', null, array(
                    'label' => false,
                    'attr' => array(
                        'help' => 'Sncf : N° Commande de la course',
                    ),
                )
            )

            ->add('tarifrefSncf', null, array(
                    'label' => false,
                    'attr' => array(
                        'help' => 'Tarif ref Sncf',
                    ),
                )
            );

    }

    /**
     * Fields to be shown on lists
     *
     * @param ListMapper $listMapper
     * @throws \Exception
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier(
                'commandeCourse',
                null,
                array(
                    'route' => array(
                        'name' => 'show',
                    ),
                )
            )
            ->add('course', null, array('label' => 'Course'))
            ->add('rlt')
            ->add('js')
            ->add('createdAt', null, array(
                'sortable' => 'createdAt',
            ))
            ->add('updatedAt', null, array(
                'sortable' => 'updatedAt',
            ))
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    ),
                )
            );
    }

    // Fields to be shown on filter forms
    /**
     * @param DatagridMapper $datagridMapper
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $authChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $datagridMapper
            ->add('codeBupo', null, array('label' => 'code Bupo'))
            ->add('rlt', null, array('label' => 'code RLT'))
            ->add('js', null, array('label' => 'code JS'))
            ->add('commandeCourse', null, array('label' => 'Commande Course'))
            ->add('createdAt', 'doctrine_orm_date_range',
                array('label' => 'Created date'),
                DateType::class,
                array(
                    'widget' => 'single_text',
                    'format' => 'YYYY-MM-dd',
                    'required' => false,
                    'attr' => array('class' => 'datetimepicker', 'data-date-format' => 'yyyy-mm-dd')
                ))
        ;
    }

    /**
     * @param string $message error message to show if the user hasn't persmission
     * @param string $attribute attribute permission
     *
     * @throws AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function checkAccess($message = ' Access denied', $attribute = 'VIEW')
    {
    }
}