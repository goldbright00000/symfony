<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\AdminBundle\Admin\DataTransformer\DriverToIntTransformer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

class WorkNightAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;
    protected $baseRouteName = 'sonata_work_night';
    protected $baseRoutePattern = 'work-night';

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    private $container;


    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create work night',
            'edit' => 'Edit work night',
            'list' => 'work night List',
            'show' => 'Show work night',
            'default' => 'work night dashboard'
        );

        $this->description = 'Manage Work night. Non ergo erunt homines deliciis diffluentes audiend, si quando de amicitia,
        quam nec usu nec ratione habent cognitam, disputabunt. Nam quis est, pro deorum fidem atque hominum! qui velit,
        ut neque diligat quemquam';
    }

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        if ($this->getRoot()->getSubject()->getId()) {
            $driver = $this->getRoot()->getSubject()->getId();
            $oDriver = $em->getRepository('CABUserBundle:User')->find($driver);
            $formMapper
                ->add('driverNight', HiddenType::class, array(), array('admin_code' => 'cab.admin.driver'));
            $formBuilder = $formMapper->getFormBuilder();
            $formBuilder->get('driverNight')->addModelTransformer(new DriverToIntTransformer($em, $oDriver));
        }


        $formMapper
            ->add('monday', CheckboxType::class, array('required' => false, "attr" => array('class' => 'checkbox-day'), 'label' => 'Monday', 'value' => '0'))
            ->add('tuesday', CheckboxType::class, array('required' => false, "attr" => array('class' => 'checkbox-day'), 'label' => 'Tuesday', 'value' => '0'))
            ->add('wednesday', CheckboxType::class, array('required' => false, "attr" => array('class' => 'checkbox-day'), 'label' => 'Wednesday', 'value' => '0'))
            ->add('thursday', CheckboxType::class, array('required' => false, "attr" => array('class' => 'checkbox-day'), 'label' => 'Thursday', 'value' => '0'))
            ->add('friday', CheckboxType::class, array('required' => false, "attr" => array('class' => 'checkbox-day'), 'label' => 'Friday', 'value' => '0'))
            ->add('saturday', CheckboxType::class, array('required' => false, "attr" => array('class' => 'checkbox-day'), 'label' => 'Saturday', 'value' => '0'))
            ->add('sunday', CheckboxType::class, array('required' => false, "attr" => array('class' => 'checkbox-day'), 'label' => 'Sunday', 'value' => '0'))
            ->add('monday_start_time_night', TimeType::class, array('required' => false, 'label' => "Monday",
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('monday_start_pause_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('monday_end_break_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('monday_end_time_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('tuesday_start_time_night', TimeType::class, array('required' => false, 'label' => "Tuesday",
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('tuesday_start_pause_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('tuesday_end_break_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('tuesday_end_time_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('wednesday_start_time_night', TimeType::class, array('required' => false, 'label' => "Wednesday",
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('wednesday_start_pause_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('wednesday_end_break_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('wednesday_end_time_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('thursday_start_time_night', TimeType::class, array('required' => false, 'label' => "Thursday",
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('thursday_start_pause_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('thursday_end_break_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('thursday_end_time_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('friday_start_time_night', TimeType::class, array('required' => false, 'label' => "Friday",
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('friday_start_pause_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('friday_end_break_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('friday_end_time_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('saturday_start_time_night', TimeType::class, array('required' => false, 'label' => "Saturday",
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('saturday_start_pause_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('saturday_end_break_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('saturday_end_time_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('sunday_start_time_night', TimeType::class, array('required' => false, 'label' => "Sunday",
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('sunday_start_pause_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('sunday_end_break_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ))
            ->add('sunday_end_time_night', TimeType::class, array('required' => false, 'label' => false,
                'attr' => array('class' => 'time_input'),
                'widget' => 'single_text'
            ));

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('monday')
            ->add('tuesday')
            ->add('wednesday')
            ->add('thursday')
            ->add('friday')
            ->add('saturday')
            ->add('sunday');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array(
                'route' => array(
                    'name' => 'show'
                )
            ))
            ->add('monday')
            ->add('tuesday')
            ->add('wednesday')
            ->add('thursday')
            ->add('friday')
            ->add('saturday')
            ->add('sunday');
    }
}