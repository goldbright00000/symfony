<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OptionsHandicapAdmin
 * @package CAB\AdminBundle\Admin
 */
class OptionsHandicapAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var
     */
    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'sonata_options_handicap';
        $this->baseRoutePattern = 'options_handicap';
        $this->pageTitle = array(
            'create' => 'Create options handicap',
            'edit' => 'Edit options handicap',
            'list' => 'options handicap List',
            'show' => 'Show options handicap',
            'default' => 'Zone options handicap'
        );
        $this->description = 'Manage options handicap. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Fields to be shown on create/edit forms
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getRoot();
        $objectOptHandicap = $object->getSubject()->getOptionsHandicap();
        $optionsHandicap = array(
            'ElectricChair' => 0,
            'ManualChair' => 0,
            'Transferable' => 0,
            'VisuallyImpaired' => 0,
            'SemiValid' => 0,
            'Autisme' => 0,
        );
        if ($objectOptHandicap !== null) {
            $optionsHandicap = array(
                'ElectricChair' => $objectOptHandicap->getElectricChair(),
                'ManualChair' => $objectOptHandicap->getManualChair(),
                'Transferable' => $objectOptHandicap->getTransferable(),
                'VisuallyImpaired' => $objectOptHandicap->getVisuallyImpaired(),
                'SemiValid' => $objectOptHandicap->getSemiValid(),
                'Autisme' => $objectOptHandicap->getAutisme(),
            );
        }
        $formMapper
            ->add(
                'electricChair',
                'checkbox',
                array(
                    'value' => $optionsHandicap['ElectricChair'],
                    'label' => 'Electric chair',
                    'required' => false,
                )
            )
            ->add(
                'manualChair',
                'checkbox',
                array('value' => $optionsHandicap['ManualChair'], 'label' => 'Manual chair', 'required' => false)
            )
            ->add(
                'transferable',
                'checkbox',
                array('value' => $optionsHandicap['Transferable'], 'label' => 'Transferable', 'required' => false)
            )
            ->add(
                'visuallyImpaired',
                'checkbox',
                array(
                    'value' => $optionsHandicap['VisuallyImpaired'],
                    'label' => 'Visually impaired',
                    'required' => false,
                )
            )
            ->add(
                'semiValid',
                'checkbox',
                array('value' => $optionsHandicap['SemiValid'], 'label' => 'Semi-valid', 'required' => false)
            )
            ->add(
                'autisme',
                'checkbox',
                array('value' => $optionsHandicap['Autisme'], 'label' => 'Autisme', 'required' => false)
            );
    }

    /**
     * Fields to be shown on filter forms
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('user')
            ->add('electricChair')
            ->add('manualChair')
            ->add('transferable')
            ->add('visuallyImpaired')
            ->add('semiValid')
            ->add('autisme');
    }

    /**
     * Fields to be shown on lists
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('electricChair')
            ->add('manualChair')
            ->add('transferable')
            ->add('visuallyImpaired')
            ->add('semiValid')
            ->add('autisme');
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('user')
            ->add('electricChair')
            ->add('manualChair')
            ->add('transferable')
            ->add('visuallyImpaired')
            ->add('semiValid')
            ->add('autisme');
    }
}
