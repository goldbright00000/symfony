<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 20/12/2016
 * Time: 08:35
 */

namespace CAB\AdminBundle\Admin;

use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\UserBundle\Form\TelType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class BusinessAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class BusinessAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_business';
    /**
     * @var string
     */
    protected $baseRoutePattern = 'business-user';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage business company users.';

        $this->pageTitle = array(
            'create' => 'Create Business Company user',
            'edit' => 'Edit Business Company user',
            'list' => 'Business Company user List',
            'show' => 'Show Business Company user',
            'default' => 'Business Company user dashboard',
        );
    }
    /**
     * Overriden from (AbstractAdmin)
     */
    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action,$object);

        $list['custom_action'] = array(
           // 'template' =>  'CABAdminBundle:Custom:custom_button.html.twig',
        );

        return $list;
    }

    /**
     * Method description
     *
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }

    /**
     * Method description
     *
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        /*
         * $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        echo $currentUser->getId(); exit;
        $query = parent::createQuery($context = 'list');
        $alias = $query->getRootAliases()[0];
        $query->andWhere($alias.'.roles LIKE :role');
        $query->andWhere($alias.'.id LIKE :id');
        $query->setParameter('role', '%ROLE_AGENT%');
        $query->setParameter('id', $currentUser->getId());
         */

        $qb = parent::createQuery($context = 'list');
        $alias = $qb->getRootAliases()[0];
        $qb->andWhere($alias . '.roles LIKE :role');
        $qb->setParameter('role', '%ROLE_COMPANYBUSINESS%');

        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        if (true === $authCheck->isGranted('ROLE_ADMIN')) {
            return $qb;
        }
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        //Get the company managed by the current user
        if ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
            $companyAdmin = $currentUser->getContacts()->getIterator();

            $company = '';
            while ($companyAdmin->valid()) {
                $company .= $companyAdmin->current()->getId();
                if ($companyAdmin->key() < $companyAdmin->count() - 1) {
                    $company .= ',';
                }
                $companyAdmin->next();
            }

            $qb->andWhere(
                $qb->expr()->in(
                    $qb->getRootAlias() . '.businessCompany',
                    $company
                )
            );

            return $qb;
        } elseif ($authCheck->isGranted('ROLE_AGENT')) {
            $companyAdmin = $currentUser->getAgentCompany();

            $qb->andWhere(
                $qb->expr()->eq(
                    $qb->getRootAlias() . '.businessCompany',
                    ':companyID'
                )
            );
            $qb->setParameter('companyID', $companyAdmin);

            return $qb;
        }

    }

    /**
     * Preperstist
     *
     * @param mixed $oAgent
     *
     * @return void
     */
    public function prePersist($oAgent)
    {
        $oAgent->setRoles(array('ROLE_COMPANYBUSINESS'));
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Method description
     *
     * @param string $name
     *
     * @return null|string|void
     */

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:business-edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;

        }

        //return parent::getTemplate($name);
    }


    // Fields to be shown on create/edit forms
    /**
     * Method description
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $request = $this->getRequest();
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this agent!', 'EDIT');
        } else {
            $this->checkAccess('You are not allowed to edit this agent!', 'CREATE');
        }
        $this->pageTitle = array(
            'create' => 'Create Agent',
            'edit' => 'Edit Agent',
        );

        $tokenStorage = $this->getConfigurationPool()->getContainer()->get('security.token_storage');
        $currentUser = $tokenStorage->getToken()->getUser();

        $formMapper
            ->add('businessCompany', ModelType::class, array(

            ), array('admin_code' => 'cab.admin.company')
            )
            ->add('firstName', TextType::class, array('label' => 'Firstname', 'required' => false, 'attr' => array('placeholder' => 'Firstname')))
            ->add('lastName', TextType::class, array('label' => 'Lastname', 'required' => false, 'attr' => array('placeholder' => 'LastName')))
            ->add('email', TextType::class, array('label' => 'Email', 'required' => false, 'attr' => array('placeholder' => 'email')))
            ->add('username', TextType::class, array('label' => 'Username', 'required' => false, 'attr' => array('placeholder' => 'username')))

            ->add(
                'plainPassword',
                RepeatedType::class,
                array(
                    'type' => PasswordType::class,
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ),
                array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'email'))
            )
            ->add('address', TextType::class, array('label' => 'Address','attr' => array('class' => 'maps_input','placeholder' => 'Address')))
            ->add('city', TextType::class, array('label' => 'City','required' => false, 'attr' => array('placeholder' => 'City')))
            ->add('postCode', TextType::class, array('label' => 'Post code','required' => false, 'attr' => array('placeholder' => 'Post code')))
            ->add('country', TextType::class, array('label' => 'Country','required' => false, 'attr' => array('placeholder' => 'Country')))
            ->add('phoneMobile', TelType::class, array('label' => 'Mobile', 'required' => false, 'attr' => array('placeholder' => 'phone Mobile')))
            ->add('phoneHome', TelType::class, array('label' => 'Home phone', 'required' => false, 'attr' => array('placeholder' => 'phone Home')))
            ->add('phoneWork', TelType::class, array('label' => 'Work Phone', 'required' => false, 'attr' => array('placeholder' => 'phone Work')))
            ->add('enabled', CheckboxType::class, array('help' => 'Is Enabled', 'attr' => array(
                'help' => 'Is Enabled',
            ), 'required' => false));

        if (!$this->getSubject()->getId()) {
            $formMapper->add('file', FileType::class, array('label' => FALSE, 'required' => false, 'attr' => array('placeholder' => '')));
        }
    }

    // Fields to be shown on filter forms
    /**
     * Method description
     *
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('email');
    }

    // Fields to be shown on lists
    /**
     * Method description
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->checkAccess('You are not allowed to view the agent lists!', 'LIST');
        $this->pageTitle = array(
            'list' => 'Agents list',
        );
        $listMapper
            ->addIdentifier('username')
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('address')
            ->add(
                'avatar',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'data' => 'something',
                    'dir' => '/uploads/avatar/',
                )
            );
    }

    public function checkAccess($message = ' Access denied', $attribute = 'VIEW')
    {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        // if the user is not a super admin we check the access
	    if($this->getSubject() != '')
	    {
	    	if(!$this->getConfigurationPool()->getContainer()->get('cab_course.security_authorization_voter.agent_voter')->checkGranted($attribute, $this->getSubject(), $currentUser))
		    {
			    throw new AccessDeniedException($message);
		    }
	    }
    }
}