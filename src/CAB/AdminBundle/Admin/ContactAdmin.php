<?php
/** Message reçu depuis le formulaire contact Frontend
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use CAB\MainBundle\Entity\Contact;
use CAB\MainBundle\Entity\ContactRepository;
use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use CAB\AdminBundle\Form\EventListener\AddZonesFieldSubscriber;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ContactAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;

    protected $baseRouteName = 'sonata_contact';
    protected $baseRoutePattern = 'contact';

    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage driver. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';
        $this->pageTitle = array(
            'create' => 'Create Driver',
            'edit' => 'Edit driver',
            'list' => 'Drivers List',
            'show' => 'Show driver',
            'default' => 'Driver dashboard'
        );
    }


    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                //return 'CABAdminBundle:CRUD:base_list.html.twig';
                return 'CABAdminBundle:CRUD:contact_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:contact_show.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

      // Fields to be shown on create/edit forms
    /**
     * Method description
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $this->pageTitle = array(
            'create' => 'Create Artilce',
            'edit' => 'Edit Article',
        );

        $formMapper
                ->add('contactName', 'text', array('label' => 'position', 'required' => true))
                ->add('email', 'text', array('label' => 'position', 'required' => true))
                ->add('company', 'text', array('label' => 'position', 'required' => true))
                ->add('phone', 'text', array('label' => 'position', 'required' => true))
                ->add('address', 'text', array('label' => 'position', 'required' => true))
                ->add('subject', 'text', array('label' => 'position', 'required' => true))
                ->add('bodyMessage', 'text', array('label' => 'position', 'required' => true))
        ;
        $formBuilder = $formMapper->getFormBuilder();
    }

    // Fields to be shown on lists
    /**
     * Method description
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {

    }

    // Fields to be shown on filter forms.
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('company');
    }


    protected function configureShowFields(ShowMapper $showMapper)
    {
      $showMapper

             // The default option is to just display the
             // value as text (for boolean this will be 1 or 0)
            ->add('name')
            ->add('phone')
            ->add('email')

        ;


    }

    public function checkAccess($message = ' Access denied', $attribute = 'LIST') {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                        ->getToken()->getUser();
        // if the user is not a super admin we check the access
        if (!$this->getConfigurationPool()->getContainer()
                        ->get('cab_course.security_authorization_voter.agent_voter')
                        ->checkGranted($attribute, 'CAB\UserBundle\Entity\User', $currentUser)
        ) {
            throw new AccessDeniedException($message);
        }
    }

}
