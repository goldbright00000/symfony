<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\CourseBundle\Entity\TypeVehiculeRepository;
use CAB\CourseBundle\Entity\ZoneRepository;

class BagageAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create bagage',
            'edit' => 'Edit bagage',
            'list' => 'Bagages List',
            'show' => 'Show bagage',
            'default' => 'Driver dashboard'
        );
        $this->description = 'Manage bagages. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->pageTitle = array(
            'create' => 'Create bagage',
            'edit' => 'Edit bagage',
        );
        $formMapper
            ->add('bagageName', 'text', array('label' => 'bagage Name'))
            ->add('Description', 'text', array('label' => 'Description'));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('bagageName')
            ->add('Description');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->pageTitle = array(
            'list' => 'Bagages list'
        );
        $listMapper
            ->addIdentifier('id')
            ->add('bagageName')
            ->add('Description');
    }


}