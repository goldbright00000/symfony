<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;
use CAB\CourseBundle\Entity\Vehicule;
use CAB\CourseBundle\Entity\VehiculeAffect;
use CAB\CourseBundle\Entity\VehiculeFuel;
use CAB\CourseBundle\Entity\VehiculeMake;
use CAB\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\CourseBundle\Entity\TypeVehiculeRepository;
use CAB\CourseBundle\Entity\ServicetransportRepository;
use CAB\CourseBundle\Entity\VehiculeModelRepository;
use CAB\UserBundle\Entity\UserRepository;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CAB\AdminBundle\Admin\DataTransformer\VehiculeToIntTransformer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Validator\Constraints\Image;
use Presta\ImageBundle\Form\Type\ImageType;

class VehiculeAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;

    protected $baseRouteName = 'sonata_vehicule';
    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);


        $this->pageTitle = array(
            'create' => 'Create Vehicule',
            'edit' => 'Edit Vehicule',
            'list' => 'Vehicule List',
            'show' => 'Show Vehicule',
            'default' => 'Vehicule dashboard',
        );
        $this->description = 'Manage Vehicule. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Vehicule',
            )
        );
    }

    /**
     * @param ErrorElement $errorElement
     * @param mixed        $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        if ($object->getId() === null) {
            $errorElement
                ->with('fileFront')
                ->assertNotNull(
                    array('message' => 'The Front vehicle photo  is required, please make sure that you upload a photo')
                )
                ->addConstraint(
                    new Image(array('mimeTypes' => "image/*", 'mimeTypesMessage' => 'You must upload an image !'))
                )
                ->addConstraint(new Image(array('maxSize' => '1m')))
                ->end();
        } else {
            $errorElement
                ->with('fileFront')
                ->addConstraint(
                    new Image(array('mimeTypes' => "image/*", 'mimeTypesMessage' => 'You must upload an image !'))
                )
                ->addConstraint(new Image(array('maxSize' => '1m')))
                ->end();
        }
        //@TODO : Make validation on size and mimetypes for all files
    }

    /**
     * @param string $name
     * @return null|string
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                //return 'CABAdminBundle:CRUD:base_list.html.twig';
                return 'CABAdminBundle:CRUD:vehicle_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:vehicle_show.html.twig';
                break;
            case 'edit':
                return 'CABAdminBundle:CRUD:vehicle_edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }

        return parent::getTemplate($name);
    }

    /**
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'CABAdminBundle:Form:form_admin_vehicle.html.twig',
            )
        );
    }

    // Fields to be shown on create/edit forms
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $fileFrontIsRequired = true;
        if ($this->getSubject()->getId()) {
            $fileFrontIsRequired = false;
            $this->checkAccess('You are not allowed to edit this service', 'EDIT');
        } else {
            $this->checkAccess('You are not allowed to create a new vehicle', 'CREATE');
        }
        /*
         * 'query_builder' => function (UserRepository $ur) use ($currentUser, $authorizationChecker) {
                        return $ur->getDriverByCompany('ROLE_DRIVER', $currentUser, $authorizationChecker, true);
                    },
         */


        /*$currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();*/
        $currentUser = $this->container->get('security.token_storage');
        $objectUser = $currentUser->getToken();
        $authorizationChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();
        /** @var UserRepository $ur */
        $ur = $em->getRepository(User::class);
        $choices = $ur->getDriversByCompanyForVehicle('ROLE_DRIVER', $currentUser, $authorizationChecker, true);
        $userManager = $this->getConfigurationPool()->getContainer()->get('cab.cab_user.manager');


        foreach ($choices as $choice) {
            $choicesDrivers[] = $userManager->loadUser($choice['id']);
        }
        if (false === $authorizationChecker->isGranted('ROLE_ADMIN')) {
            $formMapper->add('company', HiddenType::class, array(), array('admin_code' => 'cab.admin.company',));
            if ($authorizationChecker->isGranted('ROLE_ADMINCOMPANY')) {
                $companyAdmin = $objectUser->getUser()->getContacts()->first();
            } elseif ($authorizationChecker->isGranted('ROLE_DRIVER')) {
                $companyAdmin = $objectUser->getUser()->getDriverCompany();
            }
            $formBuilder->get('company')->addModelTransformer(new VehiculeToIntTransformer($em, $companyAdmin));

            //$obj->getCompanyZone();
            $formMapper->add(
                'servicesTransportVehicles',
                EntityType::class,
                array(
                    'multiple' => true,
                    'query_builder' => function (ServicetransportRepository $str) use ($objectUser) {
                        return $str->getServicesTransportByUser($objectUser->getUser());
                    },
                    'attr' => array(
                        'class' => "service-select",
                    ),
                    'label_attr' => array(
                        'class' => 'control-label required col-sm-3',
                    ),
                    'class' => 'CABCourseBundle:ServiceTransport',
                    'required' => false,
                    'placeholder' => '',

                )
            );
        } else {
            $formMapper
                ->add(
                    'company',
                    null,
                    array(
                        'attr' => array(
                            'class' => 'company-select',
                        ),
                        'class' => 'CABCourseBundle:Company',
                        'query_builder' => function (CompanyRepository $cr) {
                            return $cr->getActiveCompanies();
                        },
                        'required' => true,
                    ),
                    array('admin_code' => 'cab.admin.company',)
                );

            $formBuilder = $formMapper->getFormBuilder();
        }

        if ($this->getRoot()->getSubject()->getId()) {

            $vMake = $this->getRoot()->getSubject()->getVehicleModel();
            $vDriver = $this->getRoot()->getSubject()->getDriver();
            if ($vDriver) {
                $idDriver = $vDriver->getId();
            } else {
                $idDriver = 0;
            }
        } else {
            $vMake = $em->getRepository('CABCourseBundle:VehiculeMake')->find(1);
            $vDriver = null;
            $idDriver = 0;
        }

        $formMapper
            ->add(
                'driver',
                null,
                array(
                    'class' => 'CABUserBundle:User',
                    'choices' => $choicesDrivers,

                    //'placeholder' => $vDriver,
                    'required' => false,
                    //'data' => $vDriver,
                ),
                array('admin_code' => 'cab.admin.driver')
            )
            ->add('licensePlateNumber', TextType::class, array('label' => 'license Plate Number', 'required' => true));
        if ($authorizationChecker->isGranted('ROLE_ADMIN')) {
            $formMapper->add(
                'typeVehicule',
                EntityType::class,
                array(
                    'class' => 'CABCourseBundle:TypeVehicule',
                )
            );
        } else {
            $formMapper->add(
                'typeVehicule',
                null,
                array(
                    'class' => 'CABCourseBundle:TypeVehicule',
                    'query_builder' => function (TypeVehiculeRepository $tvr) use ($companyAdmin) {
                        return $tvr
                            ->createQueryBuilder('tv')
                            ->leftJoin('tv.tarifTypeVehicle', 't')
                            ->where('t.company = :comp')
                            ->setParameter('comp', $companyAdmin)
                            ->orderBy('tv.nameType', 'ASC')
	                        ->setCacheable(true)->setCacheRegion('cache_long_time');
                    },
                    'required' => false,
                )
            );
        }

        $formMapper->add(
            'vehicleMake',
            EntityType::class,
            array(
                'data' => $vMake,
                'mapped' => false,
                'class' => 'CABCourseBundle:vehiculeMake',
                'empty_data' => null,
            )
        );

        $formModifier = function (FormInterface $form, VehiculeMake $vehicleMake = null, $make) {
            $vMake = null === $vehicleMake ? $make : $vehicleMake;

            $form->add(
                'vehicleModel',
                null,
                array(
                    'multiple' => false,
                    'required' => false,
                    'attr' => array(
                        'class' => "model-vehicle-select",
                    ),
                    'label_attr' => array(
                        'class' => 'control-label col-sm-3',
                    ),
                    'class' => 'CABCourseBundle:VehiculeModel',
                    'placeholder' => '',
                    'query_builder' => function (VehiculeModelRepository $vmr) use ($vMake) {
                        return $vmr
                            ->createQueryBuilder('vm')
                            ->where('vm.vehicleMake = ?1')
                            ->setParameter(1, $vMake)
	                        ->setCacheable(true)->setCacheRegion('cache_long_time');
                    },
                )
            );
        };

        $formBuilder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier, $vMake) {
                $data = $event->getData();

                if ($data !== null) {
                    if ($data->getVehicleModel() === null) {
                        $vehicleMake = null;
                    } else {
                        $vehicleMake = $data->getVehicleModel()->getVehicleMake();
                    }
                    $formModifier($event->getForm(), $vehicleMake, $vMake);
                }
            }
        );

        $formBuilder->get('vehicleMake')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier, $vMake) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $vehileMake = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $vehileMake, $vMake);
            }
        );


        $formMapper->add('vehiculeName', TextType::class, array('label' => 'Vehicule name'))
            ->add('nbPerson', IntegerType::class, array('label' => 'Person number'))
            ->add('km', IntegerType::class, array('label' => 'KM', 'required' => false))
            ->add('suitcases', IntegerType::class, array('label' => 'Suitcases', 'required' => false))
            ->add('isActive', CheckboxType::class, array('label' => 'is Active', 'required' => false))
            ->add('Soustraintant', CheckboxType::class, array('label' => 'Sous Traitant', 'required' => false))
            ->add('vin', TextType::class, array('label' => 'VIN', 'required' => false))
            ->add('isOutOfService', CheckboxType::class, array('label' => 'Is Out Of Service', 'required' => false))
            ->add('isForHandicap', CheckboxType::class, array('label' => 'Adapted for handicap', 'required' => false))
            ->add('cpam', CheckboxType::class, array('label' => 'Agrée CPAM', 'required' => false))
            ->add('partageCompany', CheckboxType::class, array('label' => 'Vehicule Partager', 'required' => false))
            ->add('numberlicence', TextType::class, array('label' => 'Number licence', 'required' => false))
            ->add('statusCar', ChoiceType::class, array('choices' => self::getCarStatus(), 'required' => false))
            ->add(
                'fileFront',
                ImageType::class,
                array(
                    'label' => 'Front vehicle photo ',
                    'required' => $fileFrontIsRequired,
                )
            )
            ->add(
                'fileInterior',
                ImageType::class,
                array(
                    'label' => 'Interior vehicle photo ',
                    'required' => false,
                )
            )
            ->add(
                'fileBack',
                ImageType::class,
                array(
                    'label' => 'Back vehicle photo ',
                    'required' => false,
                )
            )
            ->add(
                'fileGreyRegistrationCardType',
                ChoiceType::class,
                [
                    'choices' => [
                        'PDF' => '1',
                        'Image' => '2',
                    ],
                    'label' => 'Type file',
                    'required' => true,

                    'multiple' => false,
                    'expanded' => true,
                    'mapped' => false,
                    'data' => 1,
                ]
            )
            ->add(
                'fileGreyRegistrationCard',
                ImageType::class,
                [
                    'label' => 'Grey Registration Card',
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'fileGreyRegistrationCardPDF',
                FileType::class,
                [
                    'label' => 'Grey Registration Card',
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'fileInsuranceCardType',
                ChoiceType::class,
                [
                    'choices' => [
                        'PDF' => '1',
                        'Image' => '2',
                    ],
                    'label' => 'Type file',
                    'required' => true,

                    'multiple' => false,
                    'expanded' => true,
                    'mapped' => false,
                    'data' => 1,
                ]
            )
            ->add(
                'fileInsuranceCard',
                ImageType::class,
                [
                    'label' => 'Grey Registration Card',
                    'required' => false,
                ]
            )
            ->add(
                'fileInsuranceCardPDF',
                FileType::class,
                [
                    'label' => 'Grey Registration Card',
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'fileVehicleTechCertifType',
                ChoiceType::class,
                [
                    'choices' => [
                        'PDF' => '1',
                        'Image' => '2',
                    ],
                    'label' => 'Type file',
                    'required' => true,

                    'multiple' => false,
                    'expanded' => true,
                    'mapped' => false,
                    'data' => 1,
                ]
            )
            ->add(
                'fileVehicleTechCertif',
                ImageType::class,
                [
                    'label' => 'Vehicle technical certificate',
                    'required' => false,
                ]
            )
            ->add(
                'fileVehicleTechCertifPDF',
                FileType::class,
                [
                    'label' => 'Vehicle technical certificate',
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'servicesTransportVehicles',
                EntityType::class,
                array(
                    'multiple' => true,
                    'query_builder' => function (ServicetransportRepository $str) use ($objectUser) {
                        return $str->getServicesTransportByUser($objectUser->getUser());
                    },
                    'attr' => array(
                        'class' => "service-select",
                    ),
                    'label_attr' => array(
                        'class' => 'control-label required col-sm-3',
                    ),
                    'class' => 'CABCourseBundle:ServiceTransport',
                    'required' => false,
                    'placeholder' => '',

                )
            )
            ->add(
                'expiredocGreyRegistrationCard',
                DatePickerType::class,
                [
                    'label' => 'Date Exp',
                    'format' => 'dd/MM/yyyy',
                ]
            )
            ->add(
                'expiredocInsuranceCard',
                DatePickerType::class,
                [
                    'label' => 'Date Exp',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                ]
            )
            ->add(
                'expiredocVehicleTechCertif',
                DatePickerType::class,
                [
                    'label' => 'Date Exp',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                ]
            );
    }

    // Fields to be shown on filter forms
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('vehiculeName')
            ->add('licensePlateNumber')
            ->add('nbPerson')
            ->add('suitcases')
            ->add('isActive')
            ->add('isOutOfService')
            ->add('isForHandicap')
            ->add('statusCar');
    }

    // Fields to be shown on lists
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier(
                'vehiculeName',
                null,
                array(
                    'route' => array(
                        'name' => 'show',
                    ),
                )
            )
            ->add('company', null, array('admin_code' => 'cab.admin.company',))
            ->add('typeVehicule')
            ->add('nbPerson')
            ->add('suitcases')
            ->add('isActive')
            ->add('isOutOfService')
            ->add('isForHandicap')
            ->add('statusCar')
            ->add('vehicles')
            ->add(
                'photoFront',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo_row.html.twig',
                    'data' => '',
                    'dir' => '/uploads/vehicule/',
                )
            )
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    ),
                )
            );
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {

        $vehicule_id = $this->getSubject();


        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $ca = $em->getRepository('CABCourseBundle:Course')->findCaByVehicule($vehicule_id);
        $race = $em->getRepository('CABCourseBundle:Course')->getTotalRaceByVehicule($vehicule_id);
        $countaffect = $em->getRepository('CABCourseBundle:VehiculeAffect')->getCountDriversByVehiculeId($vehicule_id);
        $affect = $em->getRepository('CABCourseBundle:VehiculeAffect')->getListDriversByVehiculeId($vehicule_id);
        $sumecart = $em->getRepository('CABCourseBundle:VehiculeAffect')->getSumEcartByVehicule($vehicule_id);
        $flotBarsData = $em->getRepository('CABCourseBundle:VehiculeFuel')->getflotBarsDataByVehicule($vehicule_id);
        $flotBarsData2 = $em->getRepository('CABCourseBundle:VehiculeMaintenance')->getCostMaintenanceByVehicule(
            $vehicule_id
        );
        $flotBarsData3 = $em->getRepository('CABCourseBundle:VehiculeFuel')->findBy(
            ['vehicule' => $vehicule_id],
            ['createdAt' => 'ASC']
        );


        foreach ($flotBarsData as $key => $value) {
            $flotBarsData[] = [$value['month'], $value['total']];
        }

        foreach ($flotBarsData2 as $key => $value) {
            $flotBarsData2[] = [$value['month'], $value['total']];
        }

        $before_km = 0;
        /**
         * @var              $key
         * @var VehiculeFuel $value
         */
        foreach ($flotBarsData3 as $key => $value) {
            $flotBarsData3[] = [
                $value->getCreatedAt()->format("d/m/Y"),
                ($value->getKm() - $before_km) ? (($value->getQuantity() * 100) / ($value->getKm() - $before_km)) : 0,
            ];
            $before_km = $value->getKm();
        }

        if ($this->getRoot()->getSubject()) {
            $this->checkAccess('You are not allowed to show this vehicule', 'SHOW');
        }
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('vehiculeName')
            ->add('company', null, array('admin_code' => 'cab.admin.company',))
            ->add('typeVehicule')
            ->add('nbPerson')
            ->add('suitcases')
            ->add('isActive')
            ->add('isOutOfService')
            ->add('isForHandicap')
            ->add('statusCar')
            ->add('vehicles')
            ->add(
                'data',
                null,
                [
                    'ca' => $ca,
                    'race' => $race,
                    'countaffect' => $countaffect,
                    "affect" => $affect,
                    "sumecart" => $sumecart,
                    "flotBarsData" => $flotBarsData,
                    "flotBarsData2" => $flotBarsData2,
                    "flotBarsData3" => $flotBarsData3,
                ]
            )
            ->add(
                'photoFront',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label' => 'Photo',
                    'filter' => 'big_img',
                    'dir' => '/uploads/vehicule/',
                )
            )
            ->add(
                'photoInterior',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label' => 'Photo',
                    'filter' => 'big_img',
                    'dir' => '/uploads/vehicule/',
                )
            )
            ->add(
                'photoBack',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label' => 'Photo',
                    'filter' => 'big_img',
                    'dir' => '/uploads/vehicule/',
                )
            )
            ->add(
                'docGreyRegistrationCard',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:document.html.twig',
                    'label' => 'Grey Registration Card',
                    'dir' => '/uploads/vehicule/documents/',
                )
            )
            ->add(
                'docInsuranceCard',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:document.html.twig',
                    'label' => 'Insurance Card',
                    'dir' => '/uploads/vehicule/documents/',
                )
            )
            ->add(
                'docVehicleTechCertif',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:document.html.twig',
                    'label' => 'Document Vehicle Technical Certificate',
                    'dir' => '/uploads/vehicule/documents/',
                )
            );
    }

    /**
     * @return array
     */
    static function getCarStatus()
    {
        return array(
            'New' => 'New',
            'Old' => 'Old',
            'Crashed' => 'Crashed',
            'Broken down' => 'broken down',
        );
    }

    public function getExportFields()
    {

        return array(
            'id' => 'id',
            'immatriculation' => 'licensePlateNumber',
            'vehiculeName' => 'vehiculeName',
            'active' => 'isActiveformatted',
            'nbPerson' => 'nbPerson',
            'affectation' => 'driver',
            'image avant' => 'photoFront',
            'image interieur' => 'photoInterior',
            'image arriere' => 'photoBack',
            'image carte grise ' => 'docGreyRegistrationCard',
            'date limite carte grise' => 'expiredocGreyRegistrationCard',
            'image carte verte assurance' => 'docInsuranceCard',
            'date limite carte verte assurance' => 'expiredocInsuranceCard',
            'image contrôle technique' => 'docVehicleTechCertif',
            'date limite controle technique' => 'expiredocVehicleTechCertif',
            'sous traitant' => 'Soustraintantformatted',
            'cpam' => 'cpamformatted',
            'partage' => 'partageCompanyformatted',
            'createdAt' => 'createdAt',
            'updatedAt' => 'updatedAt',
        );
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('tarif', $this->getRouterIdParameter().'/tarif');
        $collection->add('update_image', $this->getRouterIdParameter().'/update_image');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cabbundle_vehicle';
    }

    /**
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $authorizationChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $query = parent::createQuery($context = 'list');
        if (false === $authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($authorizationChecker->isGranted('ROLE_ADMINCOMPANY')) {
                //get company
                $listComp = $companyAdmin = $currentUser->getContacts();
                $companyAdmin = array();
                foreach ($listComp as $item) {
                    $companyAdmin [] = $item->getId();
                }
            } elseif ($authorizationChecker->isGranted('ROLE_AGENT')) {
                $companyAdmin = $currentUser->getAgentCompany();
            } elseif ($authorizationChecker->isGranted('ROLE_DRIVER')) {
                $companyAdmin = $currentUser->getDriverCompany();
            }
            $query->andWhere(
                $query->expr()->IN(
                    $query->getRootAlias().'.company',
                    ':company'
                )
            );
            $query->setParameter('company', $companyAdmin);

            return $query;
        } else {
            return $query;
        }

        return $query;
    }

    /**
     * @param string $message
     */
    public function checkAccess($message = " Access denied", $attribute = '')
    {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        // if the user is not a super admin we check the access
			if($this->getSubject() != null)
			{
				if(!$this->getConfigurationPool()->getContainer()->get('cab_course.security_authorization_voter.vehicle_voter')->checkGranted($attribute, $this->getSubject(), $currentUser))
				{
					throw new AccessDeniedException($message);
				}
			}


    }


    /**
     * @param $vehicule Vehicule
     *
     * @return mixed|void
     */
    public function postPersist($vehicule)
    {
        $vehicule_affecy = new VehiculeAffect();
        $vehicule_affecy->setDriver($vehicule->getDriver());
        $vehicule_affecy->setVehicule($vehicule);
        $vehicule_affecy->setKm($vehicule->getKm());
        $vehicule_affecy->setCompany($vehicule->getCompany());

        $container = $this->getConfigurationPool()->getContainer();
        $entityManager = $container->get('doctrine.orm.entity_manager');

        $entityManager->persist($vehicule_affecy);
        $entityManager->flush();
    }

    /**
     * @param $vehicule Vehicule
     *
     * @return mixed|void
     */
    public function postUpdate($vehicule)
    {
        $container = $this->getConfigurationPool()->getContainer();
        if ($this->getForm()->has('vehicleMake')) {

            $entityManager = $container->get('doctrine.orm.entity_manager');
            $vehicleMake = $entityManager->getRepository('CABCourseBundle:VehiculeMake')->find(
                $this->getForm()->get('vehicleMake')->getData()
            );
            $vehicule->setvehicleMaker($vehicleMake);
            $entityManager->persist($vehicule);
            $entityManager->flush();
        }

    }


    /**
     * @param $vehicule Vehicule
     *
     * @return mixed|void
     */
    public function preUpdate($vehicule)
    {
        $this->uploadDocs($vehicule);
    }

    /**
     * @param $vehicule Vehicule
     *
     * @return mixed|void
     */
    public function prePersist($vehicule)
    {
        $this->uploadDocs($vehicule);
    }

    /**
     * @param $vehicule Vehicule
     */
    private function uploadDocs($vehicule)
    {
        if ($this->getForm()->get('fileGreyRegistrationCardType')->getData() == 1 && $this->getForm()->get(
                'fileGreyRegistrationCardPDF'
            )->getData()
        ) {
            $vehicule->setFileGreyRegistrationCard($this->getForm()->get('fileGreyRegistrationCardPDF')->getData());
        } elseif ($this->getForm()->get('fileGreyRegistrationCardType')->getData() == 2 && $this->getForm()->get(
                'fileGreyRegistrationCard'
            )->getData()
        ) {
            $vehicule->setFileGreyRegistrationCard($this->getForm()->get('fileGreyRegistrationCard')->getData());
        }

        if ($this->getForm()->get('fileInsuranceCardType')->getData() == 1 && $this->getForm()->get(
                'fileInsuranceCardPDF'
            )->getData()
        ) {
            $vehicule->setFileInsuranceCard($this->getForm()->get('fileInsuranceCardPDF')->getData());
        } elseif ($this->getForm()->get('fileInsuranceCardType')->getData() == 2 && $this->getForm()->get(
                'fileInsuranceCard'
            )->getData()
        ) {
            $vehicule->setFileInsuranceCard($this->getForm()->get('fileInsuranceCard')->getData());
        }

        if ($this->getForm()->get('fileVehicleTechCertifType')->getData() == 1 && $this->getForm()->get(
                'fileVehicleTechCertifPDF'
            )->getData()
        ) {
            $vehicule->setFileVehicleTechCertif($this->getForm()->get('fileVehicleTechCertifPDF')->getData());
        } elseif ($this->getForm()->get('fileVehicleTechCertifType')->getData() == 2 && $this->getForm()->get(
                'fileVehicleTechCertif'
            )->getData()
        ) {
            $vehicule->setFileVehicleTechCertif($this->getForm()->get('fileVehicleTechCertif')->getData());
        }
    }
}