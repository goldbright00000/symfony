<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\AdminBundle\Admin\DataTransformer\UserToIntTransformer;
use CAB\CourseBundle\Entity\Company;
use FOS\UserBundle\Event\FormEvent;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\UserBundle\Entity\UserRepository;
use CAB\CourseBundle\Entity\CompanyTypeRepository;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CompanyAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var string $description
     */
    public $pageTitle;

    /**
     * @var EventDispatcherInterface $dispatcher
     */
    protected $dispatcher;

    protected $baseRouteName = 'sonata_company';

    protected $baseRoutePattern = 'company';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct ($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create company',
            'edit' => 'Edit company',
        );

        $this->description = 'Manage Companies. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    /**
     * @param EventDispatcherInterface $dispatcher
     *
     */
    public function setDispatcher (EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function prePersist ($company)
    {
        $event = new GenericEvent($company);
        $this->dispatcher->dispatch('cab.admin.event.create_company_business', $event);

        $this->manageFileUpload($company);
    }

    public function preUpdate ($company)
    {
        $event = new GenericEvent($company);
        $this->dispatcher->dispatch('cab.admin.event.edit_company_business', $event);

        $this->manageFileUpload($company);
    }

    /*
     * Set the template
     */
    public function getTemplate ($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:company-edit.html.twig';
                break;
            case 'create':
                return 'CABAdminBundle:CRUD:company-edit.html.twig';
                break;
            case 'list':
                return 'CABAdminBundle:CRUD:company_list.html.twig';
                break;
            case 'show':
                if (!$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_ADMIN')
                ) {
                    return 'CABAdminBundle:CRUD:company_business_show.html.twig';
                }
                return 'CABAdminBundle:CRUD:company_show.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * Set the theme form
     */
    public function getFormTheme ()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'CABAdminBundle:Form:form_admin_company_fields.html.twig',
            )
        );
    }

    /*
     * Override the query for the access right : Companies are shown only by owner in list grid
     */
    public function createQuery ($context = 'list')
    {
        $query = parent::createQuery($context);
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            // this is the queryproxy, you can call anything you could
            //call on the doctrine orm QueryBuilder
            $query->andWhere(
                $query->expr()->eq(
                    $query->getRootAlias() . '.contact',
                    ':userID'
                )
            );
            $query->andWhere(
                $query->expr()->eq(
                    $query->getRootAlias() . '.isBusiness',
                    'true'
                )
            );
            $query->setParameter('userID', $currentUser); // eg get from security context

            return $query;
        } else {
            return $query;
        }
    }

    // Create/edit forms
    protected function configureFormFields (FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this company!', 'edit');
        }
        $choiceValues = array();
        if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')
        ) {
            $formMapper->add(
                'contact',
                null,
                array(
                    'required' => true,
                    'attr' => array('class' => 'select-user'),
                    'label' => 'Responsable name',
                    'class' => 'CABUserBundle:User',
                    'query_builder' => function (UserRepository $ur) {
                        return $ur->getUsersByRole('ROLE_ADMINCOMPANY');

                    },

                ),
                array('admin_code' => 'cab.admin.agent')
            );
            $choiceValues = array(
                'normal' => Company::COMPANY_NORMAL,
                'business' => Company::COMPANY_BUSINESS
            );
            $defaultChoice = 'normal';
        } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_AGENT')
        ) {
            if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMINCOMPANY')
            ) {
                $codeAdmin = 'cab.admin.company';
            } else {
                $codeAdmin = 'cab.agent.company';
            }
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            $formBuilder = $formMapper->getFormBuilder();
            $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
            $formMapper->add('contact', 'hidden', array(), array('admin_code' => $codeAdmin,));
            /*$formBuilder->get('contact')->addModelTransformer(
                new UserToIntTransformer($em, $currentUser)
            );*/
            $choiceValues = array(
                Company::COMPANY_BUSINESS => 'business'
            );
            $defaultChoice = Company::COMPANY_BUSINESS;
        }

        $formMapper->add('isBusiness', ChoiceType::class, array(
                'choices' => $choiceValues,
                'label' => 'Is Business?',
                'data' => $defaultChoice,
                'placeholder' => false,
                'label_attr' => array(
                    'class' => 'checkbox-inline'),
                'required' => false,

            )
        )
            ->add('isCredit', null, array(
                    'label' => 'Credit is permitted ?',
                    'required' => false,
                )
            )
            ->add(
                'companyType',
                null,
                array(
                    'label' => 'You are',
                    'class' => 'CABCourseBundle:CompanyType',
                    'query_builder' => function (CompanyTypeRepository $cr) {
                        return $cr
                            ->createQueryBuilder('ct')
                            ->orderBy('ct.companyTypeName', 'ASC')->setCacheable(true)->setCacheRegion('cache_long_time');
                    },
                )
            )
            ->add('siret', TextType::class, array('label' => 'Siret'))
            ->add('companyName', TextType::class, array('label' => 'Company name'))
            ->add('fileAssurance', FileType::class, array('label' => 'Assurance', 'required' => false))
            ->add('fileKbis', FileType::class, array('label' => 'KBIS', 'required' => false))
            ->add('fileIdentitePiece', FileType::class, array('label' => 'Identite piece', 'required' => false))
            ->add('address', TextType::class, array('label' => 'Address', 'attr' => array('class' => 'maps_input')))
            ->add('lat', HiddenType::class, array('label' => 'latitude', 'required' => false))
            ->add('lng', HiddenType::class, array('label' => 'Longitude', 'required' => false))
            ->add('city', TextType::class, array('label' => 'City'))
            ->add('postCode', TextType::class, array('label' => 'Postal Code'))
            ->add('country', TextType::class, array('label' => 'Country'))
            ->add('phone', TextType::class, array('label' => 'Phone', 'required' => false))
            ->add('mobile', TextType::class, array('label' => 'Mobile', 'required' => false))
            ->add('contactMail', TextType::class, array('label' => 'Mail de contact', 'required' => true))
            ->add('file', FileType::class, array('label' => 'Logo', 'required' => false));

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters (DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('companyName')
            ->add('companyType')
            ->add('postCode')
            ->add('isBusiness')
            ->add('city')
            ->add('country');
    }

    // Fields to be shown on lists
    protected function configureListFields (ListMapper $listMapper)
    {
        $this->pageTitle = array(
            'list' => 'Companies list',
        );
        $listMapper
            ->addIdentifier('companyName')
            ->add('companyType')
            ->add('isBusiness')
            ->add('isCredit')
            ->add('contact', array(), array('admin_code' => 'cab.admin.customer',))
            ->add('siret')
            ->add('address')
            ->add('postCode')
            ->add('city')
            ->add('phone')
            ->add('contactMail')
            ->add('logo', null, array('template' => 'CABAdminBundle:Custom:logo_company.html.twig'));
    }

    protected function configureShowFields (ShowMapper $showMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to show this company');
        }
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        $email = $currentUser->getEmail();
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('companyName')
            ->add('username')
            ->add('companyType')
            ->add('isBusiness')
            ->add('isCredit')
            ->add('siret')
            ->add('address')
            ->add('city')
            ->add('postCode')
            ->add('zones')
            ->add('country')
            ->add('phone')
            ->add('mobile')
            ->add('Username')
            //->add('contact',null,array('admin_code' => 'cab.admin.agent'))
            ->add('contactMail')
            ->add(
                'logo',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'label' => 'Logo',
                    'filter' => 'logo',
                    'dir' => 'uploads/partner/logo/'
                )
            );
    }

    public function checkAccess ($message = " Access denied", $permission = 'list')
    {

        // if the user is not a super admin we check the access
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            $currentObject = $this->getSubject();

            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMINCOMPANY')
            ) {
                if ($currentObject != null && $currentObject->getContact()->getId() != $currentUser->getId()) {
                    throw new AccessDeniedException($message);
                }
            } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_AGENT') && $currentObject->getAgentCompany()->getId() != $currentUser->getId()
            ) {
                throw new AccessDeniedException($message);
            } elseif (($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                        ->isGranted('ROLE_COMPANYBUSINESS') || $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                        ->isGranted('ROLE_DRIVER')) && in_array($permission, array('edit'))
            ) {
                throw new AccessDeniedException($message);
            }

        }
    }

    private function manageFileUpload ($company)
    {
        if ($company->getFile()) {
            $company->setUpdatedAt(new \DateTime("now"));
        }
    }

}