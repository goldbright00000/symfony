<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class TypeVehiculeAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;

    protected $baseRouteName = 'sonata_typevehicule';

    //protected $baseRoutePattern = 'course';
    //protected $baseRouteName = 'course';
    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create TypeVehicule',
            'edit' => 'Edit TypeVehicule',
            'list' => 'TypeVehicule List',
            'show' => 'Show TypeVehicule',
            'default' => 'TypeVehicule dashboard'
        );

        $this->description = 'Manage vehicule type. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    /**
     * @param string $name
     * @return null|string
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:type_vehicle_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('nameType', 'text', array('label' => 'Type name'))
            ->add('tarifTypeVehicle', 'sonata_type_admin', array('label' => false));

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nameType')
            ->add('tarifTypeVehicle.company.companyName', null, array('label' => "Company"));
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('route' => array('name' => 'show')))
            ->add('nameType')
            ->add('tarifTypeVehicle.company', null, array('label' => "Company"))
            ->add('tarifTypeVehicle.tarifPC', null, array('label' => "tarifPC"))
            ->add('tarifTypeVehicle.tarifA', null, array('label' => "tarifA"))
            ->add('tarifTypeVehicle.tarifB', null, array('label' => "tarifB"))
            ->add('tarifTypeVehicle.tarifC', null, array('label' => "tarifC"))
            ->add('tarifTypeVehicle.tarifD', null, array('label' => "tarifD"))
            ->add('tarifTypeVehicle.packageApproach', null, array('label' => "packageApproach"))
        ;
    }
    protected function configureShowFields (ShowMapper $showMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to show this type vehicle', 'show');
        }
        $showMapper->add('nameType')
            ->add('tarifTypeVehicle.company.companyName', null, array('label' => "Company"))
            ->add('tarifTypeVehicle.company.tarifType', null, array('label' => "tarifType"))
            ->add('tarifTypeVehicle.tarifPC', null, array('label' => "tarifPC"))
            ->add('tarifTypeVehicle.tarifA', null, array('label' => "tarifA"))
            ->add('tarifTypeVehicle.tarifB', null, array('label' => "tarifB"))
            ->add('tarifTypeVehicle.tarifC', null, array('label' => "tarifC"))
            ->add('tarifTypeVehicle.tarifD', null, array('label' => "tarifD"))
            ->add('tarifTypeVehicle.packageApproach', null, array('label' => "packageApproach"))

            ->add('tarifTypeVehicle.slowWalkPerHour', null, array('label' => "slow Walk Per Hour"))
            ->add('tarifTypeVehicle.luggage', null, array('label' => "luggage"))
            ->add('tarifTypeVehicle.person', null, array('label' => "person"))
            ->add('tarifTypeVehicle.minPrice', null, array('label' => "minPrice"))
            ->add('tarifTypeVehicle.hourNightTarif', null, array('label' => "hour Night Tarif"))

            ->add('tarifTypeVehicle.mondayMorningIncrease', null, array('label' => "Monday Morning Increase"))
            ->add('tarifTypeVehicle.tuesdayMorningIncrease', null, array('label' => "Tuesday Morning Increase"))
            ->add('tarifTypeVehicle.wednesdayMorningIncrease', null, array('label' => "Wednesday Morning Increase"))
            ->add('tarifTypeVehicle.thursdayMorningIncrease', null, array('label' => "Thursday Morning Increase"))
            ->add('tarifTypeVehicle.fridayMorningIncrease', null, array('label' => "Friday Morning Increase"))
            ->add('tarifTypeVehicle.saturdayMorningIncrease', null, array('label' => "Saturday Morning Increase"))
            ->add('tarifTypeVehicle.sundayMorningIncrease', null, array('label' => "Sunday Morning Increase"))

            ->add('tarifTypeVehicle.mondayNightIncrease', null, array('label' => "Monday Night Increase"))
            ->add('tarifTypeVehicle.tuesdayNightIncrease', null, array('label' => "Tuesday Night Increase"))
            ->add('tarifTypeVehicle.wednesdayNightIncrease', null, array('label' => "Wednesday Night Increase"))
            ->add('tarifTypeVehicle.thursdayNightIncrease', null, array('label' => "Thursday Night Increase"))
            ->add('tarifTypeVehicle.fridayNightIncrease', null, array('label' => "Friday Night Increase"))
            ->add('tarifTypeVehicle.saturdayNightIncrease', null, array('label' => "Saturday Night Increase"))
            ->add('tarifTypeVehicle.sundayNightIncrease', null, array('label' => "Sunday Night Increase"))
            ;
    }

    public function checkAccess ($message = " Access denied", $permission = 'list')
    {
        // if the user is not a super admin we check the access
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            throw new AccessDeniedException($message);

        }
    }


}