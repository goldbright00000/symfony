<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\UserBundle\Form\TelType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use CAB\CourseBundle\Entity\ServicetransportRepository;

/**
 * Class CustomerAdmin
 * @package CAB\AdminBundle\Admin
 */
class CustomerAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct ($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'sonata_customer';
        $this->baseRoutePattern = 'customer';
        $this->pageTitle = array(
            'create' => 'Create customer',
            'edit' => 'Edit customer',
            'list' => 'Customers List',
            'show' => 'Show customer',
            'default' => 'Customer dashboard',
        );
        $this->description = 'Manage customer';
    }

    /**
     * {@inheritDoc}
     */
    public function getTemplate ($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:customer_edit.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:customer_show.html.twig';
                break;
            case 'list':
                return 'CABAdminBundle:CRUD:customer_list.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }

    }

    /**
     * @param ContainerInterface $container
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        if ($this->hasRoute('edit')) {
            $actions['send_mail'] = [
                'label' => 'Envoyer info par mail',
                'ask_confirmation' => true
            ];
        }

        return $actions;
    }

    /**
     * {@inheritDoc}
     */
    public function getFormTheme ()
    {
        return array_merge(parent::getFormTheme(), array('CABAdminBundle:Form:form_admin.html.twig'));
    }

    /**
     * {@inheritDoc}
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException
     */
    public function createQuery ($context = 'list')
    {
        $qb = parent::createQuery($context = 'list');
        $alias = $qb->getRootAliases()[0];
        $qb->andWhere($alias . '.roles LIKE :role');
        $qb->setParameter('role', '%ROLE_CUSTOMER%');

        $authorizationChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        if (false === $authorizationChecker->isGranted('ROLE_ADMIN')
        ) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($authorizationChecker->isGranted('ROLE_ADMINCOMPANY')) {
                $companies = $currentUser->getContacts()->getIterator();
                $company = '';
                while ($companies->valid()) {
                    $company .= $companies->current()->getId();
                    if ($companies->key() < $companies->count() - 1) {
                        $company .= ',';
                    }
                    $companies->next();
                }
                $qb->andWhere(
                    $qb->expr()->in(
                        $qb->getRootAlias() . '.customerCompany',
                        $company
                    )
                );
            } elseif ($authorizationChecker->isGranted('ROLE_AGENT')
                || $authorizationChecker->isGranted('ROLE_COMPANYBUSINESS')
                || $authorizationChecker->isGranted('ROLE_DRIVER')
            ) {
                if ($authorizationChecker->isGranted('ROLE_AGENT')) {
                    $company = $currentUser->getAgentCompany();
                } elseif ($authorizationChecker->isGranted('ROLE_COMPANYBUSINESS')) {
                    $company = $currentUser->getBusinessCompany();
                } elseif ($authorizationChecker->isGranted('ROLE_DRIVER')) {
                    $company = $currentUser->getDriverCompany();
                }
                $qb->andWhere(
                    $qb->expr()->eq(
                        $qb->getRootAlias() . '.customerCompany',
                        ':company'
                    )
                );
                $qb->setParameter('company', $company);
            } else {
                // this is the queryproxy, you can call anything you could
                //call on the doctrine orm QueryBuilder
                $qb->andWhere(
                    $qb->expr()->eq(
                        $qb->getRootAlias() . '.contact',
                        ':userID'
                    )
                );
                $qb->setParameter('userID', $currentUser); // eg get from security context
            }

            return $qb;
        } else {
            return $qb;
        }
    }


    /**
     * {@inheritDoc}
     */
    public function prePersist ($oDriver)
    {
        $oDriver->setRoles(array('ROLE_CUSTOMER'));
    }

    /**
     * Set notification for the created customer
     * @param mixed $user
     *
     * @return mixed|void
     */
    public function PostPersist($user)
    {
        $notificationManager = $this->getConfigurationPool()->getContainer()->get('cab_user.manager.notification_manager');
        $notifiation = $propertiesNotification = $notificationManager->setNotificationForUser($user);
        $user->setNotificationEmail($notifiation);
    }

    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('firstName')
            ->add('lastName')
            ->add('sexe')
            ->add('email')
            ->add('username')
            ->add('plainPassword')
            ->add('address')
            ->add('addressComplement')
        ;
    }

    // Fields to be shown on create/edit forms
    /**
     * {@inheritDoc}
     */
    protected function configureFormFields (FormMapper $formMapper)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();
        $authChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
	    $companyAdmin = $currentUser->getContacts()->first();
        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $formMapper
                ->add(
                    'customerCompany',
                    null,
                    array(
                        'class' => 'CABCourseBundle:Company',
                        'label' => 'Company',
                        'multiple' => false,
                        'required' => false,
                    ),
                    array('admin_code' => 'cab.admin.company',)
                );
        } elseif ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
            $formMapper->add('customerCompany', 'hidden', array(),
                array('admin_code' => 'cab.admin.company')
            );



            $formBuilder->get('customerCompany')->addModelTransformer(new CompanyToIntTransformer($em, $companyAdmin));
            $formMapper
                ->add('customerCompany', null,
                    array(
                        'label' => false,
                        'required' => true,
                        'class' => 'CABCourseBundle:Company',
                        'query_builder' => function (CompanyRepository $cr) use ($currentUser) {
                            return $cr->getCompanyByUser($currentUser, false);
                        },
                        'data' => $currentUser->getContacts()->first(),
                    ),
                    array(
                        'admin_code' => 'cab.admin.company',
                    )
                );
        } elseif ($authChecker->isGranted('ROLE_COMPANYBUSINESS')) {
            $formMapper
                ->add('customerCompany', null,
                    array(
                        'label' => false,
                        'class' => 'CABCourseBundle:Company',
                        'data' => $currentUser->getBusinessCompany(),
                        'choices' =>  [$currentUser->getBusinessCompany()],
                    ),
                    array(
                        'admin_code' => 'cab.admin.company',
                    )
                );
        }

        $formMapper->add('nameCompany', 'text',
            array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'Company name')))
            ->add('siren', 'text',
                array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'SIREN')))
            ->add('phoneCompany', TelType::class,
                array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'phone')))
            ->add('firstName', 'text',
                array('label' => false, 'attr' => array('placeholder' => 'Firstname')))
            ->add('lastName', 'text',
                array('label' => false, 'attr' => array('placeholder' => 'LastName')))
            ->add(
                'sexe',
                'choice',
                array(
                    'expanded' => true,
                    'choices' => array(
                        'm' => 'Male',
                        'f' => 'Female',
                    ),
                    'label' => false,
                )
            )
            ->add('isHandicap', 'checkbox', array(
                'value' => 0,
                'label' => 'Is Handicap',
                'required' => false,
                'help' => 'is it handicap ?')
            )
            ->add('optionsHandicap', 'sonata_type_admin', array(
                'btn_add' => false,
                'delete' => false,
                'label' => false)
            )
            ->add('email', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('placeholder' => 'Email')
                )
            )
            ->add('username', 'text',
                array(
                    'label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'username')
                )
            )
            ->add('plainPassword', 'password',
                array(
                    'label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'password')
                )
            )
            ->add(
                'address',
                'text',
                array(
                    'label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'Departure Address', 'class' => 'maps_input'),
                )
            )
            ->add(
                'addressComplement',
                'text',
                array('label' => false, 'required' => false, 'attr' => array(
                    'placeholder' => 'hotel, number fight, office, etc')
                )
            )
            ->add('lngUser', 'hidden', array(
                'label' => false,
                'required' => false)
            )
            ->add('latUser', 'hidden', array(
                'label' => false,
                'required' => false)
            )
            ->add('city', 'text',
                array('label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'City')
                )
            )
            ->add('postCode', 'text',
                array('label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'Postal code')
                )
            )
            ->add('country', 'text',
                array('label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'Country')
                )
            )
            ->add('phoneMobile', TelType::class,
                array('label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'Phone mobile')
                )
            )
            ->add('phoneHome', TelType::class,
                array('label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'Phone home')
                )
            )
            ->add('phoneWork', TelType::class,
                array('label' => false,
                    'required' => false,
                    'attr' => array('placeholder' => 'Phone work')
                )
            )
            ->add(
                'birthday',
                'birthday',
                array(
                    'widget' => 'single_text',
                    'required' => false,
                    'format' => 'yyyy-MM-dd',
                    'label' => false,
                    'attr' => array('placeholder' => '____-__-__ (birthday)'),
                )
            )
            ->add('privateNote', 'textarea',
                array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'Private note')))
            ->add('publicNote', 'textarea',
                array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'Public note')))
            ->add('enabled', 'checkbox',
                array('label' => false, 'help' => 'Activate', 'required' => false))
            ->add('file', 'file',
                array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'Avatar')))
            ->add('isCredit','checkbox',
                array(
                    'value' => 0,
                    'label' => 'Is credit permitted ?',
                    'required' => false,
                    'help' => 'is credit permitted ?'
                )
            )
            ->add(
                'servicesTransport',
                'entity',
                [
                    'class'         => 'CABCourseBundle:ServiceTransport',
                    'label'         => 'Services transport',
                    'multiple'      => true,
                    'required'      => false,
                    'query_builder' => function (ServicetransportRepository $sr) use ($companyAdmin) {
                        return $sr
                            ->createQueryBuilder('s')
                            ->where('s.company = ?1')
                            ->setParameter(1, $companyAdmin)
	                        ->setCacheable(true)->setCacheRegion('cache_long_time');
                    },
                ]
            );

    }

    // Fields to be shown on filter forms
    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters (DatagridMapper $datagridMapper)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $ServiceCompagny = array(
            'class' => 'CAB\CourseBundle\Entity\ServiceTransport',
            'required' => false,
            'query_builder' => function (ServicetransportRepository $str) {
                $currentUser = $this->container->get('security.token_storage');
                $cCompany = $currentUser->getToken()->getUser()->getContacts();
                $oCompany = $cCompany->first();
                return $str->getServicesTransportByCompany($oCompany);
            },
        );



        $datagridMapper
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('email')
            ->add('servicesTransport', null,array(
                    'field_options' => array(
                        'expanded' => false,
                        'multiple' => false,
                        'query_builder' => function (ServicetransportRepository $str) {
                            $currentUser = $this->container->get('security.token_storage');
                            $cCompany = $currentUser->getToken()->getUser()->getContacts();
                            $oCompany = $cCompany->first();
                            return $str->getServicesTransportByCompany($oCompany);
                        }
                    )
                )
            )
            ->add('phoneMobile');
    }

    // Fields to be shown on lists
    /**
     * {@inheritDoc}
     */
    protected function configureListFields (ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('customerCompany', null, array('admin_code' => 'cab.admin.company'))
            ->addIdentifier('username')
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('address')
            ->add('isCredit')
            ->add('avatar', null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'data' => 'something',
                    'dir' => '/uploads/avatar/',
                )
            );
    }
}
