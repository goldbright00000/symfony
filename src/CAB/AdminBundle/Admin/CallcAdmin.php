<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class AgentAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class CallcAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_callc';
    /**
     * @var string
     */
    protected $baseRoutePattern = 'callc';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create Callc',
            'edit' => 'Edit Callc',
            'list' => 'Callc List',
            'show' => 'Show Callc',
            'default' => 'Callc Dashboard',
        );
    }

    /**
     * Method description
     *
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }



    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Method description
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'CABAdminBundle:CRUD:callc_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:callc_show.html.twig';
                break;
             case 'edit':
                return 'CABAdminBundle:CRUD:callc_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;

        }

        //return parent::getTemplate($name);
    }

    // Fields to be shown on create/edit forms
    /**
     * Method description
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $this->pageTitle = array(
            'create' => 'Create Callc',
            'edit' => 'Edit Callc',
        );

        $formMapper
                ->add('callDuration', 'text', array('label' => 'callDuration', 'required' => true))
                ->add('datePickUp', 'text', array('label' => 'callDuration', 'required' => true));

   }

    // Fields to be shown on lists
    /**
     * Method description
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->addIdentifier('id')
            ->add('AgentId')
            ->add('CallDuration')
            ->add('DatePickUp')
            ->add('CallSubject')
            ->add('Course')
            ->add('Cidname')
            ->add('Ciddnid')
            ->add('Calleventtime')
            ->add('Infocallentreprise')
            ->add('Infocallnom')
            ->add('Infocallprenom')
            ->add('Infocalltelephone')
            ->add('Infocallcomment')
            ->add('createdAt', null, array(
                'sortable' => 'createdAt',
            ))
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    ),
                )
            );

    }






    public function checkAccess($message = ' Access denied', $attribute = 'LIST')
    {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();

        // if the user is not a super admin we check the access
        if (!$this->getConfigurationPool()->getContainer()
            ->get('cab_course.security_authorization_voter.agent_voter')
            ->checkGranted('LIST', $this->getSubject(), $currentUser)
        ) {
            throw new AccessDeniedException($message);
        }
    }
}