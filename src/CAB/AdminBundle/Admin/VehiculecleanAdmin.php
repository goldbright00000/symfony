<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\CourseBundle\Entity\Vehicule;
use CAB\CourseBundle\Entity\VehiculeClean;
use CAB\CourseBundle\Entity\VehiculeRepository;
use CAB\CourseBundle\Manager\VehiculeManager;
use CAB\UserBundle\Entity\UserRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use Sonata\AdminBundle\Show\ShowMapper;
use CAB\AdminBundle\Form\DynamicData\FormItemBasedUserData;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Presta\ImageBundle\Form\Type\ImageType;

/**
 * Class AgentAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class VehiculecleanAdmin extends AbstractAdmin {

    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_carclean';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'clean';

    /**
     * @var VehiculeManager
     */
    protected $vehicleManager;

    /**
     * @var basedUserData
     */
    protected $basedUserData;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName) {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create Vehicule Clean',
            'edit' => 'Edit Vehicule Clean',
            'list' => 'Vehicule Clean List',
            'show' => 'Show Vehicule Clean',
            'default' => 'Vehicule clean Dashboard',
        );


    }

    /**
     * Setting the property vehicle manager with the service VehiculeManager.
     * @param VehiculeManager $vehicleManager
     */
    public function setManager(VehiculeManager $vehicleManager)
    {
        $this->vehicleManager = $vehicleManager;
    }

    /**
     * Setting the service FormItemBasedUserData.
     *
     * @param FormItemBasedUserData $basedUserData
     */
    public function setDynamicData(FormItemBasedUserData $basedUserData)
    {
        $this->basedUserData = $basedUserData;
    }

      /**
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $authorizationChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $query = parent::createQuery($context = 'list');
        if (false === $authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($authorizationChecker->isGranted('ROLE_ADMINCOMPANY')) {
                //get company
                $listComp = $companyAdmin = $currentUser->getContacts();
                $companyAdmin = array();
                foreach ($listComp as $item) {
                    $companyAdmin []= $item->getId();
                }
            } elseif ($authorizationChecker->isGranted('ROLE_DRIVER')) {
                $companyAdmin = $currentUser->getDriverCompany();
            }
            $query->andWhere(
                $query->expr()->IN(
                    $query->getRootAlias() . '.company',
                    ':company'
                )
            );
            $query->setParameter('company', $companyAdmin);

            return $query;
        } else {
            return $query;
        }

        return $query;
    }

    /**
     * Method description
     *
     * @return array
     */
    public function getFormTheme() {
        return array_merge(
                parent::getFormTheme(), array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }

    /**
     * Method description
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name) {
        switch ($name) {
            case 'list':
                return 'CABAdminBundle:CRUD:vehiculeclean_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:vehiculeclean_show.html.twig';
                break;
            case 'edit':
                return 'CABAdminBundle:CRUD:vehiculeclean_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * Fields to be shown on create/edit forms
     *
     * @param FormMapper $formMapper
     * @throws \Exception
     */
    protected function configureFormFields(FormMapper $formMapper) {

    if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this service', 'EDIT');
        } else {
            $this->checkAccess('You are not allowed to create a new vehicle', 'CREATE');
        }
        $this->pageTitle = array(
            'create' => 'Create Vehicule Clean',
            'edit' => 'Edit Vehicule Clean',
            );
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        $this->setItemFormByRole($formMapper, $currentUser);

        $formMapper
                ->add('AMOUNT', 'text', array('label' => 'amount', 'required' => true))
                ->add('KM', 'text', array('label' => 'km', 'required' => true))
                ->add('interior', 'checkbox', array('label' => 'intérior', 'required' => false))
                ->add('exterior', 'checkbox', array('label' => 'exterior', 'required' => false))
          ->add('fileType',
            'choice',
            [
              'choices'           => [
                'PDF'   => '1',
                'Image' => '2'
              ],
              'label'             => 'Type file',
              'required'          => true,
              //'choices_as_values' => true,
              'multiple'          => false,
              'expanded'          => true,
              'mapped'            => false,
              'data' => 1
            ]
          )
          ->add(
            'file',
            ImageType::class,
            [
              'label'    => 'Vehicle clean doc, 2Mo max',
              'required' => false,
            ]
          )
          ->add(
            'nameFilePDF',
            'file',
            [
              'label'    => 'Vehicle clean doc, 2Mo xax',
              'required' => false,
              'mapped'   => false
            ]
          );

    }

    /**
     * Fields to be shown on lists
     *
     * @param ListMapper $listMapper
     * @throws \Exception
     */
    protected function configureListFields(ListMapper $listMapper)
    {
    $listMapper
            ->addIdentifier(
                'vehicule',
                null,
                array(
                    'route' => array(
                        'name' => 'show',
                    ),
                )
            )
            ->add('driver', null, array('admin_code' => 'cab.admin.driver',))
            ->add('amount')
            ->add('quantity')
            ->add('km')
            ->add('createdAt')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    ),
                )
            );
    }

    /**
     * Fields to be shown on filter forms
     *
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $authChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $datagridMapper
            ->add('vehicule.licensePlateNumber', null, array('label' => 'Vehicule'))
            ->add(
                'driver', null,
                array(
                    'field_type' => 'entity',
                    'admin_code'=>"cab.admin.driver",
                    'field_options' => array(
                        'query_builder' => function (UserRepository $ur) use ($authChecker) {
                            $container = $this->configurationPool->getContainer();
                            $currentUser = $container->get('security.token_storage')->getToken()->getUser();
                            return $ur->getDriverByCompany('ROLE_DRIVER', $currentUser, $authChecker);
                        }),
                )
            )
            ->add('createdAt', 'doctrine_orm_date_range',
                array('label' => 'Date'),
                'date',
                array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false,
                    'attr' => array('class' => 'datetimepicker', 'data-date-format' => 'yyyy-mm-dd')
                ));
    }

    /**
     * @param string $message error message to show if the user hasn't persmission
     * @param string $attribute attribute permission
     *
     * @throws AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function checkAccess($message = ' Access denied', $attribute = 'VIEW') {
        if (in_array($attribute, array( 'EDIT'))) {
            $idVehicle = $this->getSubject()->getVehicule()->getId();
            $oVehicle = $this->vehicleManager->loadVehicle($idVehicle);

        } else {
            $oVehicle = new Vehicule();
        }

        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        // if the user is not a super admin we check the access
        if (!$this->getConfigurationPool()->getContainer()
            ->get('cab_course.security_authorization_voter.vehicle_voter')
            ->checkGranted($attribute, $oVehicle, $currentUser)
        ) {
            throw new AccessDeniedException($message);
        }
    }

    /**
     * @param FormMapper $formMapper
     * @param $currentUser
     * @throws \Exception
     */
    protected function setItemFormByRole(FormMapper $formMapper, $currentUser)
    {
        $formBuilder = $formMapper->getFormBuilder();
        if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMINCOMPANY')
        ){
            $listComp = $companyAdmin = $currentUser->getContacts();
            $companyAdmin = array();
            foreach ($listComp as $item) {
                $companyAdmin [] = $item->getId();
            }
        } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_DRIVER')
        ) {
            $companyAdmin = array(
                $currentUser->getDriverCompany()->getId(),
            );
        }
        if (count($companyAdmin) === 0) {
            throw new \Exception('No company for current user!');
        }
        $formMapper
            ->add('vehicule', null, array(
                'class' => 'CABCourseBundle:Vehicule',
                'query_builder' => function (VehiculeRepository $vr) use ($companyAdmin) {
                    return $vr->getVehiclesByCompany($companyAdmin);
                },
                'label' => 'Vehicule',
                'choice_label' => 'licensePlateNumber',
            ));

        if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMINCOMPANY')
        ) {
            $this->basedUserData->addDriverItemForm($formBuilder);
        }
    }


  /**
   * @param $vehiculeClean VehiculeClean
   *
   * @return mixed|void
   */
  public function preUpdate($vehiculeClean)
  {
    $this->uploadDocs($vehiculeClean);
  }

  /**
   * @param $vehiculeClean VehiculeClean
   *
   * @return mixed|void
   */
  public function prePersist($vehiculeClean)
  {
    $this->uploadDocs($vehiculeClean);
  }

  /**
   * @param $vehiculeClean VehiculeClean
   */
  private function uploadDocs($vehiculeClean)
  {
    if ($this->getForm()->get('fileType')->getData() == 1 && $this->getForm()->get('nameFilePDF')->getData()) {
      $vehiculeClean->setFile($this->getForm()->get('nameFilePDF')->getData());
    } elseif ($this->getForm()->get('fileType')->getData() == 2 && $this->getForm()->get('file')->getData()) {
      $vehiculeClean->setFile($this->getForm()->get('file')->getData());
    }
  }
}
