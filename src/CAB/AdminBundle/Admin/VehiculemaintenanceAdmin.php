<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\CourseBundle\Entity\Vehicule;
use CAB\CourseBundle\Entity\VehiculeMaintenance;
use CAB\CourseBundle\Entity\VehiculeRepository;
use CAB\CourseBundle\Manager\VehiculeManager;
use CAB\UserBundle\Entity\UserRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use CAB\AdminBundle\Form\DynamicData\FormItemBasedUserData;
use Sonata\AdminBundle\Validator\ErrorElement;
use Symfony\Component\Validator\Constraints\Image;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Presta\ImageBundle\Form\Type\ImageType;



/**
 * Class AgentAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class VehiculemaintenanceAdmin extends AbstractAdmin {

    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_carmaintenance';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'maintenance';


    /**
     * @var VehiculeManager
     */
    protected $vehicleManager;

    /**
     * @var basedUserData
     */
    protected $basedUserData;

    /**
     * Constructor.
     *
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName) {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create Maintenance',
            'edit' => 'Edit Maintenance',
            'list' => 'Maintenance List',
            'show' => 'Show Maintenance',
            'default' => 'Maintenance Dashboard',
        );
    }

    /**
     * Setting the property vehicle manager with the service VehiculeManager.
     * @param VehiculeManager $vehicleManager
     */
    public function setManager(VehiculeManager $vehicleManager)
    {
        $this->vehicleManager = $vehicleManager;
    }

    /**
     * Setting the service FormItemBasedUserData.
     *
     * @param FormItemBasedUserData $basedUserData
     */
    public function setDynamicData(FormItemBasedUserData $basedUserData)
    {
        $this->basedUserData = $basedUserData;
    }

    /**
     * Setting the theme of forms templates
     *
     * @return array
     */
    public function getFormTheme() {
        return array_merge(
                parent::getFormTheme(), array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Vehicule',
            )
        );
    }

    /**
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     * @throws \Exception
     */
    public function createQuery($context = 'list') {
        $authorizationChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $query = parent::createQuery($context = 'list');
        if (false === $authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                            ->getToken()->getUser();
            if ($authorizationChecker->isGranted('ROLE_ADMINCOMPANY')) {
                //get company
                $listComp = $companyAdmin = $currentUser->getContacts();
                $companyAdmin = array();
                foreach ($listComp as $item) {
                    $companyAdmin [] = $item->getId();
                }
            } elseif ($authorizationChecker->isGranted('ROLE_DRIVER')) {
                $companyAdmin = $currentUser->getDriverCompany();
            }
            $query->andWhere(
                    $query->expr()->IN(
                            $query->getRootAlias() . '.company', ':company'
                    )
            );
            $query->setParameter('company', $companyAdmin);

            return $query;
        } else {
            return $query;
        }

        return $query;
    }

    /**
     * Setting the templates for CRUD.
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name) {
        switch ($name) {
            case 'list':
                return 'CABAdminBundle:CRUD:vehiculemaintenance_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:vehiculemaintenance_show.html.twig';
                break;
            case 'edit':
                return 'CABAdminBundle:CRUD:vehiculemaintenance_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * Fields to be shown on create/edit forms
     *
     * @param FormMapper $formMapper
     * @throws \Exception
     */
    protected function configureFormFields(FormMapper $formMapper)
        {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this vehicle maintenance', 'EDIT');
        } else {
            $this->checkAccess('You are not allowed to create a new vehicle maintenance', 'CREATE');
        }
        $this->pageTitle = array(
            'create' => 'Create Maintenance',
            'edit' => 'Edit Maintenenca',
            );

        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();

        $this->setItemFormByRole($formMapper, $currentUser);
        /*if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMINCOMPANY')
        ) {
            $this->setItemFormByRole($formMapper, $currentUser, 'ROLE_ADMINCOMPANY');
        } elseif($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_DRIVER')
        ) {
            $this->setItemFormByRole($formMapper, $currentUser, 'ROLE_DRIVER');
        }*/
        $formMapper
            ->add('amount')
            ->add('km')
            ->add('oilchange', 'checkbox', array('label' => 'oilchange', 'required' => false))
            ->add('oilajust', 'checkbox', array('label' => 'oilajust', 'required' => false))
            ->add('liquidwindiwsajust', 'checkbox', array('label' => 'liquidwindowsajust', 'required' => false))
            ->add('oilfilter', 'checkbox', array('label' => 'oilfilter', 'required' => false))
            ->add('fronttyres', 'checkbox', array('label' => 'fronttyres', 'required' => false))
            ->add('reartyres', 'checkbox', array('label' => 'reartyres', 'required' => false))
            ->add('battery', 'checkbox', array('label' => 'battery', 'required' => false))
            ->add('chassis', 'checkbox', array('label' => 'chassis', 'required' => false))
            ->add('brokenwindows', 'checkbox', array('label' => 'brokenwindows', 'required' => false))
            ->add('bulbs', 'checkbox', array('label' => 'bulbs', 'required' => false))
            ->add('frontbrakepads', 'checkbox', array('label' => 'frontbrakepads', 'required' => false))
            ->add('rearbrakepads', 'checkbox', array('label' => 'rearbrakepads', 'required' => false))
            ->add('frontbrakediscs', 'checkbox', array('label' => 'frontbrakediscs', 'required' => false))
            ->add('rearbrakediscs', 'checkbox', array('label' => 'rearbrakediscs', 'required' => false))
            ->add('tow', 'checkbox', array('label' => 'tow', 'required' => false))
            ->add('detail', 'textarea', array('label' => 'detail', 'required' => false))
            ->add('startAt','sonata_type_date_picker', ['label' => 'date Start', 'required' => false, 'format'=> 'dd/MM/yyyy'])
            ->add('endAt','sonata_type_date_picker', ['label' => 'date Start', 'required' => false, 'format'=> 'dd/MM/yyyy'])
          ->add('fileType',
            'choice',
            [
              'choices'           => [
                'PDF'   => '1',
                'Image' => '2'
              ],
              'label'             => 'Type file',
              'required'          => true,
              'choices_as_values' => true,
              'multiple'          => false,
              'expanded'          => true,
              'mapped'            => false,
              'data' => 1
            ]
          )
          ->add(
            'file',
            ImageType::class,
            [
              'label'    => 'Vehicle maintenance doc, 2mo max',
              'required' => false,
            ]
          )
          ->add(
            'nameFilePDF',
            'file',
            [
              'label'    => 'Vehicle maintenance doc, 2Mo max',
              'required' => false,
              'mapped'   => false
            ]
          );
    }

    /**
     * Fields to be shown on lists.
     *
     * @param ListMapper $listMapper
     *
     * @throws \Exception
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier(
                        'vehicule', null, array(
                    'route' => array(
                        'name' => 'show',
                    ),
                        )
                )
                ->add('driver', null, array('admin_code' => 'cab.admin.driver',))
                ->add('amount')
                ->add('km')
                ->add('oilchange')
                ->add('oilfilter')
                ->add('fronttyres')
                ->add('reartyres')
                ->add('battery')
                ->add('chassis')
                ->add('brokenwindows')
                ->add('bulbs')
                ->add('frontbrakepads')
                ->add('rearbrakepads')
                ->add('frontbrakediscs')
                ->add('rearbrakediscs')
                ->add('tow')
                ->add('detail')
                ->add('createdAt')
                ->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    ),
                        )
        );
    }

    /**
     * Fields to be shown on filter forms.
     *
     * @param DatagridMapper $datagridMapper
     *
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $authChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        $datagridMapper
                ->add('vehicule.licensePlateNumber', null, array('label' => 'Vehicule'))
            ->add(
                'driver', null,
                array(
                    'field_type' => 'entity',
                    'admin_code' => 'cab.admin.driver',
                    'field_options' => array(
                        'query_builder' => function (UserRepository $ur) use ($authChecker) {
                            $container = $this->configurationPool->getContainer();
                            $currentUser = $container->get('security.token_storage')->getToken()->getUser();
                            return $ur->getDriverByCompany('ROLE_DRIVER', $currentUser, $authChecker);
                        }),
                )
            )
            ->add('createdAt', 'doctrine_orm_date_range',
                array('label' => 'Date'),
                'date',
                array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false,
                    'attr' => array('class' => 'datetimepicker', 'data-date-format' => 'yyyy-mm-dd')
                ));
    }

    /**
     * @param string $message error message to show if the user hasn't persmission
     * @param string $attribute attribute permission
     *
     * @throws AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function checkAccess($message = ' Access denied', $attribute = 'VIEW')
    {
        if (in_array($attribute, array('VIEW', 'EDIT'))) {
            //$idVehicle = $this->getSubject()->getVehicule()->getId();
            //$oVehicle = $this->vehicleManager->loadVehicle($idVehicle);
	        $oVehicle = new Vehicule();
        } else {
            $oVehicle = new Vehicule();
        }

        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        // if the user is not a super admin we check the access
        if (!$this->getConfigurationPool()->getContainer()
            ->get('cab_course.security_authorization_voter.vehicle_voter')
            ->checkGranted($attribute, $oVehicle, $currentUser)
        ) {
            throw new AccessDeniedException($message);
        }
    }

    /**
     * @param FormMapper $formMapper
     * @param $currentUser
     * @throws \Exception
     */
    protected function setItemFormByRole(FormMapper $formMapper, $currentUser)
    {
        $formBuilder = $formMapper->getFormBuilder();
        if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMINCOMPANY')
        ){
            $listComp = $companyAdmin = $currentUser->getContacts();
            $companyAdmin = array();
            foreach ($listComp as $item) {
                $companyAdmin [] = $item->getId();
            }
        } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_DRIVER')
        ) {
            $companyAdmin = array(
                $currentUser->getDriverCompany()->getId(),
            );
        }
        if (count($companyAdmin) === 0) {
            throw new \Exception('No company for current user!');
        }
        $formMapper
            ->add('vehicule', null, array(
                'class' => 'CABCourseBundle:Vehicule',
                'query_builder' => function (VehiculeRepository $vr) use ($companyAdmin) {
                    return $vr->getVehiclesByCompany($companyAdmin);
                },
                'label' => 'Vehicule',
                'choice_label' => 'licensePlateNumber',
            ));

        if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMINCOMPANY')
        ) {
            $this->basedUserData->addDriverItemForm($formBuilder);
        }
    }

  /**
   * @param $vehiculeClean VehiculeMaintenance
   *
   * @return mixed|void
   */
  public function preUpdate($vehiculeClean)
  {
    $this->uploadDocs($vehiculeClean);
  }

  /**
   * @param $vehiculeClean VehiculeMaintenance
   *
   * @return mixed|void
   */
  public function prePersist($vehiculeClean)
  {
    $this->uploadDocs($vehiculeClean);
  }

  /**
   * @param $vehiculeClean VehiculeMaintenance
   */
  private function uploadDocs($vehiculeClean)
  {
    if ($this->getForm()->get('fileType')->getData() == 1 && $this->getForm()->get('nameFilePDF')->getData()) {
      $vehiculeClean->setFile($this->getForm()->get('nameFilePDF')->getData());
    } elseif ($this->getForm()->get('fileType')->getData() == 2 && $this->getForm()->get('file')->getData()) {
      $vehiculeClean->setFile($this->getForm()->get('file')->getData());
    }
  }
}
