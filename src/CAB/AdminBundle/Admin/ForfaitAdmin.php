<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda
 * Date: 26/12/2016
 * Time: 10:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\Tax;
use CAB\CourseBundle\Entity\TypeVehiculeRepository;
use CAB\UserBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Exception\BadMethodCallException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Menage the forfait admin.
 *
 * @author Ben Henda Mohamed <benhenda.med@gmail.com>
 */
class ForfaitAdmin extends AbstractAdmin
{

    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;
    protected $dispatcher;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct ($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'sonata_forfait';
        $this->baseRoutePattern = 'forfait';

        $this->description = 'Manage forfait. Non ergo erunt homines deliciis diffluentes audiend, si quando de amicitia,
        quam nec usu nec ratione habent cognitam, disputabunt. Nam quis est, pro deorum fidem atque hominum! qui velit,
        ut neque diligat quemquam';
    }

    /**
     * @param EventDispatcherInterface $dispatcher
     *
     */
    public function setDispatcher (EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate ($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:forfait/forfait-edit.html.twig';
                break;
            case 'create':
                return 'CABAdminBundle:CRUD:forfait/forfait-edit.html.twig';
                break;
            case 'list':
                return 'CABAdminBundle:CRUD:forfait/forfait-list.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFormTheme ()
    {
        return array_merge(
            parent::getFormTheme(), array(
                'CABAdminBundle:Form:form_admin_fields.html.twig',
            )
        );
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function createQuery ($context = 'list')
    {
        $query = parent::createQuery($context);
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            $companyAdmin = null;
            if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMINCOMPANY')
            ) {
                $companyAdmin = $currentUser->getContacts()->first();
            } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_AGENT')
            ) {
                $companyAdmin = $currentUser->getAgentCompany();
            }

            $query->andWhere(
                $query->expr()->eq(
                    $query->getRootAlias() . '.company',
                    ':company'
                )
            );

            $query->setParameter('company', $companyAdmin);

            return $query;
        } else {
            return $query;
        }
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Exception
     */
    public function configureOptions (OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Forfait',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist ($oCourse)
    {
    }

    public function postUpdate ($oCourse)
    {
    }

    /**
     * @param string $message
     *
     * @throws AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function checkAccess ($message = " Access denied", $attribute = 'EDIT')
    {
        $token = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken();
        // if the user is not a super admin we check the access
        /*
         if (!$this->getConfigurationPool()->getContainer()
            ->get('cab_course.security_authorization_voter.course_voter')
            ->checkGranted($attribute, $this->getSubject(), $token)
        ) {
            throw new AccessDeniedException($message);
        }
        */
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws BadMethodCallException
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException
     */
    protected function configureFormFields (FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this course');
        }

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager');
        /** @var User $currentUser */
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        $authChecker = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');

        $formBuilder = $formMapper->getFormBuilder();
        $tax = 0;

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $formMapper
                ->add(
                    'company', null, array(
                    'label' => false,
                    'class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) use ($currentUser) {
                        return $cr->getCompanyByUser($currentUser);
                    },
                ), array(
                        'admin_code' => 'cab.admin.customer',
                    )
                );
        } else {
            $formMapper->add('company', 'hidden', array(), array('admin_code' => 'cab.admin.company'));
            if ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
                /** @var Company $companyAdmin */
                $companyAdmin = $currentUser->getContacts()->first();
                /** @var Tax $companyTax */
                $companyTax = $companyAdmin->getTaxes()->first();
                if ($companyAdmin->getTaxes()) {
                    $companyTax = $companyAdmin->getTaxes()->first();
                    $tax = $companyTax->getValue();
                }
            } elseif ($authChecker->isGranted('ROLE_AGENT')) {
                $companyAdmin = $currentUser->getAgentCompany();
                /** @var Tax $companyTax */
                if ($companyAdmin->getTaxes()) {
                    $companyTax = $companyAdmin->getTaxes()->first();
                    $tax = $companyTax->getValue();
                }
            }

            $formBuilder->get('company')->addModelTransformer(new CompanyToIntTransformer($em, $companyAdmin));

            $companyRepository = $em->getRepository('CABCourseBundle:Company');
           // $vehicleType = $companyRepository->getVehicleType($companyAdmin);
            $formMapper->add(
                'vehicleType', null, array(
                    'label' => false,
                    'class' => 'CABCourseBundle:TypeVehicule',
                    'query_builder' => function (TypeVehiculeRepository $tvr) use ($companyAdmin) {
                        return $tvr->getTypeVehicleByCompany($companyAdmin);
                    },
                )
            );
        }
        $formMapper->add(
            'depAddress', 'text', array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Indicate the departure address',
                    'help' => 'departure address',
                    'addon' => 'fa-map-marker',
                    'class' => 'maps_input',
                ),
            )
        )
        ->add(
            'arrAddress', 'text', array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Indicate the arrival address',
                    'help' => 'Arrival address',
                    'addon' => 'fa-map-marker',
                ),
            )
        )->add(
            'radius', 'text', array(
                'attr' => array(
                    'help' => 'Radius',
                ),
                'label' => false,
                'required' => true,
            )
        )->add(
            'tax', 'text', array(
                'data' => $tax,
                'attr' => array(
                    'help' => 'Tax',
                ),
                'label' => false,
                'required' => true,
            )
        )->add(
            'tarifHT', 'text', array(
                'required' => true,
                'attr' => array(
                    'help' => 'Tarif HT',
                ),
                'label' => false,
                'required' => true,
            )
        )->add(
            'tarifTTC', 'hidden', array(
                'label' => false,
                'required' => false,
            )
        )->add('longDep', 'hidden', array(
                'required' => false,
            )
        )->add('latDep', 'hidden', array(
                'required' => false,
            )
        )->add('longArr', 'hidden', array(
                'required' => false,
            )
        )->add('latArr', 'hidden', array(
                'required' => false,
            )
        );

    }

    // Fields to be shown on filter forms.
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters (DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('company')
            ->add('depAddress')
            ->add('arrAddress')
            ->add('radius')
            ->add('tarifHT')
            ->add('tax')
            ->add('tarifTTC');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields (ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier(
                'id', null, array(
                    'route' => array(
                        'name' => 'show',
                    ),
                )
            )
            ->add('company')
            ->add('depAddress')
            ->add('arrAddress')
            ->add('radius')
            ->add('tarifHT')
            ->add('tax')
            ->add('tarifTTC');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields (ShowMapper $showMapper)
    {
        if ($this->getSubject()) {
            $this->checkAccess('You are not allowed to show this service', 'VIEW');
        }
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('company')
            ->add('depAddress')
            ->add('arrAddress')
            ->add('radius')
            ->add('tarifHT')
            ->add('tax')
            ->add('tarifTTC');
    }

}
