<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\CourseBundle\Entity\ZoneRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CompanyTypeAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;

    protected $baseRouteName = 'typecompany';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create CompanyType',
            'edit' => 'Edit CompanyType',
            'list' => 'Agents CompanyType',
            'show' => 'Show CompanyType',
            'default' => 'CompanyType dashboard'
        );
        $this->description = 'Manage Company type. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('companyTypeName', TextType::class, array('label' => 'Type name'))
            ->add('description', TextType::class, array('label' => 'Description'));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('companyTypeName');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('companyTypeName')
            ->add('description')
            ->add('createdAt');
    }
}