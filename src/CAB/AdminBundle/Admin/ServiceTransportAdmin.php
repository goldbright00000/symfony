<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use CAB\CourseBundle\Entity\ZoneRepository;
use CAB\UserBundle\Form\TelType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CAB\AdminBundle\Admin\DataTransformer\ServiceToIntTransformer;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ServiceTransportAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;
    protected $container;
    //protected $baseRoutePattern = 'course';
    //protected $baseRouteName = 'course';
    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'sonata_servicetransport';
        $this->pageTitle = array(
            'create' => 'Create ServiceTransport',
            'edit' => 'Edit ServiceTransport',
            'list' => 'ServiceTransport List',
            'show' => 'Show ServiceTransport',
            'default' => 'ServiceTransport dashboard',
        );

        $this->description = 'Manage Service Transport. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    /**
     * Method description
     *
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $name
     *
     * @return null|string
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:serviceTransport_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * Method description
     *
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context = 'list');
        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        if (false === $authCheck->isGranted('ROLE_SUPER_ADMIN')) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            $companyManager = $this->getConfigurationPool()->getContainer()->get('cab.company.manager');
            //Get the company managed by the current user
            if ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
                $company = $companyManager->getCompaniesByRole();
                $query->andWhere(
                    $query->expr()->in(
                        $query->getRootAlias() . '.company',
                        $company
                    )
                );
            } elseif ($authCheck->isGranted('ROLE_AGENT')) {
                $oCompany = $currentUser->getAgentCompany();

                $query->andWhere(
                    $query->expr()->eq(
                        $query->getRootAlias() . '.company',
                        ':company'
                    )
                );
                $query->setParameter('company', $oCompany);
            }

            return $query;
        } else {
            return $query;
        }
    }

    public function getExportFields()
    {
        $result = array(
            'id' => 'id',
            'serviceName' => 'serviceName',
            'tel' => TelType::class,
            'lotnumber' => 'lotnumber',
            'zones' => 'zones',
            'company' => 'company',
            'serviceEmail' => 'serviceEmail',
            'ctr' => 'ctr',
            'ref' => 'ref',
            'serviceEmailCompta' => 'serviceEmailCompta',
            'priceView' => 'priceView',
            'message' => 'message',
        );

        return $result;
    }


    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this service');
        }
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();
        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        if (false === $authCheck->isGranted('ROLE_SUPER_ADMIN')) {
            $formMapper->add('company', 'hidden', array(), array('admin_code' => 'cab.admin.company'));
            // if edit driver
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
                $companyManager = $this->getConfigurationPool()->getContainer()->get('cab.company.manager');
                $aCompanies = $companyManager->getCompaniesByRole(true);

                $formMapper
                    ->add('company', null, array(
                        'attr' => array(
                            'class' => 'serviceTransport-company-select',
                        ),
                        'choices' => $aCompanies,
                        'class' => 'CABCourseBundle:Company',
                    ),
                        array('admin_code' => 'cab.admin.company')
                    );
            } elseif ($authCheck->isGranted('ROLE_AGENT')) {
                $companyAdmin = $currentUser->getAgentCompany();
                $formMapper->add('company', 'hidden', array(), array('admin_code' => 'cab.admin.company'));
                $formBuilder->get('company')->addModelTransformer(new CompanyToIntTransformer($em, $companyAdmin));
            }

            $formMapper->add(
                'zones',
                null,
                array(
                    'class' => 'CABCourseBundle:Zone',
                    'query_builder' => function (ZoneRepository $zr) {
                        $oUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                            ->getToken()->getUser();
                        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');

                        return $zr->getZonesByCompany($oUser, $authCheck);
                    },
                    'label' => 'Zones',
                    'multiple' => true,
                    'required' => false,
                )
            );
        } else {
            $formMapper
                ->add('company', null, array(
                    'attr' => array(
                        'class' => 'serviceTransport-company-select',
                    ),
                    'class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) {
                        return $cr->getActiveCompanies();
                    },
                ),
                    array('admin_code' => 'cab.admin.company')
                );

        }
        $formMapper
            ->add('serviceName', 'text', array('label' => 'Service name'))
            ->add('tel', TelType::class, array('label' => 'Tel'))
            ->add('lotnumber', 'text', array('label' => 'Lot Number', 'required' => false))
            ->add('serviceEmail', 'email', array('label' => 'Email','required' => false))
            ->add('priceView', 'checkbox', array('attr' => array('class' => ''),'label' => 'Price View driver', 'required' => false), array())
            ->add('message', 'textarea', array('label' => 'message'))
            ->add('ctr', 'text', array('label' => 'CTR'))
            ->add('ref', 'text', array('label' => 'REF CONTRAT'))
            ->add('serviceEmailCompta', 'text', array('label' => 'Service Email Compta'))
            ->add('dateStart', 'sonata_type_date_picker', ['label' => 'date Start', 'required' => false, 'format'=> 'dd/MM/yyyy'])
            ->add('dateEnd', 'sonata_type_date_picker', ['label' => 'date End', 'required' => false, 'format'=> 'dd/MM/yyyy']);
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('serviceName')
            ->add('company', null, array('admin_code' => 'cab.admin.company'))
            ->add('zones');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('serviceName')
            ->add('lotnumber')
            ->add('zones')
            ->add('priceview')
            ->add('dateStart')
            ->add('dateEnd')
        ;
    }

    /**
     * Method description
     *
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to show this service');
        }
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('serviceName')
            ->add('company', null, array('admin_code' => 'cab.admin.company'))
            ->add('zones');
    }

    /**
     * Method description
     *
     * @param string $message
     */
    public function checkAccess($message = ' Access denied',$attribute = '')
    {
        // if the user is not a super admin we check the access
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $currentObject = $this->getSubject();
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
            if ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
                $companyManager = $this->getConfigurationPool()->getContainer()->get('cab.company.manager');
                $aCompanies = $companyManager->getCompaniesByRole(true);
                $aCompaniesID = array();
                foreach ($aCompanies as $key => $company) {
                    $aCompaniesID[] = $company->getId();
                }
                if ($currentObject != null && !in_array($currentObject->getCompany()->getId(), $aCompaniesID)) {
                    throw new AccessDeniedException($message);
                }
            } elseif ($authCheck->isGranted('ROLE_AGENT')) {
                $oCompany = $currentUser->getAgentCompany();
                if ($currentObject->getCompany()->getId() !== $oCompany->getId()) {
                    throw new AccessDeniedException($message);
                }
            }

        }
    }
}