<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 05/10/2015
 * Time: 23:00
 */
namespace CAB\AdminBundle\Admin\DataTransformer;

use CAB\CourseBundle\Entity\Company;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CompanyToIntTransformer implements DataTransformerInterface
{
    private $manager;
    private $objCompany;

    public function __construct(ObjectManager $manager, $objectCompany = null)
    {
        $this->manager = $manager;
        $this->objCompany = $objectCompany;
    }

    /**
     * Transforms an object (company) to a integer (number).
     *
     * @param  Company|null $company
     * @return string
     */
    public function transform($company)
    {
        if (null === $company) {

            //find the company object
            return $this->objCompany->getId();
        }

        return $company->getId();
    }

    /**
     * Transforms a integer (number) to an object (Company).
     *
     * @param  integer $companyId
     * @return Company|null
     * @throws TransformationFailedException if object (Company) is not found.
     */
    public function reverseTransform($companyId)
    {
        if (!$companyId) {
            return;
        }

        $oCompany = $this->manager
            ->getRepository('CABCourseBundle:Company')
            // query for the issue with this id
            ->find($companyId);

        if (null === $oCompany) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A company with number "%s" does not exist!',
                $companyId
            ));
        }

        return $oCompany;
    }
}