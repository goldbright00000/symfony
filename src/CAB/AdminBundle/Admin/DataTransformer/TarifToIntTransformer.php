<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 05/10/2015
 * Time: 23:00
 */
namespace CAB\AdminBundle\Admin\DataTransformer;

use CAB\CourseBundle\Entity\Tarif;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class TarifToIntTransformer implements DataTransformerInterface
{
    private $manager;
    private $tarif;

    public function __construct(ObjectManager $manager, $oTarif)
    {
        $this->manager = $manager;
        $this->tarif = $oTarif;
    }

    /**
     * Transforms an object (driver) to a integer (number).
     *
     * @param  Tarif|null $tarif
     * @return string
     */
    public function transform($tarif)
    {
        if (null === $tarif) {
            return $this->tarif->getId();
        }

        return $tarif->getId();
    }

    /**
     * Transforms a integer (number) to an object (Tarif).
     *
     * @param  integer $tarifID
     * @return Tarif|null
     * @throws TransformationFailedException if object (tarif) is not found.
     */
    public function reverseTransform($tarifID)
    {
        if (!$tarifID) {
            return;
        }

        $oTarif = $this->manager
            ->getRepository('CABCourseBundle:Tarif')
            // query for the issue with this id
            ->find($tarifID);

        if (null === $oTarif) {
            // causes a validation error
            // this message is not shown to the tarif
            // see the invalid_message option
            throw new TransformationFailedException(sprintf('An tarif with number "%s" does not exist!', $tarifID));
        }

        return $oTarif;
    }
}