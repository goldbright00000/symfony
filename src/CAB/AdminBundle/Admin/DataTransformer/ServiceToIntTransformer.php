<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 05/10/2015
 * Time: 23:00
 */
namespace CAB\AdminBundle\Admin\DataTransformer;

use CAB\CourseBundle\Entity\ServiceTransport;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ServiceToIntTransformer implements DataTransformerInterface
{
    private $manager;
    private $objCompany;

    public function __construct(ObjectManager $manager, $currentobject = null)
    {
        $this->manager = $manager;
        $this->objCompany = $currentobject;
    }

    /**
     * Transforms an ServiceTransport object to a integer (number).
     *
     * @param  ServiceTransport|null $object
     * @return string
     */
    public function transform($company)
    {
        if (null === $company) {
            //find the entity object
            return $this->objCompany->getId();
        }

        return $company->getId();
    }

    /**
     * Transforms a integer (number) to an object.
     *
     * @param  integer $objectId
     * @return ServiceTransport|null
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($objectId)
    {
        if (!$objectId) {
            return;
        }
        $object = $this->manager
            ->getRepository('CABCourseBundle:Company')
            // query for the issue with this id
            ->find($objectId);

        if (null === $object) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A service transport with number "%s" does not exist!',
                $objectId
            ));
        }

        return $object;
    }
}