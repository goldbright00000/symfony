<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 05/10/2015
 * Time: 23:00
 */
namespace CAB\AdminBundle\Admin\DataTransformer;

use CAB\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DriverToIntTransformer implements DataTransformerInterface
{
    private $manager;
    private $driver;

    public function __construct(ObjectManager $manager, $oUser)
    {
        $this->manager = $manager;
        $this->driver = $oUser;
    }

    /**
     * Transforms an object (driver) to a integer (number).
     *
     * @param  User|null $user
     * @return string
     */
    public function transform($user)
    {
        if (null === $user) {
            return $this->driver->getId();
        }

        return $user->getId();
    }

    /**
     * Transforms a integer (number) to an object (User).
     *
     * @param  integer $driverNumber
     * @return User|null
     * @throws TransformationFailedException if object (user) is not found.
     */
    public function reverseTransform($driverNumber)
    {
        if (!$driverNumber) {
            return;
        }

        $oDriver = $this->manager
            ->getRepository('CABUserBundle:User')
            // query for the issue with this id
            ->find($driverNumber);

        if (null === $oDriver) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An user with number "%s" does not exist!',
                $driverNumber
            ));
        }

        return $oDriver;
    }
}