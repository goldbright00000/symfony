<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 05/10/2015
 * Time: 23:00
 */
namespace CAB\AdminBundle\Admin\DataTransformer;

use CAB\CourseBundle\Entity\Zone;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ZoneTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transforms an Zone object to a integer (number).
     *
     * @param  Zone|null $object
     * @return string
     */
    public function transform($zone)
    {
        if (null === $zone) {
            return '';
        }

        return $zone->getId();
    }

    /**
     * Transforms a integer (number) to an object.
     *
     * @param  integer $objectId
     * @return Object|null
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($objectId)
    {
        if (!$objectId) {
            return;
        }
        $object = $this->manager
            ->getRepository('CABCourseBundle:Zone')
            // query for the issue with this id
            ->find($objectId);

        if (null === $object) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A zone with number "%s" does not exist!',
                $objectId
            ));
        }

        return $object;
    }
}