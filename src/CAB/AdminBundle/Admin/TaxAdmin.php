<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda
 * Date: 29/02/2016
 * Time: 22:26
 */

namespace CAB\AdminBundle\Admin;

use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use CAB\CourseBundle\Entity\CompanyRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Menage the tax admin.
 *
 * @author Ben Henda Mohamed <benhenda.med@gmail.com>
 */
class TaxAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;

    protected $container;


    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'sonata_tax';
        $this->baseRoutePattern = 'tax';

        $this->pageTitle = array(
            'create' => 'Create tax',
            'edit' => 'Edit tax',
            'list' => 'Taxes List',
            'show' => 'Show tax',
            'default' => 'Tax dashboard',
        );

        $this->description = 'Manage Taxes. Non ergo erunt homines deliciis diffluentes audiend, si quando de amicitia,
        quam nec usu nec ratione habent cognitam, disputabunt. Nam quis est, pro deorum fidem atque hominum! qui velit,
        ut neque diligat quemquam';
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Course',
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($context = 'list')
    {
        $qb = parent::createQuery($context = 'list');

        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMIN')
        ) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_ADMINCOMPANY')
            ) {
                $companies = $currentUser->getContacts()->getIterator();
                $company = '';
                while ($companies->valid()) {
                    //echo $companies->key() . "=" . $companies->current()->getId() . "\n\r";
                    $company .= $companies->current()->getId();
                    if ($companies->key() < $companies->count() - 1) {
                        $company .= ',';
                    }
                    $companies->next();
                }
                $qb->andWhere(
                    $qb->expr()->in(
                        $qb->getRootAlias() . '.company',
                        $company
                    )
                );
            } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_AGENT')
            ) {
                $company = $currentUser->getAgentCompany();
                $qb->andWhere(
                    $qb->expr()->eq(
                        $qb->getRootAlias() . '.customerCompany',
                        ':company'
                    )
                );
                $qb->setParameter('company', $company);
            }

            return $qb;
        } else {
            return $qb;
        }
    }

    public function checkAccess($message = ' Access denied',$attribute='')
    {
        // if the user is not a super admin we check the access
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $currentObject = $this->getSubject();

            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($currentObject->getCreatedBy() !== null) {
                $createdBy = $currentObject->getCreatedBy()->getId();
            } else {
                $createdBy = 0;
            }

            if ($currentObject->getDriver() && in_array($currentUser->getId(),
                    array(
                        $currentObject->getDriver()->getId(),
                        $createdBy,
                        $currentObject->getClient()->getId(),
                    ))
            ) {

                return true;
            } elseif (!((in_array($currentObject->getCourseStatus(), array(Course::STATUS_CREATED, Course::STATUS_UPDATED_BY_CUSTOMER))) &&
                $currentUser->getRoles()[0] == 'ROLE_DRIVER')
            ) {
                throw new AccessDeniedException($message);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();

        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMIN')
        ) {
            $formMapper->add('company', null,
                array('class' => 'CABCourseBundle:Company',),
                array('admin_code' => 'cab.admin.company',)
            );
        } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
            ->isGranted('ROLE_ADMINCOMPANY')
        ) {
            $formMapper->add('company', 'hidden', array(), array('admin_code' => 'cab.admin.company',));

            $formMapper->add('company', null,
                array('class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) use ($currentUser) {
                        return $cr->getCompanyByUser($currentUser);
                    },
                ),
                array('admin_code' => 'cab.admin.company',)
            );
        }

        $formMapper
            ->add(
                'taxName',
                'text',
                array(
                    'label' => 'Tax name',
                    'attr' => array(
                        'placeholder' => 'Tax name',
                    ),
                )
            )
            ->add(
                'description',
                'textarea',
                array(
                    'required' => false,
                    'label' => 'Description',
                    'required' => false,
                )
            )
            ->add(
                'taxType',
                'choice',
                array(
                    'choices' => array(
	                    'Fixed Tax' => 0,
	                    'Percent' => 1,
                    ),
                    'required' => true,
                    'label' => 'Tax type',
                )
            )
            ->add(
                'value',
                'text',
                array(
                    'required' => true,
                    'label' => 'Tax value',
                )
            );
    }

    // Fields to be shown on filter forms
    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'company',
                null,
                array(
                    'label' => 'Company',
                    'admin_code' => 'cab.admin.company',
                )
            )
            ->add('taxName')
            ->add('taxType')
            ->add('value')
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier(
                'id',
                null,
                array(
                    'route' => array(
                        'name' => 'show',
                    ),
                )
            )
            ->add('company', 'sonata_type_model',
                array(
                    'label' => 'Company',
                    'admin_code' => 'cab.admin.company',
                ))
            ->add('taxName')
            ->add('taxType')
            ->add('value')
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('company', 'sonata_type_model',
                array(
                    'label' => 'Company',
                    'admin_code' => 'cab.admin.company',
                ))
            ->add('taxName')
            ->add('taxType')
            ->add('description')
            ->add('value')
            ->add('createdAt');
    }
}
