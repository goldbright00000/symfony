<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TarifAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;
    protected $baseRouteName = 'sonata_price';
    protected $baseRoutePattern = 'price';
    protected $container;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create Tarif',
            'edit' => 'Edit Tarif',
            'list' => 'Tarif List',
            'show' => 'Show Tarif',
            'default' => 'Tarif dashboard',
        );
        $this->description = 'Manage tarifs. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    /**
     * @param string $name
     * @return null|string
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:tarif_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }


    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'CABAdminBundle:Form:form_admin_tarif.html.twig',
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Tarif',
            )
        );
    }

    /**
     * Override the query for the access right : Tarifs are shown only by owner in list grid
     *
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        if (false === $authCheck->isGranted('ROLE_SUPER_ADMIN')) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();

            $companyManager = $this->getConfigurationPool()->getContainer()->get('cab.company.manager');
            //Get the company managed by the current user
            if ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
                $company = $companyManager->getCompaniesByRole();
                $query->andWhere(
                    $query->expr()->in(
                        $query->getRootAlias() . '.company',
                        $company
                    )
                );
            } elseif ($authCheck->isGranted('ROLE_AGENT')) {
                $oCompany = $currentUser->getAgentCompany();

                $query->andWhere(
                    $query->expr()->eq(
                        $query->getRootAlias() . '.company',
                        ':companyID'
                    )
                );
                $query->setParameter('companyID', $oCompany); // eg get from security context
            }

            return $query;
        } else {
            return $query;
        }
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formBuilder = $formMapper->getFormBuilder();
        $currentUser = $this->container->get('security.token_storage');
        $objectUser = $currentUser->getToken()->getUser();
        $em = $this->container->get('doctrine.orm.entity_manager');
        $serviceHourTarifNight = $this->container->get('cab.hour_tarif_night.manager');
        $choiceHoursTarifNight = $serviceHourTarifNight->getArrayHourTarif();

        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        /*
         * Show the select option for the super admin or hidden for adminCompany role.
         */
        if ($authCheck->isGranted('ROLE_SUPER_ADMIN')) {
            $formMapper
                ->add('company', null, array(
                    'class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) {
                        return $cr->getActiveCompanies();
                    },
                ),
                    array('admin_code' => 'cab.admin.company')
                );
        } elseif ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
            $companyManager = $this->getConfigurationPool()->getContainer()->get('cab.company.manager');
            $aCompanies = $companyManager->getCompaniesByRole(true);

            $formMapper
                ->add('company', 'entity', array(
                    'class' => 'CABCourseBundle:Company',
                    'choices' => $aCompanies,
                    'label' => 'Company'
                ),
                    array('admin_code' => 'cab.admin.company')
                )
                ->add('typeVehiculeTarif','text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Supported'),
            ))
            ;
        } elseif ($authCheck->isGranted('ROLE_AGENT')) {
            $companyAdmin = $currentUser->getAgentCompany();
            $formMapper->add('company', 'hidden', array('label' => 'Company'), array('admin_code' => 'cab.admin.company'));
            $formBuilder->get('company')->addModelTransformer(new CompanyToIntTransformer($em, $companyAdmin));
        }

        $defaultTarifType = 1;
        if ($this->getSubject()) {
            if ($this->getSubject()->getId()) {
                $defaultTarifType = $this->getSubject()->getTarifType();
                $this->checkAccess('You are not allowed to edit this tarif!');
                //$formBuilder->addEventSubscriber(new AddNameFieldSubscriber());
            }
        }

        if ($this->getSubject()) {
            $formMapper->add('typeVehiculeTarif', null, array(
                'class' => 'CABCourseBundle:TypeVehicule',
                'choice_label' => "nameType",
            ));
        }
        $formMapper
            ->add('tarifType', 'choice', array(
                'label' => 'Choose your price type',
                'choices' => array(
                    'Tarif taxi' => '1',
                ),
                'data' => 1,
                'expanded' => true,
                'multiple' => false,
                'data' => $defaultTarifType,
            ));

        $formMapper->add('tarifPC', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Supported'),
        ))
            ->add('tarifA', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Tarif A'),
            ))
            ->add('tarifB', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Tarif B'),
            ))
            ->add('tarifC', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Tarif C'),
            ))
            ->add('tarifD', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Tarif D'),
            ))
            ->add('slowWalkPerHour', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Slow'),
            ))
            ->add('luggage', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Luggage'),
            ))
            ->add('tarifEquipment', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Equipment'),
            ))
            ->add('tarifHandicapWheelchair', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Wheelchair'),
            ))
            ->add('tarifBabySeat', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Baby Seat'),
            ))
            ->add('person', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Person'),
            ))
            ->add('minPrice', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Min price'),
            ))
            ->add('hourNightTarif', null, array(
                'label' => 'Time of hourly night tarif',
                'required' => false,
                'class' => 'CABCourseBundle:HourTarifNight',
                'choice_label' => "labelTarif",
                'placeholder' => false,
                'attr' => array('addon' => 'Night Hours'),
            ))
            ->add('packageApproach', 'text', array(
                'label' => false,
                'required' => false,
                'attr' => array('addon' => 'Package approach'),
            ))
            ->add('mondayMorningFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Monday morning from'),
            ))
            ->add('mondayMorningTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Monday morning to'),
            ))
            ->add('mondayMorningIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('thursdayMorningFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Thursday morning from'),
            ))
            ->add('thursdayMorningTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Thursday morning to'),
            ))
            ->add('thursdayMorningIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('wednesdayMorningFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'wednesday morning from'),
            ))
            ->add('wednesdayMorningTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'wednesday morning to'),
            ))
            ->add('wednesdayMorningIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('tuesdayMorningFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'tuesday morning from'),
            ))
            ->add('tuesdayMorningTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'tuesday morning to'),
            ))
            ->add('tuesdayMorningIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('fridayMorningFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'friday morning from'),
            ))
            ->add('fridayMorningTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'friday morning to'),
            ))
            ->add('fridayMorningIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('saturdayMorningFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'saturday morning from'),
            ))
            ->add('saturdayMorningTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'saturday morning to'),
            ))
            ->add('saturdayMorningIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('sundayMorningFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'sunday morning from'),
            ))
            ->add('sundayMorningTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'sunday morning to'),
            ))
            ->add('sundayMorningIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('mondayNightFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Monday night from'),
            ))
            ->add('mondayNightTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Monday night to'),
            ))
            ->add('mondayNightIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('thursdayNightFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Thursday night from'),
            ))
            ->add('thursdayNightTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'Thursday night to'),
            ))
            ->add('thursdayNightIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('wednesdayNightFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'wednesday night from'),
            ))
            ->add('wednesdayNightTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'wednesday night to'),
            ))
            ->add('wednesdayNightIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('tuesdayNightFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'tuesday night from'),
            ))
            ->add('tuesdayNightTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'tuesday night to'),
            ))
            ->add('tuesdayNightIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('fridayNightFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'friday night from'),
            ))
            ->add('fridayNightTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'friday night to'),
            ))
            ->add('fridayNightIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('saturdayNightFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'saturday night from'),
            ))
            ->add('saturdayNightTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'saturday Night to',),
            ))
            ->add('saturdayNightIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ))
            ->add('sundayNightFrom', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'sunday night from', 'widget' => 'single_text',),
            ))
            ->add('sundayNightTo', 'time', array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => true,
                'required' => false,
                'attr' => array('addon' => 'sunday Night to',),
            ))
            ->add('sundayNightIncrease', 'percent', array(
                'label' => true,
                'required' => false,
                'attr' => array('addon' => '%'),
            ));

    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('typeVehiculeTarif')
            ->add('company', null, array('admin_code' => 'cab.admin.company'));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to show this company');
        }
        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('id')
            ->add('company', null, array('admin_code' => 'cab.admin.company'));
            if ($this->getSubject()) {
                $showMapper->add('typeVehiculeTarif');
            }
            $showMapper->add('mondayMorningFrom')
            ->add('mondayMorningto')
            ->add('thursdayMorningFrom')
            ->add('thursdayMorningTo')
            ->add('wednesdayMorningFrom')
            ->add('wednesdayMorningTo')
            ->add('tuesdayMorningFrom')
            ->add('tuesdayMorningTo')
            ->add('fridayMorningFrom')
            ->add('fridayMorningTo')
            ->add('saturdayMorningFrom')
            ->add('saturdayMorningTo')
            ->add('sundayMorningFrom')
            ->add('sundayMorningTo');
    }

    public function checkAccess($message = " Access denied",$attribute = '')
    {
        // if the user is not a super admin we check the access
        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                ->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $currentObject = $this->getSubject();
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            if ($currentObject != null && $currentObject->getCompany()->getContact()->getId() != $currentUser->getId()) {
                throw new AccessDeniedException($message);
            }
        }
    }

}