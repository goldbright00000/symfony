<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 06/10/2015
 * Time: 00:30
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


/**
 * Class AbstractAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
abstract class AbstractAdmin extends Admin
{
    /** @var int */
    protected $maxPerPage = 10;

    //other attributes

    /**
     * @param int $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        // custome arguments
        if (!$this->hasRequest()) {
            $this->datagridValues = array(
                '_per_page' => $this->maxPerPage
            );
        }
    }
}
