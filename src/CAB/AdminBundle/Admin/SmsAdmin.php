<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use CAB\UserBundle\Entity\UserRepository;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CAB\AdminBundle\Admin\DataTransformer\UserToIntTransformer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Class AgentAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class SmsAdmin extends AbstractAdmin {

    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_sms';

    /**
     * @var string
     */
    protected $baseRoutePattern = 'sms';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName) {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create SMS',
            'edit' => 'Edit SMS',
            'list' => 'SMS List',
            'show' => 'Show SMS',
            'default' => 'SMS Dashboard',
        );
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * Method description
     *
     * @return array
     */
    public function getFormTheme() {
        return array_merge(
                parent::getFormTheme(), array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }

    /**
     * Method description
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name) {
        switch ($name) {
            case 'list':
                return 'CABAdminBundle:CRUD:sms_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:sms_show.html.twig';
                break;
            case 'edit':
                return 'CABAdminBundle:CRUD:sms_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }

        //return parent::getTemplate($name);
    }

    // Fields to be shown on create/edit forms
    /**
     * Method description
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper) {

        $this->pageTitle = array(
            'create' => 'Create SMS',
            'edit' => 'Edit SMS',
        );

        $formBuilder = $formMapper->getFormBuilder();
        $formMapper
                ->add('number', 'entity',
                    array(
                    'class' => 'CABUserBundle:User',
                    'label' => 'Vehicule',
                    //'multiple' => true,
                    'choice_label' => 'phoneMobile',
                    'query_builder' => function (UserRepository $vr) {
                        return $vr->getDriverEnable();
                    },
                ))
                //->add('number', 'text', array('label' => 'number', 'required' => true))
                ->add('designation', 'choice', array('choices' => array('register' => 'register', 'publicite' => 'publicite', 'relance' => 'relance')))
                ->add('message', 'textarea', array('label' => 'message', 'required' => true));
    }

    // Fields to be shown on lists
    /**
     * Method description
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper) {

    }

    // Fields to be shown on filter forms
    /**
     * @param DatagridMapper $datagridMapper
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper) {

        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                        ->getToken()->getUser()->getId();


        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('state')
                ->add('code')
                ->add('info')
                ->add('idCampagne')
                ->add('cost')
                ->add('price')
                ->add('balance')
                ->add('idSms')
                ->add('yourId')
                ->add('dateProg')
                ->add('smsDate')
                ->add('life')
                ->add('number')
                ->add('message')
                ->add('designation')
                ->add('userId');
    }

    public function checkAccess($message = ' Access denied', $attribute = 'LIST') {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                        ->getToken()->getUser();
        // if the user is not a super admin we check the access
        if (!$this->getConfigurationPool()->getContainer()
                        ->get('cab_course.security_authorization_voter.agent_voter')
                        ->checkGranted($attribute, 'CAB\UserBundle\Entity\User', $currentUser)
        ) {
            throw new AccessDeniedException($message);
        }
    }

}
