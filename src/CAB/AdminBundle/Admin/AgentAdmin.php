<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\CompanyRepository;
use CAB\UserBundle\Form\TelType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class AgentAdmin
 *
 * @package CAB\AdminBundle\Admin
 */
class AgentAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;
    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @var string
     */
    protected $baseRouteName = 'sonata_agent';
    /**
     * @var string
     */
    protected $baseRoutePattern = 'office-agent';

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->description = 'Manage office agent. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';

        $this->pageTitle = array(
            'create' => 'Create Agent',
            'edit' => 'Edit Agent',
            'list' => 'Agents List',
            'show' => 'Show Agent',
            'default' => 'Agent dashboard',
        );
    }

    /**
     * Method description
     *
     * @return array
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('CABAdminBundle:Form:form_admin_fields.html.twig')
        );
    }

    /**
     * Method description
     *
     * @param string $context
     *
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        /*
         * $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        echo $currentUser->getId(); exit;
        $query = parent::createQuery($context = 'list');
        $alias = $query->getRootAliases()[0];
        $query->andWhere($alias.'.roles LIKE :role');
        $query->andWhere($alias.'.id LIKE :id');
        $query->setParameter('role', '%ROLE_AGENT%');
        $query->setParameter('id', $currentUser->getId());
         */

        $qb = parent::createQuery($context = 'list');
        $alias = $qb->getRootAliases()[0];
        $qb->andWhere($alias . '.roles LIKE :role');
        $qb->setParameter('role', '%ROLE_AGENT%');

        $authCheck = $this->getConfigurationPool()->getContainer()->get('security.authorization_checker');
        if (true === $authCheck->isGranted('ROLE_SUPER_ADMIN')) {
            return $qb;
        }
        if (true === $authCheck->isGranted('ROLE_ADMINCOMPANY') || true === $authCheck->isGranted('ROLE_AGENT')) {
            $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                ->getToken()->getUser();
            //Get the company managed by the current user
            if ($authCheck->isGranted('ROLE_ADMINCOMPANY')) {
                $companyAdmin = $currentUser->getContacts()->getIterator();
                $company = '';
                while ($companyAdmin->valid()) {
                    //echo $companyAdmin->key() . "=" . $companyAdmin->current()->getId() . "\n\r";
                    $company .= $companyAdmin->current()->getId();
                    if ($companyAdmin->key() < $companyAdmin->count() - 1) {
                        $company .= ',';
                    }
                    $companyAdmin->next();
                }
                $qb->andWhere(
                    $qb->expr()->in(
                        $qb->getRootAlias() . '.agentCompany',
                        $company
                    )
                );
            } elseif ($authCheck->isGranted('ROLE_AGENT')) {
                $companyAdmin = $currentUser->getAgentCompany();

                $qb->andWhere(
                    $qb->expr()->eq(
                        $qb->getRootAlias() . '.agentCompany',
                        ':companyID'
                    )
                );
                $qb->setParameter('companyID', $companyAdmin);
            }

            return $qb;
        }
    }

    /**
     * Preperstist
     *
     * @param mixed $oAgent
     *
     * @return void
     */
    public function prePersist($oAgent)
    {
        $oAgent->setRoles(array('ROLE_AGENT'));
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Method description
     *
     * @param string $name
     *
     * @return null|string|void
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                //return 'CABAdminBundle:CRUD:base_list.html.twig';
                return 'CABAdminBundle:CRUD:agent_list.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:agent_show.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;

        }

        //return parent::getTemplate($name);
    }

    // Fields to be shown on create/edit forms
    /**
     * Method description
     *
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this agent!', 'EDIT');
        } else {
            $this->checkAccess('You are not allowed to edit this agent!', 'CREATE');
        }
        $this->pageTitle = array(
            'create' => 'Create Agent',
            'edit' => 'Edit Agent',
        );
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();

        $formMapper
            ->add('agentCompany', null,
                array(
                    'label' => false,
                    'class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) use ($currentUser) {
                        return $cr->getCompanyByUser($currentUser);
                    },
                ),
                array(
                    'admin_code' => 'cab.admin.company',
                )

            )
            ->add('firstName', 'text', array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'Firstname')))
            ->add('lastName', 'text', array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'LastName')))
            ->add('email', 'text', array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'email')))
            ->add('username', 'text', array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'username')))

            ->add(
                'plainPassword',
                'repeated',
                array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ),
                array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'email'))
            )
            ->add('address', 'text', array('label' => false,'attr' => array('class' => 'maps_input','placeholder' => 'Address')))
            ->add('city', 'text', array('label' => false,'required' => false, 'attr' => array('placeholder' => 'City')))
            ->add('postCode', 'text', array('label' => false,'required' => false, 'attr' => array('placeholder' => 'CodePostal')))
            ->add('country', 'text', array('label' => false,'required' => false, 'attr' => array('placeholder' => 'Country')))
            ->add('phoneMobile', TelType::class, array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'phoneMobile')))
            ->add('phoneHome', TelType::class, array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'phone Home')))
            ->add('phoneWork', TelType::class, array('label' => false, 'required' => false, 'attr' => array('placeholder' => 'phone Work')))
            ->add('enabled', 'checkbox', array('label' => false, 'help' => 'Activate', 'required' => false));

        if (!$this->getSubject()->getId()) {
            $formMapper->add('file', 'file', array('label' => FALSE, 'required' => false, 'attr' => array('placeholder' => 'phoneMobile')));
        }
    }

    // Fields to be shown on filter forms
    /**
     * Method description
     *
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('email');
    }

    // Fields to be shown on lists
    /**
     * Method description
     *
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->checkAccess('You are not allowed to view the agent lists!', 'LIST');
        $this->pageTitle = array(
            'list' => 'Agents list',
        );
        $listMapper
            ->addIdentifier('username')
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('address')
            ->add(
                'avatar',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'data' => 'something',
                    'dir' => '/uploads/avatar/',
                )
            );
    }

    public function checkAccess($message = ' Access denied', $attribute = 'LIST')
    {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
            ->getToken()->getUser();
        // if the user is not a super admin we check the access
        if (!$this->getConfigurationPool()->getContainer()
            ->get('cab_course.security_authorization_voter.agent_voter')
            ->checkGranted($attribute, 'CAB\UserBundle\Entity\User', $currentUser)
        ) {
            throw new AccessDeniedException($message);
        }
    }
}