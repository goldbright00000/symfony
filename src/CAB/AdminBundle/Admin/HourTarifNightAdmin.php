<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 11/07/2015
 * Time: 01:36
 */

namespace CAB\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class HourTarifNightAdmin extends AbstractAdmin
{
    /**
     * @var integer $description
     */
    public $description;

    public $pageTitle;

    protected $baseRouteName = 'sonata_hour_tarif_night';

    //protected $baseRoutePattern = 'course';
    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->pageTitle = array(
            'create' => 'Create hour tarif night',
            'edit' => 'Edit hour tarif night',
            'list' => 'hour tarif night List',
            'show' => 'Show hour tarif night',
            'default' => 'hour tarif night dashboard'
        );

        $this->baseRoutePattern = 'hour_tarif_night';
        $this->description = 'Manage vehicule type. Ego vero sic intellego, Patres conscripti, nos hoc tempore in
        provinciis decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non sentit omnia alia esse nobis
        vacua ab omni periculo atque etiam suspicione belli?';
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('labelTarif', 'text', array('label' => 'Tarif Label'))
            ->add('from', 'time', array('label' => 'From', 'widget' => 'single_text'))
            ->add('to', 'time', array('label' => 'To', 'widget' => 'single_text'));

    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('labelTarif');
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('labelTarif');
    }


}