<?php

/**
 * Created by PhpStorm.
 * User: m.benhenda
 * Date: 29/01/2017
 * Time: 13:36
 */

namespace CAB\AdminBundle\Admin;

use CAB\AdminBundle\Admin\DataTransformer\CompanyToIntTransformer;
use CAB\CourseBundle\Entity\CommandSncf;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\Zone;
use CAB\CourseBundle\Events\HistoryCourseEvents;
use CAB\CourseBundle\Events\StoreCourseEvents;
use CAB\CourseBundle\Events\UpdateCycleCourseEvents;
use CAB\CourseBundle\Events\UpdateStatusCourseEvents;
use CAB\CourseBundle\Manager\CompanyManager;
use CAB\UserBundle\Entity\User;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use CAB\UserBundle\Entity\UserRepository;
use CAB\CourseBundle\Entity\ServicetransportRepository;
use CAB\CourseBundle\Entity\ParticularPeriodRepository;
use CAB\CourseBundle\Entity\ZoneRepository;
use CAB\CourseBundle\Entity\CompanyRepository;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\EqualType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Exception\BadMethodCallException;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CAB\AdminBundle\Admin\DataTransformer\UserToIntTransformer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\DoctrineORMAdminBundle\Datagrid\Pager as SonataPager;

/**
 * Menage the courses admin.
 *
 * @author Ben Henda Mohamed <benhenda.med@gmail.com>
 */
class CourseAdmin extends AbstractAdmin
{

    protected $translationDomain = 'backend';

    /**
     * @var integer $description
     */
    public $description;
    public $pageTitle;
    protected $container;
    protected $dispatcher;
    private static $statusCourse;
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'Desc', // sort direction
        '_sort_by' => 'departureDate',
    );

    public function computeNbResult()
    {
        return $this->getPage() * $this->getMaxPerPage() + 1;
    }

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'sonata_course';
        $this->baseRoutePattern = 'course';

        $this->pageTitle = array(
            'create' => 'Create course',
            'edit' => 'Edit course',
            'list' => 'Courses List',
            'show' => 'Show course',
            'default' => 'Course dashboard',
        );

        $this->description = 'Manage Course. Non ergo erunt homines deliciis diffluentes audiend, si quando de amicitia,
        quam nec usu nec ratione habent cognitam, disputabunt. Nam quis est, pro deorum fidem atque hominum! qui velit,
        ut neque diligat quemquam';

        self::$statusCourse = array(
            //Course::STATUS_CREATED => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Created'),#message.fr.xliff
            Course::STATUS_UPDATED_BY_CUSTOMER => 'Updated by customer',
            Course::STATUS__CANCEL_BY_DO => 'Annulation DO',
            Course::STATUS_CANCEL_BY_SNCF => 'Annulation by SNCF',
            Course::STATUS__CANCEL_OUT_TIME => 'Annulation hors délai',
            Course::STATUS_CANCELLED_BY_CLIENT => 'Cancelled By customer',
            Course::STATUS_CANCELLED_BY_DRIVER => 'Cancelled by driver',
            Course::STATUS_NO_REPONSE => 'No response',
            Course::STATUS_AFFECTED => 'Affected',
            Course::STATUS_REJECTED => 'Rejected',
            Course::STATUS_ACCEPTED_DRIVER => 'Accepted by driver',
            Course::STATUS_ARRIVED_DRIVER => 'Arrived',
            Course::STATUS_PAYED => 'Payed',
            Course::STATUS_CACH_PAY => 'Cash payment',
            Course::STATUS_ON_RACE_DRIVER => 'On race driver',
            Course::STATUS_DONE => 'Done',
            Course::STATUS_PAYED_AND_UPDATED_BY_CUSTOMER => 'Payed and updated by customer',
            Course::STATUS_ATTENTE_PAY => 'Waiting payment',
            Course::STATUS__PRE_COMMAND => 'Pré commande',
            Course::STATUS__DEPLACEMENT_INUTILE => 'Déplacement Inutile',

        );
    }


    /**
     * @param ContainerInterface $container
     * @param EventDispatcherInterface $dispatcher
     *
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     */
    public function setContainer(ContainerInterface $container, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
    }

    /* public function archiveAction()
      {
      //$object = $this->admin->getSubject();

      //return new RedirectResponse($this->admin->generateUrl('archive'));
      } */

    /**
     * {@inheritdoc}
     */
    public function getTemplate($name)
    {
        //return parent::getTemplate($name);
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:course-edit.html.twig';
                break;
            case 'create':
                return 'CABAdminBundle:CRUD:course-edit.html.twig';
                break;
            case 'show':
                return 'CABAdminBundle:CRUD:course_show.html.twig';
                break;
            case 'list':
                return 'CABAdminBundle:CRUD:course_list.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(), array(
                'CABAdminBundle:Form:form_admin_fields.html.twig',
            )
        );
    }

    public function getExportFields()
    {


        $result = array(
            'id' => 'id',
            'Votre ref' => 'commandSncf.commandeCourse',
            'Service' => 'serviceTransportCourses[0]',
            'telservice' => 'ServiceTransportCoursesTel',
            'Client' => 'client',
            'Person Number' => 'personNumber',
            'Départ' => 'startedAddress',
            'Arrivée' => 'arrivalAddress',
            'Date' => 'DepartureDateFormatted',
            'HoursDriverGotoClientTime'=>'HoursDriverGotoClientTime',
            'Heure' => 'DepartureTimeFormatted',
            'hoursArrivalDriverTime' =>'HoursArrivalDriverTime',
            'hoursArrivalDriverEcart' =>'HoursArrivalDriverEcart',
            'Distance' => 'distance',
            'Durée' => 'CourseTimeFormatted',
            'Est_Arrival' => 'HourEstimatedArrival',
            'HoursRaceOnRaceDriverTime' => 'HoursRaceOnRaceDriverTime',
            'HoursRaceOnRaceDriverEcart' => 'HoursRaceOnRaceDriverEcart',
            'HourFinishRace'=>'HoursFinishDriverTime',
            'HoursFinishDriverEcart'=>'HoursFinishDriverEcart',
            'Est_Arrival_TLSE' => 'HourEstimatedArrivalTLSE',
            'Driver' => 'driver',
            'Temps de conduite de jour' => 'HourDayWorking',
            'Temps de conduite de nuit' => 'HourNightWorking',
            'Prix Ref' => 'commandSncf.TarifrefSncfFormatted',
            'Statut' => 'CourseStatusFormatted',
            'delays_race' => 'DelayRace',
            'Price HT' => 'priceHT',
            'Price TTC' => 'priceTTC',
            'Groupage' => 'IsGroupageFormatted',
            'Comment' => 'descriptionChildren',
            'Temps Attente' => 'timeBetweenArrivedAndStarted',
            'Tarif Attente' => 'SlowWalkPerHourTarif',
            'Prix Attente HT' => 'PriceHTattente',
            'Prix Attente TTC' => 'PriceTTCattente',
            'Prix Total HT' => 'PriceTotalHTrace',
            'Prix Total TTC' => 'PriceTotalTTCrace',
            'Course créée' => 'TimeRaceCreated',
            'Zone' => 'Zone',
            'CTR' => 'ServiceTransportCoursesCtr',
            'REF' => 'ServiceTransportCoursesRef',
            'serviceEmailCompta' => 'ServiceTransportCoursesserviceEmailCompta',
            'MESSAGE CTR' => 'ServiceTransportCoursesMessage',

        );

        return $result;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $request = $this->request;

        if ($request->attributes->get('_sonata_name') !== 'sonata_course_batch') {
            //2015-08-29 11:00:00
            $currentDateTime = date('Y-m-d H:i:s');

            if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_ADMIN')
            ) {
                $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')
                    ->getToken()->getUser();

                if ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_ADMINCOMPANY')
                ) {
                    $companyAdmin = $currentUser->getContacts()->getIterator();
                    $company = '';
                    while ($companyAdmin->valid()) {
                        $company .= $companyAdmin->current()->getId();
                        if ($companyAdmin->key() < $companyAdmin->count() - 1) {
                            $company .= ',';
                        }
                        $companyAdmin->next();
                    }
                    $query->andWhere(
                        $query->expr()->in(
                            $query->getRootAlias() . '.company', $company
                        )
                    );
                } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_COMPANYBUSINESS')
                ) {
                    $companyBusiness = $currentUser->getBusinessCompany();
                    $courseManager = $this->getConfigurationPool()->getContainer()->get('cab.course_manager');
                    $aCoursesBusiness = $courseManager->getCoursesOfBusiness($companyBusiness);

                    if (!count($aCoursesBusiness)) {
                        $aCoursesBusiness = [0];
                    }
                    $query->andWhere(
                        $query->expr()->in(
                            $query->getRootAlias() . '.id', $aCoursesBusiness
                        )
                    );
                } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_CUSTOMER')
                ) {
                    $query->andWhere(
                        $query->expr()->eq(
                            $query->getRootAlias() . '.client', ':client'
                        )
                    )->setParameter('client', $currentUser);
                } elseif ($this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                    ->isGranted('ROLE_DRIVER')
                ) {
                    $query->andWhere(
                        $query->expr()->eq(
                            $query->getRootAlias() . '.driver', ':driver'
                        )
                    )->setParameter('driver', $currentUser);
                }
            }
        }

        // $query->setSortBy(array('departureDate' => 'desc'));

        return $query;
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Exception
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CAB\CourseBundle\Entity\Course',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($oCourse)
    {

        /** @var Course $oCourse */
        if ($oCourse->getDriver()) {
        	if($oCourse->getDriver()->getVehicle()->first() == false)
	        {
		        $oCourse->setVehicle(null);
	        }
	        else{
		        $oCourse->setVehicle($oCourse->getDriver()->getVehicle()->first());
	        }
        }
        /** @var CommandSncf $sncf */
        if ($sncf = $oCourse->getCommandSncf()) {
            if (!($sncf->getCodeBupo() || $sncf->getRlt() || $sncf->getJs() || $sncf->getCommandeCourse())) {
                $oCourse->setCommandSncf(null);
            }
        }
        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();

        $event = new GenericEvent($oCourse, array('course_history' => 1, 'status_course' => $oCourse->getCourseStatus(),'agent_id'=>$currentUser));
        $this->dispatcher->dispatch(HistoryCourseEvents::HISTORY_COURSE, $event);
        $this->dispatcher->dispatch(UpdateStatusCourseEvents::UPDATE_STATUS_COURSE, $event);
        $this->dispatcher->dispatch(StoreCourseEvents::STORE_COURSE, $event);
        $this->dispatcher->dispatch('cab.admin.event.create_course', $event);

    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($oCourse)
    {

        /** @var Course $oCourse */
        if ($oCourse->getDriver()) {
	        if($oCourse->getDriver()->getVehicle()->first() == false)
	        {
		        $oCourse->setVehicle(null);
	        }
	        else{
		        if(is_array($oCourse->getDriver()->getVehicle()) && count($oCourse->getDriver()->getVehicle()) == 1)
		        {
			        $oCourse->setVehicle($oCourse->getDriver()->getVehicle()->first());
		        }
	        }

        }
        /** @var CommandSncf $sncf */
        if ($sncf = $oCourse->getCommandSncf()) {
            if (!($sncf->getCodeBupo() || $sncf->getRlt() || $sncf->getJs() || $sncf->getCommandeCourse())) {
                $oCourse->setCommandSncf(null);
            }
        }
    }


    public function postUpdate($oCourse)
    {
        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();

        $event = new GenericEvent(
            $oCourse,
            array('course_history' => 1, 'status_course' => $oCourse->getCourseStatus(), 'agent_id' => $currentUser)
        );

        $eventGroupage = new GenericEvent($oCourse, array('groupage' => 1));

        $this->dispatcher->dispatch(HistoryCourseEvents::HISTORY_COURSE, $event);
        $this->dispatcher->dispatch(UpdateStatusCourseEvents::UPDATE_STATUS_COURSE, $event);
        $this->dispatcher->dispatch(StoreCourseEvents::STORE_COURSE, $eventGroupage);

        if($this->getForm()->get('apply_next_course_cycle')->getData() == TRUE) {


            if ($this->getForm()->has('apply_next_course_cycle')) {
                $eventCycle = new GenericEvent(
                    $oCourse, array(
                        'cycle_course' => 1,
                        'apply_next_course_cycle' => $this->getForm()->get('apply_next_course_cycle')->getData()
                    )
                );
                $this->dispatcher->dispatch(UpdateCycleCourseEvents::UPDATE_CYCLE_COURSE, $eventCycle);
            }
        }


    }


    /**
     * @param string $message
     *
     * @throws AccessDeniedException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function checkAccess($message = " Access denied", $attribute = '')
    {

    	$token = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken();

	    if($this->getSubject() != null)
	    {
		    // if the user is not a super admin we check the access
		    if(!$this->getConfigurationPool()->getContainer()->get('cab_course.security_authorization_voter.course_voter')->checkGranted($attribute, $this->getSubject(), $token))
		    {
			    throw new AccessDeniedException($message);
		    }
	    }
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function addDriverToForm(FormMapper $formMapper, $field = 'driver')
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $help = '';
        $required = false;
        if ($field == 'driver') {
            $help = "<a class='modal-with-form btn btn-default' href='#modalFormNewDriver'>New driver</a>";
            $required = true;
        }
        //getDriverByCompany($role, $currentUser = null, $authorizationChecker, $isVehicleNull = false)
        $formMapper->add(
            $field, null, array(
            'required' => $required,
            'attr' => array(
                'class' => 'select-driver',
                'type_field' => "select",
                'help' => "Driver",
            ),
            'label' => false,
            'class' => 'CABUserBundle:User',
            'help' => $help,
            'query_builder' => function (UserRepository $ur) use ($authChecker) {
                $currentUser = $this->container->get('security.token_storage');
                return $ur->getDriverByCompany('ROLE_DRIVER', $currentUser, $authChecker);
            },
        ), array(
                'admin_code' => 'cab.admin.driver',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->add('archive', 'archive');
        $collection->add('accept_course', $this->getRouterIdParameter() . '/accept-course');
        $collection->add('groupage', $this->getRouterIdParameter() . '/groupage');
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     * @throws BadMethodCallException
     * @throws \Symfony\Component\Form\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getSubject()->getId()) {
            $this->checkAccess('You are not allowed to edit this course');
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $formBuilder = $formMapper->getFormBuilder();

        /** @var User $currentUser */
        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();
        $authChecker = $this->container->get('security.authorization_checker');

        $coursesForGroupage = array();

        /** @var CompanyManager $companyManager */
        $companyManager = $this->container->get('cab.company.manager');
        $companies = $companyManager->getCompaniesByRole(false, true);

        $currentUserToken = $this->container->get('security.token_storage');
        $objectUser = $currentUserToken->getToken();

        $cCompany = $currentUser->getContacts();
        $oCompany = $cCompany->first();

        $em = $this->modelManager->getEntityManager('CAB\CourseBundle\Entity\Zone');

        $query = $em->createQueryBuilder('c')
            ->select('c')
            ->from('CABCourseBundle:Zone', 'c')
            ->where('c.companyZone = :company_id')
            ->setParameter('company_id', $oCompany)
            ->orderBy('c.zoneName', 'DESC')
	        ->setCacheable(true)->setCacheRegion('cache_long_time');

        if (!$this->getSubject()->getCycleParent()) {
            $formMapper->add(
                'departureDate', DateType::class, array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'required' => false,
                    'label' => false,
                    'attr' => array(
                        'class' => 'input-datepicker',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Departure date'),
                    ),
                )
            );
        }
        $formMapper->add(
            'departureTime', 'time', array(
                'widget' => 'single_text',
                'required' => false,
                'label' => false,
                'attr' => array(
                    'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Departure time'),
                ),
            )
        );
        if ($this->getSubject()->getIsCycle() || $this->getSubject()->getCycleParent()) {
            $formMapper->add(
                'apply_next_course_cycle', 'checkbox', array(
                    'required' => false,
                    'mapped' => false,
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Apply to all next courses of the cycle ?'),
                    ),
                    'label' => false,
                )
            );
        }else{
            $formMapper->add(
                'apply_next_course_cycle','hidden', array(
                    'required' => false,
                    'mapped' => false,
                    'attr' => array(
                        'disabled'=>true,
                    ),
                    'label' => false,
                )
            );
        }
        if ($this->getSubject()->getCycleParent()) {
            $formMapper->add(
                'apply_next_course_cycle', 'checkbox', array(
                    'required' => false,
                    'mapped' => false,
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Apply to all next courses of the cycle ?'),
                    ),
                    'label' => false,
                )
            );
        }

        $adminCode = 'cab.admin.agent';
        // @noinspection ExceptionsAnnotatingAndHandlingInspection
        if (false === $authChecker->isGranted('ROLE_ADMIN')) {
            $formMapper->add('company', 'hidden', array(), array('admin_code' => 'cab.admin.company',));
            if ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
                $companyAdmin = $objectUser->getUser()->getContacts()->first();
                $this->addDriverToForm($formMapper);
                $this->addDriverToForm($formMapper, 'driverBack');
                self::$statusCourse[Course::STATUS_CANCELLED_BY_ADMIN] = 'Cancelled By Admin';
                $adminCode = 'cab.admin.agent';
            } elseif ($authChecker->isGranted('ROLE_AGENT')) {
                $companyAdmin = $objectUser->getUser()->getAgentCompany();
                $this->addDriverToForm($formMapper);
                $this->addDriverToForm($formMapper, 'driverBack');
                self::$statusCourse[Course::STATUS_CANCELLED_BY_ADMIN] = 'Cancelled By Admin';
                $adminCode = 'cab.admin.agent';
            } elseif ($authChecker->isGranted('ROLE_COMPANYBUSINESS')) {
                $companyBusiness = $objectUser->getUser()->getBusinessCompany();
                $adminCompanyUser = $companyBusiness->getContact();
                $companyAdmin = $adminCompanyUser->getContacts()->first();
                $adminCode = 'cab.admin.agent';
            } elseif ($authChecker->isGranted('ROLE_DRIVER')) {
                $companyAdmin = $objectUser->getUser()->getDriverCompany();
                $formMapper->add('driver', 'hidden', array(), array('admin_code' => 'cab.admin.driver',));
                $adminCode = 'cab.admin.driver';
                try {
                    $formBuilder->get('driver')->addModelTransformer(new UserToIntTransformer($em, $currentUser));
                    $this->addDriverToForm($formMapper, 'driverBack');
                } catch (\Exception $e) {
                    throw new BadMethodCallException($e->getMessage());
                }
            }

            $formBuilder->get('company')->addModelTransformer(new CompanyToIntTransformer($em, $companyAdmin));
        } else {
            $companyAdmin = null;
            $this->addDriverToForm($formMapper);
            $this->addDriverToForm($formMapper, 'driverBack');
        }

        $formMapper->add(
            'createdBy', 'hidden', array(), array('admin_code' => $adminCode)
        );
        $formBuilder->get('createdBy')->addModelTransformer(new UserToIntTransformer($em, $currentUser));
        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $formMapper->add(
                'company',
                null,
                array(
                    'attr' => array(
                        'class' => 'company-select',
                    ),
                    'class' => 'CABCourseBundle:Company',
                    'query_builder' => function (CompanyRepository $cr) {
                        return $cr->getActiveCompanies();
                    },
                    'required' => true,
                ),
                array('admin_code' => 'cab.admin.company',)
            );
        }
        if ($authChecker->isGranted('ROLE_AGENT')) {
            $formMapper->add('serviceTransportCourses', null, array(
                    'label' => false,
                    'attr' => array(
                        'help' => 'Service transport',
                    ),
                    'query_builder' => function (ServicetransportRepository $str) {
                        if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                                ->isGranted('ROLE_SUPER_ADMIN')
                        ) {
                            $currentUser = $this->container->get('security.token_storage');
                            $cCompany = $currentUser->getToken()->getUser()->getContacts();
                            $oCompany = $cCompany->first();
                        } else {
                            $oCompany = null;
                        }

                        return $str->getServicesTransportByCompany($oCompany);
                    },
                )
            )
                ->add('serviceTransportCoursesBack', null, array(
                        'label' => false,
                        'attr' => array(
                            'help' => 'Service transport back',
                        ),
                    )
                ) ->add('commandSncf', 'sonata_type_admin', array(
                        'label' => false,
                        'required' => false,
                        'delete' => 'false',
                        'btn_delete' => 'false',
                        'attr' => array(
                            'help' => 'Sncf Command',
                        ),
                    )
                )
                ->add(
                    'courseTime', 'text', array(
                        'required' => false,
                        'label' => false,
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Course time'),
                           // 'readonly' => 'readonly',
                        ),
                    )
                )
                ->add(
                    'courseTimeBack', 'text', array(
                        'required' => false,
                        'label' => false,
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Course time back'),
                            'readonly' => 'readonly',
                        ),
                    )
                )
                ->add(
                    'distance', 'text', array(
                        'label' => false,
                        'attr' => array(
                            'placeholder' => 'Distance',
                            'help' => 'Distance',
                            //'readonly' => 'readonly',
                        ),
                    )
                )
                ->add(
                    'distanceBack', 'text', array(
                        'label' => false,
                        'attr' => array(
                            'placeholder' => 'Distance back',
                            'help' => 'distance back',
                            'readonly' => 'readonly',
                        ),
                    )
                )
                ->add(
                    'arrivalHour', 'time', array(
                        'widget' => 'single_text',
                        'required' => false,
                        'label' => false,
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('arrival hour'),
                        ),
                    )
                )->add(
                    'arrivalHourBack', 'time', array(
                        'widget' => 'single_text',
                        'required' => false,
                        'label' => false,
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('arrival hour back'),
                        ),
                    )
                );
        }

        $formMapper
            ->add(
                'client', null, array(
                'attr' => array(
                    'class' => 'select-user',
                    'type_field' => 'select',
                    'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Customer'),
                ),
                'label' => false,
                'class' => 'CABUserBundle:User',
                'query_builder' => function (UserRepository $ur) use ($companies, $authChecker) {
                    return $ur->getUsersByRoleAndCompany('ROLE_CUSTOMER', $companies, $authChecker);
                },
            ), array(
                    'admin_code' => 'cab.admin.customer',
                )
            )
            ->add('zone','sonata_type_model', array(
                'required' => false,
                'query' => $query
            ))
            ->add(
                'courseStatus', ChoiceType::class, array(
                    'label' => false,

                    'choices' => array_flip(self::$statusCourse),
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the course status'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Course status'),
                        'addon' => 'fa-cog',
                    ),
                )
            )
            ->add(
                'startedAddress', 'text', array(
                    'label' => false,
                    'attr' => array(
                        'catalogue' => 'backend',
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the departure address'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('departure address'),
                        'addon' => 'fa-map-marker',
                        'class' => 'maps_input',

                    ),
                )
            )
            ->add(
                'indicatorStartedAddress', 'text', array(
                    'label' => false,
                    'required' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('hotel, number fight, office, etc'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('hotel, number fight, office, etc'),
                        'addon' => 'fa-info',
                    ),
                )
            )
            ->add(
                'backAddressDeparture', 'text', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the back departure address'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Back departure address'),
                        'addon' => 'fa-map-marker',
                    ),
                )
            )
            ->add(
                'indicatorBackAddressDeparture', 'text', array(
                    'label' => false,
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'hotel, number fight, office, etc',
                        'help' => 'hotel, number fight, office, etc',
                        'addon' => 'fa-info',
                    ),
                )
            )
            ->add(
                'backAddressArrival', 'text', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the back arrival address'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Back arrival address'),
                        'addon' => 'fa-map-marker',
                    ),
                )
            )
            ->add(
                'indicatorBackAddressArrival', 'text', array(
                    'label' => false,
                    'required' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('hotel, number fight, office, etc'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('hotel, number fight, office, etc'),
                        'addon' => 'fa-info',
                    ),
                )
            )->add(
                'arrivalAddress', 'text', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the arrival address'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Arrival address'),
                        'addon' => 'fa-map-marker',
                    ),
                )
            )
            ->add(
                'indicatorArrivalAddress', 'text', array(
                    'label' => false,
                    'required' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('hotel, number fight, office, etc'),
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('hotel, number fight, office, etc'),
                        'addon' => 'fa-info',
                    ),
                )
            )
            ->add(
                'personNumber', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the number of person'),
                        'addon' => 'fa-group',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of passager'),
                    ),
                )
            )
            ->add(
                'laguage', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Suitecase'),
                        'addon' => 'fa-suitcase',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('suitecase for transport'),
                    ),
                )
            )
            ->add(
                'nbWheelchair', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of Wheelchair'),
                        'addon' => 'fa-wheelchair',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of Wheelchair'),
                    ),
                )
            )
            ->add(
                'nbChildren', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of children'),
                        'addon' => 'fa-child',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of children'),
                    ),
                )
            )
            ->add(
                'personNumberBack', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Indicate the number of person for back course'),
                        'addon' => 'fa-group',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of passager for back course'),
                    ),
                )
            )
            ->add(
                'laguageBack', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Suitecase'),
                        'addon' => 'fa-suitcase',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('suitecase for transport'),
                    ),
                )
            )
            ->add(
                'nbWheelchairBack', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of Wheelchair'),
                        'addon' => 'fa-wheelchair',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of Wheelchair'),
                    ),
                )
            )
            ->add(
                'nbChildrenBack', 'integer', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of children'),
                        'addon' => 'fa-child',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('number of children'),
                    ),
                )
            )
            ->add(
                'descriptionChildren', 'textarea', array(
                    'label' => false,
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Information add'),
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Children number, age, big suitecase, etc'),
                        'rows' => 5,
                        'maxlength' => 140,
                        'data-plugin-maxlength' => '',
                    ),
                    'required' => false,
                )
            )
            ->add(
                'descriptionChildrenBack', 'textarea', array(
                    'attr' => array(
                        'placeholder' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Children number, age, big suitecase, etc for Back course'),
                        'rows' => 5,
                        'maxlength' => 140,
                        'data-plugin-maxlength' => '',
                    ),
                    'required' => false,
                )
            )
            ->add('targetType', 'checkbox', array(
                'label' => 'Goings and Comings',
                'attr' => array(
                    'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Coming race'),
                ),
                'required' => false,
            ))
            ->add('IsGroupage', 'checkbox', array(
                'label' => 'groupage',
                'attr' => array(
                'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('GRoupage'),
                    ),
                'required' => false,
                ))
            ->add(
                'isGroup', 'checkbox', array(
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is group ?'),
                    ),
                    'label' => false,
                    'required' => false,
                )
            )->add(
                'forceHourArrived', 'checkbox', array(
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Force hour arrival'),
                        'class' => 'center',
                    ),
                    'label' => false,
                    'required' => false,
                )
            )
            ->add(
                'forceHourArrivedBack', 'checkbox', array(
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Force hour arrival back'),
                    ),
                    'label' => false,
                    'required' => false,
                )
            )
            ->add(
                'backDate', DateType::class, array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Back date'),
                    ),
                    'label' => false,
                    'required' => false,
                )
            )
            ->add(
                'backTime', 'time', array(
                'widget' => 'single_text',
                'attr' => array(
                    'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Back time'),
                ),
                'label' => false,
                'required' => false,
            ))
            ->add(
                'priceHT', 'text', array(
                'attr' => array(
                    'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Price HT'),
                ),
                'label' => false,
                'required' => true,
            ))
            ->add(
                'priceTTC', 'text', array(
                'attr' => array(
                    'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Price TTC'),
                ),
                'label' => false,
                'required' => true,
            ));
        $formMapper->add('isHandicap', 'checkbox', array(
            'label' => false,
            'required' => false,
            'attr' => array(
                'class' => 'select-driver',
                'type_field' => 'select',
                'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is handicap'),
            ),
        ))
            ->add('longDep', 'hidden', array(
                'required' => false,
            ))
            ->add('latDep', 'hidden', array(
                'required' => false,
            ))
            ->add('longArr', 'hidden', array(
                'required' => false,
            ))
            ->add('latArr', 'hidden', array(
                'required' => false,
            ))
            ->add('longDepBack', 'hidden', array(
                'required' => false,
            ))
            ->add('latDepBack', 'hidden', array(
                'required' => false,
            ))
            ->add('longArrBack', 'hidden', array(
                'required' => false,
            ))
            ->add('latArrBack', 'hidden', array(
                'required' => false,
            ))
            ->add('startedAddressCity', 'hidden', array(
                'required' => false,
            ))
            ->add('arrivalAddressCity', 'hidden', array(
                'required' => false,
            ))
            ->add('typeVehicleCourse', 'sonata_type_model', array(
                'attr' => array('class' => 'hide', 'help' => 'vehicle type'),

                'btn_add' => false,
                'btn_list' => false,
                'btn_delete' => false,
                'btn_catalogue' => false,
            ))
            ->add('vehicle', 'sonata_type_model', array(
                'attr' => array('class' => 'hide', 'help' => 'vehicle'),
                'btn_add' => false,
                'btn_list' => false,
                'btn_delete' => false,
                'btn_catalogue' => false,
            ))
            ->add('priceHT', 'text', array(
                'required' => false,
                'attr' => array(
                    //'readonly' => 'readonly',
                    'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Price HT'),
                ),
            ))
            ->add('priceTTC', 'text', array(
                    'required' => false,
                    'attr' => array(
                        //'readonly' => 'readonly',
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Price TTC'),
                    ),
                )
            )
            ->add('reverse', 'checkbox', array(
                    'required' => false,
                    'mapped' => false,
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Reverse The distination'),
                    ),
                )
            );

        if (!$this->getSubject()->getCycleParent()) {
            $formMapper->add(
                'isCycle', 'checkbox', array(
                    'attr' => array(
                        'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Cycle ?'),
                    ),
                    'label' => false,
                    'required' => false,
                )
            )
                ->add(
                    'mondayCycle', 'checkbox', array(
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Monday'),
                        ),
                        'label' => false,
                        'required' => false,
                    )
                )->add(
                    'tuesdayCycle', 'checkbox', array(
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Tuesday'),
                        ),
                        'label' => false,
                        'required' => false,
                    )
                )->add(
                    'wednesdayCycle', 'checkbox', array(
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Wednesday'),
                        ),
                        'label' => false,
                        'required' => false,
                    )
                )->add(
                    'thursdayCycle', 'checkbox', array(
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Thursday'),
                        ),
                        'label' => false,
                        'required' => false,
                    )
                )->add(
                    'fridayCycle', 'checkbox', array(
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Friday'),
                        ),
                        'label' => false,
                        'required' => false,
                    )
                )->add(
                    'saturdayCycle', 'checkbox', array(
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Saturday'),
                        ),
                        'label' => false,
                        'required' => false,
                    )
                )->add(
                    'sundayCycle', 'checkbox', array(
                        'attr' => array(
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Sunday'),
                        ),
                        'label' => false,
                        'required' => false,
                    )
                )
                ->add('cycleEndDate', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'yyyy-MM-dd',
                        'required' => false,
                        'label' => false,
                        'attr' => array(
                            'class' => 'input-datepicker',
                            'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Date End of cycle'),
                        ),
                    )
                );
        }

        $formMapper->add('serviceParticularPeriodCourses', null, array(
            'label' => false,
            'attr' => array(
                'help' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Service Particular Period Exclu'),
            ),
            'query_builder' => function (ParticularPeriodRepository $str) {
                if (false === $this->getConfigurationPool()->getContainer()->get('security.authorization_checker')
                        ->isGranted('ROLE_SUPER_ADMIN')
                ) {
                    $currentUser = $this->container->get('security.token_storage');
                    $cCompany = $currentUser->getToken()->getUser()->getContacts();
                    $oCompany = $cCompany->first();
                } else {
                    $oCompany = null;
                }

                return $str->getParticularPeriodByCompany($oCompany);
            },
        ));

    }

    // Fields to be shown on filter forms.
    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        $ServiceCompagny = array(
            'class' => 'CAB\CourseBundle\Entity\ServiceTransport',
            'required' => false,
            'query_builder' => function (ServicetransportRepository $str) {
                $currentUser = $this->container->get('security.token_storage');
                $cCompany = $currentUser->getToken()->getUser()->getContacts();
                $oCompany = $cCompany->first();

                return $str->getServicesTransportByCompany($oCompany);
            },
        );
	    $adminCode = 'cab.admin.agent';
        if ($authChecker->isGranted('ROLE_ADMINCOMPANY')) {
            $datagridMapper
                ->add('id')
                ->add(
                    'client',
                    null,
                    array(
                        'field_type' => EntityType::class,
						'admin_code'  => 'cab.admin.customer',
                        'field_options' => array(
                            'query_builder' => function (UserRepository $ur) use ($authChecker) {
                                $currentUser = $this->container->get('security.token_storage');

                                return $ur->getCustomerByCompany('ROLE_CUSTOMER', $currentUser, $authChecker);
                            }
                        ),
                        'operator_options' => array(
                            'choices' => array(
                                EqualType::TYPE_IS_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is equal'),
                                EqualType::TYPE_IS_NOT_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is different'),

                            ),
                        ),

                    )


                )
                ->add(
                    'driver',
                    null,
                    array(
                        'field_type' => EntityType::class,
                        'admin_code'=>'cab.admin.driver',
                        'field_options' => array(
                            'query_builder' => function (UserRepository $ur) use ($authChecker) {
                                $currentUser = $this->container->get('security.token_storage');

                                return $ur->getDriverByCompany('ROLE_DRIVER', $currentUser, $authChecker);
                            }
                        ),
                        'operator_options' => array(
                            'choices' => array(
                                EqualType::TYPE_IS_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is equal'),
                                EqualType::TYPE_IS_NOT_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is different')
                            )
                        ),
                    )
                )
                ->add(
                    'serviceTransportCourses',
                    null,
                    array(
                        'field_options' => array(
                            'expanded' => true,
                            'multiple' => true,
                            'query_builder' => function (ServicetransportRepository $str) {
                                $currentUser = $this->container->get('security.token_storage');
                                $cCompany = $currentUser->getToken()->getUser()->getContacts();
                                $oCompany = $cCompany->first();

                                return $str->getServicesTransportByCompany($oCompany);
                            }
                        )
                    )
                )
                ->add(
                    'zone',
                    null,
                    array(
                        'field_options' => array(
                            'expanded' => true,
                            'multiple' => true,
                            'query_builder' => function (ZoneRepository $str) {
                                $currentUser = $this->container->get('security.token_storage');
                                $cCompany = $currentUser->getToken()->getUser()->getContacts();
                                $oCompany = $cCompany->first();

                                return $str->getZoneByCompany($oCompany);
                            }
                        )
                    )
                )
                ->add('arrivalAddress')
                ->add(
                    'isCycle',
                    null,
                    array
                    (
                        'translationDomain' => 'backend',
                        'label' => 'Cycle',
                        'field_type' => 'checkbox',
                    ),
                    ChoiceType::class,
                    array('choices' => array(0 => $this->getConfigurationPool()->getContainer()->get('translator')->trans('NO'), 1 => $this->getConfigurationPool()->getContainer()->get('translator')->trans('YES')))
                )
                ->add(
                    'courseStatus',
                    'doctrine_orm_choice',
                    array(
                        'translationDomain' => 'backend'
                    ),
                    ChoiceType::class,
                    array(
                        'choices' =>
                            array(
                                '0' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CREATED'),
                                '2' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('REJECTED'),
                                '3' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CANCELLED BY CLIENT'),
                                '15' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CANCELLED BY DRIVER'),
                                '4' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('AFFECTED'),
                                '6' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('DONE'),
                                '24' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('DI'),
                                '22' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('AD'),
                            ),
                    )

                )
                // ->add('isHandicap')
                //s ->add('targetType')
                ->add('isGroupage')
                ->add(
                    'departureDate',
                    'doctrine_orm_datetime_range',
                    array('label' => 'Date departure'),
						null,
                    array(
                    	'field_type' => DateType::class,

	                    'field_options' =>array(
                        'widget' => 'single_text',
                        'format' => 'yyyy-MM-dd',
                        'required' => false,
                        'attr' => array('class' => 'datetimepicker', 'data-date-format' => 'yyyy-mm-dd'),


                    ))

                );


        } elseif ($authChecker->isGranted('ROLE_COMPANYBUSINESS')) {

            $datagridMapper
                ->add(
                    'client',
                    null,
                    array(
                        'field_type' => EntityType::class,
                        'field_options' => array(
                            'query_builder' => function (UserRepository $ur) use ($authChecker) {
                                $currentUser = $this->container->get('security.token_storage');

                                return $ur->getCustomerByCompany('ROLE_CUSTOMER', $currentUser, $authChecker);
                            }
                        ),
                    )
                )
                ->add(
                    'courseStatus',
                    'doctrine_orm_choice',
                    array(
                        'translationDomain' => 'backend',
                        'field_options' => array(
                            'choices' =>
                                array(
                                    '0' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CREATED'),
                                    '2' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('REJECTED'),
                                    '3' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CANCELLED BY CLIENT'),
                                    '15' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CANCELLED BY DRIVER'),
                                    '4' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('AFFECTED'),
                                    '6' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('DONE'),
                                    '24' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('DI'),
                                    '22' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('AD'),
                                    '23' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('PRE-COMMANDE'),

                                ),
                            'required' => false,
                            'multiple' => true,
                            'expanded' => false,
                        ),
                        'field_type' => ChoiceType::class,
                    )
                )
                ->add(
                    'departureDate',
                    'doctrine_orm_datetime_range',
                    array('label' => 'Date departure'),
                    null,
                    array(
                        'widget' => 'single_text',
                        'format' => 'yyyy-MM-dd',
                        'required' => false,
                        'attr' => array('class' => 'datetimepicker', 'data-date-format' => 'yyyy-mm-dd')
                    )
                );

        } elseif ($authChecker->isGranted('ROLE_AGENT')) {
            $datagridMapper
                ->add('id')
                ->add(
                    'client',
                    null,
                    array(
                        'field_type' => EntityType::class,
                        'admin_code' => 'cab.admin.agent',
                        'field_options' => array(
                            'query_builder' => function (UserRepository $ur) use ($authChecker) {
                                $currentUser = $this->container->get('security.token_storage');

                                return $ur->getCustomerByCompany('ROLE_CUSTOMER', $currentUser, $authChecker);
                            }
                        ),
                        'operator_options' => array(
                            'choices' => array(
                                EqualType::TYPE_IS_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is equal'),
                                EqualType::TYPE_IS_NOT_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is different')
                            ),
                            'admin_code' => 'cab.admin.agent',

                        ),
                    )
                )
                ->add(
                    'driver',
                    null,
                    array(
                        'field_type' => EntityType::class,

                        'field_options' => array(
                            'query_builder' => function (UserRepository $ur) use ($authChecker) {
                                $currentUser = $this->container->get('security.token_storage');

                                return $ur->getDriverByCompany('ROLE_DRIVER', $currentUser, $authChecker);
                            }
                        ),
                        'operator_options' => array(
                            'choices' => array(
                                EqualType::TYPE_IS_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is equal'),
                                EqualType::TYPE_IS_NOT_EQUAL => $this->getConfigurationPool()->getContainer()->get('translator')->trans('Is different')
                            ),
                            'admin_code' => 'cab.admin.driver'

                        ),
                    )

                )
                ->add(
                    'serviceTransportCourses',
                    null,
                    array(
                        'field_options' => array(
                            'expanded' => true,
                            'multiple' => true,
                            'query_builder' => function (ServicetransportRepository $str) {
                                $currentUser = $this->container->get('security.token_storage');
                                $cCompany = $currentUser->getToken()->getUser()->getContacts();
                                $oCompany = $cCompany->first();

                                return $str->getServicesTransportByCompany($oCompany);
                            }
                        )
                    )
                )
                ->add(
                    'zone',
                    null,
                    array(
                        'field_options' => array(
                            'expanded' => true,
                            'multiple' => true,
                            'query_builder' => function (ZoneRepository $str) {
                                $currentUser = $this->container->get('security.token_storage');
                                $cCompany = $currentUser->getToken()->getUser()->getContacts();
                                $oCompany = $cCompany->first();

                                return $str->getZoneByCompany($oCompany);
                            }
                        )
                    )
                )
                ->add('arrivalAddress')
                ->add(
                    'isCycle',
                    null,
                    array
                    (
                        'translationDomain' => 'backend',
                        'label' => 'Cycle',
                        'field_type' => 'checkbox',
                    ),
                    ChoiceType::class,
                    array('choices' => array(0 => $this->getConfigurationPool()->getContainer()->get('translator')->trans('NO'), 1 => $this->getConfigurationPool()->getContainer()->get('translator')->trans('YES')))
                )
                ->add(
                    'courseStatus',
                    'doctrine_orm_choice',
                    array(
                        'translationDomain' => 'backend'
                    ),
                    ChoiceType::class,
                    array(
                        'choices' =>
                            array(
                                '0' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CREATED'),
                                '2' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('REJECTED'),
                                '3' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CANCELLED BY CLIENT'),
                                '15' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('CANCELLED BY DRIVER'),
                                '4' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('AFFECTED'),
                                '6' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('DONE'),
                                '24' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('DI'),
                                '22' => $this->getConfigurationPool()->getContainer()->get('translator')->trans('AD'),
                            ),
                    )

                )
                // ->add('isHandicap')
                //s ->add('targetType')
                ->add('isGroupage')
                ->add(
                    'departureDate',
                    'doctrine_orm_datetime_range',
                    array('label' => 'Date departure'),
                    null,
                    array(
                        'widget' => 'single_text',
                        'format' => 'yyyy-MM-dd',
                        'required' => false,
                        'attr' => array('class' => 'datetimepicker', 'data-date-format' => 'yyyy-mm-dd')
                    )
                );
        }

    }
    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier(
                'id', null, array(
                    'route' => array(
                        'name' => 'show',
                    ),
                )
            )


            ->add(
                'company', '', array(
                    'admin_code' => 'cab.admin.company',
                )
            )
            ->add(
                'client', '', array(
                    'admin_code' => 'cab.admin.customer',
                )
            )
            ->add('isCycle')
            ->add('departureDate')
            ->add('departureTime')
            ->add('startedAddress')
            ->add('arrivalAddress')
            ->add('personNumber')
            ->add('targetType')
            ->add('courseStatus')
            ->add('isHandicap')
            ->add('priceHT')
            ->add(
                'driver', '', array(
                    'admin_code' => 'cab.admin.driver',
                )
            )
            ->add('vehicle')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                        'groupage' => array(),
                    ),
                )
            );
        //->add('ratings');

    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        if ($this->getSubject()) {
            $this->checkAccess('You are not allowed to show this service', 'VIEW');
        }

        $race_id = $this->getSubject();
        $em = $this->container->get('doctrine.orm.entity_manager');
        $ratingcustomer = $em->getRepository('CABCourseBundle:Rating')->getAverageRaceCustomer($race_id);
        $ratingdriver = $em->getRepository('CABCourseBundle:Rating')->getAverageRaceDriver($race_id);
        $racehistory = $em->getRepository('CABCourseBundle:CourseHistory')->getHistoryRace($race_id);

        // Here we set the fields of the ShowMapper variable, $showMapper (but this can be called anything)
        $showMapper->add('startedAddress')
            ->add('arrivalAddress')
            ->add('personNumber')
            ->add('targetType')
            ->add('courseStatus')
            ->add('priceHT')
            ->add('isHandicap')
            ->add('data', null, array('ratingcustomer' => $ratingcustomer, 'ratingdriver' => $ratingdriver, 'racehistory' => $racehistory));
    }

}
