<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 13/08/2015
 * Time: 22:32
 */


namespace CAB\AdminBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use CAB\UserBundle\Entity\User;

class UserToNumberTransformer implements DataTransformerInterface
{
    /**
     * Transforms an object (user) to a string (number).
     *
     * @param  User|null $user
     * @return string
     */
    public function transform($user)
    {
        if (null === $user) {
            return "";
        }

        return $user->getNumber();
    }

    /**
     * Transforms a string (number) to an object (user).
     *
     * @param  string $number
     * @return User|null
     * @throws TransformationFailedException if object (user) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserBy(array('id' => $number));

        if (null === $user) {
            throw new TransformationFailedException(sprintf(
                'ID "%s" can\'t be found!',
                $number
            ));
        }

        return $user;
    }
}