<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 02/11/2016
 * Time: 23:52
 */

namespace CAB\AdminBundle\Form\DynamicData;


use CAB\AdminBundle\Admin\DataTransformer\DriverToIntTransformer;
use CAB\UserBundle\Entity\UserRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Form\FormBuilder;

class FormItemBasedUserData
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function addDriverItemForm(FormBuilder $formBuilder)
    {
        $currentUser = $this->tokenStorage->getToken()->getUser();
        //$formBuilder->get('driverDay')->addModelTransformer(new DriverToIntTransformer($em, $oDriver));
        $formBuilder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($currentUser) {
                $form = $event->getForm();
                $data = $event->getData();
                if ($data !== null) {
                    if ($data->getVehicule() === null) {
                        $oVehicle = null;
                    } else {
                        $oVehicle = $data->getVehicule();
                        $formOptions = array(
                            'class' => 'CAB\UserBundle\Entity\User',
                            'choice_label' => 'getUsedName',
                            'query_builder' => function (UserRepository $ur) use ($oVehicle) {
                                return $ur->getDriverByVehicle($oVehicle->getId());
                            },

                        );

                        $form->add('driver', null, $formOptions, array('admin_code' => 'cab.admin.driver'));
                    }
                } else {
                    $form->add('driver',
                        'choice',
                        array(
                            'required' => false,
                            'choices' => array(),
                            'mapped' => false,

                        ),
                        array(
                            'admin_code' => 'cab.admin.driver'
                        )
                    );
                }

            }
        );
    }
}