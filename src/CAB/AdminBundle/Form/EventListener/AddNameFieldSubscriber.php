<?php
/**
 * Created by PhpStorm.
 * User: m.benhedna
 * Date: 29/11/2015
 * Time: 19:24
 */

namespace CAB\AdminBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddNameFieldSubscriber implements EventSubscriberInterface {

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $tarif = $event->getData();
        $form = $event->getForm();

        if (null != $tarif) {
            switch ($tarif->getTarifType()) {
                case 'Tarif taxi':
                    $this->setFieldForTarifTaxi($form);
                    break;
                case 'Tarif au km parcourus ':
                    $this->setFieldForTarifKm($form);
                    break;
                case 'Tarif au km parcourus ':
                    $this->setFieldForTarifKmCovered($form);
                    break;
            }
        }
    }

    /**
     * @param $form
     */
    protected function setFieldForTarifTaxi($form)
    {
        $hoursChoices = array(
            'Night hours - Tarif B and D' => array(
                1 => 'From 19h00 To 07h00',
                2 => 'From 20h00 To 08h00',
            ),
        );

        $form->add('tarifPC', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Supported'),
            ));
        $form->add('tarifA', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Tarif A'),
        ));
        $form->add('tarifB', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Tarif B'),
            ));
        $form->add('tarifC', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Tarif C'),
            ));
        $form->add('tarifD', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Tarif D'),
            ));
        $form->add('slowWalkPerHour', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Slow Walk / Hour'),
            ));
        $form->add('luggage', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'luggage'),
            ));
        $form->add('person', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Person'),
            ));
        $form->add('minPrice', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Min price'),
            ));
        $form->add('periodWork', 'choice', array(
            'label' => false,
            'required' => false,
            'choices'  => $hoursChoices,
            'placeholder' => false,
            'attr' => array('addon' => 'Period work'),
            ));
        $form->add('packageApproach', 'text', array(
            'label' => false,
            'required' => false,
            'attr' => array('addon' => 'Package approach'),
            ));
    }

    /**
     * @param $form
     */
    protected function setFieldForTarifKm($form)
    {
        $form->add('tarifPC', 'text');

    }

    /**
     * @param $form
     */
    protected function setFieldForTarifKmCovered($form)
    {
        $form->add('tarifPC', 'text');

    }
}