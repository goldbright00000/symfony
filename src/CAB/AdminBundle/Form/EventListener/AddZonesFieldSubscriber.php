<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 10/10/2015
 * Time: 00:04
 */
namespace CAB\AdminBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddZonesFieldSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($data !== null) {

                $form->add('zones', 'entity', array(
                    'multiple' => true,
                    'attr' => array(
                        'class' => "zone-select"),
                    'label_attr' => array(
                        'class' => 'control-label required col-sm-3'),
                    'class' => 'CABCourseBundle:Zone',
                    'placeholder' => '',
                    'choices' => $data->getUserCompany()->getCompanyZones(),
                ));
            }

    }
}