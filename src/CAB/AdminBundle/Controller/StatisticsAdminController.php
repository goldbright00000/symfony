<?php

namespace CAB\AdminBundle\Controller;

use CAB\CourseBundle\Document\Repository\GeolocationRepository;
use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\CourseRepository;
use CAB\CourseBundle\Entity\VehiculeClean;
use CAB\CourseBundle\Entity\VehiculeCleanRepository;
use CAB\CourseBundle\Entity\VehiculeFuel;
use CAB\CourseBundle\Entity\VehiculeFuelRepository;
use CAB\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use CAB\CourseBundle\Document\Geolocation;
use Doctrine\ODM\MongoDB\DocumentRepository;
use DateTime;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use CAB\MainBundle\Form\ReportType;



class StatisticsAdminController extends Controller
{
    public function listAction()
    {

    $statisticsData = 'toto';
    $em = $this->getDoctrine()->getManager();
    $odm = $this->get('doctrine_mongodb');
    $user = $this->get('security.token_storage')->getToken()->getUser();




        $finalResult = ['data' => []];
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {

            $countDrivers = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_DRIVER'));
            $countCustomer = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_CUSTOMER'));
            $countVehicle = $em->getRepository('CABCourseBundle:Vehicule')->getTotalvehicle();
            $sumca = $em->getRepository('CABCourseBundle:Course')->getTotalCaAll();
            $countRace = $em->getRepository('CABCourseBundle:Course')->getTotalRaceAll();
            $totalCompany = $em->getRepository('CABCourseBundle:Course')->getTotalcompany();
            $bestCompanylist = $em->getRepository('CABCourseBundle:Course')->getBestcompanylist();
            $bestCompanyRacelist = $em->getRepository('CABCourseBundle:Course')->getBestcompanyRacelist();
            $listCustomerNumberByCompany = $em->getRepository('CABUserBundle:User')->getCustomerListStat('ROLE_CUSTOMER');
            $listDriverNumberByCompany = $em->getRepository('CABUserBundle:User')->getCustomerListStat('ROLE_DRIVER');
            $lineChartsCountTrack = $em->getRepository('CABApiBundle:TrackLogin')->getLineChartTrackAllcompany();
            $flotBarsData = $em->getRepository('CABCourseBundle:Course')->getflotBarsDatacompanylist();
            //$testrepo = $odm->getRepository('CABCourseBundle:Geolocation')->getTestDrivers();

            foreach ($flotBarsData as $key => $value){
		$flotBarsData[] = array(trim($value['month']),trim(round(($value['total']))));
		}

            return $this->render('CABAdminBundle:Stats:stats.html.twig', array(
                    'statistics_data'  => $statisticsData,
                     'countdriver' => $countDrivers,
                     'countcustomer'=>$countCustomer,
                     'countvehicule'=>$countVehicle,
                     'sumca'=> $sumca,
                     'countrace' => $countRace,
                     'totalcompany' => $totalCompany,
                     'bestcompanylist' => $bestCompanylist,
                     'bestcompanyracelist' => $bestCompanyRacelist,
                     'listcustomerbycompany'=>$listCustomerNumberByCompany,
                     'listdriverbycompany'=>$listDriverNumberByCompany,
                     'flotBarsData' => $flotBarsData,
                    'lineChartsCountTrack'=>$lineChartsCountTrack,
                     //'test' => $testrepo,

                ));

        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINCOMPANY')) {

            $currentUser = $this->get('security.token_storage')
                ->getToken()->getUser();
            $companyId = $currentUser->getContacts()->first()->getId();

            $countDrivers = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_DRIVER'));
            $countCustomer = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_CUSTOMER'));
            $countVehicle = $em->getRepository('CABCourseBundle:Vehicule')->getTotalvehicle();
            $sumca = $em->getRepository('CABCourseBundle:Course')->getTotalCaByCompany($companyId);
            $countRace = $em->getRepository('CABCourseBundle:Course')->getTotalRaceByCompany($companyId);
            $countRaceStatus = $em->getRepository('CABCourseBundle:Course')->getRaceByCompanyStatus($companyId);
            $countTotalRaceStatus = $em->getRepository('CABCourseBundle:Course')->getTotalRaceByCompanyStatus($companyId);
            $totalCompany = $em->getRepository('CABCourseBundle:Course')->getTotalcompany();
            $bestCompanylist = $em->getRepository('CABCourseBundle:Course')->getBestcompanylist();
            $bestCompanyRacelist = $em->getRepository('CABCourseBundle:Course')->getBestcompanyRacelist();
            $CustomerNumberByCompany = $em->getRepository('CABUserBundle:User')->getCustomerListStatByCompany('ROLE_CUSTOMER',$companyId);
            $DriverNumberByCompany = $em->getRepository('CABUserBundle:User')->getDriverListStatByCompany('ROLE_DRIVER',$companyId);
            $lineChartsCountTrack = $em->getRepository('CABApiBundle:TrackLogin')->getLineChartTrack($companyId);
            $flotBarsData = $em->getRepository('CABCourseBundle:Course')->getflotBarsDatabycompany($companyId);


            foreach ($flotBarsData as $key => $value){
		$flotBarsData[] = array(trim($value['month']),trim(round(($value['total']))));
		}


            return $this->render('CABAdminBundle:Stats:stats.html.twig', array(
                    'statistics_data'  => $statisticsData,
                     'countdriver' => $countDrivers,
                     'countcustomer'=>$countCustomer,
                     'countvehicule'=>$countVehicle,
                     'sumca'=> $sumca,
                     'countrace' => $countRace,
                     'countracestatus' => $countRaceStatus,
                     'countTotalRaceStatus' => $countTotalRaceStatus,
                     'totalcompany' => $totalCompany,
                     'bestcompanylist' => $bestCompanylist,
                     'bestcompanyracelist' => $bestCompanyRacelist,
                     'listcustomerbycompany'=>$CustomerNumberByCompany,
                     'listdriverbycompany'=>$DriverNumberByCompany,
                     'flotBarsData' => $flotBarsData,
                     'lineChartsCountTrack'=>$lineChartsCountTrack
                ));
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_DRIVER')) {

           $driverId = $this->container->get('security.token_storage')->getToken()->getUser()->getId();

           $flotBarsData = $em->getRepository('CABCourseBundle:Course')->getFlotBarsDatabydriver($driverId);


            return $this->render(
                'CABAdminBundle:Stats:drivers.html.twig',
                array(

                ));
        }
    }

    public function ajaxAction()
    {

    $em = $this->getDoctrine()->getManager();
    $data = $em->getRepository('CABCourseBundle:Course')->getTestcompanylist();
    return new JsonResponse($data,200);
    }

    public function driversAction(Request $request, $_locale)
    {

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->container->get('security.token_storage')->getToken()->getUser()->getId();

        $repoCourse = $em->getRepository('CABCourseBundle:Course');
        $userRepo = $em->getRepository('CABUserBundle:User');
        $repoClean = $em->getRepository('CABCourseBundle:VehiculeClean');
        $repoFuel = $em->getRepository('CABCourseBundle:VehiculeFuel');
        $odm = $this->get('doctrine_mongodb')->getManager();

        $valueFrom = $request->query->get('date_start');
        $valueTo = $request->query->get('date_end');


        /** @var GeolocationRepository $geolocationRepository */
        $geolocationRepository = $odm->getRepository('CABCourseBundle:Geolocation');


        //$driverDopData = $geolocationRepository->getDataByDriversAndDAte($currentUser, $valueFrom, $valueTo);

        $list_race = $repoCourse->findCourseByDatesIntervalStats($user, $valueFrom, $valueTo);


        $count_race = $repoCourse->findStatsDriverRaceCount($user, $valueFrom, $valueTo);
        $count_day = $repoCourse->findStatsDriverDayWorkCount($user, $valueFrom, $valueTo);
        $count_clean = $repoClean->findStatsDriverCleanCount($user, $valueFrom, $valueTo);
        $count_fuel = $repoFuel->findStatsDriverFuelCount($user, $valueFrom, $valueTo);

        foreach ($list_race as $item) {

            $service = "";
            $aDriverId = $item->getDriver()->getId();
            $statusname = "";

            foreach ($item->getServiceTransportCourses() as $value) {
                $service = $value->getserviceName();
            }

            $statusCourse = $item->getCourseStatus();


            if ($statusCourse == 12) {
                $statusname = $this->get('translator')->trans(
                    "race accepted"
                ); #traduction dans message.fr.xliff
                $CourseId = $item->getId();
            } elseif ($statusCourse == 13) {
                $statusname = $this->get('translator')->trans("arrival");
                $CourseId = $item->getId();
            } elseif ($statusCourse == 3) {
                $statusname = $this->get('translator')->trans("available");
                $CourseId = $item->getId();
            } elseif ($statusCourse == 18) {
                $statusname = $this->get('translator')->trans("go to customer");
                $CourseId = $item->getId();
            } elseif ($statusCourse == 4) {
                $statusname = $this->get('translator')->trans("affect");
                $CourseId = $item->getId();
            } elseif ($statusCourse == 9) {
                $statusname = $this->get('translator')->trans("on race");
                $CourseId = $item->getId();
            } elseif ($statusCourse == 6) {
                $statusname = $this->get('translator')->trans("finish");
                $CourseId = $item->getId();
            }


            $tempsdenettoyage = $count_clean * 0.25;
            $tempsdefuel = $count_fuel * 0.25;

            /** @var Course $item */
            $aResult[] = array(
                'id' => $item->getId(),
                'statuscourse' => $statusCourse,
                'statusname' => $statusname,
                'service' => $service,
                'id_customer' => $item->getClient()->getId(),
                'name_customer' => $item->getClient()->getUsedName(),
                'started_address' => $item->getStartedAddress(),
                'arrival_address' => $item->getArrivalAddress(),
                'departure_date' => $item->getDepartureDate()->format('d/m/Y').' '.$item->getDepartureTime()
                        ->format('H:i:s'),
                'driver' => $item->getDriver()->getUsedName(),
                'hoursArrivalDriverTime' => $item->getHoursArrivalDriverTime(),
                'hoursArrivalDriverEcart' => $item->gethoursArrivalDriverEcart(),
                'HoursRaceOnRaceDriverTime' => $item->getHoursRaceOnRaceDriverTime(),
                'HoursRaceOnRaceDriverEcart' => $item->getHoursRaceOnRaceDriverEcart(),
                'HourFinishRace' => $item->getHoursFinishDriverTime(),
                'HoursFinishDriverEcart' => $item->getHoursFinishDriverEcart(),
                'tempsdenettoyage' => $tempsdenettoyage,
                'tempsdefuel' => $tempsdefuel,
                'tempsconduitejour' => $item->getHourDayWorking(),
                'tempsconduitenuit' => $item->getHourNightWorking(),
                'commentaire' => $item->getDescriptionChildren(),
                'race_created' => $item->getCreatedAt(),
                'count_race' => $count_race,
                'count_day' => $count_day,

            );


        }

        return new \Symfony\Component\HttpFoundation\JsonResponse($aResult);
    }


    public function exportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $error = null;

        $defaultData = array();
        $driversChoice = array();
        $raceChoice = array();

        $userManager = $this->get('cab.cab_user.manager');
        $odm = $this->get('doctrine_mongodb')->getManager();

        $drivers = $userManager->getUsersByRoleQuery('ROLE_DRIVER')->getQuery()->getArrayResult();
        $courseRepository = $em->getRepository('CABCourseBundle:Course');
        $races = $courseRepository->getCources()->getQuery()->getArrayResult();

        /** @var array $driver */
        foreach ($drivers as $driver) {
            $driversChoice[$driver['firstName'] . ' ' . $driver['lastName']] =$driver['id'] ;
        }

        foreach ($races as $race) {
            $raceChoice[Course::toString($race['id']) . ' ' ./* '#' . $race['client']['id'] .*/ ' ' .
            $race['client']['username'] . ' ' . $race['client']['firstName'] . ' ' . $race['client']['lastName']] = $race['id'];
        }

        $url = $this->generateUrl('statistics_get_courses');
	    $default['driver_choice'] = $driversChoice;
	    $default['course_choice'] = $raceChoice;
	    $default['course_path'] = $url;
        $form = $this->createFormBuilder($defaultData, array())
            ->add('report',ReportType::class, array(
                'label' => false,'data'=>$default
                )
            )
            ->setMethod('POST')
            ->getForm()
        ;

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
                $data = $data['report'];

                $dateStart = $data['dateIn'];
                $dateEnd = $data['dateOut'];
                if (null !== $dateStart && null !== $dateEnd) {
                    if ($dateEnd < $dateStart) {
                        throw new \RuntimeException('error_about_date_start_more_than_date_end_label');
                    }
                }

                if (null === $data['driver'] && null !== $data['race']) {
                    $drivers = $userManager->getRepository()->getDriver($data['driver'], $data['race'], $dateStart, $dateEnd)->getQuery()->getResult();
                    if (count($drivers) > 0) {
                        $data['driver'] = $drivers[0]->getId();
                    }
                } else {
                    $drivers = $userManager->getRepository()->getDriver($data['driver'], $data['race'], $dateStart, $dateEnd)->getQuery()->getResult();
                }

                if (count($drivers) < 1) {
                    throw new \RuntimeException('error_not_found_data_for_export_label');
                }

                $resultAvailability = array();
                $driverId = $data['driver'];
                // todo: check this solution on big table
				if (count($drivers[0]->getCourseDriver()) > 0) {
				    if (null !== $data['race']) {
				        $cources = $drivers[0]->getCourseDriver();
				        foreach ($cources as $cource) {
				            if ($data['race'] == $cource->getId()) {
                                $courseDriver = $cource;
                                $dateStart = $courseDriver->getDepartureDate();
                                /** @var \DateTime $dateEnd */
                                $dateEnd = $courseDriver->getDepartureDate();
                            }
                        }
                    } else {
                        $courseDriver = $drivers[0]->getCourseDriver()[0];
                        $dateStart = $courseDriver->getDepartureDate();
                        /** @var \DateTime $dateEnd */
                        $dateEnd = $courseDriver->getDepartureDate();
                    }
				}

//				  $dateStart = new \DateTime();
//                $dateStart->setDate(2016, 4, 30);
//                $dateEnd = new \DateTime();
//                $dateEnd->setDate(2016, 4, 30);

                $geolocationRepository = $odm->getRepository('CABCourseBundle:Geolocation');
                $driverData = $geolocationRepository->getDriverDataById($driverId, false, $dateStart, $dateEnd);
                $result = array();

                /* * @var Geolocation $item */
                /** @var array $item */
                foreach ($driverData as $item) {
                    if (!($date = date_create_from_format('Y-m-d H:i', $item['day']))) {
                        continue;
                    }

                    $result[$date->format('Y-m-d')][] = array('date' => $date, 'availibility' => $item['availibility'],
                        'driverName' => $driversChoice[$driverId], 'driverId' => $driverId);
                }

                foreach ($result as $key => $item) {
                    usort($item, function($a1, $a2) {
                        $v1 = $a1['date']->getTimestamp();
                        $v2 = $a2['date']->getTimestamp();
                        return $v1 - $v2;
                    });

                    $result[$key] = $item;
                }

                $interval = 5; // 5 minutes interval between geolocation data
                $resultAvailability = array();

                foreach ($result as $key => $item) {
                    $temp = array();
                    $lastPoint = null;
                    foreach ($item as $point) {
                       if (count($temp) < 1) {
                           if (!$point['availibility']) {
                               continue;
                           }

                           $temp[] = $point;
                           $lastPoint = $point;
                           continue;
                       }

                       if (!$point['availibility']) {
//                           $temp[] = $point;
                           $temp[] = $lastPoint;
                       }

                       if (($point['date']->getTimestamp() - $lastPoint['date']->getTimestamp()) > (60 * $interval)) {
                           $temp = array();
                           $temp[] = $point;
                           $lastPoint = $point;
                       } else {
                           $lastPoint = $point;
                       }

                       if (count($temp) === 2) {
                           $resultAvailability[] = $temp;
                           $temp = array();
                       }
                    }

                    if ((null !== $lastPoint) && count($temp) > 0) {
                        $temp[] = $lastPoint;
                        $resultAvailability[] = $temp;
                    }
                }

                $preparedData = array(
                    'drivers' => $drivers,
                    'availability' => $resultAvailability,
                );

                $twig = $this->container->get('twig');
                $html = $twig->render('CABAdminBundle:Stats:_export_table.html.twig', $preparedData);
                $css = $twig->render('CABAdminBundle:Stats:_base_export_table.html.twig');

                $pdfService = $this->get('tfox.mpdfport');
                $serviceExport = $this->get('cab.main.export_report_service');

                $service = $serviceExport->createPdfService($pdfService, $html, $css);
                $filename = 'report.pdf';
                $service->Output($filename, 'I');

                die;
            }
        }
        catch(\RuntimeException $ex)
        {
            $error = $ex->getMessage();
        }
        catch(\Exception $ex)
        {
            $error = 'fatal_error_label';
            $error = $ex->getMessage();
        }

        return $this->render('CABAdminBundle:Stats:export.html.twig', array(
            'form' => $form->createView(),
            'error' => $error
        ));
    }

    private function getMax( $array )
    {
        $max = 0;
        foreach( $array as $k => $v ) {
            $max = max( array( $max, $v['date']->getTimestamp() ) );
        }
        return $max;
    }

    public function getCoursesAction(Request $request)
    {
        $data = $request->query->all();

        $serviceExport = $this->get('cab.main.export_report_service');
        $raceChoice = $serviceExport->getCourses($data);

        return new JsonResponse($raceChoice);
    }
}