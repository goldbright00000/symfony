<?php

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CAB\AdminBundle\Controller;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\ServiceTransport;
use Doctrine\MongoDB\Cursor;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController as BaseController;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse ;
use Sonata\AdminBundle\Action\DashboardAction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class CoreController extends BaseController {

    /**
     * @return Response
     */
    public function dashboardAction() {
        $blocks = array(
            'top' => array(),
            'left' => array(),
            'center' => array(),
            'right' => array(),
            'bottom' => array(),
        );

        foreach ($this->container->getParameter('sonata.admin.configuration.dashboard_blocks') as $block) {
            $blocks[$block['position']][] = $block;
        }

        $em = $this->getDoctrine()->getManager();
        $finalResult = ['data' => []];

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $countDrivers = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_DRIVER'));
            $countCustomer = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_CUSTOMER'));
            $countVehicle = $em->getRepository('CABCourseBundle:Vehicule')->getTotalvehicle();
            $sumca = $em->getRepository('CABCourseBundle:Course')->getTotalCaToday();

            //$countRaceToday = $em->getRepository('CABCourseBundle:Course')->getTotalCaToday();


            return $this->render($this->getAdminPool()->getTemplate('dashboard'), array(
                'base_template' => $this->getBaseTemplate(),
                'admin_pool' => $this->container->get('sonata.admin.pool'),
                'blocks' => $blocks,
                'countdriver' => $countDrivers,
                'countcustomer' => $countCustomer,
                'countvehicule' => $countVehicle,
                'sumca' => $sumca,
            ));
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINCOMPANY')) {

            // [New 3.0] Get our "authorization_checker" Object
            $auth_checker = $this->get('security.authorization_checker');
            # e.g: $auth_checker->isGranted('ROLE_ADMIN');

            // Get our Token (representing the currently logged in user)
            // [New 3.0] Get the `token_storage` object (instead of calling upon `security.context`)
            $token = $this->get('security.token_storage')->getToken();
            # e.g: $token->getUser();
            # e.g: $token->isAuthenticated();
            # [Careful]            ^ "Anonymous users are technically authenticated"

            // Get our user from that token
            $user = $token->getUser();


            # e.g (w/ FOSUserBundle): $user->getEmail(); $user->isSuperAdmin(); $user->hasRole();

            // [New 3.0] Check for Roles on the $auth_checker
            $isRoleAdmin = $auth_checker->isGranted('ROLE_ADMIN');
            // e.g: (bool) true/false



            $dm = $this->get('doctrine_mongodb')->getManager();
            $rep = $this->get('doctrine_mongodb')
                ->getRepository('CABCourseBundle:Geolocation');


            $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser(), 'isBusiness' => '0']);
            $companyId = $company->getId();
            $countRaceToday = $em->getRepository('CABCourseBundle:Course')->getTotalRaceTodayByCompany($companyId);
            $countDrivers = $em->getRepository('CABCourseBundle:Course')->getDriverTodayByCompany($companyId);
           // $aDrivers = $company->getUsers();
           //s $aDrivers = $aDrivers->toArray();
            /** @var Cursor $geolocationDrivers */
            //$geolocationDrivers = $rep->findGeolocationByDriver($aDrivers);

            $connectedDrivers = [];

            $countCustomer = $em->getRepository('CABCourseBundle:Course')->getCustomerTodayByCompagny($companyId);
            $countVehicle = 0;
            $sumca = $em->getRepository('CABCourseBundle:Course')->getTotalCaTodayByCompany($companyId);

            return $this->render($this->getAdminPool()->getTemplate('dashboard'), array(
                'base_template' => $this->getBaseTemplate(),
                'admin_pool' => $this->container->get('sonata.admin.pool'),
                'blocks' => $blocks,
                'countdriver' => $countDrivers,
                'countcustomer' => $countCustomer,
                'countvehicule' => $countVehicle,
                'sumca' => $sumca,
            ));
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_AGENT')) {
            $dm = $this->get('doctrine_mongodb')->getManager();
            $rep = $this->get('doctrine_mongodb')
                ->getRepository('CABCourseBundle:Geolocation');


            $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(
                ['contact' => $this->getUser(), 'isBusiness' => '0']
            );

            // [New 3.0] Get our "authorization_checker" Object
            $auth_checker = $this->get('security.authorization_checker');
            # e.g: $auth_checker->isGranted('ROLE_ADMIN');

            // Get our Token (representing the currently logged in user)
            // [New 3.0] Get the `token_storage` object (instead of calling upon `security.context`)
            $token = $this->get('security.token_storage')->getToken();
            # e.g: $token->getUser();
            # e.g: $token->isAuthenticated();
            # [Careful]            ^ "Anonymous users are technically authenticated"

            // Get our user from that token
            $user = $token->getUser();
            # e.g (w/ FOSUserBundle): $user->getEmail(); $user->isSuperAdmin(); $user->hasRole();

        }


        return $this->render($this->getAdminPool()->getTemplate('dashboard'), array(
            'base_template' => $this->getBaseTemplate(),
            'admin_pool' => $this->container->get('sonata.admin.pool'),
            'blocks' => $blocks,
        ));
    }

    /**
     * get geolocation of driver
     *
     * @throws \Exception
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getlocationAction() {



        $em = $this->getDoctrine()->getManager();
        $finalResult = ['data' => []];
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $aDrivers = $this->get('cab.cab_user.manager')->getUsersByRole('ROLE_DRIVER');
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINCOMPANY')) {
            $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser(), 'isBusiness' => 0]);
            $aDrivers = $company->getUsers();
            $aDrivers = $aDrivers->toArray();
        }



        foreach ($aDrivers as $driver) {
            $aDriversId[] = $driver->getId();
        }

        $companyDrivers = $this->get('cab.geolocation.manager')->getGeolocationsByDrivers($aDriversId);


        $existDriver = array();
        foreach ($companyDrivers as $oGeolocation) {
            $idDriver = $oGeolocation->getDriver();
            $geolocationHandler = $this->get('cab_course.handler.geolocation_handler');
            $driverLatLong = $oGeolocation->getLatitude() . ',' . $oGeolocation->getLongitude();
            $driverLat = $oGeolocation->getLatitude() ;
            $driverLong = $oGeolocation->getLongitude() ;
            $addressDriver = $geolocationHandler->getAddressByDriver($driverLat,$driverLong);




            if (!in_array($idDriver, $existDriver)) {
                try {
                    // add criteria by time today 11:28
                    $oDriver = $this->get('cab.cab_user.manager')->loadUser($idDriver);

                    // check if the driver is available: if count course is greater than 0
                    $runingCourse = $this->get('cab.course_manager')->getCourseByDateArrival($oDriver);

                    $statusCourse = null ;
                    $CourseId = null;
                    $statusname = null;
                    $errorAvailibility = 0;
                    $delay = 0;




                    $dateGeolocate = $oGeolocation->getDateGeolocate() instanceof \MongoDate ? date('Y-m-d H:i:s',
                        $oGeolocation->getDateGeolocate()->sec) : $oGeolocation->getDateGeolocate();
                    $finalResult['data'][] = [
                        'driver' => is_object($oDriver) ? $oDriver->getUsername() : '',
                        'driverphone' => $oDriver->getPhoneMobile(),
                        'accuracy' => $oGeolocation->getAccuracy(),
                        'latitude' => $oGeolocation->getLatitude(),
                        'longitude' => $oGeolocation->getLongitude(),
                        'heading' => $oGeolocation->getHeading(),
                        'dateGeolocate' => $dateGeolocate,
                        'error_availibility' => $errorAvailibility,
                        'status' => $statusCourse,
                        'statusname' => $statusname,
                        'courseid' => $CourseId,
                        'delay' => $delay,
                        'road'=> $addressDriver['road'],
                        'postcode' => $addressDriver['postcode'],
                        'village' => $addressDriver['village'],
                        'country' => $addressDriver['country'],
                    ];
                } catch (\Exception $e) {
                    //throw new Exception($e->getMessage());
                }
                $existDriver[] = $idDriver;
            }

        }

        return new \Symfony\Component\HttpFoundation\JsonResponse($finalResult);
    }

    public function getlistraceAction() {

        $errorconnect = '';

        $em = $this->getDoctrine()->getManager();
        $finalResult = ['data' => []];
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $aDrivers = $this->get('cab.cab_user.manager')->getUsersByRole('ROLE_DRIVER');
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINCOMPANY')) {
            $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser(), 'isBusiness' => 0]);
            $companyId= $company->getId();
        }


		if(isset($companyId) && $companyId != '')
		{
			$runingCourse = $this->get('cab.course_manager')->getCourseByCompagny($companyId);


			$dateNow = new \DateTime();
			foreach($runingCourse as $item)
			{

				$service   = "";
				$aDriverId = $item->getDriver()->getId();


				$nextCourse = $this->get('cab.course_manager')->findNextRaceByDriver($aDriverId);


				foreach($item->getServiceTransportCourses() as $value)
				{
					$service = $value->getserviceName();
				}

				//Get position the driver Id
				$companyDrivers = $this->get('cab.geolocation.manager')->getGeolocationsByDriver($aDriverId);

				if($item->getIsGroupage() == true)
				{

					$groupage      = 1;
					$groupage_base = 0;

					if(!empty($item->getbaseGroupage()))
					{
						$groupage_base = $item->getbaseGroupage()->getbaseCourse()->getid();
					}


				}
				else
				{
					$groupage      = 0;
					$groupage_base = 0;
				}


				foreach($nextCourse as $race)
				{
					$idRace      = $race->getId();
					$raceLatLong = $race->getlatDep().','.$race->getlongDep();
					$racedate    = $item->getDepartureDate();


					//@TODO : Make the timezone by company
					$timeZone   = date_default_timezone_set('Europe/Paris');
					$raceStatus = $race->getCourseStatus();
					//$raceDatedeparture = new \DateTime($race->getDepartureDate());
					$raceTimeDeparture = $race->getDepartureTime();
					//$raceDateTimedeparture = new DateTime($raceDatedeparture->format('Y-m-d') .' ' .$raceTimeDeparture->format('H:i:s'));

					$raceDateTimeDeparture = new \DateTime($racedate->format('Y-m-d').' '.$raceTimeDeparture->format('H:i'));
					$raceDateTimeDeparture = $raceDateTimeDeparture->format('Y-m-d H:i');
					$raceDateTimeDeparture = date_create_from_format('Y-m-d H:i', $raceDateTimeDeparture);


					$currentdate      = new \DateTime('now');
					$currentdateStamp = $currentdate->getTimestamp();

					$searchDateTime = new \DateTime('now');
					$searchDateTime->modify("+30 minutes");
					$searchDateTimeStamp        = $searchDateTime->getTimestamp();
					$raceDateTimeDepartureStamp = $raceDateTimeDeparture->getTimestamp();

					foreach($companyDrivers as $oGeolocation)
					{
						$idDriver      = $oGeolocation->getDriver();
						$driverLatLong = $oGeolocation->getLatitude().','.$oGeolocation->getLongitude();
					}


					if(isset($driverLatLong) && $raceDateTimeDepartureStamp >= $currentdateStamp && $raceDateTimeDepartureStamp <= $searchDateTimeStamp && $raceStatus == 4)
					{


						$geolocationHandler = $this->get('cab_course.handler.geolocation_handler');
						$distanceDuration   = $geolocationHandler->getDistanceMatrixDriver($driverLatLong, $raceLatLong, $raceDateTimeDepartureStamp);

						$distance            = $distanceDuration['matrixdistance'];
						$approcheTime        = $distanceDuration['matrixtime'];
						$approcheTimeTraffic = $distanceDuration['matrixtimetraffic'];


						$calculArrivalDriver = new \DateTime('now');
						$calculArrivalDriver->modify("+".$approcheTimeTraffic." minutes");


						if($raceDateTimeDeparture > $calculArrivalDriver)
						{
							$dailydriver = '0';
							$delay_time  = '0';
						}
						else
						{
							$dailydriver = $calculArrivalDriver->format('H:i');
							$delay_time  = $raceDateTimeDeparture->diff($calculArrivalDriver);
							$delay_time  = $delay_time->i.' minutes';
						}


						if($distance != 0)
						{
							$distanceDuration = array($distance, $approcheTime);
						}
						else
						{
							$distanceDuration = array('-', '-');
						}

						$statusCourse = $item->getCourseStatus();


						if($statusCourse == 12)
						{
							$statusname = $this->get('translator')->trans("race accepted"); #traduction dans message.fr.xliff
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 13)
						{
							$statusname = $this->get('translator')->trans("arrival");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 3)
						{
							$statusname = $this->get('translator')->trans("available");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 18)
						{
							$statusname = $this->get('translator')->trans("go to customer");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 4)
						{
							$statusname = $this->get('translator')->trans("affect");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 9)
						{
							$statusname = $this->get('translator')->trans("on race");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 6)
						{
							$statusname = $this->get('translator')->trans("finish");
							$CourseId   = $item->getId();
						}

						if(is_null($item->getcommandSncf()))
						{
							$numbercommand = '';
						}
						else
						{
							$numbercommand = $item->getcommandSncf()->getcommandeCourse();
						}

						/** @var Course $item */
						$aResult[] = array('id' => $item->getId(), 'numbercommand' => $numbercommand, 'statuscourse' => $statusCourse, 'statusname' => $statusname, 'service' => $service, 'id_customer' => $item->getClient()->getId(), 'name_customer' => $item->getClient()->getUsedName(), 'started_address' => $item->getStartedAddress(), 'arrival_address' => $item->getArrivalAddress(), 'departure_date' => $item->getDepartureDate()->format('d/m/Y').' '.$item->getDepartureTime()->format('H:i:s'), 'driver' => $item->getDriver()->getUsedName().' '.$item->getDriver()->getPhoneMobile(), 'distance' => $distanceDuration[0], 'duration' => $distanceDuration[1], 'error_connect' => '$errorconnect', 'daily_driver' => $dailydriver, 'daily_time' => $delay_time, 'groupage' => $groupage, 'groupage_base' => $groupage_base);


					}
					else
					{

						$distanceDuration = array('-', '-');
						$statusCourse     = $item->getCourseStatus();


						if($statusCourse == 12)
						{
							$statusname = $this->get('translator')->trans("race accepted"); #traduction dans message.fr.xliff
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 13)
						{
							$statusname = $this->get('translator')->trans("arrival");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 3)
						{
							$statusname = $this->get('translator')->trans("available");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 18)
						{
							$statusname = $this->get('translator')->trans("go to customer");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 4)
						{
							$statusname = $this->get('translator')->trans("affect");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 9)
						{
							$statusname = $this->get('translator')->trans("on race");
							$CourseId   = $item->getId();
						}
						elseif($statusCourse == 6)
						{
							$statusname = $this->get('translator')->trans("finish");
							$CourseId   = $item->getId();
						}

						if(is_null($item->getcommandSncf()))
						{
							$numbercommand = '';
						}
						else
						{
							$numbercommand = $item->getcommandSncf()->getcommandeCourse();
						}

						/** @var Course $item */
						$aResult[] = array('id' => $item->getId(), 'numbercommand' => $numbercommand, 'statuscourse' => $statusCourse, 'statusname' => $statusname, 'service' => $service, 'id_customer' => $item->getClient()->getId(), 'name_customer' => $item->getClient()->getUsedName(), 'started_address' => $item->getStartedAddress(), 'arrival_address' => $item->getArrivalAddress(), 'departure_date' => $item->getDepartureDate()->format('d/m/Y').' '.$item->getDepartureTime()->format('H:i:s'), 'driver' => $item->getDriver()->getUsedName().' '.$item->getDriver()->getPhoneMobile(), 'distance' => $distanceDuration[0], 'duration' => $distanceDuration[1], 'error_connect' => '$errorconnect', 'groupage' => $groupage, 'groupage_base' => $groupage_base);

					}

				}

			}

			return new \Symfony\Component\HttpFoundation\JsonResponse($aResult);
		}
	    return new \Symfony\Component\HttpFoundation\JsonResponse(array());
    }



    public function DataTodayAction() {
        $em = $this->getDoctrine()->getManager();
        $finalResult = ['data' => []];

        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            //$countDrivers = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_DRIVER'));
            //$countCustomer = count($this->get('cab.cab_user.manager')->getUsersByRole('ROLE_CUSTOMER'));
            $countDrivers = $em->getRepository('CABCourseBundle:Course')->getDriverToday();
            $countCustomer = $em->getRepository('CABCourseBundle:Course')->getCustomerToday();
            $countVehicle = $em->getRepository('CABCourseBundle:Vehicule')->getTotalvehicle();
            $sumca = $em->getRepository('CABCourseBundle:Course')->getTotalCaToday();
            $countrace = $em->getRepository('CABCourseBundle:Course')->getTotalRaceToday();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINCOMPANY')) {
            $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser()]);
            $companyId = $company->getId();
            $countrace = $em->getRepository('CABCourseBundle:Course')->getTotalRaceTodayByCompany($companyId);
            $countDrivers = $em->getRepository('CABCourseBundle:Course')->getDriverTodayByCompany($companyId);
            $aDrivers = $company->getUsers();
            $aDrivers = $aDrivers->toArray();
            $countCustomer = $em->getRepository('CABCourseBundle:Course')->getCustomerTodayByCompagny($companyId);
            $countVehicle = 0;
            $sumca = $em->getRepository('CABCourseBundle:Course')->getTotalCaTodayByCompany($companyId);
        }

        $finalResult['data'] = [
            'countcustomer' => $countCustomer,
            'countdriver' => $countDrivers,
            'countvehicle' => $countVehicle,
            'sumca' => $sumca,
            'countrace' => $countrace,
        ];

        return new \Symfony\Component\HttpFoundation\JsonResponse($finalResult);
    }

    public function getdriverbyzoneAction(Request $request) {

        $getJsonData = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser(), 'isBusiness' => 0]);
        $companyId= $company->getId();

        $zone_id = $getJsonData['id'];
        $em = $this->get('doctrine.orm.entity_manager');
        if ($zone_id ==""){
            $listeDriversByZone= $em->getRepository('CABUserBundle:User')->getDriverByCompanyByZoneCallback($companyId);
        }else{
            $listeDriversByZone= $em->getRepository('CABUserBundle:User')->getDriverByCompanyByZone($companyId,$zone_id);

        }
        foreach ($listeDriversByZone as $value){
            $output[]=array('id'=>$value->getId(),'firstname'=>$value->getFirstName(), 'lastname'=>$value->getLastName());
        }
        return new JsonResponse($output);
    }

    public function getpricebyzonesncfAction(Request $request)
    {

        $getJsonData = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(
            ['contact' => $this->getUser(), 'isBusiness' => 0]
        );
        $companyId = $company->getId();

        $zone_id = $getJsonData['zone_id'];
        $departure = $getJsonData['departure'];
        $arrival = $getJsonData['arrival'];
        $departureTime = $getJsonData['departureTime'];
        $departureDate = $getJsonData['departureDate'];
        $em = $this->get('doctrine.orm.entity_manager');


        $format_departure_date = date('l', strtotime($departureDate));

        if($format_departure_date == 'Sunday')
        {
            $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneSNCF(
                $zone_id,
                $departure,
                $arrival
            );

            foreach ($PriceByZone as $value) {

                $price_night_ht = $value->getPricenight();
                $price_night_ttc = round($value->getPricenight() * 1.1, 2);
                $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                $hour_night_end = date_format($value->getHournightend(), 'H:i');

                $output[] = array(
                    'price_ht' => $price_night_ht,
                    'price_ttc' => $price_night_ttc,
                    'hour_night_start' => $hour_night_start,
                    'hour_night_end' => $hour_night_end,
                    'price_km_day' => $value->getPricekmday(),
                    'price_km_night' => $value->getPricekmnight(),
                    'info' => $value->getInformation(),
                    'nuit'=> 1,
                    'dimanche'=> 1,
                    'error' => 1
                );
            }
            if (isset($output)) {
                return new JsonResponse($output);
            } else {

                $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneInfoSNCF(
                    $zone_id
                );

                foreach ($PriceByZone as $value) {

                    $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                    $hour_night_end = date_format($value->getHournightend(), 'H:i');

                    $output[] = array(
                        'information' => $value->getInformation(),
                        'hour_night_start' => $hour_night_start,
                        'hour_night_end' => $hour_night_end,
                        'price_km_day' => $value->getPricekmday(),
                        'price_km_night' => $value->getPricekmnight(),
                        'info' => $value->getInformation(),
                        'nuit'=>1,
                        'error' => 0
                    );


                }
                return new JsonResponse($output);

            }

        }

        /*RECHERCHE DES HORAIRES DE NUIT PAR ZONE */
        $gethoursnight = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneInfoSNCF(
            $zone_id);

        foreach ($gethoursnight as $value) {
            $hour_night_start = date_format($value->getHournightstart(), 'H:i');
            $hour_night_end = date_format($value->getHournightend(), 'H:i');
            $format_date_night_start = date_format($value->getHournightstart(), 'd.m.Y H:i');
            $format_date_night_end = date_format($value->getHournightend(), 'd.m.Y H:i');
        }

        $minuit = $time = date('H:i', strtotime('00:00'));
        $night_start = date('H:i', strtotime($hour_night_start));
        $night_end = date('d.m.Y H:i', strtotime($hour_night_end.'+1 day'));
        $start_race = date('H:i', strtotime($departureTime));
        $format_date_minuit = date('d.m.Y H:i',strtotime('00:00'));
        $format_date_start= date('d.m.Y H:i', strtotime($departureTime));

        if($format_date_start >= $format_date_night_start && $format_date_start <= $night_end){
            //dump('la nuit A');
            //exit;
            $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneSNCF(
                $zone_id,
                $departure,
                $arrival
            );
            foreach ($PriceByZone as $value) {

                $price_night_ht = $value->getPricenight();
                $price_night_ttc = round($value->getPricenight() * 1.1, 2);
                $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                $hour_night_end = date_format($value->getHournightend(), 'H:i');

                $output[] = array(
                    'price_ht' => $price_night_ht,
                    'price_ttc' => $price_night_ttc,
                    'hour_night_start' => $hour_night_start,
                    'hour_night_end' => $hour_night_end,
                    'price_km_day' => $value->getPricekmday(),
                    'price_km_night' => $value->getPricekmnight(),
                    'info' => $value->getInformation(),
                    'nuit'=> 1,
                    'dimanche' => 0,
                    'error' => 1
                );
            }
            if (isset($output)) {
                return new JsonResponse($output);
            } else {

                $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneInfoSNCF(
                    $zone_id
                );
                foreach ($PriceByZone as $value) {

                    $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                    $hour_night_end = date_format($value->getHournightend(), 'H:i');

                    $output[] = array(
                        'information' => $value->getInformation(),
                        'hour_night_start' => $hour_night_start,
                        'hour_night_end' => $hour_night_end,
                        'price_km_day' => $value->getPricekmday(),
                        'price_km_night' => $value->getPricekmnight(),
                        'info' => $value->getInformation(),
                        'nuit'=>1,
                        'dimanche'=> 0,
                        'error' => 0
                    );


                }
                return new JsonResponse($output);

            }
        }elseif ($start_race<=$hour_night_end){
           // dump('la nuit B');
           // exit;
            $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneSNCF(
                $zone_id,
                $departure,
                $arrival
            );
            foreach ($PriceByZone as $value) {
                $price_night_ht = $value->getPricenight();
                $price_night_ttc = round($value->getPricenight() * 1.1, 2);
                $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                $hour_night_end = date_format($value->getHournightend(), 'H:i');

                $output[] = array(
                    'price_ht' => $price_night_ht,
                    'price_ttc' => $price_night_ttc,
                    'hour_night_start' => $hour_night_start,
                    'hour_night_end' => $hour_night_end,
                    'price_km_day' => $value->getPricekmday(),
                    'price_km_night' => $value->getPricekmnight(),
                    'info' => $value->getInformation(),
                    'nuit'=>1,
                    'dimanche' => 0,
                    'error' => 1
                );
            }
            if (isset($output)) {
                return new JsonResponse($output);
            } else {

                $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneInfoSNCF(
                    $zone_id
                );
                foreach ($PriceByZone as $value) {

                    $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                    $hour_night_end = date_format($value->getHournightend(), 'H:i');

                    $output[] = array(
                        'information' => $value->getInformation(),
                        'hour_night_start' => $hour_night_start,
                        'hour_night_end' => $hour_night_end,
                        'price_km_day' => $value->getPricekmday(),
                        'price_km_night' => $value->getPricekmnight(),
                        'info' => $value->getInformation(),
                        'nuit' => 1,
                        'dimanche' => 0,
                        'error' => 0
                    );
                }
                return new JsonResponse($output);

            }
        }else{
            $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneSNCF(
                $zone_id,
                $departure,
                $arrival
            );
            foreach ($PriceByZone as $value) {

                $price_day_ht = $value->getPriceday();
                $price_day_ttc = round($value->getPriceday() * 1.1, 2);
                $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                $hour_night_end = date_format($value->getHournightend(), 'H:i');

                $output[] = array(
                    'price_ht' => $price_day_ht,
                    'price_ttc' => $price_day_ttc,
                    'hour_night_start' => $hour_night_start,
                    'hour_night_end' => $hour_night_end,
                    'price_km_day' => $value->getPricekmday(),
                    'price_km_night' => $value->getPricekmnight(),
                    'info' => $value->getInformation(),
                    'error' => 1
                );
            }
        }
        if (isset($output)) {
            return new JsonResponse($output);
        } else {

            $PriceByZone = $em->getRepository('CABArticleBundle:PriceByZone')->getPriceByZoneInfoSNCF(
                $zone_id
            );
            foreach ($PriceByZone as $value) {
                $hour_night_start = date_format($value->getHournightstart(), 'H:i');
                $hour_night_end = date_format($value->getHournightend(), 'H:i');

                $output[] = array(
                    'information' => $value->getInformation(),
                    'hour_night_start' => $hour_night_start,
                    'hour_night_end' => $hour_night_end,
                    'price_km_day' => $value->getPricekmday(),
                    'price_km_night' => $value->getPricekmnight(),
                    'info' => $value->getInformation(),
                    'error' => 0
                );
            }
            return new JsonResponse($output);
        }
    }



    public function getlistbupoAction(Request $request) {

        $getJsonData = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser(), 'isBusiness' => 0]);
        $companyId= $company->getId();
        $bupo_id = $getJsonData['id'];
        $em = $this->get('doctrine.orm.entity_manager');
        if ($bupo_id ==""){
            $listeSearchByBupo= $em->getRepository('CABCourseBundle:Servicetransport')->getServicesTransportByBupoEmpty($companyId);
        }else{
            $listeSearchByBupo= $em->getRepository('CABCourseBundle:Servicetransport')->getServicesTransportByBupo($companyId,$bupo_id);
        }
        foreach ($listeSearchByBupo as $value){
            $output[]=array('id'=>$value->getId(),'nameservice'=>$value->getserviceName());
        }
        return new JsonResponse($output);
    }

    public function getrefbcAction(Request $request) {

        $getJsonData = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $company = $em->getRepository('CABCourseBundle:Company')->findOneBy(['contact' => $this->getUser(), 'isBusiness' => 0]);
        $companyId= $company->getId();
        $ref_bc = $getJsonData['id'];
        $em = $this->get('doctrine.orm.entity_manager');
        if ($ref_bc ==""){
            $listeSearchByRefbc = null;
        }else{
            $listeSearchByRefbc = $em->getRepository('CABCourseBundle:Commandsncf')->getRefByCommand($ref_bc);
        }

        return new JsonResponse($listeSearchByRefbc);
    }


}
