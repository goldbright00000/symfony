<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 28/07/2015
 * Time: 23:52
 */

namespace CAB\AdminBundle\Controller;

use Sonata\AdminBundle\Admin\BreadcrumbsBuilder;
use CAB\CourseBundle\Form\GroupageType;
use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Entity\Groupage;
use CAB\CourseBundle\Entity\GroupageRepository;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Class CRUDController
 *
 * @package CAB\AdminBundle\Controller
 */
class CRUDController extends Controller
{
    public function batchActionSendMail(ProxyQueryInterface $query)
    {

        try {
            $contacts = $query->execute();

            $this->sendEmail($contacts);

            $this->addFlash('sonata_flash_success', 'Les emails ont bien été envoyés');
        } catch (ModelManagerException $e) {
            $this->handleModelManagerException($e);
            $this->addFlash('sonata_flash_error', 'Une erreur est survenue');
        }

        return new RedirectResponse($this->admin->generateUrl(
            'list',
            ['filter' => $this->admin->getFilterParameters()]
        ));
    }

    public function sendEmail($contacts)
    {
        $mails = [];

        foreach($contacts as $contact) {
            $mails[] = $contact->getEmail();
        }

        $mailer = $this->container->get('mailer');
        $message = new \Swift_Message('votre compte');

	    $message->setFrom('no-reply@up.taxi')
            ->setTo($mails)
            ->setBcc('info@up.taxi');
        $message->setBody(
            $this->renderView('CABMainBundle:Mail:infocustomer.html.twig', array('contact'=>$contact)),
            'text/html'
        );
        $ret = $mailer->send($message);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function calendarAction()
    {
        $request = $this->getRequest();
        $userId = $request->get('id');
        $serviceUser = $this->get('cab.cab_user.manager');
        $oUser = $serviceUser->loadUser($userId);
        $servicePlaning = $this->get('cab.planing.manager');
        //$result = $servicePlaning->getArrayPlaning($oUser);

        return $this->render('CABAdminBundle:CRUD:calendar.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function groupageAction()
    {
        $request = $this->getRequest();
        $courseId = $request->get('id');
        $courseManager = $this->get('cab.course_manager');
        $groupageManager = $this->get('cab_course.manager.groupage_manager');
        $oCourse = $courseManager->loadCourse($courseId);
        $departureDate = $oCourse->getDepartureDate()->format('Y-m-d') . ' ' . $oCourse->getDepartureTime()->format('H:i');
        $oDateCourse = \DateTime::createFromFormat('Y-m-d H:i', $departureDate);

        $result = $courseManager->getCoursesForGroupage($oCourse->getLongDep(),$oCourse->getLongArr(), $oDateCourse, $oCourse->getCompany()->getId());
        if (count($result)) {
            $groupage = $groupageManager->findByBaseCourse($courseId);
            if (!$groupage) {
                $groupage = new Groupage();
            }
            //$groupage->setBaseCourse($oCourse);
            foreach ($result as $item) {

                $groupage->addLinkedCourse($item);
            }

            $form = $this->createForm(GroupageType::class, $groupage, array(
                    'method' => 'post',
                    'course' => $oCourse,
                    'action' => $this->generateUrl('sonata_course_groupage', array('id' => $oCourse->getId())),
                )
            );


            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                // $form->getData() holds the submitted values
                // but, the original `$task` variable has also been updated
                /** @var Groupage $grouppage */
                $grouppage = $form->getData();


                $em = $this->getDoctrine()->getManager();
                $em->persist($grouppage);
                $em->persist($grouppage);
                $em->flush();

                $iteratorLinked = $grouppage->getLinkedCourses()->getIterator();
                while ($iteratorLinked->valid()) {
                    /** @var Course $linkedCourse */
                    $linkedCourse = $iteratorLinked->current();

                    $linkedCourse->setLinkedGroupage($grouppage);
                    $linkedCourse->setIsGroupage(true);
                    $courseManager->save($linkedCourse);
                    $iteratorLinked->next();
                }

                return $this->redirectToRoute('sonata_course_list');
            }

            return $this->render('CABAdminBundle:CRUD:groupage.html.twig', array(
                'form' => $form->createView(),
            ));

            /*

            $em->persist($groupage);
            $em->flush();*/

        }else{
            var_dump('toto'); exit;
        }


        return $this->render('CABAdminBundle:CRUD:groupage.html.twig');
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articleAction()
    {
        return $this->render('CABAdminBundle:CRUD:article.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fuelAction()
    {
        $title = "How it works ?";
        return $this->render('CABAdminBundle:CRUD:vehiculefuel.html.twig', array('title' => $title));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function acceptCourseAction()
    {
        $request = $this->get('request');
        $courseId = $request->get('id');
        $sCourse = $this->get('cab.course_manager');
        $oCourse = $sCourse->loadCourse($courseId);
        $fosThreadManager = $this->get('fos_message.thread_manager');
        $collectionThread = $oCourse->getCourseThread();
        $aThreads = $collectionThread->getValues();
        foreach ($aThreads as $thread) {
            $fosThreadManager->deleteThread($thread);
        }

        if (in_array($oCourse->getCourseStatus(), array(Course::STATUS_AFFECTED, Course::STATUS_DONE, Course::STATUS_CANCELLED_BY_CLIENT, Course::STATUS_CANCELLED_BY_ADMIN))) {
            $url = $this->get('router')->generate('sonata_admin_dashboard');
            $this->addFlash(
                'sonata_flash_notice',
                $this->get('translator')->trans(
                    'The course has been accepted by another driver',
                    array(),
                    'SonataAdminBundle'
                )
            );

            return new RedirectResponse($url);
        }

        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $this->get('cab_course.handler.planing_handler')->handlerEvent($currentUser, $oCourse, 'Accepted by driver');
        $sCourse->setAttr($oCourse, 'driver', $currentUser);
        $sCourse->setAttr($oCourse, 'courseStatus', Course::STATUS_AFFECTED);
        $sCourse->saveCourse($oCourse);
        return $this->render('CABAdminBundle:CRUD:accept_course.html.twig');

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function archiveAction()
    {
        return $this->render('CABAdminBundle:CRUD:archive.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlepriceAction()
    {
        return $this->render('CABAdminBundle:CRUD:archive.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function PricebyzoneAction()
    {
        return $this->render('CABAdminBundle:CRUD:archive.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callcAction()
    {
        return $this->render('CABAdminBundle:CRUD:callc.html.twig');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tarifAction()
    {
        $object = $this->admin->getSubject();
        if (!$object)
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', 'id'));


        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }


        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->getRestMethod() == 'POST') {
            $form->submit($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($form->isSubmitted() && $isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {

                try {
                    $object = $this->admin->update($object);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result'    => 'ok',
                            'objectId'  => $this->admin->getNormalizedIdentifier($object)
                        ));
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->get('translator')->trans(
                            'flash_edit_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    // redirect to edit mode
                    return $this->redirectTo($object);

                } catch (ModelManagerException $e) {
                    $this->logModelManagerException($e);

                    $isFormValid = false;
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->get('translator')->trans(
                            'flash_edit_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render('CABAdminBundle:CRUD:tarif.html.twig', array(
            'action' => 'create',
            'base_template' => 'CABAdminBundle::layout.html.twig',
            'admin_pool'      => $this->get('sonata.admin.pool'),
            'form'   => $view,
            'object' => $object,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    /**
     * @param $e
     */
    private function logModelManagerException($e)
    {
        $context = array('exception' => $e);
        if ($e->getPrevious()) {
            $context['previous_exception_message'] = $e->getPrevious()->getMessage();
        }
        $this->getLogger()->error($e->getMessage(), $context);
    }

    /**
     * Execute a batch delete.
     *
     * @param ProxyQueryInterface $query
     *
     * @return RedirectResponse
     *
     * @throws AccessDeniedException If access is not granted
     */
    public function batchActionDelete(ProxyQueryInterface $query)
    {
        if (false === $this->admin->isGranted('DELETE')) {
            throw new AccessDeniedException();
        }
        $courseManager = $this->get('cab.course_manager');
        $modelManager = $this->admin->getModelManager();
        try {
            /** @var \Doctrine\ORM\Query $resultQuery */
            $resultQuery = $query->getQuery();

            /** @var \Doctrine\ORM\Query\Parameter $object */
            foreach ($resultQuery->getResult() as $pos => $object) {
                if ($object instanceof Course) {
                    if ($object->getIsCycle()) {
                        //$this->changeParentCycle($courseManager, $object);
                    }
                }
            }
            $modelManager->batchDelete($this->admin->getClass(), $query);

            $this->addFlash('sonata_flash_success', 'flash_batch_delete_success');
        } catch (ModelManagerException $e) {
            //dump($e->getMessage());exit;
            $this->logModelManagerException($e);
            $mess = $e->getMessage() . ' in ' .$e->getFile() . ' in line : '.$e->getLine();
            $this->addFlash('sonata_flash_error', $mess);
        }

        return new RedirectResponse($this->admin->generateUrl(
            'list',
            array('filter' => $this->admin->getFilterParameters())
        ));
    }


    public function showAction($id = null)
    {

        $id = $this->getRequest()->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('VIEW', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        return $this->render($this->admin->getTemplate('show'), array(
            'action'   => 'show',
            'object'   => $object,
            'elements' => $this->admin->getShow(),
        ));
    }

    /**
     * Delete action.
     *
     * @param int|string|null $id
     *
     * @return Response|RedirectResponse
     *
     * @throws NotFoundHttpException If the object does not exist
     * @throws AccessDeniedException If access is not granted
     */

    public function deleteAction($id)
    {
        $id     = $this->getRequest()->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('DELETE', $object)) {
            throw new AccessDeniedException();
        }

        if ($this->getRestMethod() == 'DELETE') {
            // check the csrf token
            $this->validateCsrfToken('sonata.delete');

            try {
                if ($object instanceof Course) {
                    $courseManager = $this->get('cab.course_manager');
                    if ($object->getIsCycle()) {
                        //$this->changeParentCycle($courseManager, $object);
                    }
                }
                $this->admin->delete($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'ok'));
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->get('translator')->trans(
                        'flash_delete_success',
                        array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                        'SonataAdminBundle'
                    )
                );
            } catch (ModelManagerException $e) {
                $this->logModelManagerException($e);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'error'));
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->get('translator')->trans(
                        'flash_delete_error',
                        array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                        'SonataAdminBundle'
                    )
                );
            }

            return $this->redirectTo($object);
        }

        return $this->render($this->admin->getTemplate('delete'), array(
            'object'     => $object,
            'action'     => 'delete',
            'csrf_token' => $this->getCsrfToken('sonata.delete'),
        ));
    }

    /**
     * @param $courseManager
     * @param $object
     *
     */
    private function changeParentCycle($courseManager, $object)
    {
        $aCoursesChildCycle = $courseManager->getCycleChildCourse($object);
        /** @var Course $newParentCycleCourse */
        if (count($aCoursesChildCycle)) {
            $newParentCycleCourse = $aCoursesChildCycle[0];

            $newParentCycleCourse->setIsCycle(true);
            $newParentCycleCourse->setDaysCycle($object->getDaysCycle());
            $newParentCycleCourse->setCycleEndDate($object->getCycleEndDate());
            $newParentCycleCourse->setMondayCycle($object->getMondayCycle());
            $newParentCycleCourse->setTuesdayCycle($object->getTuesdayCycle());
            $newParentCycleCourse->setWednesdayCycle($object->getWednesdayCycle());
            $newParentCycleCourse->setThursdayCycle($object->getThursdayCycle());
            $newParentCycleCourse->setFridayCycle($object->getFridayCycle());
            $newParentCycleCourse->setSaturdayCycle($object->getSaturdayCycle());
            $newParentCycleCourse->setSundayCycle($object->getSundayCycle());
            $newParentCycleCourse->setTimeCycleParent($object->getTimeCycleParent());
            $newParentCycleCourse->setCycleParent(null);
            $courseManager->save($newParentCycleCourse);
            if (count($aCoursesChildCycle)) {
                /** @var Course $child */
                foreach ( $aCoursesChildCycle as $child ) {
                    if ($child->getId() !== $newParentCycleCourse->getId()) {
                        $child->setCycleParent($newParentCycleCourse->getId());
                        $courseManager->save($child);
                    }
                }
            }
        }
    }
}
