<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 28/07/2015
 * Time: 23:52
 */

namespace CAB\AdminBundle\Controller;

use CAB\CourseBundle\Entity\Vehicule;
use Presta\ImageBundle\Form\DataTransformer\Base64ToImageTransformer;
use Presta\ImageBundle\Form\Type\ImageType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CRUDController
 *
 * @package CAB\AdminBundle\Controller
 */
class VehiculeCRUDController extends CRUDController
{

    public function showAction($id = null)
    {
        $id = $this->get('request_stack')->getCurrentRequest()->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('VIEW', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $new_image_cropp = $this->get('form.factory')->createNamedBuilder(
            'UpdateImageVehiculeForm',
            FormType::class,
            null,
            [
                'attr' => [
                    'id' => 'UpdateImageVehiculeForm'
                ]
            ]
        )
            ->add('type_field', HiddenType::class)
            ->add('update_image', ImageType::class, ['label' => 'Upload new image'])
            ->getForm();

        $new_docs_cropp = $this->get('form.factory')->createNamedBuilder(
            'UpdateDocsVehiculeForm',
            FormType::class,
            null,
            [
                'attr' => [
                    'id' => 'UpdateDocsVehiculeForm'
                ]
            ]
        )
            ->add('type_field', HiddenType::class)
            ->add('upload_type',
                ChoiceType::class,
                [
                    'choices'           => [
                        'PDF'   => '1',
                        'Image' => '2'
                    ],
                    'label'             => 'Type file',
                    'required'          => true,
                    'choices_as_values' => true,
                    'multiple'          => false,
                    'expanded'          => true,
                    'mapped'            => false,
                    'data' => 1
                ]
            )
            ->add('expired_date', DateTimePickerType::class, [
                'label' => 'Change Date Exp',
                'format' => 'yyyy-MM-dd',
                'required' => false
            ])
            ->add('update_image', ImageType::class, ['label' => 'Upload new image'])
            ->add(
                'update_pdf',
                FileType::class,
                [
                    'label'    => 'Vehicle technical certificate',
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->getForm();


        return $this->render($this->admin->getTemplate('show'), [
            'action'          => 'show',
            'object'          => $object,
            'new_image_cropp' => $new_image_cropp->createView(),
            'new_docs_cropp'  => $new_docs_cropp->createView(),
            'elements'        => $this->admin->getShow(),
        ]);
    }

    public function updateImageAction($id = null)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        /** @var Vehicule $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        /** @var $form \Symfony\Component\Form\Form */
        if ($this->getRestMethod() == 'POST') {
            $form_data = $this->get('request')->request->get('UpdateImageVehiculeForm') ? $this->get('request')->request->get('UpdateImageVehiculeForm') : $this->get('request')->request->get('UpdateDocsVehiculeForm');

            $upload_type = isset($form_data['upload_type'])?$form_data['upload_type']:2;

            if (isset($form_data['expired_date']) && $form_data['expired_date']) {
                switch ($form_data['type_field']) {
                    case 'FileGreyRegistrationCard':
                        $object->setExpiredocGreyRegistrationCard(new \DateTime($form_data['expired_date']));
                        break;
                    case 'FileInsuranceCard':
                        $object->setExpiredocInsuranceCard(new \DateTime($form_data['expired_date']));
                        break;
                    case 'FileVehicleTechCertif':
                        $object->setExpiredocVehicleTechCertif(new \DateTime($form_data['expired_date']));
                        break;
                }
            }

            if ($upload_type && $upload_type == 1) {
                $uf = $this->get('request')->files->get('UpdateDocsVehiculeForm')['update_pdf'];
            } else {
                $b64 = new Base64ToImageTransformer();
                $uf = $b64->reverseTransform($form_data['update_image']);
            }
            $method_name = "set" . $form_data['type_field'];
            $object->{$method_name}($uf);
            $em = $this->getDoctrine()->getManager();
            $em->persist($object);
            $em->flush();

            $new_image_name = '';

            switch ($form_data['type_field']) {
                case 'FileFront':
                    $new_image_name = $object->getPhotoFront();
                    break;
                case 'FileInterior':
                    $new_image_name = $object->getPhotoInterior();
                    break;
                case 'FileBack':
                    $new_image_name = $object->getPhotoBack();
                    break;
                case 'FileGreyRegistrationCard':
                    $new_image_name = $object->getDocGreyRegistrationCard();
                    break;
                case 'FileInsuranceCard':
                    $new_image_name = $object->getDocInsuranceCard();
                    break;
                case 'FileVehicleTechCertif':
                    $new_image_name = $object->getDocVehicleTechCertif();
                    break;
            }

            $return = [
              'result'   => 'ok',
              'new_link' => '/uploads/vehicule/' . ($this->get('request')->request->get('UpdateImageVehiculeForm') ? '' : 'documents') . '/' . $new_image_name,
              'file_name' => $new_image_name
            ];

            if (isset($form_data['expired_date']) && $form_data['expired_date']) {
                $fmt = new \IntlDateFormatter(
                    $this->get('request')->getLocale(),
                    \IntlDateFormatter::LONG,
                    \IntlDateFormatter::NONE
                );

                $return['date_text'] = $fmt->format(new \DateTime($form_data['expired_date']));
                $dStart = new \DateTime();
                $dEnd  = new \DateTime($form_data['expired_date']);
                $dDiff = $dStart->diff($dEnd);

                $return['date_diff'] = $dDiff->invert ? "-" . ($dDiff->days + 1) : $dDiff->days + 1;
            }

            return $this->renderJson($return);
        }
    }
}