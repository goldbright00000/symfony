<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda<benhenda.med@gmail.com>
 * Date: 10/10/2015
 * Time: 00:42
 */

namespace CAB\AdminBundle\Controller;

use CAB\CourseBundle\Entity\Vehicule;
use Doctrine\DBAL\Cache\CacheException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use CAB\CourseBundle\Entity\Company;
use CAB\CourseBundle\Entity\VehiculeMake;
use CAB\CourseBundle\Entity\Zone;
use CAB\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class CommonController
 *
 * @package CAB\AdminBundle\Controller
 */
class CommonController extends Controller
{
    /*
     * @ParamConverter("vehicule", class="CABCourseBundle:Company")
     */
    /**
     * @param Company $company
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function ajaxAction (Company $company)
    {
        if (is_object($company)) {
            if ($this->isGranted('ROLE_ADMINCOMPANY')) {
                $repository = $this->getDoctrine()->getRepository('CABCourseBundle:Zone');
                $aZones = $repository->findBy(array('companyZone' => $company));
                $value = null;
                $listZones = array();

                foreach ($aZones as $zone) {
                    $listZones[] = array('val' => $zone->getId(), 'text' => $zone->getZoneName());
                }
                if ($aZones) {
                    $status = true;
                    $value = $listZones;
                } else {
                    $status = false;
                }

                $response = new JsonResponse();
                $response->setData(array(
                    'status_response' => $status,
                    'code_error' => 1,
                    'response' => $value,
                ));

                return $response;
            } else {
                $response = new JsonResponse();
                $response->setData(array(
                    'status_response' => false,
                    'code_error' => 2,
                    'response' => 'You are not allowed to access to this section',
                ));
            }
        } else {
            $response = new JsonResponse();
            $response->setData(array(
                'status_response' => false,
                'code_error' => 0,
                'response' => 'Error',
            ));
        }


    }

    /*
     * @ParamConverter("vehicleMake", class="CABCourseBundle:VehiculeMake")
     */
    /**
     * @param VehiculeMake $vehicleMake
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function modelVehicleAction (VehiculeMake $vehicleMake)
    {
        if (is_object($vehicleMake)) {
            if ($this->isGranted('ROLE_ADMINCOMPANY')) {
                $repository = $this->getDoctrine()->getRepository('CABCourseBundle:VehiculeModel');
                $models = $repository->findBy(array('vehicleMake' => $vehicleMake));
                $value = null;
                $listModels = array();
                foreach ($models as $model) {
                    $modelVeh = $model->getModUtac() . ' - ' . $model->getDscom() . ' - ' . $model->getCnit();
                    $listModels[] = array('val' => $model->getId(), 'text' => $modelVeh);
                }
                if ($models) {
                    $status = true;
                    $value = $listModels;
                } else {
                    $status = false;
                }

                $response = new JsonResponse();
                $response->setData(array(
                    'status_response' => $status,
                    'code_error' => 1,
                    'response' => $value,
                ));

                return $response;
            } else {
                $response = new JsonResponse();
                $response->setData(array(
                    'status_response' => false,
                    'code_error' => 2,
                    'response' => 'You are not allowed to access to this section',
                ));
            }
        } else {
            $response = new JsonResponse();
            $response->setData(array(
                'status_response' => false,
                'code_error' => 0,
                'response' => 'Error',
            ));
        }

    }

    /**
     * @param string $file
     */
    public function downloadDocumentAction ($file)
    {
        // Generate response
        $response = new Response();

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($file));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($file) . '";');
        $response->headers->set('Content-length', filesize($file));

        // Send headers before outputting anything
        $response->sendHeaders();

        $response->setContent(file_get_contents($file));
    }

    /**
     * @param string $longLatDep
     * @param integer $nbPerson
     * @param string $distance
     *
     * @throws \Exception if the query cannot be explained.
     * @return json
     */
    public function vehicleByLocationAction ($longLatDep, $nbPerson, $distance)
    {
        $aLonLat = explode(':', $longLatDep);
        $aDistance = explode(' ', $distance);
        $vehicleManager = $this->get('cab.vehicule_manager');
        try {
            $result = $vehicleManager->getVehiclesByLangLat($aLonLat[0], $aLonLat[1], $nbPerson, $aDistance[0]);
            $status = 1;
        } catch (\Exception $e) {
            $status = 0;
            $result = $e->getMessage();
        }

        $response = new JsonResponse();

        $response->setData(array(
            'status_response' => $status,
            'response' => $result,
        ));

        return $response;
    }

    /**
     * @param int $vehicleType
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function vehicleCourseAction ($vehicleType)
    {
        $vehicleManager = $this->get('cab.vehicule_manager');
        try {
            $oResult = $vehicleManager->getVehiclesBy('typeVehicule', $vehicleType);
            $result = array();
            foreach ($oResult as $item) {
                $result[] = array('id' => $item->getID(), 'name' => $item->getVehiculeName());
            }
            $status = 1;
        } catch (CacheException $e) {
            $status = 0;
            $result = $e->getMessage();

        }

        $response = new JsonResponse();
        $response->setData(array(
            'status_response' => $status,
            'response' => $result,
        ));

        return $response;
    }




    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function getVehiculeByExpiredDocsAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $unique_dates = [];
        $vehicules_by_date = [];
        $user_by_date = [];
        $dates = $em->getRepository('CABCourseBundle:Vehicule')->getVehiculeByExpiredDocsAll($this->getUser()->getId());


        $contacts = $this->getUser()->getContacts();
        $ids_contacts = [];
        /** @var Company $contact */
        foreach ($contacts as $contact) {
            $ids_contacts[] = $contact->getId();
        }
        $dates_user = $em->getRepository('CABUserBundle:User')->getUsersByExpiredDocsAll($ids_contacts);

        /** @var Vehicule $date */
      foreach ($dates as $date) {
        if ($date['expiredocGreyRegistrationCard']) {
          $unique_dates[] = $date['expiredocGreyRegistrationCard']->format("Y-m-d");
          $vehicules_by_date[$date['expiredocGreyRegistrationCard']->format("Y-m-d")][$date['id']] = [
            'id' => $date['id'],
            'number' => $date['licensePlateNumber']
          ];
        }
        if ($date['expiredocInsuranceCard']) {
          $unique_dates[] = $date['expiredocInsuranceCard']->format("Y-m-d");
          $vehicules_by_date[$date['expiredocInsuranceCard']->format("Y-m-d")][$date['id']] = [
            'id' => $date['id'],
            'number' => $date['licensePlateNumber']
          ];
        }
        if ($date['expiredocVehicleTechCertif']) {
          $unique_dates[] = $date['expiredocVehicleTechCertif']->format("Y-m-d");
          $vehicules_by_date[$date['expiredocVehicleTechCertif']->format("Y-m-d")][$date['id']] = [
            'id' => $date['id'],
            'number' => $date['licensePlateNumber']
          ];
        }
      }
        /** @var Vehicule $date */
      foreach ($dates_user as $date) {
        if ($date['expireDocLicence']) {
          $unique_dates[] = $date['expireDocLicence']->format("Y-m-d");
            $user_by_date[$date['expireDocLicence']->format("Y-m-d")][$date['id']] = [
            'id' => $date['id'],
            'username' => $date['username']
          ];
        }
        if ($date['expireDocVisitMed']) {
          $unique_dates[] = $date['expireDocVisitMed']->format("Y-m-d");
            $user_by_date[$date['expireDocVisitMed']->format("Y-m-d")][$date['id']] = [
            'id' => $date['id'],
            'username' => $date['username']
          ];
        }
        if ($date['expireDocIdentityPiece']) {
          $unique_dates[] = $date['expireDocIdentityPiece']->format("Y-m-d");
            $user_by_date[$date['expireDocIdentityPiece']->format("Y-m-d")][$date['id']] = [
            'id' => $date['id'],
            'username' => $date['username']
          ];
        }
        if ($date['expireDocCartePro']) {
          $unique_dates[] = $date['expireDocCartePro']->format("Y-m-d");
            $user_by_date[$date['expireDocCartePro']->format("Y-m-d")][$date['id']] = [
            'id' => $date['id'],
            'username' => $date['username']
          ];
        }
      }

        $response = new JsonResponse();
        $response->setData(array(
            'result'   => 'ok',
            'dates' => $unique_dates,
            'vehicules_by_date' => $vehicules_by_date,
            'user_by_date' => $user_by_date
        ));

        return $response;
    }

  /**
   * @param $vehicule_id
   * @param $date
   *
   * @return JsonResponse
   */
    public function getVehiculeAffectByDateAction($vehicule_id, $date)
    {
        $em = $this->getDoctrine()->getManager();
      $vehicule_affects = $em->getRepository('CABCourseBundle:VehiculeAffect')->getListDriversByVehiculeIdAndDate($vehicule_id, $date);

        $response = new JsonResponse();
        $response->setData(array(
            'result'   => 'ok',
            'vehicule_affects' => $vehicule_affects
        ));

        return $response;
    }

    /**
     * @param $driver_id
     * @param $date
     *
     * @return JsonResponse
     * @internal param $vehicule_id
     */
    public function getDriverConnectionByDateAction($driver_id, $date)
    {
        $em = $this->getDoctrine()->getManager();
        $vehicule_affects = $em->getRepository('CABUserBundle:User')->getListConnectionByDriverIdAndDate($driver_id, $date);

        foreach ($vehicule_affects as $key => $vehicule_affect) {
            $vehicule_affects[$key]['DATE'] = date("l F jS \\a\\t g:ia", strtotime($vehicule_affect['DATE']));
        }

        $response = new JsonResponse();
        $response->setData([
            'result'             => 'ok',
            'driver_connections' => $vehicule_affects
        ]);

        return $response;
    }

    /**
     * @param $driver_id
     * @param $date
     *
     * @return JsonResponse
     * @internal param $vehicule_id
     */
    public function getDriverAffectByDateAction($driver_id, $date)
    {
        $em = $this->getDoctrine()->getManager();
        $vehicule_affects = $em->getRepository('CABUserBundle:User')->getListAffectByDriverIdAndDate($driver_id, $date);

        foreach ($vehicule_affects as $key => $vehicule_affect) {
            $vehicule_affects[$key]['DATE'] = date("l F jS \\a\\t g:ia", strtotime($vehicule_affect['DATE']));
        }

        $response = new JsonResponse();
        $response->setData([
            'result'             => 'ok',
            'driver_affects' => $vehicule_affects
        ]);

        return $response;
    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function priceCourseAction (Request $request)
    {
        $data = $request->query->all();

        $longDep = $data['long_dep'];
        $latDep = $data['lat_dep'];
        $longArr = $data['long_arr'];
        $latArr = $data['lat_arr'];


        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINCOMPANY')) {
            $company = $data['company'];
            if ($company === '') {
                $company = null;
            }
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINCOMPANY')) {
            $company = $currentUser->getContacts()->first()->getId();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_AGENT')) {
            $company = $currentUser->getAgentCompany()->getId();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_DRIVER')) {
            $company = $currentUser->getDriverCompany()->getId();
        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_COMPANYBUSINESS')) {
            $company = $data['company'];
            if ($company === '') {
                $company = null;
            }
        }

        $em = $this->getDoctrine()->getManager();

        $checkForfait = $em->getRepository('CABCourseBundle:Forfait')->getForfaitByLongAndLat(
            $longDep,
            $latDep,
            $longArr,
            $latArr,
            $company
        );

        if (0 !== count($checkForfait)) {
            try {
                $forfait = $checkForfait[0];
                $oForfait = $em->getRepository('CABCourseBundle:Forfait')->find($forfait['id']);

                $aPrice = array(
                    'priceBasically' => 0,
                    'pricePerDistance' => 0,
                    'pricePerDate' => 0,
                    'pricePerPerson' => 0,
                    'pricePerLuggage' => 0,
                    'pricePerWheelChair' => 0,
                    'pricePerBabySeat' => 0,
                    'priceFromCompanyToDepAddr' => 0,
                    'priceTTC' => $oForfait->getTarifTTC(),
                    'totalPrice' => $oForfait->getTarifTTC(),
                    'isForfait' => 1,
                );

                $status = 1;
                $result = array(
                    'price' => array(
                        'price_total' => number_format($oForfait->getTarifHT(), 2, '.', ''),
                        'price_total_ttc' => number_format($oForfait->getTarifTTC(), 2, '.', ''),
                        'details' => array(
                            'departure' => $aPrice,
                            'back' => '',
                        ),
                        'tarif_id' => null,
                        'estimated_tarif' => number_format($oForfait->getTarifTTC(), 2, '.', ''),
                    )
                );
            } catch (CacheException $e) {
                $status = 0;
                $result = $e->getMessage();
            }
        } else {
            $driver = $data['driver'];

            $userManager = $this->get('cab.cab_user.manager');
            $oDriver = $userManager->loadUser($driver);
            //
            $company = $oDriver->getDriverCompany();
            $tarifManager = $this->get('cab.tarif.manager');

            try {
                $params = array(
                    'companyId' => $company->getId(),
                    'timeDeparture' => $data ['departure_time'],
                    'dateDeparture' => $data['departure_date'],
                    'departureAddress' => $data['departureAddress'],
                    'nbPerson' => $data['nb_person'],
                    'estimatedDistance' => $data['estimated_distance'],
                    'luggage' => $data['laguage'],
                    'wheelchair' => $data['nbWheelchair'],
                    'babySeat' => $data['babySeat'],
                );
                if (isset($data ['departure_back'])) {
                    $params['estimatedDistanceBack'] = $data ['departure_time'];
                    $params['nbPersonBack'] = $data ['nb_person_back'];
                    $params['dateDepartureBack'] = $data ['departure_date_back'];
                    $params['timeDepartureBack'] = $data ['departure_time_back'];
                    $params['luggageBack'] = $data ['laguage_back'];
                    $params['wheelchair_back'] = $data ['wheelchair_back'];
                    $params['babySeat_back'] = $data ['babySeat_back'];
                }

                $aPrice = $tarifManager->getTarifCourse($params);

                $result = array('price' => $aPrice);
                $status = 1;
            } catch (CacheException $e) {
                $status = 0;
                $result = $e->getMessage();
            }
        }


        $response = new JsonResponse();
        $response->setData(array(
            'status_response' => $status,
            'response' => $result,
        ));

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function addEventAction (Request $request)
    {
        $oDriver = $this->getDoctrine()->getRepository('CABUserBundle:User')->find($request->get('driver'));
        $sPlaning = $this->get('cab.planing.manager');
        $oStart = new \DateTime($request->get('start'));
        $oEnd = new \DateTime($request->get('end'));
        $status = $sPlaning->saveObject(array(
            'planingName' => $request->get('title'),
            'start' => $oStart,
            'end' => $oEnd,
            'driver' => $oDriver,
        ));

        $response = new JsonResponse();
        $response->setData(array(
            'status_response' => $status['status'],
            'response' => $status['response'],
        ));

        return $response;
    }

    /*
     * @ParamConverter("driver", class="CABUserBundle:User")
     */
    public function getEventsAction (User $driver)
    {
        $sPlaning = $this->get('cab.course_manager');
        try {
            $start = new \DateTime($_GET["start"]);
            //$start = $start->format('Y-m-d');
            $end = new \DateTime($_GET["end"]);
            //$end = $end->format('Y-m-d');
            $driver = $driver->getId();
            $result = $sPlaning->getArrayPlaning($driver,$start,$end);
            $status = true;
        } catch (\Exception $e) {
            $status = false;
            $result = $e->getMessage();
        }
        $response = new JsonResponse();
        $response->setData($result);

        return $response;
    }

    public function messageAction ()
    {
        $provider = $this->get('fos_message.provider');
        $threads = $provider->getInboxThreads();

        return $this->render('CABAdminBundle:Common:message.html.twig', array('threads' => $threads));
    }

    /**
     * Get the distance and time of course, this action is called from create course in BO
     *
     * @param string $departureAddress
     * @param string $arrivalAddress
     *
     * @return JsonResponse
     */
    public function getDistanceAndTimeAction ($departureAddress, $arrivalAddress)
    {
        $geolocationHandler = $this->get('cab_course.handler.geolocation_handler');
        $result = $geolocationHandler->getDistanceMatrix($departureAddress, $arrivalAddress, false, false, false);
        $resultResponse = array(
            'code_error' => 0,
            'response' => array(
                'distance' => $result[0],
                'time' => $result[1],
            ),
        );
        $response = new JsonResponse();
        $response->setData($resultResponse);

        return $response;

    }

    public function getCoursesForGroupageAction(Request $request)
    {
        $departureAddress = $request->request->get('departureAddress');
        $dateDepart = $request->request->get('dateDepart');
        $timeDepart = $request->request->get('timeDepart');
        $dateCourse = $dateDepart . $timeDepart;
        $oDateCourse = \DateTime::createFromFormat('Y-m-d H:i', $dateCourse);

        $currentUser = $this->getUser();
        $company = $currentUser->getContacts()->first();

        if (!$oDateCourse) {
            $code_error = 1;
            $result = array();
        } elseif ($company instanceof Company) {
            $code_error = 0; 
            $companyId = $company->getId();
            $courseManager = $this->get('cab.course_manager');
            $result = $courseManager->getCoursesForGroupage($departureAddress, $oDateCourse, $companyId);
        } else {
            $code_error = 2;
            $result = array();
        }

        $resultResponse = array(
            'code_error' => $code_error,
            'response' => $result,
        );
        $response = new JsonResponse();
        $response->setData($resultResponse);

        return $response;

    }


    /**
     * /**
     * Get the Business user (Super admin add business company and affect user business
     * @param  string $role
     * @return JsonResponse
     * @throws \Exception
     */
    public function getBusinessUserAction ($role)
    {
        $geolocationHandler = $this->get('cab_course.handler.geolocation_handler');
        $query = $this->getDoctrine()->getManager()->getRepository('CABUserBundle:User')->getUsersByRole($role);
        $result = $query->getQuery()->getResult();
        foreach ($result as $item) {

        }
        $resultResponse = array(
            'code_error' => 0,
            'response' => array(
                'distance' => '',
                'time' => $result,
            ),
        );
        $response = new JsonResponse();
        $response->setData($resultResponse);

        return $response;

    }

    /**
     * Get information of client, this action is called from create course in BO
     *
     * @param string $idClient
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public function getClientInfoAction ($idClient)
    {
        /** @var User $user */
        $user = $this->get('fos_user.user_manager')->findUserBy(array('id' => $idClient));

        $resultResponse = array(
            'code_error' => 0,
            'response' => array(
                'address' => $user->getAddress(),
                'lat' => $user->getLatUser(),
                'lng' => $user->getLngUser(),
            ),
        );
        $response = new JsonResponse();
        $response->setData($resultResponse);

        return $response;

    }

    /**
     * Get information of client, this action is called from create course in BO
     *
     * @param string $depDate
     * @param string $courseTime
     *
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public function getUnivailableDriversAction ($depDate, $courseTime)
    {
        $timezone = new \DateTimeZone('GMT');
        $courseTime = ceil($courseTime);
        list($dateDep, $hourDep, $minDep) = explode('_', $depDate);
        $depDate = $dateDep . ' ' . $hourDep . ':' . $minDep;

        $oDepDate = \DateTime::createFromFormat('Y-m-d H:i', $depDate, $timezone);
        $oArrDate = \DateTime::createFromFormat('Y-m-d H:i', $depDate, $timezone);
        $oArrDate->add(new \DateInterval('PT' . $courseTime . 'M'));
        /** @var array $aDrivers */
        $aDrivers = $this->get('cab.planing.manager')->getNotAvailableDrivers($oDepDate, $oArrDate);

        $resultResponse = array(
            'code_error' => 0,
            'response' => $aDrivers,
        );
        $response = new JsonResponse();
        $response->setData($resultResponse);

        return $response;

    }

    public function getDriversByServiceTransportAction ($serviceTransport)
    {
        $response = new JsonResponse();
        $authorizationChecker = $this->get('security.authorization_checker');
        $data = array();
        $em = $this->getDoctrine();

        if (!$serviceTransport) {
            $data['status'] = false;
        } else {

            $drivers = $em->getRepository('CABUserBundle:User')->getDriversServiceTransport((int)$serviceTransport, $this->getUser(), $authorizationChecker);

            $data['status'] = true;
            $data['drivers'] = $this->getDriversValues($drivers);
        }

        $response->setData($data);

        return $response;
    }

    private function getDriversValues ($drivers)
    {
        $getDrivers = array();

        if (count($drivers)) {
            foreach ($drivers as $key => $driver) {
                $getDrivers[] = array(
                    'id' => $driver->getId(),
                    'name' => strtoupper($driver->getLastName()) . ' ' . ucfirst($driver->getFirstName())
                );
            }
        }

        return $getDrivers;
    }
}