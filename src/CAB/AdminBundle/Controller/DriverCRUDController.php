<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 28/07/2015
 * Time: 23:52
 */

namespace CAB\AdminBundle\Controller;

use CAB\CourseBundle\Entity\Vehicule;
use CAB\UserBundle\Entity\User;
use Presta\ImageBundle\Form\DataTransformer\Base64ToImageTransformer;
use Presta\ImageBundle\Form\Type\ImageType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Class CRUDController
 *
 * @package CAB\AdminBundle\Controller
 */
class DriverCRUDController extends CRUDController
{

    public function showAction($id = null)
    {
        $id = $this->getRequest()->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('VIEW', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        $new_docs_cropp = $this->get('form.factory')->createNamedBuilder(
            'UpdateDocsDriverForm',
            'form',
            null,
            [
                'attr' => [
                    'id' => 'UpdateDocsDriverForm'
                ]
            ]
        )
            ->add('type_field', 'hidden')
            ->add('upload_type',
                'choice',
                [
                    'choices'           => [
                        'PDF'   => '1',
                        'Image' => '2'
                    ],
                    'label'             => 'Type file',
                    'required'          => true,
                    'choices_as_values' => true,
                    'multiple'          => false,
                    'expanded'          => true,
                    'mapped'            => false,
                    'data'              => 1
                ]
            )
            ->add('expired_date', 'sonata_type_date_picker', [
                'label'    => 'Change Date Exp',
                'format'   => 'yyyy-MM-dd',
                'required' => false
            ])
            ->add('update_image', ImageType::class, ['label' => 'Upload new image'])
            ->add(
                'update_pdf',
                'file',
                [
                    'label'    => 'Vehicle technical certificate',
                    'required' => false,
                    'mapped'   => false
                ]
            )
            ->getForm();


        return $this->render($this->admin->getTemplate('show'), [
            'action'         => 'show',
            'object'         => $object,
            'new_docs_cropp' => $new_docs_cropp->createView(),
            'elements'       => $this->admin->getShow(),
        ]);
    }

    public function updateImageAction($id = null)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        /** @var User $object */
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }


        /** @var $form \Symfony\Component\Form\Form */
        if ($this->getRestMethod() == 'POST') {
            $form_data = $this->get('request')->request->get('UpdateDocsDriverForm');

            $upload_type = isset($form_data['upload_type']) ? $form_data['upload_type'] : 2;

            if (isset($form_data['expired_date']) && $form_data['expired_date']) {
                switch ($form_data['type_field']) {
                    case 'FileLicence':
                        $object->setExpireDocLicence(new \DateTime($form_data['expired_date']));
                        break;
                    case 'FileVisitMed':
                        $object->setExpireDocVisitMed(new \DateTime($form_data['expired_date']));
                        break;
                    case 'FileIdentityPiece':
                        $object->setExpireDocIdentityPiece(new \DateTime($form_data['expired_date']));
                        break;
                    case 'FileCartePro':
                        $object->setExpireDocCartePro(new \DateTime($form_data['expired_date']));
                        break;
                }
            }

            if ($upload_type && $upload_type == 1) {
                $uf = $this->get('request')->files->get('UpdateDocsDriverForm')['update_pdf'];
            } else {
                $b64 = new Base64ToImageTransformer();
                $uf = $b64->reverseTransform($form_data['update_image']);
            }
            $method_name = "set" . $form_data['type_field'];
            $object->{$method_name}($uf);
            $em = $this->getDoctrine()->getManager();
            $em->persist($object);
            $em->flush();

            $path = '';
            $new_image_name = '';
            $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');

            switch ($form_data['type_field']) {
                case 'FileLicence':
                    $path = $helper->asset($object, 'fileLicence');
                    $new_image_name = $object->getNameLicence();
                    break;
                case 'FileVisitMed':
                    $path = $helper->asset($object, 'fileVisitMed');
                    $new_image_name = $object->getNameVisitMed();
                    break;
                case 'FileIdentityPiece':
                    $path = $helper->asset($object, 'fileIdentityPiece');
                    $new_image_name = $object->getNameIdentityPiece();
                    break;
                case 'FileCartePro':
                    $path = $helper->asset($object, 'fileCartePro');
                    $new_image_name = $object->getNameCartePro();
                    break;
            }


            $return = [
                'result'    => 'ok',
                'new_link'  => $path,
                'file_name' => $new_image_name
            ];

            if (isset($form_data['expired_date']) && $form_data['expired_date']) {
                $fmt = new \IntlDateFormatter(
                    $this->get('request')->getLocale(),
                    \IntlDateFormatter::LONG,
                    \IntlDateFormatter::NONE
                );

                $return['date_text'] = $fmt->format(new \DateTime($form_data['expired_date']));
                $dStart = new \DateTime();
                $dEnd = new \DateTime($form_data['expired_date']);
                $dDiff = $dStart->diff($dEnd);

                $return['date_diff'] = $dDiff->invert ? "-" . ($dDiff->days + 1) : $dDiff->days + 1;

            }

            return $this->renderJson($return);
        }
    }
}