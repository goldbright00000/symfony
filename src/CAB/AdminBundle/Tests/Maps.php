<?php

namespace CAB\AdminBundle\Tests;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;



use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Doctrine\ORM\Query\Expr;

/**
 * Class CustomerAdmin
 * @package CAB\AdminBundle\Admin
 */

class Maps extends Admin
{
    /**
     * @var integer $description
     */
    public $description;

    /**
     * @var array
     */
    public $pageTitle;

    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->baseRouteName = 'sonata_maps';
        $this->baseRoutePattern = 'maps';
        $this->pageTitle = array(
            'create' => 'Create Maps',
            'edit' => 'Edit Maps',
            'list' => 'Maps List',
            'show' => 'Show Maps',
            'default' => 'Maps dashboard',
        );

        $this->description = 'Manage customer. nec ipse ab ullo diligatur, circumfluere omnibus copiis atque in omnium
        rerum abundantia vivere? Haec enim est tyrannorum vita nimirum, in qua nulla fides, nulla caritas, nulla
        stabilis benevolentiae potest esse fiducia, omnia semper suspecta atque sollicita, nullus locus amicitiae.';
    }

    /**
     * {@inheritDoc}
     */
    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'CABAdminBundle:CRUD:customer_edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }

    }

    /**
     * {@inheritDoc}
     */
    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'CABAdminBundle:Form:form_admin.html.twig',
            )
        );
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($context = 'list')
    {
        $qb = parent::createQuery($context = 'list');
        $alias = $qb->getRootAliases()[0];
        $qb->andWhere($alias.'.roles LIKE :role');
        $qb->setParameter('role', '%ROLE_CUSTOMER%');

        return $qb;
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist($oDriver)
    {
        $oDriver->setRoles(array('ROLE_CUSTOMER'));
    }

    
    

    // Fields to be shown on filter forms
    /**
     * {@inheritDoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('email');
    }

    // Fields to be shown on lists
    /**
     * {@inheritDoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('address')
            ->add(
                'avatar',
                null,
                array(
                    'template' => 'CABAdminBundle:Custom:photo.html.twig',
                    'data' => 'something',
                    'dir' => '/uploads/avatar/',
                )
            );
    }
}
