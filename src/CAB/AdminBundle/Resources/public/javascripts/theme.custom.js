(function( $ ) {
    'use strict';

    $.extend(theme.PluginDatePicker.defaults, {
        format: "yyyy-mm-dd"
    });
    $.extend(theme.PluginTimePicker.defaults, {
        showSeconds: false,
        showMeridian: false,
        minuteStep: 15
    });

    //$('#collapse0One').collapse("hide");
    $('#collapse1One').collapse("hide");
    $('#collapse2One').collapse("hide");
    $('#collapse3One').collapse("hide");
    $('#collapse4One').collapse("hide");
    $('#collapse5One').collapse("hide");

    // BusinessCompany: Super admin add business company or admin company
    if ($('select[name$="[isBusiness]"]').val() == 1) {
        $('#div_select_business_company').css('display', 'block');
    }
    $('select[name$="[isBusiness]"]').change(function () {
        if ($(this).val() == 1) {
            $('#div_select_business_company').css('display', 'block');
        } else {
            $('#div_select_business_company').css('display', 'none');
        }

        // Submit data via AJAX to the form's action path.
        /*
         var data = {};
         data[$(this).attr('name')] = $(this).val();
         // ... retrieve the corresponding form.
         var $form = $(this).closest('form');
         $.ajax({
            //url: Routing.generate('get_business_user', { 'role': role}),
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            beforeSend : function () {
                $('body').append("<div class='ajax-wait'></div>")
                $('body').addClass("loading")
            },
            success: function(html) {
                // Replace current position field ...
                $('#select_business_company').replaceWith(
                    // ... with the returned one from the AJAX response.
                    $(html).find('#select_business_company')
                );
                // Position field now displays the appropriate positions.
            },
            complete: function () {
                $('body').removeClass("loading");
                $('.ajax-wait').css("display", 'none');
            }
        });
        */
    });

    /*
    *checkbox workday
     */
    $('div[id$="_driverWorkDay_monday"]').click(function(){
        itemDisplay($(this).children( "input" ), '#monday_start_time_day_tr');
        var checkStatus = $(this).val();
    });

    $('div[id$="_driverWorkDay_tuesday"]').click(function(){
        itemDisplay($(this).children( "input" ),'#tuesday_start_time_day_tr');
        var checkStatus = $(this).val();
    });
    $('div[id$="_driverWorkDay_wednesday"]').click(function(){
        itemDisplay($(this).children( "input" ),'#wednesday_start_time_day_tr');
        var checkStatus = $(this).val();
    });
    $('div[id$="_driverWorkDay_thursday"]').click(function(){
        itemDisplay($(this).children( "input" ),'#thursday_start_time_day_tr');
        var checkStatus = $(this).val();
    });
    $('div[id$="_driverWorkDay_friday"]').click(function(){
        itemDisplay($(this).children( "input" ),'#friday_start_time_day_tr');
        var checkStatus = $(this).val();
    });
    $('div[id$="_driverWorkDay_saturday"]').click(function(){
        itemDisplay($(this).children( "input" ),'#saturday_start_time_day_tr');
        var checkStatus = $(this).val();
    });
    $('div[id$="_driverWorkDay_sunday"]').click(function(){
        itemDisplay($(this).children( "input" ),'#sunday_start_time_day_tr');
        var checkStatus = $(this).val();
    });
    var daySelector = "_start_time_day_tr";
    if($('input[id$="_driverWorkDay_monday"]').is(':checked') == true){
        showTableHours('monday', daySelector, 'day');
    }
    if($('input[id$="_driverWorkDay_tuesday"]').is(':checked') == true){
        showTableHours('tuesday', daySelector, 'day');
    }
    if($('input[id$="_driverWorkDay_wednesday"]').is(':checked') == true){
        showTableHours('wednesday', daySelector, 'day');
    }
    if($('input[id$="_driverWorkDay_thursday"]').is(':checked') == true){
        showTableHours('thursday', daySelector, 'day');
    }
    if($('input[id$="_driverWorkDay_friday"]').is(':checked') == true){
        showTableHours('friday', daySelector, 'day');
    }
    if($('input[id$="_driverWorkDay_saturday"]').is(':checked') == true){
        showTableHours('saturday', daySelector, 'day');
    }
    if($('input[id$="_driverWorkDay_sunday"]').is(':checked') == true){
        showTableHours('sunday', daySelector, 'day');
    }

    /*
    * chexbox work night
     */
    if( $("input[name$='[nightWorkingDriver]']").val() == 1){
        $('#driver-work-night').removeClass('hide');
    }
    $('div[id$="_nightWorkingDriver"]').click(function(){
        var statusNight = $("input[name$='[nightWorkingDriver]").val();
        if(statusNight == 1){
            $('#driver-work-night').removeClass('hide');
        }else if(statusNight == 0){
            $('#driver-work-night').addClass('hide');
        }
    });


    $('div[id$="_driverWorkNight_monday"]').click(function(){
        itemDisplayNight($(this).children( "input" ), '#monday_start_time_night_tr_night');
    });

    $('div[id$="_driverWorkNight_tuesday"]').click(function(){
        itemDisplayNight($(this).children( "input" ),'#tuesday_start_time_night_tr_night');
    });
    $('div[id$="_driverWorkNight_wednesday"]').click(function(){
        itemDisplayNight($(this).children( "input" ),'#wednesday_start_time_night_tr_night');
    });
    $('div[id$="_driverWorkNight_thursday"]').click(function(){
        itemDisplayNight($(this).children( "input" ),'#thursday_start_time_night_tr_night');
    });
    $('div[id$="_driverWorkNight_friday"]').click(function(){
        itemDisplayNight($(this).children( "input" ),'#friday_start_time_night_tr_night');
    });
    $('div[id$="_driverWorkNight_saturday"]').click(function(){
        itemDisplayNight($(this).children( "input" ),'#saturday_start_time_night_tr_night');
    });
    $('div[id$="_driverWorkNight_sunday"]').click(function(){
        itemDisplayNight($(this).children( "input" ),'#sunday_start_time_night_tr_night');
    });

    var selectorNight = "_start_time_night_tr_night";
    if($('input[id$="_driverWorkNight_monday"]').val() == 1){
        showTableHours('monday', selectorNight, 'night');
    }
    if($('input[id$="_driverWorkNight_tuesday"]').val() == 1){
        showTableHours('tuesday', selectorNight, 'night');
    }
    if($('input[id$="_driverWorkNight_wednesday"]').val() == 1){
        showTableHours('wednesday', selectorNight, 'night');
    }
    if($('input[id$="_driverWorkNight_thursday"]').val() == 1){
        showTableHours('thursday', selectorNight, 'night');
    }
    if($('input[id$="_driverWorkNight_friday"]').val() == 1){
        showTableHours('friday', selectorNight, 'night');
    }
    if($('input[id$="_driverWorkNight_saturday"]').val() == 1){
        showTableHours('saturday', selectorNight, 'night');
    }
    if($('input[id$="_driverWorkNight_sunday"]').val() == 1){
        showTableHours('sunday', selectorNight, 'night');
    }

    function showTableHours(day, selector, type){

        if(type == 'day') {
            $('#cab-head-table').show('slow');
            $('#cab-head-table').removeClass('hide');
        }else{
            $('#cab-head-table-night').show('slow');
            $('#cab-head-table-night').removeClass('hide');
        }

        var selectorDay = '#' + day + selector;
        $(selectorDay).show('slow');
        $(selectorDay).removeClass('hide');

    }
    function itemDisplay(obj, item){
        if($(obj).val() == 1){
            $('#cab-head-table').show('slow');
            $('#cab-head-table').removeClass('hide');
            $(item).show("slow");
            $(item).removeClass('hide');
        }else  if($(obj).val() == 0){
            $(item).hide("slow");
            $(item).addClass('hide');

            if($('input[id$="_driverWorkDay_monday"]').val() == 0 && $('input[id$="_driverWorkDay_tuesday"]').val() == 0
                && $('input[id$="_driverWorkDay_wednesday"]').val() == 0 && $('input[id$="_driverWorkDay_thursday"]').val() == 0
                && $('input[id$="_driverWorkDay_friday"]').val() == 0 && $('input[id$="_driverWorkDay_saturday"]').val() == 0
                && $('input[id$="_driverWorkDay_sunday"]').val() == 0 ){
                $('#cab-head-table').hide('slow');
                $('#cab-head-table').addClass('hide');
            }
        }
    }

    function itemDisplayNight(obj, item){
        if($(obj).val() == 1){
            $('#cab-head-table-night').show('slow');
            $('#cab-head-table-night').removeClass('hide');
            $(item).show("slow");
            $(item).removeClass('hide');
        }else  if($(obj).val() == 0){
            $(item).hide("slow");
            $(item).addClass('hide');

            if($('input[id$="_driverWorkNight_monday"]').val() == 0 && $('input[id$="_driverWorkNight_tuesday"]').val() == 0
                && $('input[id$="_driverWorkNight_wednesday"]').val() == 0 && $('input[id$="_driverWorkNight_thursday"]').val() == 0
                && $('input[id$="_driverWorkNight_friday"]').val() == 0 && $('input[id$="_driverWorkNight_saturday"]').val() == 0
                && $('input[id$="_driverWorkNight_sunday"]').val() == 0 ){
                $('#cab-head-table-night').hide('slow');
                $('#cab-head-table-night').addClass('hide');
            }
        }
    }

    // registration customer through modal template (admin)
    $("input[name='cab_user_registration[address]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();
            $.each(result.address_components, function (index, value) {
                if (value.types == "country,political") {
                    $("#cab_user_registration_country").val(value.short_name);
                }
                if (value.types == "postal_code") {
                    $("#cab_user_registration_postCode").val(value.long_name);
                }
                if (value.types == "locality,political") {
                    $("#cab_user_registration_city").val(value.long_name);
                }
            });
            //var placeID = result.place_id;
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();

            $("#cab_user_registration_lnguser").val(longitude);
            $("#cab_user_registration_latuser").val(latitude);
        });

    /* admin new/edit forfait*/
    $("input[name$='[depAddress]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();

            $("#longitudeDep").val(longitude);
            $("#latitudeDep").val(latitude);
        });

    $("input[name$='[arrivalAddress]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            var storableLocation = {};
            for (var ac = 0; ac < result.address_components.length; ac++) {
                var component = result.address_components[ac];

                switch(component.types[0]) {
                    case 'locality':
                        storableLocation.city = component.long_name;
                        $("#arrivalAddressCity").val(storableLocation.city);
                        break;
                    case 'country':
                        storableLocation.country = component.long_name;
                        storableLocation.registered_country_iso_code = component.short_name;
                        break;
                }
            };
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();
            $("#longitudeArrival").val(longitude);
            $("#latitudeArrival").val(latitude);
        });

    $("input[name$='[arrAddress]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();

            $("#longitudeArrival").val(longitude);
            $("#latitudeArrival").val(latitude);
        });


    $("input[name$='[startedAddress]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            var storableLocation = {};
            for (var ac = 0; ac < result.address_components.length; ac++) {
                var component = result.address_components[ac];

                switch(component.types[0]) {
                    case 'locality':
                        storableLocation.city = component.long_name;
                        $("#startedAddressCity").val(storableLocation.city);
                        break;
                    case 'country':
                        storableLocation.country = component.long_name;
                        storableLocation.registered_country_iso_code = component.short_name;
                        break;
                }
            };
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();
            var departureCity = result.vicinity;
            $("#longitudeDep").val(longitude);
            $("#latitudeDep").val(latitude);

        });

    $("input[name$='[address]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();
            $("#longitudeDep").val(longitude);
            $("#latitudeDep").val(latitude);

        });

    $('input[name$="[tarifHT]"]').change(function () {
        var tarifHT = $(this).val();
        var tarifTTC = tarifHT;

        var tax = $('input[name$="[tax]"]').val();
        if (tax != 0) {
            var tarifTTC = parseInt(tarifHT) + parseInt(((tarifHT * tax) / 100));
        }
        $('input[name$="[tarifTTC]"]').val(tarifTTC);
    });

    /* END admin new/edit forfait */


        /* Insert price article */


    $("input[name$='[departure]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            //console.log(result);
            console.log('toto');
            var bla = $("input[name$='[departure]']").val();
            console.log(bla);
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();

            $( "input[id*='latDep']" ).val(latitude);
            $( "input[id*='longDep']" ).val(longitude);

        });

    $("input[name$='[arrival]']").geocomplete()
        .bind("geocode:result", function (event, result) {
            var latitude = result.geometry.location.lat();
            var longitude = result.geometry.location.lng();

            $( "input[id*='latArr']" ).val(latitude);
            $( "input[id*='longArr']" ).val(longitude);

        });

    //  'get distance and time'.
    $('#get-distance-time-article').click(function(){


        var departure = $('input[id$="_departure"]').val();
        var latDeparture = $('input[id$="latDep"]').val();
        var longDeapture = $('input[id$="longDep"]').val();
        var arrival = $('input[id$="_arrival"]').val();
        var latArrival = $('input[id$="latArr"]').val();
        var longArrival = $('input[id$="longArr"]').val();
        var latlngDeparture = latArrival +','+longDeapture;
        var latlngArrival = latArrival +','+longArrival;

        if (departure.length == 0 ) {
            $('.modal-title').text('Error departure address field');
            $('#content_modal_body').text('Check the departure address');
            $('#courseModal').modal('show');
            $('input[id$="_departure"]').css('border-color','#c89494');
            $('input[id$="_departure"]').css('background-color','#c89494');
        } else if (arrival.length == 0) {
            $('.modal-title').text('Error arrival address field');
            $('#content_modal_body').text('Check the arrival address');
            $('#courseModal').modal('show');
            $('input[id$="_arrival"]').css('border-color','#c89494');
            $('input[id$="_arrival"]').css('background-color','#c89494');
        } else {
            $.get("https://route.api.here.com/routing/7.2/calculateroute.json?waypoint0="+latlngDeparture+"&waypoint1="+latlngArrival+"&app_id=SwwoDXGzVt198ybVyZ6l&app_code=VRl28m4nCLieuds47Ei8iw&mode=fastest;car;traffic:enabled", function(data, status) {

                $.each(data.response.route, function (k, v) {
                    //console.log(v);
                    var distance = v.summary.distance;
                    distance = (distance/1000).toFixed(2);
                    var time = v.summary.baseTime;
                    time = Math.round(time/60);
                    var trafficTime = v.summary.trafficTime;
                    trafficTime = Math.round(trafficTime/60);
                    var timeadd = trafficTime-time;
                    $('input[id$="_distance"]').val(distance);
                    $('input[id$="_time"]').val(trafficTime);
                    var price4 = (distance*1.35).toFixed(0);
                    var price6 = (distance*1.90).toFixed(0);
                    $('input[id$="_price4"]').val(price4);
                    $('input[id$="_price6"]').val(price6)
                });
            });
        }

    });

    /* END Insert price article */




    if ($.fn.datepicker) {

    $('.input-datepicker').datepicker({
        format: "yyyy-mm-dd",
        forceParse: false,
        autoclose: true,
        todayHighlight: true
    });
    }
    if ($.fn.timepicker) {

    $('#timepicker1').timepicker();
    $('.timepicker-input').timepicker({"showMeridian": false, "minuteStep": 15});
    }
        if( $("input[name$='[zoneAddress]']").length || $("input[name$='[startedAddress]']").length || $("input[name$='[arrivalAddress]']").length) {

            $("input[name$='[zoneAddress]'], input[name$='[startedAddress]'], input[name$='[arrivalAddress]']").geocomplete()
                .bind("geocode:result", function (event, result) {
                    var latitude = result.geometry.location.lat();
                    var longitude = result.geometry.location.lng();

                    $("input[name$='[longitude]']").val(longitude);
                    $("input[name$='[latitude]']").val(latitude);
                });
        }

    if( $("input[name$='[_address]']").length && $.fn.geocomplete) {

        //form add company: get lat and lng
        $("input[name$='[_address]']").geocomplete()
            .bind("geocode:result", function (event, result) {


                $.each(result.address_components, function (index, value) {
                    if (value.types == "country,political") {
                        $("input[name$='[country]']").val(value.short_name);
                    }
                    if (value.types == "postal_code") {
                        $("input[name$='[postCode]']").val(value.long_name);
                    }
                    if (value.types == "locality,political") {
                        $("input[name$='[city]']").val(value.long_name);
                    }

                });
                //var placeID = result.place_id;
                var latitude = result.geometry.location.lat();
                var longitude = result.geometry.location.lng();

                $("input[name$='[lng]']").val(longitude);
                $("input[name$='[lat]']").val(latitude);
            });
    }

    if( $("input[name$='[address]']").length && $.fn.geocomplete) {

        //form add company: get lat and lng
        $("input[name$='[address]']").geocomplete()
            .bind("geocode:result", function (event, result) {
                $.each(result.address_components, function (index, value) {
                    console.log(index + " -- " + value.types + "----" + value.long_name);
                    if (value.types == "country,political") {
                        $("input[name$='[country]']").val(value.short_name);
                    }
                    if (value.types == "postal_code") {

                        $("input[name$='[postCode]']").val(value.long_name);
                    }else{
                        $("input[name$='[postCode]']").val('');

                    }
                    if (value.types == "locality,political") {

                        $("input[name$='[city]']").val(value.long_name);
                    }

                });
                //var placeID = result.place_id;
                var latitude = result.geometry.location.lat();

                var longitude = result.geometry.location.lng();

                $("input[name$='[lngUser]']").val(longitude);
                $("input[name$='[latUser]']").val(latitude);
            });
    }

    //Save New user(customer/contact..) and return it

    var options = {
        //target:        '#output1',   // target element(s) to be updated with server response
        success:       function(responseText, statusText, xhr, $form) {
            if(statusText == 'success' && responseText.status == 1){
                $('select.select-user').append($('<option>', {
                    value:responseText.id,
                    text: responseText.text
                }));
                $('select.select-user').val(responseText.id);
                $.magnificPopup.close();
            }
        } // post-submit callback

        // other available options:
        //url:       url         // override for form's 'action' attribute
        //type:      type        // 'get' or 'post', override for form's 'method' attribute
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
        //clearForm: true        // clear all form fields after successful submit
        //resetForm: true        // reset the form after successful submit

        // $.ajax options can be used here too, for example:
        //timeout:   3000
    };
    // bind form using 'ajaxForm'
    $('#modalFormNewCustomer .fos_user_registration_register').submit(function() {
        $(this).ajaxSubmit(options);
        return false;
    });
    $('#modalFormNotification').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id= button.data('userid') // Extract info from data-* attributes
        var name= button.data('username') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Manage notification for ' + name)
        modal.find('.modal-body p').val(id)
    })

    if ($.fn.ajaxForm)
    $('#modal-create-user').ajaxForm(options);

    /*
     * customer add: show field namecompany, siren and phone company when the iscompany is checked
     */
    if($('input[name$="[isCompany]"]').is(':checked') == true || $('input[name$="[isCompany]"]').val() == 1){
        $('#form-company-inner-customer').css('display', 'block');
    }
    $('div[id$="_isCompany"]').click(function(){

        if($('input[name$="[isCompany]"]').val() == 1){

            $('#form-company-inner-customer').css('display', 'block');
        }else {
            $('#form-company-inner-customer').css('display', 'none');
        }
    });

    /*
     * customer add: show field options handicap when the isHandicap is checked
     */
    if($('input[name$="[isHandicap]"]').is(':checked') == true){
        $('#option-handicap').css('display', 'block');

    }
    $('div[id$="_isHandicap"]').click(function(){

        if($('input[name$="[isHandicap]"]').is(':checked') == true){

            $('#option-handicap').css('display', 'block');
        }else {
            $('#option-handicap').css('display', 'none');
        }
    });

    // create course from BO. click on btn 'get distance and time'.

    $('#get-distance-time').click(function(){

        var departure = $('input[id$="_startedAddress"]').val();
        var latDeparture = $('input[id$="latitudeDep"]').val();
        var longDeapture = $('input[id$="longitudeDep"]').val();
        var latArrival = $('input[id$="latitudeArrival"]').val();
        var longArrival = $('input[id$="longitudeArrival"]').val();

        var latlngDeparture = latDeparture +','+longDeapture;
        var latlngArrival = latArrival +','+longArrival;
        var longArrival = $('input[id$="longitudeArrival"]').val();
        var arrival = $('input[id$="_arrivalAddress"]').val();
        var _personNumber = $('input[id$="_personNumber"]').val();
        var _laguage = $('input[id$="_laguage"]').val();
        var _nbWheelchair = $('input[id$="_nbWheelchair"]').val();
        var _nbChildren = $('input[id$="_nbChildren"]').val();

        if (departure.length == 0 ) {
            $('.modal-title').text('Error departure address field');
            $('#content_modal_body').text('Check the departure address');
            $('#courseModal').modal('show');
            $('input[id$="_startedAddress"]').css('border-color','#c89494');
            $('input[id$="_startedAddress"]').css('background-color','#c89494');
        } else if (arrival.length == 0) {
            $('.modal-title').text('Error arrival address field');
            $('#content_modal_body').text('Check the arrival address');
            $('#courseModal').modal('show');
            $('input[id$="_arrivalAddress"]').css('border-color','#c89494');
            $('input[id$="_arrivalAddress"]').css('background-color','#c89494');
        } else {
            $.get("https://route.api.here.com/routing/7.2/calculateroute.json?waypoint0="+latlngDeparture+"&waypoint1="+latlngArrival+"&app_id=SwwoDXGzVt198ybVyZ6l&app_code=VRl28m4nCLieuds47Ei8iw&mode=fastest;car;traffic:enabled", function(data, status) {

                $.each(data.response.route, function (k, v) {
                    //console.log(v);
                    var distance = v.summary.distance;
                    distance = (distance/1000).toFixed(2);
                    var time = v.summary.baseTime;
                    time = Math.round(time/60);
                    var trafficTime = v.summary.trafficTime;
                    trafficTime = Math.round(trafficTime/60);
                    var timeadd = trafficTime-time;
                    $('input[id$="_distance"]').val(distance);
                    $('input[id$="_courseTime"]').val(trafficTime);
                    if (timeadd>0){
                        document.getElementById("inforace").innerHTML = 'il faut prévoir '+timeadd+' minutes de temps supplémentaire (inital : '+time+' minutes)' ;
                    }
                });
            });
        }

    });




    // create course from BO. click on btn 'get distance and time' for back .
    $('#get-distance-time-back').click(function(){
        var departure = $('input[id$="_backAddressDeparture"]').val();
        var arrival = $('input[id$="_backAddressArrival"]').val();

        if (departure.length == 0 || arrival.length == 0) {
            $('.modal-title').text('Error address fields');
            $('#content_modal_body').text('Check the address for back');
            $('#courseModal').modal('show');
            if (departure.length == 0) {
                $('input[id$="_backAddressDeparture"]').css('border-color','#c89494');
                $('input[id$="_backAddressDeparture"]').css('background-color','#c89494');
            }
            if (departure.length == 0) {
                $('input[id$="_backAddressArrival"]').css('border-color','#c89494');
                $('input[id$="_backAddressArrival"]').css('background-color','#c89494');
            }
        } else {
            $.ajax({
                url: Routing.generate('get_distance_time', {departureAddress: departure, arrivalAddress: arrival}),
                error: function () {

                    console.log('Error Get distance and time Back: An error has occurred');
                },
                success: function (data) {
                    if (data.response != null) {
                        $('input[id$="_distanceBack"]').val(data.response.distance);

                        $('input[id$="_distanceBack"]').css('border-color','#CCCCCC');
                        $('input[id$="_distanceBack"]').css('background-color','#ffffff');
                        var calculTime = data.response.time / 60;
                        $('input[id$="_courseTimeBack"]').val(calculTime);

                    } else if (data.code_error != 0) {


                        console.log('Get distance and time Back: An error has occurred');
                    }
                },
                type: 'GET'
            });
        }

    });


    // create course from BO. click on btn 'Get Price and vechcle available'.
    $('#get-price-vehicle').click(function(){

        var departure = $('input[id$="_startedAddress"]').val();
        var arrival = $('input[id$="_arrivalAddress"]').val();
        var nb_person = $('input[id$="_personNumber"]').val();
        var estimated_distance = $('input[id$="_distance"]').val();
        var departure_date = $('input[id$="_departureDate"]').val();
        var departure_time = $('input[id$="_departureTime"]').val();
        var driver = $('select[id$="_driver"]').val();

        if (departure.length == 0 || arrival.length == 0 || estimated_distance.length == 0
            || departure_date.length == 0 || departure_time.length == 0 || driver.length == 0) {
            $('.modal-title').text('Error fields');
            $('#content_modal_body').text('Check the colored fields');
            $('#courseModal').modal('show');

            if (driver.length == 0) {
                $('select[id$="_driver"]').css('border-color','#c89494');
                $('select[id$="_driver"]').css('background-color','#c89494');
            }

            if (departure.length == 0) {
                $('input[id$="_startedAddress"]').css('border-color','#c89494');
                $('input[id$="_startedAddress"]').css('background-color','#c89494');
            }

            if (arrival.length == 0) {
                $('input[id$="_arrivalAddress"]').css('border-color','#c89494');
                $('input[id$="_arrivalAddress"]').css('background-color','#c89494');
            }
            if (estimated_distance.length == 0) {
                $('input[id$="_distance"]').css('border-color','#c89494');
                $('input[id$="_distance"]').css('background-color','#c89494');
            }

            if (departure_date.length == 0) {
                $('input[id$="_departureDate"]').css('border-color','#c89494');
                $('input[id$="_departureDate"]').css('background-color','#c89494');
            }

            if (departure_time.length == 0) {
                $('input[id$="_departureTime"]').css('border-color','#c89494');
                $('input[id$="_departureTime"]').css('background-color','#c89494');
            }


        } else {
            var _laguage = $('input[id$="_laguage"]').val();
            var _nbWheelchair = $('input[id$="_nbWheelchair"]').val();
            var _nbChildren = $('input[id$="_nbChildren"]').val();

            var longDep = $('#longitudeDep').val();
            var latDep = $('#latitudeDep').val();
            var longArr = $('#longitudeArrival').val();
            var latArr = $('#latitudeArrival').val();
            var company = $('input[name$="[company]"]').val();
            var data_submit = {
                departureAddress: departure,
                arrivalAddress: arrival,
                nb_person: nb_person,
                estimated_distance: estimated_distance,
                departure_date: departure_date,
                departure_time: departure_time,
                driver: driver,
                laguage: _laguage,
                nbWheelchair: _nbWheelchair,
                babySeat: _nbChildren,
                long_dep: longDep,
                lat_dep: latDep,
                long_arr: longArr,
                lat_arr: latArr,
                company: company

            };
            //console.log(data_submit);

            if ($('input[id$="_targetType"]').is(':checked')) {
                var departure_back = $('input[id$="_backAddressDeparture"]').val();
                var arrival_back = $('input[id$="_backAddressArrival"]').val();
                var nb_person_back = $('input[id$="personNumberBack"]').val();
                var estimated_distance_back = $('input[id$="_distanceBack"]').val();
                var departure_date_back = $('input[id$="_backDate"]').val();
                var departure_time_back = $('input[id$="_backTime"]').val();
                var driver_back = $('input[id$="driverBack"]').val();
                if (departure_back.length == 0 || arrival_back.length == 0 || departure_date_back.length == 0
                    || departure_time_back.length == 0 || driver_back.length == 0 || estimated_distance_back.length == 0) {
                    $('.modal-title').text('Error fields');
                    $('#content_modal_body').text('Check the colored fields');
                    $('#courseModal').modal('show');
                    if (departure_back.length == 0) {
                        $('input[id$="_backAddressDeparture"]').css('border-color', '#c89494');
                        $('input[id$="_backAddressDeparture"]').css('background-color', '#c89494');
                    }

                    if (arrival_back.length == 0) {
                        $('input[id$="_backAddressArrival"]').css('border-color', '#c89494');
                        $('input[id$="_backAddressArrival"]').css('background-color', '#c89494');
                    }

                    if (estimated_distance_back.length == 0) {
                        $('input[id$="_distanceBack"]').css('border-color', '#c89494');
                        $('input[id$="_distanceBack"]').css('background-color', '#c89494');
                    }

                    if (departure_date_back.length == 0) {
                        $('input[id$="_backDate"]').css('border-color', '#c89494');
                        $('input[id$="_backDate"]').css('background-color', '#c89494');
                    }

                    if (departure_time_back.length == 0) {
                        $('input[id$="_backTime"]').css('border-color', '#c89494');
                        $('input[id$="_backTime"]').css('background-color', '#c89494');
                    }

                    if (driver_back.length == 0) {
                        $('select[id$="driverBack"]').css('border-color', '#c89494');
                        $('select[id$="driverBack"]').css('background-color', '#c89494');
                    }

                    return false;
                }
                var _laguage_back = $('input[id$="_laguageBack"]').val();
                var _nbWheelchair_back = $('input[id$="_nbWheelchairBack"]').val();
                var _nbChildren_back = $('input[id$="_nbChildrenBack"]').val();

                data_submit.departure_back = departure_back;
                data_submit.arrival_back = arrival_back;
                data_submit.nb_person_back = nb_person_back;
                data_submit.estimated_distance_back = estimated_distance_back;
                data_submit.departure_date_back = departure_date_back;
                data_submit.departure_time_back = departure_time_back;
                data_submit.driver_back = driver_back;

                data_submit.laguage_back = _laguage_back;
                data_submit.wheelchair_back = _nbWheelchair_back;
                data_submit.babySeat_back = _nbChildren_back;
            }

            $.ajax({
                url: Routing.generate('get_price_course'),
                data: data_submit,
                error: function () {
                    console.log('totoCalculate price: An error has occurred');
                },
                success: function (data) {
                    if (data.response != null) {
                        $('input[id$="_priceHT"]').val(data.response.price.price_total);
                        $('input[id$="_priceTTC"]').val(data.response.price.price_total_ttc);
                    } else if (data.code_error != 0) {
                        console.log('Calculate price: An error has occurred');
                    }
                },
                type: 'GET'
            });
        }
    });

    // Calcul arrival hour in create course
    $('input[id$="_departureTime"]').change(function () {
        var depHour = $('input[id$="_departureTime"]').val();
        var b = depHour.split(':');
        var courseTime = $('input[id$="_courseTime"]').val();
        console.log(courseTime);
        var theFutureTime = moment().hour(b[0]).minute(b[1]).add(courseTime,'minute').format("HH:mm");
        var depDate = $('input[id$="_departureDate"]').val();
        var depDateTime = depDate + '_' + b[0] + '_' + b[1];
        if($('input[id$="_forceHourArrived"]').is(':checked')) {
        } else {
            $('input[id$="_arrivalHour"]').val(theFutureTime);
        }
        // remove unvailable drivers from select field
        /* TODO A revoir car bug si groupage
         if (courseTime != '') {
            $.ajax({
                url: Routing.generate('get_unvailable_drivers', {depDate: depDateTime, courseTime: courseTime}),
                error: function () {
                    console.log('Get unvailables drivers: An error has occurred');
                },
                success: function (data) {
                    if (data.response != null) {
                        var selectorDriver = 'select[id$="_driver"]';
                        data.response.forEach(function (item) {
                            var opt = selectorDriver + " option[value=" + item + "]";
                            $(opt).remove();
                        });
                    } else if (data.code_error != 0) {
                        console.log('Get unvailables driver: An error has occurred');
                    }
                },
                type: 'GET'
            });
        } */
    });
    $('input[id$="_arrivalHour"]').change(function () {
        var _arrivalHour = $('input[id$="_arrivalHour"]').val();
        var b = _arrivalHour.split(':');
        var courseTime = $('input[id$="_courseTime"]').val();
        var theCalculatedTime = moment().hour(b[0]).minute(b[1]).subtract(courseTime,'minute').format("HH:mm");
        if($('input[id$="_forceHourArrived"]').is(':checked')) {
            $('input[id$="_departureTime"]').val(theCalculatedTime);
        }
    });
    //add course: get information of the client and fill in the form
    $('input[id$="_reverse"]').click(function() {
            var initvalStart = $('input[id$="_startedAddress"]').val();
            var initvalArriv = $('input[id$="_arrivalAddress"]').val();

            var initvallngDep = $('input[name$="[longDep]"]').val();
            var initvallatDep = $('input[name$="[latDep]"]').val();
            var initvallngArr = $('input[name$="[longArr]"]').val();
            var initvallatArr = $('input[name$="[latArr]"]').val();

            $('input[id$="_arrivalAddress"]').val(initvalStart);
            $('input[id$="_startedAddress"]').val(initvalArriv);

            $('input[name$="[longArr]"]').val(initvallngDep);
            $('input[name$="[latArr]"]').val(initvallatDep);
            $('input[name$="[longDep]"]').val(initvallngArr);
            $('input[name$="[latDep]"]').val(initvallatArr);
    });
    $('select[id$="_client"]').on('change', function() {
        $.ajax({
            url: Routing.generate('get_client_infos', { idClient: this.value }),
            error: function() {
                console.log('Get information client: An error has occurred');
            },
            success: function(data) {
                if(data.response != null) {
                        if($('input[id$="_reverse"]').is(':checked')) {
                            $('input[id$="_arrivalAddress"]').val(data.response.address);
                            $("input[name$='[longArr]']").val(data.response.lng);
                            $("input[name$='[latArr]']").val(data.response.lat);
                        } else {
                            $('input[id$="_startedAddress"]').val(data.response.address);
                            $("input[name$='[longDep]']").val(data.response.lng);
                            $("input[name$='[latDep]']").val(data.response.lat);
                        }
                }else if(data.code_error != 0){
                    console.log('Get information client: An error has occurred');
                }
            },
            type: 'GET'
        });
    });
    // form edit/add course from BO
    $('form#Course').on('submit', function() {
        var departure = $('input[id$="_startedAddress"]').val();
        var arrival = $('input[id$="_arrivalAddress"]').val();
        var price = $('input[id$="_priceHT"]').val();
        $('select[id$="_client"]').css('background-color', '#c89494');

        if(departure.length == 0 || arrival.length == 0 || price.length == 0 || !$('select[id$="_client"] option:selected').length) {
            $('.modal-title').text('Error fields');
            $('#content_modal_body').text('Check departure, arrival and price fields');
            if(departure.length == 0) {
                $('input[id$="_startedAddress"]').css('border-color','#c89494');
                $('input[id$="_startedAddress"]').css('background-color','#c89494');
            }
            if (arrival.length == 0) {
                $('input[id$="_arrivalAddress"]').css('border-color','#c89494');
                $('input[id$="_arrivalAddress"]').css('background-color','#c89494');
            }
            if (price.length == 0 ) {
                $('input[id$="_priceHT"]').css('border-color','#c89494');
                $('input[id$="_priceHT"]').css('background-color','#c89494');
            }
            if (!$('select[id$="_client"] option:selected').length) {
                $('select[id$="_client"]').css('border-color','#c89494');
                $('select[id$="_client"]').css('background-color','#c89494');
            }
            $('#courseModal').modal('show');

            return false;
        }
    });

    $("input,select").on("change paste keyup", function() {
        $(this).css('border-color','#CCCCCC');
        $(this).css('background-color','#ffffff');
    });

    // create new course: select the service transport => get drivers by services
    /*$("[id$='_serviceTransportCourses']").on('change', function() {
        var selectedOption = $("[id$='_serviceTransportCourses'] option:selected").val();
        var selectorDriver = $("[name$='[driver][]']");
        console.log(selectedOption);
        $.ajax({
            url: Routing.generate('service_transport_drivers', {serviceTransport: selectedOption}),
            data: {
                serviceTransport: selectedOption
            },
            error: function() {
                //$('#zone-alert').show('slow');
                //$('#zone-alert-body').html('An error has occurred');
            },
            success: function(data) {
                $(selectorDriver).empty();
                if (data.response != null) {
                    data.response.forEach(function (item) {
                        //$('#zone-alert').hide();
                        $(selectorDriver).append('<option selected="" value="' + item.val + '">' + item.text + '</option>');
                    });
                } else if (data.code_error != 0){
                    // $('#zone-alert').show('slow');
                    //$('#zone-alert-body').html('You need to affect/create zones for company');
                }

            },
            type: 'GET'
        });
        return false;
    });*/


}).apply( this, [ jQuery ]);

