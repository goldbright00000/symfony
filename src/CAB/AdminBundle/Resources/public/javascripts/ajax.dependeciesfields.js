/** 
 * Created by Administrateur on 10/10/2015.
 */
(function( $ ) {
    'use strict';
    $(document).ready(function () {

        /*var company = $(".company-select");
        var zone = $(".service-select");

        // When company gets selected ...
        company.change(function () {
            // ... retrieve the corresponding form.
            var $form = $(this).closest('form');
          pho  // Simulate form data, but only include the selected sport value.
            var data = {};
            data[company.attr('name')] = company.val();
            // Submit data via AJAX to the form's action path.
            var generatedRoute = Routing.generate('get_object_company', { id: company.val(), searched: 'zone' });

            $.ajax({
                url: generatedRoute,
                type: 'GET',
                success: function (html) {
                    var mySelect = $('.service-select');
                    mySelect.empty();
                    $.each(html.result, function(val, text) {
                        mySelect.append(
                            $('<option></option>').val(val).html(text)
                        );
                    });
                }
            });



        });*/


        var selectedOption = $("[id$='_userCompany'] option:selected").val();
        if (typeof selectedOption !== "undefined") {
            cab_callAjax(selectedOption, "[name$='[zones][]']");
        }

        $("[id$='_userCompany']").on('change', function() {
            var selectedOption = $("[id$='_userCompany'] option:selected").val();
            cab_callAjax(selectedOption, "[name$='[zones][]']");
        });

        $("[id$='_vehicleMake']").on('change', function() {
            var selectedOption = $("[id$='_vehicleMake'] option:selected").val();
            cab_vehiclesAjax(selectedOption, "[name$='[vehicleModel]']");
            return false;
        });

        // Add driver: select the company => get zones and service Transport for this company
        $("[id$='_driverCompany']").on('change', function() {
            var selectedOption = $("[id$='_driverCompany'] option:selected").val();
            cab_callAjax(selectedOption, "[name$='[zones][]']");
            cab_serviceAjax(selectedOption, "[name$='[servicesTransport][]']", 'serviceTransport');
            return false;
        });

        // add/edit service transport: get zone for the selected company
        $(".serviceTransport-company-select").on('change', function() {
            var selectedOption = $(".serviceTransport-company-select option:selected").val();
            cab_callAjax(selectedOption, "[name$='[zones][]']");
            return false;
        });
        // edit service transport: select zones by default according the selected company
        /*var selectedOptionCompany = $(".serviceTransport-company-select option:selected").val();
        if (typeof selectedOptionCompany !== "undefined" && selectedOptionCompany != '') {
            cab_callAjax(selectedOptionCompany, "[name$='[zones][]']");
            return false;
        }*/

        // Add/Edit Price FORM
        $("select#tarif-company").on('change', function (e) {
            var id_company = this.value;
            if(id_company){
                cab_serviceAjax(id_company, null, 'tarifFrenchTaxi');
            }
        });

        // Edit/add tarif by adminCompany
        var hidden_company_tarif = $('#tarif-company').val();
        if (typeof hidden_company_tarif !== "undefined" && $('#tarif-company').attr('type') == 'hidden') {
            cab_serviceAjax(hidden_company_tarif, null, 'tarifFrenchTaxi');
        }

        $('#close-alert').click(function(){
            $('#zone-alert').hide('fast');
            $('#vehicle-alert').hide('fast');
            $('#service-alert').hide('fast');
        });

        function cab_callAjax(valueSelected, selector){
            $.ajax({
                url: Routing.generate('get_zone_company', { id: valueSelected }),
                data: {
                    id: valueSelected
                },
                error: function() {
                    $('#zone-alert').show('slow');
                    $('#zone-alert-body').html('An error has occurred');
                },
                //dataType: 'jsonp',
                success: function(data) {
                    $(selector).empty();

                   // $("[name$='[zones][]']").empty();
                    if (data.response != null) {
                        data.response.forEach(function (item) {
                            $('#zone-alert').hide();
                            $(selector).append('<option selected="" value="' + item.val + '">' + item.text + '</option>');
                        });
                    } else if (data.code_error != 0){
                        $('#zone-alert').show('slow');
                        $('#zone-alert-body').html('You need to affect/create zones for company');
                    }

                },
                type: 'GET'
            });

        }

        function cab_vehiclesAjax(valueSelected, selector){
            console.log(valueSelected);

            $.ajax({
                url: Routing.generate('get_model_vehicle', { id: valueSelected }),
                data: {
                    id: valueSelected
                },
                error: function() {
                    $('#vehicle-alert').show('slow');
                    $('#vehicle-alert-body').html('An error has occurred');
                },
                //dataType: 'jsonp',
                success: function(data) {
                    $(selector).empty();

                    // $("[name$='[zones][]']").empty();
                    if(data.response != null) {
                        data.response.forEach(function (item) {
                            $('#vehicle-alert').hide();
                            $(selector).append('<option selected="" value="' + item.val + '">' + item.text + 'TOTO</option>');
                        });
                    }else if(data.code_error != 0){

                        $('#vehicle-alert').show('slow');
                        $('#vehicle-alert-body').html('Error vehiclesAjax !');
                    }
                },
                type: 'GET'
            });
        }

        function cab_serviceAjax(valueSelected, selector, searchedObj)
        {
            $.ajax({
                url: Routing.generate('get_object_company', { id: valueSelected, searched: searchedObj}),
                data: {
                    id: valueSelected
                },
                error: function() {
                    $('#service-alert').show('slow');
                    $('#service-alert-body').html('An error has occurred');
                },
                //dataType: 'jsonp',
                success: function(data) {
                    if(selector !== null) {
                        $(selector).empty();

                        if (data.response != null) {
                            data.response.forEach(function (item) {
                                $('#service-alert').hide();
                                $(selector).append('<option selected="" value="' + item.val + '">' + item.text + '</option>');
                            });
                        } else if (data.code_error != 0) {

                            $('#service-alert').show('slow');
                            $('#service-alert-body').html('Error vehiclesAjax !');
                        }
                    }
                    else{
                        // Case tarifFrenchTaxi
                        var o_response = data.response[0];
                        var tarif_pc = o_response.text.tarif_pc;
                        var tarif_a = o_response.text.tarif_a;
                        var tarif_b = o_response.text.tarif_b;
                        var tarif_c = o_response.text.tarif_c;
                        var tarif_d = o_response.text.tarif_d;
                        var slowWalkPerHour = o_response.text.slowWalkPerHour;
                        var luggage = o_response.text.luggage;
                        var person = o_response.text.person;
                        var minPrice = o_response.text.minPrice;
                        var packageApproach = o_response.text.packageApproach;



                        $("[id$='_tarifPC']").val(tarif_pc);
                        $("[id$='_tarifA']").val(tarif_a);
                        $("[id$='_tarifB']").val(tarif_b);
                        $("[id$='_tarifC']").val(tarif_c);
                        $("[id$='_tarifD']").val(tarif_d);
                        $("[id$='_slowWalkPerHour']").val(slowWalkPerHour);
                        $("[id$='_luggage']").val(luggage);
                        $("[id$='_person']").val(person);
                        $("[id$='_minPrice']").val(minPrice);
                        $("[id$='_packageApproach']").val(packageApproach);
                    }
                },
                type: 'GET'
            });
        }

        $("#cab-vehicle-sss-id").on('change', function() {
            var valueSelected = $("#cab-vehicle-sss-id option:selected").val();
           $.ajax({
                url: Routing.generate('driver_by_vehicle', { vehicleId: valueSelected }),
                data: {
                },
                error: function() {
                    $('#vehicle-alert').show('slow');
                    $('#vehicle-alert-body').html('An error has occurred');
                },
                success: function(data) {
                    if(data.status == true) {
                        $('#vehicle-alert').hide();
                        $('#cab-driver-vf-sss-id > option:first-child').text(data.driver.text);
                    }else if(data.status == false){
                        $('#vehicle-alert').show('slow');
                        $('#vehicle-alert-body').html('An error has occurred, please contact the web master !');
                    }
                },
                type: 'GET'
            });
        });
    });
})(jQuery);