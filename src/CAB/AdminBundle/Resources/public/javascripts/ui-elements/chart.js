/*
Name: 			UI Elements / Charts - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.3.0
*/

(function ($) {

  'use strict';


  /*
  Flot: Bars
  */
  (function () {
    var plot = $.plot('#flotBars', [flotBarsData], {
      colors     : ['#8CC9E8'],
      series     : {
        bars: {
          show    : true,
          barWidth: 0.8,
          align   : 'center'
        }
      },
      xaxis      : {
        mode      : 'categories',
        tickLength: 0
      },
      grid       : {
        hoverable      : true,
        clickable      : true,
        borderColor    : 'rgba(0,0,0,0.1)',
        borderWidth    : 1,
        labelMargin    : 15,
        backgroundColor: 'transparent'
      },
      tooltip    : true,
      tooltipOpts: {
        content     : '%y',
        shifts      : {
          x: -10,
          y: 20
        },
        defaultTheme: false
      }
    });

    var plot = $.plot('#flotBars2', [flotBarsData2], {
      colors     : ['#8CC9E8'],
      series     : {
        bars: {
          show    : true,
          barWidth: 0.8,
          align   : 'center'
        }
      },
      xaxis      : {
        mode      : 'categories',
        tickLength: 0
      },
      grid       : {
        hoverable      : true,
        clickable      : true,
        borderColor    : 'rgba(0,0,0,0.1)',
        borderWidth    : 1,
        labelMargin    : 15,
        backgroundColor: 'transparent'
      },
      tooltip    : true,
      tooltipOpts: {
        content     : '%y',
        shifts      : {
          x: -10,
          y: 20
        },
        defaultTheme: false
      }
    });

    var plot = $.plot('#flotBars3', [flotBarsData3], {
      colors     : ['#8CC9E8'],
      series: {
          lines: {
              show: true,
              lineWidth: 2
          },
          points: {
              show: true
          },
          shadowSize: 0
      },
      xaxis      : {
        mode      : 'categories',
        tickLength: 0
      },
      grid       : {
        hoverable      : true,
        clickable      : true,
        borderColor    : 'rgba(0,0,0,0.1)',
        borderWidth    : 1,
        labelMargin    : 15,
        backgroundColor: 'transparent'
      },
      tooltip    : true,
      tooltipOpts: {
        content     : '%y',
        shifts      : {
          x: -10,
          y: 20
        },
        defaultTheme: false
      }
    });
  })();


  /*
  Morris: Line
  */
  Morris.Line({
                resize    : true,
                element   : 'morrisLine',
                data      : morrisLineData,
                xkey      : ['date'],
                ykeys     : ['a'],
                labels    : ['Chauffeurs login'],
                hideHover : true,
                lineColors: ['#0088cc']
              });


  /*
   Flot: Pie
   */
  var plot = $.plot('#flotPie', flotPieData, {
    series: {
      pie: {
        show   : true,
        combine: {
          color    : '#999',
          threshold: 0.1
        }
      }
    },
    legend: {
      show: false
    },
    grid  : {
      hoverable: true,
      clickable: true
    }
  });


}).apply(this, [jQuery]);