<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 14/12/2016
 * Time: 17:15
 */

namespace CAB\AdminBundle\EventListener;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Handler\Cycle\CycleHandler;
use CAB\CourseBundle\Manager\CourseManager;
use CAB\CourseBundle\Manager\PlaningManager;
use CAB\CourseBundle\Manager\TransactionPaymentManager;
use Symfony\Component\EventDispatcher\GenericEvent;
use CAB\CourseBundle\Manager\CabUserManager;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Psr\Log\LoggerInterface;

/**
 * Class UpdateCycleCourseListener
 *
 * @package CAB\CourseBundle\EventListener
 */
class UpdateCycleCourseListener
{
    /**
     * @var PlaningManager
     */
    private $planingManager;

    /**
     * @var CourseManager
     */
    private $courseManager;

    /**
     * @var TransactionPaymentManager
     */
    private $transactionManager;

    /**
     * @var CabUserManager
     */
    private $userManager;

    /**
     * @var TranslatorInterface
     */
    private $trans;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param PlaningManager            $planingManager
     * @param CycleHandler              $cycleHandler
     * @param CourseManager             $courseManager
     * @param TransactionPaymentManager $transactionManager
     * @param CabUserManager            $userManager
     * @param TranslatorInterface       $trans
     * @param Router                    $router
     * @param LoggerInterface           $logger
     */
    public function __construct(
        PlaningManager $planingManager,
        CycleHandler $cycleHandler,
        CourseManager $courseManager,
        TransactionPaymentManager $transactionManager,
        CabUserManager $userManager,
        TranslatorInterface $trans,
        Router $router,
        LoggerInterface $logger
    ) {
        $this->planingManager = $planingManager;
        $this->cycleHandler = $cycleHandler;
        $this->courseManager = $courseManager;
        $this->transactionManager = $transactionManager;
        $this->userManager = $userManager;
        $this->trans = $trans;
        $this->router = $router;
        $this->logger = $logger;
    }

    /**
     * Listen the generic event dispatched when the status course is updated
     *
     * @param GenericEvent $event
     *
     * @throws \RuntimeException
     * @throws \ErrorException
     */
    public function onUpdateCycleCourse(GenericEvent $event)
    {
        if ($event->getSubject() instanceof Course) {

            /** @var Course $course */
            $course = $event->getSubject();
            $applyAll = false;
            if ($event->hasArgument('apply_next_course_cycle')){
                $applyAll = $event->getArgument('apply_next_course_cycle');
            }
            if ($event->hasArgument('cycle_course') && $course->getIsCycle()) {
                list($listDaysDates, $aDays, $firstDateChanged, $endDateChanged, $courseTimeChanged, $sDates,
                    $timeZone, $currentDateTime, $aListChildCourse, $oldListDays) = $this->paramsParentcourse($course);

                var_dump('toto1');
                exit;

                if (count($aListChildCourse)) {
                    /** @var Course $courseChild */
                    foreach ($aListChildCourse as $courseChild) {
                        //Apply change to all children from current date.
                        if ($applyAll && $currentDateTime < $courseChild->getDepartureDate()) {
                            $this->changeChildCourse($course, $courseChild);
                        } /*elseif ($course->getDepartureDate() < $courseChild->getDepartureDate()) {
                            $this->changeChildCourse($course, $courseChild);
                        }*/
                        if ($firstDateChanged && $courseChild->getDepartureDate() < $course->getDepartureDate()) {
                            $this->courseManager->deleteCourse($courseChild);
                        }
                        if ($endDateChanged && $courseChild->getDepartureDate() > $course->getCycleEndDate()) {
                             $this->courseManager->deleteCourse($courseChild);
                        }
                        if ($courseTimeChanged) {
                             $this->courseManager->setAttr($courseChild, 'departureTime', $course->getDepartureTime());
                             $this->courseManager->setAttr($courseChild, 'arrivalHour', $course->getArrivalHour());
                        }
                        // remove the days which are unchecked after the changes
                        if (!array_key_exists($courseChild->getDepartureDate()->format('l'), $listDaysDates)) {
                            $this->courseManager->deleteCourse($courseChild);
                        }

                        $courseChild->setDaysCycle($sDates);
                        $courseChild->setDateEndParent($course->getCycleEndDate());
                        $courseChild->setCycleEndDate($course->getCycleEndDate());
                        $courseChild->setTimeCycleParent($course->getDepartureTime());
                        $courseChild->setDepartureTime($course->getDepartureTime());

                        $this->courseManager->save($courseChild);

                        /*$diff = array_diff($oldListDays, $aDays);
                        if (count($diff)) {
                            //$this->courseManager->deleteCourse($courseChild);
                        }*/
                    }
                }

                // Add dates which are checked after the changes : Add new days in the cycle list.
                $diff2 = array_diff($aDays, $oldListDays);

                if (count($diff2)) {
                    foreach ($diff2 as $dayWeek) {
                        // check if the $dayweek match the current day
                        $timeNow = date('H:i:s');
                        $dateForTimestamp = "1970-01-01 ".$timeNow;
                        $timestampNow = strtotime($dateForTimestamp);
                        $cycleTimeCourse = $course->getDepartureTime()->getTimestamp();
                        if ($course->getDepartureDate() > $currentDateTime) {
                            $nextCloseDay = $course->getDepartureDate();
                        } else {
                            if ($dayWeek == date('l') && $timestampNow < $cycleTimeCourse) {
                                $nextCloseDay = date_create_from_format('now');
                            } else {
                                $stringDay = 'next ' . $dayWeek;
                                $dt = strtotime($stringDay);
                                $d = date('Y-m-d', $dt);
                                $nextCloseDay = date_create_from_format('Y-m-d', $d, $timeZone);
                            }
                        }
                    }

                    $this->findDatesAndDuplicateCourse(
                        $course,
                        $nextCloseDay->format('Y-m-d'),
                        $course->getCycleEndDate()->format('Y-m-d'),
                        array($dayWeek)
                    );
                }

                // When the departure date is changed  and the new departure date is less then the old departure date
                if ($firstDateChanged && $course->getDateStartParent() > $course->getDepartureDate()) {
                    $this->findDatesAndDuplicateCourse(
                        $course,
                        $course->getDepartureDate()->format('Y-m-d'),
                        $course->getDateStartParent()->format('Y-m-d'),
                        $aDays
                    );
                }

                // When the end date of the cycle is changed and it is greater then the old cycle end date
                if ($endDateChanged && $course->getCycleEndDate() > $course->getDateEndParent()) {
                    $this->findDatesAndDuplicateCourse(
                        $course,
                        $course->getDateEndParent()->format('Y-m-d'),
                        $course->getCycleEndDate()->format('Y-m-d'),
                        $aDays
                    );
                }
                $course->setDaysCycle($sDates);
                $course->setDateStartParent($course->getDepartureDate());
                $course->setDateEndParent($course->getCycleEndDate());
                $course->setTimeCycleParent($course->getDepartureTime());
                $this->courseManager->saveCourse($course);
            } elseif ($event->hasArgument('cycle_course')) {


                $parent = $this->courseManager->loadCourse($course->getCycleParent());
                list($listDaysDates, $aDays, $firstDateChanged, $endDateChanged, $courseTimeChanged, $sDates,
                    $timeZone, $currentDateTime, $aListChildCourse, $oldListDays) = $this->paramsParentcourse($parent);

                //var_dump(count($aListChildCourse));
                //exit;

                if (count($aListChildCourse)) {


                    /** @var Course $courseChild */
                    foreach ($aListChildCourse as $courseChild) {
                            
                        if ($courseChild->getId() !== $course->getId()) {
                            //Apply change to all children from current date.
                            if ($applyAll && $course->getDepartureDate() < $courseChild->getDepartureDate()) {

                                var_dump('t');
                                exit;
                                $this->changeChildCourse($course, $courseChild);
                            }

                            if ($applyAll && $courseTimeChanged && $course->getDepartureDate() < $courseChild->getDepartureDate()) {
                                var_dump('e');
                                exit;
                                $this->courseManager->setAttr($courseChild, 'departureTime', $course->getDepartureTime());
                                $this->courseManager->setAttr($courseChild, 'arrivalHour', $course->getArrivalHour());
                            }
                            $this->courseManager->save($courseChild);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Course $course
     * @param string $start
     * @param string $end
     * @param array  $aDays
     */
    protected function findDatesAndDuplicateCourse($course, $start, $end, $aDays)
    {
        $addedDaysToCycle = $this->cycleHandler->getRange(
            $start,
            $end,
            '+1 day',
            'd/m/Y',
            $aDays
        );

        foreach ($addedDaysToCycle as $dateC) {
            foreach ($dateC as $dateToAdded) {
                $oDate = date_create_from_format('d/m/Y', $dateToAdded);
                $this->cycleHandler->setDuplicateCourse($course, $oDate);
            }
        }
    }

    /**
     * Apply all changes from parent course to child course
     * @param Course $parentcourse
     * @param Course $childrenCourse
     */
    protected function changeChildCourse($parentcourse, $childrenCourse) {
        $listAttributesMayChanged = array(
            'driver' => 'set',
            'applicableTarif' => 'set',
            'courseDetailsTarif' => 'set',
            'typeVehicleCourse' => 'set',
            'serviceTransportCourse' => 'add',
            'serviceParticularPeriodCourse' => 'add',
            'startedAddress' => 'set',
            'indicatorStartedAddress' => 'set',
            'arrivalAddress' => 'set',
            'indicatorArrivalAddress' => 'set',
            'targetType' => 'set',
            'courseTime' => 'set',
            'distance' => 'set',
            'personNumber' => 'set',
            'forceHourArrived' => 'set',
            'arrivalHour' => 'set',
            'priceHT' => 'set',
            'priceTTC' => 'set',
            'courseStatus' => 'set',
            'isHandicap' => 'set',
            'laguage' => 'set',
            'nbWheelchair' => 'set',
            'nbChildren' => 'set',
            'descriptionChildren' => 'set',
            'daysCycle' => 'set',
            'mondayCycle' => 'set',
            'tuesdayCycle' => 'set',
            'wednesdayCycle' => 'set',
            'thursdayCycle' => 'set',
            'fridayCycle' => 'set',
            'saturdayCycle' => 'set',
            'sundayCycle' => 'set',
            'cycleEndDate' => 'set',
            'dateStartParent' => 'set',
            'dateEndParent' => 'set',
            'longDep' => 'set',
            'latDep' => 'set',
            'longArr' => 'set',
            'latArr' => 'set',
            'approachTime' => 'set',

        );
        foreach ($listAttributesMayChanged as $attr => $meth) {

            var_dump($meth);
            exit;
            $methodSet = $meth . ucFirst($attr);
            if ($attr === 'serviceTransportCourse') {
                $methodGet = 'getServiceTransportCourses';
            } elseif ($attr === 'serviceParticularPeriodCourse') {
                $methodGet = 'getServiceParticularPeriodCourses';
            } else {
                $methodGet = 'get' . ucFirst($attr);
            }

            if ($meth === 'add') {
                $methodeRemove = 'remove' . ucFirst($attr);
                if ($childrenCourse->$methodGet()->count( )) {
                    foreach ($childrenCourse->$methodGet()->getSnapshot( ) as $item) {
                        $childrenCourse->$methodeRemove($item);
                    }
                }

                if ($parentcourse->$methodGet()->count( )) {
                    foreach ($parentcourse->$methodGet()->getSnapshot( ) as $item) {
                        $childrenCourse->$methodSet($item);
                    }
                }
            } else {
                $childrenCourse->$methodSet($parentcourse->$methodGet());
            }

        }
        $this->courseManager->saveCourse($childrenCourse);
    }

    /**
     * @param $course
     * @return array
     */
    private function paramsParentcourse($course)
    {
        //check if the days of the cycle are changed.
        list($listDaysDates, $aDays) = $this->cycleHandler->initCycle($course);
        // 1. check if the start date is changed
        $firstDateChanged = false;
        $endDateChanged = false;
        $courseTimeChanged = false;
        if ($course->getDateStartParent() != $course->getDepartureDate()) {
            $firstDateChanged = true;
        }
        if ($course->getDateEndParent() != $course->getCycleEndDate()) {
            $endDateChanged = true;
        }
        $timeCycleParent = $course->getTimeCycleParent()->format('H:i:s');
        $dateForCycleParentTimestamp = "1970-01-01 " . $timeCycleParent;
        $timestampCycleParent = strtotime($dateForCycleParentTimestamp);
        if ($timestampCycleParent != $cycleTimeCourse = $course->getDepartureTime()->getTimestamp()) {
            $courseTimeChanged = true;
        }
        $sDates = '';
        foreach ( $aDays as $d ) {
            $sDates .= $d . '-';
        }

        $timeZone = new \DateTimeZone('GMT');
        $currentDateTime = new \DateTime('now', $timeZone);
        /** @var array $aListChildCourse */
        $aListChildCourse = $this->courseManager->getCycleChildCourse($course);
        $oldListDays = explode('-', $course->getDaysCycle());
        unset($oldListDays[count($oldListDays) - 1]);
        return array($listDaysDates, $aDays, $firstDateChanged, $endDateChanged, $courseTimeChanged, $sDates, $timeZone, $currentDateTime, $aListChildCourse, $oldListDays);
    }
}