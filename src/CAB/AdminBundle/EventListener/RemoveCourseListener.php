<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 16/09/2017
 * Time: 19:56
 */

namespace CAB\AdminBundle\EventListener;

use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Handler\Cycle\CycleHandler;
use CAB\CourseBundle\Manager\CourseManager;
use CAB\CourseBundle\Manager\PlaningManager;
use CAB\CourseBundle\Manager\TransactionPaymentManager;
use Symfony\Component\EventDispatcher\GenericEvent;
use CAB\CourseBundle\Manager\CabUserManager;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Psr\Log\LoggerInterface;

class RemoveCourseListener
{
    /**
     * @var PlaningManager
     */
    private $planingManager;

    /**
     * @var CourseManager
     */
    private $courseManager;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * @var CabUserManager
     */
    private $userManager;

    /**
     * @var TranslatorInterface
     */
    private $trans;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param PlaningManager            $planingManager
     * @param CycleHandler              $cycleHandler
     * @param CourseManager             $courseManager
     * @param TransactionPaymentManager $transactionManager
     * @param CabUserManager            $userManager
     * @param TranslatorInterface       $trans
     * @param Router                    $router
     * @param LoggerInterface           $logger
     */
    public function __construct(
        PlaningManager $planingManager,
        CycleHandler $cycleHandler,
        CourseManager $courseManager,
        TransactionPaymentManager $transactionManager,
        CabUserManager $userManager,
        TranslatorInterface $trans,
        Router $router,
        LoggerInterface $logger
    ) {
        $this->planingManager = $planingManager;
        $this->cycleHandler = $cycleHandler;
        $this->courseManager = $courseManager;
        $this->transactionManager = $transactionManager;
        $this->userManager = $userManager;
        $this->trans = $trans;
        $this->router = $router;
        $this->logger = $logger;
    }

    /**
     * Listen the generic event dispatched when the course is removed
     *
     * @param GenericEvent $event
     *
     * @throws \RuntimeException
     * @throws \ErrorException
     */
    public function onRemoveCourse(GenericEvent $event) {
    }
}