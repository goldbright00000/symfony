<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 23/12/2016
 * Time: 00:14
 */

namespace CAB\AdminBundle\EventListener;

use CAB\CourseBundle\Entity\Company;
use Doctrine\Common\CommonException;
use Symfony\Component\EventDispatcher\GenericEvent;
use FOS\UserBundle\Doctrine\UserManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Twig_Environment as Environment;

/**
 * Class CreateBusinessCompanyListener
 *
 * @package CAB\AdminBundle\EventListener
 */

class CreateBusinessCompanyListener
{
    /**
     * @var UserManager $userManager
     */
    private $userManager;

    /**
     * @var \Swift_Mailer $mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var authorizationChecker
     */
    protected $authorizationChecker;

    public function __construct(UserManager $userManager, \Swift_Mailer $mailer, Environment $twig,
                  EntityManager $entityManager, TokenStorage $tokenStorage, AuthorizationChecker $authorizationChecker
    ) {
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Listen the generic event dispatched when the type is business
     *
     * @param GenericEvent $event
     *
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws CommonException
     * @throws \ErrorException
     */
    public function onCreateBusinessCompany(GenericEvent $event)
    {
        if ($event->getSubject() instanceof Company) {
            /** @var Company $company */
            $company = $event->getSubject();
            $email = $company->getContactMail();
            $checkUserMail = $this->userManager->findUserByEmail($email);

	        // $event->getName() === 'cab.admin.event.create_company_business'
            if ($checkUserMail && $event->getName() === 'cab.admin.event.create_company_business') {
                throw new CommonException('The email is already exist, you should change the contact mail');
            }
            if (null === $checkUserMail && $company->getIsBusiness()) {
                try{
                    $username = mt_rand();
                    $pass = $this->generateRandomPassword(8);
                    $adressCompany = $company->getAddress();
                    $phone = $company->getPhone();
                    $adminBusiness = $this->userManager->createUser();
                    $adminBusiness->setEmail($email);
                    $adminBusiness->setUsername($username);
                    $adminBusiness->setPlainPassword($pass);
                    $adminBusiness->setEnabled(true);
                    $adminBusiness->setFirstName('User');
                    $adminBusiness->setLastName('Business');
                    $adminBusiness->setAddress($adressCompany);
                    $adminBusiness->setPhoneMobile($phone);
                    $adminBusiness->setRoles(['ROLE_COMPANYBUSINESS']);
                    $adminBusiness->setBusinessCompany($company);
                    $adminBusiness->setIsCompany(true);

                    $this->userManager->updateUser($adminBusiness, true);
                    $currentUser = $this->tokenStorage->getToken()->getUser();
                    $company->setContact($currentUser);
                    $this->entityManager->persist($company);
                    $this->entityManager->flush();

                    $message = (new \Swift_Message('Business Company: A new account is created'))
	                    ->setFrom('no-reply@up.taxi')
                        ->setTo($email);
                    $message->setBody(
                        $this->twig->render('CABUserBundle:Registration:email.business.html.twig', ['user' => $adminBusiness, 'pass' => $pass]),
                        'text/html'
                    );
                    $this->mailer->send($message);
                } catch (CommonException $e) {
                    throw new CommonException($e->getMessage().' in '.$e->getFile().' ligne: '.$e->getLine(), $e->getCode());
                }
            }
        }
    }

    private function generateRandomPassword($length) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#!;';
        return substr(str_shuffle(strtolower(sha1(rand() . time() . $chars))),0, $length);
    }
}