<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 24/12/2016
 * Time: 23:00
 */

namespace CAB\AdminBundle\EventListener;


use CAB\CourseBundle\Entity\Course;
use CAB\CourseBundle\Handler\CourseHandler;
use CAB\CourseBundle\Manager\CourseManager;
use CAB\CourseBundle\Manager\TarifManager;
use CAB\UserBundle\Entity\User;
use Doctrine\Common\CommonException;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Translation\TranslatorInterface;

class CourseListener
{
    /**
     * @var CourseManager
     */
    private $courseManager;

    /**
     * @var TarifManager
     */
    private $tarifManager;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @var TranslatorInterface
     */
    private $trans;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var CourseHandler
     */
    private $courseHandler;

    /**
     * Constructor.
     *
     * @param CourseManager $courseManager
     * @param TarifManager $tarifManager
     * @param AuthorizationChecker $authorizationChecker
     * @param TranslatorInterface $trans
     * @param Router $router
     * @param CourseHandler $courseHandler
     */
    public function __construct(
        CourseManager $courseManager,
        TarifManager $tarifManager,
        AuthorizationChecker $authorizationChecker,
        TranslatorInterface $trans,
        Router $router,
        CourseHandler $courseHandler
    ) {
        $this->courseManager = $courseManager;
        $this->tarifManager = $tarifManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->trans = $trans;
        $this->router = $router;
        $this->courseHandler = $courseHandler;
    }

    /** Listen the generic event dispatched when the course is created.
     * Check if the current user is business company and push notification + send message
     *
     * @param GenericEvent $event
     *
     * @throws /Exception
     */
    public function onCreateCourse(GenericEvent $event)
    {
        if ($event->getSubject() instanceof Course) {
            /** @var Course $course */
            $course = $event->getSubject();
            if ($course->getCompany()->getIsBusiness() ||
                ($this->authorizationChecker->isGranted('ROLE_COMPANYBUSINESS') && !$this->authorizationChecker->isGranted('ROLE_ADMINCOMPANY'))
            ) {
                $oCourse = $this->tarifManager->getTarifCourse(array(), $course);
                $this->courseManager->save($oCourse);

                $dateCourseAffected = '';
                if ($course->getDepartureDate() != null && $course->getDepartureTime() != null) {
                    $dateCourseAffected = $course->getDepartureDate()->format('d-m-Y') .
                        $course->getDepartureTime()->format('H:i');
                }
                $drivers = $course->getCompany()->getUsers();
                //send internal message to company drivers
                try {
                    $body = $this->trans->trans(
                        "A course has been affected to you. Below all information of the course<br/>
                            To check the course click on this link below:<br/>
                            ",
                        array(
                            '%name%' => $course->getClient() ? $course->getClient()->getUsedName() : '',
                            '%departure%' => $course->getStartedAddress(),
                            '%arrival%' => $course->getArrivalAddress(),
                            '%datetime%' => $course->getDepartureDate()->format('Y-m-d') . " &nbsp;" . $course->getDepartureTime()
                                    ->format('H:i:s'),
                        )
                    );
                } catch (Exception $e) {
                    throw new RuntimeException();
                }
                $url = $this->router->generate('sonata_course_show', array('id' => $course->getId()));
                $body .= ' <a href="' . $url . '">Course</a>';
                /** @var User $driver */
                foreach ($drivers as $driver) {
                    $subject = 'New course was created ' . $dateCourseAffected;
                    $this->courseHandler->sendInternalMessageWithoutCheck(
                        $course,
                        array($driver->getId()),
                        $subject,
                        $body
                    );
                }
            }
        }
    }
}