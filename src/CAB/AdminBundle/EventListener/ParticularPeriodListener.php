<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda <benhenda.med@gmail.com>
 * Date: 22/10/2017
 * Time: 15:14
 */

namespace CAB\AdminBundle\EventListener;

use CAB\CourseBundle\Entity\Company;
use Doctrine\Common\Persistence\ObjectManager;
use CAB\CourseBundle\Entity\ParticularPeriod;
use Doctrine\Common\CommonException;
use Symfony\Component\EventDispatcher\GenericEvent;
use Psr\Log\LoggerInterface;
Use FOS\UserBundle\Doctrine\UserManager;

/**
 * Class CreateBusinessCompanyListener
 *
 * @package CAB\AdminBundle\EventListener
 */

class ParticularPeriodListener
{
    /**
     * @var UserManager $userManager
     */
    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Listen the generic event dispatched when the type is particular period (ParticularPeriodAdmin)
     *
     * @param GenericEvent $event
     *
     * @throws CommonException
     * @throws \ErrorException
     */
    public function onCreateParticularPeriod(GenericEvent $event)
    {
        if ($event->getSubject() instanceof ParticularPeriod) {
           // $dateStart = $event->getSubject()->getStartedDate();
            //$dateStart->setTimezone(new \DateTimeZone('CET'));
            //$dateEnd = $dateStart->add(new \DateInterval('PT23H59M59S'));
            //$dateEnd->setTimezone(new \DateTimeZone('CET'));
            //$event->getSubject()->setStartedDate($dateStart);
            //$event->getSubject()->setEndDate($dateEnd);
            //$this->om->persist($event->getSubject());
           // $this->om->flush();
        }
    }
}