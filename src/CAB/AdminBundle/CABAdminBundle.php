<?php

namespace CAB\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
class CABAdminBundle extends Bundle
{
	public function build(ContainerBuilder $container)
	{
		parent::build($container);
	}

}
