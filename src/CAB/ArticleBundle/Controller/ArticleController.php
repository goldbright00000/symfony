<?php

namespace CAB\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use CAB\ArticleBundle\Entity\Article;
use CAB\ArticleBundle\Form\ArticleType;

class ArticleController extends Controller
{
    /**
     * @Route("/article", name="cab_article_list")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine();
    	$articles = $em->getRepository('CABArticleBundle:Article')->findAll();
        
        return $this->render('CABArticleBundle:Article:index.html.twig', array(
            'articles' => $articles
        ));
    }

    /**
     * @Route("/article/{slug}", name="cab_article_get_by_slug")
     * @Method("GET")
     */
    public function getArticleAction($slug)
    {
    	$em      = $this->getDoctrine();
        $article = $em->getRepository('CABArticleBundle:Article')->findOneBy(array('slug' => $slug));

        return $this->render('CABArticleBundle:Article:single.html.twig', array(
            'article' => $article
        ));
    }

    /**
     * @Route("/article/new", name="cab_article_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $form    = $this->createCreateForm(new Article());

        return $this->render('CABArticleBundle:Article:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/article/create", name="cab_article_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
    	$article = new Article();
    	$form    = $this->createCreateForm($article);
    	$form->handleRequest($request);

        if ( 'POST' === $request->getMethod() ) {
	    	if ($form->isSubmitted() && $form->isValid()) {
	    		$em = $this->getDoctrine()->getManager();

	    		$em->persist($article); 
	    		$em->flush();

	    	   return $this->redirect($this->generateUrl('cab_article_list'));
	    	}
    	}

    	return $this->render('CABArticleBundle:Article:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Creates a form to create a Article entity.
     *
     * @param Article $entity The entity
     *
     * @return \Symfony\Component\Form\Form
     */
    private function createCreateForm(Article $article)
    {
        $form = $this->createForm(new ArticleType(), $article, array());

        return $form;
    }
}
