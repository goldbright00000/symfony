<?php

namespace CAB\ArticleBundle\Entity;

use CAB\CourseBundle\Entity\Zone;
use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PriceByZone
 *
 * @ORM\Table(name="cab_price_by_zone_SNCF")
 * @ORM\Entity(repositoryClass="CAB\ArticleBundle\Entity\PriceByZoneRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class PriceByZone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Zone", inversedBy="zone")
     * @ORM\JoinColumn(name="zone_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    private $zone;

    /**
     * @var string
     *
     * @ORM\Column(name="service", type="string", length=255, nullable=true)
     */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="priceday", type="string", length=255)
     */
    private $priceday;

    /**
     * @var string
     *
     * @ORM\Column(name="pricenight", type="string", length=255)
     */
    private $pricenight;

    /**
     * @var string
     *
     * @ORM\Column(name="priceferie", type="string", length=255)
     */
    private $priceferie;

    /**
     * @var string
     *
     * @ORM\Column(name="departure_SNCF", type="string", length=255)
     */
    private $departure_SNCF;

    /**
     * @var string
     *
     * @ORM\Column(name="arrival_SNCF", type="string", length=255)
     */
    private $arrival_SNCF;

    /**
     * @var text
     *
     * @ORM\Column(name="information", type="text", length=255)
     */
    private $information;

    /**
     * @var time
     *
     * @ORM\Column(name="hour_night_start", type="time", length=255)
     */
    private $hournightstart;

    /**
     * @var time
     *
     * @ORM\Column(name="hour_night_end", type="time", length=255)
     */
    private $hournightend;

    /**
     * @var string
     *
     * @ORM\Column(name="price_km_day", type="decimal", precision=8, scale=2)
     */
    private $pricekmday;

    /**
     * @var string
     *
     * @ORM\Column(name="price_km_night", type="decimal", precision=8, scale=2)
     */
    private $pricekmnight;

    /**
     * @var string
     *
     * @ORM\Column(name="price_attente", type="decimal", precision=8, scale=2)
     */
    private $priceattente;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zone
     *
     * @param \CAB\CourseBundle\Entity\Zone $zone
     * @return Zone
     */
    public function setZone($zone)
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * Get zone
     *
     * @return string
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * Set service
     *
     * @param string $service
     * @return PriceByZone
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set priceday
     *
     * @param string $priceday
     * @return PriceByZone
     */
    public function setPriceday($priceday)
    {
        $this->priceday = $priceday;

        return $this;
    }

    /**
     * Get priceday
     *
     * @return string
     */
    public function getPriceday()
    {
        return $this->priceday;
    }

    /**
     * Set pricenight
     *
     * @param string $pricenight
     * @return PriceByZone
     */
    public function setPricenight($pricenight)
    {
        $this->pricenight = $pricenight;

        return $this;
    }

    /**
     * Get pricenight
     *
     * @return string
     */
    public function getPricenight()
    {
        return $this->pricenight;
    }

    /**
     * Set priceferie
     *
     * @param string $priceferie
     * @return PriceByZone
     */
    public function setPriceferie($priceferie)
    {
        $this->priceferie = $priceferie;

        return $this;
    }

    /**
     * Get priceferie
     *
     * @return string
     */
    public function getPriceferie()
    {
        return $this->priceferie;
    }

    /**
     * Set departure
     *
     * @param string $departure
     * @return PriceByZone
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    /**
     * Get departure
     *
     * @return string
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set arrival
     *
     * @param string $arrival
     * @return PriceByZone
     */
    public function setArrival($arrival)
    {
        $this->arrival = $arrival;

        return $this;
    }

    /**
     * Get arrival
     *
     * @return string
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * Set information
     *
     * @param string $information
     * @return PriceByZone
     */
    public function setInformation($information)
    {
        $this->information = $information;

        return $this;
    }

    /**
     * Get information
     *
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->zone = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add zone
     *
     * @param \CAB\CourseBundle\Entity\Zone $zone
     * @return PriceByZone
     */
    public function addZone(\CAB\CourseBundle\Entity\Zone $zone)
    {
        $this->zone[] = $zone;

        return $this;
    }

    /**
     * Remove zone
     *
     * @param \CAB\CourseBundle\Entity\Zone $zone
     */
    public function removeZone(\CAB\CourseBundle\Entity\Zone $zone)
    {
        $this->zone->removeElement($zone);
    }

    /**
     * Set departure_SNCF
     *
     * @param string $departureSNCF
     * @return PriceByZone
     */
    public function setDepartureSNCF($departureSNCF)
    {
        $this->departure_SNCF = $departureSNCF;

        return $this;
    }

    /**
     * Get departure_SNCF
     *
     * @return string
     */
    public function getDepartureSNCF()
    {
        return $this->departure_SNCF;
    }

    /**
     * Set arrival_SNCF
     *
     * @param string $arrivalSNCF
     * @return PriceByZone
     */
    public function setArrivalSNCF($arrivalSNCF)
    {
        $this->arrival_SNCF = $arrivalSNCF;

        return $this;
    }

    /**
     * Get arrival_SNCF
     *
     * @return string
     */
    public function getArrivalSNCF()
    {
        return $this->arrival_SNCF;
    }

    /**
     * Set hournightstart
     *
     * @param \DateTime $hournightstart
     * @return PriceByZone
     */
    public function setHournightstart($hournightstart)
    {
        $this->hournightstart = $hournightstart;

        return $this;
    }

    /**
     * Get hournightstart
     *
     * @return \DateTime
     */
    public function getHournightstart()
    {
        return $this->hournightstart;
    }

    /**
     * Set hournightend
     *
     * @param \DateTime $hournightend
     * @return PriceByZone
     */
    public function setHournightend($hournightend)
    {
        $this->hournightend = $hournightend;

        return $this;
    }

    /**
     * Get hournightend
     *
     * @return \DateTime
     */
    public function getHournightend()
    {
        return $this->hournightend;
    }

    /**
     * Set pricekmday
     *
     * @param string $pricekmday
     * @return PriceByZone
     */
    public function setPricekmday($pricekmday)
    {
        $this->pricekmday = $pricekmday;

        return $this;
    }

    /**
     * Get pricekmday
     *
     * @return string
     */
    public function getPricekmday()
    {
        return $this->pricekmday;
    }

    /**
     * Set pricekmnight
     *
     * @param string $pricekmnight
     * @return PriceByZone
     */
    public function setPricekmnight($pricekmnight)
    {
        $this->pricekmnight = $pricekmnight;

        return $this;
    }

    /**
     * Get pricekmnight
     *
     * @return string
     */
    public function getPricekmnight()
    {
        return $this->pricekmnight;
    }

    /**
     * Set priceattente
     *
     * @param string $priceattente
     * @return PriceByZone
     */
    public function setPriceattente($priceattente)
    {
        $this->priceattente = $priceattente;

        return $this;
    }

    /**
     * Get priceattente
     *
     * @return string
     */
    public function getPriceattente()
    {
        return $this->priceattente;
    }
}
