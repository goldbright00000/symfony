<?php
/**
 *
 */

namespace CAB\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Price
 *
 *  @ORM\Table(name="cab_article_price")
 * @ORM\Entity(repositoryClass="CAB\ArticleBundle\Entity\PriceRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Price
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="departure", type="text")
     */
    private $departure;

    /**
     * @var string
     *
     * @ORM\Column(name="arrival", type="text")
     */
    private $arrival;

    /**
     * @var decimal $longDep
     *
     * @ORM\Column(name="long_dep", type="decimal", precision=17, scale=14, nullable=true)
     */
    private $longDep;

    /**
     * @var decimal $latDep
     *
     * @ORM\Column(name="lat_dep", type="decimal", precision=17, scale=14, nullable=true)
     */
    private $latDep;

    /**
     * @var decimal $longArr
     *
     * @ORM\Column(name="long_arr", type="decimal", precision=17, scale=14, nullable=true)
     */
    private $longArr;

    /**
     * @var decimal $latArr
     *
     * @ORM\Column(name="lat_arr", type="decimal", precision=17, scale=14, nullable=true)
     */
    private $latArr;

    /**
     * @var string
     *
     * @ORM\Column(name="price4", type="string", length=255)
     */
    private $price4;

    /**
     * @var string
     *
     * @ORM\Column(name="price6", type="string", length=255)
     */
    private $price6;

    /**
     * @var string
     *
     * @ORM\Column(name="price12", type="string", length=255)
     */
    private $price12;

    /**
     * @var string
     *
     * @ORM\Column(name="price16", type="string", length=255)
     */
    private $price16;

    /**
     * @var string
     *
     * @ORM\Column(name="cible", type="string", length=255)
     */
    private $cible;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text")
     */
    private $metadescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text")
     */
    private $metakeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="distance", type="string", length=255)
     */
    private $distance;

    /**
     * @var string
     *
     * @ORM\Column(name="time", type="string", length=255)
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="link_description", type="text")
     */
    private $linkdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="ref_description", type="string", length=255)
     */
    private $ref;

    /**
     * @var string
     *
     * @ORM\Column(name="country_rate", type="string", length=255)
     */
    private $countryrate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Price
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set departure
     *
     * @param string $departure
     * @return Price
     */
    public function setDeparture($departure)
    {
        $this->departure = $departure;

        return $this;
    }

    /**
     * Get departure
     *
     * @return string
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * Set arrival
     *
     * @param string $arrival
     * @return Price
     */
    public function setArrival($arrival)
    {
        $this->arrival = $arrival;

        return $this;
    }

    /**
     * Get arrival
     *
     * @return string
     */
    public function getArrival()
    {
        return $this->arrival;
    }

    /**
     * Set price4
     *
     * @param string $price4
     * @return Price
     */
    public function setPrice4($price4)
    {
        $this->price4 = $price4;

        return $this;
    }

    /**
     * Get price4
     *
     * @return string
     */
    public function getPrice4()
    {
        return $this->price4;
    }

    /**
     * Set price6
     *
     * @param string $price6
     * @return Price
     */
    public function setPrice6($price6)
    {
        $this->price6 = $price6;

        return $this;
    }

    /**
     * Get price6
     *
     * @return string
     */
    public function getPrice6()
    {
        return $this->price6;
    }

    /**
     * Set price12
     *
     * @param string $price12
     * @return Price
     */
    public function setPrice12($price12)
    {
        $this->price12 = $price12;

        return $this;
    }

    /**
     * Get price12
     *
     * @return string
     */
    public function getPrice12()
    {
        return $this->price12;
    }

    /**
     * Set cible
     *
     * @param string $cible
     * @return Price
     */
    public function setCible($cible)
    {
        $this->cible = $cible;

        return $this;
    }

    /**
     * Get cible
     *
     * @return string
     */
    public function getCible()
    {
        return $this->cible;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Price
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set price16
     *
     * @param string $price16
     * @return Price
     */
    public function setPrice16($price16)
    {
        $this->price16 = $price16;

        return $this;
    }

    /**
     * Get price16
     *
     * @return string
     */
    public function getPrice16()
    {
        return $this->price16;
    }

    /**
     * Set metadescription
     *
     * @param string $metadescription
     * @return Price
     */
    public function setMetadescription($metadescription)
    {
        $this->metadescription = $metadescription;

        return $this;
    }

    /**
     * Get metadescription
     *
     * @return string
     */
    public function getMetadescription()
    {
        return $this->metadescription;
    }

    /**
     * Set metakeywords
     *
     * @param string $metakeywords
     * @return Price
     */
    public function setMetakeywords($metakeywords)
    {
        $this->metakeywords = $metakeywords;

        return $this;
    }

    /**
     * Get metakeywords
     *
     * @return string
     */
    public function getMetakeywords()
    {
        return $this->metakeywords;
    }

    /**
     * Set distance
     *
     * @param string $distance
     * @return Price
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return string
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set time
     *
     * @param string $time
     * @return Price
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set linkdescription
     *
     * @param string $linkdescription
     * @return Price
     */
    public function setLinkdescription($linkdescription)
    {
        $this->linkdescription = $linkdescription;

        return $this;
    }

    /**
     * Get linkdescription
     *
     * @return string
     */
    public function getLinkdescription()
    {
        return $this->linkdescription;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Price
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Price
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set countryrate
     *
     * @param string $countryrate
     * @return Price
     */
    public function setCountryrate($countryrate)
    {
        $this->countryrate = $countryrate;

        return $this;
    }

    /**
     * Get countryrate
     *
     * @return string
     */
    public function getCountryrate()
    {
        return $this->countryrate;
    }

    /**
     * Set longDep
     *
     * @param string $longDep
     * @return Price
     */
    public function setLongDep($longDep)
    {
        $this->longDep = $longDep;

        return $this;
    }

    /**
     * Get longDep
     *
     * @return string
     */
    public function getLongDep()
    {
        return $this->longDep;
    }

    /**
     * Set latDep
     *
     * @param string $latDep
     * @return Price
     */
    public function setLatDep($latDep)
    {
        $this->latDep = $latDep;

        return $this;
    }

    /**
     * Get latDep
     *
     * @return string
     */
    public function getLatDep()
    {
        return $this->latDep;
    }

    /**
     * Set longArr
     *
     * @param string $longArr
     * @return Price
     */
    public function setLongArr($longArr)
    {
        $this->longArr = $longArr;

        return $this;
    }

    /**
     * Get longArr
     *
     * @return string
     */
    public function getLongArr()
    {
        return $this->longArr;
    }

    /**
     * Set latArr
     *
     * @param string $latArr
     * @return Price
     */
    public function setLatArr($latArr)
    {
        $this->latArr = $latArr;

        return $this;
    }

    /**
     * Get latArr
     *
     * @return string
     */
    public function getLatArr()
    {
        return $this->latArr;
    }
}
