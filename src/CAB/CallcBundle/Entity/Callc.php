<?php
/**
 * Created by PhpStorm.
 * Project: cab
 * User: m.benhenda
 * Date: 06/11/2017
 * Time: 21:09
 */

namespace CAB\CallcBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;


/**
 * Class Callc
 * @package CAB\CallcBundle\Entity
 *
 * @ORM\Table("cab_callc")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="CAB\CallcBundle\Entity\CallcRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Callc
{
    /**
     * @var int $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;

    /**
     * @var int $agentId
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User", inversedBy="callc", cascade={"remove", "persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id", nullable=true)
     * @Expose
     * @Groups({"Public"})
     */
    protected $agentId;

    /**
     * @var $appelDuration
     * @ORM\Column(type="string", length=64)
     */
    protected $callDuration;

    /**
     * @var \DateTime $datePickUp
     * @ORM\Column(type="datetime")
     */
    protected $datePickUp;

    /**
     * @var string $callSubject
     * @ORM\Column(type="text", length=255)
     */
    protected $callSubject;

    /**
     * @var int $course
     * @ORM\ManyToOne(targetEntity="CAB\CourseBundle\Entity\Course", inversedBy="callc", cascade={"remove", "persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @Expose
     * @Groups({"Public"})
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $course;

    /**
     * @var string $cidname
     * @ORM\Column(type="string", length=255)
     */
    protected $cidname;

    /**
     * @var string $ciddnid
     * @ORM\Column(type="string", length=255)
     */
    protected $ciddnid;

    /**
     * @var datetime $calleventtime
     *
     * @ORM\Column(type="datetime", name="callevent_time", nullable=true)
     */
    private $calleventtime;

    /**
     * @var string $infocallentreprise
     * @ORM\Column(type="string", length=255)
     */
    protected $infocallentreprise;

    /**
     * @var string $infocallnom
     * @ORM\Column(type="string", length=255)
     */
    protected $infocallnom;

    /**
     * @var string $infocallprenom
     * @ORM\Column(type="string", length=255)
     */
    protected $infocallprenom;

    /**
     * @var string $infocalltelephone
     * @ORM\Column(type="string", length=255)
     */
    protected $infocalltelephone;

    /**
     * @var string $infocallcomment
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    protected $infocallcomment;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=true)
     */
    private $createdAt;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
    *
    * @ORM\PreUpdate
    */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agentId
     *
     * @param integer $agentId
     * @return Callc
     */
    public function setAgentId($agentId)
    {
        $this->agentId = $agentId;

        return $this;
    }

    /**
     * Get agentId
     *
     * @return integer
     */
    public function getAgentId()
    {
        return $this->agentId;
    }

    /**
     * Set callDuration
     *
     * @param string $callDuration
     * @return Callc
     */
    public function setCallDuration($callDuration)
    {
        $this->callDuration = $callDuration;

        return $this;
    }

    /**
     * Get callDuration
     *
     * @return string
     */
    public function getCallDuration()
    {
        return $this->callDuration;
    }

    /**
     * Set datePickUp
     *
     * @param \DateTime $datePickUp
     * @return Callc
     */
    public function setDatePickUp($datePickUp)
    {
        $this->datePickUp = $datePickUp;

        return $this;
    }

    /**
     * Get datePickUp
     *
     * @return \DateTime
     */
    public function getDatePickUp()
    {
        return $this->datePickUp;
    }

    /**
     * Set callSubject
     *
     * @param string $callSubject
     * @return Callc
     */
    public function setCallSubject($callSubject)
    {
        $this->callSubject = $callSubject;

        return $this;
    }

    /**
     * Get callSubject
     *
     * @return string
     */
    public function getCallSubject()
    {
        return $this->callSubject;
    }

    /**
     * Set course
     *
     * @param integer $course
     * @return Callc
     */
    public function setCourse($course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return integer
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Callc
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Callc
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set cidname
     *
     * @param string $cidname
     * @return Callc
     */
    public function setCidname($cidname)
    {
        $this->cidname = $cidname;

        return $this;
    }

    /**
     * Get cidname
     *
     * @return string
     */
    public function getCidname()
    {
        return $this->cidname;
    }

    /**
     * Set ciddnid
     *
     * @param string $ciddnid
     * @return Callc
     */
    public function setCiddnid($ciddnid)
    {
        $this->ciddnid = $ciddnid;

        return $this;
    }

    /**
     * Get ciddnid
     *
     * @return string
     */
    public function getCiddnid()
    {
        return $this->ciddnid;
    }

    /**
     * Set calleventtime
     *
     * @param \DateTime $calleventtime
     * @return Callc
     */
    public function setCalleventtime($calleventtime)
    {
        $this->calleventtime = $calleventtime;

        return $this;
    }

    /**
     * Get calleventtime
     *
     * @return \DateTime
     */
    public function getCalleventtime()
    {
        return $this->calleventtime;
    }

    /**
     * Set infocallentreprise
     *
     * @param string $infocallentreprise
     * @return Callc
     */
    public function setInfocallentreprise($infocallentreprise)
    {
        $this->infocallentreprise = $infocallentreprise;

        return $this;
    }

    /**
     * Get infocallentreprise
     *
     * @return string
     */
    public function getInfocallentreprise()
    {
        return $this->infocallentreprise;
    }

    /**
     * Set infocallnom
     *
     * @param string $infocallnom
     * @return Callc
     */
    public function setInfocallnom($infocallnom)
    {
        $this->infocallnom = $infocallnom;

        return $this;
    }

    /**
     * Get infocallnom
     *
     * @return string
     */
    public function getInfocallnom()
    {
        return $this->infocallnom;
    }

    /**
     * Set infocallprenom
     *
     * @param string $infocallprenom
     * @return Callc
     */
    public function setInfocallprenom($infocallprenom)
    {
        $this->infocallprenom = $infocallprenom;

        return $this;
    }

    /**
     * Get infocallprenom
     *
     * @return string
     */
    public function getInfocallprenom()
    {
        return $this->infocallprenom;
    }

    /**
     * Set infocalltelephone
     *
     * @param string $infocalltelephone
     * @return Callc
     */
    public function setInfocalltelephone($infocalltelephone)
    {
        $this->infocalltelephone = $infocalltelephone;

        return $this;
    }

    /**
     * Get infocalltelephone
     *
     * @return string
     */
    public function getInfocalltelephone()
    {
        return $this->infocalltelephone;
    }

    /**
     * Set infocallcomment
     *
     * @param string $infocallcomment
     * @return Callc
     */
    public function setInfocallcomment($infocallcomment)
    {
        $this->infocallcomment = $infocallcomment;

        return $this;
    }

    /**
     * Get infocallcomment
     *
     * @return string
     */
    public function getInfocallcomment()
    {
        return $this->infocallcomment;
    }
}
