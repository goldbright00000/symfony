<?php

namespace CAB\CallcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CABCallcBundle:Default:index.html.twig', array('name' => $name));
    }
}
