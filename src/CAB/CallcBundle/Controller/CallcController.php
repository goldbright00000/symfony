<?php

namespace CAB\CallcBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use CAB\CallcBundle\Entity\Callc;


class CallcController extends Controller
{
    /**
     * @Route("/callc", name="cab_call_list")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $callc = $em->getRepository('CABCallcBundle:Callc')->findAll();

        return $this->render('CABCallcBundle:Default:index.html.twig', array(
            'article' => $callc
        ));
    }
}
