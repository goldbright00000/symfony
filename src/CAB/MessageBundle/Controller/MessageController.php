<?php

namespace CAB\MessageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\MessageBundle\Provider\ProviderInterface;

class MessageController extends Controller
{
    /**
     * Displays a thread, also allows to reply to it
     *
     * @Route("thread/{threadId}/view", name="cab_message_thread_view")
     *
     * @Template()
     * @Method("GET")
     * @param string $threadId the thread id
     *
     * @return array|redirection
     */
    public function messageThreadsAction($threadId)
    {
    	$thread = $this->getProvider()->getThread($threadId);
        $form = $this->container->get('fos_message.reply_form.factory')->create($thread);
        $formHandler = $this->container->get('fos_message.reply_form.handler');
        if ($message = $formHandler->process($form)) {
            return new RedirectResponse($this->container->get('router')->generate('cab_message_thread_view', array(
                'threadId' => $message->getThread()->getId()
            )));
        }

        return array(
            'form' => $form->createView(),
            'thread' => $thread
        );
    }

    /**
     * Displays a thread in box
     *
     * @Route("inbox", name="cab_message_inbox")
     *
     * @Template()
     *
     * @return Response $response
     */
    public function cabInboxAction()
    {
        $threads = $this->getProvider()->getInboxThreads();

        return array(
            'threads' => $threads
        );
    }

    /**
     * Displays the authenticated participant messages sent
     *
     * @Route("sent", name="cab_message_sent")
     *
     * @Template()
     *
     * @return Response $response
     */
    public function cabSentAction()
    {
        $threads = $this->getProvider()->getSentThreads();

        return array(
            'threads' => $threads
        );
    }

    /**
     * Displays the authenticated participant deleted threads
     *
     * @Route("deleted", name="cab_message_sent")
     *
     * @Template()
     *
     * @return Response $response
     */
    public function cabDeletedAction()
    {
        $threads = $this->getProvider()->getDeletedThreads();

        return array(
            'threads' => $threads
        );
    }

    /**
     * Create a new message thread
     *
     * @Route("new", name="cab_message_thread_new")
     *
     * @Template()
     *
     * @return Response $response
     */
    public function cabNewThreadAction()
    {
        $form = $this->container->get('fos_message.new_thread_form.factory')->create();
        $formHandler = $this->container->get('fos_message.new_thread_form.handler');

        if ($message = $formHandler->process($form)) {
            return new RedirectResponse($this->container->get('router')->generate('cab_message_thread_view', array(
                'threadId' => $message->getThread()->getId()
            )));
        }

        return array(
            'form' => $form->createView(),
            'data' => $form->getData()
        );
    }

    /**
     * Deletes a thread
     *
     * @Route("{threadId}/delete", name="cab_message_thread_delete")
     *
     * @Template()
     *
     * @return RedirectResponse
     */
    public function cabDeleteAction($threadId)
    {
        $thread = $this->getProvider()->getThread($threadId);
        $this->container->get('fos_message.deleter')->markAsDeleted($thread);
        $this->container->get('fos_message.thread_manager')->saveThread($thread);

        return new RedirectResponse($this->container->get('router')->generate('cab_message_inbox'));
    }

    /**
     * Undeletes a thread
     *
     * @Route("{threadId}/undelete", name="cab_message_thread_undelete")
     *
     * @Template()
     *
     * @return RedirectResponse
     */
    public function cabUndeleteAction($threadId)
    {
        $thread = $this->getProvider()->getThread($threadId);
        $this->container->get('fos_message.deleter')->markAsUndeleted($thread);
        $this->container->get('fos_message.thread_manager')->saveThread($thread);

        return new RedirectResponse($this->container->get('router')->generate('cab_message_inbox'));
    }

    /**
     * Searches for messages in the inbox and sentbox
     *
     * @Route("search", name="cab_message_search")
     *
     * @Template()
     *
     * @return Response $response
     */
    public function cabSearchAction()
    {
        $query = $this->container->get('fos_message.search_query_factory')->createFromRequest();
        $threads = $this->container->get('fos_message.search_finder')->find($query);

        return array(
            'query' => $query,
            'threads' => $threads
        );
    }

    /**
     * Gets the provider service
     *
     * @return ProviderInterface
     */
    protected function getProvider()
    {
        return $this->container->get('fos_message.provider');
    }

}
