<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 20/01/2016
 * Time: 02:19
 */

namespace CAB\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Entity\ThreadMetadata as BaseThreadMetadata;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\Table(name="cab_thread_metadata")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="CAB\MessageBundle\Entity\ThreadMetaDataRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class ThreadMetadata extends BaseThreadMetadata {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *   targetEntity="CAB\MessageBundle\Entity\Thread",
     *   inversedBy="metadata"
     * )
     * @var \FOS\MessageBundle\Model\ThreadInterface
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $thread;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User")
     * @var \FOS\MessageBundle\Model\ParticipantInterface
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $participant;
}
