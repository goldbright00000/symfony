<?php
/**
 * Created by PhpStorm.
 * User: m.benhenda(benhenda.med@gmail.com)
 * Date: 20/01/2016
 * Time: 02:09
 */

namespace CAB\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\MessageBundle\Entity\Thread as BaseThread;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ORM\Entity
 * @ORM\Table(name="cab_thread")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="CAB\MessageBundle\Entity\ThreadRepository")
 * @ExclusionPolicy("all")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
 */
class Thread extends BaseThread
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CAB\UserBundle\Entity\User")
     * @var \FOS\MessageBundle\Model\ParticipantInterface
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $createdBy;

    /**
     * @ORM\OneToMany(
     *   targetEntity="CAB\MessageBundle\Entity\Message",
     *   mappedBy="thread",
     *   cascade={"all"}
     * )
     * @var Message[]|\Doctrine\Common\Collections\Collection
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $messages;

    /**
     * @ORM\OneToMany(
     *   targetEntity="CAB\MessageBundle\Entity\ThreadMetadata",
     *   mappedBy="thread",
     *   cascade={"all"}
     * )
     * @var ThreadMetadata[]|\Doctrine\Common\Collections\Collection
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $metadata;

    /**
     * @ORM\ManyToOne(
     *   targetEntity="CAB\CourseBundle\Entity\Course",
     *   inversedBy="courseThread"
     * )
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="cache_long_time")
     */
    protected $course;

    /**
     * Remove messages
     *
     * @param \CAB\MessageBundle\Entity\Message $messages
     */
    public function removeMessage(\CAB\MessageBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Add metadata
     *
     * @param \CAB\MessageBundle\Entity\ThreadMetadata $metadata
     * @return Thread
     */
    public function addMetadatum(\CAB\MessageBundle\Entity\ThreadMetadata $metadata)
    {
        $this->metadata[] = $metadata;

        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \CAB\MessageBundle\Entity\ThreadMetadata $metadata
     */
    public function removeMetadatum(\CAB\MessageBundle\Entity\ThreadMetadata $metadata)
    {
        $this->metadata->removeElement($metadata);
    }

    /**
     * Get metadata
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set course
     *
     * @param \CAB\CourseBundle\Entity\Course $course
     * @return Thread
     */
    public function setCourse(\CAB\CourseBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \CAB\CourseBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }
}
