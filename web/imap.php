<?php
/**
 * Created by PhpStorm.
 * Project: cab2018
 * User: m.benhenda
 * Date: 09/01/2018
 * Time: 00:07
 */
$inbox = imap_open("{imap.ionos.fr:143}INBOX", "test.galap@tasm.fr", "Nectariencar60!");
$emailCount = imap_num_msg($inbox);
$date = date ( "d M Y" );
var_dump($date);
$uids = imap_search ( $inbox, "SINCE \"$date\"" );
$sorted_mbox = imap_sort($inbox, SORTDATE, 1);
$totalrows = imap_num_msg($inbox);
print_r($uids);
foreach ($uids as $uid) {
	$sendMail = false;
	$headerinfo = imap_headerinfo($inbox, $uid );
	$from = $headerinfo->from[0];
	$sender = $from->mailbox.'@'.$from->host;

	if (($headerinfo->subject === 'Création')) {
		$message = imap_fetchbody($inbox,$uid,2);
		$structure = imap_fetchstructure($inbox,$uid);
		$countParts = count($structure->parts);
		$attachments = array();
		if(isset($structure->parts) && count($structure->parts)) {
			for($i = 0; $i < $countParts; $i++) {
				$attachments[$i] = array(
					'is_attachment' => false,
					'filename' => '',
					'name' => '',
					'attachment' => '');

				if($structure->parts[$i]->ifdparameters) {
					foreach($structure->parts[$i]->dparameters as $object) {
						if(strtolower($object->attribute) == 'filename') {
							$attachments[$i]['is_attachment'] = true;
							$attachments[$i]['filename'] = $object->value;
						}
					}
				}

				if($structure->parts[$i]->ifparameters) {
					foreach($structure->parts[$i]->parameters as $object) {
						if(strtolower($object->attribute) == 'name') {
							$attachments[$i]['is_attachment'] = true;
							$attachments[$i]['name'] = $object->value;
						}
					}
				}

				if($attachments[$i]['is_attachment']) {
					$attachments[$i]['attachment'] = imap_fetchbody($inbox, $uid, $i+1);
					if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
						$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
					}
					elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
						$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
					}
				}
			} // for($i = 0; $i < count($structure->parts); $i++)
		} // if(isset($structure->parts) && count($structure->parts))

		if(count($attachments)!=0){
			/* iterate through each attachment and save it */
			foreach($attachments as $attachment)
			{
				if($attachment['is_attachment'] == 1)
				{
					$contentCSV = utf8_encode($attachment['attachment']);
					$Data = str_getcsv($contentCSV, "\n"); //parse the rows
					echo "<pre>";
					$csvRow = 0;
					foreach($Data as &$row) {
						$row = str_getcsv($row, ";");
						if ($csvRow === 0) {
							print_r($row);
						}
						if ($csvRow === 1) {
							print_r($row);
							$commandSncf = $row[0];
							$commndType = $row[1];
							$codeJS = $row[5];
							$codeRLT = $row[6];
							$agentTransported = $row[10] . ' ' . $row[11];
							$agentTel = $row[12];
							$courseDate = $row[13];
							$courseTime = $row[14];
							$courseDepartureAddress = $row[18] . ' ' . $row[19] . ' ' . $row[20];
							$courseArrivalAddress = $row[28] . ' ' . $row[29] . ' ' . $row[30];
							$codeBupo = $row[34];
						}
						$csvRow ++;
					}
					echo "</pre>";
					exit;
					$filename = $attachment['name'];
					if(empty($filename)) $filename = $attachment['filename'];

					if(empty($filename)) $filename = time() . '.dat';

					$fp = fopen($uid -1 . '-' . $filename, 'w+') or die("can't open file");

					fwrite($fp, utf8_encode($attachment['attachment']));
					fclose($fp);
					$sendMail = true;
				}
			}
		}
	}

	if ($sendMail) {
		$status = imap_setflag_full($inbox, $uid, "\\Seen \\Flagged");
		$message = "DONE!\r\nLine 1\r\nLine 2\r\nLine 3";
		$message = wordwrap($message, 70, "\r\n");
		$headers = 'From: no-reply@tasm.fr' . "\r\n" .
			"Reply-To: $sender" . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
		//mail($sender, 'TASM Confirm Reception of Command SNCF', $message, $headers);
	}
}

imap_close($imap);
